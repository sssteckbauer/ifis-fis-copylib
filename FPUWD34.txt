      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWD34                              04/24/00  13:13  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWD34.
           02  SAVE-DBKEY-ID-4086-PU34        COMP      PIC S9(8).
           02  SYSTEM-DATE-PU34               COMP-3    PIC S9(8).
           02  MAND-ERR-FLAG-PU34                       PIC X(1).
           02  ERR-FLAG-PU34                            PIC X(1).
           02  ORGZN-TITLE-PU34                         PIC X(35).
           02  DELETE-FLAG-PU34                         PIC X(1).
           02  TRANS-DATE-PU34                COMP-3    PIC S9(8).
           02  ADDSHP-FLAG-PU34                         PIC X(1).
           02  WORK-SHIP-TO-CODE-PU34                   PIC X(10).
           02  WORK-ASSGN-SHIP-TO-PU34 REDEFINES
               WORK-SHIP-TO-CODE-PU34.
               03  WORK-FILLER-PU34                     PIC X(4).
               03  SHIP-TO-CODE-PU34                    PIC 9(6).
