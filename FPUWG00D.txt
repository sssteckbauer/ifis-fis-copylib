      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWG00D                             04/24/00  12:53  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWG00D.
           02  UNVRS-CODE-6001-PU00                     PIC X(2).
           02  VNDR-CODE-4073-PU00.
               03  VNDR-ID-DIGIT-ONE-4073-PU00          PIC X(1).
               03  VNDR-ID-LAST-NINE-4073-PU00          PIC X(9).
           02  RQST-CODE-4070-PU00                      PIC X(8).
           02  ITEM-NMBR-4071-PU00                      PIC X(4).
           02  SEQ-NMBR-4080-PU00                       PIC X(4).
           02  DCMNT-REF-NMBR-PU00                      PIC X(10).
           02  PO-NMBR-4083-PU00                        PIC X(8).
           02  CHNG-SEQ-NMBR-4096-PU00                  PIC X(3).
           02  ITEM-NMBR-4085-PU00                      PIC X(4).
           02  SEQ-NMBR-4061-PU00                       PIC X(4).
           02  BID-NMBR-4088-PU00                       PIC X(8).
           02  ITEM-NMBR-4089-PU00                      PIC X(4).
           02  BUYER-CODE-4072-PU00                     PIC X(4).
           02  AGRMT-CODE-4081-PU00                     PIC X(15).
           02  CMDTY-CODE-4074-PU00                     PIC X(8).
           02  SHIP-TO-CODE-4086-PU00                   PIC X(6).
           02  RTRN-RSN-CODE-4201-PU00                  PIC X(4).
           02  COA-CODE-4000-PU00                       PIC X(1).
           02  ORGZN-CODE-4002-PU00                     PIC X(6).
           02  WORK-DATE-PU00.
               03  MONTH-DATE-PU00                      PIC X(2).
               03  DAY-DATE-PU00                        PIC X(2).
               03  YEAR-DATE-PU00                       PIC X(2).
           02  PO-RTRN-CODE-4099-PU00                   PIC X(8).
           02  CLAUSE-CODE-4209-PU00                    PIC X(8).
           02  RPRT-CODE-4060-PU00                      PIC X(8).
           02  PRNTR-DEST-CODE-4167-PU00                PIC X(8).
