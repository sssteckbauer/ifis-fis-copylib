      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R6156-PRSN-PRVNM' TO DBLINK-CURR-PARAGRAPH.   276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R6156-PRSN-PRVNM' TO RECORD-NAME.                      276 BRTN
YYY991     MOVE 'F-PRSN' TO AREA-NAME.                                  276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6156-PRSN-PRVNM TO DBLINK-RECORD-MADE-CURRENT.  276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R6156-PRSN-PRVNM-EXIT                     276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE PPN-FK-PRS-IREF-ID TO DBLINK-R6156-PRSN-PRVNM-01    276 BRTN
YYY991         MOVE PPN-FK-PCH-CHG-DT TO DBLINK-R6156-PRSN-PRVNM-02     276 BRTN
YYY991         MOVE PPN-PRS-SET-TS TO DBLINK-R6156-PRSN-PRVNM-03        276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6156-PRSN-PRVNM TO DBLINK-VIEW-NAME-CURRENT.    276 BRTN
YYY991     MOVE DBLINK-R6156-PRSN-PRVNM-KEY TO DBLINK-VIEW-200-CURRENT. 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-6117-6156                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE PPN-FK-PRS-IREF-ID TO DBLINK-S-6117-6156-01.            276 BRTN
YYY991     MOVE PPN-FK-PCH-CHG-DT TO DBLINK-S-6117-6156-02.             276 BRTN
YYY991     MOVE PPN-PRS-SET-TS TO DBLINK-S-6117-6156-03.                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-6313-6156                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE PPN-FK-PCH-IREF-ID TO DBLINK-S-6313-6156-01.            276 BRTN
YYY991     MOVE PPN-FK-PCH-CHG-DT TO DBLINK-S-6313-6156-02.             276 BRTN
YYY991     MOVE PPN-FK-PCH-SET-TS TO DBLINK-S-6313-6156-03.             276 BRTN
YYY991     MOVE PPN-PCH-SET-TS TO DBLINK-S-6313-6156-04.                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-PRSN-NAME                                            276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE PPN-NAME-KEY TO DBLINK-S-INDX-PRSN-NAME-01.             276 BRTN
YYY991     MOVE PPN-IDX-PRSN-NAME-TS TO DBLINK-S-INDX-PRSN-NAME-02.     276 BRTN
