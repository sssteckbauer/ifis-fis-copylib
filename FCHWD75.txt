      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWD75                              04/24/00  12:58  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWD75.
           02  WORK-SLCTN-DATE-CH75           COMP-3    PIC S9(8).
           02  WORK-SYSTEM-DATE-CH75          COMP-3    PIC S9(8).
           02  WORK-SAVE-DBKEY-ID-4033-CH75
                                     COMP   OCCURS 11   PIC S9(8).
           02  WORK-SAVE-DBKEY-ID-4062-CH75
                                     COMP   OCCURS 11   PIC S9(8).
           02  WORK-LOOP-FLAG-CH75                      PIC X(1).
           02  WORK-SAVE-DBKEY-CH75           COMP      PIC S9(8).
           02  ACTN-FLAG-CH75                           PIC X(1).
           02  WORK-ADD-FLAG-CH75                       PIC X(1).
           02  WORK-LIST-FLAG-CH75                      PIC X(1).
           02  WORK-SEL-FLAG-CH75                       PIC X(1).
           02  SEL-FLAG-CH75                            PIC X(1).
