       01  R4197-GNRL-LDGR.
           02  DB-PROC-ID-4197.
               03  USER-CODE-4197                       PIC X(8).
               03  LAST-ACTVY-DATE-4197                 PIC X(5).
               03  SYSTEM-TIME-STAMP-4197               PIC X(8).
               03  TRMNL-ID-4197                        PIC X(8).
               03  PURGE-FLAG-4197                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  GNRL-LDGR-KEY-4197.
               03  UNVRS-CODE-4197                      PIC X(2).
               03  COA-CODE-4197                        PIC X(1).
               03  FSCL-CC-YR-4197.
                   04  FSCL-CC-4197                     PIC X(2).
                   04  FSCL-YR-4197                     PIC X(2).
               03  FUND-CODE-4197                       PIC X(6).
               03  ACCT-CODE-4197                       PIC X(6).
           02  ACTVY-DATE-4197                COMP-3    PIC S9(8).
           02  SMRY-PRDC-DEBITS-4197
                                     COMP-3 OCCURS 14   PIC 9(13)V99.
           02  SMRY-PRDC-CRDTS-4197  COMP-3 OCCURS 14   PIC 9(13)V99.
           02  BEG-SMRY-PRDC-DEBITS-4197      COMP-3    PIC 9(13)V99.
           02  BEG-SMRY-PRDC-CREDITS-4197     COMP-3    PIC 9(13)V99.
           02  FILLER02                                 PIC X(13).
