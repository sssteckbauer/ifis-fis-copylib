      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FUTWM06                              04/24/00  12:48  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FUTWM06.
           02  NAME-KEY-6157-R-6186-UT06                PIC X(35).
           02  ADR-TYPE-6139-R-6185-UT06    OCCURS  4   PIC X(2).
           02  LINE-ADR-6139-1-6185-UT06    OCCURS  4   PIC X(35).
           02  LINE-ADR-6139-2-6185-UT06    OCCURS  4   PIC X(35).
           02  LINE-ADR-6139-3-6185-UT06    OCCURS  4   PIC X(35).
           02  ACH-IND-UT06                 OCCURS  4   PIC X(35).
           02  CITY-NAME-UT06               OCCURS  4   PIC X(35).
           02  STATE-CODE-UT06              OCCURS  4   PIC X(35).
           02  START-DATE-6139-R-6185-UT06  OCCURS  4   PIC X(6).
           02  END-DATE-6139-R-6185-UT06    OCCURS  4   PIC X(6).
           02  ACTN-CODE-GROUP-UT06.
               03  ACTN-CODE-UT06           OCCURS  4   PIC X(1).
