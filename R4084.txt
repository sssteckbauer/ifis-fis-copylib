       01  R4084-PO-CLASS.
           02  DB-PROC-ID-4084.
               03  USER-CODE-4084                       PIC X(8).
               03  LAST-ACTVY-DATE-4084                 PIC X(5).
               03  TRMNL-ID-4084                        PIC X(8).
               03  PURGE-FLAG-4084                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  PO-CLASS-KEY-4084.
               03  UNVRS-CODE-4084                      PIC X(2).
               03  PO-CLASS-CODE-4084                   PIC X(1).
           02  PO-CLASS-DESC-4084                       PIC X(35).
           02  START-DATE-4084                COMP-3    PIC S9(8).
           02  END-DATE-4084                  COMP-3    PIC S9(8).
           02  ACTVY-DATE-4084                COMP-3    PIC S9(8).
           02  MAX-AMT-4084                   COMP-3    PIC 9(10)V99.
