       01  R4006-LCTN.
           02  DB-PROC-ID-4006.
               03  USER-CODE-4006                       PIC X(8).
               03  LAST-ACTVY-DATE-4006                 PIC X(5).
               03  TRMNL-ID-4006                        PIC X(8).
               03  PURGE-FLAG-4006                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  LCTN-KEY-4006.
               03  UNVRS-CODE-4006                      PIC X(2).
               03  COA-CODE-4006                        PIC X(1).
               03  LCTN-CODE-4006                       PIC X(6).
           02  ACTVY-DATE-4006                COMP-3    PIC S9(8).
