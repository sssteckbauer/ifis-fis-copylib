      *01  BUDGET-MASTER-SORT-KEY.
           05  SRT-LOCATION-1              PIC  X(02).
           05  SRT-DIST-CODE               PIC  X(01).
           05  SRT-SAU                     PIC  X(01).
           05  SRT-SUB-CAMPUS-ID           PIC  X(01).
           05  SRT-FUND-TYPE               PIC  X(01).
           05  SRT-FUNCT-COLLEGE.
               10  SRT-FUNCTION-CD-1       PIC  X(02).
               10  SRT-COLLEGE-CD          PIC  X(02).
           05  SRT-RAFG REDEFINES SRT-FUNCT-COLLEGE.
               10  SRT-REV-ACC-FND-GRP     PIC  X(03).
               10  SRT-FILL-1              PIC  X(01).
           05  SRT-FUND-GROUP              PIC  X(02).
           05  SRT-ACCOUNT-NO-1            PIC  X(07).
           05  SRT-FND-1 REDEFINES SRT-ACCOUNT-NO-1.
               10  SRT-FUND-NO-1           PIC  X(06).
               10  SRT-FILL-2              PIC  X(01).
           05  SRT-SUB-BUDGET-CD-1         PIC  X(02).
           05  SRT-FUNCTION-CD-2 REDEFINES
               SRT-SUB-BUDGET-CD-1         PIC  X(02).
           05  SRT-ACCOUNT-NO-2            PIC  X(07).
           05  SRT-FND-2 REDEFINES SRT-ACCOUNT-NO-2.
               10  SRT-FUND-NO-2           PIC  X(06).
               10  SRT-FILL-3              PIC  X(01).
           05  SRT-SUB-BUDGET-CD-2         PIC  X(02).
           05  SRT-RECORD-TYPE             PIC  X(01).
           05  SRT-TRANS-CLASS             PIC  X(01).
           05  SRT-TRANS-DOC-NO            PIC  X(05).
           05  SRT-TRANS-DOC-SUF           PIC  X(02).
           05  SRT-LOCATION-2              PIC  X(02).
           05  FILLER                      PIC  X(04).
