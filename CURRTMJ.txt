      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4306-TIME-J' TO DBLINK-CURR-PARAGRAPH.       276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4306-TIME-J' TO RECORD-NAME.                          276 BRTN
YYY991     MOVE 'F-AST' TO AREA-NAME.                                   276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4306-TIME-J TO DBLINK-RECORD-MADE-CURRENT.      276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4306-TIME-J-EXIT                         276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE TMJ-FK-AST-TAG-NBR TO DBLINK-R4306-TIME-J-01        276 BRTN
YYY991         MOVE TMJ-START-DT TO DBLINK-R4306-TIME-J-02              276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4306-TIME-J TO DBLINK-VIEW-NAME-CURRENT.        276 BRTN
YYY991     MOVE DBLINK-R4306-TIME-J-KEY TO DBLINK-VIEW-200-CURRENT.     276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4151-4306                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-S-4151-4306-F               276 BRTN
YYY991     IF TMJ-IVD-SETF = DBLINK-MEMBER                              276 BRTN
YYY991         MOVE TMJ-IVD-SETF TO DBLINK-S-4151-4306-F                276 BRTN
YYY991         MOVE TMJ-FK-IVD-DOC-NBR TO DBLINK-S-4151-4306-01         276 BRTN
YYY991         MOVE TMJ-FK-IVD-ITEM-NBR TO DBLINK-S-4151-4306-02        276 BRTN
YYY991         MOVE TMJ-TAG-NBR TO DBLINK-S-4151-4306-03                276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4300-4306                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE TMJ-FK-AST-TAG-NBR TO DBLINK-S-4300-4306-01.            276 BRTN
YYY991     MOVE TMJ-START-DT TO DBLINK-S-4300-4306-02.                  276 BRTN
