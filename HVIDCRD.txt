       975-TO-R4170-CHK-RQST-D SECTION.
           MOVE CRD-USER-CD TO
               USER-CODE-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-UNVRS-CD TO
               UNVRS-CODE-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-DOC-NBR TO
               DCMNT-NMBR-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-ITEM-NBR TO
               ITEM-NMBR-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-SEQ-NBR TO
               SEQ-NMBR-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-COA-CD TO
               COA-CODE-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-ACCT-INDX-CD TO
               ACCT-INDX-CODE-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-FUND-CD TO
               FUND-CODE-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-ORGN-CD TO
               ORGZN-CODE-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-ACCT-CD TO
               ACCT-CODE-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-PRGRM-CD TO
               PRGRM-CODE-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-ACTVY-CD TO
               ACTVY-CODE-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-LCTN-CD TO
               LCTN-CODE-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-APRVD-AMT TO
               APRVD-AMT-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-DSCNT-AMT TO
               DSCNT-AMT-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-TAX-AMT TO
               TAX-AMT-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-ADDL-CHRG TO
               ADDL-CHRG-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-PAID-AMT TO
               PAID-AMT-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-FDRL-WTHHLD TO
               FDRL-WTHHLD-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-STATE-WTHHLD TO
               STATE-WTHHLD-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-PSTNG-PRD TO
               PSTNG-PRD-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-FSCL-YR TO
               FSCL-YR-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-RULE-CLS-CD TO
               RULE-CLASS-CODE-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-DSCNT-RULE-CLS TO
               DSCNT-RULE-CLASS-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-TAX-RULE-CLS TO
               TAX-RULE-CLASS-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-ADDL-CHRG-RUL-CLS TO
               ADDL-CHRG-RULE-CLASS-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-PO-NBR TO
               PO-NMBR-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-PO-ITEM-NBR TO
               PO-ITEM-NMBR-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-LIQDTN-IND TO
               LIQDTN-IND-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-PRJCT-CD TO
               PRJCT-CODE-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-DOC-TYP-SEQ-NBR TO
               DCMNT-TYPE-SEQ-NMBR-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-ADJMT-CD TO
               ADJMT-CODE-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-VNDR-INV-NBR TO
               VNDR-INV-NMBR-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-VNDR-INV-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               VNDR-INV-DATE-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-DOC-REF-NBR TO
               DCMNT-REF-NMBR-4170 OF R4170-CHK-RQST-D
                                                                     .
           MOVE CRD-TAX-RATE-CD TO
               TAX-RATE-CODE-4170 OF R4170-CHK-RQST-D
                                                                     .
       975-TO-R4170-CHK-RQST-D-EXIT.
           EXIT.
