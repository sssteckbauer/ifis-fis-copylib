      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FGAWK08                              04/24/00  12:31  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FGAWK08.
           02  COA-CODE-GA08                            PIC X(1).
           02  FSCL-CC-YR-GA08.
               03  FSCL-CC-GA08                         PIC X(2).
               03  FSCL-YR-GA08                         PIC X(2).
           02  ACTG-PRD-GA08                            PIC X(2).
           02  ACCT-INDX-CODE-GA08                      PIC X(10).
           02  ORGZN-CODE-GA08                          PIC X(6).
           02  FUND-CODE-GA08                           PIC X(6).
           02  ACCT-CODE-GA08                           PIC X(6).
           02  PRGRM-CODE-GA08                          PIC X(6).
           02  DSPLY-OPTN-GA08                          PIC X(1).
           02  DSPLY-DATA-CODE-1-GA08                   PIC X(6).
           02  DSPLY-TITLE-GA08             OCCURS 4    PIC X(5).
           02  PRD-START-DATE-4024-GA08                 PIC X(6).
           02  PRD-END-DATE-4024-GA08                   PIC X(6).
