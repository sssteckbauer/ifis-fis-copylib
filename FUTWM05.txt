      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FUTWM05                              04/24/00  13:23  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FUTWM05.
           02  FULL-NAME-6151-UT05                      PIC X(35).
           02  CITY-NAME-6152-UT05          OCCURS 10   PIC X(18).
           02  ACTN-CODE-GROUP-UT05.
               03  ACTN-CODE-UT05           OCCURS 10   PIC X(1).
           02  SYS-ADD-FLAG-6152-UT05       OCCURS 10   PIC X(1).
           02  FULL-NAME-6153-UT05                      PIC X(35).
           02  ZIP-CODE-6152-UT05           OCCURS 10   PIC X(10).
           02  REC-TRTRY-CODE-2389-UT05     OCCURS 10   PIC X(4).
