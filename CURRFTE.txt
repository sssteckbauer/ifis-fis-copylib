      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4009-FNDT-EFCTV' TO DBLINK-CURR-PARAGRAPH.   276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4009-FNDT-EFCTV' TO RECORD-NAME.                      276 BRTN
YYY991     MOVE 'F-FUND' TO AREA-NAME.                                  276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4009-FNDT-EFCTV TO DBLINK-RECORD-MADE-CURRENT.  276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4009-FNDT-EFCTV-EXIT                     276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE FTE-FK-FDT-FUND-TYPCD2 TO DBLINK-R4009-FNDT-EFCTV-01276 BRTN
YYY991         MOVE FTE-START-DT TO DBLINK-R4009-FNDT-EFCTV-02          276 BRTN
YYY991         MOVE FTE-TIME-STMP TO DBLINK-R4009-FNDT-EFCTV-03         276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4009-FNDT-EFCTV TO DBLINK-VIEW-NAME-CURRENT.    276 BRTN
YYY991     MOVE DBLINK-R4009-FNDT-EFCTV-KEY TO DBLINK-VIEW-200-CURRENT. 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4008-4009-O                                               276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-S-4008-4009-O-F             276 BRTN
YYY991     IF FTE-FDT-SET-TS1 = DBLINK-MEMBER                           276 BRTN
YYY991         MOVE FTE-FDT-SET-TS1 TO DBLINK-S-4008-4009-O-F           276 BRTN
YYY991         MOVE FTE-FK-FDT-FUND-TYPCD1 TO DBLINK-S-4008-4009-O-01   276 BRTN
YYY991         MOVE FTE-SBRDT-FUND-TYP TO DBLINK-S-4008-4009-O-02       276 BRTN
YYY991         MOVE FTE-START-DT TO DBLINK-S-4008-4009-O-03             276 BRTN
YYY991         MOVE FTE-TIME-STMP TO DBLINK-S-4008-4009-O-04            276 BRTN
YYY991         MOVE FTE-FDT-SET-TS2 TO DBLINK-S-4008-4009-O-05          276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4008-4009-P                                               276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE FTE-FK-FDT-FUND-TYPCD2 TO DBLINK-S-4008-4009-P-01.      276 BRTN
YYY991     MOVE FTE-START-DT TO DBLINK-S-4008-4009-P-02.                276 BRTN
YYY991     MOVE FTE-TIME-STMP TO DBLINK-S-4008-4009-P-03.               276 BRTN
