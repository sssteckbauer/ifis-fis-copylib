       975-TO-R4191-ENCMBR-HDR SECTION.
           MOVE ECH-USER-CD TO
               USER-CODE-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-UNVRS-CD TO
               UNVRS-CODE-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-ENCMBR-NBR TO
               ENCMBRNC-NMBR-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-DOC-TYP-SEQ-NBR TO
               DCMNT-TYPE-SEQ-NMBR-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACTVY-DATE-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-ENCMBR-DESC TO
               ENCMBRNC-DESC-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-DOC-REF-NBR TO
               DCMNT-REF-NMBR-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-ORGNL-ENCMBR-AMT TO
               ORGNL-ENCMBRNC-AMT-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-ENCMBR-TYP TO
               ENCMBRNC-TYPE-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-ENCMBR-STATUS TO
               ENCMBRNC-STATUS-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-CMTMNT-TYP TO
               CMTMNT-TYPE-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-CMTMNT-PCT TO
               CMTMNT-PCT-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-BDGT-DSPSN TO
               BDGT-DSPSN-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-ESTBLD-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ESTBLD-DATE-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-VNDR-ID-DGT-ONE TO
               VNDR-ID-DIGIT-ONE-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-VNDR-ID-LST-NINE TO
               VNDR-ID-LAST-NINE-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-PO-IND TO
               PO-IND-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-HDR-ERROR-IND TO
               HDR-ERROR-IND-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-HDR-CNTR-ACCT TO
               HDR-CNTR-ACCT-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-CMPLT-IND TO
               CMPLT-IND-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-TRANS-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               TRANS-DATE-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-PSTNG-IND TO
               PSTNG-IND-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-CHNG-SEQ-NBR TO
               CHNG-SEQ-NMBR-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-APRVL-IND TO
               APRVL-IND-4191 OF R4191-ENCMBR-HDR
                                                                     .
           MOVE ECH-LIEN-CLOSED-CD TO
               LIEN-CLOSED-CODE-4191 OF R4191-ENCMBR-HDR
                                                                     .
       975-TO-R4191-ENCMBR-HDR-EXIT.
           EXIT.
