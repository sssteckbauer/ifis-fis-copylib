      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R6139-PRSN-ADR' TO DBLINK-CURR-PARAGRAPH.     276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R6139-PRSN-ADR' TO RECORD-NAME.                        276 BRTN
YYY991     MOVE 'F-GEO' TO AREA-NAME.                                   276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6139-PRSN-ADR TO DBLINK-RECORD-MADE-CURRENT.    276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R6139-PRSN-ADR-EXIT                       276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE PRA-FK-PRS-IREF-ID TO DBLINK-R6139-PRSN-ADR-01      276 BRTN
YYY991         MOVE PRA-ADR-TYP-CD TO DBLINK-R6139-PRSN-ADR-02          276 BRTN
YYY991         MOVE PRA-END-DT TO DBLINK-R6139-PRSN-ADR-03              276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6139-PRSN-ADR TO DBLINK-VIEW-NAME-CURRENT.      276 BRTN
YYY991     MOVE DBLINK-R6139-PRSN-ADR-KEY TO DBLINK-VIEW-200-CURRENT.   276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-6117-6139                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE PRA-FK-PRS-IREF-ID TO DBLINK-S-6117-6139-01.            276 BRTN
YYY991     MOVE PRA-ADR-TYP-CD TO DBLINK-S-6117-6139-02.                276 BRTN
YYY991     MOVE PRA-END-DT TO DBLINK-S-6117-6139-03.                    276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-6152-6139                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-S-6152-6139-F               276 BRTN
YYY991     IF PRA-ZIP-SETF = DBLINK-MEMBER                              276 BRTN
YYY991         MOVE PRA-ZIP-SETF TO DBLINK-S-6152-6139-F                276 BRTN
YYY991         MOVE PRA-FK-ZIP-CNTRY-CD TO DBLINK-S-6152-6139-01        276 BRTN
YYY991         MOVE PRA-FK-ZIP-ZIP-CD TO DBLINK-S-6152-6139-02          276 BRTN
YYY991         MOVE PRA-ZIP-SET-TS TO DBLINK-S-6152-6139-03             276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-PRSN-ADR1                                            276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE PRA-LINE-ADR-1-UC TO DBLINK-S-INDX-PRSN-ADR1-01.        276 BRTN
YYY991     MOVE PRA-IDX-PRSN-ADR1-TS TO DBLINK-S-INDX-PRSN-ADR1-02.     276 BRTN
