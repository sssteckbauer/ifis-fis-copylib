    ***Created by Convert/DC version V8R03 on 11/01/00 at 10:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE SLCTN-STATUS-CH66 OF FCHWK66 TO WK01-STATUS-CH66.
      *%--------------------------------------------------------------%*
           MOVE SLCTN-DATE-CH66 OF FCHWK66 TO WK01-DATE-CH66.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH66 OF FCHWM66(0001) TO WK01-CODE-CH661.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH66 OF FCHWM66(0002) TO WK01-CODE-CH662.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH66 OF FCHWM66(0003) TO WK01-CODE-CH663.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH66 OF FCHWM66(0004) TO WK01-CODE-CH664.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH66 OF FCHWM66(0005) TO WK01-CODE-CH665.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH66 OF FCHWM66(0006) TO WK01-CODE-CH666.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH66 OF FCHWM66(0007) TO WK01-CODE-CH667.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH66 OF FCHWM66(0008) TO WK01-CODE-CH668.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH66 OF FCHWM66(0009) TO WK01-CODE-CH669.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH66 OF FCHWM66(0010) TO WK01-CODE-CH6610.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH66 OF FCHWM66(0011) TO WK01-CODE-CH6611.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-DESC-4063-CH66 OF FCHWM66(0001) TO
                WK01-MTHD-DESC-4063-CH661.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-DESC-4063-CH66 OF FCHWM66(0002) TO
                WK01-MTHD-DESC-4063-CH662.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-DESC-4063-CH66 OF FCHWM66(0003) TO
                WK01-MTHD-DESC-4063-CH663.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-DESC-4063-CH66 OF FCHWM66(0004) TO
                WK01-MTHD-DESC-4063-CH664.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-DESC-4063-CH66 OF FCHWM66(0005) TO
                WK01-MTHD-DESC-4063-CH665.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-DESC-4063-CH66 OF FCHWM66(0006) TO
                WK01-MTHD-DESC-4063-CH666.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-DESC-4063-CH66 OF FCHWM66(0007) TO
                WK01-MTHD-DESC-4063-CH667.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-DESC-4063-CH66 OF FCHWM66(0008) TO
                WK01-MTHD-DESC-4063-CH668.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-DESC-4063-CH66 OF FCHWM66(0009) TO
                WK01-MTHD-DESC-4063-CH669.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-DESC-4063-CH66 OF FCHWM66(0010) TO
                WK01-MTHD-DESC-4063-CH6610.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-DESC-4063-CH66 OF FCHWM66(0011) TO
                WK01-MTHD-DESC-4063-CH6611.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4063-CH66 OF FCHWM66(0001) TO WK01-4063-CH661.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4063-CH66 OF FCHWM66(0002) TO WK01-4063-CH662.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4063-CH66 OF FCHWM66(0003) TO WK01-4063-CH663.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4063-CH66 OF FCHWM66(0004) TO WK01-4063-CH664.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4063-CH66 OF FCHWM66(0005) TO WK01-4063-CH665.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4063-CH66 OF FCHWM66(0006) TO WK01-4063-CH666.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4063-CH66 OF FCHWM66(0007) TO WK01-4063-CH667.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4063-CH66 OF FCHWM66(0008) TO WK01-4063-CH668.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4063-CH66 OF FCHWM66(0009) TO WK01-4063-CH669.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4063-CH66 OF FCHWM66(0010) TO WK01-4063-CH6610.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4063-CH66 OF FCHWM66(0011) TO WK01-4063-CH6611.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4063-CH66 OF FCHWM66(0001) TO
                WK01-DATE-4063-CH661.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4063-CH66 OF FCHWM66(0002) TO
                WK01-DATE-4063-CH662.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4063-CH66 OF FCHWM66(0003) TO
                WK01-DATE-4063-CH663.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4063-CH66 OF FCHWM66(0004) TO
                WK01-DATE-4063-CH664.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4063-CH66 OF FCHWM66(0005) TO
                WK01-DATE-4063-CH665.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4063-CH66 OF FCHWM66(0006) TO
                WK01-DATE-4063-CH666.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4063-CH66 OF FCHWM66(0007) TO
                WK01-DATE-4063-CH667.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4063-CH66 OF FCHWM66(0008) TO
                WK01-DATE-4063-CH668.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4063-CH66 OF FCHWM66(0009) TO
                WK01-DATE-4063-CH669.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4063-CH66 OF FCHWM66(0010) TO
                WK01-DATE-4063-CH6610.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4063-CH66 OF FCHWM66(0011) TO
                WK01-DATE-4063-CH6611.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4063-CH66 OF FCHWM66(0001) TO
                WK01-DATE-4063-CH6A1.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4063-CH66 OF FCHWM66(0002) TO
                WK01-DATE-4063-CH6A2.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4063-CH66 OF FCHWM66(0003) TO
                WK01-DATE-4063-CH6A3.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4063-CH66 OF FCHWM66(0004) TO
                WK01-DATE-4063-CH6A4.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4063-CH66 OF FCHWM66(0005) TO
                WK01-DATE-4063-CH6A5.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4063-CH66 OF FCHWM66(0006) TO
                WK01-DATE-4063-CH6A6.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4063-CH66 OF FCHWM66(0007) TO
                WK01-DATE-4063-CH6A7.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4063-CH66 OF FCHWM66(0008) TO
                WK01-DATE-4063-CH6A8.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4063-CH66 OF FCHWM66(0009) TO
                WK01-DATE-4063-CH6A9.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4063-CH66 OF FCHWM66(0010) TO
                WK01-DATE-4063-CH6A10.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4063-CH66 OF FCHWM66(0011) TO
                WK01-DATE-4063-CH6A11.
      *%--------------------------------------------------------------%*
           MOVE RQST-PAY-MTHD-CODE-CH66 OF FCHWK66 TO
                WK01-PAY-MTHD-CODE-CH66.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-CODE-4035-CH66 OF FCHWM66(0001) TO
                WK01-MTHD-CODE-4035-CH661.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-CODE-4035-CH66 OF FCHWM66(0002) TO
                WK01-MTHD-CODE-4035-CH662.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-CODE-4035-CH66 OF FCHWM66(0003) TO
                WK01-MTHD-CODE-4035-CH663.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-CODE-4035-CH66 OF FCHWM66(0004) TO
                WK01-MTHD-CODE-4035-CH664.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-CODE-4035-CH66 OF FCHWM66(0005) TO
                WK01-MTHD-CODE-4035-CH665.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-CODE-4035-CH66 OF FCHWM66(0006) TO
                WK01-MTHD-CODE-4035-CH666.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-CODE-4035-CH66 OF FCHWM66(0007) TO
                WK01-MTHD-CODE-4035-CH667.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-CODE-4035-CH66 OF FCHWM66(0008) TO
                WK01-MTHD-CODE-4035-CH668.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-CODE-4035-CH66 OF FCHWM66(0009) TO
                WK01-MTHD-CODE-4035-CH669.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-CODE-4035-CH66 OF FCHWM66(0010) TO
                WK01-MTHD-CODE-4035-CH6610.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-CODE-4035-CH66 OF FCHWM66(0011) TO
                WK01-MTHD-CODE-4035-CH6611.
