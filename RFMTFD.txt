      ******************************************************************00010000
      ** !!!WARNING!!! ANY CHANGES MADE TO THIS COPYBOOK MEMBER       **00020000
      **               (RFMTFD) MUST ALSO BE MADE TO RFMTREC          **00030000
      ******************************************************************00040000
      ******************************************************************00050000
      *                                                                 00060000
      *01   REFORMATED-JRNL-IMAGE-PLACE.                                00070000
      *  05 RFD-REC-LEN                          PIC 9(04) COMP.        00080000
      *  05 RFD-2ND-PART-OF-VAR-LEN-STUF         PIC 9(04) COMP.        00090000
      * ABOVE TWO FIELDS ARE NOT SEEN BY COBOL WHEN READING THE         00100000
      * VARIABLE-LENGTH TRACER PRODUCED "DBTRFMTJ" FILE.                00110000
      *  02 RFD-DBTRACE-REBUILT-JRNL-REC.                               00120000
      ******************************************************************00130000
         05 RFD-FIELDS-FROM-JRNL-REC.                                   00140000
           10 RFD-TYPE                           PIC X(04).             00150000
               88 RFD-DETAIL                             VALUES 'BFOR'  00160000
                                                                'AFTR'. 00170000
           10 RFD-SEQ                            PIC 9(08) COMP.        00180000
           10 RFD-RUN-UNIT-ID                    PIC 9(08) COMP.        00190000
           10 RFD-AREA-TYPE                      PIC X(01).             00200000
           10 RFD-VERB-NUM                       PIC X(01).             00210000
           10 RFD-PAGE-GROUP                     PIC 9(04) COMP.        00220000
           10 RFD-DBK-FORMAT                     PIC 9(08) COMP.        00230000
           10 RFD-DBK-LAYOUT REDEFINES RFD-DBK-FORMAT.                  00240000
              15 RFD-DBK-PAGE-NBR-BITS           PIC X(03).             00250000
              15 RFD-DBK-LINE-NBR-BITS           PIC X(01).             00260000
           10 RFD-DB-KEY                         PIC 9(08) COMP.        00270000
           10 RFD-USER-REC-ID                    PIC 9(04) COMP.        00280000
           10 RFD-PG-DISPL                       PIC 9(04) COMP.        00290000
           10 RFD-IMAGE-LEN                      PIC 9(04) COMP.        00300000
           10 RFD-PREFIX-LEN                     PIC 9(04) COMP.        00310000
           10 RFD-DISPLACEMENT                   PIC 9(04) COMP.        00320000
           10 RFD-SPAN-OFFSET                    PIC 9(04) COMP.        00330000
         05 RFD-FIELDS-BUILT-BY-DBTRACE.                                00340000
           10 RFD-TASK-ID-NUMBER                 PIC 9(08) COMP.        00350000
           10 RFD-VIASET-OWNER-DBKEY             PIC 9(08) COMP.        00360000
           10 RFD-VIASET-NEXT-DBKEY              PIC 9(08) COMP.        00370000
      ******************************************************************00380000
      *-B- R23                                                          00390000
      ***  10 RFD-RECORDS-KEYLENGTH              PIC 9(08) COMP.        00400000
      ******************************************************************00410000
           10 RFD-RECORDS-KEYLENGTH              PIC 9(04) COMP.        00420000
           10 RFD-NBR-RECS-PRC-EARLIER-RUNU      PIC 9(04) COMP.        00430000
      ******************************************************************00440000
      *-E- R23                                                          00450000
      ******************************************************************00460000
           10 RFD-STORAGE-TYPE                   PIC X(01).             00470000
               88 RFD-THIS-VIA-RECORD                    VALUE 'V'.     00480000
               88 RFD-THIS-CALC-RECORD                   VALUE 'C'.     00490000
               88 RFD-THIS-DIRECT-RECORD                 VALUE 'D'.     00500000
           10 RFD-RUN-UNIT-START-DATE.                                  00510000
               15  RFD-RUN-UNIT-START-MONTH      PIC 9(02).             00520000
               15  RFD-RUN-UNIT-SLASH-1          PIC X(01).             00530000
               15  RFD-RUN-UNIT-START-DAY        PIC 9(02).             00540000
               15  RFD-RUN-UNIT-SLASH-2          PIC X(01).             00550000
               15  RFD-RUN-UNIT-START-YEAR       PIC 9(02).             00560000
           10 RFD-RUN-UNIT-START-TIME-NBR        PIC 9(08).             00570000
           10 RFD-RUN-UNIT-START-TIME-GRP REDEFINES                     00580000
              RFD-RUN-UNIT-START-TIME-NBR.                              00590000
               15  RFD-RUN-UNIT-START-HOUR       PIC 9(02).             00600000
               15  RFD-RUN-UNIT-START-MINUTE     PIC 9(02).             00610000
               15  RFD-RUN-UNIT-START-SECOND     PIC 9(02).             00620000
               15  RFD-RUN-UNIT-START-HUNDR-SEC  PIC 9(02).             00630000
           10 RFD-RUN-UNIT-PROGRAM-NAME          PIC X(08).             00640000
           10 RFD-RECORD-NAME                    PIC X(16).             00650000
      ******************************************************************00660000
      *-B- ADDITIONAL INFORMATION ABOUT POINTER UPDATES                 00670000
      * THE 'RFD-CHANGED-SET-NAME' IS USED ON POINTER-ONLY UPDATES TO   00680000
      * TELL THE NAME OF THE SET ASSOCIATED WITH THE CHANGED POINTERS   00690000
      * WHEN IT IS INCLUDED IN THE 'SEL-SUBSCHEMA-NAME' LOAD MODULE.    00700000
      * I.E., WHEN 'RFD-DATA-OR-POINTERS-ONLY' IS SET TO 'P' THEN THE   00710000
      * RFD-CHANGED-SET-NAME' IS USED - OTHERWISE THE 'RFD-VIASET-NAME' 00720000
      * IS ONLY FILLED IN WHEN THE UPDATED RECORD IS STORED VIA A SET   00730000
      * INCLUDED IN THE 'SEL-SUBSCHEMA-NAME'.                           00740000
      ******************************************************************00750000
           10 RFD-VIASET-NAME                    PIC X(16).             00760000
           10 RFD-CHANGED-SET-NAME REDEFINES RFD-VIASET-NAME            00770000
                                                 PIC X(16).             00780000
      ******************************************************************00790000
      *-E- DLW                                                          00800000
      ******************************************************************00810000
           10 RFD-VIASET-OWNER-RECORD-NAME       PIC X(16).             00820000
      ******************************************************************00830000
      *-B- DLW                                                          00840000
      *    THE 'RFD-CHANGED-SET-OWNER-NAME' IS USED ON POINTER-ONLY     00850000
      *    UPDATES TO TELL THE NAME OF THE OWNER OF THE SET WHOSE       00860000
      *    POINTER(S) WAS UPDATED.                                      00870000
      ******************************************************************00880000
           10 RFD-CHANGED-SET-OWNER-NAME REDEFINES                      00890000
                  RFD-VIASET-OWNER-RECORD-NAME   PIC X(16).             00900000
      ******************************************************************00910000
      *-E- DLW                                                          00920000
      ******************************************************************00930000
           10 RFD-SCHEMA-NAME                    PIC X(08).             00940000
      ******************************************************************00950000
      *    10 RFD-FILLER-ALIGN-2                 PIC X(01).             00960000
      ******************************************************************00970000
           10 RFD-WHICH-POINTERS-SET-HAS         PIC X(01).             00980000
           10 RFD-SCHEMA-VERSION-NUMBER          PIC 9(04)  COMP.       00990000
           10 RFD-YN-MODIFY-CHANGED-OR-NOT       PIC X(01).             01000000
               88 RFD-RECORD-WAS-CHANGED                 VALUE 'Y'.     01010000
               88 RFD-RECORD-NOT-CHANGED                 VALUE 'N'.     01020000
           10 RFD-BEFORE-IMAGE-PRESENT-IND       PIC X(04).             01030000
               88 RFD-BEFORE-IMAGE-HERE                  VALUE 'BFOR'.  01040000
           10 RFD-AFTER-IMAGE-PRESENT-IND        PIC X(04).             01050000
               88 RFD-AFTER-IMAGE-HERE                   VALUE 'AFTR'.  01060000
           10 RFD-DATA-OR-POINTERS-ONLY          PIC X(01).             01070000
               88 RFD-THIS-CHANGE-ONLY-PTRS              VALUE 'P'.     01080000
               88 RFD-THIS-CHANGED-DATA                  VALUE 'D'.     01090000
           10 RFD-VERB-IN-CHARACTERS.                                   01100000
               15  RFD-VERB-NAME-1ST-PART        PIC X(05).             01110000
               88 RFD-ANY-ERASE-TYPE-VERB                VALUE 'ERASE'. 01120000
               88 RFD-MODIFY-VERB                        VALUE 'MODIF'. 01130000
               88 RFD-STORE-VERB                         VALUE 'STORE'. 01140000
               88 RFD-CONNECT-VERB                       VALUE 'CONNE'. 01150000
               88 RFD-DISCONNECT-VERB                    VALUE 'DISCO'. 01160000
      ******************************************************************01170000
      *-B- DLW BELOW DISTINGUISH THE KIND OF ERASE VERB ISSUED          01180000
      ******************************************************************01190000
               15  RFD-VERB-NAME-2ND-PART        PIC X(03).             01200000
               88 RFD-ERASE-SELECTIVE-VERB               VALUE 'SEL'.   01210000
               88 RFD-ERASE-PERMANENT-VERB               VALUE 'PRM'.   01220000
               88 RFD-ERASE-ALL-VERB                     VALUE 'ALL'.   01230000
      ******************************************************************01240000
      *-E- DLW                                                          01250000
      ******************************************************************01260000
           10 RFD-ENVIRONMENT                    PIC X(04).             01270000
               88 RFD-ONLINE-RUN-UNIT                    VALUE 'DBDC'.  01280000
               88 RFD-BATCH-RUN-UNIT                     VALUE 'BATC'.  01290000
               88 RFD-CICS-RUN-UNIT                      VALUE 'CICS'.  01300000
      ******************************************************************01310000
      *    10 RFD-FILLER-1                       PIC X(01).             01320000
      ******************************************************************01330000
           10 RFD-YN-THIS-TARGET-OF-DML-VERB     PIC X(01).             01340000
           10 RFD-TASK-ORIENTED-FIELDS.                                 01350000
               15  RFD-TASK-NAME                 PIC X(08).             01360000
               15  RFD-TERMINAL                  PIC X(08).             01370000
               15  RFD-USERID-LEN                PIC 9(04) COMP.        01380000
      ******************************************************************01390000
               15  RFD-USERID                    PIC X(32).             01400000
               15  RFD-USERID-REDEF-LO1 REDEFINES RFD-USERID.           01410000
                 20  RFD-USERID-8-BYTES          PIC X(08).             01420000
                 20  RFD-USERID-FLD-PROGRAM      PIC X(08).             01430000
                 20  RFD-SSC-COMP-DATE.                                 01440000
                    25  RFD-SSC-COMP-YEAR        PIC 9(02).             01450000
                    25  FILLER                   PIC X(01).             01460000
                    25  RFD-SSC-COMP-MONTH       PIC 9(02).             01470000
                    25  FILLER                   PIC X(01).             01480000
                    25  RFD-SSC-COMP-DAY         PIC 9(02).             01490000
                 20  RFD-DB-SEGMENT-IDMS-RLSE-12 PIC X(08).             01500000
               15  RFD-USERID-REDEF-LO2 REDEFINES RFD-USERID.           01510000
                 20  RFD-USERID-1ST-5-BYTES      PIC X(05).             01520000
                 20  RFD-USERID-OTHER-FIELDS     PIC X(27).             01530000
      ******************************************************************01540000
               15  RFD-SUBSCHEMA                 PIC X(08).             01550000
      ******************************************************************01560000
      *-B- DLW 05/27/93 ADDITIONAL INFORMATION ABOUT POINTER UPDATES    01570000
      *    10  RFD-FILLER-2                      PIC X(01).             01580000
      ******************************************************************01590000
           10  RFD-OM-REC-ROLE-IN-CHNGD-SET      PIC X(01).             01600000
           10  RFD-SORT-ORDER-OF-CHNGD-SET       PIC X(01).             01610000
           10  RFD-WHO-TO-KEY-ON-AT-INSERT       PIC X(01).             01620000
           10  RFD-IC-INDEX-OR-CHAINED-SET       PIC X(02).             01630000
           10  RFD-MEMBERSHIP-TYPE-IN-C-SET      PIC X(02).             01640000
           10  RFD-NBR-DML-VERB-THIS-RUNU        PIC 9(04) COMP.        01650000
      ******************************************************************01660000
      *        15  RFD-FILLER-2                  PIC X(09).             01670000
      *-E- DLW 05/27/93                                                 01680000
      ******************************************************************01690000
           10 RFD-1ST-DIFF-OFFSET                PIC 9(04) COMP.        01700000
      ******************************************************************01710000
      *-B- DLW 06/16/93                                                 01720000
      ***  10 RFD-RECORD-DATA-LENGTH             PIC 9(04) COMP.        01730000
      ******************************************************************01740000
           10 RFD-SET-PNTR-OFFSET-NEXT           PIC X(01).             01750000
           10 RFD-SET-PNTR-OFFSET-PRIOR          PIC X(01).             01760000
      ******************************************************************01770000
      *-E- DLW 06/16/93                                                 01780000
      *-B- DLW 06/15/93                                                 01790000
      ***  10 RFD-MAX-POINTER-LENGTH             PIC 9(04) COMP.        01800000
      ******************************************************************01810000
           10 RFD-NBR-NON-SYS-RECS-PRCSD         PIC 9(04) COMP.        01820000
      ******************************************************************01830000
      *-E- DLW 06/15/93                                                 01840000
      ******************************************************************01850000
           10 RFD-PAGE-NUMBER-MATRIX             PIC 9(08) COMP.        01860000
           10 RFD-LINE-INDEX-MATRIX              PIC 9(04) COMP.        01870000
           10 RFD-DDNAME-WHERE-REC-STORED        PIC X(08).             01880000
           10 RFD-CLOSEST-UPDATE-TIME.                                  01890000
               15  RFD-CLOSEST-HOUR              PIC X(02).             01900000
               15  RFD-CLOSEST-MINUTE            PIC X(02).             01910000
               15  RFD-CLOSEST-SECOND            PIC X(02).             01920000
               15  RFD-CLOSEST-HUNDRETH-SECOND   PIC X(02).             01930000
               15  RFD-CLOSEST-THOUSANDTH-SECOND PIC X(02).             01940000
      ******************************************************************01950000
      *-B- DLW 06/16/93                                                 01960000
      ***  10 RFD-POINTER-LENGTH                 PIC 9(04) COMP.        01970000
      ******************************************************************01980000
           10 RFD-SET-PNTR-OFFSET-OWNER          PIC X(01).             01990000
           10 RFD-FILLER-1-BYTE                  PIC X(01).             02000000
      ******************************************************************02010000
      *-E- DLW 06/16/93                                                 02020000
      ******************************************************************02030000
           10 RFD-RFMT-RECORD-LENGTH             PIC 9(04) COMP.        02040000
           10 RFD-ROLLOVER-SEQUENCE-NBR          PIC 9(04) COMP.        02050000
      ******************************************************************02060000
      *-B- DLW 05/27/93 ADDITIONAL INFORMATION ABOUT POINTER UPDATES    02070000
      ******************************************************************02080000
           10 RFD-NEXT-PRIOR-OWN-ESTAB-CRNCY     PIC X(01).             02090000
           10 RFD-NPO-TYPE-OF-PNTR-CHANGED       PIC X(01).             02100000
           10 RFD-REESTABLISH-CRNCY-DBKEY        PIC 9(08) COMP.        02110000
           10 RFD-NEXT-POINTER-CHANGED           PIC 9(08) COMP.        02120000
           10 RFD-PRIOR-POINTER-CHANGED          PIC 9(08) COMP.        02130000
           10 RFD-OWNER-POINTER-CHANGED          PIC 9(08) COMP.        02140000
           10 RFD-ALT-IMAGE-NON-TARG-PTR-CHG     PIC 9(08) COMP.        02150000
           10 RFD-INDEX-BLOCK-COUNT              PIC 9(04) COMP.        02160000
           10 RFD-AUTOMATIC-DML-VERB             PIC X(01).             02170000
           10 RFD-B4-OR-AF-NPO-PTRS              PIC X(01).             02180000
      ******************************************************************02190000
      *    10 RFD-FILLER-3                       PIC X(26).             02200000
      *-E- DLW 05/27/93                                                 02210000
      ******************************************************************02220000
           10 RFD-LENGTH-OF-BFOR-PTRS-IMAGE      PIC 9(04) COMP.        02230000
           10 RFD-LENGTH-OF-AFTR-PTRS-IMAGE      PIC 9(04) COMP.        02240000
           10 RFD-LENGTH-OF-BFOR-DATA-IMAGE      PIC 9(04) COMP.        02250000
           10 RFD-LENGTH-OF-AFTR-DATA-IMAGE      PIC 9(04) COMP.        02260000
           10 RFD-BFOR-POINTERS-AREA-GRP.                               02270000
              15 RFD-BFOR-POINTERS-AREA          PIC X(01)              02280000
                                                      OCCURS 0 TO 1000  02290000
                 DEPENDING ON RFD-LENGTH-OF-BFOR-PTRS-IMAGE.            02300000
           10 RFD-AFTR-POINTERS-AREA-GRP.                               02310000
              15 RFD-AFTR-POINTERS-AREA          PIC X(01)              02320000
                                                      OCCURS 0 TO 1000  02330000
                 DEPENDING ON RFD-LENGTH-OF-AFTR-PTRS-IMAGE.            02340000
           10 RFD-BFOR-DATA-AREA-GRP.                                   02350000
              15 RFD-BFOR-DATA-AREA             PIC X(01)               02360000
                                                     OCCURS 0 TO 10000  02370000
                 DEPENDING ON RFD-LENGTH-OF-BFOR-DATA-IMAGE.            02380000
           10 RFD-AFTR-DATA-AREA-GRP.                                   02390000
              15 RFD-AFTR-DATA-AREA             PIC X(01)               02400000
                                                     OCCURS 0 TO 10000  02410000
                 DEPENDING ON RFD-LENGTH-OF-AFTR-DATA-IMAGE.            02420000
