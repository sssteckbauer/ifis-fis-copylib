    ***Created by Convert/DC version V8R03 on 11/01/00 at 08:00***

      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH00 OF FCHWM00 TO WK01-CODE-CH00.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4067-CH00 OF FCHWM00 TO WK01-DATE-4067-CH00.
      *%--------------------------------------------------------------%*
           MOVE TIME-STAMP-4067-CH00 OF FCHWM00 TO WK01-STAMP-4067-CH00.
      *%--------------------------------------------------------------%*
           MOVE FUND-TITLE-4005-CH00 OF FCHWM00 TO WK01-TITLE-4005-CH00.
      *%--------------------------------------------------------------%*
           MOVE ORGZN-TITLE-4010-CH00 OF FCHWM00 TO
                WK01-TITLE-4010-CH00.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-CH00 OF FCHWM00 TO
                WK01-CODE-TITLE-4034-CH00.
      *%--------------------------------------------------------------%*
           MOVE ACCT-INDX-TITLE-4067-CH00 OF FCHWM00 TO
                WK01-INDX-TITLE-4067-CH00.
      *%--------------------------------------------------------------%*
           MOVE PRGRM-TITLE-4105-CH00 OF FCHWM00 TO
                WK01-TITLE-4105-CH00.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-TITLE-4066-CH00 OF FCHWM00 TO
                WK01-TITLE-4066-CH00.
      *%--------------------------------------------------------------%*
           MOVE FUND-CODE-4067-CH00 OF FCHWM00 TO WK01-CODE-4067-CH00.
      *%--------------------------------------------------------------%*
           MOVE ORGZN-CODE-4067-CH00 OF FCHWM00 TO WK01-CODE-4067-CH0A.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-4067-CH00 OF FCHWM00 TO WK01-CODE-4067-CH0B.
      *%--------------------------------------------------------------%*
           MOVE PRGRM-CODE-4067-CH00 OF FCHWM00 TO WK01-CODE-4067-CH0C.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-CODE-4067-CH00 OF FCHWM00 TO WK01-CODE-4067-CH0D.
      *%--------------------------------------------------------------%*
           MOVE LCTN-CODE-4067-CH00 OF FCHWM00 TO WK01-CODE-4067-CH0E.
      *%--------------------------------------------------------------%*
           MOVE NXT-CHANGE-DATE-CH00 OF FCHWM00 TO
                WK01-CHANGE-DATE-CH00.
      *%--------------------------------------------------------------%*
           MOVE LAST-ACTVY-DATE-4045-CH00 OF FCHWM00 TO
                WK01-ACTVY-DATE-4045-CH00.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4045-CH00 OF FCHWK00 TO WK01-CODE-4045-CH00.
      *%--------------------------------------------------------------%*
           MOVE ACCT-INDX-CODE-4045-CH00 OF FCHWK00 TO
                WK01-INDX-CODE-4045-CH00.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4067-CH00 OF FCHWK00 TO WK01-DATE-4067-CH0A.
      *%--------------------------------------------------------------%*
           MOVE STATUS-DESC-CH00 OF FCHWM00 TO WK01-DESC-CH00.
      *%--------------------------------------------------------------%*
           MOVE FUND-OVRDE-4067-CH00 OF FCHWM00 TO WK01-OVRDE-4067-CH00.
      *%--------------------------------------------------------------%*
           MOVE ORGZN-OVRDE-4067-CH00 OF FCHWM00 TO
                WK01-OVRDE-4067-CH0A.
      *%--------------------------------------------------------------%*
           MOVE ACCT-OVRDE-4067-CH00 OF FCHWM00 TO WK01-OVRDE-4067-CH0B.
      *%--------------------------------------------------------------%*
           MOVE PRGRM-OVRDE-4067-CH00 OF FCHWM00 TO
                WK01-OVRDE-4067-CH0C.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-OVRDE-4067-CH00 OF FCHWM00 TO
                WK01-OVRDE-4067-CH0D.
      *%--------------------------------------------------------------%*
           MOVE LCTN-OVRDE-4067-CH00 OF FCHWM00 TO WK01-OVRDE-4067-CH0E.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-CH00 OF FCHWM00 TO WK01-PM-FLAG-CH00.
      *%--------------------------------------------------------------%*
           MOVE LCTN-TITLE-4007-CH00 OF FCHWM00 TO WK01-TITLE-4007-CH00.
      *%--------------------------------------------------------------%*
           MOVE EARLY-INACTV-DATE-4067-CH00 OF FCHWM00 TO
                WK01-INACTV-DATE-4067-CH00.
      *%--------------------------------------------------------------%*
           MOVE EMAIL-MSG-LIT-CH00 OF FCHWM00 TO WK01-LIT-CH00.
      *%--------------------------------------------------------------%*
           MOVE EMAIL-MSG-IND-CH00 OF FCHWM00 TO WK01-IND-CH00.
      *%--------------------------------------------------------------%*
           MOVE EMAIL-MSG-LIT2-CH00 OF FCHWM00 TO WK01-LIT2-CH00.
      *%--------------------------------------------------------------%*
           MOVE EMAIL-MSG-ADDR-CH00 OF FCHWM00 TO WK01-ADDR-CH00.
      *%--------------------------------------------------------------%*
           MOVE USER-CD-4067-CH00 OF FCHWM00 TO WK01-USER-4067-CH00.
      *%--------------------------------------------------------------%*
