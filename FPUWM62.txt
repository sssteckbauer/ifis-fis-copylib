      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWM62                              04/24/00  12:40  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWM62.
           02  ACTN-CODE-PU62                           PIC X(1).
           02  SEQ-NMBR-4058-PU62                       PIC X(4).
           02  ACTVY-DATE-4058-PU62                     PIC X(6).
           02  AS-OF-DATE-4058-PU62                     PIC X(6).
           02  DLT-IND-4058-PU62                        PIC X(1).
           02  HOLD-IND-4058-PU62                       PIC X(1).
           02  PO-NMBR-4058-PU62                        PIC X(8).
           02  CMDTY-CODE-4058-PU62                     PIC X(8).
           02  ORGZN-CODE-4058-PU62                     PIC X(6).
           02  RQST-CODE-4058-PU62                      PIC X(8).
           02  VNDR-TYPE-CODE-4058-PU62     OCCURS 10   PIC X(2).
           02  VNDR-CODE-PU62.
               03  VNDR-ID-DIGIT-ONE-PU62               PIC X(1).
               03  VNDR-ID-LAST-NINE-PU62               PIC X(9).
           02  BID-NMBR-4058-PU62                       PIC X(8).
           02  COA-CODE-4058-PU62                       PIC X(1).
           02  CHNG-SEQ-NMBR-4058-PU62                  PIC X(3).
           02  RPRT-TITLE-4060-PU62                     PIC X(40).
           02  START-DATE-PU62                          PIC X(6).
