      ******************************************************************CPWSXBTB
      * 001AT  EXPAND TABLE SIZE LIMIT                                  CPWSXBTB
      ******************************************************************CPWSXBTB
      *01  BUDGET-TABLES-RECORD.                                        CPWSXBTB
           05  TAB-TABLE-NO            PIC X(03).                       CPWSXBTB
           05  FILLER                  PIC X(01).                       CPWSXBTB
           05  TAB-RANGE-FROM          PIC X(10).                       CPWSXBTB
           05  TAB-RANGE-TO            PIC X(10).                       CPWSXBTB
           05  TAB-CLASS-CODE          PIC X(06).                       CPWSXBTB
           05  TAB-CLASS-TITLE         PIC X(30).                       CPWSXBTB
           05  FILLER                  PIC X(20).                       CPWSXBTB
       01  BUDGET-TABLES.                                               CPWSXBTB
001AT      05  BUDGET-TABLE         OCCURS 2500 INDEXED BY TAB-INDX     CPWSXBTB
                                                           TAB-END-INDX.CPWSXBTB
               10  TABLE-NO                PIC  X(03).                  CPWSXBTB
               10  FILLER                  PIC  X(01).                  CPWSXBTB
               10  TABLE-RANGE-FROM        PIC  X(10).                  CPWSXBTB
               10  TABLE-RANGE-TO          PIC  X(10).                  CPWSXBTB
               10  TABLE-CLASS-CODE        PIC  X(06).                  CPWSXBTB
               10  TABLE-CLASS-TITLE       PIC  X(30).                  CPWSXBTB
001AT      05  TABLE-SIZE                  PIC S9(04) COMP.             CPWSXBTB
001AT          88 TABLE-SIZE-OVERFLOW                   VALUE +2501.    CPWSXBTB
                                                                        CPWSXBTB
           05  BUDGET-TABLE-INDEXES OCCURS 10 INDEXED BY TAB-NO-INDX.   CPWSXBTB
               10  INDX-TABLE-NO           PIC  X(03).                  CPWSXBTB
001AT          10  INDX-TABLE-BEG          PIC S9(04) COMP.             CPWSXBTB
001AT          10  INDX-TABLE-END          PIC S9(04) COMP.             CPWSXBTB
           05  TABLE-NO-ARG                PIC  X(03).                  CPWSXBTB
           05  TABLE-ENTRY-ARG             PIC  X(10).                  CPWSXBTB
           05  TABLE-SEARCH-TYPE-ARG       PIC  X(05).                  CPWSXBTB
           05  END-OF-TABLE-FLAG           PIC  X(01).                  CPWSXBTB
               88  END-OF-TABLE                         VALUE 'Y'.      CPWSXBTB
           05  BUDGET-TABLES-FILE-STATUS-FLAG PIC X(01) VALUE 'B'.      CPWSXBTB
               88  BUDGET-TABLES-BEGIN                  VALUE 'B'.      CPWSXBTB
               88  BUDGET-TABLES-READ                   VALUE 'R'.      CPWSXBTB
               88  BUDGET-TABLES-EOF                    VALUE 'E'.      CPWSXBTB
           05  TABLE-ENTRY-FOUND-FLAG      PIC  X(01)   VALUE 'N'.      CPWSXBTB
               88  TABLE-ENTRY-FOUND                    VALUE 'Y'.      CPWSXBTB
               88  TABLE-ENTRY-NOT-FOUND                VALUE 'N'.      CPWSXBTB
           05  TABLE-LOAD-ERROR-FLAG       PIC  X(01)   VALUE 'N'.      CPWSXBTB
               88  TABLE-LOAD-OK                        VALUE 'N'.      CPWSXBTB
               88  TABLE-LOAD-ERROR                     VALUE 'Y'.      CPWSXBTB
           05  TABLE-SEARCH-ERROR-FLAG     PIC  X(01)   VALUE 'N'.      CPWSXBTB
               88  TABLE-SEARCH-OK                      VALUE 'N'.      CPWSXBTB
               88  TABLE-SEARCH-ERROR                   VALUE 'Y'.      CPWSXBTB
