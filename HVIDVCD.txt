       975-TO-R4104-VENDOR-CMD SECTION.
           MOVE VCD-USER-CD TO
               USER-CODE-4104 OF R4104-VENDOR-CMD
                                                                     .
           MOVE VCD-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4104 OF R4104-VENDOR-CMD
                                                                     .
           MOVE VCD-CMDTY-CD TO
               CMDTY-CODE-4104 OF R4104-VENDOR-CMD
                                                                     .
       975-TO-R4104-VENDOR-CMD-EXIT.
           EXIT.
