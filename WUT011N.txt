      ******************************************************************
      *    COPYBOOK WUT011N - USED FOR WEB3270 NAVIGATION SERVICES     *
      *             --------------------------------------             *
      *             WEB SERVICE NAME = Coeus4FundSetupIFIS             *
      *                                                                *
      *  NOTE: THIS COPYBOOK MUST CONFORM TO THE FORMAT OF WUT010N.    *
      *        ONLY THE CA-SOAP-RESP-AREA IS FURTHER DEFINED.          *
      ******************************************************************
      * CHANGE LOG:                                                    *
FP6406*    MODIFIED BY ..... DEVMR0                                    *
FP6406*    DATE MODIFIED ... 10/20/14                                  *
FP6406*    MODIFICATION .... CHANGED ARRAY SIZE FROM 50 TO 99 ENTRIES  *
FP6406*                      CA-INVLACCT-ARRAY, PIC X(7) TIMES N.      *
      *                                                                *
HVW001*    MODIFIED BY ..... H.VANDERWEIT, JIRA#CARCT-350              *
HVW001*    DATE MODIFIED ... 12/08/17                                  *
HVW001*    MODIFICATION .... ADDED AWARD-TYPE AND TRFRDETL FIELDS FOR  *
HVW001*                      ACCT-DIST-INDEX, ACCT-DIST-ACCT,          *
HVW001*                      ACCT-DIST-PROGRAM, AND ACCT-RECV-ACCT.    *
HVW001*                      CHANGED ARRAY SIZE FROM 99 TO 96 ENTRIES  *
      *                                                                *
      ******************************************************************

       01  WEB3270N-COMMAREA.

           05  CA-CHANNEL-ID           PIC X(08).
      *        *********************************************************
      *        * MUST BE 'WEB3270N '                                   *
      *        *********************************************************

           05  CA-VERS-RESP-CD         PIC X(08).
      *        *********************************************************
      *        * INCOMING: COMMAREA VERSION = 01.00.00                 *
      *        * OUTGOING: RETURN RESPONSE FOR WWUT (WEB UT SYSTEM)    *
      *        *********************************************************
               88  CA-RESPONSE-SUCCESSFUL              VALUE 'WWUT010I'.
               88  CA-RESPONSE-ERROR                   VALUE 'WWUT999E'.

      *    *************************************************************
      *    * SERVICE-NAME/RECORD-KEY IDENTIFY A UNIQUE SERVICE REQUEST *
      *    *************************************************************
           05  CA-SERVICE-NAME         PIC X(24).
           05  CA-RECORD-KEY           PIC X(32).

           05  CA-SCREEN-STEP          PIC X(16).
      *        *********************************************************
      *        * INCOMING VALUES:                                      *
      *        *   1) 'START' = WEB SERVICE REQUEST STARTED            *
      *        *   2) '?????' = SCREEN-STEP (NAME?) COMPLETED          *
      *        *   3) 'END  ' = WEB SERVICE REQUEST COMPLETED          *
      *        * OUTGOING VALUES:                                      *
      *        *   1) IF A WEB SERVICE REQUEST IS BEING RESTARTED,     *
      *        *      WNV.LAST_STEP_COMPLETE IS RETURNED HERE          *
      *        *********************************************************

           05  CA-BEF-AFT-IND          PIC X(01).
      *        *********************************************************
      *        *   'B' = INVOKED BEFORE SCREEN-STEP STARTED            *
      *        *   'A' = INVOKED AFTER  SCREEN-STEP COMPLETED          *
      *        *********************************************************

           05  CA-USER-CD              PIC X(08).
      *        *********************************************************
      *        * LOGON USERID OF THE WEB SERVICE REQUESTOR             *
      *        *********************************************************

           05  CA-FILLER               PIC X(01).

           05  CA-SOAP-RESP-AREA.
      *        *********************************************************
FP6406*        *           VARIABLE LENGTH = VARCHAR(948)              *
      *        * INCOMING: SOAP INPUT AREA (PASSED WHEN SCREEN-STEP =  *
      *        *           'START'; REDEFINED FOR EACH SERVICE-NAME)   *
      *        * OUTGOING: TEXT ERROR MESSAGE WHEN CA-RESPONSE-ERROR   *
      *        *           IS RETURNED IN CA-VERS-RESP-CD              *
      *        * ----------------------------------------------------- *
      *        * DEFINE SOAP INPUTS FOR WEB3270I IMAGECOPY OR AUDIT/   *
      *        * TRACKING.  DEFINE ONLY THE SOAP INPUTS NEEDED (MAX    *
      *        * LENGTH IS 605).  THIS COPYBOOK IS USED BY THE         *
      *        * Coeus4FundSetupIFIS WEB SERVICE TO POPULATE THE       *
      *        * WUT010N/WEB3270N COMMAREA WHEN SCREEN-STEP = 'START'. *
      *        *********************************************************
               10  CA-SOAP-RESP-AREA-LEN
                                       PIC S9(4) USAGE COMP.
               10  CA-SOAP-RESP-AREA-TEXT
FP6406                                 PIC X(948).

               10  CA-SOAP-INPUT       REDEFINES
                   CA-SOAP-RESP-AREA-TEXT.

HVW001             15  CA-AWARD-TYPE   PIC X(02).
HVW001             15  FILLER          PIC X(01).
                   15  CA-FUNDTBL-LEVEL4-FUND
                                       PIC X(06).
                   15  FILLER          PIC X(01).
                   15  CA-FUNDTBL-LEVEL5-FUND
                                       PIC X(06).
                   15  FILLER          PIC X(01).
                   15  CA-FUNDTBL-FUND-DESCRIPTION
                                       PIC X(35).
                   15  FILLER          PIC X(01).
                   15  CA-FUNDTBL-EFFECTIVE-DATE
                                       PIC X(08).
                   15  FILLER          PIC X(01).
                   15  CA-FUNDTBL-FUND-TYPE
                                       PIC X(02).
                   15  FILLER          PIC X(01).
                   15  CA-FUNDTBL-LEVEL4-PREDECESSOR
                                       PIC X(06).
                   15  FILLER          PIC X(01).
                   15  CA-FUNDTBL-FED-DEMO-PROJ-FLAG
                                       PIC X(01).
                   15  FILLER          PIC X(01).
                   15  CA-FUNDTBL-REVENUE-ACCOUNT
                                       PIC X(06).
                   15  FILLER          PIC X(01).
                   15  CA-FUNDTBL-ACCRUAL-ACCOUNT
                                       PIC X(06).
                   15  FILLER          PIC X(01).
                   15  CA-GRANTTBL-CONTRACT-NUMBER
                                       PIC X(20).
                   15  FILLER          PIC X(01).
                   15  CA-GRANTTBL-COST-SHARE-METHOD
                                       PIC X(06).
                   15  FILLER          PIC X(01).
                   15  CA-GRANTTBL-MAX-COST-SHARE-AMT
                                       PIC X(10).
                   15  FILLER          PIC X(01).
                   15  CA-GRANTTBL-IND-COST-METHOD
                                       PIC X(06).
                   15  FILLER          PIC X(01).
                   15  CA-GRANTTBL-PAY-METHOD
                                       PIC X(04).
                   15  FILLER          PIC X(01).
                   15  CA-GRANTTBL-PMS-CODE
                                       PIC X(15).
                   15  FILLER          PIC X(01).
                   15  CA-INDEXTBL-ACCOUNT-INDEX
                                       PIC X(10).
                   15  FILLER          PIC X(01).
                   15  CA-INDEXTBL-ACCT-INDEX-TITLE
                                       PIC X(35).
                   15  FILLER          PIC X(01).
                   15  CA-INDEXTBL-ORGANIZATION
                                       PIC X(06).
                   15  FILLER          PIC X(01).
                   15  CA-INDEXTBL-PROGRAM
                                       PIC X(06).
                   15  FILLER          PIC X(01).
                   15  CA-STIPFUND-INDEX
                                       PIC X(07).
                   15  FILLER          PIC X(01).
                   15  CA-DETLCODE-EFFECTIVE-DATE
                                       PIC X(08).
                   15  FILLER          PIC X(01).
                   15  CA-DETLCODE-CATEGORY
                                       PIC X(04).
                   15  FILLER          PIC X(01).
HVW001             15  CA-TRFRDETL-ACCT-DIST-INDEX
HVW001                                 PIC X(10).
HVW001             15  FILLER          PIC X(01).
HVW001             15  CA-TRFRDETL-ACCT-DIST-ACCT
HVW001                                 PIC X(06).
HVW001             15  FILLER          PIC X(01).
HVW001             15  CA-TRFRDETL-ACCT-DIST-PROGRAM
HVW001                                 PIC X(06).
HVW001             15  FILLER          PIC X(01).
HVW001             15  CA-TRFRDETL-ACCT-RECV-ACCT
                                       PIC X(06).
                   15  FILLER          PIC X(01).
HVW001             15  CA-INVLACCT-ARRAY               OCCURS 96 TIMES.
                       20  CA-INVLACCT-CODE
                                       PIC X(06).
                       20  FILLER      PIC X(01).
HVW001             15  FILLER          PIC X(06).

      ******************************************************************
      * TOTAL COMMAREA LENGTH = 100 TO 1048, DEPENDING ON SOAP-RESP-AREA
      ******************************************************************
