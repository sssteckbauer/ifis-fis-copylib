      *%---------------------------------------------------------------*
DEVMLJ*%   CHANGED FSYDI30 TO AN INCLUDE MODULE                        *
      *%---------------------------------------------------------------*
      *%                                                               *
      *%                   150-FSYPI30 ROUTINE                         *
      *%                                                               *
      *%---------------------------------------------------------------*
       150-FSYPI30.

      ******************************************************************
      *******
      **********************  BEGINNING OF MODULE  *********************
      *******
      ******************************************************************
      *******
      *******
      ***  MODULE      : FSYPI30
      ***  DESCRIPTION : RULE-CLASS-CODE RETREIVAL
      ***  WRITTEN BY  : JOSEPH ROMERO
      ***  DATE WRITTEN: 10/89
      ***  COMMENTS    :
      ***  CURRENCY    :
      ***    EXPECTED  : NONE
      ***    PASSED    : FSYWG30D
      ******************************************************************
      *******
      ******************************************************************
      *******
      ***                           CHANGE HISTORY
      *     *
      ***
      *     *
      *** REF  AUTH    DATE                  DESCRIPTION               T
      *SIS  *
      *** ---  ----  --------  --------------------------------------  -
      *---- *
      *** 001  DDK   03/18/91  NO LONGER USED BY CHECKS APPLYING INVOICE
      *S    *
      ***                      TO CALL                                 0
      *0000 *
      *** 002  XXX   MM/DD/YY  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX  0
      *0000 *
      ******************************************************************
      *****
      *** VALIDATE THE RULE CLASS CODE BY EXTRACTING FROM SYSDATA
      ******************************************************************
      *****
           MOVE UNVRS-CODE-SY30 TO UNVRS-CODE-4011.
           MOVE FIMS-ENTY-CODE-SY30 TO FIMS-ENTY-CODE-4011.
           MOVE 'RULE-CLASS-CODE' TO ELMNT-NAME-4011.
           MOVE SPACES TO COA-CODE-4011.
      *%   FIND CALC R4011-SYSDAT
           MOVE 0001 TO DBLINK-CURR-IO-NUM
           MOVE DBLINK-FIND TO DBLINK-TYPE-SELECT
           MOVE DBLINK-CALC TO DBLINK-TYPE-GET
           MOVE FIMS-ENTY-CODE-4011 OF R4011-SYSDAT TO SSD-FIMS-ENTY-CD
           MOVE ELMNT-NAME-4011 OF R4011-SYSDAT TO SSD-ELMNT-NAME
           PERFORM 906-05-R4011-SYSDAT
              THRU 906-05-R4011-SYSDAT-EXIT
           PERFORM 999-SQL-STATUS
              THRU 999-SQL-STATUS-EXIT
           IF DB-REC-NOT-FOUND
               MOVE 000574 TO MSG-ID-SY00
           GO TO 150-FSYPI30-EXIT.
      *
           MOVE SPACES TO OPTN-2-CODE-4012.
           MOVE SPACE TO AC99W-STRNG01.
           STRING OPTN-1-CODE-SY30 DELIMITED SIZE
                  OPTN-2-CODE-4012 DELIMITED SIZE
                  '00' DELIMITED SIZE
                  INTO AC99W-STRNG01.
FCAMH *    MOVE AC99W-STRNG01(1:0006) TO APLCN-DATA-KEY-SY00.
FCAMH      MOVE AC99W-STRNG01(1:0030) TO APLCN-DATA-KEY-SY00.
      *%   OBTAIN R4012-SYSDAT-DTL WITHIN S-4011-4012 USING
      *%    APLCN-DATA-KEY-SY00.
           MOVE 0002 TO DBLINK-CURR-IO-NUM
           MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
           MOVE DBLINK-SORTED TO DBLINK-TYPE-GET
           MOVE APLCN-DATA-KEY-SY00 TO DBLINK-WS-S-4011-4012-KEY
           MOVE DBLINK-WS-S-4011-4012-03 TO DBLINK-S-4011-4012-03
           MOVE DBLINK-WS-S-4011-4012-04 TO DBLINK-S-4011-4012-04
           MOVE DBLINK-WS-S-4011-4012-05 TO DBLINK-S-4011-4012-05
           PERFORM 906-02-S-4011-4012
              THRU 906-02-S-4011-4012-EXIT
           PERFORM 999-SQL-STATUS
              THRU 999-SQL-STATUS-EXIT.
           IF DB-REC-NOT-FOUND
               MOVE 000574 TO MSG-ID-SY00

      *************************************
      ** RETURN TO AC99W-HIGHER
      *************************************
               MOVE AC99W-NO TO ACCOMM-FLAG-CLEAR
               MOVE AC99W-NO TO ACCOMM-FLAG-CONTINUE
               MOVE AC99W-HIGHER TO AC99W-NEXT-PROGRAM
               PERFORM 395-RETURN
                  THRU 395-RETURN-EXIT
      *** NO ENTRY IN SYSDATA TABLE
           END-IF.
      *
      ******************************************************************
      *****
      *** RETRIEVE THE RULE CLASS CODE
      ******************************************************************
      *****
      *
           MOVE UNVRS-CODE-SY30 TO UNVRS-CODE-4180.
           MOVE DATA-DESC-4012 TO ACWEXTR-PARM-IN.
FXXCHG     CALL 'ACXEXTR' USING ACWEXTR-PARMS.
           MOVE ACWEXTR-PARM-OUT TO RULE-CLASS-CODE-4180.
      *%   FIND CALC R4180-RULE-CLASS
           MOVE 0003 TO DBLINK-CURR-IO-NUM
           MOVE DBLINK-FIND TO DBLINK-TYPE-SELECT
           MOVE DBLINK-CALC TO DBLINK-TYPE-GET
           MOVE RULE-CLASS-CODE-4180 OF R4180-RULE-CLASS TO
               RLC-RULE-CLS-CD
           PERFORM 906-05-R4180-RULE-CLASS
              THRU 906-05-R4180-RULE-CLASS-EXIT
           PERFORM 999-SQL-STATUS
              THRU 999-SQL-STATUS-EXIT
           IF DB-REC-NOT-FOUND
               MOVE 000718 TO MSG-ID-SY00
           GO TO 150-FSYPI30-EXIT
           END-IF.
      *
           MOVE SYSTEM-DATE-SY30 TO START-DATE-4181.
           MOVE HIGH-VALUES TO TIME-STAMP-4181.
      *%   FIND R4181-RULE-EFCTV WITHIN S-4180-4181 USING
      *%    EFCTV-RCRD-KEY-4181
           MOVE 0004 TO DBLINK-CURR-IO-NUM
           MOVE DBLINK-FIND TO DBLINK-TYPE-SELECT
           MOVE DBLINK-SORTED TO DBLINK-TYPE-GET
           MOVE EFCTV-RCRD-KEY-4181 TO DBLINK-WS-S-4180-4181-KEY
      *%**DATE/TIME CNV** IDD KEY (WS)FLD -> DBLINKX-INPUT             *
           MOVE DBLINK-WS-S-4180-4181-02 TO DBLINKX-INPUT-PARM-08
      *%**DATE/TIME CNV**  CONVERT DATE                                *
           MOVE '1' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
FXXCHG     CALL 'IDDTECNV' USING
FXXCHG             DBLINKX-DATE-TIME-PARMS
           IF NOT DBLINKX-OK
      *%**DATE/TIME CNV**  CONVERT DATE FAILED                         *
           MOVE DBLINKX-NULL-DB2-DATE-ISO TO DBLINKX-OUTPUT-PARM
           END-IF
      *%**DATE/TIME CNV**  DBLINKXD -> KEY (HV)FLD                     *
           MOVE DBLINKX-OUTPUT-PARM TO DBLINK-S-4180-4181-02
      *%**DATE/TIME CNV** IDD KEY (WS)FLD -> DBLINKX-INPUT             *
           MOVE DBLINK-WS-S-4180-4181-03 TO DBLINKX-INPUT-PARM
      *%**DATE/TIME CNV**  CONVERT TIME                                *
           MOVE '3' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-TIME-FORMAT
           MOVE '51' TO DBLINKX-IDMS-TIME-FORMAT
FXXCHG     CALL 'IDDTECNV' USING
FXXCHG             DBLINKX-DATE-TIME-PARMS
           IF NOT DBLINKX-OK
      *%**DATE/TIME CNV**  CONVERT TIME FAILED                         *
           MOVE DBLINKX-NULL-DB2-TIME-ISO TO DBLINKX-OUTPUT-PARM
           END-IF
      *%**DATE/TIME CNV**  DBLINKXD -> KEY (HV)FLD                     *
           MOVE DBLINKX-OUTPUT-PARM TO DBLINK-S-4180-4181-03
           PERFORM 906-02-S-4180-4181
              THRU 906-02-S-4180-4181-EXIT
           PERFORM 999-SQL-STATUS
              THRU 999-SQL-STATUS-EXIT
      *%   OBTAIN PRIOR R4181-RULE-EFCTV WITHIN S-4180-4181.
           MOVE 0005 TO DBLINK-CURR-IO-NUM
           MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
           MOVE DBLINK-PRIOR TO DBLINK-TYPE-GET
           PERFORM 906-00P-S-4180-4181
              THRU 906-00P-S-4180-4181-EXIT
           PERFORM 999-SQL-STATUS
              THRU 999-SQL-STATUS-EXIT.

           IF DB-END-OF-SET
               MOVE 001358 TO MSG-ID-SY00
           GO TO 150-FSYPI30-EXIT
           ELSE
               IF STATUS-4181 NOT = 'A' OR
                  SYSTEM-DATE-SY30 > END-DATE-4181
                   MOVE 001358 TO MSG-ID-SY00
               GO TO 150-FSYPI30-EXIT
               END-IF
           END-IF.
      *
           IF RULE-CLASS-CODE-4180 = 'NOOP'
               CONTINUE
           ELSE
               IF CMPLT-IND-4181 = 'N'
                   MOVE 001359 TO MSG-ID-SY00
                   GO TO 150-FSYPI30-EXIT
               END-IF
           END-IF.
      *
      ******************************************************************
      *****
      *** RULE CLASS CODE IS VALID, MOVE IT TO THE WORK FIELD AND RETURN
      * **
      *** TO THE CALLING DIALOG
      * **
      ******************************************************************
      *****
      *
           MOVE RULE-CLASS-CODE-4180 TO RULE-CLASS-CODE-SY30.
           GO TO 150-FSYPI30-EXIT.
      *
      ******************************************************************
      *****
      ************************   END  OF  MODULE  **********************
      *****
      ******************************************************************
      *****

       150-FSYPI30-EXIT.
           EXIT.


       EJECT
