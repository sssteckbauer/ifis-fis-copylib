       975-TO-R4096-PO-HDR-SEQ SECTION.
           MOVE POS-USER-CD TO
               USER-CODE-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-CHNG-SEQ-NBR TO
               CHNG-SEQ-NMBR-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-PO-CMPLT-IND TO
               PO-CMPLT-IND-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-PRINT-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               PRINT-DATE-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-PO-PRINT-FLAG TO
               PO-PRINT-FLAG-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-DLVRY-BY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               DLVRY-BY-DATE-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-ACKNWDG-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACKNWDG-DATE-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-ORDER-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ORDER-DATE-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-APRVL-IND TO
               APRVL-IND-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-HDR-CNTR-DTL TO
               HDR-CNTR-DTL-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-HDR-CNTR-ACCT TO
               HDR-CNTR-ACCT-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-HDR-ERROR-IND TO
               HDR-ERROR-IND-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-TOTAL-AMT TO
               TOTAL-AMT-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACTVY-DATE-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-CNCL-IND TO
               CNCL-IND-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-CNCL-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               CNCL-DATE-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-ADDL-AMT TO
               ADDL-AMT-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-DSCNT-AMT TO
               DSCNT-AMT-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-ITEM-COUNT TO
               ITEM-COUNT-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-BUYER-CD TO
               BUYER-CODE-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-VNDR-CNTCT-NM TO
               VNDR-CNTCT-NAME-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-MAILCODE TO
               MAILCODE-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-PO-DSCNT-AMT TO
               PO-DSCNT-AMT-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-DSCNT-BFR-TX-IND TO
               DSCNT-BEFORE-TAX-IND-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-PO-DSCNT-PCT TO
               PO-DSCNT-PCT-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-TAX-CD TO
               TAX-CODE-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-DSCNT-CD TO
               DSCNT-CODE-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-PYMT-CD TO
               PYMT-CODE-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-SHIP-TO-CD TO
               SHIP-TO-CODE-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-TRNST-RISK-CD TO
               TRNST-RISK-CODE-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-ACKNWDG-IND TO
               ACKNWDG-IND-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-ADR-TYP-CD TO
               ADR-TYPE-CODE-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-END-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               END-DATE-4096 OF R4096-PO-HDR-SEQ
                                                                     .
           MOVE POS-ACCT-INDX-CD TO
               ACCT-INDX-CODE-4096 OF R4096-PO-HDR-SEQ
                                                                     .
       975-TO-R4096-PO-HDR-SEQ-EXIT.
           EXIT.
