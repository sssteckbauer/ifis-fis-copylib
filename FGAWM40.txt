      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FGAWM40                              04/24/00  12:33  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FGAWM40.
           02  ACTN-CODE-GROUP-GA40.
               03  ACTN-CODE-GA40           OCCURS 11   PIC X(1).
           02  ACCT-CODE-GA40               OCCURS 11   PIC X(6).
           02  ACCT-CODE-TITLE-4034-GA40    OCCURS 11   PIC X(35).
           02  ERROR-STATUS-GA40            OCCURS 11   PIC X(1).
           02  LAST-ACTVY-DATE-GA40         OCCURS 11   PIC X(6).
           02  TIME-STAMP-GA40              OCCURS 11   PIC X(4).
           02  AM-PM-FLAG-GA40              OCCURS 11   PIC X(1).
