       975-TO-R4159-CHK-DTL SECTION.
           MOVE CKD-USER-CD TO
               USER-CODE-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-UNVRS-CD TO
               UNVRS-CODE-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-DOC-NBR TO
               DCMNT-NMBR-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-ITEM-NBR TO
               ITEM-NMBR-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-SEQ-NBR TO
               SEQ-NMBR-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-COA-CD TO
               COA-CODE-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-ACCT-INDX-CD TO
               ACCT-INDX-CODE-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-FUND-CD TO
               FUND-CODE-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-ORGN-CD TO
               ORGZN-CODE-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-ACCT-CD TO
               ACCT-CODE-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-PRGRM-CD TO
               PRGRM-CODE-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-ACTVY-CD TO
               ACTVY-CODE-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-LCTN-CD TO
               LCTN-CODE-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-APRVD-AMT TO
               APRVD-AMT-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-DSCNT-AMT TO
               DSCNT-AMT-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-TAX-AMT TO
               TAX-AMT-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-ADDL-CHRG TO
               ADDL-CHRG-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-PAID-AMT TO
               PAID-AMT-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-FDRL-WTHHLD TO
               FDRL-WTHHLD-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-STATE-WTHHLD TO
               STATE-WTHHLD-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-PSTNG-PRD TO
               PSTNG-PRD-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-FSCL-YR TO
               FSCL-YR-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-RULE-CLS-CD TO
               RULE-CLASS-CODE-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-DSCNT-RULE-CLS TO
               DSCNT-RULE-CLASS-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-TAX-RULE-CLS TO
               TAX-RULE-CLASS-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-ADDL-CHRG-RULE-CLS TO
               ADDL-CHRG-RULE-CLASS-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-PO-NBR TO
               PO-NMBR-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-PO-ITEM-NBR TO
               PO-ITEM-NMBR-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-LIQDTN-IND TO
               LIQDTN-IND-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-PRJCT-CD TO
               PRJCT-CODE-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-DOC-TYP-SEQ-NBR TO
               DCMNT-TYPE-SEQ-NMBR-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-ADJMT-CD TO
               ADJMT-CODE-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-VNDR-INV-NBR TO
               VNDR-INV-NMBR-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-VNDR-INV-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               VNDR-INV-DATE-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-DOC-REF-NBR TO
               DCMNT-REF-NMBR-4159 OF R4159-CHK-DTL
                                                                     .
           MOVE CKD-TAX-RATE-CD TO
               TAX-RATE-CODE-4159 OF R4159-CHK-DTL
                                                                     .
       975-TO-R4159-CHK-DTL-EXIT.
           EXIT.
