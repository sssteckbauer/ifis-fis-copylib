      * /************************************************************/  $
      * /*   CHANGE LOG -- CPWSXBT1                                 */  $
      * /*   NAME            DATE      COMMENTS                     */  $
      * /*..........................................................*/  $
      * /*   B. ORTIZ       05/07/98   MODIFIED FOR GFAU (REL027)   */  $
      * /*                                                          */  $
      * /************************************************************/  $
      *01  XBT1-BUDGET-TRAILER.                                         CPWSXBT1
           05  XBT1-KEY.                                                CPWSXBT1
      ****     10  XBT1-LOCATION               PIC X(02).               3202RRRR
      ****     10  XBT1-SAU                    PIC X(01).               3202RRRR
      ****     10  XBT1-SUB-CAMPUS             PIC X(01).               3202RRRR
      ****     10  XBT1-ACCOUNT                PIC X(07).               3202RRRR
      ****     10  XBT1-FUND                   PIC X(06).               3202RRRR
      ****     10  XBT1-SUB-BUDGET             PIC X(02).               3202RRRR
000810         10  XBT1-FAU.                                            3202RRRR
000820             15  XBT1-LOCATION           PIC X(02).               3202RRRR
000830             15  XBT1-SAU                PIC X(01).               3202RRRR
000840             15  XBT1-SUB-CAMPUS         PIC X(01).               3202RRRR
000850             15  XBT1-ACCOUNT            PIC X(07).               3202RRRR
000860             15  XBT1-FUND               PIC X(06).               3202RRRR
000870             15  XBT1-SUB-BUDGET         PIC X(02).               3202RRRR
               10  XBT1-SEGMENT-TYPE           PIC X(01).               CPWSXBT1
           05  XBT1-INITIAL-AMOUNT             PIC S9(10).              CPWSXBT1
           05  XBT1-INITIAL-FTE                PIC S9(4)V99.            CPWSXBT1
           05  XBT1-TOT-TRANS-EXTR-AMT         PIC S9(10).              CPWSXBT1
           05  XBT1-TOT-TRANS-EXTR-FTE         PIC S9(4)V99.            CPWSXBT1
           05  XBT1-TOT-TRANS-NOT-EXTR-AMT     PIC S9(10).              CPWSXBT1
           05  XBT1-TOT-TRANS-NOT-EXTR-FTE     PIC S9(4)V99.            CPWSXBT1
           05  XBT1-TOT-ADJUSTED-AMT           PIC S9(10).              CPWSXBT1
           05  XBT1-TOT-ADJUSTED-FTE           PIC S9(4)V99.            CPWSXBT1
           05  XBT1-FILLER                     PIC X(31).               CPWSXBT1
           05  XBT1-IFIS-IFOAPAL.
               10  XBT1-IFIS-INDEX             PIC X(10).
               10  XBT1-IFIS-FUND              PIC X(06).
               10  XBT1-IFIS-ORG               PIC X(06).
               10  XBT1-IFIS-ACCOUNT           PIC X(06).
               10  XBT1-IFIS-PROGRAM           PIC X(06).
               10  XBT1-IFIS-ACTIVITY          PIC X(06).
               10  XBT1-IFIS-LOCATION          PIC X(06).
           05  XBT1-FILLER2                    PIC X(245).
