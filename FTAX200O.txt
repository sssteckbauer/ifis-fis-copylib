       01  FTAX200O-RECORD.
           03  FTAX200O-SORT-KEY.
               05  FTAX200O-FIXED-KEY.
                   07  FTAX200O-UNVRS-CODE          PICTURE X(002).
                   07  FTAX200O-REQUESTOR           PICTURE X(008).
                   07  FTAX200O-RPRT-CODE           PICTURE X(008).
                   07  FTAX200O-REQUEST-NMBR        PICTURE 9(004).
               05  FTAX200O-VARIABLE-KEY            PICTURE X(082).
               05  FTAX200O-OPTION-1   REDEFINES FTAX200O-VARIABLE-KEY. 06640699
                   07  FTAX200O-PRSN-NAME-1         PICTURE X(035).     06640899
                   07  FTAX200O-PRSN-ID-1.                              06640999
                       09  FTAX200O-PRSN-DIGIT-ONE-1                    06641099
                                                    PICTURE X(001).     06641199
                       09  FTAX200O-PRSN-DIGIT-NINE-1                   06641299
                                                    PICTURE X(009).     06641399
                   07  FTAX200O-EVENT-NMBR-1        PICTURE X(008).     06640899
                   07  FTAX200O-ACTVY-DATE-1        PICTURE 9(008).     06641499
                   07  FTAX200O-ACTVY-NMBR-1        PICTURE X(008).     06641599
               05  FTAX200O-OPTION-2   REDEFINES FTAX200O-VARIABLE-KEY. 06640699
                   07  FTAX200O-RCNCLTN-DUE-DATE-2  PICTURE 9(008).     06640899
                   07  FTAX200O-PRSN-NAME-2         PICTURE X(030).     06640899
                   07  FTAX200O-PRSN-ID-2.                              06640999
                       09  FTAX200O-PRSN-DIGIT-ONE-2                    06641099
                                                    PICTURE X(001).     06641199
                       09  FTAX200O-PRSN-DIGIT-NINE-2                   06641299
                                                    PICTURE X(009).     06641399
                   07  FTAX200O-EVENT-NMBR-2        PICTURE X(008).     06640899
                   07  FTAX200O-ACTVY-DATE-2        PICTURE 9(008).     06641499
                   07  FTAX200O-ACTVY-NMBR-2        PICTURE X(008).     06641599
               05  FTAX200O-OPTION-3   REDEFINES FTAX200O-VARIABLE-KEY. 06640699
                   07  FTAX200O-ZIP-CODE-3          PICTURE X(010).     06640899
                   07  FTAX200O-PRSN-NAME-3         PICTURE X(030).     06640899
                   07  FTAX200O-PRSN-ID-3.                              06640999
                       09  FTAX200O-PRSN-DIGIT-ONE-3                    06641099
                                                    PICTURE X(001).     06641199
                       09  FTAX200O-PRSN-DIGIT-NINE-3                   06641299
                                                    PICTURE X(009).     06641399
                   07  FTAX200O-EVENT-NMBR-3        PICTURE X(008).     06640899
                   07  FTAX200O-ACTVY-DATE-3        PICTURE 9(008).     06641499
                   07  FTAX200O-ACTVY-NMBR-3        PICTURE X(008).     06641599
               05  FTAX200O-OPTION-4   REDEFINES FTAX200O-VARIABLE-KEY. 06640699
                   07  FTAX200O-RCNCLTN-DUE-DATE-4  PICTURE 9(008).     06640899
                   07  FTAX200O-ZIP-CODE-4          PICTURE X(010).     06640899
                   07  FTAX200O-PRSN-NAME-4         PICTURE X(030).     06640899
                   07  FTAX200O-PRSN-ID-4.                              06640999
                       09  FTAX200O-PRSN-DIGIT-ONE-4                    06641099
                                                    PICTURE X(001).     06641199
                       09  FTAX200O-PRSN-DIGIT-NINE-4                   06641299
                                                    PICTURE X(009).     06641399
                   07  FTAX200O-EVENT-NMBR-4        PICTURE X(008).     06640899
                   07  FTAX200O-ACTVY-DATE-4        PICTURE 9(008).     06641499
                   07  FTAX200O-ACTVY-NMBR-4        PICTURE X(008).     06641599
               05  FTAX200O-OPTION-5   REDEFINES FTAX200O-VARIABLE-KEY.
                   07  FTAX200O-EMAIL-ADR-TYPE-CODE-5
                                                    PICTURE X(002).
                   07  FTAX200O-PRSN-NAME-5         PICTURE X(030).
                   07  FTAX200O-RCNCLIN-DUE-DATE    PICTURE 9(008).
                   07  FTAX200O-PRSN-ID-5.
                       09  FTAX200O-PRSN-DIGIT-ONE-5
                                                    PICTURE X(001).
                       09  FTAX200O-PRSN-DIGIT-NINE-5
                                                    PICTURE X(009).
                   07  FTAX200O-EVENT-NMBR-5        PICTURE X(008).
                   07  FTAX200O-ACTVY-DATE-5        PICTURE 9(008).
                   07  FTAX200O-ACTVY-NMBR-5        PICTURE X(008).
               05  FTAX200O-TYPE.
                   07  FILLER                       PICTURE X(001).
                   07  FTAX200O-REC-TYPE            PICTURE X(001).
           03  FTAX200O-DATA.
               05  FTAX200O-VARIABLE-DATA           PICTURE X(306).
               05  FTAX200O-HDR-DATA
                                  REDEFINES FTAX200O-VARIABLE-DATA.
                   07  FTAX200O-UNVRS-NAME          PICTURE X(035).     03380036
                   07  FTAX200O-RPRT-TITLE          PICTURE X(060).     03380036
                   07  FTAX200O-USER-ID             PICTURE X(008).     03380036
                   07  FTAX200O-LAST-ACTVY-DATE     PICTURE 9(008).     03410099
                   07  FTAX200O-SEQ-NMBR            PICTURE 9(004).     03420036
                   07  FTAX200O-AS-OF-DATE          PICTURE 9(008).     03410099
                   07  FTAX200O-TO-DATE             PICTURE 9(008).
                   07  FTAX200O-DLT-IND             PICTURE X(001).     03430036
                   07  FTAX200O-HOLD-IND            PICTURE X(001).     03430036
                   07  FTAX200O-DUNNING-RPT-IND     PICTURE X(001).     03470036
                   07  FTAX200O-DUNNING-NOTICES-IND PICTURE X(001).     03470036
                   07  FTAX200O-SORT-PARM           PICTURE X(002).     03510099
                   07  FTAX200O-COMM-PLAN           PICTURE X(004).     03540036
                   07  FTAX200O-COMM-REF-ID         PICTURE 9(006).     03540036
                   07  FTAX200O-LABEL-SEL-IND       PICTURE X(001).     03640036
                   07  FTAX200O-LABEL-SORT-PARM     PICTURE X(002).     03660099
                   07  FTAX200O-TA-ADR-TYPE-CODE    OCCURS 5 TIMES
                                                    PICTURE X(002).
                   07  FTAX200O-PRINT-TOP-LINE-IND  PICTURE X(001).     03710036
                   07  FTAX200O-ADR-SEL-DATE        PICTURE 9(008).     03760099
                   07  FTAX200O-LABEL-LITERAL       PICTURE X(035).     03780036
                   07  FTAX200O-TA-EMAIL-TYPE-CODE  OCCURS 4 TIMES
                                                    PICTURE X(002).
               05  FTAX200O-TRAVELLER-DATA
                                  REDEFINES FTAX200O-VARIABLE-DATA.
                   07  FTAX200O-TRVL-PID.
                       09  FTAX200O-TRVL-ACCT-DIGIT-ONE
                                                    PICTURE X(001).
                       09  FTAX200O-TRVL-ACCT-LAST-NINE
                                                    PICTURE X(009).
                   07  FTAX200O-TRVL-NAME           PICTURE X(035).
                   07  FTAX200O-TRVL-FULL-NAME      PICTURE X(055).
                   07  FTAX200O-COMPLETE-ADDRESS.
                       09  FTAX200O-ADDR-LINE       OCCURS 4 TIMES
                                                    PICTURE X(035).
                       09  FTAX200O-CITY            PICTURE X(018).
                       09  FTAX200O-STATE           PICTURE X(002).
                       09  FTAX200O-ZIP             PICTURE X(010).
                       09  FTAX200O-COUNTRY         PICTURE X(035).
                   07  FTAX200O-EMPLY-IND           PICTURE X(001).
               05  FTAX200O-EVENT-DATA
                                  REDEFINES FTAX200O-VARIABLE-DATA.
                   07  FTAX200O-EVENT-EMPLY-IND     PICTURE X(001).
                   07  FTAX200O-EVENT-NMBR          PICTURE X(008).
                   07  FTAX200O-EVENT-DESC          PICTURE X(035).
                   07  FTAX200O-EVENT-BAL           PICTURE S9(10)V99.
                   07  FTAX200O-EVENT-START-DATE    PICTURE 9(008).
                   07  FTAX200O-EVENT-END-DATE      PICTURE 9(008).
                   07  FTAX200O-RCNCLTN-DUE-DATE    PICTURE 9(008).
                   07  FTAX200O-AIR-DRCT-BILL-IND   PICTURE X(001).
                   07  FTAX200O-CAR-DRCT-BILL-IND   PICTURE X(001).
                   07  FTAX200O-HOTL-DRCT-BILL-IND  PICTURE X(001).
                   07  FTAX200O-MEAL-DRCT-BILL-IND  PICTURE X(001).
                   07  FTAX200O-OTHR-DRCT-BILL-IND  PICTURE X(001).
                   07  FTAX200O-ESTMD-EXPNS-AMT     PICTURE S9(10)V99.
                   07  FTAX200O-TOTL-ADVNC-AMT      PICTURE S9(10)V99.
                   07  FTAX200O-DRCT-BILL-AMT       PICTURE S9(10)V99.
                   07  FTAX200O-TOTL-EXPNS-AMT      PICTURE S9(10)V99.
               05  FTAX200O-ACTIVITY-DATA
                                  REDEFINES FTAX200O-VARIABLE-DATA.
                   07  FTAX200O-ACTVY-DCMNT-NMBR    PICTURE X(008).
                   07  FTAX200O-ACTVY-DATE          PICTURE 9(008).
                   07  FTAX200O-ACTVY-AMT           PICTURE S9(10)V99.
                   07  FTAX200O-ACTVY-DESC          PICTURE X(035).
               05  FTAX200O-EMAIL-DATA
                                  REDEFINES FTAX200O-VARIABLE-DATA.
                   07  FTAX200O-EMAIL-ADDR-TYPE-CODE
                                                    PICTURE X(002).
                   07  FTAX200O-EMAIL-ADDRESS       PICTURE X(035).
