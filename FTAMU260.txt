    ***Created by Convert/DC version V8R03 on 12/08/00 at 14:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-TA26-Z = 'Y'
               MOVE WK01-CODE-TA26 TO ACTN-CODE-TA26 OF FTAWM26
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-4415-TA26-Z = 'Y'
               MOVE WK01-TYPE-4415-TA26 TO RCNCLTN-TYPE-4415-TA26 OF
                FTAWK26
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4415-TA26-Z = 'Y'
               MOVE WK01-DESC-4415-TA26 TO FULL-DESC-4415-TA26 OF
                FTAWM26
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4415-TA2A-Z = 'Y'
               MOVE WK01-DESC-4415-TA2A TO SHORT-DESC-4415-TA26 OF
                FTAWM26
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-4180-TA26-Z = 'Y'
               MOVE WK01-CLASS-CODE-4180-TA26 TO
                RULE-CLASS-CODE-4180-TA26 OF FTAWM26
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-DESC-4181-TA26-Z = 'Y'
               MOVE WK01-CLASS-DESC-4181-TA26 TO
                RULE-CLASS-DESC-4181-TA26 OF FTAWM26
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-CODE-4033-TA26-Z = 'Y'
               MOVE WK01-ACCT-CODE-4033-TA26 TO
                BANK-ACCT-CODE-4033-TA26 OF FTAWM26
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-DESC-4062-TA26-Z = 'Y'
               MOVE WK01-CODE-DESC-4062-TA26 TO
                BANK-CODE-DESC-4062-TA26 OF FTAWM26
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4000-TA26-Z = 'Y'
               MOVE WK01-CODE-4000-TA26 TO COA-CODE-4000-TA26 OF FTAWM26
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-4415-TA26-Z = 'Y'
               MOVE WK01-INDX-CODE-4415-TA26 TO
                ACCT-INDX-CODE-4415-TA26 OF FTAWM26
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4415-TA26-Z = 'Y'
               MOVE WK01-CODE-4415-TA26 TO FUND-CODE-4415-TA26 OF
                FTAWM26
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4415-TA2A-Z = 'Y'
               MOVE WK01-CODE-4415-TA2A TO ORGZN-CODE-4415-TA26 OF
                FTAWM26
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4415-TA2B-Z = 'Y'
               MOVE WK01-CODE-4415-TA2B TO ACCT-CODE-4415-TA26 OF
                FTAWM26
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4415-TA2C-Z = 'Y'
               MOVE WK01-CODE-4415-TA2C TO PRGRM-CODE-4415-TA26 OF
                FTAWM26
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4415-TA2D-Z = 'Y'
               MOVE WK01-CODE-4415-TA2D TO ACTVY-CODE-4415-TA26 OF
                FTAWM26
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4415-TA2E-Z = 'Y'
               MOVE WK01-CODE-4415-TA2E TO LCTN-CODE-4415-TA26 OF
                FTAWM26
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CHECK-IND-4415-TA26-Z = 'Y'
               MOVE WK01-CHECK-IND-4415-TA26 TO
                IMMDT-CHECK-IND-4415-TA26 OF FTAWM26
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-BAL-IND-4415-TA26-Z = 'Y'
               MOVE WK01-BAL-IND-4415-TA26 TO PAID-BAL-IND-4415-TA26 OF
                FTAWM26
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CHECK-IND-4415-TA2A-Z = 'Y'
               MOVE WK01-CHECK-IND-4415-TA2A TO
                BATCH-CHECK-IND-4415-TA26 OF FTAWM26
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4415-TA26-Z = 'Y'
               MOVE WK01-IND-4415-TA26 TO BLNCD-IND-4415-TA26 OF FTAWM26
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CHECK-IND-4415-TA2B-Z = 'Y'
               MOVE WK01-CHECK-IND-4415-TA2B TO MNL-CHECK-IND-4415-TA26
                OF FTAWM26
           END-IF.
