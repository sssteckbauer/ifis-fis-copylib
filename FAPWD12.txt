      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWD12                              04/24/00  12:55  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWD12.
           02  SYSTEM-DATE-AP12               COMP-3    PIC S9(8).
           02  START-DATE-AP12                COMP-3    PIC S9(8).
           02  SAVE-DBKEY-ID-4172-AP12        COMP      PIC S9(8).
           02  END-DATE-AP12                  COMP-3    PIC S9(8).
           02  EFCTV-KEY-AP12.
               03  EFCTV-DATE-AP12            COMP-3    PIC S9(8).
               03  TIME-STAMP-AP12                      PIC X(6).
           02  DSCNT-PCT-AP12                 COMP-3    PIC S9(3)V999.
