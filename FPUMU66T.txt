       01  MAP-CONTROL.

    ***Created by Convert/DC version V8R03 on 12/11/00 at 15:00***

           02  FPUMU66-MAP-CONTROL.
           03  FPUMU66T.
           04  WK01-DCXMAPL-PARMS.
               10  WK01-MAPNAME                  PIC X(8)
                                                 VALUE 'FPUMU66I'.
               10  WK01-IKE-LENGTH               PIC S9(8)  COMP
                                                 VALUE 12075.
               10  WK01-TINA-LENGTH              PIC S9(4)  COMP
                                                 VALUE 915.
           05  WK01-ATTRIBUTES                   PIC X(60)
                                                 VALUE SPACE.
           05  WK01-ATTRIBUTES-ERROR             PIC X(60) VALUE
               'U  BIF'.
           05  WK01-CURSOR-DFLD                  PIC X(36)
                                                 VALUE SPACES.
           05  WK01-CURSOR-DFLD-QUAL             PIC X(30)
                                                 VALUE SPACES.
           05  WK01-ROW                          PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-COLUMN                       PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-ERROR-MSG                    PIC X(80)
                                                 VALUE SPACES.
           05  WK01-MAP-DATA-LEN                 PIC S9(6)
                                                 VALUE ZERO.
           05  WK01-1ST-FLAG                     PIC X(1)
                                                 VALUE 'Y'.
           05  WK01-FLAG-OUTPUT-DATA             PIC X(1)
                                                 VALUE 'Y'.
      *    05  WK01-STATUS-RETURN-FLAG           PIC X(1)
      *                                          VALUE SPACES.
           05  WK01-ALL-BUT-FLAG                 PIC X(1)
                                                 VALUE SPACES.
           05  WK01-FLAG-ALL-FIELDS-CHANGED      PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-CHANGED      PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERROR        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERROR        PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.

           05  WK01-VARDATA                      PIC X(11880)
                                                 VALUE SPACES.
           05  FILLER                            PIC X(100)
                                                 VALUE SPACES.
           04  WK01-ELEMENT-AREA.

               10  WK01-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-6001-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00       PIC X(35)
                                               VALUE SPACES.
               10  WK01-DATE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-HH-MM-TIME-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00       PIC X(4)
                                               VALUE SPACES.
               10  WK01-PM-FLAG-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-USER-ID-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID       PIC X(32)
                                               VALUE SPACES.
               10  WK01-RSPNS-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-FUNC-DESCRIPTION-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION       PIC X(28)
                                               VALUE SPACES.
               10  WK01-STATUS-CODE-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-CODE-PU66-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU66-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU66-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU66-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU66-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU66       PIC X(1)
                                               VALUE SPACES.
               10  WK01-NMBR-PU66-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-PU66-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-PU66-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-PU66-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-PU66-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-PU66       PIC X(8)
                                               VALUE SPACES.
               10  WK01-SEQ-NMBR-PU66-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-NMBR-PU66-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-NMBR-PU66-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-NMBR-PU66-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-NMBR-PU66-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-NMBR-PU66       PIC X(3)
                                               VALUE SPACES.
               10  WK01-CO-PU66-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CO-PU66-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CO-PU66-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CO-PU66-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CO-PU66-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CO-PU66       PIC X(3)
                                               VALUE SPACES.
               10  WK01-AMT-PU66-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-PU66-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-PU66-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-PU66-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-PU66-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-PU66       PIC S9(10)V9(2)
                                               VALUE ZEROS.
               10  WK01-TOTAL-PU66-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TOTAL-PU66-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TOTAL-PU66-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TOTAL-PU66-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TOTAL-PU66-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TOTAL-PU66       PIC S9(9)V9(2)
                                               VALUE ZEROS.
               10  WK01-CHRG-PU66-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CHRG-PU66-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CHRG-PU66-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CHRG-PU66-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CHRG-PU66-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CHRG-PU66       PIC S9(10)V9(2)
                                               VALUE ZEROS.
               10  WK01-SEQ-ADD-PU66-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-ADD-PU66-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-ADD-PU66-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-ADD-PU66-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-ADD-PU66-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-ADD-PU66       PIC S9(10)V9(2)
                                               VALUE ZEROS.
               10  WK01-HDR-DESC-PU66-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HDR-DESC-PU66-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HDR-DESC-PU66-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HDR-DESC-PU66-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HDR-DESC-PU66-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HDR-DESC-PU66       PIC X(50)
                                               VALUE SPACES.
               10  WK01-NMBR-PU6A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-PU6A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-PU6A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-PU6A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-PU6A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-PU6A       PIC 9(4)
                                               VALUE ZEROS.
               10  WK01-COUNT-PU66-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-COUNT-PU66-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-COUNT-PU66-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-COUNT-PU66-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-COUNT-PU66-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-COUNT-PU66       PIC 9(4)
                                               VALUE ZEROS.
               10  WK01-PU66-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PU66-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PU66-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PU66-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PU66-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PU66       PIC S9(6)V9(2)
                                               VALUE ZEROS.
               10  WK01-PRICE-PU66-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PRICE-PU66-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PRICE-PU66-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PRICE-PU66-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PRICE-PU66-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PRICE-PU66       PIC 9(10)V9(4)
                                               VALUE ZEROS.
               10  WK01-DSCNT-AMT-PU66-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DSCNT-AMT-PU66-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DSCNT-AMT-PU66-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DSCNT-AMT-PU66-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DSCNT-AMT-PU66-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DSCNT-AMT-PU66       PIC S9(10)V9(2)
                                               VALUE ZEROS.
               10  WK01-DSCNT-AMT-PU6A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DSCNT-AMT-PU6A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DSCNT-AMT-PU6A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DSCNT-AMT-PU6A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DSCNT-AMT-PU6A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DSCNT-AMT-PU6A       PIC S9(10)V9(2)
                                               VALUE ZEROS.
               10  WK01-UNIT-PRICE-PU66-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-UNIT-PRICE-PU66-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-UNIT-PRICE-PU66-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-UNIT-PRICE-PU66-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-UNIT-PRICE-PU66-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-UNIT-PRICE-PU66       PIC S9(7)V9(2)
                                               VALUE ZEROS.
               10  WK01-SEQ-AMT-PU66-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-AMT-PU66-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-AMT-PU66-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-AMT-PU66-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-AMT-PU66-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-AMT-PU66       PIC S9(10)V9(2)
                                               VALUE ZEROS.
               10  WK01-TAX-AMT-PU66-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TAX-AMT-PU66-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TAX-AMT-PU66-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TAX-AMT-PU66-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TAX-AMT-PU66-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TAX-AMT-PU66       PIC S9(10)V9(2)
                                               VALUE ZEROS.
               10  WK01-SEQ-TAX-PU66-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-TAX-PU66-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-TAX-PU66-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-TAX-PU66-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-TAX-PU66-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-TAX-PU66       PIC S9(10)V9(2)
                                               VALUE ZEROS.
               10  WK01-NMBR-PU6B-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-PU6B-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-PU6B-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-PU6B-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-PU6B-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-PU6B       PIC 9(4)
                                               VALUE ZEROS.
               10  WK01-DTL-DESC-PU66-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DTL-DESC-PU66-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DTL-DESC-PU66-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DTL-DESC-PU66-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DTL-DESC-PU66-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DTL-DESC-PU66       PIC X(50)
                                               VALUE SPACES.
               10  WK01-INDX-CODE-PU66-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-CODE-PU66-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-CODE-PU66-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-CODE-PU66-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-CODE-PU66-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-CODE-PU66       PIC X(10)
                                               VALUE SPACES.
               10  WK01-AMT-PU6A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-PU6A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-PU6A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-PU6A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-PU6A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-PU6A       PIC S9(10)V9(2)
                                               VALUE ZEROS.
               10  WK01-PCT-DSTBN-PU66-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PCT-DSTBN-PU66-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PCT-DSTBN-PU66-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PCT-DSTBN-PU66-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PCT-DSTBN-PU66-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PCT-DSTBN-PU66       PIC 9(3)V9(3)
                                               VALUE ZEROS.
               10  WK01-CODE-PU6A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU6A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU6A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU6A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU6A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU6A       PIC X(6)
                                               VALUE SPACES.
               10  WK01-TAX-AMT-PU6A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TAX-AMT-PU6A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TAX-AMT-PU6A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TAX-AMT-PU6A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TAX-AMT-PU6A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TAX-AMT-PU6A       PIC S9(10)V9(2)
                                               VALUE ZEROS.
               10  WK01-PCT-DSTBN-PU6A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PCT-DSTBN-PU6A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PCT-DSTBN-PU6A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PCT-DSTBN-PU6A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PCT-DSTBN-PU6A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PCT-DSTBN-PU6A       PIC 9(3)V9(3)
                                               VALUE ZEROS.
               10  WK01-CODE-PU6B-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU6B-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU6B-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU6B-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU6B-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU6B       PIC X(6)
                                               VALUE SPACES.
               10  WK01-ADDL-CHRG-PU66-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADDL-CHRG-PU66-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADDL-CHRG-PU66-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADDL-CHRG-PU66-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADDL-CHRG-PU66-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADDL-CHRG-PU66       PIC S9(10)V9(2)
                                               VALUE ZEROS.
               10  WK01-PCT-DSTBN-PU6B-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PCT-DSTBN-PU6B-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PCT-DSTBN-PU6B-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PCT-DSTBN-PU6B-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PCT-DSTBN-PU6B-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PCT-DSTBN-PU6B       PIC 9(3)V9(3)
                                               VALUE ZEROS.
               10  WK01-CODE-PU6C-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU6C-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU6C-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU6C-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU6C-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU6C       PIC X(6)
                                               VALUE SPACES.
               10  WK01-COUNT-PU6A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-COUNT-PU6A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-COUNT-PU6A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-COUNT-PU6A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-COUNT-PU6A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-COUNT-PU6A       PIC 9(4)
                                               VALUE ZEROS.
               10  WK01-ZZMESSAGE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE       PIC X(160)
                                               VALUE SPACES.
               10  WK01-PASSED-ONE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE       PIC X(32)
                                               VALUE SPACES.
               10  WK01-MAP-RESPONSE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-TEXT-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00       PIC X(14)
                                               VALUE SPACES.
