      ******************************************************************
      *  ***** COPYBOOK WPU009V - USED FOR VALBATCH WEB SERVICE *****  *
      *                                                                *
      *  WEB/CICS COMMAREA TO VALIDATE SCIQUEST PURCHASE ORDER BATCH   *
      *  PAYLOAD                                                       *
      ******************************************************************

      ******************************************************************
      *    AFTER CHANGES ARE MADE TO THIS COPYBOOK, IT MUST BE COPIED  *
      *    INTO A WEBSPHERE DEVELOPER PROJECT FOLDER, FROM WHICH THE   *
      *    WEB SERVICE INTERFACE PROGRAM, WSDL, & WSBIND ARE GENERATED.*
      ******************************************************************

       01  VALBATCH-PARM-AREA.

         02  VALBATCH-CHANNEL-HEADER.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND AND OUTBOUND  *
      *      ***********************************************************

           05  VALBATCH-CHANNEL-ID            PIC X(08).
      *        *********************************************************
      *        * MUST BE 'VALBATCH '                                   *
      *        *********************************************************

           05  VALBATCH-VERS-RESP-CD          PIC X(08).
      *        *********************************************************
      *        * INCOMING: COMMAREA VERSION = 01.00.00                 *
      *        * OUTGOING: RETURN RESPONSE FOR WFPU(WEB/IFIS PU SYSTEM)*
      *        *********************************************************
               88  VALBATCH-RESPONSE-SUCCESSFUL        VALUE 'WFPU000I'.
               88  VALBATCH-RESPONSE-ERROR             VALUE 'WFPU999E'.

         02  VALBATCH-INPUT-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND AND OUTBOUND  *
      *      ***********************************************************

           05  VALBATCH-SCIQ-BATCH-ID         PIC X(25).

         02  VALBATCH-OUTPUT-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: OUTBOUND ONLY         *
      *      ***********************************************************

           05  VALBATCH-COMPLETED-POS-COUNT   PIC  9(05).
           05  VALBATCH-COMPLETED-POS-AMT     PIC +9(10).99.
           05  VALBATCH-SUSPENDED-POS-COUNT   PIC  9(05).
           05  VALBATCH-SUSPENDED-POS-AMT     PIC +9(10).99.

         02  VALBATCH-RETURN-STATUS-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: OUTBOUND ONLY         *
      *      ***********************************************************

           05  VALBATCH-RETURN-STATUS-CODE    PIC X(01).
      *        *********************************************************
      *        * S = SUCCESSFUL, E = DATA ERROR(S), F = FATAL ERROR    *
      *        *********************************************************

           05  VALBATCH-RETURN-MSG-COUNT      PIC 9(01).
      *        *********************************************************
      *        * 1 = NUMBER OF RETURN-MSG-AREA(S)                      *
      *        *********************************************************

           05  VALBATCH-RETURN-MSG-AREA.
      *        *********************************************************
      *        * FOR STATUS 'S', SUCCESS MESSAGE NUMBER & TEXT         *
      *        * FOR STATUS 'E', ERROR MSG NUMBER, FIELD & TEXT        *
      *        * FOR STATUS 'F', FATAL SQLCODE, CURR-IO-NUM WITH       *
      *        *                 PARAGRAPH NAME, & ASSOC DB2 TABLE     *
      *        *********************************************************
               10  VALBATCH-RETURN-MSG-NUM    PIC X(06).
               10  VALBATCH-RETURN-MSG-FIELD  PIC X(30).
               10  VALBATCH-RETURN-MSG-TEXT   PIC X(40).

