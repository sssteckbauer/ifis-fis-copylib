      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM64                              04/24/00  12:18  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM64.
           02  ACTN-CODE-GROUP-CH64.
               03  ACTN-CODE-CH64           OCCURS 11   PIC X(1).
           02  COA-CODE-4026-CH64           OCCURS 11   PIC X(1).
           02  STATUS-4065-CH64             OCCURS 11   PIC X(1).
           02  START-DATE-4065-CH64         OCCURS 11   PIC X(6).
           02  END-DATE-4065-CH64           OCCURS 11   PIC X(6).
           02  INDRT-COST-CODE-4026-CH64    OCCURS 11   PIC X(6).
           02  INDRT-COST-DESC-4065-CH64    OCCURS 11   PIC X(35).
