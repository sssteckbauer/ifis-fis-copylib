    ***Created by Convert/DC version V8R03 on 01/04/01 at 09:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE USER-NAME-UT02 OF FUTWK02 TO WK01-NAME-UT02.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-DESC-UT02 OF FUTWK02 TO WK01-DESC-UT02.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-TOP-UT02 OF FUTWM02 TO WK01-CODE-TOP-UT02.
      *%--------------------------------------------------------------%*
           MOVE USER-ID-UT02 OF FUTWK02 TO WK01-ID-UT02.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-TYPE-UT02 OF FUTWK02 TO WK01-TYPE-UT02.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-UT02 OF FUTWM02(0001) TO WK01-CODE-UT021.
      *%--------------------------------------------------------------%*
           MOVE APRVL-ID-UT02 OF FUTWM02(0001) TO WK01-ID-UT0A11.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-UT02 OF FUTWM02(0001) TO WK01-NAME-UT0A11.
      *%--------------------------------------------------------------%*
           MOVE APRVL-LIMIT-UT02 OF FUTWM02(0001) TO WK01-LIMIT-UT021.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-APRVL-CODE-UT02 OF FUTWM02(0001) TO
                WK01-APRVL-CODE-UT021.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-APRVL-CODE-UT02 OF FUTWM02(0002) TO
                WK01-APRVL-CODE-UT022.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-APRVL-CODE-UT02 OF FUTWM02(0003) TO
                WK01-APRVL-CODE-UT023.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-APRVL-CODE-UT02 OF FUTWM02(0004) TO
                WK01-APRVL-CODE-UT024.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-APRVL-CODE-UT02 OF FUTWM02(0005) TO
                WK01-APRVL-CODE-UT025.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-APRVL-CODE-UT02 OF FUTWM02(0006) TO
                WK01-APRVL-CODE-UT026.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-NAME-UT02 OF FUTWM02(0001) TO WK01-NAME-UT0B11.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-NAME-UT02 OF FUTWM02(0002) TO WK01-NAME-UT0A12.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-NAME-UT02 OF FUTWM02(0003) TO WK01-NAME-UT0A13.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-NAME-UT02 OF FUTWM02(0004) TO WK01-NAME-UT0A14.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-NAME-UT02 OF FUTWM02(0005) TO WK01-NAME-UT0A15.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-NAME-UT02 OF FUTWM02(0006) TO WK01-NAME-UT0A16.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-UT02 OF FUTWM02(0002) TO WK01-CODE-UT022.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-UT02 OF FUTWM02(0003) TO WK01-CODE-UT023.
      *%--------------------------------------------------------------%*
           MOVE APRVL-ID-UT02 OF FUTWM02(0002) TO WK01-ID-UT0A12.
      *%--------------------------------------------------------------%*
           MOVE APRVL-ID-UT02 OF FUTWM02(0003) TO WK01-ID-UT0A13.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-UT02 OF FUTWM02(0002) TO WK01-NAME-UT0B12.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-UT02 OF FUTWM02(0003) TO WK01-NAME-UT0B13.
      *%--------------------------------------------------------------%*
           MOVE APRVL-LIMIT-UT02 OF FUTWM02(0002) TO WK01-LIMIT-UT022.
      *%--------------------------------------------------------------%*
           MOVE APRVL-LIMIT-UT02 OF FUTWM02(0003) TO WK01-LIMIT-UT023.
      *%--------------------------------------------------------------%*
           MOVE SEQ-NMBR-UT02 OF FUTWM02(0002) TO WK01-NMBR-UT022.
      *%--------------------------------------------------------------%*
           MOVE SEQ-NMBR-UT02 OF FUTWM02(0003) TO WK01-NMBR-UT023.
      *%--------------------------------------------------------------%*
           MOVE LVL-RQST-UT02 OF FUTWM02 TO WK01-RQST-UT02.
      *%--------------------------------------------------------------%*
           MOVE APRVL-TMPLT-CODE-UT02 OF FUTWK02 TO
                WK01-TMPLT-CODE-UT02.
      *%--------------------------------------------------------------%*
           MOVE APRVL-TMPLT-DESC-UT02 OF FUTWK02 TO
                WK01-TMPLT-DESC-UT02.
      *%--------------------------------------------------------------%*
           MOVE SEQ-NMBR-UT02 OF FUTWM02(0001) TO WK01-NMBR-UT021.
      *%--------------------------------------------------------------%*
           MOVE CONFIRM-DELETE-UT02  TO WK01-DELETE-IND-UT02.
      *%--------------------------------------------------------------%*
           MOVE DELETE-LIT-UT02  TO WK01-DEL-LIT-UT02.
