      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FUTWM88                              04/24/00  12:50  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FUTWM88.
           02  ACTN-CODE-GROUP-UT88.
               03  ACTN-CODE-UT88           OCCURS 3    PIC X(1).
           02  RSPNS-ID-UT88                OCCURS 3    PIC X(8).
           02  LONG-DESC-UT88               OCCURS 3    PIC X(30).
           02  CURR-RESPONSE-UT88                       PIC X(8).
           02  RESPONSE-DESC-UT88                       PIC X(30).
           02  TEXT-IND-UT88                OCCURS 3    PIC X(1).
