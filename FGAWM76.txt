      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FGAWM76.                             04/24/00  12:22  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FGAWM76.
          02  MAP-CONTROL.
           03  WK01-DCXMAPL-PARMS.
               10 WK01-MAPNAME                   PIC X(8)
                                                 VALUE 'FGAMU76'.
               05 WK01-ATTRIBUTES                PIC X(60)
                                                     VALUE SPACE.
               05 WK01-ATTRIBUTES-ERROR          PIC X(60) VALUE
                   'U  BIF'.
               05 WK01-CURSOR-DFLD               PIC X(36)
                                                     VALUE SPACES.
               05 WK01-CURSOR-DFLD-QUAL          PIC X(30)
                                                     VALUE SPACES.
               05 WK01-ROW                       PIC S9(4) COMP
                                                     VALUE ZERO.
               05 WK01-COLUMN                    PIC S9(4) COMP
                                                     VALUE ZERO.
               05 WK01-ERROR-MSG                 PIC X(80)
                                                     VALUE SPACES.
               05 WK01-MAP-DATA-LEN              PIC S9(6)
                                                     VALUE 1229.
               05 WK01-1ST-FLAG                  PIC X(1)
                                                     VALUE 'Y'.
               05 WK01-FLAG-OUTPUT-DATA          PIC X(1)
                                                     VALUE 'Y'.
               05 WK01-ALL-BUT-FLAG              PIC X(1)
                                                     VALUE SPACES.
               05 WK01-FLAG-ALL-FIELDS-CHANGED   PIC X(1)  VALUE 'Y'.
               05 WK01-FLAG-ANY-FIELDS-CHANGED   PIC X(1)  VALUE 'N'.
                  88 ANY-FIELDS-CHANGED          VALUE 'Y'.
               05 WK01-FLAG-ALL-FIELDS-ERROR     PIC X(1)  VALUE 'Y'.
               05 WK01-FLAG-ANY-FIELDS-ERROR     PIC X(1)  VALUE 'N'.
               05 WK01-FLAG-ALL-FIELDS-ERASE     PIC X(1)  VALUE 'Y'.
               05 WK01-FLAG-ANY-FIELDS-ERASE     PIC X(1)  VALUE 'Y'.
               05 WK01-FLAG-START-DATE-CHANGED   PIC X(1)  VALUE 'Y'.
               05 WK01-PASSED-ONE-F              PIC X(1)  VALUE SPACE.
               05 WK01-VARDATA                   PIC X(80)
                                                     VALUE SPACES.
           04  WK01-ELEMENT-AREA.
               05  UCA-PMKEY-FIELD-SAVED.
                   07 UNIV-W-IND-SAVED           PIC X.                 00003210
                   07 ACCT-NUMBER-SAVED          PIC X(6).              00003130
                   07 FSCL-YR-SAVED              PIC X(02).             00003080
               05 FSCL-YR-STATUS-SAVED           PIC X.                 00003040
               05 FSCL-YR-STATUS-DESC-SAVED      PIC X(14).             00003080
               05 FUND-NAME-SAVED                PIC X(35).             00003170
               05 ACCT-NBR-SAVED                 PIC X(6).              00003250
               05 IFIS-NBR-DESC-SAVED            PIC X(35).             00003300
               05 INTRL-ENTY-CODE-SAVED          PIC X(4).              00003340
               05 ACCT-GRP-CD-SAVED              PIC X(6).              00003390
               05 ARC-SAVED                      PIC X(6).              00003440
               05 ARC-DESC-SAVED                 PIC X(35).             00003440
               05 UAS-SAVED                      PIC X(6).              00003480
               05 UAS-DESC-SAVED                 PIC X(30).             00003480
               05 UAS-ACAD-CODE-SAVED            PIC X(3).              00003520
               05 NFS-CODE-SAVED                 PIC X(3).              00003560
               05 MARINE-SCI-IND                 PIC X(1).              00003610
               05 SAU-SAVED                      PIC X(1).              00003660
               05 PROGRAM-CODE-SAVED             PIC X(6).              00003710
               05 ORG-UN-LVL1-SAVED              PIC X(6).              00003750
               05 ORG-UN-LVL2-SAVED              PIC X(6).              00003750
               05 ORG-UN-LVL3-SAVED              PIC X(6).              00003750
               05 ORG-UN-NA-LVL1-SAVED           PIC X(30).             00003750
               05 ORG-UN-NA-LVL2-SAVED           PIC X(30).             00003750
               05 ORG-UN-NA-LVL3-SAVED           PIC X(30).             00003750
