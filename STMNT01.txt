      ***************************************************************** 01350000
      ** THIS COPYLIB IS USED BY UEXU200 AND UEXU300 PROGRAMS ********* 01350000
      ** THE DD STATMENT IS STMNT01.                          ********* 01350000
      ** THERE ARE TWO TYPES OF RECORD IN THIS FILE LAYOUT    ********* 01350000
      ** HDR-RECORD - EXTR-REC-TYPE = '02'                    ********* 01350000
      ** DTL-RECORD - EXTR-REC-TYPE = '03'                    ********* 01350000
      ***************************************************************** 01350000
       01  EXTRACT-RECORD.                                              01350000
           03  EXTRACT-KEY.                                             01360000
               05  EXTR-MAIL-CODE              PIC X(05).               01370000
               05  EXTR-VNDR-CODE              PIC X(09).               01380000
               05  EXTR-VNDR-NAME              PIC X(35).               01390000
               05  EXTR-CMDTY-CODE             PIC X(08).               01400000
               05  EXTR-CMDTY-DESC             PIC X(35).               01410000
               05  EXTR-FILE-NMBR              PIC 9(05).               01420000
               05  EXTR-ORGN-FILE-NMBR         PIC 9(05).               01420000
               05  EXTR-FILE-DATE              PIC 9(08).               01430000
               05  EXTR-STMNT-START-DT         PIC 9(08).               01440000
               05  EXTR-STMNT-END-DT           PIC 9(08).               01450000
               05  EXTR-ACCT-INDX-CODE         PIC X(10).               01460000
               05  EXTR-RELEASE-NMBR           PIC X(14).               01470000
               05  EXTR-BUYER-CODE             PIC X(04).               01480000
               05  EXTR-ORDERED-BY             PIC X(35).               01490000
               05  EXTR-ORDER-DATE             PIC 9(08).               01500000
               05  EXTR-COA-CODE               PIC X(01).               01510000
               05  EXTR-FUND-CODE              PIC X(06).               01520000
               05  EXTR-ORGZN-CODE             PIC X(06).               01530000
               05  EXTR-ACCT-CODE              PIC X(06).               01540000
               05  EXTR-PRGRM-CODE             PIC X(06).               01550000
               05  EXTR-ACTVY-CODE             PIC X(06).               01560000
               05  EXTR-LCTN-CODE              PIC X(06).               01570000
               05  EXTR-REC-TYPE               PIC X(02).               01580000
           03  EXTRACT-DATA                    PIC X(172).              01590047
           03  EXTRACT-FILE-DATA REDEFINES EXTRACT-DATA.                01600000
               05  TOT-AMT                     PIC S9(10)V99.           01610000
               05  TOT-VOL-AMT                 PIC S9(10)V99.           01620000
               05  TOT-SUB-AMT                 PIC S9(10)V99.           01630000
               05  TOT-TAX-AMT                 PIC S9(10)V99.           01640000
               05  TOT-ADDL-AMT                PIC S9(10)V99.           01650000
               05  TOT-DSCNT-AMT               PIC S9(10)V99.           01660000
               05  TOT-RLSE-AMT                PIC S9(10)V99.           01670000
               05  TOT-DBKEY                   PIC S9(08) COMP.         01680046
FCIJP *        05  FILLER                      PIC X(84).               01690046
FCIJP          05  TOT-DB2-DBKEY-4229          PIC X(35).
FCIJP          05  FILLER                      PIC X(49).
           03  EXTRACT-RLSE-DATA REDEFINES EXTRACT-DATA.                01700000
               05  RLSE-LINE-NMBR              PIC 9(04).               01670000
               05  RLSE-CATALOG-NMBR           PIC X(14).               01680000
               05  RLSE-CATALOG-DESC           PIC X(35).               01690000
               05  RLSE-PART-NMBR              PIC X(15).               01700000
               05  RLSE-QTY                    PIC 9(06).               01710000
               05  RLSE-UNIT-OF-MEA            PIC X(04).               01720000
               05  RLSE-UNIT-PRICE             PIC S9(10)V99.           01770000
               05  RLSE-TOTAL-AMT              PIC S9(10)V99.           01780000
               05  RLSE-MFR-NAME               PIC X(35).               01750000
               05  RLSE-MODEL-NMBR             PIC X(30).               01760000
               05  FILLER                      PIC X(05).               01810047
                                                                        01780000
