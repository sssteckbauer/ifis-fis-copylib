    ***Created by Convert/DC version V8R03 on 12/11/00 at 12:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE DCSD-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY01.
      *%--------------------------------------------------------------%*
           MOVE CNFDL-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0A.
      *%--------------------------------------------------------------%*
           MOVE HOLDS-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0B.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-TA09 OF FTAWM09 TO WK01-CODE-TA09.
      *%--------------------------------------------------------------%*
           MOVE DELETE-LIT-TA09 OF FTAWM09 TO WK01-LIT-TA09.
      *%--------------------------------------------------------------%*
           MOVE CONFIRM-DELETE-TA09 OF FTAWM09 TO WK01-DELETE-TA09.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-NMBR-4410-TA09 OF FTAWK09 TO WK01-NMBR-4410-TA09.
      *%--------------------------------------------------------------%*
           MOVE CHECK-LIT-TA09 OF FTAWM09 TO WK01-LIT-TA0A.
      *%--------------------------------------------------------------%*
           MOVE CONFIRM-CHECK-TA09 OF FTAWM09 TO WK01-CHECK-TA09.
      *%--------------------------------------------------------------%*
           MOVE VNDR-ID-LAST-NINE-4073-TA09 OF FTAWM09 TO
                WK01-ID-LAST-NIN-4073-T09.
      *%--------------------------------------------------------------%*
           MOVE NAME-KEY-6117-TA09 OF FTAWM09 TO WK01-KEY-6117-TA09.
      *%--------------------------------------------------------------%*
           MOVE OPEN-PAID-HOLD-IND-4410-TA09 OF FTAWM09 TO
                WK01-PAD-HLD-IND-4410-T09.
      *%--------------------------------------------------------------%*
           MOVE INVD-AMT-4410-TA09 OF FTAWM09 TO WK01-AMT-4410-TA09.
      *%--------------------------------------------------------------%*
           MOVE VNDR-INV-NMBR-4410-TA09 OF FTAWM09 TO
                WK01-INV-NMBR-4410-TA09.
      *%--------------------------------------------------------------%*
           MOVE CRDT-MEMO-4410-TA09 OF FTAWM09 TO WK01-MEMO-4410-TA09.
      *%--------------------------------------------------------------%*
           MOVE INV-DATE-4410-TA09 OF FTAWM09 TO WK01-DATE-4410-TA09.
      *%--------------------------------------------------------------%*
           MOVE PYMT-DUE-DATE-4410-TA09 OF FTAWM09 TO
                WK01-DUE-DATE-4410-TA09.
      *%--------------------------------------------------------------%*
           MOVE BILL-TYPE-DESC-TA09 OF FTAWM09 TO WK01-TYPE-DESC-TA09.
      *%--------------------------------------------------------------%*
           MOVE BANK-ACCT-CODE-4033-TA09 OF FTAWM09 TO
                WK01-ACCT-CODE-4033-TA09.
      *%--------------------------------------------------------------%*
           MOVE BANK-CODE-DESC-4062-TA09 OF FTAWM09 TO
                WK01-CODE-DESC-4062-TA09.
      *%--------------------------------------------------------------%*
           MOVE ADR-TYPE-CODE-6139-TA09 OF FTAWM09 TO
                WK01-TYPE-CODE-6139-TA09.
      *%--------------------------------------------------------------%*
           MOVE SHORT-DESC-6137-TA09 OF FTAWM09 TO WK01-DESC-6137-TA09.
      *%--------------------------------------------------------------%*
           MOVE APRVL-IND-4410-TA09 OF FTAWM09 TO WK01-IND-4410-TA09.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-6139-TA09 OF FTAWM09(0001) TO
                WK01-ADR-6139-TA091.
      *%--------------------------------------------------------------%*
           MOVE CNCL-IND-4410-TA09 OF FTAWM09 TO WK01-IND-4410-TA0A.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-6139-TA09 OF FTAWM09(0002) TO
                WK01-ADR-6139-TA092.
      *%--------------------------------------------------------------%*
           MOVE CNCL-DATE-4410-TA09 OF FTAWM09 TO WK01-DATE-4410-TA0A.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-6139-TA09 OF FTAWM09(0003) TO
                WK01-ADR-6139-TA093.
      *%--------------------------------------------------------------%*
           MOVE HDR-ERROR-IND-4410-TA09 OF FTAWM09 TO
                WK01-ERROR-IND-4410-TA09.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-6139-TA09 OF FTAWM09(0004) TO
                WK01-ADR-6139-TA094.
      *%--------------------------------------------------------------%*
           IF HDR-CNTR-DTL-4410-TA09 OF FTAWM09 NOT NUMERIC
              MOVE HDR-CNTR-DTL-4410-TA09 OF FTAWM09 TO
                WK01-CNTR-DTL-4410-TA09-X
           ELSE
              MOVE HDR-CNTR-DTL-4410-TA09 OF FTAWM09 TO
                WK01-CNTR-DTL-4410-TA09
           END-IF.
      *%--------------------------------------------------------------%*
           MOVE CITY-NAME-6139-TA09 OF FTAWM09 TO WK01-NAME-6139-TA09.
      *%--------------------------------------------------------------%*
           MOVE STATE-CODE-6151-TA09 OF FTAWM09 TO WK01-CODE-6151-TA09.
      *%--------------------------------------------------------------%*
           MOVE ZIP-CODE-6152-TA09 OF FTAWM09 TO WK01-CODE-6152-TA09.
      *%--------------------------------------------------------------%*
           MOVE CNTRY-CODE-6153-TA09 OF FTAWM09 TO WK01-CODE-6153-TA09.
      *%--------------------------------------------------------------%*
           MOVE CMPLT-IND-4410-TA09 OF FTAWM09 TO WK01-IND-4410-TA0B.
      *%--------------------------------------------------------------%*
           MOVE CHECK-GRPNG-IND-4410-TA09 OF FTAWM09 TO
                WK01-GRPNG-IND-4410-TA09.
      *%--------------------------------------------------------------%*
           MOVE BILL-TYPE-IND-4410-TA09 OF FTAWM09 TO
                WK01-TYPE-IND-4410-TA09.
      *%--------------------------------------------------------------%*
           MOVE VNDR-INV-DATE-4410-TA09 OF FTAWM09 TO
                WK01-INV-DATE-4410-TA09.
      *%--------------------------------------------------------------%*
           MOVE APRVL-TMPLT-CODE-TA09 OF FTAWM09 TO
                WK01-TMPLT-CODE-TA09.
      *%--------------------------------------------------------------%*
           MOVE ACH-ADR-TYPE-TA09 OF FTAWM09 TO WK01-ADR-TYPE-TA09.
      *%--------------------------------------------------------------%*
           MOVE CHECK-TEXT-IND-TA09 OF FTAWM09 TO WK01-TEXT-IND-TA09.
      *%--------------------------------------------------------------%*
           MOVE DOCT-TEXT-IND-TA09 OF FTAWM09 TO WK01-TEXT-IND-TA0A.
      *%--------------------------------------------------------------%*
           MOVE CHECK-NMBR-TA09 OF FTAWM09 TO WK01-NMBR-TA09.
      *%--------------------------------------------------------------%*
           MOVE CHECK-DATE-TA09 OF FTAWM09 TO WK01-DATE-TA09.
      *%--------------------------------------------------------------%*
           MOVE CHECK-STATUS-TA09 OF FTAWM09 TO WK01-STATUS-TA09.
