      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWK40                              04/24/00  12:25  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWK40.
           02  RQST-COA-CODE-CH40                       PIC X(1).
           02  RQST-ORGZN-CODE-CH40                     PIC X(6).
           02  USER-ID-CH40                             PIC X(8).
           02  RQST-ORGZN-TYPE-CODE-CH40                PIC X(2).
