      *----------------------------------------------------------------*
      *    LAYOUT FOR: FAPUK05O - EXTRACT RECORD LAYOUT CREATED BY     *
      *                           PROGRAM FAPUK05                      *
      *    NOTE:                                                       *
      *        ANY MODIFICATIONS/ADDITIONS TO THIS COPY BOOK MUST TAKE *
      *        PROGRAM FAPUK05, FAPUK06.                               *
      *                                                                *
      *                         INTO CONSIDERATION                     *
      *----------------------------------------------------------------*
       01  FAPUK05O-RECORD.
           03  FAPUK05O-SORT-KEY.
               05  FAPUK05O-FIXED-KEY.
                   07  FAPUK05O-UNVRS-CODE        PICTURE X(002).
                   07  FAPUK05O-PROGRAM-NAME      PICTURE X(008).
                   07  FAPUK05O-USER-ID           PICTURE X(008).
                   07  FAPUK05O-ORGN-CODE         PICTURE X(004).
               05  FAPUK05O-VARIABLE-KEY          PICTURE X(012).
               05  FAPUK05O-OPTION-1 REDEFINES FAPUK05O-VARIABLE-KEY.
                   07  FAPUK05O-DCMNT-NMBR-1      PICTURE X(008).
                   07  FILLER                     PICTURE X(004).
               05  FAPUK05O-REC-TYPE              PICTURE X(001).
           03  FAPUK05O-RECORD-BODY.
               05  FAPUK05O-DCMNT-DATE            PICTURE 9(008).
               05  FAPUK05O-DCMNT-NMBR            PICTURE X(008).
               05  FAPUK05O-DCMNT-TYPE-SEQ-NMBR   PICTURE 9(004).
               05  FAPUK05O-BANK-CODE             PICTURE X(002).
               05  FILLER                         PICTURE X(002).


      *-----------------------  END OF FAPUK05O  ----------------------*
