      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM91                              04/24/00  12:21  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM91.
           02  COA-CODE-4001-CH91                       PIC X(1).
           02  FUND-CODE-4001-CH91                      PIC X(6).
           02  FUND-TITLE-4005-CH91                     PIC X(35).
           02  PREDCSR-CODE-4005-CH91                   PIC X(6).
           02  PREDCSR-TITLE-4005-CH91                  PIC X(35).
           02  HRCHY-TITLE-4012-CH91        OCCURS 5    PIC X(20).
           02  HRCHY-FUND-4001-CH91         OCCURS 5    PIC X(6).
           02  HRCHY-FUND-TITLE-4005-CH91   OCCURS 5    PIC X(35).
