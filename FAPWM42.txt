      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWM42                              04/24/00  12:17  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWM42.
           02  ACTN-CODE-GROUP-AP42.
               03  ACTN-CODE-AP42           OCCURS 12   PIC X(1).
           02  SUB-DCMNT-TYPE-4169-AP42     OCCURS 12   PIC X(3).
           02  INV-CLS-DESC-4169-AP42       OCCURS 12   PIC X(25).
           02  CLAUSE-CODE-4169-AP42        OCCURS 12   PIC X(8).
           02  START-DATE-4169-AP42         OCCURS 12   PIC X(6).
           02  END-DATE-4169-AP42           OCCURS 12   PIC X(6).
           02  CMDTY-CODE-4169-AP42         OCCURS 12   PIC X(8).
           02  ORGN-CODE-4169-AP42          OCCURS 12   PIC X(4).
