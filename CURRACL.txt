      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4199-ACCT-LDGR' TO DBLINK-CURR-PARAGRAPH.    276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4199-ACCT-LDGR' TO RECORD-NAME.                       276 BRTN
YYY991     MOVE 'F-TRANSACTION' TO AREA-NAME.                           276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4199-ACCT-LDGR TO DBLINK-RECORD-MADE-CURRENT.   276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4199-ACCT-LDGR-EXIT                      276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE ACL-PKEY-TS TO DBLINK-R4199-ACCT-LDGR-01            276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4199-ACCT-LDGR TO DBLINK-VIEW-NAME-CURRENT.     276 BRTN
YYY991     MOVE DBLINK-R4199-ACCT-LDGR-KEY TO DBLINK-VIEW-200-CURRENT.  276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4199-4189                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4199-ACCT-LDGR-01 TO DBLINK-S-4199-4189-01.     276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4199-4189-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET R4199-CALC-SET                                              276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ACL-FSCL-CC TO DBLINK-R4199-CALC-SET-01.                276 BRTN
YYY991     MOVE ACL-FSCL-YR TO DBLINK-R4199-CALC-SET-02.                276 BRTN
YYY991     MOVE ACL-ACCT-INDX-CD TO DBLINK-R4199-CALC-SET-03.           276 BRTN
YYY991     MOVE ACL-FUND-CD TO DBLINK-R4199-CALC-SET-04.                276 BRTN
YYY991     MOVE ACL-ORGN-CD TO DBLINK-R4199-CALC-SET-05.                276 BRTN
YYY991     MOVE ACL-ACCT-CD TO DBLINK-R4199-CALC-SET-06.                276 BRTN
YYY991     MOVE ACL-PRGRM-CD TO DBLINK-R4199-CALC-SET-07.               276 BRTN
YYY991     MOVE ACL-ACTVY-CD TO DBLINK-R4199-CALC-SET-08.               276 BRTN
YYY991     MOVE ACL-LCTN-CD TO DBLINK-R4199-CALC-SET-09.                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-OL                                                   276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ACL-FSCL-CC TO DBLINK-S-INDX-OL-01.                     276 BRTN
YYY991     MOVE ACL-FSCL-YR TO DBLINK-S-INDX-OL-02.                     276 BRTN
YYY991     MOVE ACL-ACCT-INDX-CD TO DBLINK-S-INDX-OL-03.                276 BRTN
YYY991     MOVE ACL-FUND-CD TO DBLINK-S-INDX-OL-04.                     276 BRTN
YYY991     MOVE ACL-ORGN-CD TO DBLINK-S-INDX-OL-05.                     276 BRTN
YYY991     MOVE ACL-ACCT-CD TO DBLINK-S-INDX-OL-06.                     276 BRTN
YYY991     MOVE ACL-PRGRM-CD TO DBLINK-S-INDX-OL-07.                    276 BRTN
YYY991     MOVE ACL-ACTVY-CD TO DBLINK-S-INDX-OL-08.                    276 BRTN
YYY991     MOVE ACL-LCTN-CD TO DBLINK-S-INDX-OL-09.                     276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-OL1                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ACL-FSCL-CC TO DBLINK-S-INDX-OL1-01.                    276 BRTN
YYY991     MOVE ACL-FSCL-YR TO DBLINK-S-INDX-OL1-02.                    276 BRTN
YYY991     MOVE ACL-FUND-CD TO DBLINK-S-INDX-OL1-03.                    276 BRTN
YYY991     MOVE ACL-ORGN-CD TO DBLINK-S-INDX-OL1-04.                    276 BRTN
YYY991     MOVE ACL-ACCT-CD TO DBLINK-S-INDX-OL1-05.                    276 BRTN
YYY991     MOVE ACL-PRGRM-CD TO DBLINK-S-INDX-OL1-06.                   276 BRTN
YYY991     MOVE ACL-ACTVY-CD TO DBLINK-S-INDX-OL1-07.                   276 BRTN
YYY991     MOVE ACL-LCTN-CD TO DBLINK-S-INDX-OL1-08.                    276 BRTN
YYY991     MOVE ACL-ACCT-INDX-CD TO DBLINK-S-INDX-OL1-09.               276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-OL2                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ACL-FSCL-CC TO DBLINK-S-INDX-OL2-01.                    276 BRTN
YYY991     MOVE ACL-FSCL-YR TO DBLINK-S-INDX-OL2-02.                    276 BRTN
YYY991     MOVE ACL-FUND-CD TO DBLINK-S-INDX-OL2-03.                    276 BRTN
YYY991     MOVE ACL-ORGN-CD TO DBLINK-S-INDX-OL2-04.                    276 BRTN
YYY991     MOVE ACL-PRGRM-CD TO DBLINK-S-INDX-OL2-05.                   276 BRTN
YYY991     MOVE ACL-ACCT-CD TO DBLINK-S-INDX-OL2-06.                    276 BRTN
YYY991     MOVE ACL-ACTVY-CD TO DBLINK-S-INDX-OL2-07.                   276 BRTN
YYY991     MOVE ACL-LCTN-CD TO DBLINK-S-INDX-OL2-08.                    276 BRTN
YYY991     MOVE ACL-ACCT-INDX-CD TO DBLINK-S-INDX-OL2-09.               276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-OL3                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ACL-FSCL-CC TO DBLINK-S-INDX-OL3-01.                    276 BRTN
YYY991     MOVE ACL-FSCL-YR TO DBLINK-S-INDX-OL3-02.                    276 BRTN
YYY991     MOVE ACL-ORGN-CD TO DBLINK-S-INDX-OL3-03.                    276 BRTN
YYY991     MOVE ACL-FUND-CD TO DBLINK-S-INDX-OL3-04.                    276 BRTN
YYY991     MOVE ACL-ACCT-CD TO DBLINK-S-INDX-OL3-05.                    276 BRTN
YYY991     MOVE ACL-PRGRM-CD TO DBLINK-S-INDX-OL3-06.                   276 BRTN
YYY991     MOVE ACL-ACTVY-CD TO DBLINK-S-INDX-OL3-07.                   276 BRTN
YYY991     MOVE ACL-LCTN-CD TO DBLINK-S-INDX-OL3-08.                    276 BRTN
YYY991     MOVE ACL-ACCT-INDX-CD TO DBLINK-S-INDX-OL3-09.               276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-OL4                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ACL-FSCL-CC TO DBLINK-S-INDX-OL4-01.                    276 BRTN
YYY991     MOVE ACL-FSCL-YR TO DBLINK-S-INDX-OL4-02.                    276 BRTN
YYY991     MOVE ACL-ORGN-CD TO DBLINK-S-INDX-OL4-03.                    276 BRTN
YYY991     MOVE ACL-FUND-CD TO DBLINK-S-INDX-OL4-04.                    276 BRTN
YYY991     MOVE ACL-PRGRM-CD TO DBLINK-S-INDX-OL4-05.                   276 BRTN
YYY991     MOVE ACL-ACCT-CD TO DBLINK-S-INDX-OL4-06.                    276 BRTN
YYY991     MOVE ACL-ACTVY-CD TO DBLINK-S-INDX-OL4-07.                   276 BRTN
YYY991     MOVE ACL-LCTN-CD TO DBLINK-S-INDX-OL4-08.                    276 BRTN
YYY991     MOVE ACL-ACCT-INDX-CD TO DBLINK-S-INDX-OL4-09.               276 BRTN
