       01  MAP-CONTROL.

    ***Created by Convert/DC version V8R03 on 01/04/01 at 09:00***

           02  FUTMU29-MAP-CONTROL.
           03  FUTMU29T.
           04  WK01-DCXMAPL-PARMS.
               10  WK01-MAPNAME                  PIC X(8)
                                                 VALUE 'FUTMU29I'.
               10  WK01-IKE-LENGTH               PIC S9(8)  COMP
DMW001                                           VALUE 12286.
               10  WK01-TINA-LENGTH              PIC S9(4)  COMP
                                                 VALUE 1153.
           05  WK01-ATTRIBUTES                   PIC X(60)
                                                 VALUE SPACE.
           05  WK01-ATTRIBUTES-ERROR             PIC X(60) VALUE
               'U  BIF'.
           05  WK01-CURSOR-DFLD                  PIC X(36)
                                                 VALUE SPACES.
           05  WK01-CURSOR-DFLD-QUAL             PIC X(30)
                                                 VALUE SPACES.
           05  WK01-ROW                          PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-COLUMN                       PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-ERROR-MSG                    PIC X(80)
                                                 VALUE SPACES.
           05  WK01-MAP-DATA-LEN                 PIC S9(6)
                                                 VALUE ZERO.
           05  WK01-1ST-FLAG                     PIC X(1)
                                                 VALUE 'Y'.
           05  WK01-FLAG-OUTPUT-DATA             PIC X(1)
                                                 VALUE 'Y'.
      *    05  WK01-STATUS-RETURN-FLAG           PIC X(1)
      *                                          VALUE SPACES.
           05  WK01-ALL-BUT-FLAG                 PIC X(1)
                                                 VALUE SPACES.
           05  WK01-FLAG-ALL-FIELDS-CHANGED      PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-CHANGED      PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERROR        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERROR        PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.

           05  WK01-VARDATA                      PIC X(11880)
                                                 VALUE SPACES.
           05  FILLER                            PIC X(100)
                                                 VALUE SPACES.
           04  WK01-ELEMENT-AREA.

               10  WK01-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-6001-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00       PIC X(35)
                                               VALUE SPACES.
               10  WK01-DATE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-HH-MM-TIME-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00       PIC X(4)
                                               VALUE SPACES.
               10  WK01-PM-FLAG-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-USER-ID-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID       PIC X(32)
                                               VALUE SPACES.
               10  WK01-RSPNS-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-FUNC-DESCRIPTION-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION       PIC X(28)
                                               VALUE SPACES.
               10  WK01-STATUS-CODE-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-ID-UT02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-UT02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-UT02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-UT02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-UT02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-UT02       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-UT02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT02       PIC X(35)
                                               VALUE SPACES.
               10  WK01-TYPE-UT02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TYPE-UT02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TYPE-UT02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TYPE-UT02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TYPE-UT02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TYPE-UT02       PIC X(3)
                                               VALUE SPACES.
               10  WK01-DESC-UT02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-UT02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-UT02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-UT02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-UT02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-UT02       PIC X(35)
                                               VALUE SPACES.
               10  WK01-TMPLT-CODE-UT02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-CODE-UT02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-CODE-UT02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-CODE-UT02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-CODE-UT02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-CODE-UT02       PIC X(3)
                                               VALUE SPACES.
               10  WK01-TMPLT-DESC-UT02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-DESC-UT02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-DESC-UT02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-DESC-UT02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-DESC-UT02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-DESC-UT02       PIC X(35)
                                               VALUE SPACES.
               10  WK01-CODE-UT021-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-UT021-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-UT021-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-UT021-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-UT021-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-UT021       PIC X(1)
                                               VALUE SPACES.
               10  WK01-NMBR-UT021-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-UT021-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-UT021-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-UT021-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-UT021-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-UT021       PIC X(4)
                                               VALUE SPACES.
               10  WK01-ID-UT0A11-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-UT0A11-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-UT0A11-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-UT0A11-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-UT0A11-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-UT0A11       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-UT0A11-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A11-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A11-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A11-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A11-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A11       PIC X(35)
                                               VALUE SPACES.
               10  WK01-LIMIT-UT29-Z             PIC X(1)
                                               VALUE SPACE.
               10  WK01-LIMIT-UT29-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LIMIT-UT29-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LIMIT-UT29-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LIMIT-UT29-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LIMIT-UT29       PIC X(16)
                                               VALUE SPACES.
               10  WK01-APRVL-CODE-UT021-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT021-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT021-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT021-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT021-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT021       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-UT0B11-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0B11-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0B11-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0B11-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0B11-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0B11       PIC X(35)
                                               VALUE SPACES.
               10  WK01-APRVL-CODE-UT022-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT022-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT022-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT022-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT022-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT022       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-UT0A12-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A12-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A12-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A12-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A12-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A12       PIC X(35)
                                               VALUE SPACES.
               10  WK01-APRVL-CODE-UT023-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT023-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT023-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT023-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT023-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT023       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-UT0A13-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A13-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A13-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A13-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A13-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A13       PIC X(35)
                                               VALUE SPACES.
               10  WK01-APRVL-CODE-UT024-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT024-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT024-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT024-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT024-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT024       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-UT0A14-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A14-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A14-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A14-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A14-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A14       PIC X(35)
                                               VALUE SPACES.
               10  WK01-APRVL-CODE-UT025-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT025-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT025-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT025-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT025-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT025       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-UT0A15-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A15-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A15-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A15-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A15-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A15       PIC X(35)
                                               VALUE SPACES.
               10  WK01-APRVL-CODE-UT026-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT026-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT026-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT026-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT026-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT026       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-UT0A16-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A16-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A16-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A16-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A16-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A16       PIC X(35)
                                               VALUE SPACES.
               10  WK01-APRVL-CODE-UT027-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT027-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT027-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT027-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT027-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT027       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-UT0A17-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A17-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A17-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A17-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A17-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A17       PIC X(35)
                                               VALUE SPACES.
               10  WK01-APRVL-CODE-UT028-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT028-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT028-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT028-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT028-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT028       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-UT0A18-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A18-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A18-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A18-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A18-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A18       PIC X(35)
                                               VALUE SPACES.
               10  WK01-APRVL-CODE-UT029-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT029-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT029-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT029-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT029-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-APRVL-CODE-UT029       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-UT0A19-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A19-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A19-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A19-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A19-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-UT0A19       PIC X(35)
                                               VALUE SPACES.
               10  WK01-ZZMESSAGE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE       PIC X(160)
                                               VALUE SPACES.
               10  WK01-PASSED-ONE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE       PIC X(32)
                                               VALUE SPACES.
               10  WK01-MAP-RESPONSE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-TEXT-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00       PIC X(14)
                                               VALUE SPACES.
           04  WK01-SUBSCRIPTED-WORK-AREA.
               05  FILLER                      PIC X(1).
               05  WK01-APRVL-CODE-UT02-F      PIC X(1)
                                               OCCURS 9.
               05  WK01-NAME-UT0A1-F             PIC X(1)
                                               OCCURS 9.
