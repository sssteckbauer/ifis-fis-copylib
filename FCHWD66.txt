      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWD66                              04/24/00  12:57  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWD66.
           02  WORK-SLCTN-DATE-CH66           COMP-3    PIC S9(8).
           02  WORK-SYSTEM-DATE-CH66          COMP-3    PIC S9(8).
           02  WORK-SAVE-DBKEY-ID-4035-CH66
                                     COMP   OCCURS 11   PIC S9(8).
           02  WORK-SAVE-DBKEY-ID-4063-CH66
                                     COMP   OCCURS 11   PIC S9(8).
           02  WORK-LOOP-FLAG-CH66                      PIC X(1).
           02  WORK-SAVE-DBKEY-CH66           COMP      PIC S9(8).
           02  ACTN-FLAG-CH66                           PIC X(1).
           02  WORK-ADD-FLAG-CH66                       PIC X(1).
           02  WORK-LIST-FLAG-CH66                      PIC X(1).
           02  WORK-SEL-FLAG-CH66                       PIC X(1).
           02  SEL-FLAG-CH66                            PIC X(1).
