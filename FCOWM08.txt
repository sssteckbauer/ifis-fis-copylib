      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCOWM08                              04/24/00  12:28  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCOWM08.
           02  COMM-VIEW-6257-CO08                      PIC X(8).
           02  LONG-DESC-6257-CO08                      PIC X(30).
           02  COMM-TYPE-6258-CO08                      PIC X(4).
           02  SHORT-DESC-6258-CO08                     PIC X(10).
           02  COMM-PLAN-CODE-6259-CO08                 PIC X(4).
           02  SHORT-DESC-6259-CO08                     PIC X(10).
           02  SHORT-DESC-6260-CO08                     PIC X(10).
           02  ACTN-CODE-GROUP-CO08.
               03  ACTN-CODE-CO08                       PIC X(1).
           02  LINE-LGTH-6260-CO08                      PIC X(3).
FCAMH      02  LINE-LGTH-6260-CO08-R
FCAMH          REDEFINES LINE-LGTH-6260-CO08            PIC 9(3).
           02  LINES-PER-PAGE-6260-CO08                 PIC X(3).
FCAMH      02  LINES-PER-PAGE-6260-CO08-R
FCAMH          REDEFINES LINES-PER-PAGE-6260-CO08       PIC 9(3).
           02  TOP-MARGIN-6260-CO08                     PIC X(2).
FCAMH      02  TOP-MARGIN-6260-CO08-R
FCAMH          REDEFINES TOP-MARGIN-6260-CO08           PIC 9(2).
           02  LABEL-ID-6260-CO08                       PIC X(1).
           02  VERIFY-FLAG-6260-CO08                    PIC X(1).
           02  COMM-TEXT-6261-CO08          OCCURS 10   PIC X(79).
           02  COPY-LINE-LGTH-CO08                      PIC X(3).
           02  COPY-LINES-PER-PAGE-CO08                 PIC X(3).
           02  COPY-TOP-MARGIN-CO08                     PIC X(2).
           02  COPY-TEXT-CODE-CO08                      PIC X(4).
