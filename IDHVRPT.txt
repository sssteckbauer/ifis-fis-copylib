       976-FROM-R4060-RPT SECTION.

FCCCE**        MOVE USER-CODE-4060 OF R4060-RPT
FCCCE          MOVE DBLINK-USER-CD
               TO RPT-USER-CD.

               MOVE LAST-ACTVY-DATE-4060 OF R4060-RPT
               TO RPT-LAST-ACTVY-DT.

               MOVE UNVRS-CODE-4060 OF R4060-RPT
               TO RPT-UNVRS-CD.

               MOVE RPRT-CODE-4060 OF R4060-RPT
               TO RPT-RPRT-CD.

           IF ACTVY-DATE-4060 OF R4060-RPT  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO RPT-ACTVY-DT
           ELSE
               MOVE ACTVY-DATE-4060 OF R4060-RPT
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO RPT-ACTVY-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO RPT-ACTVY-DT
               END-IF
           END-IF.

               MOVE RPRT-TITLE-4060 OF R4060-RPT
               TO RPT-RPRT-TITLE.

           IF NMBR-OF-COPIES-4060 OF R4060-RPT  NOT NUMERIC
               MOVE ZEROS TO RPT-NBR-OF-COPIES
           ELSE
               MOVE NMBR-OF-COPIES-4060 OF R4060-RPT
               TO RPT-NBR-OF-COPIES
           END-IF.
       976-FROM-R4060-RPT-EXIT.
           EXIT.
