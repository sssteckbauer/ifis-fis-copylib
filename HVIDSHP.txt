       975-TO-R4086-SHIP-TO SECTION.
           MOVE SHP-USER-CD TO
               USER-CODE-4086 OF R4086-SHIP-TO
                                                                     .
           MOVE SHP-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4086 OF R4086-SHIP-TO
                                                                     .
           MOVE SHP-UNVRS-CD TO
               UNVRS-CODE-4086 OF R4086-SHIP-TO
                                                                     .
           MOVE SHP-SHIP-TO-CD TO
               SHIP-TO-CODE-4086 OF R4086-SHIP-TO
                                                                     .
           MOVE SHP-ADR-1 TO
               ADR-1-4086 OF R4086-SHIP-TO
                                                                     .
           MOVE SHP-ADR-2 TO
               ADR-2-4086 OF R4086-SHIP-TO
                                                                     .
           MOVE SHP-ADR-3 TO
               ADR-3-4086 OF R4086-SHIP-TO
                                                                     .
           MOVE SHP-ADR-4 TO
               ADR-4-4086 OF R4086-SHIP-TO
                                                                     .
           MOVE SHP-CITY-NAME TO
               CITY-NAME-4086 OF R4086-SHIP-TO
                                                                     .
           MOVE SHP-STATE-CD TO
               STATE-CODE-4086 OF R4086-SHIP-TO
                                                                     .
           MOVE SHP-ZIP-CD TO
               ZIP-CODE-4086 OF R4086-SHIP-TO
                                                                     .
           MOVE SHP-CNTCT-NAME TO
               CNTCT-NAME-4086 OF R4086-SHIP-TO
                                                                     .
           MOVE SHP-TLPHN-ID TO
               TLPHN-ID-4086 OF R4086-SHIP-TO
                                                                     .
           MOVE SHP-ORGN-CD TO
               ORGZN-CODE-4086 OF R4086-SHIP-TO
                                                                     .
           MOVE SHP-ROUTE-CD TO
               ROUTE-CODE-4086 OF R4086-SHIP-TO
                                                                     .
           MOVE SHP-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACTVY-DATE-4086 OF R4086-SHIP-TO
                                                                     .
           MOVE SHP-CNTRY-CD TO
               CNTRY-CODE-4086 OF R4086-SHIP-TO
                                                                     .
           MOVE SHP-COA-CD TO
               COA-CODE-4086 OF R4086-SHIP-TO
                                                                     .
           MOVE SHP-SHIP-TYP-CD TO
               SHIP-TYPE-CODE-4086 OF R4086-SHIP-TO
                                                                     .
           MOVE SHP-CNTCT-NM-UPPER TO
               CNTCT-NAME-UPPER-4086 OF R4086-SHIP-TO
                                                                     .
           MOVE SHP-START-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               START-DATE-4086 OF R4086-SHIP-TO
                                                                     .
           MOVE SHP-END-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               END-DATE-4086 OF R4086-SHIP-TO
                                                                     .
       975-TO-R4086-SHIP-TO-EXIT.
           EXIT.
