    ***Created by Convert/DC version V8R03 on 11/20/00 at 12:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY01-Z = 'Y'
               MOVE WK01-TEXT-SY01 TO DCSD-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0A-Z = 'Y'
               MOVE WK01-TEXT-SY0A TO CNFDL-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0B-Z = 'Y'
               MOVE WK01-TEXT-SY0B TO HOLDS-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT071-Z = 'Y'
               MOVE WK01-CODE-UT071 TO ACTN-CODE-UT07 OF FUTWM07(0001)
           END-IF.
           MOVE WK01-CODE-UT071-F TO WK01-CODE-UT07-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-UT071-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-UT071 TO
                ADR-TYPE-CODE-6137-UT07 OF FUTWM07(0001)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-UT071-F TO
                WK01-TYPE-CODE-6137-UT07-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-UT071-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-UT071 TO
                DSPLY-PRTY-SEQ-6137-UT07 OF FUTWM07(0001)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-UT071-F TO WK01-PRTY-SEQ-6137-UT07-F
                (1).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT071-Z = 'Y'
               MOVE WK01-DESC-6137-UT071 TO SHORT-DESC-6137-UT07 OF
                FUTWM07(0001)
           END-IF.
           MOVE WK01-DESC-6137-UT071-F TO WK01-DESC-6137-UT07-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT0A1-Z = 'Y'
               MOVE WK01-DESC-6137-UT0A1 TO LONG-DESC-6137-UT07 OF
                FUTWM07(0001)
           END-IF.
           MOVE WK01-DESC-6137-UT0A1-F TO WK01-DESC-6137-UT0A-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT072-Z = 'Y'
               MOVE WK01-CODE-UT072 TO ACTN-CODE-UT07 OF FUTWM07(0002)
           END-IF.
           MOVE WK01-CODE-UT072-F TO WK01-CODE-UT07-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT073-Z = 'Y'
               MOVE WK01-CODE-UT073 TO ACTN-CODE-UT07 OF FUTWM07(0003)
           END-IF.
           MOVE WK01-CODE-UT073-F TO WK01-CODE-UT07-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT074-Z = 'Y'
               MOVE WK01-CODE-UT074 TO ACTN-CODE-UT07 OF FUTWM07(0004)
           END-IF.
           MOVE WK01-CODE-UT074-F TO WK01-CODE-UT07-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT075-Z = 'Y'
               MOVE WK01-CODE-UT075 TO ACTN-CODE-UT07 OF FUTWM07(0005)
           END-IF.
           MOVE WK01-CODE-UT075-F TO WK01-CODE-UT07-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT076-Z = 'Y'
               MOVE WK01-CODE-UT076 TO ACTN-CODE-UT07 OF FUTWM07(0006)
           END-IF.
           MOVE WK01-CODE-UT076-F TO WK01-CODE-UT07-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT077-Z = 'Y'
               MOVE WK01-CODE-UT077 TO ACTN-CODE-UT07 OF FUTWM07(0007)
           END-IF.
           MOVE WK01-CODE-UT077-F TO WK01-CODE-UT07-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT078-Z = 'Y'
               MOVE WK01-CODE-UT078 TO ACTN-CODE-UT07 OF FUTWM07(0008)
           END-IF.
           MOVE WK01-CODE-UT078-F TO WK01-CODE-UT07-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT079-Z = 'Y'
               MOVE WK01-CODE-UT079 TO ACTN-CODE-UT07 OF FUTWM07(0009)
           END-IF.
           MOVE WK01-CODE-UT079-F TO WK01-CODE-UT07-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT0710-Z = 'Y'
               MOVE WK01-CODE-UT0710 TO ACTN-CODE-UT07 OF FUTWM07(0010)
           END-IF.
           MOVE WK01-CODE-UT0710-F TO WK01-CODE-UT07-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT0711-Z = 'Y'
               MOVE WK01-CODE-UT0711 TO ACTN-CODE-UT07 OF FUTWM07(0011)
           END-IF.
           MOVE WK01-CODE-UT0711-F TO WK01-CODE-UT07-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT0712-Z = 'Y'
               MOVE WK01-CODE-UT0712 TO ACTN-CODE-UT07 OF FUTWM07(0012)
           END-IF.
           MOVE WK01-CODE-UT0712-F TO WK01-CODE-UT07-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-UT072-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-UT072 TO
                ADR-TYPE-CODE-6137-UT07 OF FUTWM07(0002)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-UT072-F TO
                WK01-TYPE-CODE-6137-UT07-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-UT073-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-UT073 TO
                ADR-TYPE-CODE-6137-UT07 OF FUTWM07(0003)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-UT073-F TO
                WK01-TYPE-CODE-6137-UT07-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-UT074-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-UT074 TO
                ADR-TYPE-CODE-6137-UT07 OF FUTWM07(0004)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-UT074-F TO
                WK01-TYPE-CODE-6137-UT07-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-UT075-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-UT075 TO
                ADR-TYPE-CODE-6137-UT07 OF FUTWM07(0005)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-UT075-F TO
                WK01-TYPE-CODE-6137-UT07-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-UT076-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-UT076 TO
                ADR-TYPE-CODE-6137-UT07 OF FUTWM07(0006)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-UT076-F TO
                WK01-TYPE-CODE-6137-UT07-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-UT077-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-UT077 TO
                ADR-TYPE-CODE-6137-UT07 OF FUTWM07(0007)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-UT077-F TO
                WK01-TYPE-CODE-6137-UT07-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-UT078-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-UT078 TO
                ADR-TYPE-CODE-6137-UT07 OF FUTWM07(0008)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-UT078-F TO
                WK01-TYPE-CODE-6137-UT07-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-UT079-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-UT079 TO
                ADR-TYPE-CODE-6137-UT07 OF FUTWM07(0009)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-UT079-F TO
                WK01-TYPE-CODE-6137-UT07-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-UT0710-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-UT0710 TO
                ADR-TYPE-CODE-6137-UT07 OF FUTWM07(0010)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-UT0710-F TO
                WK01-TYPE-CODE-6137-UT07-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-UT0711-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-UT0711 TO
                ADR-TYPE-CODE-6137-UT07 OF FUTWM07(0011)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-UT0711-F TO
                WK01-TYPE-CODE-6137-UT07-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-UT0712-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-UT0712 TO
                ADR-TYPE-CODE-6137-UT07 OF FUTWM07(0012)
           END-IF.
           MOVE WK01-TYPE-CODE-6137-UT0712-F TO
                WK01-TYPE-CODE-6137-UT07-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-UT072-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-UT072 TO
                DSPLY-PRTY-SEQ-6137-UT07 OF FUTWM07(0002)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-UT072-F TO WK01-PRTY-SEQ-6137-UT07-F
                (2).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-UT073-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-UT073 TO
                DSPLY-PRTY-SEQ-6137-UT07 OF FUTWM07(0003)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-UT073-F TO WK01-PRTY-SEQ-6137-UT07-F
                (3).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-UT074-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-UT074 TO
                DSPLY-PRTY-SEQ-6137-UT07 OF FUTWM07(0004)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-UT074-F TO WK01-PRTY-SEQ-6137-UT07-F
                (4).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-UT075-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-UT075 TO
                DSPLY-PRTY-SEQ-6137-UT07 OF FUTWM07(0005)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-UT075-F TO WK01-PRTY-SEQ-6137-UT07-F
                (5).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-UT076-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-UT076 TO
                DSPLY-PRTY-SEQ-6137-UT07 OF FUTWM07(0006)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-UT076-F TO WK01-PRTY-SEQ-6137-UT07-F
                (6).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-UT077-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-UT077 TO
                DSPLY-PRTY-SEQ-6137-UT07 OF FUTWM07(0007)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-UT077-F TO WK01-PRTY-SEQ-6137-UT07-F
                (7).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-UT078-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-UT078 TO
                DSPLY-PRTY-SEQ-6137-UT07 OF FUTWM07(0008)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-UT078-F TO WK01-PRTY-SEQ-6137-UT07-F
                (8).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-UT079-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-UT079 TO
                DSPLY-PRTY-SEQ-6137-UT07 OF FUTWM07(0009)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-UT079-F TO WK01-PRTY-SEQ-6137-UT07-F
                (9).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-UT0710-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-UT0710 TO
                DSPLY-PRTY-SEQ-6137-UT07 OF FUTWM07(0010)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-UT0710-F TO
                WK01-PRTY-SEQ-6137-UT07-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-UT0711-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-UT0711 TO
                DSPLY-PRTY-SEQ-6137-UT07 OF FUTWM07(0011)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-UT0711-F TO
                WK01-PRTY-SEQ-6137-UT07-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-PRTY-SEQ-6137-UT0712-Z = 'Y'
               MOVE WK01-PRTY-SEQ-6137-UT0712 TO
                DSPLY-PRTY-SEQ-6137-UT07 OF FUTWM07(0012)
           END-IF.
           MOVE WK01-PRTY-SEQ-6137-UT0712-F TO
                WK01-PRTY-SEQ-6137-UT07-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT072-Z = 'Y'
               MOVE WK01-DESC-6137-UT072 TO SHORT-DESC-6137-UT07 OF
                FUTWM07(0002)
           END-IF.
           MOVE WK01-DESC-6137-UT072-F TO WK01-DESC-6137-UT07-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT073-Z = 'Y'
               MOVE WK01-DESC-6137-UT073 TO SHORT-DESC-6137-UT07 OF
                FUTWM07(0003)
           END-IF.
           MOVE WK01-DESC-6137-UT073-F TO WK01-DESC-6137-UT07-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT074-Z = 'Y'
               MOVE WK01-DESC-6137-UT074 TO SHORT-DESC-6137-UT07 OF
                FUTWM07(0004)
           END-IF.
           MOVE WK01-DESC-6137-UT074-F TO WK01-DESC-6137-UT07-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT075-Z = 'Y'
               MOVE WK01-DESC-6137-UT075 TO SHORT-DESC-6137-UT07 OF
                FUTWM07(0005)
           END-IF.
           MOVE WK01-DESC-6137-UT075-F TO WK01-DESC-6137-UT07-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT076-Z = 'Y'
               MOVE WK01-DESC-6137-UT076 TO SHORT-DESC-6137-UT07 OF
                FUTWM07(0006)
           END-IF.
           MOVE WK01-DESC-6137-UT076-F TO WK01-DESC-6137-UT07-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT077-Z = 'Y'
               MOVE WK01-DESC-6137-UT077 TO SHORT-DESC-6137-UT07 OF
                FUTWM07(0007)
           END-IF.
           MOVE WK01-DESC-6137-UT077-F TO WK01-DESC-6137-UT07-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT078-Z = 'Y'
               MOVE WK01-DESC-6137-UT078 TO SHORT-DESC-6137-UT07 OF
                FUTWM07(0008)
           END-IF.
           MOVE WK01-DESC-6137-UT078-F TO WK01-DESC-6137-UT07-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT079-Z = 'Y'
               MOVE WK01-DESC-6137-UT079 TO SHORT-DESC-6137-UT07 OF
                FUTWM07(0009)
           END-IF.
           MOVE WK01-DESC-6137-UT079-F TO WK01-DESC-6137-UT07-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT0710-Z = 'Y'
               MOVE WK01-DESC-6137-UT0710 TO SHORT-DESC-6137-UT07 OF
                FUTWM07(0010)
           END-IF.
           MOVE WK01-DESC-6137-UT0710-F TO WK01-DESC-6137-UT07-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT0711-Z = 'Y'
               MOVE WK01-DESC-6137-UT0711 TO SHORT-DESC-6137-UT07 OF
                FUTWM07(0011)
           END-IF.
           MOVE WK01-DESC-6137-UT0711-F TO WK01-DESC-6137-UT07-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT0712-Z = 'Y'
               MOVE WK01-DESC-6137-UT0712 TO SHORT-DESC-6137-UT07 OF
                FUTWM07(0012)
           END-IF.
           MOVE WK01-DESC-6137-UT0712-F TO WK01-DESC-6137-UT07-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT0A2-Z = 'Y'
               MOVE WK01-DESC-6137-UT0A2 TO LONG-DESC-6137-UT07 OF
                FUTWM07(0002)
           END-IF.
           MOVE WK01-DESC-6137-UT0A2-F TO WK01-DESC-6137-UT0A-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT0A3-Z = 'Y'
               MOVE WK01-DESC-6137-UT0A3 TO LONG-DESC-6137-UT07 OF
                FUTWM07(0003)
           END-IF.
           MOVE WK01-DESC-6137-UT0A3-F TO WK01-DESC-6137-UT0A-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT0A4-Z = 'Y'
               MOVE WK01-DESC-6137-UT0A4 TO LONG-DESC-6137-UT07 OF
                FUTWM07(0004)
           END-IF.
           MOVE WK01-DESC-6137-UT0A4-F TO WK01-DESC-6137-UT0A-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT0A5-Z = 'Y'
               MOVE WK01-DESC-6137-UT0A5 TO LONG-DESC-6137-UT07 OF
                FUTWM07(0005)
           END-IF.
           MOVE WK01-DESC-6137-UT0A5-F TO WK01-DESC-6137-UT0A-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT0A6-Z = 'Y'
               MOVE WK01-DESC-6137-UT0A6 TO LONG-DESC-6137-UT07 OF
                FUTWM07(0006)
           END-IF.
           MOVE WK01-DESC-6137-UT0A6-F TO WK01-DESC-6137-UT0A-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT0A7-Z = 'Y'
               MOVE WK01-DESC-6137-UT0A7 TO LONG-DESC-6137-UT07 OF
                FUTWM07(0007)
           END-IF.
           MOVE WK01-DESC-6137-UT0A7-F TO WK01-DESC-6137-UT0A-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT0A8-Z = 'Y'
               MOVE WK01-DESC-6137-UT0A8 TO LONG-DESC-6137-UT07 OF
                FUTWM07(0008)
           END-IF.
           MOVE WK01-DESC-6137-UT0A8-F TO WK01-DESC-6137-UT0A-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT0A9-Z = 'Y'
               MOVE WK01-DESC-6137-UT0A9 TO LONG-DESC-6137-UT07 OF
                FUTWM07(0009)
           END-IF.
           MOVE WK01-DESC-6137-UT0A9-F TO WK01-DESC-6137-UT0A-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT0A10-Z = 'Y'
               MOVE WK01-DESC-6137-UT0A10 TO LONG-DESC-6137-UT07 OF
                FUTWM07(0010)
           END-IF.
           MOVE WK01-DESC-6137-UT0A10-F TO WK01-DESC-6137-UT0A-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT0A11-Z = 'Y'
               MOVE WK01-DESC-6137-UT0A11 TO LONG-DESC-6137-UT07 OF
                FUTWM07(0011)
           END-IF.
           MOVE WK01-DESC-6137-UT0A11-F TO WK01-DESC-6137-UT0A-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-UT0A12-Z = 'Y'
               MOVE WK01-DESC-6137-UT0A12 TO LONG-DESC-6137-UT07 OF
                FUTWM07(0012)
           END-IF.
           MOVE WK01-DESC-6137-UT0A12-F TO WK01-DESC-6137-UT0A-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-TYPE-CODE-UT07-Z = 'Y'
               MOVE WK01-ADR-TYPE-CODE-UT07 TO RQST-ADR-TYPE-CODE-UT07
                OF FUTWK07
           END-IF.
