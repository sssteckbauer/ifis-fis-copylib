       975-TO-R4228-FILE-CNTL SECTION.
           MOVE FIL-USER-CD TO
               USER-CODE-4228 OF R4228-FILE-CNTL
                                                                     .
           MOVE FIL-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4228 OF R4228-FILE-CNTL
                                                                     .
           MOVE FIL-VNDR-ID-DGT-ONE TO
               VNDR-ID-DIGIT-ONE-4228 OF R4228-FILE-CNTL
                                                                     .
           MOVE FIL-VNDR-ID-LST-NINE TO
               VNDR-ID-LAST-NINE-4228 OF R4228-FILE-CNTL
                                                                     .
           MOVE FIL-CMDTY-CD TO
               CMDTY-CODE-4228 OF R4228-FILE-CNTL
                                                                     .
           MOVE FIL-FILE-NBR TO
               FILE-NMBR-4228 OF R4228-FILE-CNTL
                                                                     .
           MOVE FIL-FILE-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               FILE-DATE-4228 OF R4228-FILE-CNTL
                                                                     .
           MOVE FIL-STMNT-START-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               STMNT-START-DATE-4228 OF R4228-FILE-CNTL
                                                                     .
           MOVE FIL-STMNT-END-DAT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               STMNT-END-DATE-4228 OF R4228-FILE-CNTL
                                                                     .
           MOVE FIL-HDR-ERROR-IND TO
               HDR-ERROR-IND-4228 OF R4228-FILE-CNTL
                                                                     .
           MOVE FIL-DTL-ERROR-IND TO
               DTL-ERROR-IND-4228 OF R4228-FILE-CNTL
                                                                     .
           MOVE FIL-RUN-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               RUN-DATE-4228 OF R4228-FILE-CNTL
                                                                     .
           MOVE FIL-RUN-TIME TO
               DBLINKX-INPUT-PARM
           MOVE '4' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-TIME-FORMAT
           MOVE '51' TO DBLINKX-IDMS-TIME-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-PARM TO
               RUN-TIME-4228 OF R4228-FILE-CNTL
                                                                     .
           MOVE FIL-PRCSD-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               PRCSD-DATE-4228 OF R4228-FILE-CNTL
                                                                     .
           MOVE FIL-PRCSD-TIME TO
               DBLINKX-INPUT-PARM
           MOVE '4' TO DBLINKX-REQUEST-CODE
           MOVE '51' TO DBLINKX-IDMS-TIME-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-PARM TO
               PRCSD-TIME-4228 OF R4228-FILE-CNTL
                                                                     .
           MOVE FIL-HASH-TOTAL TO
               HASH-TOTAL-4228 OF R4228-FILE-CNTL
                                                                     .
           MOVE FIL-TOTAL-NBR-REC TO
               TOTAL-NMBR-REC-4228 OF R4228-FILE-CNTL
                                                                     .
       975-TO-R4228-FILE-CNTL-EXIT.
           EXIT.
