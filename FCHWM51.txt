      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM51                              04/24/00  12:25  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM51.
           02  COST-SHARE-DESC-4064-CH51                PIC X(35).
           02  COST-SHARE-BASIS-4064-CH51               PIC X(1).
           02  COST-SHARE-MEMO-IND-4064-CH51            PIC X(1).
           02  APPLY-TO-BASIS-IND-4064-CH51             PIC X(1).
           02  COST-SHARE-STNDD-PCT-4064-CH51           PIC X(7).
           02  ACTN-CODE-GROUP-CH51.
               03  ACTN-CODE-CH51           OCCURS 9    PIC X(1).
           02  CNTRB-PCT-4031-CH51          OCCURS 9    PIC X(7).
           02  ACCT-INDX-CODE-4031-CH51     OCCURS 9    PIC X(10).
           02  FUND-CODE-4031-CH51          OCCURS 9    PIC X(6).
           02  ORGZN-CODE-4031-CH51         OCCURS 9    PIC X(6).
           02  ACCT-CODE-4031-CH51          OCCURS 9    PIC X(6).
           02  PRGRM-CODE-4031-CH51         OCCURS 9    PIC X(6).
           02  ACTVY-CODE-4031-CH51         OCCURS 9    PIC X(6).
           02  LCTN-CODE-4031-CH51          OCCURS 9    PIC X(6).
           02  COST-SHARE-CRDT-ACCT-4031-CH51
                                            OCCURS 9    PIC X(6).
           02  CNTRB-TOTAL-PCT-4031-CH51                PIC X(7).
           02  CMPLT-IND-4064-CH51                      PIC X(1).
