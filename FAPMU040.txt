    ***Created by Convert/DC version V8R03 on 12/04/00 at 13:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP04-Z = 'Y'
               MOVE WK01-CODE-AP04 TO ACTN-CODE-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-4150-AP04-Z = 'Y'
               MOVE WK01-TYPE-CODE-4150-AP04 TO ADR-TYPE-CODE-4150-AP04
                OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4150-AP04-Z = 'Y'
               MOVE WK01-IND-4150-AP04 TO 1099-IND-4150-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-AP04-Z = 'Y'
               MOVE WK01-AMT-AP04 TO APRVD-AMT-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-AP0A-Z = 'Y'
               MOVE WK01-AMT-AP0A TO DSCNT-AMT-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-AP0B-Z = 'Y'
               MOVE WK01-AMT-AP0B TO TAX-AMT-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-AP0C-Z = 'Y'
               MOVE WK01-AMT-AP0C TO NET-AMT-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4150-AP0A-Z = 'Y'
               MOVE WK01-IND-4150-AP0A TO CMPLT-IND-4150-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-AP04-Z = 'Y'
               MOVE WK01-NAME-AP04 TO CITY-NAME-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4161-AP04-Z = 'Y'
               MOVE WK01-CODE-4161-AP04 TO DSCNT-CODE-4161-AP04 OF
                FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RATE-CODE-4150-AP04-Z = 'Y'
               MOVE WK01-RATE-CODE-4150-AP04 TO TAX-RATE-CODE-4150-AP04
                OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4150-AP04-Z = 'Y'
               MOVE WK01-NMBR-4150-AP04 TO DCMNT-NMBR-4150-AP04 OF
                FAPWK04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GP-AP04-Z = 'Y'
               MOVE WK01-NAME-GP-AP04 TO VNDR-NAME-GP-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INV-NMBR-4150-AP04-Z = 'Y'
               MOVE WK01-INV-NMBR-4150-AP04 TO VNDR-INV-NMBR-4150-AP04
                OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MEMO-IND-4150-AP04-Z = 'Y'
               MOVE WK01-MEMO-IND-4150-AP04 TO CRDT-MEMO-IND-4150-AP04
                OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-IND-AP04-Z = 'Y'
               MOVE WK01-TEXT-IND-AP04 TO CHECK-TEXT-IND-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP0A-Z = 'Y'
               MOVE WK01-CODE-AP0A TO STATE-CODE-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP0B-Z = 'Y'
               MOVE WK01-CODE-AP0B TO ZIP-CODE-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4150-AP0A-Z = 'Y'
               MOVE WK01-NMBR-4150-AP0A TO PO-NMBR-4150-AP04 OF FAPWK04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NN-4073-AP04-Z = 'Y'
               MOVE WK01-ID-LAST-NN-4073-AP04 TO
                VNDR-ID-LAST-NINE-4073-AP04 OF FAPWK04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-LIT-AP04-Z = 'Y'
               MOVE WK01-LIT-AP04 TO DELETE-LIT-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DELETE-AP04-Z = 'Y'
               MOVE WK01-DELETE-AP04 TO CONFIRM-DELETE-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4150-AP0B-Z = 'Y'
               MOVE WK01-IND-4150-AP0B TO CNCL-IND-4150-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INV-DATE-4150-AP04-Z = 'Y'
               MOVE WK01-INV-DATE-4150-AP04 TO VNDR-INV-DATE-4150-AP04
                OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-AP04-Z = 'Y'
               MOVE WK01-CLASS-CODE-AP04 TO PO-CLASS-CODE-AP04 OF
                FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-AP04-Z = 'Y'
               MOVE WK01-REF-NMBR-AP04 TO DCMNT-REF-NMBR-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RATE-DESC-AP04-Z = 'Y'
               MOVE WK01-RATE-DESC-AP04 TO TAX-RATE-DESC-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ADR-AP041-Z = 'Y'
               MOVE WK01-ADR-AP041 TO LINE-ADR-AP04 OF FAPWM04(0001)
           END-IF.
           MOVE WK01-ADR-AP041-F TO WK01-ADR-AP04-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-AP042-Z = 'Y'
               MOVE WK01-ADR-AP042 TO LINE-ADR-AP04 OF FAPWM04(0002)
           END-IF.
           MOVE WK01-ADR-AP042-F TO WK01-ADR-AP04-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-AP043-Z = 'Y'
               MOVE WK01-ADR-AP043 TO LINE-ADR-AP04 OF FAPWM04(0003)
           END-IF.
           MOVE WK01-ADR-AP043-F TO WK01-ADR-AP04-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP0C-Z = 'Y'
               MOVE WK01-CODE-AP0C TO CNTRY-CODE-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-IND-AP0A-Z = 'Y'
               MOVE WK01-TEXT-IND-AP0A TO DOCT-TEXT-IND-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP04-Z = 'Y'
               MOVE WK01-AMT-4150-AP04 TO INVD-AMT-4150-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-AP04-Z = 'Y'
               MOVE WK01-IND-AP04 TO LIQDTN-IND-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AP04-Z = 'Y'
               MOVE WK01-AP04 TO MAILCODE-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-AP04-Z = 'Y'
               MOVE WK01-INDX-CODE-AP04 TO ACCT-INDX-CODE-AP04 OF
                FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP0D-Z = 'Y'
               MOVE WK01-CODE-AP0D TO ACCT-CODE-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4150-AP04-Z = 'Y'
               MOVE WK01-DATE-4150-AP04 TO TRANS-DATE-4150-AP04 OF
                FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DUE-DATE-4150-AP04-Z = 'Y'
               MOVE WK01-DUE-DATE-4150-AP04 TO PYMT-DUE-DATE-4150-AP04
                OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-AP04-Z = 'Y'
               MOVE WK01-NMBR-AP04 TO CHECK-NMBR-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-AP04-Z = 'Y'
               MOVE WK01-DATE-AP04 TO ISSUE-DATE-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-QTY-4151-AP04-Z = 'Y'
               MOVE WK01-QTY-4151-AP04 TO APRVD-QTY-4151-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CHRG-4150-AP04-Z = 'Y'
               MOVE WK01-CHRG-4150-AP04 TO ADDL-CHRG-4150-AP04 OF
                FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4150-AP04-Z = 'Y'
               MOVE WK01-CODE-4150-AP04 TO ADJMT-CODE-4150-AP04 OF
                FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-UNIT-PRICE-4151-AP04-Z = 'Y'
               MOVE WK01-UNIT-PRICE-4151-AP04 TO
                APRVD-UNIT-PRICE-4151-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-AP04-Z = 'Y'
               MOVE WK01-STATUS-AP04 TO CHECK-STATUS-AP04 OF FAPWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FISCAL-YR-AP04-Z = 'Y'
               MOVE WK01-FISCAL-YR-AP04 TO FISCAL-YR-AP04 OF FAPWM04
           END-IF.
