       975-TO-R4161-DSCNT-TBLE SECTION.
           MOVE DSC-USER-CD TO
               USER-CODE-4161 OF R4161-DSCNT-TBLE
                                                                     .
           MOVE DSC-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4161 OF R4161-DSCNT-TBLE
                                                                     .
           MOVE DSC-UNVRS-CD TO
               UNVRS-CODE-4161 OF R4161-DSCNT-TBLE
                                                                     .
           MOVE DSC-DSCNT-CD TO
               DSCNT-CODE-4161 OF R4161-DSCNT-TBLE
                                                                     .
       975-TO-R4161-DSCNT-TBLE-EXIT.
           EXIT.
