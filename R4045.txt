       01  R4045-ACTI.
           02  DB-PROC-ID-4045.
               03  USER-CODE-4045                       PIC X(8).
               03  LAST-ACTVY-DATE-4045                 PIC X(5).
               03  TRMNL-ID-4045                        PIC X(8).
               03  PURGE-FLAG-4045                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  ACCT-INDX-KEY-4045.
               03  UNVRS-CODE-4045                      PIC X(2).
               03  COA-CODE-4045                        PIC X(1).
               03  ACCT-INDX-CODE-4045                  PIC X(10).
           02  ACTVY-DATE-4045                COMP-3    PIC S9(8).
