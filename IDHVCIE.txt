       976-FROM-R6262 SECTION.

FCYI **        MOVE USER-CODE-6262 OF R6262
FCYI           MOVE DBLINK-USER-CD
               TO CIE-USER-CD.

               MOVE LAST-ACTVY-DATE-6262 OF R6262
               TO CIE-LAST-ACTVY-DT.

               MOVE INSRT-ELMNT-ID-6262 OF R6262
               TO CIE-INSRT-ELMNT-ID.

               MOVE LONG-DESC-6262 OF R6262
               TO CIE-LONG-DESC.

               MOVE RCRD-TYPE-6262 OF R6262
               TO CIE-RCRD-TYP.

           IF CODE-PSTN-6262 OF R6262  NOT NUMERIC
               MOVE ZEROS TO CIE-CODE-PSTN
           ELSE
               MOVE CODE-PSTN-6262 OF R6262
               TO CIE-CODE-PSTN
           END-IF.

           IF CODE-LGTH-6262 OF R6262  NOT NUMERIC
               MOVE ZEROS TO CIE-CODE-LGTH
           ELSE
               MOVE CODE-LGTH-6262 OF R6262
               TO CIE-CODE-LGTH
           END-IF.

           IF CODE-OCCURS-6262 OF R6262  NOT NUMERIC
               MOVE ZEROS TO CIE-CODE-OCCURS
           ELSE
               MOVE CODE-OCCURS-6262 OF R6262
               TO CIE-CODE-OCCURS
           END-IF.

               MOVE CODE-TABLE-6262 OF R6262
               TO CIE-CODE-TABLE.

           IF SHORT-DESC-LGTH-6262 OF R6262  NOT NUMERIC
               MOVE ZEROS TO CIE-SHORT-DSC-LGTH
           ELSE
               MOVE SHORT-DESC-LGTH-6262 OF R6262
               TO CIE-SHORT-DSC-LGTH
           END-IF.

           IF LONG-DESC-LGTH-6262 OF R6262  NOT NUMERIC
               MOVE ZEROS TO CIE-LONG-DSC-LGTH
           ELSE
               MOVE LONG-DESC-LGTH-6262 OF R6262
               TO CIE-LONG-DSC-LGTH
           END-IF.

               MOVE FRMT-OPTN-6262 OF R6262 (01)
               TO CIE-FRMT-OPTN-01.

               MOVE FRMT-OPTN-6262 OF R6262 (02)
               TO CIE-FRMT-OPTN-02.

               MOVE FRMT-OPTN-6262 OF R6262 (03)
               TO CIE-FRMT-OPTN-03.

               MOVE FRMT-OPTN-6262 OF R6262 (04)
               TO CIE-FRMT-OPTN-04.

               MOVE FRMT-OPTN-6262 OF R6262 (05)
               TO CIE-FRMT-OPTN-05.

               MOVE FRMT-OPTN-6262 OF R6262 (06)
               TO CIE-FRMT-OPTN-06.

               MOVE FRMT-OPTN-6262 OF R6262 (07)
               TO CIE-FRMT-OPTN-07.

               MOVE FRMT-OPTN-6262 OF R6262 (08)
               TO CIE-FRMT-OPTN-08.

               MOVE FRMT-OPTN-6262 OF R6262 (09)
               TO CIE-FRMT-OPTN-09.

               MOVE FRMT-OPTN-6262 OF R6262 (10)
               TO CIE-FRMT-OPTN-10.

               MOVE DATA-TYPE-6262 OF R6262
               TO CIE-DATA-TYP.
       976-FROM-R6262-EXIT.
           EXIT.
