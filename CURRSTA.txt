      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R6151-STATE' TO DBLINK-CURR-PARAGRAPH.        276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R6151-STATE' TO RECORD-NAME.                           276 BRTN
YYY991     MOVE 'F-GEO' TO AREA-NAME.                                   276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6151-STATE TO DBLINK-RECORD-MADE-CURRENT.       276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R6151-STATE-EXIT                          276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE STA-CNTRY-CD TO DBLINK-R6151-STATE-01               276 BRTN
YYY991         MOVE STA-STATE-CD TO DBLINK-R6151-STATE-02               276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6151-STATE TO DBLINK-VIEW-NAME-CURRENT.         276 BRTN
YYY991     MOVE DBLINK-R6151-STATE-KEY TO DBLINK-VIEW-200-CURRENT.      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-6151-6152                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6151-STATE-01 TO DBLINK-S-6151-6152-01.         276 BRTN
YYY991     MOVE DBLINK-R6151-STATE-02 TO DBLINK-S-6151-6152-02.         276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-6151-6152-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-6001-6151                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE STA-FK-UNV-UNVRS-CD TO DBLINK-S-6001-6151-01.           276 BRTN
YYY991     MOVE STA-FK-UNV-UNVRS-CD TO DBLINK-S-6001-6151-02.           276 BRTN
YYY991     MOVE STA-CNTRY-CD TO DBLINK-S-6001-6151-03.                  276 BRTN
YYY991     MOVE STA-STATE-CD TO DBLINK-S-6001-6151-04.                  276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-6153-6151                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE STA-FK-CTY-CNTRY-CD TO DBLINK-S-6153-6151-01.           276 BRTN
YYY991     MOVE STA-STATE-CD TO DBLINK-S-6153-6151-02.                  276 BRTN
