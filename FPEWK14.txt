      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPEWK14                              04/24/00  12:34  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPEWK14.
           02  WORK-PRSN-ID-PE14.
               03  WORK-PRSN-ID-ONE-PE14                PIC X(3).
               03  WORK-PRSN-ID-TWO-PE14                PIC X(2).
               03  WORK-PRSN-ID-THREE-PE14              PIC X(4).
           02  RQST-ADR-TYPE-CODE-6137-PE14             PIC X(2).
