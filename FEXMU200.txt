    ***Created by Convert/DC version V8R03 on 12/05/00 at 12:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-EX20-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-EX20 TO VNDR-ID-LAST-NINE-EX20 OF
                FEXWK20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-EX20-Z = 'Y'
               MOVE WK01-NAME-EX20 TO VNDR-NAME-EX20 OF FEXWK20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX20-Z = 'Y'
               MOVE WK01-CODE-EX20 TO CMDTY-CODE-EX20 OF FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-EX20-Z = 'Y'
               MOVE WK01-TYPE-CODE-EX20 TO ADR-TYPE-CODE-EX20 OF FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-EX20-Z = 'Y'
               MOVE WK01-DESC-EX20 TO CMDTY-DESC-EX20 OF FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX2A-Z = 'Y'
               MOVE WK01-CODE-EX2A TO ACCT-CODE-EX20 OF FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-TITLE-EX20-Z = 'Y'
               MOVE WK01-CODE-TITLE-EX20 TO ACCT-CODE-TITLE-EX20 OF
                FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ADR-EX201-Z = 'Y'
               MOVE WK01-ADR-EX201 TO LINE-ADR-EX20 OF FEXWM20(0001)
           END-IF.
           MOVE WK01-ADR-EX201-F TO WK01-ADR-EX20-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-EX202-Z = 'Y'
               MOVE WK01-ADR-EX202 TO LINE-ADR-EX20 OF FEXWM20(0002)
           END-IF.
           MOVE WK01-ADR-EX202-F TO WK01-ADR-EX20-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-EX203-Z = 'Y'
               MOVE WK01-ADR-EX203 TO LINE-ADR-EX20 OF FEXWM20(0003)
           END-IF.
           MOVE WK01-ADR-EX203-F TO WK01-ADR-EX20-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-EX204-Z = 'Y'
               MOVE WK01-ADR-EX204 TO LINE-ADR-EX20 OF FEXWM20(0004)
           END-IF.
           MOVE WK01-ADR-EX204-F TO WK01-ADR-EX20-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-EX2A-Z = 'Y'
               MOVE WK01-NAME-EX2A TO CITY-NAME-EX20 OF FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX2B-Z = 'Y'
               MOVE WK01-CODE-EX2B TO STATE-CODE-EX20 OF FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX2C-Z = 'Y'
               MOVE WK01-CODE-EX2C TO ZIP-CODE-EX20 OF FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-EX20-Z = 'Y'
               MOVE WK01-CLASS-CODE-EX20 TO RULE-CLASS-CODE-EX20 OF
                FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RULE-CLASS-EX20-Z = 'Y'
               MOVE WK01-RULE-CLASS-EX20 TO TAX-RULE-CLASS-EX20 OF
                FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RULE-CLASS-EX2A-Z = 'Y'
               MOVE WK01-RULE-CLASS-EX2A TO ADDL-RULE-CLASS-EX20 OF
                FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RULE-CLASS-EX2B-Z = 'Y'
               MOVE WK01-RULE-CLASS-EX2B TO DSCNT-RULE-CLASS-EX20 OF
                FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-DESC-EX20-Z = 'Y'
               MOVE WK01-CLASS-DESC-EX20 TO RULE-CLASS-DESC-EX20 OF
                FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RULE-DESC-EX20-Z = 'Y'
               MOVE WK01-RULE-DESC-EX20 TO TAX-RULE-DESC-EX20 OF FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RULE-DESC-EX2A-Z = 'Y'
               MOVE WK01-RULE-DESC-EX2A TO ADDL-RULE-DESC-EX20 OF
                FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RULE-DESC-EX2B-Z = 'Y'
               MOVE WK01-RULE-DESC-EX2B TO DSCNT-RULE-DESC-EX20 OF
                FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX2D-Z = 'Y'
               MOVE WK01-CODE-EX2D TO ACTN-CODE-EX20 OF FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-EX20-Z = 'Y'
               MOVE WK01-IND-EX20 TO EQP-IND-EX20 OF FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-CR-EX20-Z = 'Y'
               MOVE WK01-CLASS-CODE-CR-EX20 TO RULE-CLASS-CODE-CR-EX20
                OF FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RULE-CLASS-CR-EX20-Z = 'Y'
               MOVE WK01-RULE-CLASS-CR-EX20 TO TAX-RULE-CLASS-CR-EX20
                OF FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-DESC-CR-EX20-Z = 'Y'
               MOVE WK01-CLASS-DESC-CR-EX20 TO RULE-CLASS-DESC-CR-EX20
                OF FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RULE-DESC-CR-EX20-Z = 'Y'
               MOVE WK01-RULE-DESC-CR-EX20 TO TAX-RULE-DESC-CR-EX20 OF
                FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RULE-CLASS-CR-EX2A-Z = 'Y'
               MOVE WK01-RULE-CLASS-CR-EX2A TO ADDL-RULE-CLASS-CR-EX20
                OF FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RULE-DESC-CR-EX2A-Z = 'Y'
               MOVE WK01-RULE-DESC-CR-EX2A TO ADDL-RULE-DESC-CR-EX20 OF
                FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RULE-CLASS-CR-EX2B-Z = 'Y'
               MOVE WK01-RULE-CLASS-CR-EX2B TO DSCNT-RULE-CLASS-CR-EX20
                OF FEXWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RULE-DESC-CR-EX2B-Z = 'Y'
               MOVE WK01-RULE-DESC-CR-EX2B TO DSCNT-RULE-DESC-CR-EX20
                OF FEXWM20
           END-IF.
