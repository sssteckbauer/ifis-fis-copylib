      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R6605' TO DBLINK-CURR-PARAGRAPH.              276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R6605' TO RECORD-NAME.                                 276 BRTN
YYY991     MOVE 'F-BATCHX' TO AREA-NAME.                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6605 TO DBLINK-RECORD-MADE-CURRENT.             276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R6605-EXIT                                276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE CBH-BATCH-USER-CD TO DBLINK-R6605-01                276 BRTN
YYY991         MOVE CBH-COMM-REF-NBR TO DBLINK-R6605-02                 276 BRTN
YYY991         MOVE CBH-TEXT-CD TO DBLINK-R6605-03                      276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6605 TO DBLINK-VIEW-NAME-CURRENT.               276 BRTN
YYY991     MOVE DBLINK-R6605-KEY TO DBLINK-VIEW-200-CURRENT.            276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-6605-6606                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6605-01 TO DBLINK-S-6605-6606-01.               276 BRTN
YYY991     MOVE DBLINK-R6605-02 TO DBLINK-S-6605-6606-02.               276 BRTN
YYY991     MOVE DBLINK-R6605-03 TO DBLINK-S-6605-6606-03.               276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-6605-6606-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-6605-6607                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6605-01 TO DBLINK-S-6605-6607-01.               276 BRTN
YYY991     MOVE DBLINK-R6605-02 TO DBLINK-S-6605-6607-02.               276 BRTN
YYY991     MOVE DBLINK-R6605-03 TO DBLINK-S-6605-6607-03.               276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-6605-6607-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-BATCH-NO                                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE CBH-BATCH-USER-CD TO DBLINK-S-INDX-BATCH-NO-01.         276 BRTN
YYY991     MOVE CBH-COMM-REF-NBR TO DBLINK-S-INDX-BATCH-NO-02.          276 BRTN
YYY991     MOVE CBH-TEXT-CD TO DBLINK-S-INDX-BATCH-NO-03.               276 BRTN
