       976-FROM-R6185-ENTY-ADR SECTION.

FCCCE**        MOVE USER-CODE-6185 OF R6185-ENTY-ADR
FCCCE          MOVE DBLINK-USER-CD
               TO ENA-USER-CD.

               MOVE LAST-ACTVY-DATE-6185 OF R6185-ENTY-ADR
               TO ENA-LAST-ACTVY-DT.

               MOVE ADR-TYPE-CODE-6185 OF R6185-ENTY-ADR
               TO ENA-ADR-TYP-CD.

           IF END-DATE-6185 OF R6185-ENTY-ADR  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO ENA-END-DT
           ELSE
               MOVE END-DATE-6185 OF R6185-ENTY-ADR
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO ENA-END-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO ENA-END-DT
               END-IF
           END-IF.

           IF START-DATE-6185 OF R6185-ENTY-ADR  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO ENA-START-DT
           ELSE
               MOVE START-DATE-6185 OF R6185-ENTY-ADR
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO ENA-START-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO ENA-START-DT
               END-IF
           END-IF.

               MOVE LINE-ADR-1-6185 OF R6185-ENTY-ADR
               TO ENA-LINE-ADR-1.

               MOVE LINE-ADR-2-6185 OF R6185-ENTY-ADR
               TO ENA-LINE-ADR-2.

               MOVE LINE-ADR-3-6185 OF R6185-ENTY-ADR
               TO ENA-LINE-ADR-3.

               MOVE LINE-ADR-4-6185 OF R6185-ENTY-ADR
               TO ENA-LINE-ADR-4.

               MOVE CITY-NAME-6185 OF R6185-ENTY-ADR
               TO ENA-CITY-NAME.

               MOVE TLPHN-ID-6185 OF R6185-ENTY-ADR
               TO ENA-TLPHN-ID.

               MOVE CNTY-CODE-6185 OF R6185-ENTY-ADR
               TO ENA-CNTY-CD.

               MOVE STATE-CODE-6185 OF R6185-ENTY-ADR
               TO ENA-STATE-CD.

               MOVE ZIP-CODE-6185 OF R6185-ENTY-ADR
               TO ENA-ZIP-CD.

               MOVE CNTRY-CODE-6185 OF R6185-ENTY-ADR
               TO ENA-FK-ZIP-CNTRY-CD.

               MOVE LINE-ADR-1-UC-6185 OF R6185-ENTY-ADR
               TO ENA-LINE-ADR-1-UC.

               MOVE FAX-TLPHN-ID-6185 OF R6185-ENTY-ADR
               TO ENA-FAX-TLPHN-ID.

               MOVE EMADR-LINE-6185 OF R6185-ENTY-ADR
               TO ENA-EMADR-LINE.

               MOVE FAX-STOP-6185 OF R6185-ENTY-ADR
               TO ENA-FAX-STOP.

SN6132         MOVE POSTAL-CODE-6185 OF R6185-ENTY-ADR
SN6132         TO ENA-POSTAL-CODE.

       976-FROM-R6185-ENTY-ADR-EXIT.
           EXIT.
