       975-TO-R4069-JRNL-HDR SECTION.
           MOVE JLH-USER-CD TO
               USER-CODE-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-UNVRS-CD TO
               UNVRS-CODE-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-DOC-NBR TO
               DCMNT-NMBR-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-RVRSL-IND TO
               RVRSL-IND-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACTVY-DATE-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-DOC-DESC TO
               DCMNT-DESC-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-DOC-AMT TO
               DCMNT-AMT-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-HDR-ERROR-IND TO
               HDR-ERROR-IND-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-HDR-CNTR-ACCT TO
               HDR-CNTR-ACCT-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-APRVL-IND TO
               APRVL-IND-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-PSTNG-IND TO
               PSTNG-IND-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-TRANS-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               TRANS-DATE-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-CMPLT-IND TO
               CMPLT-IND-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-DSTBN-IND TO
               DSTBN-IND-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-UPDT-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               UPDT-ACTVY-DATE-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-UPDT-TIME-STMP TO
               DBLINKX-INPUT-PARM
           MOVE '4' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-TIME-FORMAT
           MOVE '51' TO DBLINKX-IDMS-TIME-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-PARM TO
               UPDT-TIME-STAMP-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-JV-DEBIT-AMT TO
               JV-DEBIT-AMT-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-JV-CRDT-AMT TO
               JV-CRDT-AMT-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-SUBSYSTEM-ID TO
               SUBSYSTEM-ID-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-ACTG-PRD TO
               ACTG-PRD-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-SUB-DOC-TYP TO
               SUB-DCMNT-TYPE-4069 OF R4069-JRNL-HDR
                                                                     .
           MOVE JLH-REEDIT-STATUS TO
               REEDIT-STATUS-4069 OF R4069-JRNL-HDR
                                                                     .
       975-TO-R4069-JRNL-HDR-EXIT.
           EXIT.
