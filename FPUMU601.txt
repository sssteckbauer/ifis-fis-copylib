    ***Created by Convert/DC version V8R03 on 11/20/00 at 12:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE CLAUSE-CODE-4209-PU60 OF FPUWK60 TO WK01-CODE-4209-PU60.
      *%--------------------------------------------------------------%*
           MOVE CMNT-TEXT-4210-PU60 OF FPUWM60(0001) TO
                WK01-TEXT-4210-PU601.
      *%--------------------------------------------------------------%*
           MOVE CMNT-TEXT-4210-PU60 OF FPUWM60(0002) TO
                WK01-TEXT-4210-PU602.
      *%--------------------------------------------------------------%*
           MOVE CMNT-TEXT-4210-PU60 OF FPUWM60(0003) TO
                WK01-TEXT-4210-PU603.
      *%--------------------------------------------------------------%*
           MOVE CMNT-TEXT-4210-PU60 OF FPUWM60(0004) TO
                WK01-TEXT-4210-PU604.
      *%--------------------------------------------------------------%*
           MOVE CMNT-TEXT-4210-PU60 OF FPUWM60(0005) TO
                WK01-TEXT-4210-PU605.
      *%--------------------------------------------------------------%*
           MOVE CMNT-TEXT-4210-PU60 OF FPUWM60(0006) TO
                WK01-TEXT-4210-PU606.
      *%--------------------------------------------------------------%*
           MOVE CMNT-TEXT-4210-PU60 OF FPUWM60(0007) TO
                WK01-TEXT-4210-PU607.
      *%--------------------------------------------------------------%*
           MOVE CMNT-TEXT-4210-PU60 OF FPUWM60(0008) TO
                WK01-TEXT-4210-PU608.
      *%--------------------------------------------------------------%*
           MOVE CMNT-TEXT-4210-PU60 OF FPUWM60(0009) TO
                WK01-TEXT-4210-PU609.
      *%--------------------------------------------------------------%*
           MOVE CMNT-TEXT-4210-PU60 OF FPUWM60(0010) TO
                WK01-TEXT-4210-PU6010.
      *%--------------------------------------------------------------%*
           MOVE CMNT-TEXT-4210-PU60 OF FPUWM60(0011) TO
                WK01-TEXT-4210-PU6011.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4209-PU60 OF FPUWM60 TO WK01-DATE-4209-PU60.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4209-PU60 OF FPUWM60 TO WK01-DATE-4209-PU6A.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-DATE-4209-PU60 OF FPUWM60 TO WK01-DATE-4209-PU6B.
      *%--------------------------------------------------------------%*
           MOVE CLAUSE-DESC-4209-PU60 OF FPUWM60 TO WK01-DESC-4209-PU60.
