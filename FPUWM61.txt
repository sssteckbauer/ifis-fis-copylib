      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWM61                              04/24/00  12:37  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWM61.
           02  ACTN-CODE-GROUP-PU61.
               03  ACTN-CODE-PU61           OCCURS 13   PIC X(1).
           02  CLAUSE-CODE-4209-PU61        OCCURS 13   PIC X(8).
           02  CLAUSE-DESC-4209-PU61        OCCURS 13   PIC X(35).
           02  ACTVY-DATE-4209-PU61         OCCURS 13   PIC X(6).
           02  SAVE-DBKEY-ID-4209-PU61
DEVBWS                                      OCCURS 13   PIC X(8).
