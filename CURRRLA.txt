      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4182-RULE-ACTN' TO DBLINK-CURR-PARAGRAPH.    276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4182-RULE-ACTN' TO RECORD-NAME.                       276 BRTN
YYY991     MOVE 'F-RULE' TO AREA-NAME.                                  276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4182-RULE-ACTN TO DBLINK-RECORD-MADE-CURRENT.   276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4182-RULE-ACTN-EXIT                      276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE RLA-FK-RLE-RULE-CLS-CD TO DBLINK-R4182-RULE-ACTN-01 276 BRTN
YYY991         MOVE RLA-FK-RLE-START-DT TO DBLINK-R4182-RULE-ACTN-02    276 BRTN
YYY991         MOVE RLA-FK-RLE-TIME-STMP TO DBLINK-R4182-RULE-ACTN-03   276 BRTN
YYY991         MOVE RLA-SEQ-NBR TO DBLINK-R4182-RULE-ACTN-04            276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4182-RULE-ACTN TO DBLINK-VIEW-NAME-CURRENT.     276 BRTN
YYY991     MOVE DBLINK-R4182-RULE-ACTN-KEY TO DBLINK-VIEW-200-CURRENT.  276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4181-4182                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE RLA-FK-RLE-RULE-CLS-CD TO DBLINK-S-4181-4182-01.        276 BRTN
YYY991     MOVE RLA-FK-RLE-START-DT TO DBLINK-S-4181-4182-02.           276 BRTN
YYY991     MOVE RLA-FK-RLE-TIME-STMP TO DBLINK-S-4181-4182-03.          276 BRTN
YYY991     MOVE RLA-SEQ-NBR TO DBLINK-S-4181-4182-04.                   276 BRTN
