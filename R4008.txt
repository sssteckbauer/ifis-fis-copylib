       01  R4008-FNDT.
           02  DB-PROC-ID-4008.
               03  USER-CODE-4008                       PIC X(8).
               03  LAST-ACTVY-DATE-4008                 PIC X(5).
               03  TRMNL-ID-4008                        PIC X(8).
               03  PURGE-FLAG-4008                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  FUND-TYPE-KEY-4008.
               03  UNVRS-CODE-4008                      PIC X(2).
               03  COA-CODE-4008                        PIC X(1).
               03  FUND-TYPE-CODE-4008                  PIC X(2).
           02  ACTVY-DATE-4008                COMP-3    PIC S9(8).
           02  GOVNMT-OWNED-IND-4008                    PIC X(1).
