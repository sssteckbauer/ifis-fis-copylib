    ***Created by Convert/DC version V8R03 on 12/11/00 at 12:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE RPRT-CODE-4060-GA37 OF FGAWK37 TO WK01-CODE-4060-GA37.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-GA37 OF FGAWM37 TO WK01-CODE-GA37.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-DATE-4058-GA37 OF FGAWM37 TO WK01-DATE-4058-GA37.
      *%--------------------------------------------------------------%*
           MOVE FSCL-YR-4058-GA37 OF FGAWM37 TO WK01-YR-4058-GA37.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4058-GA37 OF FGAWM37 TO WK01-CODE-4058-GA37.
      *%--------------------------------------------------------------%*
           MOVE AS-OF-DATE-4058-GA37 OF FGAWM37 TO
                WK01-OF-DATE-4058-GA37.
      *%--------------------------------------------------------------%*
           MOVE ACRL-IND-4058-GA37 OF FGAWM37 TO WK01-IND-4058-GA37.
      *%--------------------------------------------------------------%*
           MOVE FUND-TYPE-CODE-4058-GA37 OF FGAWM37 TO
                WK01-TYPE-CODE-4058-GA37.
      *%--------------------------------------------------------------%*
           MOVE AGNCY-LAST-NINE-4058-GA37 OF FGAWM37 TO
                WK01-LAST-NINE-4058-GA37.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-CODE-4058-GA37 OF FGAWM37 TO
                WK01-MTHD-CODE-4058-GA37.
      *%--------------------------------------------------------------%*
           MOVE FUND-TYPE-LEVEL-IND-4058-GA37 OF FGAWM37 TO
                WK01-TYPE-LEVL-IND-4058-G37.
      *%--------------------------------------------------------------%*
           MOVE FUND-LEVEL-IND-4058-GA37 OF FGAWM37 TO
                WK01-LEVEL-IND-4058-GA37.
      *%--------------------------------------------------------------%*
           MOVE ACCT-LEVEL-IND-4058-GA37 OF FGAWM37 TO
                WK01-LEVEL-IND-4058-GA3A.
      *%--------------------------------------------------------------%*
           MOVE PRINT-RPRT-TOTAL-IND-4058-GA37 OF FGAWM37 TO
                WK01-RPRT-TOTL-IND-4058-G37.
      *%--------------------------------------------------------------%*
           MOVE PRINT-NET-TOTAL-IND-4058-GA37 OF FGAWM37 TO
                WK01-NET-TOTAL-IND-4058-G37.
      *%--------------------------------------------------------------%*
           MOVE DLT-IND-4058-GA37 OF FGAWM37 TO WK01-IND-4058-GA3A.
      *%--------------------------------------------------------------%*
           MOVE PRINT-ORGZN-TOTAL-4058-GA37 OF FGAWM37 TO
                WK01-ORGZN-TOTAL-4058-GA37.
      *%--------------------------------------------------------------%*
           MOVE PRINT-FUND-TOTAL-IND-4058-GA37 OF FGAWM37 TO
                WK01-FUND-TOTL-IND-4058-G37.
      *%--------------------------------------------------------------%*
           MOVE HOLD-IND-4058-GA37 OF FGAWM37 TO WK01-IND-4058-GA3B.
      *%--------------------------------------------------------------%*
           MOVE LEVEL-NMBR-4058-GA37 OF FGAWM37 TO WK01-NMBR-4058-GA37.
      *%--------------------------------------------------------------%*
           MOVE FUND-CODE-4058-GA37 OF FGAWM37 TO WK01-CODE-4058-GA3A.
      *%--------------------------------------------------------------%*
           MOVE ORGZN-CODE-4058-GA37 OF FGAWM37 TO WK01-CODE-4058-GA3B.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-4058-GA37 OF FGAWM37 TO WK01-CODE-4058-GA3C.
      *%--------------------------------------------------------------%*
           MOVE PRGRM-CODE-4058-GA37 OF FGAWM37 TO WK01-CODE-4058-GA3D.
      *%--------------------------------------------------------------%*
           MOVE LCTN-CODE-4058-GA37 OF FGAWM37 TO WK01-CODE-4058-GA3E.
      *%--------------------------------------------------------------%*
           MOVE PRGRM-LEVEL-IND-4058-GA37 OF FGAWM37 TO
                WK01-LEVEL-IND-4058-GA3B.
      *%--------------------------------------------------------------%*
           MOVE SEQ-NMBR-4058-GA37 OF FGAWM37 TO WK01-NMBR-4058-GA3A.
      *%--------------------------------------------------------------%*
           MOVE USER-ID-4058-GA37 OF FGAWK37 TO WK01-ID-4058-GA37.
      *%--------------------------------------------------------------%*
           MOVE RQST-SEQ-NMBR-GA37 OF FGAWK37 TO WK01-SEQ-NMBR-GA37.
      *%--------------------------------------------------------------%*
           MOVE XTRNL-ENTY-CODE-4058-GA37 OF FGAWM37 TO
                WK01-ENTY-CODE-4058-GA37.
      *%--------------------------------------------------------------%*
           MOVE LDGR-IND-4058-GA37 OF FGAWM37 TO WK01-IND-4058-GA3C.
      *%--------------------------------------------------------------%*
           MOVE XTRCT-IND-4058-GA37 OF FGAWM37 TO WK01-IND-4058-GA3D.
      *%--------------------------------------------------------------%*
           MOVE FROM-DSBRSMNT-DATE-4058-GA37 OF FGAWM37 TO
                WK01-DSBRSMNT-DATE-4058-G37.
      *%--------------------------------------------------------------%*
           MOVE TO-DSBRSMNT-DATE-4058-GA37 OF FGAWM37 TO
                WK01-DSBRSMNT-DATE-4058-G3A.
      *%--------------------------------------------------------------%*
           MOVE RPRT-TITLE-4060-GA37 OF FGAWM37 TO WK01-TITLE-4060-GA37.
