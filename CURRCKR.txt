      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4157-CHK-RQST-H' TO DBLINK-CURR-PARAGRAPH.   276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4157-CHK-RQST-H' TO RECORD-NAME.                      276 BRTN
YYY991     MOVE 'F-CHECKS' TO AREA-NAME.                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4157-CHK-RQST-H TO DBLINK-RECORD-MADE-CURRENT.  276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4157-CHK-RQST-H-EXIT                     276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE CKR-CHECK-RQST-ISEQ TO DBLINK-R4157-CHK-RQST-H-01   276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4157-CHK-RQST-H TO DBLINK-VIEW-NAME-CURRENT.    276 BRTN
YYY991     MOVE DBLINK-R4157-CHK-RQST-H-KEY TO DBLINK-VIEW-200-CURRENT. 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4157-4158                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4157-CHK-RQST-H-01 TO DBLINK-S-4157-4158-01.    276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4157-4158-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4157-4170                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4157-CHK-RQST-H-01 TO DBLINK-S-4157-4170-01.    276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4157-4170-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-RQSTR                                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE CKR-RQSTR-ID-DGT-ONE TO DBLINK-S-INDX-RQSTR-01.         276 BRTN
YYY991     MOVE CKR-RQSTR-ID-LST-NINE TO DBLINK-S-INDX-RQSTR-02.        276 BRTN
YYY991     MOVE CKR-SRVCD-DT TO DBLINK-S-INDX-RQSTR-03.                 276 BRTN
YYY991     MOVE CKR-IDX-RQSTR-TS TO DBLINK-S-INDX-RQSTR-04.             276 BRTN
