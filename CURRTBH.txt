      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R6600-TABL-HDR' TO DBLINK-CURR-PARAGRAPH.     276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R6600-TABL-HDR' TO RECORD-NAME.                        276 BRTN
YYY991     MOVE 'F-TABLEX' TO AREA-NAME.                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6600-TABL-HDR TO DBLINK-RECORD-MADE-CURRENT.    276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R6600-TABL-HDR-EXIT                       276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE TBH-APLCN-CD TO DBLINK-R6600-TABL-HDR-01            276 BRTN
YYY991         MOVE TBH-TABLE-CD TO DBLINK-R6600-TABL-HDR-02            276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6600-TABL-HDR TO DBLINK-VIEW-NAME-CURRENT.      276 BRTN
YYY991     MOVE DBLINK-R6600-TABL-HDR-KEY TO DBLINK-VIEW-200-CURRENT.   276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-6600-6601                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6600-TABL-HDR-01 TO DBLINK-S-6600-6601-01.      276 BRTN
YYY991     MOVE DBLINK-R6600-TABL-HDR-02 TO DBLINK-S-6600-6601-02.      276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-6600-6601-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-6600-6601-AC1                                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6600-TABL-HDR-01 TO DBLINK-S-6600-6601-AC1-01.  276 BRTN
YYY991     MOVE DBLINK-R6600-TABL-HDR-02 TO DBLINK-S-6600-6601-AC1-02.  276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-6600-6601-AC1-KEY-M.             276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-6600-6601-AC2                                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R6600-TABL-HDR-01 TO DBLINK-S-6600-6601-AC2-01.  276 BRTN
YYY991     MOVE DBLINK-R6600-TABL-HDR-02 TO DBLINK-S-6600-6601-AC2-02.  276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-6600-6601-AC2-KEY-M.             276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
