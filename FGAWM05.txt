      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FGAWM05                              04/24/00  12:30  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FGAWM05.
           02  FUND-TITLE-4005-GA05                     PIC X(35).
           02  BAL-ACCT-CODE-4003-GA05      OCCURS 10   PIC X(6).
           02  ACCT-CODE-TITLE-4034-GA05    OCCURS 10   PIC X(35).
           02  BEG-BAL-AMT-GA05             OCCURS 10   PIC X(16).
           02  CRNT-BAL-AMT-GA05            OCCURS 10   PIC X(17).
           02  TOT-DESC-GA05                            PIC X(35).
           02  TOT-BEG-BAL-AMT-GA05                     PIC X(16).
           02  TOT-CRD-BAL-AMT-GA05                     PIC X(17).
           02  BEG-BAL-DC-IND-GA05          OCCURS 10   PIC X(2).
           02  BEG-TOT-DC-IND-GA05                      PIC X(2).
           02  CRNT-BAL-DC-IND-GA05         OCCURS 10   PIC X(2).
           02  CRNT-TOT-DC-IND-GA05                     PIC X(2).
