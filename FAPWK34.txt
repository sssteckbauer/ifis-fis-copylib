      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWK34                              04/24/00  12:15  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWK34.
           02  RQSTR-ID-AP34.
               03  RQSTR-ID-DIGIT-ONE-AP34              PIC X(1).
               03  RQSTR-ID-LAST-NINE-AP34              PIC X(9).
           02  VNDR-NAME-AP34                           PIC X(35).
           02  WS-OUTSIDE-VENDOR-IND                    PIC X(01).
           02  WS-SYSTEM-ID                             PIC X(09).
           02  WS-FIMS-VENDOR-IND                       PIC X(1).
