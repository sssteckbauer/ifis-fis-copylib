      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4064-CS-EFCTV' TO DBLINK-CURR-PARAGRAPH.     276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4064-CS-EFCTV' TO RECORD-NAME.                        276 BRTN
YYY991     MOVE 'F-COST-SHARE' TO AREA-NAME.                            276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4064-CS-EFCTV TO DBLINK-RECORD-MADE-CURRENT.    276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4064-CS-EFCTV-EXIT                       276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE CSE-FK-CSH-COST-SHR-CD TO DBLINK-R4064-CS-EFCTV-01  276 BRTN
YYY991         MOVE CSE-START-DT TO DBLINK-R4064-CS-EFCTV-02            276 BRTN
YYY991         MOVE CSE-TIME-STMP TO DBLINK-R4064-CS-EFCTV-03           276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4064-CS-EFCTV TO DBLINK-VIEW-NAME-CURRENT.      276 BRTN
YYY991     MOVE DBLINK-R4064-CS-EFCTV-KEY TO DBLINK-VIEW-200-CURRENT.   276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4064-4031                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4064-CS-EFCTV-01 TO DBLINK-S-4064-4031-01.      276 BRTN
YYY991     MOVE DBLINK-R4064-CS-EFCTV-02 TO DBLINK-S-4064-4031-02.      276 BRTN
YYY991     MOVE DBLINK-R4064-CS-EFCTV-03 TO DBLINK-S-4064-4031-03.      276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4064-4031-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4064-4032                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4064-CS-EFCTV-01 TO DBLINK-S-4064-4032-01.      276 BRTN
YYY991     MOVE DBLINK-R4064-CS-EFCTV-02 TO DBLINK-S-4064-4032-02.      276 BRTN
YYY991     MOVE DBLINK-R4064-CS-EFCTV-03 TO DBLINK-S-4064-4032-03.      276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4064-4032-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4030-4064                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE CSE-FK-CSH-COST-SHR-CD TO DBLINK-S-4030-4064-01.        276 BRTN
YYY991     MOVE CSE-START-DT TO DBLINK-S-4030-4064-02.                  276 BRTN
YYY991     MOVE CSE-TIME-STMP TO DBLINK-S-4030-4064-03.                 276 BRTN
