       976-FROM-MSG-LINE-144 SECTION.

               MOVE CMT-ID-144 OF MSG-LINE-144
               TO MSL-CMT-ID.

               MOVE MDRDEST1-144 OF MSG-LINE-144
               TO MSL-MDRDEST1.

               MOVE MDRDEST2-144 OF MSG-LINE-144
               TO MSL-MDRDEST2.

               MOVE MDROSDSC-144 OF MSG-LINE-144
               TO MSL-MDROSDSC.

               MOVE MDROSRTC-144 OF MSG-LINE-144
               TO MSL-MDROSRTC.

               MOVE MDRDSTID-144 OF MSG-LINE-144
               TO MSL-MDRDSTID.

               MOVE MDRSEVCD-144 OF MSG-LINE-144
               TO MSL-MDRSEVCD.

               MOVE MDRTEXTL-144 OF MSG-LINE-144
               TO MSL-MDRTEXTL.

               MOVE MSG-144 OF MSG-LINE-144 (1)
               TO MSL-MSG-1.

               MOVE MSG-144 OF MSG-LINE-144 (2)
               TO MSL-MSG-2.
       976-FROM-MSG-LINE-144-EXIT.
           EXIT.
