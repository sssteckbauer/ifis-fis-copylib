    ***Created by Convert/DC version V8R03 on 11/17/00 at 16:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY01-Z = 'Y'
               MOVE WK01-TEXT-SY01 TO DCSD-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0A-Z = 'Y'
               MOVE WK01-TEXT-SY0A TO CNFDL-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0B-Z = 'Y'
               MOVE WK01-TEXT-SY0B TO HOLDS-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ADR-1-KEY-UT13-Z = 'Y'
               MOVE WK01-ADR-1-KEY-UT13 TO LINE-ADR-1-KEY-UT13 OF
                FUTWK13
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PRSN-IND-UT13-Z = 'Y'
               MOVE WK01-PRSN-IND-UT13 TO ENTY-PRSN-IND-UT13 OF FUTWK13
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT131-Z = 'Y'
               MOVE WK01-CODE-UT131 TO ACTN-CODE-UT13 OF FUTWM13(0001)
           END-IF.
           MOVE WK01-CODE-UT131-F TO WK01-CODE-UT13-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT132-Z = 'Y'
               MOVE WK01-CODE-UT132 TO ACTN-CODE-UT13 OF FUTWM13(0002)
           END-IF.
           MOVE WK01-CODE-UT132-F TO WK01-CODE-UT13-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT133-Z = 'Y'
               MOVE WK01-CODE-UT133 TO ACTN-CODE-UT13 OF FUTWM13(0003)
           END-IF.
           MOVE WK01-CODE-UT133-F TO WK01-CODE-UT13-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-UT131-Z = 'Y'
               MOVE WK01-NMBR-UT131 TO ID-NMBR-UT13 OF FUTWM13(0001)
           END-IF.
           MOVE WK01-NMBR-UT131-F TO WK01-NMBR-UT13-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-UT132-Z = 'Y'
               MOVE WK01-NMBR-UT132 TO ID-NMBR-UT13 OF FUTWM13(0002)
           END-IF.
           MOVE WK01-NMBR-UT132-F TO WK01-NMBR-UT13-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-UT133-Z = 'Y'
               MOVE WK01-NMBR-UT133 TO ID-NMBR-UT13 OF FUTWM13(0003)
           END-IF.
           MOVE WK01-NMBR-UT133-F TO WK01-NMBR-UT13-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-UT131-Z = 'Y'
               MOVE WK01-TYPE-CODE-UT131 TO ADR-TYPE-CODE-UT13 OF
                FUTWM13(0001)
           END-IF.
           MOVE WK01-TYPE-CODE-UT131-F TO WK01-TYPE-CODE-UT13-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-UT132-Z = 'Y'
               MOVE WK01-TYPE-CODE-UT132 TO ADR-TYPE-CODE-UT13 OF
                FUTWM13(0002)
           END-IF.
           MOVE WK01-TYPE-CODE-UT132-F TO WK01-TYPE-CODE-UT13-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-UT133-Z = 'Y'
               MOVE WK01-TYPE-CODE-UT133 TO ADR-TYPE-CODE-UT13 OF
                FUTWM13(0003)
           END-IF.
           MOVE WK01-TYPE-CODE-UT133-F TO WK01-TYPE-CODE-UT13-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-2-UT131-Z = 'Y'
               MOVE WK01-ADR-2-UT131 TO LINE-ADR-2-UT13 OF FUTWM13(0001)
           END-IF.
           MOVE WK01-ADR-2-UT131-F TO WK01-ADR-2-UT13-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-KEY-UT131-Z = 'Y'
               MOVE WK01-KEY-UT131 TO NAME-KEY-UT13 OF FUTWM13(0001)
           END-IF.
           MOVE WK01-KEY-UT131-F TO WK01-KEY-UT13-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-KEY-UT132-Z = 'Y'
               MOVE WK01-KEY-UT132 TO NAME-KEY-UT13 OF FUTWM13(0002)
           END-IF.
           MOVE WK01-KEY-UT132-F TO WK01-KEY-UT13-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-KEY-UT133-Z = 'Y'
               MOVE WK01-KEY-UT133 TO NAME-KEY-UT13 OF FUTWM13(0003)
           END-IF.
           MOVE WK01-KEY-UT133-F TO WK01-KEY-UT13-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-1-UT132-Z = 'Y'
               MOVE WK01-ADR-1-UT132 TO LINE-ADR-1-UT13 OF FUTWM13(0002)
           END-IF.
           MOVE WK01-ADR-1-UT132-F TO WK01-ADR-1-UT13-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-1-UT133-Z = 'Y'
               MOVE WK01-ADR-1-UT133 TO LINE-ADR-1-UT13 OF FUTWM13(0003)
           END-IF.
           MOVE WK01-ADR-1-UT133-F TO WK01-ADR-1-UT13-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT131-Z = 'Y'
               MOVE WK01-NAME-UT131 TO CITY-NAME-UT13 OF FUTWM13(0001)
           END-IF.
           MOVE WK01-NAME-UT131-F TO WK01-NAME-UT13-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-2-UT133-Z = 'Y'
               MOVE WK01-ADR-2-UT133 TO LINE-ADR-2-UT13 OF FUTWM13(0003)
           END-IF.
           MOVE WK01-ADR-2-UT133-F TO WK01-ADR-2-UT13-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-WORD-LIT-UT13-Z = 'Y'
               MOVE WK01-WORD-LIT-UT13 TO WORK-WORD-LIT-UT13 OF FUTWK13
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT134-Z = 'Y'
               MOVE WK01-CODE-UT134 TO ACTN-CODE-UT13 OF FUTWM13(0004)
           END-IF.
           MOVE WK01-CODE-UT134-F TO WK01-CODE-UT13-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-UT134-Z = 'Y'
               MOVE WK01-NMBR-UT134 TO ID-NMBR-UT13 OF FUTWM13(0004)
           END-IF.
           MOVE WK01-NMBR-UT134-F TO WK01-NMBR-UT13-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-KEY-UT134-Z = 'Y'
               MOVE WK01-KEY-UT134 TO NAME-KEY-UT13 OF FUTWM13(0004)
           END-IF.
           MOVE WK01-KEY-UT134-F TO WK01-KEY-UT13-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-UT134-Z = 'Y'
               MOVE WK01-TYPE-CODE-UT134 TO ADR-TYPE-CODE-UT13 OF
                FUTWM13(0004)
           END-IF.
           MOVE WK01-TYPE-CODE-UT134-F TO WK01-TYPE-CODE-UT13-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-1-UT134-Z = 'Y'
               MOVE WK01-ADR-1-UT134 TO LINE-ADR-1-UT13 OF FUTWM13(0004)
           END-IF.
           MOVE WK01-ADR-1-UT134-F TO WK01-ADR-1-UT13-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-2-UT132-Z = 'Y'
               MOVE WK01-ADR-2-UT132 TO LINE-ADR-2-UT13 OF FUTWM13(0002)
           END-IF.
           MOVE WK01-ADR-2-UT132-F TO WK01-ADR-2-UT13-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT133-Z = 'Y'
               MOVE WK01-NAME-UT133 TO CITY-NAME-UT13 OF FUTWM13(0003)
           END-IF.
           MOVE WK01-NAME-UT133-F TO WK01-NAME-UT13-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-2-UT134-Z = 'Y'
               MOVE WK01-ADR-2-UT134 TO LINE-ADR-2-UT13 OF FUTWM13(0004)
           END-IF.
           MOVE WK01-ADR-2-UT134-F TO WK01-ADR-2-UT13-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT132-Z = 'Y'
               MOVE WK01-NAME-UT132 TO CITY-NAME-UT13 OF FUTWM13(0002)
           END-IF.
           MOVE WK01-NAME-UT132-F TO WK01-NAME-UT13-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT134-Z = 'Y'
               MOVE WK01-NAME-UT134 TO CITY-NAME-UT13 OF FUTWM13(0004)
           END-IF.
           MOVE WK01-NAME-UT134-F TO WK01-NAME-UT13-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-FLAG-UT131-Z = 'Y'
               MOVE WK01-ADR-FLAG-UT131 TO ACH-ADR-FLAG-UT13 OF
                FUTWM13(0001)
           END-IF.
           MOVE WK01-ADR-FLAG-UT131-F TO WK01-ADR-FLAG-UT13-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-FLAG-UT132-Z = 'Y'
               MOVE WK01-ADR-FLAG-UT132 TO ACH-ADR-FLAG-UT13 OF
                FUTWM13(0002)
           END-IF.
           MOVE WK01-ADR-FLAG-UT132-F TO WK01-ADR-FLAG-UT13-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-FLAG-UT133-Z = 'Y'
               MOVE WK01-ADR-FLAG-UT133 TO ACH-ADR-FLAG-UT13 OF
                FUTWM13(0003)
           END-IF.
           MOVE WK01-ADR-FLAG-UT133-F TO WK01-ADR-FLAG-UT13-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-FLAG-UT134-Z = 'Y'
               MOVE WK01-ADR-FLAG-UT134 TO ACH-ADR-FLAG-UT13 OF
                FUTWM13(0004)
           END-IF.
           MOVE WK01-ADR-FLAG-UT134-F TO WK01-ADR-FLAG-UT13-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-1-UT131-Z = 'Y'
               MOVE WK01-ADR-1-UT131 TO LINE-ADR-1-UT13 OF FUTWM13(0001)
           END-IF.
           MOVE WK01-ADR-1-UT131-F TO WK01-ADR-1-UT13-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT1A1-Z = 'Y'
               MOVE WK01-CODE-UT1A1 TO STATE-CODE-UT13 OF FUTWM13(0001)
           END-IF.
           MOVE WK01-CODE-UT1A1-F TO WK01-CODE-UT1A-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT1A2-Z = 'Y'
               MOVE WK01-CODE-UT1A2 TO STATE-CODE-UT13 OF FUTWM13(0002)
           END-IF.
           MOVE WK01-CODE-UT1A2-F TO WK01-CODE-UT1A-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT1A3-Z = 'Y'
               MOVE WK01-CODE-UT1A3 TO STATE-CODE-UT13 OF FUTWM13(0003)
           END-IF.
           MOVE WK01-CODE-UT1A3-F TO WK01-CODE-UT1A-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT1A4-Z = 'Y'
               MOVE WK01-CODE-UT1A4 TO STATE-CODE-UT13 OF FUTWM13(0004)
           END-IF.
           MOVE WK01-CODE-UT1A4-F TO WK01-CODE-UT1A-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT1B1-Z = 'Y'
               MOVE WK01-CODE-UT1B1 TO ZIP-CODE-UT13 OF FUTWM13(0001)
           END-IF.
           MOVE WK01-CODE-UT1B1-F TO WK01-CODE-UT1B-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT1B2-Z = 'Y'
               MOVE WK01-CODE-UT1B2 TO ZIP-CODE-UT13 OF FUTWM13(0002)
           END-IF.
           MOVE WK01-CODE-UT1B2-F TO WK01-CODE-UT1B-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT1B3-Z = 'Y'
               MOVE WK01-CODE-UT1B3 TO ZIP-CODE-UT13 OF FUTWM13(0003)
           END-IF.
           MOVE WK01-CODE-UT1B3-F TO WK01-CODE-UT1B-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT1B4-Z = 'Y'
               MOVE WK01-CODE-UT1B4 TO ZIP-CODE-UT13 OF FUTWM13(0004)
           END-IF.
           MOVE WK01-CODE-UT1B4-F TO WK01-CODE-UT1B-F (4).
