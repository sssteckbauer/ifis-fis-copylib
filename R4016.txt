       01  R4016-BDGT-CTRL.
           02  DB-PROC-ID-4016.
               03  USER-CODE-4016                       PIC X(8).
               03  LAST-ACTVY-DATE-4016                 PIC X(5).
               03  TRMNL-ID-4016                        PIC X(8).
               03  PURGE-FLAG-4016                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  BDGT-CNTRL-KEY-4016.
               03  UNVRS-CODE-4016                      PIC X(2).
               03  COA-CODE-4016                        PIC X(1).
               03  FUND-CODE-4016                       PIC X(6).
               03  ORGZN-CODE-4016                      PIC X(6).
           02  ACTVY-DATE-4016                COMP-3    PIC S9(8).
