       8999-CENTER-HEADING SECTION.
       8999-010.
      *----------------------------------------------------------------*
      *    COPYLIB MEMBER: CENTRHED                                    *
      *    CODE TO CENTER A HEADING FOR A REPORT AS PASSED TO THE      *
      *    WSCTRHED PARAMETERS.                                        *
      *----------------------------------------------------------------*
           MOVE SPACES TO WS-HOLD-HEADING-B.

           UNSTRING WS-HOLD-HEADING-A DELIMITED BY WS-TWO-SPACES
               INTO WS-HOLD-HEADING-B COUNT IN WS-CHARACTER-CNT.

           IF WS-CHARACTER-CNT > 33
               GO TO 8999-EXIT.

           MOVE SPACES TO WS-HOLD-HEADING-A.

           COMPUTE WS-POINTER = ((( 35 - WS-CHARACTER-CNT ) / 2) + 1).

           STRING WS-HOLD-HEADING-B DELIMITED BY WS-TWO-SPACES
               INTO WS-HOLD-HEADING-A WITH POINTER WS-POINTER.

       8999-EXIT.
           EXIT.
      *
