000100**************************************************************/   36040426
000200*  COPYMEMBER: CPWSXTTL                                      */   36040426
000300*  RELEASE # ____0426____ SERVICE REQUEST NO(S)___3604_______*/   36040426
000400*  NAME __ROB FISHER___   MODIFICATION DATE ____09/29/89_____*/   36040426
000500*  DESCRIPTION                                               */   36040426
000600*   - ADDED PAID OVER CODE FOR APPOINTMENT PAID OVER EDITS.  */   36040426
000700**************************************************************/   36040426
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXTTL                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/22/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   13880212
000200*  COPYMEMBER: CPWSXTTL                                      */   13880212
000300*  REL#:  0212   REF: NONE  SERVICE REQUESTS:   ____1388____ */   13880212
000400*  NAME __M.NISHI______   MODIFICATION DATE ____05/01/86_____*/   13880212
000500*  DESCRIPTION                                               */   13880212
000600*                                                            */   13880212
000700*  ONE NEW FIELD  WAS ADDED  FOR THE MAP PROGRAM CALLED      */   13880212
000800*  XTTL-TITLE-TYPE.                                          */   13880212
000900*                                                            */   13880212
001000*  ALSO, COMBINE XTTL-STEPS AND XTTL-MGMT-SAL-GRADE TO       */   13880212
001100*  CREATE A NEW FIELD CALL XTTL-STEPS-GRADE.                 */   13880212
001200**************************************************************/   13880212
001300     SKIP1                                                        13880212
001400**************************************************************/   22360148
001500*  COPYMEMBER: CPWSXTTL                                      */   22360148
001600*  RELEASE # ___0148___   SERVICE REQUEST NO(S) 2236         */   22360148
001700*  NAME ___JAG_________   MODIFICATION DATE ____03/25/85_____*/   22360148
001800*  DESCRIPTION                                               */   22360148
001900*       **** COLLECTIVE BARGAINING MODIFICATIONS ****        */   22360148
002000*                                                            */   22360148
002100*   XTTL-PERB2-CODE AND XTTL-PERB3-CODE HAVE BEEN ELIMINATED.*/   22360148
002200*  IN PLACE OF THESE TWO CODES, THE FOLLOWING FIELDS HAVE    */   22360148
002300*  BEEN DEFINED:                                             */   22360148
002400*                XTTL-SPCL-HNDLG-CODE (TITLE SPECIAL         */   22360148
002500*                                      HANDLING CODE)        */   22360148
002600*                FILLER                                      */   22360148
002700*                                                            */   22360148
002800**************************************************************/   22360148
002900**************************************************************/   21980111
003000*  MODULE:  CPWSXTTL                                         */   21980111
003100*  RELEASE # ___0111___   SERVICE REQUEST NO(S) __2198______ */   21980111
003200*  NAME ___AMC_________   MODIFICATION DATE ____06/19/84_____*/   21980111
003300*  DESCRIPTION                                               */   21980111
003400*   A NEW SELECTION CODE AND EFFECTIVE DATE WERE ADDED.      */   21980111
003500**************************************************************/   21980111
003600     SKIP3                                                        21980111
003700**************************************************************/   20600088
003800*  RELEASE # ___0088___   SERVICE REQUEST NO(S) ____2060____ */   20600088
003900*  NAME __M.NISHI______   MODIFICATION DATE ____11/15/83_____*/   20600088
004000*  DESCRIPTION                                               */   20600088
004100*  TWO NEW FIELDS WERE ADDED FOR THE MODIFIED RANGE ADJUST-  */   20600088
004200*  MENT PROCESS (PROGRAMS PPP900-PPP930).                    */   20600088
004300**************************************************************/   20600088
004400**************************************************************/   00820049
004500*  RELEASE # ___0049___   SERVICE REQUEST NO(S) ____0082____ */   00820049
004600*  NAME ___MCN_________   MODIFICATION DATE ____02/17/83_____*/   00820049
004700*  DESCRIPTION                                               */   00820049
004800*  THREE NEW FIELDS WERE ADDED IN FILLER SPACE AFTER         */   00820049
004900*  XTTL-STAFF-GRP-CODE FOR THE PERB1, PERB2 AND PERB3 FIELDS.*/   00820049
005000**************************************************************/   00820049
005100*    COPYID=CPWSXTTL                                              CPWSXTTL
005200*01  XTTL-TITLE-CODE-RECORD.                                      30930413
005300*-----------------------------------------------------------------CPWSXTTL
005400*          T I T L E   C O D E   T A B L E   R E C O R D          CPWSXTTL
005500*-----------------------------------------------------------------CPWSXTTL
005600     03  XTTL-DELETE             PIC X.                           CPWSXTTL
005700     03  XTTL-KEY.                                                CPWSXTTL
005800         05  XTTL-KEY-CONSTANT   PIC X(7).                        CPWSXTTL
005900         05  FILLER              PIC X(2).                        CPWSXTTL
006000         05  XTTL-KEY-TTL-CODE   PIC X(4).                        CPWSXTTL
006100     03  XTTL-USE                PIC X.                           CPWSXTTL
006200     03  XTTL-JOB-TITLE          PIC X(50).                       CPWSXTTL
006300     03  XTTL-JOB-TITLE-ABBRV    PIC X(30).                       CPWSXTTL
006400     03  XTTL-TITLE-TYPE         PIC X(1).                        13880212
006500     03  FILLER                  PIC X(1).                        13880212
006600***  03  FILLER                  PIC X(2).                        13880212
006700     03  XTTL-PRIOR-JOB-NO       PIC X(4).                        CPWSXTTL
006800     03  XTTL-SALARY.                                             CPWSXTTL
006900         05  XTTL-ANNUAL-HOURLY-INFO OCCURS 2 TIMES.              CPWSXTTL
007000             07  XTTL-RATE-IND       PIC X.                       CPWSXTTL
007100             07  XTTL-MINIMUM-SAL    PIC S9(7)V99  COMP-3.        CPWSXTTL
007200             07  XTTL-MIN-HOURLY REDEFINES   XTTL-MINIMUM-SAL     CPWSXTTL
007300                                 PIC S9(5)V9(4)  COMP-3.          CPWSXTTL
007400             07  XTTL-MAXIMUM-SAL    PIC S9(7)V99  COMP-3.        CPWSXTTL
007500             07  XTTL-MAX-HOURLY REDEFINES   XTTL-MAXIMUM-SAL     CPWSXTTL
007600                                 PIC S9(5)V9(4)  COMP-3.          CPWSXTTL
007700     03  XTTL-SALARY-X       REDEFINES   XTTL-SALARY.             CPWSXTTL
007800         05  FILLER                  OCCURS  2 TIMES.             CPWSXTTL
007900             07  FILLER              PIC X.                       CPWSXTTL
008000             07  XTTL-MIN            PIC X(5).                    CPWSXTTL
008100             07  XTTL-MAX            PIC X(5).                    CPWSXTTL
008200     03  XTTL-STEPS-GRADE        PIC X(3).                        13880212
008300***  03  XTTL-STEPS              PIC 9(2).                        13880212
008400***  03  XTTL-MGMT-SAL-GRADE     PIC X.                           13880212
008500     03  XTTL-ON-CALL-RATE       PIC 99V99.                       CPWSXTTL
008600     03  XTTL-ON-CALL-RATE-X     REDEFINES   XTTL-ON-CALL-RATE    CPWSXTTL
008700                                 PIC X(4).                        CPWSXTTL
008800     03  XTTL-SHIFT-DIFFERENTIAL OCCURS 2.                        CPWSXTTL
008900         05  XTTL-SHIFT-DIFF-CODE    PIC X.                       CPWSXTTL
009000         05  XTTL-SHIFT-RATE         PIC 99V99.                   CPWSXTTL
009100         05  XTTL-SHIFT-RATE-X   REDEFINES   XTTL-SHIFT-RATE      CPWSXTTL
009200                                 PIC X(4).                        CPWSXTTL
009300     03  XTTL-STD-WORK-HRS       PIC 99V99.                       CPWSXTTL
009400     03  XTTL-STD-WORK-HRS-X     REDEFINES   XTTL-STD-WORK-HRS    CPWSXTTL
009500                                 PIC X(4).                        CPWSXTTL
009600     03  XTTL-FLSA-STATUS        PIC X.                           CPWSXTTL
009700     03  XTTL-FOC                PIC X.                           CPWSXTTL
009800     03  XTTL-FOC-SUB            PIC X(2).                        CPWSXTTL
009900     03  XTTL-STATUS-CODE        PIC X.                           CPWSXTTL
010000     03  XTTL-STATUS-DATE.                                        CPWSXTTL
010100         05  XTTL-STATUS-DATE-MTH PIC XX.                         CPWSXTTL
010200         05  XTTL-STATUS-DATE-DAY PIC XX.                         CPWSXTTL
010300         05  XTTL-STATUS-DATE-YR  PIC XX.                         CPWSXTTL
010400     03  XTTL-CLASS-TTL-OUTLINE  PIC X(3).                        CPWSXTTL
010500     03  XTTL-RETIRE-CODE-1      PIC X.                           CPWSXTTL
010600     03  XTTL-RETIRE-CODE-2      PIC X.                           CPWSXTTL
010700     03  XTTL-RETIRE-CODE-3      PIC X.                           CPWSXTTL
010800     03  XTTL-DATE-LAST-REVIEWED.                                 CPWSXTTL
010900         05  XTTL-LAST-REVIEW-MTH PIC XX.                         CPWSXTTL
011000         05  XTTL-LAST-REVIEW-DAY PIC XX.                         CPWSXTTL
011100         05  XTTL-LAST-REVIEW-YR  PIC XX.                         CPWSXTTL
011200     03  XTTL-LOCATION           PIC X(2).                        CPWSXTTL
011300     03  XTTL-ACADEMIC-RANK      PIC X.                           CPWSXTTL
011400     03  XTTL-ACADEMIC-GRP-CODE  PIC X(3).                        CPWSXTTL
011500     03  XTTL-SABB-ELIG          PIC X.                           CPWSXTTL
011600     03  XTTL-ACAD-SERV-LIMIT    PIC X(2).                        CPWSXTTL
011700     03  XTTL-ACAD-APPT-TYPE     PIC X.                           CPWSXTTL
011800     03  XTTL-INSTR-RANK         PIC X(4).                        CPWSXTTL
011900     03  XTTL-STAFF-GRP-CODE     PIC X(2).                        CPWSXTTL
012000     03  XTTL-PERB1-CODE         PIC X(2).                        00820049
012100     03  XTTL-SPCL-HNDLG-CODE    PIC X(1).                        22360148
012200     03  FILLER                  PIC X(3).                        22360148
012300     03  XTTL-SELECTION-TABLE.                                    21980111
012400         05  XTTL-SELECTION          PIC X(03).                   21980111
012500         05  XTTL-RA-DATE.                                        21980111
012600             10  XTTL-RA-YY          PIC XX.                      21980111
012700             10  XTTL-RA-MM          PIC XX.                      21980111
012800             10  XTTL-RA-DD          PIC XX.                      21980111
012900         05  XTTL-SELECTION-2        PIC X(03).                   21980111
013000         05  XTTL-RA-DATE-2.                                      21980111
013100             10  XTTL-RA-YY-2        PIC XX.                      21980111
013200             10  XTTL-RA-MM-2        PIC XX.                      21980111
013300             10  XTTL-RA-DD-2        PIC XX.                      21980111
013400     03  XTTL-SEL-TABLE  REDEFINES  XTTL-SELECTION-TABLE.         21980111
013500         05  FILLER  OCCURS  2  TIMES.                            21980111
013600             10  XTTL-SEL           PIC XXX.                      21980111
013700             10  XTTL-RA-DT.                                      21980111
013800                 15  XTTL-RA-YEAR   PIC XX.                       21980111
013900                 15  XTTL-RA-MONTH  PIC XX.                       21980111
014000                 15  XTTL-RA-DAY    PIC XX.                       21980111
015500     03  XTTL-PAID-OVER          PIC  X.                          36040426
015600*****03  FILLER                  PIC X(6).                        36040426
015700     03  FILLER                  PIC X(5).                        36040426
014200     03  XTTL-UPDATE-INFO.                                        CPWSXTTL
014300         05  XTTL-ADD-A-TRAN     PIC X.                           CPWSXTTL
014400         05  XTTL-ADD-B-TRAN     PIC X.                           CPWSXTTL
014500         05  XTTL-ADD-C-TRAN     PIC X.                           CPWSXTTL
014600         05  XTTL-ADD-P-TRAN     PIC X.                           CPWSXTTL
014700         05  XTTL-LAST-ACTION    PIC X.                           CPWSXTTL
014800         05  XTTL-LAST-UPDATE    PIC X(6).                        CPWSXTTL
