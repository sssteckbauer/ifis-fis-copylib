       975-TO-R4074-CMDTY SECTION.
           MOVE CDY-USER-CD TO
               USER-CODE-4074 OF R4074-CMDTY
                                                                     .
           MOVE CDY-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4074 OF R4074-CMDTY
                                                                     .
           MOVE CDY-UNVRS-CD TO
               UNVRS-CODE-4074 OF R4074-CMDTY
                                                                     .
           MOVE CDY-CMDTY-CD TO
               CMDTY-CODE-4074 OF R4074-CMDTY
                                                                     .
           MOVE CDY-CMDTY-DESC TO
               CMDTY-DESC-4074 OF R4074-CMDTY
                                                                     .
           MOVE CDY-START-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               START-DATE-4074 OF R4074-CMDTY
                                                                     .
           MOVE CDY-END-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               END-DATE-4074 OF R4074-CMDTY
                                                                     .
           MOVE CDY-UNIT-MEA-CD TO
               UNIT-MEA-CODE-4074 OF R4074-CMDTY
                                                                     .
           MOVE CDY-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACTVY-DATE-4074 OF R4074-CMDTY
                                                                     .
           MOVE CDY-CMDTY-UPPER-DESC TO
               CMDTY-UPPER-DESC-4074 OF R4074-CMDTY
                                                                     .
       975-TO-R4074-CMDTY-EXIT.
           EXIT.
