      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FEXWD30                              04/24/00  13:05  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FEXWD30.
           02  INDX-KEY-4073-EX30.
               03  UNVRS-CODE-KEY-EX30                  PIC X(2).
               03  VNDR-CODE-KEY-EX30.
                   04  VNDR-ID-DIGIT-ONE-KEY-EX30       PIC X(1).
                   04  VNDR-ID-LAST-NINE-KEY-EX30       PIC X(9).
           02  SET-SORT-KEY-6311-EX30.
               03  UNVRS-CODE-EX30                      PIC X(2).
               03  INTRL-REF-ID-EX30          COMP-3    PIC S9(7).
           02  SYSTEM-DATE-EX30               COMP-3    PIC S9(8).
           02  SAVE-DBKEY-ID-EX30             COMP      PIC S9(8).
