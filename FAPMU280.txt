    ***Created by Convert/DC version V8R03 on 12/05/00 at 08:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY01-Z = 'Y'
               MOVE WK01-TEXT-SY01 TO DCSD-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0A-Z = 'Y'
               MOVE WK01-TEXT-SY0A TO CNFDL-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0B-Z = 'Y'
               MOVE WK01-TEXT-SY0B TO HOLDS-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ORGN-CODE-AP28-Z = 'Y'
               MOVE WK01-ORGN-CODE-AP28 TO ORGNT-ORGN-CODE-AP28 OF
                FAPWK28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-BANK-ACCT-CODE-AP28-Z = 'Y'
               MOVE WK01-BANK-ACCT-CODE-AP28 TO
                ORGNT-BANK-ACCT-CODE-AP28 OF FAPWK28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-DESC-4062-AP28-Z = 'Y'
               MOVE WK01-CODE-DESC-4062-AP28 TO
                BANK-CODE-DESC-4062-AP28 OF FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4158-AP28-Z = 'Y'
               MOVE WK01-NMBR-4158-AP28 TO CHECK-NMBR-4158-AP28 OF
                FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4158-AP28-Z = 'Y'
               MOVE WK01-DATE-4158-AP28 TO CHECK-DATE-4158-AP28 OF
                FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-4158-AP28-Z = 'Y'
               MOVE WK01-TYPE-CODE-4158-AP28 TO
                CHECK-TYPE-CODE-4158-AP28 OF FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4159-AP28-Z = 'Y'
               MOVE WK01-AMT-4159-AP28 TO GROSS-AMT-4159-AP28 OF FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4159-AP2A-Z = 'Y'
               MOVE WK01-AMT-4159-AP2A TO DSCNT-AMT-4159-AP28 OF FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4159-AP2B-Z = 'Y'
               MOVE WK01-AMT-4159-AP2B TO TAX-AMT-4159-AP28 OF FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CHRG-4159-AP28-Z = 'Y'
               MOVE WK01-CHRG-4159-AP28 TO ADDL-CHRG-4159-AP28 OF
                FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TRD-IN-AMT-AP28-Z = 'Y'
               MOVE WK01-TRD-IN-AMT-AP28 TO TRADE-IN-AMT-AP28 OF
                FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-WTHHLD-AMT-4159-AP28-Z = 'Y'
               MOVE WK01-WTHHLD-AMT-4159-AP28 TO
                FDRL-WTHHLD-AMT-4159-AP28 OF FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-WTHHLD-PCT-4159-AP28-Z = 'Y'
               MOVE WK01-WTHHLD-PCT-4159-AP28 TO
                FDRL-WTHHLD-PCT-4159-AP28 OF FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-LITERAL-AP28-Z = 'Y'
               MOVE WK01-LITERAL-AP28 TO PERCENTAGE-LITERAL-AP28 OF
                FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-WTHHLD-AMT-4159-AP2A-Z = 'Y'
               MOVE WK01-WTHHLD-AMT-4159-AP2A TO
                STATE-WTHHLD-AMT-4159-AP28 OF FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-WTHHLD-PCT-4159-AP2A-Z = 'Y'
               MOVE WK01-WTHHLD-PCT-4159-AP2A TO
                STATE-WTHHLD-PCT-4159-AP28 OF FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-LITERAL-AP2A-Z = 'Y'
               MOVE WK01-LITERAL-AP2A TO PERCENTAGE-LITERAL-AP28 OF
                FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4158-AP28-Z = 'Y'
               MOVE WK01-AMT-4158-AP28 TO CHECK-AMT-4158-AP28 OF FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DEST-CODE-4167-AP28-Z = 'Y'
               MOVE WK01-DEST-CODE-4167-AP28 TO
                PRNTR-DEST-CODE-4167-AP28 OF FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEST-PATRN-AP28-Z = 'Y'
               MOVE WK01-TEST-PATRN-AP28 TO PRINT-TEST-PATRN-AP28 OF
                FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CHECK-AP28-Z = 'Y'
               MOVE WK01-CHECK-AP28 TO PRINT-CHECK-AP28 OF FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CONFIRM-AP28-Z = 'Y'
               MOVE WK01-CONFIRM-AP28 TO PRINT-CONFIRM-AP28 OF FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-LCTN-DESC-4167-AP28-Z = 'Y'
               MOVE WK01-LCTN-DESC-4167-AP28 TO
                PRNTR-LCTN-DESC-4167-AP28 OF FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-DESC-4012-AP28-Z = 'Y'
               MOVE WK01-CODE-DESC-4012-AP28 TO
                ORGN-CODE-DESC-4012-AP28 OF FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4073-AP28-Z = 'Y'
               MOVE WK01-CODE-4073-AP28 TO VNDR-CODE-4073-AP28 OF
                FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-AP28-Z = 'Y'
               MOVE WK01-NAME-AP28 TO PAYEE-NAME-AP28 OF FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP28-Z = 'Y'
               MOVE WK01-CODE-AP28 TO ACTN-CODE-AP28 OF FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DCMNT-NMBR-AP28-Z = 'Y'
               MOVE WK01-DCMNT-NMBR-AP28 TO ORGNT-DCMNT-NMBR-AP28 OF
                FAPWK28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-LIT-AP28-Z = 'Y'
               MOVE WK01-LIT-AP28 TO CHECK-LIT-AP28 OF FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-START-AP28-Z = 'Y'
               MOVE WK01-START-AP28 TO CHK-START-AP28 OF FAPWM28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-END-AP28-Z = 'Y'
               MOVE WK01-END-AP28 TO CHK-END-AP28 OF FAPWM28
           END-IF.
