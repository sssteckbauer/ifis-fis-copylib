      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FGAWM71.                                              *
      *                                                               *
      *---------------------------------------------------------------*
       01  FGAWM71.
          02  MAP-CONTROL.
           03  WK01-DCXMAPL-PARMS.
               10 WK01-MAPNAME                   PIC X(8)
                                                 VALUE 'FGAMI71'.
               05 WK01-ATTRIBUTES                PIC X(60)
                                                     VALUE SPACE.
               05 WK01-ATTRIBUTES-ERROR          PIC X(60) VALUE
                   'U  BIF'.
               05 WK01-CURSOR-DFLD               PIC X(36)
                                                     VALUE SPACES.
               05 WK01-CURSOR-DFLD-QUAL          PIC X(30)
                                                     VALUE SPACES.
               05 WK01-ROW                       PIC S9(4) COMP
                                                     VALUE ZERO.
               05 WK01-COLUMN                    PIC S9(4) COMP
                                                     VALUE ZERO.
               05 WK01-ERROR-MSG                 PIC X(80)
                                                     VALUE SPACES.
               05 WK01-MAP-DATA-LEN              PIC S9(6)
                                                     VALUE 1229.
               05 WK01-1ST-FLAG                  PIC X(1)
                                                     VALUE 'Y'.
               05 WK01-FLAG-OUTPUT-DATA          PIC X(1)
                                                     VALUE 'Y'.
               05 WK01-ALL-BUT-FLAG              PIC X(1)
                                                     VALUE SPACES.
               05 WK01-FLAG-ALL-FIELDS-CHANGED   PIC X(1)  VALUE 'Y'.
               05 WK01-FLAG-ANY-FIELDS-CHANGED   PIC X(1)  VALUE 'N'.
                  88 ANY-FIELDS-CHANGED          VALUE 'Y'.
               05 WK01-FLAG-ALL-FIELDS-ERROR     PIC X(1)  VALUE 'Y'.
               05 WK01-FLAG-ANY-FIELDS-ERROR     PIC X(1)  VALUE 'N'.
               05 WK01-FLAG-ALL-FIELDS-ERASE     PIC X(1)  VALUE 'Y'.
               05 WK01-FLAG-ANY-FIELDS-ERASE     PIC X(1)  VALUE 'Y'.
               05 WK01-FLAG-START-DATE-CHANGED   PIC X(1)  VALUE 'Y'.
               05 WK01-PASSED-ONE-F              PIC X(1)  VALUE SPACE.
               05 WK01-VARDATA                   PIC X(80)
                                                     VALUE SPACES.
           03  WK01-ELEMENT-AREA.
             04  FIRST-LINE-KEY.
               05 FIRST-UCOP-FUND-NBR            PIC X(5).
               05 FIRST-UCOP-FUND-NAME           PIC X(35).
               05 FIRST-IFIS-NBR                 PIC X(7).
               05 FIRST-SPN-CD                   PIC X(4).
               05 FIRST-SPNSR-CAT-CD             PIC X(2).

             04  LAST-LINE-KEY.
               05 LAST-UCOP-FUND-NBR             PIC X(5).
               05 LAST-UCOP-FUND-NAME            PIC X(35).
               05 LAST-IFIS-NBR                  PIC X(7).
               05 LAST-SPN-CD                    PIC X(4).
               05 LAST-SPNSR-CAT-CD              PIC X(2).

             04 WORK-SYSTEM-DATE-GA71            PIC 9(08).

           03 WS-SEARCH-REQUEST              PIC X(1).
              88 SEARCH-BY-IFIS-NBR          VALUE 'I'.
              88 SEARCH-BY-FUND-NBR          VALUE 'F'.
              88 SEARCH-BY-NAME              VALUE 'N'.
              88 SEARCH-BY-SPNCD             VALUE 'S'.
