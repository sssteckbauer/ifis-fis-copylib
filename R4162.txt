       01  R4162-CHK-PARMS.
           02  DB-PROC-ID-4162.
               03  USER-CODE-4162                       PIC X(8).
               03  LAST-ACTVY-DATE-4162                 PIC X(5).
               03  TRMNL-ID-4162                        PIC X(8).
               03  PURGE-FLAG-4162                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  CHECK-BATCH-PARM-KEY-4162.
               03  UNVRS-CODE-4162                      PIC X(2).
               03  ORGN-CODE-4162                       PIC X(4).
               03  BANK-ACCT-CODE-4162                  PIC X(2).
           02  CHECK-RUN-DATE-4162            COMP-3    PIC S9(8).
           02  START-CHECK-NMBR-4162                    PIC X(8).
           02  SEQ-NMBR-4162                            PIC 9(4).
