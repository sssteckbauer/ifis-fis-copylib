      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWK80                              04/24/00  12:19  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWK80.
           02  RQST-COA-CODE-CH80                       PIC X(1).
           02  SLCTN-DATE-CH80                          PIC X(6).
           02  SLCTN-STATUS-CH80                        PIC X(1).
           02  RQST-FUND-TYPE-CODE-CH80                 PIC X(2).
