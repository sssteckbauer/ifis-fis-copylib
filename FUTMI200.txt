    ***Created by Convert/DC version V8R03 on 11/20/00 at 12:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY01-Z = 'Y'
               MOVE WK01-TEXT-SY01 TO DCSD-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0A-Z = 'Y'
               MOVE WK01-TEXT-SY0A TO CNFDL-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0B-Z = 'Y'
               MOVE WK01-TEXT-SY0B TO HOLDS-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-KEY-UT20-Z = 'Y'
               MOVE WK01-KEY-UT20 TO NAME-KEY-UT20 OF FUTWK20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT201-Z = 'Y'
               MOVE WK01-CODE-UT201 TO ACTN-CODE-UT20 OF FUTWM20(0001)
           END-IF.
           MOVE WK01-CODE-UT201-F TO WK01-CODE-UT20-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT202-Z = 'Y'
               MOVE WK01-CODE-UT202 TO ACTN-CODE-UT20 OF FUTWM20(0002)
           END-IF.
           MOVE WK01-CODE-UT202-F TO WK01-CODE-UT20-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT203-Z = 'Y'
               MOVE WK01-CODE-UT203 TO ACTN-CODE-UT20 OF FUTWM20(0003)
           END-IF.
           MOVE WK01-CODE-UT203-F TO WK01-CODE-UT20-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT204-Z = 'Y'
               MOVE WK01-CODE-UT204 TO ACTN-CODE-UT20 OF FUTWM20(0004)
           END-IF.
           MOVE WK01-CODE-UT204-F TO WK01-CODE-UT20-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT205-Z = 'Y'
               MOVE WK01-CODE-UT205 TO ACTN-CODE-UT20 OF FUTWM20(0005)
           END-IF.
           MOVE WK01-CODE-UT205-F TO WK01-CODE-UT20-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT206-Z = 'Y'
               MOVE WK01-CODE-UT206 TO ACTN-CODE-UT20 OF FUTWM20(0006)
           END-IF.
           MOVE WK01-CODE-UT206-F TO WK01-CODE-UT20-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT207-Z = 'Y'
               MOVE WK01-CODE-UT207 TO ACTN-CODE-UT20 OF FUTWM20(0007)
           END-IF.
           MOVE WK01-CODE-UT207-F TO WK01-CODE-UT20-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT208-Z = 'Y'
               MOVE WK01-CODE-UT208 TO ACTN-CODE-UT20 OF FUTWM20(0008)
           END-IF.
           MOVE WK01-CODE-UT208-F TO WK01-CODE-UT20-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT209-Z = 'Y'
               MOVE WK01-CODE-UT209 TO ACTN-CODE-UT20 OF FUTWM20(0009)
           END-IF.
           MOVE WK01-CODE-UT209-F TO WK01-CODE-UT20-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT2010-Z = 'Y'
               MOVE WK01-CODE-UT2010 TO ACTN-CODE-UT20 OF FUTWM20(0010)
           END-IF.
           MOVE WK01-CODE-UT2010-F TO WK01-CODE-UT20-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-TO-CODE-UT201-Z = 'Y'
               MOVE WK01-TO-CODE-UT201 TO SHIP-TO-CODE-UT20 OF
                FUTWM20(0001)
           END-IF.
           MOVE WK01-TO-CODE-UT201-F TO WK01-TO-CODE-UT20-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-TO-CODE-UT202-Z = 'Y'
               MOVE WK01-TO-CODE-UT202 TO SHIP-TO-CODE-UT20 OF
                FUTWM20(0002)
           END-IF.
           MOVE WK01-TO-CODE-UT202-F TO WK01-TO-CODE-UT20-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-TO-CODE-UT203-Z = 'Y'
               MOVE WK01-TO-CODE-UT203 TO SHIP-TO-CODE-UT20 OF
                FUTWM20(0003)
           END-IF.
           MOVE WK01-TO-CODE-UT203-F TO WK01-TO-CODE-UT20-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-TO-CODE-UT204-Z = 'Y'
               MOVE WK01-TO-CODE-UT204 TO SHIP-TO-CODE-UT20 OF
                FUTWM20(0004)
           END-IF.
           MOVE WK01-TO-CODE-UT204-F TO WK01-TO-CODE-UT20-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-TO-CODE-UT205-Z = 'Y'
               MOVE WK01-TO-CODE-UT205 TO SHIP-TO-CODE-UT20 OF
                FUTWM20(0005)
           END-IF.
           MOVE WK01-TO-CODE-UT205-F TO WK01-TO-CODE-UT20-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-TO-CODE-UT206-Z = 'Y'
               MOVE WK01-TO-CODE-UT206 TO SHIP-TO-CODE-UT20 OF
                FUTWM20(0006)
           END-IF.
           MOVE WK01-TO-CODE-UT206-F TO WK01-TO-CODE-UT20-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-TO-CODE-UT207-Z = 'Y'
               MOVE WK01-TO-CODE-UT207 TO SHIP-TO-CODE-UT20 OF
                FUTWM20(0007)
           END-IF.
           MOVE WK01-TO-CODE-UT207-F TO WK01-TO-CODE-UT20-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-TO-CODE-UT208-Z = 'Y'
               MOVE WK01-TO-CODE-UT208 TO SHIP-TO-CODE-UT20 OF
                FUTWM20(0008)
           END-IF.
           MOVE WK01-TO-CODE-UT208-F TO WK01-TO-CODE-UT20-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-TO-CODE-UT209-Z = 'Y'
               MOVE WK01-TO-CODE-UT209 TO SHIP-TO-CODE-UT20 OF
                FUTWM20(0009)
           END-IF.
           MOVE WK01-TO-CODE-UT209-F TO WK01-TO-CODE-UT20-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-TO-CODE-UT2010-Z = 'Y'
               MOVE WK01-TO-CODE-UT2010 TO SHIP-TO-CODE-UT20 OF
                FUTWM20(0010)
           END-IF.
           MOVE WK01-TO-CODE-UT2010-F TO WK01-TO-CODE-UT20-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-UT201-Z = 'Y'
               MOVE WK01-TYPE-CODE-UT201 TO SHIP-TYPE-CODE-UT20 OF
                FUTWM20(0001)
           END-IF.
           MOVE WK01-TYPE-CODE-UT201-F TO WK01-TYPE-CODE-UT20-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-UT202-Z = 'Y'
               MOVE WK01-TYPE-CODE-UT202 TO SHIP-TYPE-CODE-UT20 OF
                FUTWM20(0002)
           END-IF.
           MOVE WK01-TYPE-CODE-UT202-F TO WK01-TYPE-CODE-UT20-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-UT203-Z = 'Y'
               MOVE WK01-TYPE-CODE-UT203 TO SHIP-TYPE-CODE-UT20 OF
                FUTWM20(0003)
           END-IF.
           MOVE WK01-TYPE-CODE-UT203-F TO WK01-TYPE-CODE-UT20-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-UT204-Z = 'Y'
               MOVE WK01-TYPE-CODE-UT204 TO SHIP-TYPE-CODE-UT20 OF
                FUTWM20(0004)
           END-IF.
           MOVE WK01-TYPE-CODE-UT204-F TO WK01-TYPE-CODE-UT20-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-UT205-Z = 'Y'
               MOVE WK01-TYPE-CODE-UT205 TO SHIP-TYPE-CODE-UT20 OF
                FUTWM20(0005)
           END-IF.
           MOVE WK01-TYPE-CODE-UT205-F TO WK01-TYPE-CODE-UT20-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-UT206-Z = 'Y'
               MOVE WK01-TYPE-CODE-UT206 TO SHIP-TYPE-CODE-UT20 OF
                FUTWM20(0006)
           END-IF.
           MOVE WK01-TYPE-CODE-UT206-F TO WK01-TYPE-CODE-UT20-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-UT207-Z = 'Y'
               MOVE WK01-TYPE-CODE-UT207 TO SHIP-TYPE-CODE-UT20 OF
                FUTWM20(0007)
           END-IF.
           MOVE WK01-TYPE-CODE-UT207-F TO WK01-TYPE-CODE-UT20-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-UT208-Z = 'Y'
               MOVE WK01-TYPE-CODE-UT208 TO SHIP-TYPE-CODE-UT20 OF
                FUTWM20(0008)
           END-IF.
           MOVE WK01-TYPE-CODE-UT208-F TO WK01-TYPE-CODE-UT20-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-UT209-Z = 'Y'
               MOVE WK01-TYPE-CODE-UT209 TO SHIP-TYPE-CODE-UT20 OF
                FUTWM20(0009)
           END-IF.
           MOVE WK01-TYPE-CODE-UT209-F TO WK01-TYPE-CODE-UT20-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-UT2010-Z = 'Y'
               MOVE WK01-TYPE-CODE-UT2010 TO SHIP-TYPE-CODE-UT20 OF
                FUTWM20(0010)
           END-IF.
           MOVE WK01-TYPE-CODE-UT2010-F TO WK01-TYPE-CODE-UT20-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-KEY-UT201-Z = 'Y'
               MOVE WK01-NAME-KEY-UT201 TO CNTCT-NAME-KEY-UT20 OF
                FUTWM20(0001)
           END-IF.
           MOVE WK01-NAME-KEY-UT201-F TO WK01-NAME-KEY-UT20-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-KEY-UT202-Z = 'Y'
               MOVE WK01-NAME-KEY-UT202 TO CNTCT-NAME-KEY-UT20 OF
                FUTWM20(0002)
           END-IF.
           MOVE WK01-NAME-KEY-UT202-F TO WK01-NAME-KEY-UT20-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-KEY-UT203-Z = 'Y'
               MOVE WK01-NAME-KEY-UT203 TO CNTCT-NAME-KEY-UT20 OF
                FUTWM20(0003)
           END-IF.
           MOVE WK01-NAME-KEY-UT203-F TO WK01-NAME-KEY-UT20-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-KEY-UT204-Z = 'Y'
               MOVE WK01-NAME-KEY-UT204 TO CNTCT-NAME-KEY-UT20 OF
                FUTWM20(0004)
           END-IF.
           MOVE WK01-NAME-KEY-UT204-F TO WK01-NAME-KEY-UT20-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-KEY-UT205-Z = 'Y'
               MOVE WK01-NAME-KEY-UT205 TO CNTCT-NAME-KEY-UT20 OF
                FUTWM20(0005)
           END-IF.
           MOVE WK01-NAME-KEY-UT205-F TO WK01-NAME-KEY-UT20-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-KEY-UT206-Z = 'Y'
               MOVE WK01-NAME-KEY-UT206 TO CNTCT-NAME-KEY-UT20 OF
                FUTWM20(0006)
           END-IF.
           MOVE WK01-NAME-KEY-UT206-F TO WK01-NAME-KEY-UT20-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-KEY-UT207-Z = 'Y'
               MOVE WK01-NAME-KEY-UT207 TO CNTCT-NAME-KEY-UT20 OF
                FUTWM20(0007)
           END-IF.
           MOVE WK01-NAME-KEY-UT207-F TO WK01-NAME-KEY-UT20-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-KEY-UT208-Z = 'Y'
               MOVE WK01-NAME-KEY-UT208 TO CNTCT-NAME-KEY-UT20 OF
                FUTWM20(0008)
           END-IF.
           MOVE WK01-NAME-KEY-UT208-F TO WK01-NAME-KEY-UT20-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-KEY-UT209-Z = 'Y'
               MOVE WK01-NAME-KEY-UT209 TO CNTCT-NAME-KEY-UT20 OF
                FUTWM20(0009)
           END-IF.
           MOVE WK01-NAME-KEY-UT209-F TO WK01-NAME-KEY-UT20-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-KEY-UT2010-Z = 'Y'
               MOVE WK01-NAME-KEY-UT2010 TO CNTCT-NAME-KEY-UT20 OF
                FUTWM20(0010)
           END-IF.
           MOVE WK01-NAME-KEY-UT2010-F TO WK01-NAME-KEY-UT20-F (10).
