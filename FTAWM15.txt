      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FTAWM15                              04/24/00  12:44  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FTAWM15.
           02  NAME-KEY-6117-TA15                       PIC X(35).
           02  MSG-DESC-TA15                            PIC X(17).
           02  EMPLY-IND-4400-TA15                      PIC X(1).
           02  ACTN-CODE-GROUP-TA15.
               03  ACTN-CODE-TA15           OCCURS 11   PIC X(1).
           02  ADVNC-NMBR-4404-TA15         OCCURS 11   PIC X(8).
           02  ADVNC-DATE-4404-TA15         OCCURS 11   PIC X(6).
           02  ADVNC-AMT-4404-TA15          OCCURS 11   PIC X(11).
           02  ADVNC-TYPE-4404-TA15         OCCURS 11   PIC X(4).
           02  ADVNC-STATUS-4404-TA15       OCCURS 11   PIC X(1).
           02  STATUS-DESC-TA15             OCCURS 11   PIC X(10).
           02  EVENT-NMBR-4401-TA15         OCCURS 11   PIC X(8).
           02  EVENT-STATUS-TA15            OCCURS 11   PIC X(10).
