    ***Created by Convert/DC version V8R03 on 11/01/00 at 09:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-GA11-Z = 'Y'
               MOVE WK01-CODE-GA11 TO RPRT-CODE-GA11 OF FGAWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-GA11-Z = 'Y'
               MOVE WK01-TITLE-GA11 TO RPRT-TITLE-GA11 OF FGAWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DEST-ID-GA11-Z = 'Y'
               MOVE WK01-DEST-ID-GA11 TO PRINTER-DEST-ID-GA11 OF FGAWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-EMAIL-ADDR-GA11-Z = 'Y'
               MOVE WK01-EMAIL-ADDR-GA11 TO EMAIL-ADDRESS-GA11
                                                        OF FGAWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-GA11-Z = 'Y'
               MOVE WK01-CLASS-GA11 TO JOB-CLASS-GA11 OF FGAWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA11-Z = 'Y'
               MOVE WK01-NAME-GA11 TO RECEIVER-NAME-GA11 OF FGAWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAILCODE-GA11-Z = 'Y'
               MOVE WK01-MAILCODE-GA11 TO RECEIVER-MAILCODE-GA11 OF
                FGAWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A11-Z = 'Y'
               MOVE WK01-NAME-GA1A11 TO FIELD-NAME-GA11 OF FGAWM11(0001)
           END-IF.
           MOVE WK01-NAME-GA1A11-F TO WK01-NAME-GA1A1-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A12-Z = 'Y'
               MOVE WK01-NAME-GA1A12 TO FIELD-NAME-GA11 OF FGAWM11(0002)
           END-IF.
           MOVE WK01-NAME-GA1A12-F TO WK01-NAME-GA1A1-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A13-Z = 'Y'
               MOVE WK01-NAME-GA1A13 TO FIELD-NAME-GA11 OF FGAWM11(0003)
           END-IF.
           MOVE WK01-NAME-GA1A13-F TO WK01-NAME-GA1A1-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A14-Z = 'Y'
               MOVE WK01-NAME-GA1A14 TO FIELD-NAME-GA11 OF FGAWM11(0004)
           END-IF.
           MOVE WK01-NAME-GA1A14-F TO WK01-NAME-GA1A1-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A15-Z = 'Y'
               MOVE WK01-NAME-GA1A15 TO FIELD-NAME-GA11 OF FGAWM11(0005)
           END-IF.
           MOVE WK01-NAME-GA1A15-F TO WK01-NAME-GA1A1-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A16-Z = 'Y'
               MOVE WK01-NAME-GA1A16 TO FIELD-NAME-GA11 OF FGAWM11(0006)
           END-IF.
           MOVE WK01-NAME-GA1A16-F TO WK01-NAME-GA1A1-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A17-Z = 'Y'
               MOVE WK01-NAME-GA1A17 TO FIELD-NAME-GA11 OF FGAWM11(0007)
           END-IF.
           MOVE WK01-NAME-GA1A17-F TO WK01-NAME-GA1A1-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A18-Z = 'Y'
               MOVE WK01-NAME-GA1A18 TO FIELD-NAME-GA11 OF FGAWM11(0008)
           END-IF.
           MOVE WK01-NAME-GA1A18-F TO WK01-NAME-GA1A1-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A19-Z = 'Y'
               MOVE WK01-NAME-GA1A19 TO FIELD-NAME-GA11 OF FGAWM11(0009)
           END-IF.
           MOVE WK01-NAME-GA1A19-F TO WK01-NAME-GA1A1-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A110-Z = 'Y'
               MOVE WK01-NAME-GA1A110 TO FIELD-NAME-GA11 OF
                FGAWM11(0010)
           END-IF.
           MOVE WK01-NAME-GA1A110-F TO WK01-NAME-GA1A1-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A111-Z = 'Y'
               MOVE WK01-NAME-GA1A111 TO FIELD-NAME-GA11 OF
                FGAWM11(0011)
           END-IF.
           MOVE WK01-NAME-GA1A111-F TO WK01-NAME-GA1A1-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A112-Z = 'Y'
               MOVE WK01-NAME-GA1A112 TO FIELD-NAME-GA11 OF
                FGAWM11(0012)
           END-IF.
           MOVE WK01-NAME-GA1A112-F TO WK01-NAME-GA1A1-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A113-Z = 'Y'
               MOVE WK01-NAME-GA1A113 TO FIELD-NAME-GA11 OF
                FGAWM11(0013)
           END-IF.
           MOVE WK01-NAME-GA1A113-F TO WK01-NAME-GA1A1-F (13).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A114-Z = 'Y'
               MOVE WK01-NAME-GA1A114 TO FIELD-NAME-GA11 OF
                FGAWM11(0014)
           END-IF.
           MOVE WK01-NAME-GA1A114-F TO WK01-NAME-GA1A1-F (14).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A115-Z = 'Y'
               MOVE WK01-NAME-GA1A115 TO FIELD-NAME-GA11 OF
                FGAWM11(0015)
           END-IF.
           MOVE WK01-NAME-GA1A115-F TO WK01-NAME-GA1A1-F (15).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A116-Z = 'Y'
               MOVE WK01-NAME-GA1A116 TO FIELD-NAME-GA11 OF
                FGAWM11(0016)
           END-IF.
           MOVE WK01-NAME-GA1A116-F TO WK01-NAME-GA1A1-F (16).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A117-Z = 'Y'
               MOVE WK01-NAME-GA1A117 TO FIELD-NAME-GA11 OF
                FGAWM11(0017)
           END-IF.
           MOVE WK01-NAME-GA1A117-F TO WK01-NAME-GA1A1-F (17).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A118-Z = 'Y'
               MOVE WK01-NAME-GA1A118 TO FIELD-NAME-GA11 OF
                FGAWM11(0018)
           END-IF.
           MOVE WK01-NAME-GA1A118-F TO WK01-NAME-GA1A1-F (18).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A119-Z = 'Y'
               MOVE WK01-NAME-GA1A119 TO FIELD-NAME-GA11 OF
                FGAWM11(0019)
           END-IF.
           MOVE WK01-NAME-GA1A119-F TO WK01-NAME-GA1A1-F (19).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A120-Z = 'Y'
               MOVE WK01-NAME-GA1A120 TO FIELD-NAME-GA11 OF
                FGAWM11(0020)
           END-IF.
           MOVE WK01-NAME-GA1A120-F TO WK01-NAME-GA1A1-F (20).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A121-Z = 'Y'
               MOVE WK01-NAME-GA1A121 TO FIELD-NAME-GA11 OF
                FGAWM11(0021)
           END-IF.
           MOVE WK01-NAME-GA1A121-F TO WK01-NAME-GA1A1-F (21).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A122-Z = 'Y'
               MOVE WK01-NAME-GA1A122 TO FIELD-NAME-GA11 OF
                FGAWM11(0022)
           END-IF.
           MOVE WK01-NAME-GA1A122-F TO WK01-NAME-GA1A1-F (22).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A123-Z = 'Y'
               MOVE WK01-NAME-GA1A123 TO FIELD-NAME-GA11 OF
                FGAWM11(0023)
           END-IF.
           MOVE WK01-NAME-GA1A123-F TO WK01-NAME-GA1A1-F (23).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-GA1A124-Z = 'Y'
               MOVE WK01-NAME-GA1A124 TO FIELD-NAME-GA11 OF
                FGAWM11(0024)
           END-IF.
           MOVE WK01-NAME-GA1A124-F TO WK01-NAME-GA1A1-F (24).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA111-Z = 'Y'
               MOVE WK01-VALUE-GA111 TO FIELD-VALUE-GA11 OF
                FGAWM11(0001)
           END-IF.
           MOVE WK01-VALUE-GA111-F TO WK01-VALUE-GA11-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA112-Z = 'Y'
               MOVE WK01-VALUE-GA112 TO FIELD-VALUE-GA11 OF
                FGAWM11(0002)
           END-IF.
           MOVE WK01-VALUE-GA112-F TO WK01-VALUE-GA11-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA113-Z = 'Y'
               MOVE WK01-VALUE-GA113 TO FIELD-VALUE-GA11 OF
                FGAWM11(0003)
           END-IF.
           MOVE WK01-VALUE-GA113-F TO WK01-VALUE-GA11-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA114-Z = 'Y'
               MOVE WK01-VALUE-GA114 TO FIELD-VALUE-GA11 OF
                FGAWM11(0004)
           END-IF.
           MOVE WK01-VALUE-GA114-F TO WK01-VALUE-GA11-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA115-Z = 'Y'
               MOVE WK01-VALUE-GA115 TO FIELD-VALUE-GA11 OF
                FGAWM11(0005)
           END-IF.
           MOVE WK01-VALUE-GA115-F TO WK01-VALUE-GA11-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA116-Z = 'Y'
               MOVE WK01-VALUE-GA116 TO FIELD-VALUE-GA11 OF
                FGAWM11(0006)
           END-IF.
           MOVE WK01-VALUE-GA116-F TO WK01-VALUE-GA11-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA117-Z = 'Y'
               MOVE WK01-VALUE-GA117 TO FIELD-VALUE-GA11 OF
                FGAWM11(0007)
           END-IF.
           MOVE WK01-VALUE-GA117-F TO WK01-VALUE-GA11-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA118-Z = 'Y'
               MOVE WK01-VALUE-GA118 TO FIELD-VALUE-GA11 OF
                FGAWM11(0008)
           END-IF.
           MOVE WK01-VALUE-GA118-F TO WK01-VALUE-GA11-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA119-Z = 'Y'
               MOVE WK01-VALUE-GA119 TO FIELD-VALUE-GA11 OF
                FGAWM11(0009)
           END-IF.
           MOVE WK01-VALUE-GA119-F TO WK01-VALUE-GA11-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA1110-Z = 'Y'
               MOVE WK01-VALUE-GA1110 TO FIELD-VALUE-GA11 OF
                FGAWM11(0010)
           END-IF.
           MOVE WK01-VALUE-GA1110-F TO WK01-VALUE-GA11-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA1111-Z = 'Y'
               MOVE WK01-VALUE-GA1111 TO FIELD-VALUE-GA11 OF
                FGAWM11(0011)
           END-IF.
           MOVE WK01-VALUE-GA1111-F TO WK01-VALUE-GA11-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA1112-Z = 'Y'
               MOVE WK01-VALUE-GA1112 TO FIELD-VALUE-GA11 OF
                FGAWM11(0012)
           END-IF.
           MOVE WK01-VALUE-GA1112-F TO WK01-VALUE-GA11-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA1113-Z = 'Y'
               MOVE WK01-VALUE-GA1113 TO FIELD-VALUE-GA11 OF
                FGAWM11(0013)
           END-IF.
           MOVE WK01-VALUE-GA1113-F TO WK01-VALUE-GA11-F (13).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA1114-Z = 'Y'
               MOVE WK01-VALUE-GA1114 TO FIELD-VALUE-GA11 OF
                FGAWM11(0014)
           END-IF.
           MOVE WK01-VALUE-GA1114-F TO WK01-VALUE-GA11-F (14).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA1115-Z = 'Y'
               MOVE WK01-VALUE-GA1115 TO FIELD-VALUE-GA11 OF
                FGAWM11(0015)
           END-IF.
           MOVE WK01-VALUE-GA1115-F TO WK01-VALUE-GA11-F (15).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA1116-Z = 'Y'
               MOVE WK01-VALUE-GA1116 TO FIELD-VALUE-GA11 OF
                FGAWM11(0016)
           END-IF.
           MOVE WK01-VALUE-GA1116-F TO WK01-VALUE-GA11-F (16).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA1117-Z = 'Y'
               MOVE WK01-VALUE-GA1117 TO FIELD-VALUE-GA11 OF
                FGAWM11(0017)
           END-IF.
           MOVE WK01-VALUE-GA1117-F TO WK01-VALUE-GA11-F (17).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA1118-Z = 'Y'
               MOVE WK01-VALUE-GA1118 TO FIELD-VALUE-GA11 OF
                FGAWM11(0018)
           END-IF.
           MOVE WK01-VALUE-GA1118-F TO WK01-VALUE-GA11-F (18).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA1119-Z = 'Y'
               MOVE WK01-VALUE-GA1119 TO FIELD-VALUE-GA11 OF
                FGAWM11(0019)
           END-IF.
           MOVE WK01-VALUE-GA1119-F TO WK01-VALUE-GA11-F (19).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA1120-Z = 'Y'
               MOVE WK01-VALUE-GA1120 TO FIELD-VALUE-GA11 OF
                FGAWM11(0020)
           END-IF.
           MOVE WK01-VALUE-GA1120-F TO WK01-VALUE-GA11-F (20).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA1121-Z = 'Y'
               MOVE WK01-VALUE-GA1121 TO FIELD-VALUE-GA11 OF
                FGAWM11(0021)
           END-IF.
           MOVE WK01-VALUE-GA1121-F TO WK01-VALUE-GA11-F (21).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA1122-Z = 'Y'
               MOVE WK01-VALUE-GA1122 TO FIELD-VALUE-GA11 OF
                FGAWM11(0022)
           END-IF.
           MOVE WK01-VALUE-GA1122-F TO WK01-VALUE-GA11-F (22).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA1123-Z = 'Y'
               MOVE WK01-VALUE-GA1123 TO FIELD-VALUE-GA11 OF
                FGAWM11(0023)
           END-IF.
           MOVE WK01-VALUE-GA1123-F TO WK01-VALUE-GA11-F (23).
      *%--------------------------------------------------------------%*
           IF WK01-VALUE-GA1124-Z = 'Y'
               MOVE WK01-VALUE-GA1124 TO FIELD-VALUE-GA11 OF
                FGAWM11(0024)
           END-IF.
           MOVE WK01-VALUE-GA1124-F TO WK01-VALUE-GA11-F (24).
