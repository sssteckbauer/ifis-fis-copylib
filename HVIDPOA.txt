       975-TO-R4061-PO-ACCT SECTION.
           MOVE POA-USER-CD TO
               USER-CODE-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-SEQ-NBR TO
               SEQ-NMBR-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-PCT-DSTBN TO
               PCT-DSTBN-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-COA-CD TO
               COA-CODE-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-ACCT-INDX-CD TO
               ACCT-INDX-CODE-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-FUND-CD TO
               FUND-CODE-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-ORGN-CD TO
               ORGZN-CODE-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-ACCT-CD TO
               ACCT-CODE-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-PRGRM-CD TO
               PRGRM-CODE-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-ACTVY-CD TO
               ACTVY-CODE-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-LCTN-CD TO
               LCTN-CODE-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-AMT TO
               AMT-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-PRJCT-CD TO
               PRJCT-CODE-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-ACCT-ERROR-IND TO
               ACCT-ERROR-IND-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-RULE-CLS-CD TO
               RULE-CLASS-CODE-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-DSCNT-RULE-CLS TO
               DSCNT-RULE-CLASS-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-TAX-RULE-CLS TO
               TAX-RULE-CLASS-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-ADDL-CHRG-RULE-CLS TO
               ADDL-CHRG-RULE-CLASS-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-FSCL-YR TO
               FSCL-YR-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-PSTNG-PRD TO
               PSTNG-PRD-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-NSF-OVRDE TO
               NSF-OVRDE-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-ADDL-CHRG TO
               ADDL-CHRG-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-TAX-AMT TO
               TAX-AMT-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-LIEN-CLOSED-CD TO
               LIEN-CLOSED-CODE-4061 OF R4061-PO-ACCT
                                                                     .
           MOVE POA-PO-DSCNT-AMT TO
               PO-DSCNT-AMT-4061 OF R4061-PO-ACCT
                                                                     .
       975-TO-R4061-PO-ACCT-EXIT.
           EXIT.
