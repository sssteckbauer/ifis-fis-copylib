      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM06                              04/24/00  12:22  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM06.
           02  ACTN-CODE-GROUP-CH06.
               03  ACTN-CODE-CH06                       PIC X(1).
           02  NAME-KEY-6117-CH06                       PIC X(35).
           02  MGR-TITLE-4019-CH06                      PIC X(35).
           02  COA-CODE-4019-CH06                       PIC X(1).
           02  DFLT-LCTN-CODE-4019-CH06                 PIC X(6).
           02  DFLT-ORGZN-CODE-4019-CH06                PIC X(6).
           02  START-DATE-4019-CH06                     PIC X(6).
           02  END-DATE-4019-CH06                       PIC X(6).
           02  LAST-ACTVY-DATE-4019-CH06                PIC X(6).
           02  ORGZN-TITLE-4019-CH06                    PIC X(35).
           02  LCTN-TITLE-4019-CH06                     PIC X(35).
