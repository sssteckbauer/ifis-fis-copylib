       01  R6440-ENTYDCDTXT.
           02  DB-PROC-ID-6440.
               03  USER-CODE-6440                       PIC X(8).
               03  LAST-ACTVY-DATE-6440                 PIC X(5).
               03  TRMNL-ID-6440                        PIC X(8).
               03  PURGE-FLAG-6440                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  CMNT-TEXT-6440                           PIC X(55).
