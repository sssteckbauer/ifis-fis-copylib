       975-TO-R4031-COST-CONTR SECTION.
           MOVE CCT-USER-CD TO
               USER-CODE-4031 OF R4031-COST-CONTR
                                                                     .
           MOVE CCT-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4031 OF R4031-COST-CONTR
                                                                     .
           MOVE CCT-CNTRB-PCT TO
               CNTRB-PCT-4031 OF R4031-COST-CONTR
                                                                     .
           MOVE CCT-ACCT-INDX-CD TO
               ACCT-INDX-CODE-4031 OF R4031-COST-CONTR
                                                                     .
           MOVE CCT-FUND-CD TO
               FUND-CODE-4031 OF R4031-COST-CONTR
                                                                     .
           MOVE CCT-ORGN-CD TO
               ORGZN-CODE-4031 OF R4031-COST-CONTR
                                                                     .
           MOVE CCT-ACCT-CD TO
               ACCT-CODE-4031 OF R4031-COST-CONTR
                                                                     .
           MOVE CCT-PRGRM-CD TO
               PRGRM-CODE-4031 OF R4031-COST-CONTR
                                                                     .
           MOVE CCT-ACTVY-CD TO
               ACTVY-CODE-4031 OF R4031-COST-CONTR
                                                                     .
           MOVE CCT-LCTN-CD TO
               LCTN-CODE-4031 OF R4031-COST-CONTR
                                                                     .
           MOVE CCT-COST-SHR-CR-ACCT TO
               COST-SHARE-CRDT-ACCT-4031 OF R4031-COST-CONTR
                                                                     .
       975-TO-R4031-COST-CONTR-EXIT.
           EXIT.
