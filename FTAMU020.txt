    ***Created by Convert/DC version V8R03 on 12/05/00 at 15:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6117-TA02-Z = 'Y'
               MOVE WK01-KEY-6117-TA02 TO NAME-KEY-6117-TA02 OF FTAWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-TA02-Z = 'Y'
               MOVE WK01-TEXT-TA02 TO MSG-TEXT-TA02 OF FTAWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-LONG-DESC-6137-TA02-Z = 'Y'
               MOVE WK01-LONG-DESC-6137-TA02 TO
                STNDD-LONG-DESC-6137-TA02 OF FTAWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4400-TA02-Z = 'Y'
               MOVE WK01-IND-4400-TA02 TO EMPLY-IND-4400-TA02 OF FTAWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-LST-NN-4400-T02-Z = 'Y'
               MOVE WK01-ACCT-LST-NN-4400-T02 TO
                TRVL-ACCT-LAST-NINE-4400-TA02 OF FTAWK02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6137-TA02-Z = 'Y'
               MOVE WK01-TYPE-CODE-6137-TA02 TO ADR-TYPE-CODE-6137-TA02
                OF FTAWK02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RQST-DATE-TA02-Z = 'Y'
               MOVE WK01-RQST-DATE-TA02 TO MAP-RQST-DATE-TA02 OF FTAWK02
           END-IF.
      *%--------------------------------------------------------------%*
DEVMJM* ACR46019
           IF WK01-UPDT-USER-ID-TA02-Z = 'Y'
               MOVE WK01-UPDT-USER-ID-TA02  TO UPDT-USER-ID-TA02
           END-IF.
      *%--------------------------------------------------------------%*
DEVMJM* ACR46019
           IF WK01-CODE-TA02-Z = 'Y'
               MOVE WK01-CODE-TA02 TO ACTN-CODE-TA02 OF FTAWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ADR-6139-TA021-Z = 'Y'
               MOVE WK01-ADR-6139-TA021 TO LINE-ADR-6139-TA02 OF
                FTAWM02(0001)
           END-IF.
           MOVE WK01-ADR-6139-TA021-F TO WK01-ADR-6139-TA02-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-6139-TA022-Z = 'Y'
               MOVE WK01-ADR-6139-TA022 TO LINE-ADR-6139-TA02 OF
                FTAWM02(0002)
           END-IF.
           MOVE WK01-ADR-6139-TA022-F TO WK01-ADR-6139-TA02-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-6139-TA023-Z = 'Y'
               MOVE WK01-ADR-6139-TA023 TO LINE-ADR-6139-TA02 OF
                FTAWM02(0003)
           END-IF.
           MOVE WK01-ADR-6139-TA023-F TO WK01-ADR-6139-TA02-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-6139-TA024-Z = 'Y'
               MOVE WK01-ADR-6139-TA024 TO LINE-ADR-6139-TA02 OF
                FTAWM02(0004)
           END-IF.
           MOVE WK01-ADR-6139-TA024-F TO WK01-ADR-6139-TA02-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6139-TA02-Z = 'Y'
               MOVE WK01-NAME-6139-TA02 TO CITY-NAME-6139-TA02 OF
                FTAWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6153-TA02-Z = 'Y'
               MOVE WK01-CODE-6153-TA02 TO CNTRY-CODE-6153-TA02 OF
                FTAWM02
           END-IF.
DEVMJM*
      *%--------------------------------------------------------------%*
           IF WK01-FRGN-PROV-TA02-Z = 'Y'
               MOVE WK01-FRGN-PROV-TA02 TO FOREIGN-PROV-CD-TA02 OF
                FTAWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FRGN-PROV-NAME-Z = 'Y'
               MOVE WK01-FRGN-PROV-NAME-TA02
                                        TO FOREIGN-PROV-NAME-TA02 OF
                FTAWM02
           END-IF.
DEVMJM*
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6139-TA02-Z = 'Y'
               MOVE WK01-DATE-6139-TA02 TO START-DATE-6139-TA02 OF
                FTAWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6139-TA0A-Z = 'Y'
               MOVE WK01-DATE-6139-TA0A TO END-DATE-6139-TA02 OF FTAWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-EMAIL-ADDR-6139-TA02-Z = 'Y'
               MOVE WK01-EMAIL-ADDR-6139-TA02
                 TO EMAIL-ADDR-6139-TA02 OF FTAWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6151-TA02-Z = 'Y'
               MOVE WK01-CODE-6151-TA02 TO STATE-CODE-6151-TA02 OF
                FTAWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6152-TA02-Z = 'Y'
               MOVE WK01-CODE-6152-TA02 TO ZIP-CODE-6152-TA02 OF FTAWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AREA-CODE-6139-TA02-Z = 'Y'
               MOVE WK01-AREA-CODE-6139-TA02 TO
                TLPHN-AREA-CODE-6139-TA02 OF FTAWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-XCHNG-ID-6139-TA02-Z = 'Y'
               MOVE WK01-XCHNG-ID-6139-TA02 TO TLPHN-XCHNG-ID-6139-TA02
                OF FTAWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-ID-6139-TA02-Z = 'Y'
               MOVE WK01-SEQ-ID-6139-TA02 TO TLPHN-SEQ-ID-6139-TA02 OF
                FTAWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-XTNSN-ID-6139-TA02-Z = 'Y'
               MOVE WK01-XTNSN-ID-6139-TA02 TO TLPHN-XTNSN-ID-6139-TA02
                OF FTAWM02
           END-IF.
