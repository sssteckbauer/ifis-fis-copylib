       01  R4042-EQVLN-UOM.
           02  DB-PROC-ID-4042.
               03  USER-CODE-4042                       PIC X(8).
               03  LAST-ACTVY-DATE-4042                 PIC X(5).
               03  TRMNL-ID-4042                        PIC X(8).
               03  PURGE-FLAG-4042                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  EQVLN-KEY-4042.
               03  UNVRS-CODE-4042                      PIC X(2).
               03  UNIT-MEA-CODE-4042                   PIC X(3).
               03  EQVLN-UNIT-OF-MEA-4042               PIC X(3).
           02  EQVLN-UNITS-4042               COMP-3    PIC 9(6)V9999.
