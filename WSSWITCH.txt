      *======================= START  WSSWITCH ======================*  01180010
      *  HOLD FIELDS , SWITCHES  AND  STUFF                          *  01190020
      ****************************************************************  01200030
       01  MISC-FIELDS.                                                 01820036
           03  WS-ABORT                 VALUE SPACES.                   01830036
               05  WS-ABORT-NUM         PICTURE 9(001).                 01830036
           03  WS-DATE-9                PICTURE 9(008).                 01830036
           03  WS-DATE-CYMD             REDEFINES WS-DATE-9.            01830036
               05  WS-DATE-CC           PICTURE 9(002).
               05  WS-DATE.
                   07  WS-DATE-YY       PICTURE 9(002).
                   07  WS-DATE-MM       PICTURE 9(002).
                   07  WS-DATE-DD       PICTURE 9(002).
           03  WS-TALLY                 PICTURE 9(001) VALUE 0.         01830036
           03  REQUEST-NBR              PICTURE 9(002) VALUE ZERO.      01900036
           03  HOLD-REQUEST-NBR         PICTURE 9(002) VALUE ZERO.      01910036
           03  HOLD-USER-ID             PICTURE X(008) VALUE SPACES.    01930036
           03  HOLD-START-DATE          PICTURE 9(008) VALUE ZERO.      01940036
           03  HOLD-COMM-DATE           PICTURE 9(008) VALUE ZERO.      01950036
           03  HOLD-TIME.
               05  HOLD-TIME-HRS        PICTURE X(002) VALUE SPACES.
               05  HOLD-TIME-MINS       PICTURE X(002) VALUE SPACES.
           03  W-SUB                    PICTURE 9(03)  VALUE ZERO.
           03  W-TIME-SUB               PICTURE 9(03)  VALUE ZERO.
           03  W-DAY-SUB                PICTURE 9(03)  VALUE ZERO.
           03  SW-IDMS-BINDS-DONE       PICTURE X(01)  VALUE 'N'.       SCS00320
               88 IDMS-BINDS-DONE                      VALUE 'Y'.
      *======================= END    WSSWITCH ======================*  01180010
