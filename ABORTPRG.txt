      *
      *      ** STANDARD PART OF ABORT PROCESS
      *      ** MODIFIED 6/91 TO CHANGE PROGRAM-NAME TO PROG-NAME
      *
           DISPLAY  '******************************************'.
           DISPLAY  '******************************************'.
           DISPLAY  '******************************************'.
      *
           DISPLAY  '* PROGRAM '  PROG-NAME  ' ABORTED'.
      *
           SET  MSG-ABORT-INDEX  TO  1.
           SEARCH  MSG-ABORT
              AT END
                  DISPLAY  '9999- INTERNAL PROGRAM ERROR.  ABORT-CODE '
                  ABORT-CODE
                  ' NOT FOUND IN MSG-ABORT-TABLE'
              WHEN (MSG-ABORT-CODE (MSG-ABORT-INDEX) = ABORT-CODE)
                  DISPLAY  MSG-ABORT (MSG-ABORT-INDEX).
      *
           DISPLAY  '******************************************'.
           DISPLAY  '******************************************'.
           DISPLAY  '******************************************'.

           MOVE 1000 TO RETURN-CODE.
