    ***Created by Convert/DC version V8R03 on 12/05/00 at 14:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4000-TA30-Z = 'Y'
               MOVE WK01-CODE-4000-TA30 TO COA-CODE-4000-TA30 OF FTAWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4417-TA30-Z = 'Y'
               MOVE WK01-DESC-4417-TA30 TO FULL-DESC-4417-TA30 OF
                FTAWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4417-TA3A-Z = 'Y'
               MOVE WK01-DESC-4417-TA3A TO SHORT-DESC-4417-TA30 OF
                FTAWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-4180-TA30-Z = 'Y'
               MOVE WK01-CLASS-CODE-4180-TA30 TO
                RULE-CLASS-CODE-4180-TA30 OF FTAWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-DESC-4181-TA30-Z = 'Y'
               MOVE WK01-CLASS-DESC-4181-TA30 TO
                RULE-CLASS-DESC-4181-TA30 OF FTAWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-CODE-4033-TA30-Z = 'Y'
               MOVE WK01-ACCT-CODE-4033-TA30 TO
                BANK-ACCT-CODE-4033-TA30 OF FTAWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-DESC-4062-TA30-Z = 'Y'
               MOVE WK01-CODE-DESC-4062-TA30 TO
                BANK-CODE-DESC-4062-TA30 OF FTAWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-4417-TA30-Z = 'Y'
               MOVE WK01-INDX-CODE-4417-TA30 TO
                ACCT-INDX-CODE-4417-TA30 OF FTAWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4417-TA30-Z = 'Y'
               MOVE WK01-CODE-4417-TA30 TO FUND-CODE-4417-TA30 OF
                FTAWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4417-TA3A-Z = 'Y'
               MOVE WK01-CODE-4417-TA3A TO ORGZN-CODE-4417-TA30 OF
                FTAWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4417-TA3B-Z = 'Y'
               MOVE WK01-CODE-4417-TA3B TO ACCT-CODE-4417-TA30 OF
                FTAWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4417-TA3C-Z = 'Y'
               MOVE WK01-CODE-4417-TA3C TO PRGRM-CODE-4417-TA30 OF
                FTAWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4417-TA3D-Z = 'Y'
               MOVE WK01-CODE-4417-TA3D TO ACTVY-CODE-4417-TA30 OF
                FTAWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4417-TA3E-Z = 'Y'
               MOVE WK01-CODE-4417-TA3E TO LCTN-CODE-4417-TA30 OF
                FTAWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CHECK-IND-4417-TA30-Z = 'Y'
               MOVE WK01-CHECK-IND-4417-TA30 TO
                IMMDT-CHECK-IND-4417-TA30 OF FTAWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CHECK-IND-4417-TA3A-Z = 'Y'
               MOVE WK01-CHECK-IND-4417-TA3A TO MNL-CHECK-IND-4417-TA30
                OF FTAWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CHECK-IND-4417-TA3B-Z = 'Y'
               MOVE WK01-CHECK-IND-4417-TA3B TO
                BATCH-CHECK-IND-4417-TA30 OF FTAWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4417-TA30-Z = 'Y'
               MOVE WK01-IND-4417-TA30 TO CASH-IND-4417-TA30 OF FTAWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-TA30-Z = 'Y'
               MOVE WK01-CODE-TA30 TO ACTN-CODE-TA30 OF FTAWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-4417-TA30-Z = 'Y'
               MOVE WK01-TYPE-4417-TA30 TO ADVNC-TYPE-4417-TA30 OF
                FTAWK30
           END-IF.
