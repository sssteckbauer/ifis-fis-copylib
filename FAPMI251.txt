    ***Created by Convert/DC version V8R03 on 12/01/00 at 17:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE SLCTN-STATUS-AP25 OF FAPWK25 TO WK01-STATUS-AP25.
      *%--------------------------------------------------------------%*
           MOVE TAX-CODE-REQUEST-AP25 OF FAPWK25 TO
                WK01-CODE-REQUEST-AP25.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-AP25 OF FAPWM25(0001) TO WK01-CODE-AP251.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-AP25 OF FAPWM25(0002) TO WK01-CODE-AP252.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-AP25 OF FAPWM25(0003) TO WK01-CODE-AP253.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-AP25 OF FAPWM25(0004) TO WK01-CODE-AP254.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-AP25 OF FAPWM25(0005) TO WK01-CODE-AP255.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-AP25 OF FAPWM25(0006) TO WK01-CODE-AP256.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-AP25 OF FAPWM25(0007) TO WK01-CODE-AP257.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-AP25 OF FAPWM25(0008) TO WK01-CODE-AP258.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-AP25 OF FAPWM25(0009) TO WK01-CODE-AP259.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-AP25 OF FAPWM25(0010) TO WK01-CODE-AP2510.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-AP25 OF FAPWM25(0011) TO WK01-CODE-AP2511.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-DESC-4154-AP25 OF FAPWM25(0001) TO
                WK01-RATE-DESC-4154-AP251.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-DESC-4154-AP25 OF FAPWM25(0002) TO
                WK01-RATE-DESC-4154-AP252.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-DESC-4154-AP25 OF FAPWM25(0003) TO
                WK01-RATE-DESC-4154-AP253.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-DESC-4154-AP25 OF FAPWM25(0004) TO
                WK01-RATE-DESC-4154-AP254.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-DESC-4154-AP25 OF FAPWM25(0005) TO
                WK01-RATE-DESC-4154-AP255.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-DESC-4154-AP25 OF FAPWM25(0006) TO
                WK01-RATE-DESC-4154-AP256.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-DESC-4154-AP25 OF FAPWM25(0007) TO
                WK01-RATE-DESC-4154-AP257.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-DESC-4154-AP25 OF FAPWM25(0008) TO
                WK01-RATE-DESC-4154-AP258.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-DESC-4154-AP25 OF FAPWM25(0009) TO
                WK01-RATE-DESC-4154-AP259.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-DESC-4154-AP25 OF FAPWM25(0010) TO
                WK01-RATE-DESC-4154-AP2510.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-DESC-4154-AP25 OF FAPWM25(0011) TO
                WK01-RATE-DESC-4154-AP2511.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-PCT-4154-AP25 OF FAPWM25(0001) TO
                WK01-RATE-PCT-4154-AP251.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-PCT-4154-AP25 OF FAPWM25(0002) TO
                WK01-RATE-PCT-4154-AP252.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-PCT-4154-AP25 OF FAPWM25(0003) TO
                WK01-RATE-PCT-4154-AP253.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-PCT-4154-AP25 OF FAPWM25(0004) TO
                WK01-RATE-PCT-4154-AP254.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-PCT-4154-AP25 OF FAPWM25(0005) TO
                WK01-RATE-PCT-4154-AP255.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-PCT-4154-AP25 OF FAPWM25(0006) TO
                WK01-RATE-PCT-4154-AP256.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-PCT-4154-AP25 OF FAPWM25(0007) TO
                WK01-RATE-PCT-4154-AP257.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-PCT-4154-AP25 OF FAPWM25(0008) TO
                WK01-RATE-PCT-4154-AP258.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-PCT-4154-AP25 OF FAPWM25(0009) TO
                WK01-RATE-PCT-4154-AP259.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-PCT-4154-AP25 OF FAPWM25(0010) TO
                WK01-RATE-PCT-4154-AP2510.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-PCT-4154-AP25 OF FAPWM25(0011) TO
                WK01-RATE-PCT-4154-AP2511.
      *%--------------------------------------------------------------%*
           MOVE EFCTV-DATE-4154-AP25 OF FAPWM25(0001) TO
                WK01-DATE-4154-AP251.
      *%--------------------------------------------------------------%*
           MOVE EFCTV-DATE-4154-AP25 OF FAPWM25(0002) TO
                WK01-DATE-4154-AP252.
      *%--------------------------------------------------------------%*
           MOVE EFCTV-DATE-4154-AP25 OF FAPWM25(0003) TO
                WK01-DATE-4154-AP253.
      *%--------------------------------------------------------------%*
           MOVE EFCTV-DATE-4154-AP25 OF FAPWM25(0004) TO
                WK01-DATE-4154-AP254.
      *%--------------------------------------------------------------%*
           MOVE EFCTV-DATE-4154-AP25 OF FAPWM25(0005) TO
                WK01-DATE-4154-AP255.
      *%--------------------------------------------------------------%*
           MOVE EFCTV-DATE-4154-AP25 OF FAPWM25(0006) TO
                WK01-DATE-4154-AP256.
      *%--------------------------------------------------------------%*
           MOVE EFCTV-DATE-4154-AP25 OF FAPWM25(0007) TO
                WK01-DATE-4154-AP257.
      *%--------------------------------------------------------------%*
           MOVE EFCTV-DATE-4154-AP25 OF FAPWM25(0008) TO
                WK01-DATE-4154-AP258.
      *%--------------------------------------------------------------%*
           MOVE EFCTV-DATE-4154-AP25 OF FAPWM25(0009) TO
                WK01-DATE-4154-AP259.
      *%--------------------------------------------------------------%*
           MOVE EFCTV-DATE-4154-AP25 OF FAPWM25(0010) TO
                WK01-DATE-4154-AP2510.
      *%--------------------------------------------------------------%*
           MOVE EFCTV-DATE-4154-AP25 OF FAPWM25(0011) TO
                WK01-DATE-4154-AP2511.
      *%--------------------------------------------------------------%*
           MOVE TERM-DATE-4154-AP25 OF FAPWM25(0001) TO
                WK01-DATE-4154-AP2A1.
      *%--------------------------------------------------------------%*
           MOVE TERM-DATE-4154-AP25 OF FAPWM25(0002) TO
                WK01-DATE-4154-AP2A2.
      *%--------------------------------------------------------------%*
           MOVE TERM-DATE-4154-AP25 OF FAPWM25(0003) TO
                WK01-DATE-4154-AP2A3.
      *%--------------------------------------------------------------%*
           MOVE TERM-DATE-4154-AP25 OF FAPWM25(0004) TO
                WK01-DATE-4154-AP2A4.
      *%--------------------------------------------------------------%*
           MOVE TERM-DATE-4154-AP25 OF FAPWM25(0005) TO
                WK01-DATE-4154-AP2A5.
      *%--------------------------------------------------------------%*
           MOVE TERM-DATE-4154-AP25 OF FAPWM25(0006) TO
                WK01-DATE-4154-AP2A6.
      *%--------------------------------------------------------------%*
           MOVE TERM-DATE-4154-AP25 OF FAPWM25(0007) TO
                WK01-DATE-4154-AP2A7.
      *%--------------------------------------------------------------%*
           MOVE TERM-DATE-4154-AP25 OF FAPWM25(0008) TO
                WK01-DATE-4154-AP2A8.
      *%--------------------------------------------------------------%*
           MOVE TERM-DATE-4154-AP25 OF FAPWM25(0009) TO
                WK01-DATE-4154-AP2A9.
      *%--------------------------------------------------------------%*
           MOVE TERM-DATE-4154-AP25 OF FAPWM25(0010) TO
                WK01-DATE-4154-AP2A10.
      *%--------------------------------------------------------------%*
           MOVE TERM-DATE-4154-AP25 OF FAPWM25(0011) TO
                WK01-DATE-4154-AP2A11.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-CODE-4168-AP25 OF FAPWM25(0001) TO
                WK01-RATE-CODE-4168-AP251.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-CODE-4168-AP25 OF FAPWM25(0002) TO
                WK01-RATE-CODE-4168-AP252.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-CODE-4168-AP25 OF FAPWM25(0003) TO
                WK01-RATE-CODE-4168-AP253.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-CODE-4168-AP25 OF FAPWM25(0004) TO
                WK01-RATE-CODE-4168-AP254.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-CODE-4168-AP25 OF FAPWM25(0005) TO
                WK01-RATE-CODE-4168-AP255.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-CODE-4168-AP25 OF FAPWM25(0006) TO
                WK01-RATE-CODE-4168-AP256.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-CODE-4168-AP25 OF FAPWM25(0007) TO
                WK01-RATE-CODE-4168-AP257.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-CODE-4168-AP25 OF FAPWM25(0008) TO
                WK01-RATE-CODE-4168-AP258.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-CODE-4168-AP25 OF FAPWM25(0009) TO
                WK01-RATE-CODE-4168-AP259.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-CODE-4168-AP25 OF FAPWM25(0010) TO
                WK01-RATE-CODE-4168-AP2510.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-CODE-4168-AP25 OF FAPWM25(0011) TO
                WK01-RATE-CODE-4168-AP2511.
      *%--------------------------------------------------------------%*
           MOVE TAX-TYPE-AP25 OF FAPWM25(0001) TO WK01-TYPE-AP251.
      *%--------------------------------------------------------------%*
           MOVE TAX-TYPE-AP25 OF FAPWM25(0002) TO WK01-TYPE-AP252.
      *%--------------------------------------------------------------%*
           MOVE TAX-TYPE-AP25 OF FAPWM25(0003) TO WK01-TYPE-AP253.
      *%--------------------------------------------------------------%*
           MOVE TAX-TYPE-AP25 OF FAPWM25(0004) TO WK01-TYPE-AP254.
      *%--------------------------------------------------------------%*
           MOVE TAX-TYPE-AP25 OF FAPWM25(0005) TO WK01-TYPE-AP255.
      *%--------------------------------------------------------------%*
           MOVE TAX-TYPE-AP25 OF FAPWM25(0006) TO WK01-TYPE-AP256.
      *%--------------------------------------------------------------%*
           MOVE TAX-TYPE-AP25 OF FAPWM25(0007) TO WK01-TYPE-AP257.
      *%--------------------------------------------------------------%*
           MOVE TAX-TYPE-AP25 OF FAPWM25(0008) TO WK01-TYPE-AP258.
      *%--------------------------------------------------------------%*
           MOVE TAX-TYPE-AP25 OF FAPWM25(0009) TO WK01-TYPE-AP259.
      *%--------------------------------------------------------------%*
           MOVE TAX-TYPE-AP25 OF FAPWM25(0010) TO WK01-TYPE-AP2510.
      *%--------------------------------------------------------------%*
           MOVE TAX-TYPE-AP25 OF FAPWM25(0011) TO WK01-TYPE-AP2511.
