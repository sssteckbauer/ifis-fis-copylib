       01  R4080-RQST-ACCT.
           02  DB-PROC-ID-4080.
               03  USER-CODE-4080                       PIC X(8).
               03  LAST-ACTVY-DATE-4080                 PIC X(5).
               03  TRMNL-ID-4080                        PIC X(8).
               03  PURGE-FLAG-4080                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  SEQ-NMBR-4080                            PIC 9(4).
           02  PCT-DSTBN-4080                 COMP-3    PIC 9(3)V999.
           02  ACTVY-DATE-4080                COMP-3    PIC S9(8).
           02  ACCT-AMT-4080                  COMP-3    PIC 9(10)V99.
           02  ACCT-ERROR-IND-4080                      PIC X(1).
           02  COA-CODE-4080                            PIC X(1).
           02  ACCT-INDX-CODE-4080                      PIC X(10).
           02  FUND-CODE-4080                           PIC X(6).
           02  ORGZN-CODE-4080                          PIC X(6).
           02  ACCT-CODE-4080                           PIC X(6).
           02  PRGRM-CODE-4080                          PIC X(6).
           02  ACTVY-CODE-4080                          PIC X(6).
           02  LCTN-CODE-4080                           PIC X(6).
           02  RULE-CLASS-CODE-4080                     PIC X(4).
           02  FSCL-YR-4080                             PIC X(2).
           02  PSTNG-PRD-4080                           PIC X(2).
           02  NSF-OVRDE-4080                           PIC X(1).
           02  LIEN-CLOSED-CODE-4080                    PIC X(1).
           02  GOVNMT-OWNED-IND-4080                    PIC X(1).
