    ***Created by Convert/DC version V8R03 on 11/09/00 at 16:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-GA14-Z = 'Y'
               MOVE WK01-INDX-CODE-GA14 TO ACCT-INDX-CODE-GA14 OF
                FGAWK14
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INDX-TITLE-4067-GA14-Z = 'Y'
               MOVE WK01-INDX-TITLE-4067-GA14 TO
                ACCT-INDX-TITLE-4067-GA14 OF FGAWM14
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4005-GA14-Z = 'Y'
               MOVE WK01-TITLE-4005-GA14 TO FUND-TITLE-4005-GA14 OF
                FGAWM14
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4010-GA14-Z = 'Y'
               MOVE WK01-TITLE-4010-GA14 TO ORGZN-TITLE-4010-GA14 OF
                FGAWM14
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-TITLE-4034-GA14-Z = 'Y'
               MOVE WK01-CODE-TITLE-4034-GA14 TO
                ACCT-CODE-TITLE-4034-GA14 OF FGAWM14
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4105-GA14-Z = 'Y'
               MOVE WK01-TITLE-4105-GA14 TO PRGRM-TITLE-4105-GA14 OF
                FGAWM14
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-GA14-Z = 'Y'
               MOVE WK01-CODE-GA14 TO COA-CODE-GA14 OF FGAWK14
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-YR-GA14-Z = 'Y'
               MOVE WK01-YR-GA14 TO FSCL-YR-GA14 OF FGAWK14
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PRD-GA14-Z = 'Y'
               MOVE WK01-PRD-GA14 TO ACTG-PRD-GA14 OF FGAWK14
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-GA1A-Z = 'Y'
               MOVE WK01-CODE-GA1A TO FUND-CODE-GA14 OF FGAWK14
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-GA1B-Z = 'Y'
               MOVE WK01-CODE-GA1B TO ORGZN-CODE-GA14 OF FGAWK14
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-GA1C-Z = 'Y'
               MOVE WK01-CODE-GA1C TO ACCT-CODE-GA14 OF FGAWK14
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-GA1D-Z = 'Y'
               MOVE WK01-CODE-GA1D TO PRGRM-CODE-GA14 OF FGAWK14
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-4024-GA14-Z = 'Y'
               MOVE WK01-START-DATE-4024-GA14 TO
                PRD-START-DATE-4024-GA14 OF FGAWK14
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-4024-GA14-Z = 'Y'
               MOVE WK01-END-DATE-4024-GA14 TO PRD-END-DATE-4024-GA14
                OF FGAWK14
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-GA14-Z = 'Y'
               MOVE WK01-TYPE-GA14 TO TRANSACT-TYPE-GA14 OF FGAWM14
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-OPTN-GA14-Z = 'Y'
               MOVE WK01-OPTN-GA14 TO DSPLY-OPTN-GA14 OF FGAWM14
           END-IF.
