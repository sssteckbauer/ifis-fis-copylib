       975-TO-R6186-ENTY-ID SECTION.
           MOVE ENI-USER-CD TO
               USER-CODE-6186 OF R6186-ENTY-ID
                                                                     .
           MOVE ENI-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-6186 OF R6186-ENTY-ID
                                                                     .
           MOVE ENI-UNVRS-CD TO
               UNVRS-CODE-6186 OF R6186-ENTY-ID
                                                                     .
           MOVE ENI-ENTY-ID-DGT-ONE TO
               ENTY-ID-DIGIT-ONE-6186 OF R6186-ENTY-ID
                                                                     .
           MOVE ENI-ENTY-ID-LST-NINE TO
               ENTY-ID-LAST-NINE-6186 OF R6186-ENTY-ID
                                                                     .
           MOVE ENI-FK2-ECG-CHG-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               CHANGE-DATE-6186 OF R6186-ENTY-ID
                                                                     .
       975-TO-R6186-ENTY-ID-EXIT.
           EXIT.
