      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FEXWD70                              04/24/00  13:06  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FEXWD70.
           02  SYSTEM-DATE-EX70               COMP-3    PIC S9(8).
           02  SET-SORT-KEY-4012-EX70.
               03  OPTN-1-CODE-EX70                     PIC X(8).
               03  OPTN-2-CODE-EX70                     PIC X(8).
               03  LEVEL-NMBR-EX70                      PIC 9(2).
           02  CHECK-VNDR-KEY-EX70.
               03  CHECK-NMBR-KEY-EX70                  PIC X(8).
               03  VNDR-CODE-KEY-EX70.
                   04  VNDR-ID-DIGIT-ONE-KEY-EX70       PIC X(1).
                   04  VNDR-ID-LAST-NINE-KEY-EX70       PIC X(9).
