      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM50                              04/24/00  12:25  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM50.
           02  NAME-KEY-6311-CH50                       PIC X(35).
           02  STATUS-4046-CH50                         PIC X(8).
           02  ACTN-CODE-CH50                           PIC X(1).
           02  CNTCT-NAME-4046-CH50                     PIC X(35).
           02  BASIC-TLPHN-ID-4046-CH50.
               03  TLPHN-AREA-CODE-4046-CH50            PIC X(3).
               03  TLPHN-XCHNG-ID-4046-CH50             PIC X(3).
               03  TLPHN-SEQ-ID-4046-CH50               PIC X(4).
           02  TLPHN-XTNSN-ID-4046-CH50                 PIC X(4).
