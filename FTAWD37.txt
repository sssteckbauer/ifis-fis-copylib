      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FTAWD37                              04/24/00  13:20  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FTAWD37.
           02  SAVE-DBKEY-ID-ADVNC-TA37       COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-EVENT-TA37       COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-EVENT-NEW-TA37   COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-E-TRAVELER-TA37  COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-E-TRAV-NEW-TA37  COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-TRIP-TA37        COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-TRAV-NEW-TA37    COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-TRAVELER-TA37    COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-EXPNS-TA37       COMP      PIC S9(8).
           02  FIELD-KEY-TA37.
               03  UNVRS-CODE-TA37                      PIC X(2).
               03  FIELD-CODE-TA37                      PIC X(10).
