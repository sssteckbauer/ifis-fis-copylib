      ****************************************************************
      *                    CHANGE LOG                                *
      * 10-15-85   BVB   ADDED LEVEL 10 JOURNAL TYPE.                *
      * 11-11-91   MLJ   ADDED IFIS IFOAPAL.                         *
      ****************************************************************
      *01  PAYROLL-BJE-RECORD.
           05  BJE-TYPE-ENTRY          PIC X(02).
           05  BJE-LOCATION            PIC X(01).
               88  BJE-LOCAL           VALUE '1' THRU '9'.
               88  BJE-UWIDE           VALUE 'J' THRU 'R'.
           05  BJE-ACCOUNT-NO          PIC X(06).
           05  BJE-FUND-NO             PIC X(05).
           05  BJE-SUB-BUDGET-CD       PIC X(02).
           05  BJE-TITLE-CD            PIC X(04).
           05  BJE-DATE.
               10  BJE-MONTH           PIC X(02).
               10  BJE-DAY             PIC X(02).
               10  BJE-YEAR            PIC X(01).
           05  BJE-DESCRIPTION         PIC X(18).
           05  BJE-REFERENCE-NO        PIC X(06).
           05  FILLER                  PIC X(04).
           05  BJE-SUFFIX              PIC X(01).
           05  FILLER                  PIC X(04).
           05  BJE-JOURNAL-NO.
               10  FILLER              PIC X(04).
               10  BJE-JOURNAL-TYPE    PIC X(01).
           05  BJE-FTE                 PIC X(07).
           05  BJE-FTE-9               REDEFINES BJE-FTE
                                       PIC 9(05)V99.
           05  BJE-AMOUNT              PIC X(10).
           05  BJE-AMOUNT-9            REDEFINES BJE-AMOUNT
                                       PIC S9(08)V99.
           05  BJE-IFIS-IFOAPAL.
               10  BJE-IFIS-INDEX      PIC X(10).
               10  BJE-IFIS-FUND       PIC X(06).
               10  BJE-IFIS-ORG        PIC X(06).
               10  BJE-IFIS-ACCOUNT    PIC X(06).
               10  BJE-IFIS-PROGRAM    PIC X(06).
               10  BJE-IFIS-ACTIVITY   PIC X(06).
               10  BJE-IFIS-LOCATION   PIC X(06).
