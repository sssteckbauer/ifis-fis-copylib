      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWK88                              04/24/00  12:21  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWK88.
           02  RQST-COA-CODE-CH88                       PIC X(1).
           02  RQST-ACCT-INDX-CODE-CH88                 PIC X(10).
           02  SLCTN-DATE-CH88                          PIC X(6).
           02  SLCTN-STATUS-CH88                        PIC X(1).
