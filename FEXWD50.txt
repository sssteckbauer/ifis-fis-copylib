      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FEXWD50                              04/24/00  13:06  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FEXWD50.
           02  INDX-KEY-4073-EX50.
               03  UNVRS-CODE-KEY-EX50                  PIC X(2).
               03  VNDR-CODE-KEY-EX50.
                   04  VNDR-ID-DIGIT-ONE-KEY-EX50       PIC X(1).
                   04  VNDR-ID-LAST-NINE-KEY-EX50       PIC X(9).
           02  SET-SORT-KEY-6311-EX50.
               03  UNVRS-CODE-EX50                      PIC X(2).
               03  INTRL-REF-ID-EX50          COMP-3    PIC S9(7).
           02  SYSTEM-DATE-EX50               COMP-3    PIC S9(8).
           02  SAVE-DBKEY-ID-EX50                       PIC X(38).
