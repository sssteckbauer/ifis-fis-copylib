       975-TO-R6202-ACH-COMM SECTION.
           MOVE ACM-USER-CD TO
               USER-CODE-6202 OF R6202-ACH-COMM
                                                                     .
           MOVE ACM-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-6202 OF R6202-ACH-COMM
                                                                     .
           MOVE ACM-COMM-PLAN-CD TO
               COMM-PLAN-CODE-6202 OF R6202-ACH-COMM
                                                                     .
           MOVE ACM-TEXT-CD TO
               TEXT-CODE-6202 OF R6202-ACH-COMM
                                                                     .
           MOVE ACM-END-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               END-DATE-6202 OF R6202-ACH-COMM
                                                                     .
           MOVE ACM-START-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               START-DATE-6202 OF R6202-ACH-COMM
                                                                     .
           MOVE ACM-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACTVY-DATE-6202 OF R6202-ACH-COMM
                                                                     .
       975-TO-R6202-ACH-COMM-EXIT.
           EXIT.
