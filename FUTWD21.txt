      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FUTWD21                              04/24/00  13:22  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FUTWD21.
           02  NAME-INDX-KEY-WD-UT21                    PIC X(30).
           02  SRCH-CONCLUDED-SW-UT21                   PIC X(1).
           02  SELECT-FLAG-WD-UT21                      PIC X(1).
           02  DBKEY-PLUS-ONE-RESET-WD-UT21             PIC X(1).
           02  SYSTEM-DATE-WD-UT21            COMP-3    PIC S9(8).
