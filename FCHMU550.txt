    ***Created by Convert/DC version V8R03 on 11/09/00 at 14:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY01-Z = 'Y'
               MOVE WK01-TEXT-SY01 TO DCSD-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0A-Z = 'Y'
               MOVE WK01-TEXT-SY0A TO CNFDL-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0B-Z = 'Y'
               MOVE WK01-TEXT-SY0B TO HOLDS-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-4180-CH55-Z = 'Y'
               MOVE WK01-CLASS-CODE-4180-CH55 TO
                RULE-CLASS-CODE-4180-CH55 OF FCHWK55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-DESC-4181-CH55-Z = 'Y'
               MOVE WK01-CLASS-DESC-4181-CH55 TO
                RULE-CLASS-DESC-4181-CH55 OF FCHWM55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-TYPE-4181-CH55-Z = 'Y'
               MOVE WK01-CLASS-TYPE-4181-CH55 TO
                RULE-CLASS-TYPE-4181-CH55 OF FCHWM55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CLSS-TYP-DSC-4181-CH55-Z = 'Y'
               MOVE WK01-CLSS-TYP-DSC-4181-CH55 TO
                RULE-CLASS-TYPE-DESC-4181-CH55 OF FCHWM55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4181-CH55-Z = 'Y'
               MOVE WK01-DATE-4181-CH55 TO START-DATE-4181-CH55 OF
                FCHWM55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4180-CH55-Z = 'Y'
               MOVE WK01-DATE-4180-CH55 TO ACTVY-DATE-4180-CH55 OF
                FCHWM55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STAMP-4181-CH55-Z = 'Y'
               MOVE WK01-STAMP-4181-CH55 TO TIME-STAMP-4181-CH55 OF
                FCHWM55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-CH55-Z = 'Y'
               MOVE WK01-PM-FLAG-CH55 TO AM-PM-FLAG-CH55 OF FCHWM55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH55-Z = 'Y'
               MOVE WK01-CODE-CH55 TO ACTN-CODE-CH55 OF FCHWM55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4184-CH55-Z = 'Y'
               MOVE WK01-NMBR-4184-CH55 TO SEQ-NMBR-4184-CH55 OF FCHWK55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-4184-CH55-Z = 'Y'
               MOVE WK01-NAME-4184-CH55 TO ELMNT-NAME-4184-CH55 OF
                FCHWM55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-DESC-4184-CH55-Z = 'Y'
               MOVE WK01-NAME-DESC-4184-CH55 TO
                ELMNT-NAME-DESC-4184-CH55 OF FCHWM55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4184-CH55-Z = 'Y'
               MOVE WK01-CODE-4184-CH55 TO EDIT-CODE-4184-CH55 OF
                FCHWM55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-DESC-4184-CH55-Z = 'Y'
               MOVE WK01-CODE-DESC-4184-CH55 TO
                EDIT-CODE-DESC-4184-CH55 OF FCHWM55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SVRTY-IND-4184-CH55-Z = 'Y'
               MOVE WK01-SVRTY-IND-4184-CH55 TO
                ERROR-SVRTY-IND-4184-CH55 OF FCHWM55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SVRTY-DESC-4184-CH55-Z = 'Y'
               MOVE WK01-SVRTY-DESC-4184-CH55 TO
                ERROR-SVRTY-DESC-4184-CH55 OF FCHWM55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-4184-CH55-Z = 'Y'
               MOVE WK01-ERROR-IND-4184-CH55 TO
                CNTNU-ERROR-IND-4184-CH55 OF FCHWM55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MSG-4184-CH55-Z = 'Y'
               MOVE WK01-MSG-4184-CH55 TO ERROR-MSG-4184-CH55 OF FCHWM55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-4184-CH55-Z = 'Y'
               MOVE WK01-4184-CH55 TO OPER-4184-CH55 OF FCHWM55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4184-CH55-Z = 'Y'
               MOVE WK01-DESC-4184-CH55 TO OPER-DESC-4184-CH55 OF
                FCHWM55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FIELD-1-4184-CH55-Z = 'Y'
               MOVE WK01-FIELD-1-4184-CH55 TO LTRL-FIELD-1-4184-CH55 OF
                FCHWM55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FIELD-2-4184-CH55-Z = 'Y'
               MOVE WK01-FIELD-2-4184-CH55 TO LTRL-FIELD-2-4184-CH55 OF
                FCHWM55
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4181-CH55-Z = 'Y'
               MOVE WK01-IND-4181-CH55 TO CMPLT-IND-4181-CH55 OF FCHWM55
           END-IF.
