      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWM34                              04/24/00  12:15  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWM34.
           02  ACTN-CODE-AP34               OCCURS 11   PIC X(1).
           02  DCMNT-NMBR-AP34              OCCURS 11   PIC X(8).
           02  VNDR-INV-NMBR-AP34           OCCURS 11   PIC X(9).
           02  CHECK-NMBR-AP34              OCCURS 11   PIC X(8).
           02  INV-DATE-AP34                OCCURS 11   PIC X(8).
           02  SRVCD-DATE-AP34              OCCURS 11   PIC X(8).
           02  APRVD-AMT-AP34        COMP-3 OCCURS 11   PIC S9(10)V99.
           02  SAVE-DBKEY-ID-AP34    COMP   OCCURS 11   PIC S9(8).
           02  ITEM-NMBR-AP34               OCCURS 11   PIC X(4).
           02  SEQ-NMBR-AP34                OCCURS 11   PIC X(4).
           02  CHECK-AMT-AP34        COMP-3 OCCURS 11   PIC 9(11)V99.
           02  CNCL-IND-AP34                OCCURS 11   PIC X(1).
