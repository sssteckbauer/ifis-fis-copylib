       975-TO-R4402-TRIP SECTION.
           MOVE TRP-USER-CD TO
               USER-CODE-4402 OF R4402-TRIP
                                                                     .
           MOVE TRP-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4402 OF R4402-TRIP
                                                                     .
           MOVE TRP-UNVRS-CD TO
               UNVRS-CODE-4402 OF R4402-TRIP
                                                                     .
           MOVE TRP-TRIP-NBR TO
               TRIP-NMBR-4402 OF R4402-TRIP
                                                                     .
           MOVE TRP-TRIP-DESC TO
               TRIP-DESC-4402 OF R4402-TRIP
                                                                     .
           MOVE TRP-START-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               START-DATE-4402 OF R4402-TRIP
                                                                     .
           MOVE TRP-END-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               END-DATE-4402 OF R4402-TRIP
                                                                     .
           MOVE TRP-ESTMD-EXPNS-AMT TO
               ESTMD-EXPNS-AMT-4402 OF R4402-TRIP
                                                                     .
           MOVE TRP-RULE-CLS-CD TO
               RULE-CLASS-CODE-4402 OF R4402-TRIP
                                                                     .
           MOVE TRP-NSF-OVRDE TO
               NSF-OVRDE-4402 OF R4402-TRIP
                                                                     .
           MOVE TRP-SPNSR-COA-CD TO
               SPNSR-COA-CODE-4402 OF R4402-TRIP
                                                                     .
           MOVE TRP-SPNSR-ORGN-CD TO
               SPNSR-ORGZN-CODE-4402 OF R4402-TRIP
                                                                     .
           MOVE TRP-CMPLT-IND TO
               CMPLT-IND-4402 OF R4402-TRIP
                                                                     .
           MOVE TRP-APRVL-IND TO
               APRVL-IND-4402 OF R4402-TRIP
                                                                     .
           MOVE TRP-CNCL-IND TO
               CNCL-IND-4402 OF R4402-TRIP
                                                                     .
           MOVE TRP-CNCL-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               CNCL-DATE-4402 OF R4402-TRIP
                                                                     .
           MOVE TRP-HDR-ERROR-IND TO
               HDR-ERROR-IND-4402 OF R4402-TRIP
                                                                     .
           MOVE TRP-HDR-CNTR-DTL TO
               HDR-CNTR-DTL-4402 OF R4402-TRIP
                                                                     .
           MOVE TRP-ACRL-IND TO
               ACRL-IND-4402 OF R4402-TRIP
                                                                     .
           MOVE TRP-LIEN-CLOSED-CD TO
               LIEN-CLOSED-CODE-4402 OF R4402-TRIP
                                                                     .
           MOVE TRP-TRIP-AUTH-DAT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               TRIP-AUTH-DATE-4402 OF R4402-TRIP
                                                                     .
       975-TO-R4402-TRIP-EXIT.
           EXIT.
