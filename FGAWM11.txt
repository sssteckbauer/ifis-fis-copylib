      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FGAWM11                              04/24/00  12:32  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FGAWM11.
           02  RPRT-CODE-GA11                           PIC X(8).
           02  RPRT-TITLE-GA11                          PIC X(60).
           02  FIELD-NAME-GA11              OCCURS 24   PIC X(20).
           02  FIELD-VALUE-GA11             OCCURS 24   PIC X(15).
           02  PRINTER-DEST-ID-GA11                     PIC X(8).
           02  EMAIL-ADDRESS-GA11                       PIC X(30).
           02  JOB-CLASS-GA11                           PIC X(1).
           02  RECEIVER-NAME-GA11                       PIC X(15).
           02  RECEIVER-MAILCODE-GA11                   PIC X(5).
           02  FIELD-EDIT-GA11              OCCURS 24   PIC X(15).
