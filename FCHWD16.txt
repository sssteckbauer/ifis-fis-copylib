      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWD16                              04/24/00  13:01  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWD16.
           02  SYSTEM-DATE-CH16               COMP-3    PIC S9(8).
           02  FROM-GRANT-DATE-CH16           COMP-3    PIC S9(8).
           02  TO-GRANT-DATE-CH16             COMP-3    PIC S9(8).
           02  FROM-BDGT-DATE-CH16            COMP-3    PIC S9(8).
           02  TO-BDGT-DATE-CH16              COMP-3    PIC S9(8).
           02  NEXT-CHNG-DATE-CH16                      PIC X(6).
DEVBWS     02  SAVE-DBKEY-ID-4005-CH16                  PIC X(24).
           02  SET-SORT-KEY-4012-CH16.
               03  OPTN-1-CODE-CH16                     PIC X(8).
               03  OPTN-2-CODE-CH16                     PIC X(8).
               03  LEVEL-NMBR-CH16                      PIC 9(2).
           02  SET-SORT-KEY-6311-CH16.
               03  UNVRS-CODE-6311-CH16                 PIC X(2).
               03  INTRL-REF-ID-6311-CH16     COMP-3    PIC S9(7).
           02  INVGR-INTRL-REF-ID-CH16        COMP-3    PIC S9(7).
           02  CO-INVGR-INTRL-REF-ID-CH16     COMP-3    PIC S9(7).
           02  AGNCY-INTRL-REF-ID-CH16        COMP-3    PIC S9(7).
DEVBWS     02  SAVE-DBKEY-ID-PLUS-ONE-CH16              PIC X(24).
DEVBWS     02  SAVE-DBKEY-ID-MINUS-ONE-CH16              PIC X(24).
