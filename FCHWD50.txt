      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWD50                              04/24/00  13:03  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWD50.
           02  SAVE-DBKEY-ID-6311-CH50        COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-6185-CH50        COMP      PIC S9(8).
           02  SYS-DATE-CH50                  COMP-3    PIC S9(8).
           02  SAVE-DBKEY-ID-4046-CH50        COMP      PIC S9(8).
           02  NMRC-START-DATE-CH50           COMP-3    PIC S9(8).
           02  NMRC-END-DATE-CH50             COMP-3    PIC S9(8).
           02  HOLD-DBKEY-ID-6185-CH50        COMP      PIC S9(8).
           02  ENTY-INDX-KEY-CH50.
               03  UNVRS-CODE-6001-CH50                 PIC X(2).
               03  INTRL-REF-ID-6311-CH50     COMP-3    PIC S9(7).
           02  SYSTEM-DATE-CH50               COMP-3    PIC S9(8).
           02  WORK-ASGN-ID-CH50.
               03  WORK-STORE-ID-CH50.
                   04  WORK-ALPHA-ID-CH50               PIC X(1).
                   04  WORK-NMRC-ID-CH50                PIC 9(7).
               03  WORK-CHCK-DIGIT-ID-CH50              PIC 9(1).
