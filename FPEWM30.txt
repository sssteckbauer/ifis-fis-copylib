      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPEWM30                              04/24/00  12:35  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPEWM30.
           02  ACTN-CODE-PE30                           PIC X(1).
           02  TLPHN-ID-6185-PE30.
               03  BASIC-TLPHN-ID-6185-PE30.
                   04  TLPHN-AREA-CODE-6185-PE30        PIC X(3).
                   04  TLPHN-XCHNG-ID-6185-PE30         PIC X(3).
                   04  TLPHN-SEQ-ID-6185-PE30           PIC X(4).
               03  TLPHN-XTNSN-ID-6185-PE30             PIC X(4).
           02  CITY-NAME-6185-PE30                      PIC X(18).
           02  LONG-DESC-6137-PE30                      PIC X(30).
           02  STATE-CODE-6151-PE30                     PIC X(2).
           02  CNTRY-CODE-6153-PE30                     PIC X(2).
           02  ZIP-CODE-6152-PE30                       PIC X(10).
           02  START-DATE-6185-PE30                     PIC X(6).
           02  END-DATE-6185-PE30                       PIC X(6).
           02  LINE-ADR-6185-PE30           OCCURS 4    PIC X(35).
           02  NAME-KEY-6352-PE30                       PIC X(35).
           02  CNTY-CODE-2346-PE30                      PIC X(4).
           02  FULL-NAME-2346-PE30                      PIC X(35).
           02  FULL-NAME-6151-PE30                      PIC X(35).
           02  FULL-NAME-6153-PE30                      PIC X(35).
           02  FAX-ID-6185-PE30.
               03  FAX-AREA-CODE-PE30                   PIC X(3).
               03  FAX-XCHNG-ID-PE30                    PIC X(3).
               03  FAX-SEQ-ID-PE30                      PIC X(4).
           02  EMADR-LINE-6185-PE30                     PIC X(70).
