      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FZYWM03                              04/24/00  12:50  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FZYWM03.
           02  DTL-CODE-CLMN-LABEL-1-SY03               PIC X(10).
           02  DTL-CODE-CLMN-LABEL-2-SY03               PIC X(10).
           02  FLAG-1-LABEL-1-SY03                      PIC X(5).
           02  FLAG-1-LABEL-2-SY03                      PIC X(5).
           02  FLAG-2-LABEL-1-SY03                      PIC X(5).
           02  FLAG-2-LABEL-2-SY03                      PIC X(5).
           02  ACTN-CODE-GROUP-SY03.
               03  ACTN-CODE-SY03           OCCURS 13   PIC X(1).
           02  DTL-CODE-6601-SY03           OCCURS 13   PIC X(10).
           02  SHORT-DESC-6601-SY03         OCCURS 13   PIC X(10).
           02  LONG-DESC-6601-SY03          OCCURS 13   PIC X(30).
           02  FLAG-1-6601-SY03             OCCURS 13   PIC X(1).
           02  FLAG-2-6601-SY03             OCCURS 13   PIC X(1).
           02  ACTV-INATV-CODE-FLAG-6601-SY03
                                            OCCURS 13   PIC X(1).
           02  RQST-DTL-CODE-CLMN-LABEL-SY03            PIC X(22).
