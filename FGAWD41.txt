      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FGAWD41                              04/24/00  12:52  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FGAWD41.
           02  WORK-DATE-CCYYDDD-GA41         COMP-3    PIC S9(7).
           02  WORK-DATE-YYDDD-GA41           COMP-3    PIC S9(5).
           02  WORK-DATE-MMDDCCYY-GA41        COMP-3    PIC S9(8).
           02  WORK-SYSTEM-DATE-GA41          COMP-3    PIC S9(8).
           02  WORK-SAVE-DBKEY-ID-4029-GA41
                                     COMP   OCCURS 10   PIC S9(8).
           02  WORK-LOOP-FLAG-GA41                      PIC X(1).
           02  WORK-SAVE-DBKEY-GA41           COMP      PIC S9(8).
           02  ACTN-FLAG-GA41                           PIC X(1).
           02  COPY-RQSTED-FLAG-GA41                    PIC X(1).
           02  SEL-FLAG-GA41                            PIC X(1).
           02  SAVE-COPY-FR-FUND-CODE-GA41              PIC X(6).
           02  DEL-QTY-GA41                   COMP-3    PIC 9(3).
           02  ADD-QTY-GA41                   COMP-3    PIC 9(3).
