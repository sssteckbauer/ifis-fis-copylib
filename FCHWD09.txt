      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWD09                              04/24/00  13:01  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWD09.
           02  SYSTEM-DATE-CH09               COMP-3    PIC S9(8).
           02  START-DATE-CH09                COMP-3    PIC S9(8).
           02  WORK-SAVE-DBKEY-ID-4007-CH09   COMP      PIC S9(8).
           02  END-DATE-CH09                  COMP-3    PIC S9(8).
           02  RQST-FLAG-CH09                           PIC X(1).
           02  LOOP-FLAG-CH09                           PIC X(1).
           02  LEVEL-CNTR-CH09                          PIC 9(1).
           02  LCTN-CODE-CH09               OCCURS 5    PIC X(6).
