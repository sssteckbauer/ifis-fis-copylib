      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWD39                              04/24/00  13:13  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWD39.
           02  SAVE-DBKEY-ID-4084-PU39
                                     COMP   OCCURS 6    PIC S9(8).
           02  SYSTEM-DATE-PU39               COMP-3    PIC S9(8).
           02  START-DATE-PU39       COMP-3 OCCURS 6    PIC S9(8).
           02  END-DATE-PU39         COMP-3 OCCURS 6    PIC S9(8).
           02  MAX-AMT-PU39          COMP-3 OCCURS 6    PIC 9(10)V99.
           02  ACTN-ERR-FLAG-PU39                       PIC X(1).
