      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCOWK15                              04/24/00  12:27  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCOWK15.
           02  CMBNTN-ID-CO15.
               03  CMBNTN-ID-DIGIT-ONE-CO15             PIC X(1).
               03  CMBNTN-ID-LAST-NINE-CO15             PIC X(9).
           02  COMM-VIEW-6257-CO15                      PIC X(8).
           02  COMM-TYPE-6258-CO15                      PIC X(4).
           02  COMM-PLAN-CODE-6259-CO15                 PIC X(4).
           02  TEXT-CODE-6260-CO15                      PIC X(4).
           02  OFC-CODE-2155-CO15                       PIC X(4).
           02  PARA-CODE-6268-CO15                      PIC X(4).
           02  REF-NUMBER-CO15                          PIC X(6).
           02  MAP-DATE-CO15                            PIC X(6).
