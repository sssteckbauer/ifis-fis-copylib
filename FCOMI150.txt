    ***Created by Convert/DC version V8R03 on 12/13/00 at 09:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6001-CO15-Z = 'Y'
              IF WK01-CODE-6001-CO15 NOT NUMERIC
               MOVE WK01-CODE-6001-CO15-X TO FICE-CODE-6001-CO15 OF
                FCOWM15
           ELSE
               MOVE WK01-CODE-6001-CO15 TO FICE-CODE-6001-CO15 OF
                FCOWM15
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY0A-Z = 'Y'
               MOVE WK01-NAME-6001-SY0A TO FULL-NAME-6001-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-CO15-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-CO15 TO CMBNTN-ID-LAST-NINE-CO15
                OF FCOWK15
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-VIEW-6257-CO15-Z = 'Y'
               MOVE WK01-VIEW-6257-CO15 TO COMM-VIEW-6257-CO15 OF
                FCOWK15
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-6258-CO15-Z = 'Y'
               MOVE WK01-TYPE-6258-CO15 TO COMM-TYPE-6258-CO15 OF
                FCOWK15
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PLAN-CODE-6259-CO15-Z = 'Y'
               MOVE WK01-PLAN-CODE-6259-CO15 TO
                COMM-PLAN-CODE-6259-CO15 OF FCOWK15
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6260-CO15-Z = 'Y'
               MOVE WK01-CODE-6260-CO15 TO TEXT-CODE-6260-CO15 OF
                FCOWK15
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-2155-CO15-Z = 'Y'
               MOVE WK01-CODE-2155-CO15 TO OFC-CODE-2155-CO15 OF FCOWK15
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6268-CO15-Z = 'Y'
               MOVE WK01-CODE-6268-CO15 TO PARA-CODE-6268-CO15 OF
                FCOWK15
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NUMBER-CO15-Z = 'Y'
               MOVE WK01-NUMBER-CO15 TO REF-NUMBER-CO15 OF FCOWK15
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-CO15-Z = 'Y'
               MOVE WK01-DATE-CO15 TO MAP-DATE-CO15 OF FCOWK15
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-KEY-CO15-Z = 'Y'
               MOVE WK01-KEY-CO15 TO NAME-KEY-CO15 OF FCOWM15
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6257-CO15-Z = 'Y'
               MOVE WK01-DESC-6257-CO15 TO LONG-DESC-6257-CO15 OF
                FCOWM15
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6258-CO15-Z = 'Y'
               MOVE WK01-DESC-6258-CO15 TO LONG-DESC-6258-CO15 OF
                FCOWM15
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6259-CO15-Z = 'Y'
               MOVE WK01-DESC-6259-CO15 TO LONG-DESC-6259-CO15 OF
                FCOWM15
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6260-CO15-Z = 'Y'
               MOVE WK01-DESC-6260-CO15 TO LONG-DESC-6260-CO15 OF
                FCOWM15
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-2155-CO15-Z = 'Y'
               MOVE WK01-NAME-2155-CO15 TO FULL-NAME-2155-CO15 OF
                FCOWM15
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6268-CO15-Z = 'Y'
               MOVE WK01-DESC-6268-CO15 TO LONG-DESC-6268-CO15 OF
                FCOWM15
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY01-Z = 'Y'
               MOVE WK01-TEXT-SY01 TO DCSD-TEXT-SY01 OF FZYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0A-Z = 'Y'
               MOVE WK01-TEXT-SY0A TO CNFDL-TEXT-SY01 OF FZYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0B-Z = 'Y'
               MOVE WK01-TEXT-SY0B TO HOLDS-TEXT-SY01 OF FZYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0C-Z = 'Y'
               MOVE WK01-TEXT-SY0C TO ARCHVD-TEXT-SY01 OF FZYWG01M
           END-IF.
