      ******************************************************************
      * THIS IS SQL INCLUDE MEMBER 'STDERRCP'.                         *
      * THIS IS THE PROCEDURAL INCLUDE MEMBER USED BY DB2/CICS         *
      * PROGRAMS TO EXECUTE STDERR-ERROR-ROUTINE.                      *
      *                                                                *
      * PROGRAMS USING THIS ROUTINE MUST ALSO INCLUDE THE FOLLOWING:   *
      * STDWSTG  - STANDARD COMMON WORKING-STORAGE FIELDS              *
      * STDWSGET - STD-GET-SYSTEM-INFO (GET TIME, REGION, ETC)         *
      *                                                                *
      ******************************************************************
      * STDERRCP PERFORMS ERROR PROCESSING FOR 3 TYPES OF ABENDS,      *
      * DETERMINED BY THE VALUE PASSED IN STDERR-ABEND-TYPE:           *
      *   DB2:  ABEND WAS TRIGGERED BY A DB2 ERROR.                    *
      *         SENDMSG INCLUDES PASSED SQL-NUMBER AND SQLCA MESSAGES. *
      *         ABEND CODE IS SET TO THE SQLCODE.                      *
      *   CICS: ABEND WAS TRIGGERED BY A CICS ERROR.                   *
      *         SENDMSG INCLUDES ONLY THE STANDARD INFORMATION.        *
      *         ABEND CODE IS SET TO THE CICS-ASSIGNED ABEND CODE.     *
      *   PGM:  ABEND WAS TRIGGERED BY A PROGRAM DETECTED ERROR.       *
      *         SENDMSG INCLUDES PASSED PGM-MESSAGE.                   *
      *         ABEND CODE IS SET TO 'PGMX'.                           *
      *                                                                *
      * SYNCPOINT ROLLBACK IS ALWAYS EXECUTED TO BACK OUT UPDATES.     *
      * PASSED PROGRAM INFORMATION AND STANDARD SYSTEM INFO (RETRIEVED *
      * BY STDWSGET) IS FORMATTED INTO STDERR-SENDMSG-AREA.            *
      *                                                                *
      * DEPENDING ON THE PASSED Y/N SWITCH SETTINGS, THE ROUTINE WILL: *
      * . SEND THE MESSAGES TO THE SCREEN (SEE FORMAT BELOW).          *
      * . ABEND THE TASK WITH ABEND CODE DETERMINED BY THE ABEND TYPE. *
      *                                                                *
      * THESE 2 OPTIONS CAN BE SET EITHER THE SAME OR DIFFERENTLY FOR  *
      * THE DVLP REGION AND THE PROD/QA/TRAINING REGIONS BY CHANGING   *
      * THE DEFAULT SETTINGS (ALL SWITCHES ARE PRESET TO 'Y') PRIOR    *
      * TO INVOKING THIS ROUTINE:                                      *
      *    MOVE 'N'                    TO STDERR-DVLP-SENDMSG-SW       *
      *    MOVE 'N'                    TO STDERR-DVLP-ABEND-SW         *
      *    MOVE 'N'                    TO STDERR-PROD-SENDMSG-SW       *
      *    MOVE 'N'                    TO STDERR-PROD-ABEND-SW         *
      *                                                                *
      * IF SENDMSG IS REQUESTED, THE FOLLOWING ERROR MESSAGE FORMAT    *
      * IS SENT TO THE FULL SCREEN (24 X 80):                          *
      *                                                                *
      * PGM TERMINATED ABNORMALLY - PRINT THIS SCREEN AND NOTIFY ACT.  *
      * APPLID=XXXXXXXX  USER ID=XXXXXXXX XXXXXXXXXXXXXXXXX  DATE TIME *
      * PGM ID=XXXXXXXX  PARAGRAPH=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX      *
      * ABEND TYPE=XXXX  ABEND CODE=XXXX       TRANID=XXXX TERMID=XXXX *
      * MESSAGE=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX+ (FOR PGM ABENDS ONLY) *
      * SQL NBR=XXXXX    SQL ERROR TEXT:         (FOR DB2 ABENDS ONLY) *
      * (UP TO 19 LINES OF ERROR MESSAGE TEXT FROM THE SQLCA COMMAREA) *
      *                                                                *
      ******************************************************************
      * THE FOLLOWING MOVE STATEMENTS SHOULD BE EXECUTED ONE TIME      *
      * PRIOR TO INVOKING STDERR-ERROR-ROUTINE:                        *
      *                                                   (ABEND TYPE) *
      *    MOVE 'DB2','CICS',OR 'PGM'  TO STDERR-ABEND-TYPE   (ALL)    *
      *    MOVE (PARAGRAPH NAME)       TO STDERR-PARAGRAPH    (ALL)    *
      *    MOVE (PROGRAM NUMBER)       TO STDERR-PROGRAM      (ALL)    *
      *    MOVE (UNIQUE SQL NUMBER)    TO STDERR-SQL-NBR      (DB2)    *
      *    MOVE SQLCA                  TO STDERR-SQLCA        (DB2)    *
      *    MOVE (ERROR MESSAGE)        TO STDERR-PGM-MESSAGE  (PGM)    *
      *                                                                *
      * SQL NUMBER CAN BE THE PARAGRAPH NUMBER BUT SHOULD BE UNIQUE.   *
      * THE FOLLOWING LINES CAN BE PLACED AFTER EACH SQL STATEMENT:    *
      *                                                                *
      *    EVALUATE SQLCODE                                            *
      *      WHEN +0                                                   *
      *        CONTINUE                                                *
      *      WHEN +100                                                 *
      *        PERFORM NOT-FOUND-LOGIC                                 *
      *      WHEN OTHER                                                *
      *        PERFORM STDERR-ERROR-ROUTINE                            *
      *    END-EVALUATE.                                               *
      ******************************************************************


       STDERR-ERROR-ROUTINE.

           EXEC CICS HANDLE ABEND
                     CANCEL
           END-EXEC.

           MOVE STDERR-PROGRAM         TO STDERR-SENDMSG-PROGRAM.
           MOVE STDERR-PARAGRAPH       TO STDERR-SENDMSG-PARAGRAPH.

           PERFORM STD-GET-SYSTEM-INFO.

           MOVE STD-APPLID             TO STDERR-SENDMSG-APPLID.
           MOVE STD-USER-ID            TO STDERR-SENDMSG-USER-ID.
           MOVE STD-USER-NAME          TO STDERR-SENDMSG-USER-NAME.
           MOVE STD-CURR-MMDDYYYY      TO STDERR-SENDMSG-DATE.
           MOVE STD-CURR-TIME          TO STDERR-SENDMSG-TIME.
           MOVE EIBTRNID               TO STDERR-SENDMSG-TRANID.
           MOVE EIBTRMID               TO STDERR-SENDMSG-TERMID.

           EVALUATE STDERR-ABEND-TYPE
             WHEN 'DB2'
               PERFORM STDERR-DB2-ERROR

             WHEN 'CICS'
               PERFORM STDERR-CICS-ERROR

             WHEN 'PGM'
               PERFORM STDERR-PGM-ERROR

             WHEN OTHER
               MOVE 'ABEND TYPE IS INVALID. MUST REVIEW DUMP TO RESOLVE'
                                       TO STDERR-PGM-MESSAGE
               PERFORM STDERR-PGM-ERROR
           END-EVALUATE.

           EXEC CICS SYNCPOINT
                     ROLLBACK
           END-EXEC.

           IF STD-REGION = 'DVLP'
               IF  STDERR-DVLP-SENDMSG-SW = 'N'
               AND STDERR-DVLP-ABEND-SW   = 'N'
                   NEXT SENTENCE
               ELSE
                   IF STDERR-DVLP-SENDMSG-SW = 'Y'
                       PERFORM STDERR-CICS-SEND
                   END-IF

                   IF STDERR-DVLP-ABEND-SW = 'Y'
                       GO TO STDERR-CICS-ABEND
                   ELSE
                       GO TO STDERR-CICS-RETURN
                   END-IF
               END-IF
           ELSE
               IF  STDERR-PROD-SENDMSG-SW = 'N'
               AND STDERR-PROD-ABEND-SW   = 'N'
                   NEXT SENTENCE
               ELSE
                   IF STDERR-PROD-SENDMSG-SW = 'Y'
                       PERFORM STDERR-CICS-SEND
                   END-IF

                   IF STDERR-PROD-ABEND-SW = 'Y'
                       GO TO STDERR-CICS-ABEND
                   ELSE
                       GO TO STDERR-CICS-RETURN
                   END-IF
               END-IF
           END-IF.

       STDERR-ERROR-ROUTINE-EXIT.      EXIT.


       STDERR-DB2-ERROR.

           MOVE 'DB2'                  TO STDERR-SENDMSG-TYPE.
           MOVE 'SQL NBR='             TO STDERR-SENDMSG-TYPE-LABEL.
           MOVE STDERR-SQL-NBR         TO STDERR-SENDMSG-SQL-NBR.

           CALL 'DSNTIAR'           USING STDERR-SQLCA
                                          STDERR-SQLTXT-AREA
                                          STDERR-SQLTXT-LINE-LENGTH.

           IF RETURN-CODE = ZERO
               MOVE 'SQL ERROR TEXT: ' TO STDERR-SENDMSG-DB2-LABEL
               MOVE SQLCODE            TO STDERR-DISPLAY-SQLCODE
           ELSE
               MOVE 'DSNTIAR ERR, RC=' TO STDERR-SENDMSG-DB2-LABEL
               MOVE RETURN-CODE        TO STDERR-SENDMSG-DB2-RETCODE
           END-IF.

           MOVE STDERR-DISPLAY-SQLCODE TO STDERR-ABEND-CODE-9.
           MOVE STDERR-ABEND-CODE      TO STDERR-SENDMSG-ABCODE.

           PERFORM WITH TEST BEFORE
                   VARYING STDERR-SQLTXT-INDX FROM +1 BY +1
                     UNTIL STDERR-SQLTXT-INDX   > +19

               SET  STDERR-SENDMSG-INDX
                                       TO STDERR-SQLTXT-INDX

               MOVE STDERR-SQLTXT-LINE   (STDERR-SQLTXT-INDX)
                 TO STDERR-SENDMSG-TEXT  (STDERR-SENDMSG-INDX)
           END-PERFORM.


       STDERR-CICS-ERROR.

           MOVE 'CICS'                 TO STDERR-SENDMSG-TYPE.
           MOVE SPACES                 TO STDERR-SENDMSG-TYPE-LABEL.

           EXEC CICS ASSIGN ABCODE (STDERR-ABEND-CODE)
           END-EXEC.

           IF STDERR-ABEND-CODE = SPACES
               MOVE STD-CICS-RESP      TO STDERR-SENDMSG-ABCODE
           ELSE
               MOVE STDERR-ABEND-CODE  TO STDERR-SENDMSG-ABCODE
           END-IF.


       STDERR-PGM-ERROR.

           MOVE STDERR-ABEND-TYPE      TO STDERR-SENDMSG-TYPE.
           MOVE 'MESSAGE='             TO STDERR-SENDMSG-TYPE-LABEL.
           MOVE STDERR-PGM-MESSAGE     TO STDERR-SENDMSG-TYPE-PGM-MSG.

           MOVE 'PGMX'                 TO STDERR-ABEND-CODE.
           MOVE STDERR-ABEND-CODE      TO STDERR-SENDMSG-ABCODE.


       STDERR-CICS-SEND.

           EXEC CICS SEND
                     FROM   (STDERR-SENDMSG-AREA)
                     LENGTH (1920)
                     WAIT
                     ERASE
           END-EXEC.


       STDERR-CICS-ABEND.

           EXEC CICS ABEND
                     ABCODE (STDERR-ABEND-CODE)
           END-EXEC.

           GOBACK.


       STDERR-CICS-RETURN.

           EXEC CICS RETURN
           END-EXEC.

           GOBACK.

      ************* END OF INCLUDE MEMBER 'STDERRCP' *******************
