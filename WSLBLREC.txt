      *----------------------------------------------------------------*00000100
      *    LAYOUT FOR: WSLBLREC - HOLD AREA FOR BUILDING LABEL RECORDS.*00000110
      *----------------------------------------------------------------*00000120
       01  WS-LABEL-RECORD.                                             00000130
      *                                                                 00000140
           03  WLR-SORT-KEY                 PICTURE X(100).             00000150
      *                                                                 00000160
           03  WLR-RECORD-TYPE              PICTURE X(001).             00000170
      *                                                                 00000180
           03  WLR-DATA                     PICTURE X(079).             00000190
           03  WLR-TYPE-1-OR-2 REDEFINES WLR-DATA.                      00000200
               05  WLR-PART-A               PICTURE X(035).             00000210
               05  FILLER                   PICTURE X(009).             00000220
               05  WLR-PART-B               PICTURE X(035).             00000230
           03  WLR-TYPE-3      REDEFINES WLR-DATA.                      00000240
               05  WLR-CITY                 PICTURE X(020).             00000250
               05  WLR-STATE                PICTURE X(002).             00000260
               05  WLR-ZIP                  PICTURE X(010).             00000270
               05  FILLER                   PICTURE X(007).             00000280
               05  WLR-COUNTRY              PICTURE X(035).             00000290
               05  FILLER                   PICTURE X(005).             00000300
      *                                                                 00000310
