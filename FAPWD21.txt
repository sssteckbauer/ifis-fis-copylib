      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWD21                              04/24/00  12:53  *
      *  REDEFINE SAVE-DBKEY-ID-4150      - 10/31/01 DMW              *
      *---------------------------------------------------------------*
       01  FAPWD21.
           02  SAVE-DBKEY-ID-4150-AP21
                                            OCCURS 11   PIC X(08).
           02  ACTN-ERR-FLAG-AP21                       PIC X(1).
           02  LISTVAL-FLAG-AP21                        PIC X(1).
           02  ADD-FLAG-AP21                            PIC X(1).
           02  WK-APRVD-AMT-AP21              COMP-3    PIC S9(10)V99.
           02  WK-APRVD-AMT-TOTAL-AP21        COMP-3    PIC S9(10)V99.
           02  X-DATECHG-DATE-AP21.
               03  X-DATECHG-MM-AP21                    PIC 9(2).
               03  X-DATECHG-DD-AP21                    PIC 9(2).
               03  X-DATECHG-CC-AP21                    PIC 9(2).
               03  X-DATECHG-YY-AP21                    PIC 9(2).
           02  CAL-DATE-AP21.
               03  CAL-MM-AP21                          PIC 9(2).
               03  CAL-DD-AP21                          PIC 9(2).
               03  CAL-YY-AP21                          PIC 9(2).
           02  NUMERIC-8-AP21                           PIC 9(8).
