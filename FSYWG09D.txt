      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FSYWG09D                             04/24/00  12:53  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FSYWG09D.
           02  UNVRS-CODE-SY09                          PIC X(2).
           02  SEQ-NMBR-SY09                            PIC 9(4).
           02  DCMNT-NMBR-SY09                          PIC X(8).
           02  CHNG-SEQ-NMBR-SY09                       PIC X(3).
           02  FSCL-YR-SY09                             PIC X(2).
           02  ACTG-PRD-SY09                            PIC X(2).
           02  ACTG-PRD-NMRC-SY09                       PIC 9(2).
           02  TRANS-DATE-SY09                COMP-3    PIC S9(8).
           02  ACTG-PRD-ERR-IND-SY09                    PIC X(1).
           02  TRANS-DATE-ERR-IND-SY09                  PIC X(1).
           02  FSCL-YR-ERR-IND-SY09                     PIC X(1).
