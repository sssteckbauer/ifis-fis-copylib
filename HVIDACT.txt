       975-TO-R4014-ACTT SECTION.
           MOVE ACT-USER-CD TO
               USER-CODE-4014 OF R4014-ACTT
                                                                     .
           MOVE ACT-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4014 OF R4014-ACTT
                                                                     .
           MOVE ACT-UNVRS-CD TO
               UNVRS-CODE-4014 OF R4014-ACTT
                                                                     .
           MOVE ACT-COA-CD TO
               COA-CODE-4014 OF R4014-ACTT
                                                                     .
           MOVE ACT-ACCT-TYP-CD TO
               ACCT-TYPE-CODE-4014 OF R4014-ACTT
                                                                     .
           MOVE ACT-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACTVY-DATE-4014 OF R4014-ACTT
                                                                     .
       975-TO-R4014-ACTT-EXIT.
           EXIT.
