       01  WS-LITERALS.
           03  WS-LIT-DATE.
               05  FILLER              PIC X(02).
               05  FILLER              PIC X(01)     VALUE '/'.
               05  FILLER              PIC X(02).
               05  FILLER              PIC X(01)     VALUE '/'.
               05  FILLER              PIC X(02).
           03  WS-LIT-TIME.
               05  LIT-HH              PIC 9(02).
               05  FILLER              PIC X(01)     VALUE ':'.
               05  LIT-MM              PIC 9(02).
               05  FILLER              PIC X(01)     VALUE ':'.
               05  LIT-SS              PIC 9(02).
