      * /************************************************************/  $
      * /*   CHANGE LOG -- CPWSXPVS                                 */  $
      * /*   NAME            DATE      COMMENTS                     */  $
      * /*..........................................................*/  $
      * /*   BROWN          05/23/85  MODIFIED FOR COLLECTIVE BARG. */  $
      * /*   M. JEWETT      01/13/92  ADDED IFIS IFOAPAL AND TITLES.*/  $
      * /*   B. ORTIZ       05/07/98  MODIFIED FOR GFAU (REL0027)  .*/  $
      * /************************************************************/  $
      *01  XSL1-STAFFING-LIST-SEGMENT.                                  CPWSXSL1
           05  XSL1-KEY.                                                CPWSXSL1
      ****     10  XSL1-LOCATION               PIC X(02).               3202RRRR
      ****     10  XSL1-SAU                    PIC X(01).               3202RRRR
      ****     10  XSL1-SUB-CAMPUS             PIC X(01).               3202RRRR
      ****     10  XSL1-ACCOUNT                PIC X(07).               3202RRRR
      ****     10  XSL1-FUND                   PIC X(06).               3202RRRR
      ****     10  XSL1-SUB-BUDGET             PIC X(02).               3202RRRR
000801         10  XSL1-FAU.                                            3202RRRR
000810             15  XSL1-LOCATION           PIC X(02).               3202RRRR
000820             15  XSL1-SAU                PIC X(01).               3202RRRR
000830             15  XSL1-SUB-CAMPUS         PIC X(01).               3202RRRR
000840             15  XSL1-ACCOUNT            PIC X(07).               3202RRRR
000850             15  XSL1-FUND               PIC X(06).               3202RRRR
000860             15  XSL1-SUB-BUDGET         PIC X(02).               3202RRRR
               10  XSL1-SEGMENT-SORT           PIC X(01).               CPWSXSL1
                   88  XSL1-BUDGET-HEADER      VALUE '1'.               CPWSXSL1
                   88  XSL1-BUDGET-DETAIL      VALUE '2'.               CPWSXSL1
                   88  XSL1-BUDGET-TRAILER     VALUE '3'.               CPWSXSL1
           05  XSL1-FILLER                     PIC X(94).               CPWSXSL1
           05  XSL1-PAYROLL-SEGMENT-TYPE       PIC X(01).               CPWSXSL1
                   88  XSL1-PAYROLL-DISTR      VALUE '4'.               CPWSXSL1
                   88  XSL1-PAYROLL-XREF       VALUE '5'.               CPWSXSL1
                   88  XSL1-PROVISION          VALUE '6'.               CPWSXSL1
           05  XSL1-IFIS-IFOAPAL.
               10  XSL1-IFIS-INDEX             PIC X(10).
               10  XSL1-IFIS-FUND              PIC X(06).
               10  XSL1-IFIS-ORG               PIC X(06).
               10  XSL1-IFIS-ACCOUNT           PIC X(06).
               10  XSL1-IFIS-PROGRAM           PIC X(06).
               10  XSL1-IFIS-ACTIVITY          PIC X(06).
               10  XSL1-IFIS-LOCATION          PIC X(06).
           05  XSL1-IFIS-TITLES.
               10  XSL1-IFIS-INDEX-TITLE       PIC X(35).
               10  XSL1-IFIS-FUND-TITLE        PIC X(35).
               10  XSL1-IFIS-ORG-TITLE         PIC X(35).
               10  XSL1-IFIS-ACCOUNT-TITLE     PIC X(35).
               10  XSL1-IFIS-PROGRAM-TITLE     PIC X(35).
               10  XSL1-IFIS-ACTIVITY-TITLE    PIC X(35).
               10  XSL1-IFIS-LOCATION-TITLE    PIC X(35).
