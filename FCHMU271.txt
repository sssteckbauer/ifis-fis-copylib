    ***Created by Convert/DC version V8R03 on 12/11/00 at 16:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE DCSD-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY01.
      *%--------------------------------------------------------------%*
           MOVE CNFDL-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0A.
      *%--------------------------------------------------------------%*
           MOVE HOLDS-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0B.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH27 OF FCHWM27 TO WK01-CODE-CH27.
      *%--------------------------------------------------------------%*
           MOVE FICE-CODE-6001-CH27 OF FCHWM27 TO WK01-CODE-6001-CH27.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-CH27 OF FCHWM27 TO WK01-NAME-6001-CH27.
      *%--------------------------------------------------------------%*
           MOVE CITY-NAME-6001-CH27 OF FCHWM27 TO WK01-NAME-6001-CH2A.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-AREA-CODE-CH27 OF FCHWM27 TO WK01-AREA-CODE-CH27.
      *%--------------------------------------------------------------%*
           MOVE SHORT-NAME-6001-CH27 OF FCHWM27 TO WK01-NAME-6001-CH2B.
      *%--------------------------------------------------------------%*
           MOVE STATE-CODE-6001-CH27 OF FCHWM27 TO WK01-CODE-6001-CH2A.
      *%--------------------------------------------------------------%*
           MOVE ZIP-CODE-6001-CH27 OF FCHWM27 TO WK01-CODE-6001-CH2B.
      *%--------------------------------------------------------------%*
           MOVE CNTRY-CODE-6001-CH27 OF FCHWM27 TO WK01-CODE-6001-CH2C.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XCHNG-ID-CH27 OF FCHWM27 TO WK01-XCHNG-ID-CH27.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-SEQ-ID-CH27 OF FCHWM27 TO WK01-SEQ-ID-CH27.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XTNSN-ID-CH27 OF FCHWM27 TO WK01-XTNSN-ID-CH27.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-6001-CH27 OF FCHWM27(0001) TO
                WK01-ADR-6001-CH271.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-6001-CH27 OF FCHWM27(0002) TO
                WK01-ADR-6001-CH272.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-6001-CH27 OF FCHWM27(0003) TO
                WK01-ADR-6001-CH273.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-6001-CH27 OF FCHWM27(0004) TO
                WK01-ADR-6001-CH274.
