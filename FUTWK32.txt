      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FUTWK32                              04/24/00  12:50  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FUTWK32.
           02  ACH-ID-UT32.
               03  ACH-ID-DIGIT-ONE-UT32                PIC X(1).
               03  ACH-ID-LAST-NINE-UT32                PIC X(9).
           02  NAME-KEY-UT32                            PIC X(35).
           02  ENTY-PRSN-IND-UT32                       PIC X(1).
           02  REQ-COMM-PLAN-CODE-UT32                  PIC X(4).
