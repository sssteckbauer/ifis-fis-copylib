      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWM17                              04/24/00  12:16  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWM17.
           02  TOP-ACTN-CODE-AP17                       PIC X(1).
           02  TAX-RATE-PCT-AP17            OCCURS 10   PIC X(7).
           02  END-DATE-AP17                OCCURS 10   PIC X(6).
           02  ACTVY-DATE-AP17       COMP-3 OCCURS 10   PIC S9(8).
           02  STATUS-AP17                  OCCURS 10   PIC X(1).
           02  NEXT-CHNG-DATE-AP17   COMP-3 OCCURS 10   PIC S9(8).
           02  STATUS-DESC-AP17             OCCURS 10   PIC X(8).
           02  START-DATE-AP17              OCCURS 10   PIC X(6).
           02  ACTN-CODE-GROUP-AP17.
               03  ACTN-CODE-AP17           OCCURS 10   PIC X(1).
           02  SAVE-DBKEY-ID-AP17    COMP   OCCURS 10   PIC S9(8).
           02  TAX-CODE-IND-AP17            OCCURS 10   PIC X(1).
           02  TAX-TYPE-AP17                OCCURS 10   PIC X(1).
