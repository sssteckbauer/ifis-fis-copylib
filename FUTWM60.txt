      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FUTWM60                              04/24/00  12:50  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FUTWM60.
           02  ACTN-CODE-GROUP-UT60.
               03  ACTN-CODE-UT60                       PIC X(1).
           02  FAX-TYPE-KEY-UT60.
               03  UNVRS-CODE-UT60                      PIC X(2).
               03  FAX-DCMNT-TYPE-UT60                  PIC X(3).
               03  FAX-DCMNT-NMBR-UT60                  PIC X(8).
               03  CHNG-SEQ-NMBR-UT60                   PIC X(3).
               03  FAX-NMBR-UT60                        PIC X(8).
           02  CNTCT-PID-UT60.
               03  CNTCT-PID-DIGIT-ONE-UT60             PIC X(1).
               03  CNTCT-PID-DIGIT-LAST-NINE-UT60       PIC X(9).
           02  CNTCT-LINE1-UT60                         PIC X(35).
           02  CNTCT-LINE2-UT60                         PIC X(35).
           02  CNTCT-LINE3-UT60                         PIC X(35).
           02  CNTCT-FAX-TLPHN-ID-UT60.
               03  CNTCT-FAX-AREA-CODE-UT60             PIC X(3).
               03  CNTCT-FAX-XCHNG-ID-UT60              PIC X(3).
               03  CNTCT-FAX-SEQ-ID-UT60                PIC X(4).
           02  CNTCT-OFC-TLPHN-ID-UT60.
               03  CNTCT-OFC-AREA-CODE-UT60             PIC X(3).
               03  CNTCT-OFC-XCHNG-ID-UT60              PIC X(3).
               03  CNTCT-OFC-SEQ-ID-UT60                PIC X(4).
               03  CNTCT-OFC-XTNSN-ID-UT60              PIC X(4).
           02  FAXER-NAME-UT60                          PIC X(35).
           02  FAXER-FAX-TLPHN-ID-UT60.
               03  FAXER-FAX-AREA-CODE-UT60             PIC X(3).
               03  FAXER-FAX-XCHNG-ID-UT60              PIC X(3).
               03  FAXER-FAX-SEQ-ID-UT60                PIC X(4).
           02  FAXER-OFC-TLPHN-ID-UT60.
               03  FAXER-OFC-AREA-CODE-UT60             PIC X(3).
               03  FAXER-OFC-XCHNG-ID-UT60              PIC X(3).
               03  FAXER-OFC-SEQ-ID-UT60                PIC X(4).
               03  FAXER-OFC-XTNSN-ID-UT60              PIC X(4).
           02  FAX-RQST-DATE-UT60             COMP-3    PIC S9(8).
           02  FAX-STAT-DATE-UT60             COMP-3    PIC S9(8).
           02  FAX-STAT-TIME-STAMP-UT60                 PIC 9(6).
           02  AM-PM-FLAG-UT60                          PIC X(1).
           02  FAX-STAT-FLAG-UT60                       PIC X(1).
           02  FAX-INDEX-UT60                           PIC X(10).
           02  FAX-DCMNT-NMBR-RQST-UT60                 PIC X(8).
           02  STAT-DESC-UT60                           PIC X(20).
           02  TEXT-FLAG-UT60                           PIC X(1).
           02  FAX-RQST-FLAG-UT60                       PIC X(1).
           02  CONFIRM-FAX-UT60                         PIC X(1).
           02  CONFIRM-FAX-LIT-UT60                     PIC X(12).
           02  INTL-TLPHN-ID-UT60                       PIC X(12).
