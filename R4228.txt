       01  R4228-FILE-CNTL.
           02  DB-PROC-ID-4228.
               03  USER-CODE-4228                       PIC X(8).
               03  LAST-ACTVY-DATE-4228                 PIC X(5).
               03  TRMNL-ID-4228                        PIC X(8).
               03  PURGE-FLAG-4228                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  FILE-KEY-4228.
               03  VNDR-CODE-4228.
                   04  VNDR-ID-DIGIT-ONE-4228           PIC X(1).
                   04  VNDR-ID-LAST-NINE-4228           PIC X(9).
               03  CMDTY-CODE-4228                      PIC X(8).
               03  FILE-NMBR-4228                       PIC 9(5).
           02  FILE-DATE-4228                 COMP-3    PIC S9(8).
           02  STMNT-START-DATE-4228          COMP-3    PIC S9(8).
           02  STMNT-END-DATE-4228            COMP-3    PIC S9(8).
           02  HDR-ERROR-IND-4228                       PIC X(1).
           02  DTL-ERROR-IND-4228                       PIC X(1).
           02  RUN-DATE-4228                  COMP-3    PIC S9(8).
           02  RUN-TIME-4228                            PIC X(6).
           02  PRCSD-DATE-4228                COMP-3    PIC S9(8).
           02  PRCSD-TIME-4228                          PIC X(6).
           02  HASH-TOTAL-4228                COMP-3    PIC S9(10)V99.
           02  TOTAL-NMBR-REC-4228            COMP-3    PIC S9(5).
