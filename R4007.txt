       01  R4007-LCTN-EFCTV.
           02  DB-PROC-ID-4007.
               03  USER-CODE-4007                       PIC X(8).
               03  LAST-ACTVY-DATE-4007                 PIC X(5).
               03  TRMNL-ID-4007                        PIC X(8).
               03  PURGE-FLAG-4007                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  EFCTV-RCRD-KEY-4007.
               03  START-DATE-4007            COMP-3    PIC S9(8).
               03  TIME-STAMP-4007                      PIC X(6).
           02  END-DATE-4007                  COMP-3    PIC S9(8).
           02  STATUS-4007                              PIC X(1).
           02  LCTN-TITLE-4007                          PIC X(35).
           02  ADR-LINE1-4007                           PIC X(35).
           02  ADR-LINE2-4007                           PIC X(35).
           02  ADR-LINE3-4007                           PIC X(35).
           02  CITY-NAME-4007                           PIC X(18).
           02  STATE-CODE-4007                          PIC X(2).
           02  ZIP-CODE-4007                            PIC X(10).
           02  CNTY-CODE-4007                           PIC X(4).
           02  CNTRY-CODE-4007                          PIC X(2).
           02  TLPHN-ID-4007.
               03  BASIC-TLPHN-ID-4007.
                   04  TLPHN-AREA-CODE-4007             PIC X(3).
                   04  TLPHN-XCHNG-ID-4007              PIC X(3).
                   04  TLPHN-SEQ-ID-4007                PIC X(4).
               03  TLPHN-XTNSN-ID-4007                  PIC X(4).
           02  SQR-FTGE-4007                            PIC 9(6).
           02  SQR-FTGE-RATE-4007             COMP-3    PIC 9(6)V99.
           02  PREDCSR-CODE-4007                        PIC X(6).
           02  LCTN-CODE-4007                           PIC X(6).
