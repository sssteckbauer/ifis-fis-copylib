      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FUTWK30                              04/24/00  12:50  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FUTWK30.
           02  ACH-ID-UT30.
               03  ACH-ID-DIGIT-ONE-UT30                PIC X(1).
               03  ACH-ID-LAST-NINE-UT30                PIC X(9).
           02  ACH-STOP-IND-UT30                        PIC X(1).
           02  COMM-IND-UT30                            PIC X(1).
           02  ADR-TYPE-CODE-UT30                       PIC X(2).
           02  WK-END-DATE-KEY-COMP-UT30      COMP-3    PIC S9(8).
           02  WK-END-DATE-KEY-DISP-UT30                PIC X(8).
           02  WK-UPDATED-DATE-UT30                     PIC X(8).
           02  WK-UPDATED-BY-UT30                       PIC X(8).
           02  NAME-KEY-UT30                            PIC X(35).
           02  ENTY-PRSN-IND-UT30                       PIC X(1).
           02  WK-ACH-ERROR-REASON-UT30                 PIC X(3).
           02  CTX-PYMT-IND-UT30                        PIC X(1).
           02  CTX-PYMT-IND-ORIG                        PIC X(1).
           02  IAT-PYMT-IND-UT30                        PIC X(1).
           02  IAT-PYMT-IND-ORIG                        PIC X(1).
