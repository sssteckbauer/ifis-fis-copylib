       01  R6441-ENTYDCDVAL.
           02  DB-PROC-ID-6441.
               03  USER-CODE-6441                       PIC X(8).
               03  LAST-ACTVY-DATE-6441                 PIC X(5).
               03  TRMNL-ID-6441                        PIC X(8).
               03  PURGE-FLAG-6441                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  ENTY-DATA-CODE-6441                      PIC X(6).
           02  ENTY-DATA-VALUE-6441                     PIC X(15).
