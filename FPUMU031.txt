    ***Created by Convert/DC version V8R03 on 11/20/00 at 11:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE RQST-CODE-4070-PU03 OF FPUWK03 TO WK01-CODE-4070-PU03.
      *%--------------------------------------------------------------%*
           MOVE RQST-CMPLT-IND-4070-PU03 OF FPUWM03 TO
                WK01-CMPLT-IND-4070-PU03.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PU03 OF FPUWM03 TO WK01-CODE-PU03.
      *%--------------------------------------------------------------%*
           MOVE TRADE-IN-IND-PU03 OF FPUWM03 TO
                WK01-TRADE-IN-IND-PU03.
      *%--------------------------------------------------------------%*
           MOVE TRADE-IN-UCID-PU03 OF FPUWM03 TO
                WK01-TRADE-IN-UCID-PU03.
      *%--------------------------------------------------------------%*
           MOVE ACCESSORY-IND-PU03 OF FPUWM03 TO
                WK01-ACCESSORY-IND-PU03.
      *%--------------------------------------------------------------%*
           MOVE ACCESSORY-UCID-PU03 OF FPUWM03 TO
                WK01-ACCESSORY-UCID-PU03.
      *%--------------------------------------------------------------%*
           MOVE FABRICATION-IND-PU03 OF FPUWM03 TO
                WK01-FABRICATION-IND-PU03.
      *%--------------------------------------------------------------%*
           MOVE FABRICATION-NUM-PU03 OF FPUWM03 TO
                WK01-FABRICATION-NUM-PU03.
      *%--------------------------------------------------------------%*
           MOVE SHIP-TO-CODE-4071-PU03 OF FPUWM03 TO
                WK01-TO-CODE-4071-PU03.
      *%--------------------------------------------------------------%*
           MOVE RQST-NAME-4070-PU03 OF FPUWM03 TO WK01-NAME-4070-PU03.
      *%--------------------------------------------------------------%*
           MOVE APRVL-IND-4070-PU03 OF FPUWM03 TO WK01-IND-4070-PU03.
      *%--------------------------------------------------------------%*
           MOVE DTL-ERROR-IND-4071-PU03 OF FPUWM03 TO
                WK01-ERROR-IND-4071-PU03.
      *%--------------------------------------------------------------%*
           MOVE ITEM-NMBR-4071-PU03 OF FPUWM03 TO WK01-NMBR-4071-PU03.
      *%--------------------------------------------------------------%*
           MOVE CMDTY-CODE-4074-PU03 OF FPUWM03 TO WK01-CODE-4074-PU03.
      *%--------------------------------------------------------------%*
           MOVE CMDTY-DESC-4074-PU03 OF FPUWM03 TO WK01-DESC-4074-PU03.
      *%--------------------------------------------------------------%*
           MOVE UNIT-MEA-CODE-4071-PU03 OF FPUWM03 TO
                WK01-MEA-CODE-4071-PU03.
      *%--------------------------------------------------------------%*
           MOVE RQST-ITEM-NMBR-PU03 OF FPUWK03 TO WK01-ITEM-NMBR-PU03.
      *%--------------------------------------------------------------%*
           MOVE CNCL-DATE-4070-PU03 OF FPUWM03 TO WK01-DATE-4070-PU03.
      *%--------------------------------------------------------------%*
           MOVE DTL-CNTR-ACCT-4071-PU03 OF FPUWM03 TO
                WK01-CNTR-ACCT-4071-PU03.
      *%--------------------------------------------------------------%*
           MOVE UNIT-MEA-DESC-4075-PU03 OF FPUWM03 TO
                WK01-MEA-DESC-4075-PU03.
      *%--------------------------------------------------------------%*
           MOVE PRJCT-CODE-4071-PU03 OF FPUWM03 TO WK01-CODE-4071-PU03.
      *%--------------------------------------------------------------%*
           MOVE PO-NMBR-4083-PU03 OF FPUWM03 TO WK01-NMBR-4083-PU03.
      *%--------------------------------------------------------------%*
           MOVE PO-ITEM-NMBR-4085-PU03 OF FPUWM03 TO
                WK01-ITEM-NMBR-4085-PU03.
      *%--------------------------------------------------------------%*
           MOVE AGRMT-CODE-4071-PU03 OF FPUWM03 TO WK01-CODE-4071-PU0A.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-DATE-4071-PU03 OF FPUWM03 TO WK01-DATE-4071-PU03.
      *%--------------------------------------------------------------%*
           MOVE ITEM-COUNT-4070-PU03 OF FPUWM03 TO WK01-COUNT-4070-PU03.
      *%--------------------------------------------------------------%*
           MOVE QTY-4071-PU03 OF FPUWM03 TO WK01-4071-PU03.
      *%--------------------------------------------------------------%*
           MOVE UNIT-PRICE-4071-PU03 OF FPUWM03 TO WK01-PRICE-4071-PU03.
      *%--------------------------------------------------------------%*
           MOVE EXTENDED-PRICE-PU03 OF FPUWM03 TO WK01-PRICE-PU03.
      *%--------------------------------------------------------------%*
           MOVE TEXT-FLAG-PU03 OF FPUWM03 TO WK01-FLAG-PU03.
