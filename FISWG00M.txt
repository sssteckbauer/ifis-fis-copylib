      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FSYWG00M                             04/24/00  12:06  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FISWG00M.
           02  FSYWG00M.
               03  TRMNL-ID-SY00                        PIC X(8).
               03  FULL-NAME-6001-SY00                  PIC X(35).
               03  WORK-HH-MM-TIME-SY00                 PIC X(4).
               03  AM-PM-FLAG-SY00                      PIC X(1).
               03  CURR-RSPNS-ID-SY00                   PIC X(8).
               03  PAGE-STATUS-CODE-SY00                PIC X(1).
               03  ERROR-TEXT-SY00                      PIC X(14)
                   VALUE 'MORE ERRORS...'.
      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FSYWG00D                             04/24/00  11:53  *
      *                                                               *
      *---------------------------------------------------------------*
           02  FSYWG00D.
               03  MODULE-ID-SY00                       PIC X(2).
               03  MAIN-SUB-SY00              COMP-3    PIC 9(3).
               03  MSG-QTY-SY00               COMP-3    PIC 9(3).
               03  RSPNS-QTY-SY00             COMP-3    PIC 9(3).
               03  SUB-1-SY00                 COMP-3    PIC 9(3).
               03  SUB-2-SY00                 COMP-3    PIC 9(3).
               03  SUB-3-SY00                 COMP-3    PIC 9(3).
               03  SUB-4-SY00                 COMP-3    PIC 9(3).
               03  LIST-FLAG-SY00                       PIC X(1).
               03  PAGE-FLAG-SY00                       PIC X(1).
               03  VALID-KEY-FLAG-SY00                  PIC X(1).
               03  SAVE-DBKEY-ID-MINUS-ONE-SY00 COMP    PIC S9(8).
               03  SAVE-DBKEY-ID-PLUS-ONE-SY00 COMP     PIC S9(8).
               03  PRVS-RSPNS-GROUP-SY00.
                   04  PRVS-RSPNS-ID-SY00   OCCURS 10   PIC X(8).
               03  MSG-ID-SY00                          PIC 9(6).
               03  MAP-ELMNT-NAME-SY00                  PIC X(30).
               03  MSG-TEXT-GROUP-SY00.
                   04  MSG-TEXT-SY00        OCCURS 4    PIC X(40).
               03  APLCN-DATA-KEY-SY00                  PIC X(30).
               03  TERM-SEQ-ID-SY00                     PIC 9(4).
               03  WORK-TERM-SEQ-ID-SY00 REDEFINES
                   TERM-SEQ-ID-SY00                     PIC X(4).
               03  YEAR-DATE-SY00                       PIC 9(2).
               03  EFCTV-HH-MM-TIME-SY00                PIC X(4).
               03  EFCTV-AM-PM-FLAG-SY00                PIC X(1).
               03  EFCTV-TIME-SY00                      PIC X(6).
               03  SAVE-DBKEY-ID-SY00         COMP      PIC S9(8).
      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FSYWG01D                             04/24/00  12:52  *
      *                                                               *
      *---------------------------------------------------------------*
           02  FSYWG01D.
               03  ADD-PRMSN-FLAG-SY01                  PIC X(1).
               03  CHG-PRMSN-FLAG-SY01                  PIC X(1).
               03  DLT-PRMSN-FLAG-SY01                  PIC X(1).
               03  ATHRZN-FLAG-SY01                     PIC X(1).
               03  SCRTY-VLTN-FLAG-SY01                 PIC X(1).
               03  UNVRS-CODE-6001-SY01                 PIC X(2).
      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FSYWG99D                             04/24/00  12:59  *
      *                                                               *
      *---------------------------------------------------------------*
           02  FSYWG99D.
               03  UNVRS-CODE-6001-SY99                 PIC X(2).
               03  PRFL-CODE-8013-SY99                  PIC X(8).
      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FSYWG06D                             04/24/00  12:54  *
      *                                                               *
      *---------------------------------------------------------------*
           02  FSYWG06D.
               03  CNTNT-VLTN-FLAG-SY06                 PIC X(1).
               03  CNTNT-ACCESS-FLAG-SY06               PIC X(1).
               03  CNTNT-ELMNT-NAME-SY06                PIC X(8).
               03  CNTNT-DATA-VALUE-SY06                PIC X(8).
               03  DLG-CODE-SY06                        PIC X(8).
               03  SCRN-CODE-SY06                       PIC X(8).
               03  ADD-PRMSN-FLAG-SY06                  PIC X(1).
               03  CHNG-PRMSN-FLAG-SY06                 PIC X(1).
               03  DLT-PRMSN-FLAG-SY06                  PIC X(1).
      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FUTWG00D                             04/24/00  12:52  *
      *                                                               *
      *---------------------------------------------------------------*
           02  FUTWG00D.
               03  UNVRS-CODE-6001-UT00                 PIC X(2).
               03  WORK-DATE-UT00.
                   04  MONTH-DATE-UT00                  PIC X(2).
                   04  DAY-DATE-UT00                    PIC X(2).
                   04  YEAR-DATE-UT00                   PIC X(2).
               03  DCMNT-TYPE-4203-UT00                 PIC X(3).
               03  USER-ID-4206-UT00                    PIC X(8).
               03  VNDR-CODE-4073-UT00.
                   04  VNDR-ID-DIGIT-ONE-4073-UT00      PIC X(1).
                   04  VNDR-ID-LAST-NINE-4073-UT00      PIC X(9).
               03  STATE-CODE-6151-UT00                 PIC X(2).
               03  CNTRY-CODE-6152-UT00                 PIC X(2).
               03  DCMNT-TYPE-SEQ-NMBR-4203-UT00        PIC 9(4).
               03  TEXT-ENTY-CODE-4120-UT00             PIC X(15).
               03  CHNG-SEQ-NMBR-4120-UT00              PIC X(3).
               03  ITEM-NMBR-4120-UT00        COMP-3    PIC 9(4).
               03  DCMNT-NMBR-UT00                      PIC X(8).
               03  APRVL-TMPLT-CODE-UT00                PIC X(3).
               03  DATA-DESC-UT00                       PIC X(15).
               03  ENTY-PRSN-IND-UT00                   PIC X(1).
      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWG00D                             04/24/00  12:52  *
      *                                                               *
      *---------------------------------------------------------------*
           02  FAPWG00D.
               03  UNVRS-CODE-6001-AP00                 PIC X(2).
               03  DCMNT-NMBR-4150-AP00                 PIC X(8).
               03  VNDR-CODE-4073-AP00.
                   04  VNDR-ID-DIGIT-ONE-4073-AP00      PIC X(1).
                   04  VNDR-ID-LAST-NINE-4073-AP00      PIC X(9).
               03  PO-NMBR-4083-AP00                    PIC X(8).
               03  CHECK-NMBR-4158-AP00                 PIC X(8).
               03  BANK-ACCT-CODE-4158-AP00             PIC X(2).
               03  TAX-RATE-CODE-4168-AP00              PIC X(3).
               03  DSCNT-CODE-4161-AP00                 PIC X(2).
               03  VNDR-TYPE-CODE-4166-AP00             PIC X(2).
               03  ADJMT-CODE-4156-AP00                 PIC X(2).
               03  INCM-TYPE-CODE-4155-AP00             PIC X(2).
               03  ADR-TYPE-CODE-4073-AP00              PIC X(2).
               03  CMDTY-CODE-4074-AP00                 PIC X(8).
               03  ITEM-NMBR-4151-AP00                  PIC X(4).
               03  SEQ-NMBR-4152-AP00                   PIC X(4).
               03  COA-CODE-4000-AP00                   PIC X(1).
               03  RPRT-CODE-4060-AP00                  PIC X(8).
               03  ORGN-CODE-4162-AP00                  PIC X(4).
               03  PRNTR-DEST-CODE-4167-AP00            PIC X(8).
      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWG00D                             04/24/00  12:53  *
      *                                                               *
      *---------------------------------------------------------------*
           02  FPUWG00D.
               03  UNVRS-CODE-6001-PU00                 PIC X(2).
               03  VNDR-CODE-4073-PU00.
                   04  VNDR-ID-DIGIT-ONE-4073-PU00      PIC X(1).
                   04  VNDR-ID-LAST-NINE-4073-PU00      PIC X(9).
               03  RQST-CODE-4070-PU00                  PIC X(8).
               03  ITEM-NMBR-4071-PU00                  PIC X(4).
               03  SEQ-NMBR-4080-PU00                   PIC X(4).
               03  DCMNT-REF-NMBR-PU00                  PIC X(10).
               03  PO-NMBR-4083-PU00                    PIC X(8).
               03  CHNG-SEQ-NMBR-4096-PU00              PIC X(3).
               03  ITEM-NMBR-4085-PU00                  PIC X(4).
               03  SEQ-NMBR-4061-PU00                   PIC X(4).
               03  BID-NMBR-4088-PU00                   PIC X(8).
               03  ITEM-NMBR-4089-PU00                  PIC X(4).
               03  BUYER-CODE-4072-PU00                 PIC X(4).
               03  AGRMT-CODE-4081-PU00                 PIC X(15).
               03  CMDTY-CODE-4074-PU00                 PIC X(8).
               03  SHIP-TO-CODE-4086-PU00               PIC X(6).
               03  RTRN-RSN-CODE-4201-PU00              PIC X(4).
               03  COA-CODE-4000-PU00                   PIC X(1).
               03  ORGZN-CODE-4002-PU00                 PIC X(6).
               03  WORK-DATE-PU00.
                   04  MONTH-DATE-PU00                  PIC X(2).
                   04  DAY-DATE-PU00                    PIC X(2).
                   04  YEAR-DATE-PU00                   PIC X(2).
               03  PO-RTRN-CODE-4099-PU00               PIC X(8).
               03  CLAUSE-CODE-4209-PU00                PIC X(8).
               03  RPRT-CODE-4060-PU00                  PIC X(8).
               03  PRNTR-DEST-CODE-4167-PU00            PIC X(8).
