      *01  XHD1-STANDARD-REPORT-HEADER.                                 CPWSXHDR
           05  XHD1-STD-RPT-HDR-LINE-1.                                 CPWSXHDR
               10  XHD1-CTL                    PIC X(01).               CPWSXHDR
               10  XHD1-RPT-NAME               PIC X(08).               CPWSXHDR
               10  FILLER                      PIC X(03).               CPWSXHDR
               10  XHD1-RPT-TITLE              PIC X(98).               CPWSXHDR
               10  FILLER                      PIC X(06).               CPWSXHDR
               10  XHD1-RPT-PAGE-ID            PIC X(12).               CPWSXHDR
               10  XHD1-RPT-PAGE-NO            PIC Z(04)9(01).          CPWSXHDR
           05  XHD2-STD-RPT-HDR-LINE-2.                                 CPWSXHDR
               10  XHD2-CTL                    PIC X(01).               CPWSXHDR
               10  XHD2-LOC-ID                 PIC X(03).               CPWSXHDR
               10  FILLER                      PIC X(01).               CPWSXHDR
               10  XHD2-LOC                    PIC X(02).               CPWSXHDR
               10  FILLER                      PIC X(01).               CPWSXHDR
               10  XHD2-LOC-TITLE              PIC X(23).               CPWSXHDR
               10  FILLER                      PIC X(02).               CPWSXHDR
               10  XHD2-REPORT-SUB-TITLE       PIC X(74).               CPWSXHDR
               10  FILLER                      PIC X(05).               CPWSXHDR
               10  XHD2-REPORT-DATE-ID         PIC X(13).               CPWSXHDR
               10  XHD2-REPORT-DATE.                                    CPWSXHDR
                   15  XHD2-REPORT-MONTH       PIC X(02).               CPWSXHDR
                   15  XHD2-DASH1              PIC X(01).               CPWSXHDR
                   15  XHD2-REPORT-DAY         PIC X(02).               CPWSXHDR
                   15  XHD2-DASH2              PIC X(01).               CPWSXHDR
                   15  XHD2-REPORT-YEAR        PIC X(02).               CPWSXHDR
           05  XHD3-STD-RPT-HDR-LINE-3.                                 CPWSXHDR
               10  XHD3-CTL                    PIC X(01).               CPWSXHDR
               10  XHD3-SAU-ID                 PIC X(03).               CPWSXHDR
               10  FILLER                      PIC X(01).               CPWSXHDR
               10  XHD3-SAU                    PIC X(01).               CPWSXHDR
               10  FILLER                      PIC X(01).               CPWSXHDR
               10  XHD3-SAU-TITLE              PIC X(24).               CPWSXHDR
               10  FILLER                      PIC X(18).               CPWSXHDR
               10  XHD3-SUB-CAMPUS-ID          PIC X(10).               CPWSXHDR
               10  FILLER                      PIC X(01).               CPWSXHDR
               10  XHD3-SUB-CAMPUS             PIC X(01).               CPWSXHDR
               10  FILLER                      PIC X(01).               CPWSXHDR
               10  XHD3-SUB-CAMPUS-TITLE       PIC X(24).               CPWSXHDR
               10  FILLER                      PIC X(26).               CPWSXHDR
               10  XHD3-RUN-DATE-ID            PIC X(13).               CPWSXHDR
               10  XHD3-RUN-DATE.                                       CPWSXHDR
                   15  XHD3-RUN-MONTH          PIC X(02).               CPWSXHDR
                   15  XHD3-DASH1              PIC X(01).               CPWSXHDR
                   15  XHD3-RUN-DAY            PIC X(02).               CPWSXHDR
                   15  XHD3-DASH2              PIC X(01).               CPWSXHDR
                   15  XHD3-RUN-YEAR           PIC X(02).               CPWSXHDR
           05  XHDC-STD-HDR-CONSTANTS.                                  CPWSXHDR
               10  XHDC-RPT-PAGE-ID            PIC X(05) VALUE 'PAGE:'. CPWSXHDR
               10  XHDC-REPORT-SUB-TITLE.                               CPWSXHDR
                   15  FILLER                  PIC X(03) VALUE SPACES.  CPWSXHDR
                   15  FILLER                  PIC X(08) VALUE          CPWSXHDR
                       'PROGRAM'.                                       CPWSXHDR
                   15  XHDC-PROGRAM-NAME       PIC X(08).               CPWSXHDR
                   15  FILLER                  PIC X(10) VALUE          CPWSXHDR
                       ' SEQUENCE'.                                     CPWSXHDR
                   15  XHDC-SEQ-OPTION         PIC X(01).               CPWSXHDR
                   15  FILLER                  PIC X(09) VALUE          CPWSXHDR
                       ' VERSION'.                                      CPWSXHDR
                   15  XHDC-VERSION-NO         PIC X(02).               CPWSXHDR
                   15  FILLER                  PIC X(08) VALUE          CPWSXHDR
                       ' FORMAT'.                                       CPWSXHDR
                   15  XHDC-FORMAT-OPTION      PIC X(01).               CPWSXHDR
                   15  FILLER                  PIC X(16) VALUE          CPWSXHDR
                       ' SELECT-OPTIONS='.                              CPWSXHDR
                   15  XHDC-SELECT-OPTIONS     PIC X(08).               CPWSXHDR
               10  XHDC-REPORT-DATE-ID         PIC X(13) VALUE          CPWSXHDR
                   'REPORT DATE:'.                                      CPWSXHDR
               10  XHDC-LOC-ID                 PIC X(03) VALUE 'LOC'.   CPWSXHDR
               10  XHDC-SAU-ID                 PIC X(03) VALUE 'SAU'.   CPWSXHDR
               10  XHDC-SUB-CAMPUS-ID          PIC X(10) VALUE          CPWSXHDR
                   'SUB-CAMPUS'.                                        CPWSXHDR
               10  XHDC-JOB-DATE-ID            PIC X(13) VALUE          CPWSXHDR
                   'JOB DATE:'.                                         CPWSXHDR
               10  XHDC-RUN-DATE-ID            PIC X(13) VALUE          CPWSXHDR
                   'RUN DATE:'.                                         CPWSXHDR
