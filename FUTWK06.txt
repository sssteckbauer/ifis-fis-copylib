      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FUTWK06                              04/24/00  12:48  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FUTWK06.
           02  WORK-PRSN-ID-UT06.
               03  WORK-PRSN-ID-ONE-UT06                PIC X(3).
               03  WORK-PRSN-ID-TWO-UT06                PIC X(2).
               03  WORK-PRSN-ID-THREE-UT06              PIC X(4).
           02  RQST-ADR-TYPE-CODE-UT06                  PIC X(2).
           02  WHICH-ID-PROCESS-UT06                    PIC X(1).
