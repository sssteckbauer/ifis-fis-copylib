      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4173-FIMS-NAME' TO DBLINK-CURR-PARAGRAPH.    276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4173-FIMS-NAME' TO RECORD-NAME.                       276 BRTN
YYY991     MOVE 'F-TABLE' TO AREA-NAME.                                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4173-FIMS-NAME TO DBLINK-RECORD-MADE-CURRENT.   276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4173-FIMS-NAME-EXIT                      276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE FNM-IREF-ID TO DBLINK-R4173-FIMS-NAME-01            276 BRTN
YYY991         MOVE FNM-ENTPSN-IND TO DBLINK-R4173-FIMS-NAME-02         276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4173-FIMS-NAME TO DBLINK-VIEW-NAME-CURRENT.     276 BRTN
YYY991     MOVE DBLINK-R4173-FIMS-NAME-KEY TO DBLINK-VIEW-200-CURRENT.  276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-AGNT-NAME                                            276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-S-INDX-AGNT-NAME-F          276 BRTN
YYY991     IF FNM-IDX-AGNT-SETF = DBLINK-MEMBER                         276 BRTN
YYY991         MOVE FNM-IDX-AGNT-SETF TO DBLINK-S-INDX-AGNT-NAME-F      276 BRTN
YYY991         MOVE FNM-NAME-KEY TO DBLINK-S-INDX-AGNT-NAME-01          276 BRTN
YYY991         MOVE FNM-IDX-AGNT-TS TO DBLINK-S-INDX-AGNT-NAME-02       276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-BNK-NAME                                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-S-INDX-BNK-NAME-F           276 BRTN
YYY991     IF FNM-IDX-BNK-SETF = DBLINK-MEMBER                          276 BRTN
YYY991         MOVE FNM-IDX-BNK-SETF TO DBLINK-S-INDX-BNK-NAME-F        276 BRTN
YYY991         MOVE FNM-NAME-KEY TO DBLINK-S-INDX-BNK-NAME-01           276 BRTN
YYY991         MOVE FNM-IDX-BNK-TS TO DBLINK-S-INDX-BNK-NAME-02         276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-BUYR-NAME                                            276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-S-INDX-BUYR-NAME-F          276 BRTN
YYY991     IF FNM-IDX-BUYR-SETF = DBLINK-MEMBER                         276 BRTN
YYY991         MOVE FNM-IDX-BUYR-SETF TO DBLINK-S-INDX-BUYR-NAME-F      276 BRTN
YYY991         MOVE FNM-NAME-KEY TO DBLINK-S-INDX-BUYR-NAME-01          276 BRTN
YYY991         MOVE FNM-IDX-BUYR-TS TO DBLINK-S-INDX-BUYR-NAME-02       276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-CUST-NAME                                            276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-S-INDX-CUST-NAME-F          276 BRTN
YYY991     IF FNM-IDX-CUST-SETF = DBLINK-MEMBER                         276 BRTN
YYY991         MOVE FNM-IDX-CUST-SETF TO DBLINK-S-INDX-CUST-NAME-F      276 BRTN
YYY991         MOVE FNM-NAME-KEY TO DBLINK-S-INDX-CUST-NAME-01          276 BRTN
YYY991         MOVE FNM-IDX-CUST-TS TO DBLINK-S-INDX-CUST-NAME-02       276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-FNCL-NAME                                            276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-S-INDX-FNCL-NAME-F          276 BRTN
YYY991     IF FNM-IDX-FNCL-SETF = DBLINK-MEMBER                         276 BRTN
YYY991         MOVE FNM-IDX-FNCL-SETF TO DBLINK-S-INDX-FNCL-NAME-F      276 BRTN
YYY991         MOVE FNM-NAME-KEY TO DBLINK-S-INDX-FNCL-NAME-01          276 BRTN
YYY991         MOVE FNM-IDX-FNCL-TS TO DBLINK-S-INDX-FNCL-NAME-02       276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-TRVL-NAME                                            276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-S-INDX-TRVL-NAME-F          276 BRTN
YYY991     IF FNM-IDX-TRVL-SETF = DBLINK-MEMBER                         276 BRTN
YYY991         MOVE FNM-IDX-TRVL-SETF TO DBLINK-S-INDX-TRVL-NAME-F      276 BRTN
YYY991         MOVE FNM-NAME-KEY TO DBLINK-S-INDX-TRVL-NAME-01          276 BRTN
YYY991         MOVE FNM-IDX-TRVL-TS TO DBLINK-S-INDX-TRVL-NAME-02       276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-VNDR-NAME                                            276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-S-INDX-VNDR-NAME-F          276 BRTN
YYY991     IF FNM-IDX-VNDR-SETF = DBLINK-MEMBER                         276 BRTN
YYY991         MOVE FNM-IDX-VNDR-SETF TO DBLINK-S-INDX-VNDR-NAME-F      276 BRTN
YYY991         MOVE FNM-NAME-KEY TO DBLINK-S-INDX-VNDR-NAME-01          276 BRTN
YYY991         MOVE FNM-IDX-VNDR-TS TO DBLINK-S-INDX-VNDR-NAME-02       276 BRTN
YYY991     END-IF.                                                      276 BRTN
