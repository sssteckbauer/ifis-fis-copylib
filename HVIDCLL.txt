       975-TO-R6274 SECTION.
           MOVE CLL-USER-CD TO
               USER-CODE-6274 OF R6274
                                                                     .
           MOVE CLL-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-6274 OF R6274
                                                                     .
           MOVE CLL-START-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               START-DATE-6274 OF R6274
                                                                     .
           MOVE CLL-FK-CLG-COMM-REFNBR TO
               COMM-REF-NMBR-6274 OF R6274
                                                                     .
           MOVE CLL-TEXT-CD TO
               TEXT-CODE-6274 OF R6274
                                                                     .
           MOVE CLL-OFC-CD TO
               OFC-CODE-6274 OF R6274
                                                                     .
           MOVE CLL-COMM-NAME TO
               COMM-NAME-6274 OF R6274
                                                                     .
           MOVE CLL-COMM-ADR-TYP TO
               COMM-ADR-TYPE-6274 OF R6274
                                                                     .
           MOVE CLL-COMM-VARIABLE TO
               COMM-VARIABLE-6274 OF R6274
                                                                     .
       975-TO-R6274-EXIT.
           EXIT.
