       01  R4120-TXT-HDR.
           02  DB-PROC-ID-4120.
               03  USER-CODE-4120                       PIC X(8).
               03  LAST-ACTVY-DATE-4120                 PIC X(5).
               03  TRMNL-ID-4120                        PIC X(8).
               03  PURGE-FLAG-4120                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  TEXT-KEY-4120.
               03  UNVRS-CODE-4120                      PIC X(2).
               03  DCMNT-TYPE-SEQ-NMBR-4120             PIC 9(4).
               03  TEXT-ENTY-CODE-4120                  PIC X(15).
               03  CHNG-SEQ-NMBR-4120                   PIC X(3).
               03  ITEM-NMBR-4120             COMP-3    PIC 9(4).
