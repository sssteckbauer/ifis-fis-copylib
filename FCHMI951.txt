    ***Created by Convert/DC version V8R03 on 11/09/00 at 14:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE ORGZN-CODE-4002-CH95 OF FCHWM95 TO WK01-CODE-4002-CH95.
      *%--------------------------------------------------------------%*
           MOVE ORGZN-TITLE-4010-CH95 OF FCHWM95 TO
                WK01-TITLE-4010-CH95.
      *%--------------------------------------------------------------%*
           MOVE PREDCSR-CODE-4010-CH95 OF FCHWM95 TO
                WK01-CODE-4010-CH95.
      *%--------------------------------------------------------------%*
           MOVE PREDCSR-TITLE-4010-CH95 OF FCHWM95 TO
                WK01-TITLE-4010-CH9A.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4002-CH95 OF FCHWM95 TO WK01-CODE-4002-CH9A.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-TITLE-4012-CH95 OF FCHWM95(0001) TO
                WK01-TITLE-4012-CH951.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-TITLE-4012-CH95 OF FCHWM95(0002) TO
                WK01-TITLE-4012-CH952.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-TITLE-4012-CH95 OF FCHWM95(0003) TO
                WK01-TITLE-4012-CH953.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-TITLE-4012-CH95 OF FCHWM95(0004) TO
                WK01-TITLE-4012-CH954.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-TITLE-4012-CH95 OF FCHWM95(0005) TO
                WK01-TITLE-4012-CH955.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-TITLE-4012-CH95 OF FCHWM95(0006) TO
                WK01-TITLE-4012-CH956.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-TITLE-4012-CH95 OF FCHWM95(0007) TO
                WK01-TITLE-4012-CH957.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-TITLE-4012-CH95 OF FCHWM95(0008) TO
                WK01-TITLE-4012-CH958.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-ORGZN-4002-CH95 OF FCHWM95(0001) TO
                WK01-ORGZN-4002-CH951.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-ORGZN-4002-CH95 OF FCHWM95(0002) TO
                WK01-ORGZN-4002-CH952.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-ORGZN-4002-CH95 OF FCHWM95(0003) TO
                WK01-ORGZN-4002-CH953.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-ORGZN-4002-CH95 OF FCHWM95(0004) TO
                WK01-ORGZN-4002-CH954.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-ORGZN-4002-CH95 OF FCHWM95(0005) TO
                WK01-ORGZN-4002-CH955.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-ORGZN-4002-CH95 OF FCHWM95(0006) TO
                WK01-ORGZN-4002-CH956.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-ORGZN-4002-CH95 OF FCHWM95(0007) TO
                WK01-ORGZN-4002-CH957.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-ORGZN-4002-CH95 OF FCHWM95(0008) TO
                WK01-ORGZN-4002-CH958.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-LONG-DESC-4012-CH95 OF FCHWM95(0001) TO
                WK01-LONG-DESC-4012-CH951.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-LONG-DESC-4012-CH95 OF FCHWM95(0002) TO
                WK01-LONG-DESC-4012-CH952.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-LONG-DESC-4012-CH95 OF FCHWM95(0003) TO
                WK01-LONG-DESC-4012-CH953.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-LONG-DESC-4012-CH95 OF FCHWM95(0004) TO
                WK01-LONG-DESC-4012-CH954.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-LONG-DESC-4012-CH95 OF FCHWM95(0005) TO
                WK01-LONG-DESC-4012-CH955.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-LONG-DESC-4012-CH95 OF FCHWM95(0006) TO
                WK01-LONG-DESC-4012-CH956.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-LONG-DESC-4012-CH95 OF FCHWM95(0007) TO
                WK01-LONG-DESC-4012-CH957.
      *%--------------------------------------------------------------%*
           MOVE HRCHY-LONG-DESC-4012-CH95 OF FCHWM95(0008) TO
                WK01-LONG-DESC-4012-CH958.
