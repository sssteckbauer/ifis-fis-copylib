       975-TO-R4306-TIME-J SECTION.
           MOVE TMJ-USER-CD TO
               USER-CODE-4306 OF R4306-TIME-J
                                                                     .
           MOVE TMJ-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4306 OF R4306-TIME-J
                                                                     .
           MOVE TMJ-START-DT TO
               START-DATE-4306 OF R4306-TIME-J
                                                                     .
           MOVE TMJ-TIME-STMP TO
               TIME-STAMP-4306 OF R4306-TIME-J
                                                                     .
           MOVE TMJ-END-DT TO
               END-DATE-4306 OF R4306-TIME-J
                                                                     .
           MOVE TMJ-TAG-NBR TO
               TAG-NMBR-4306 OF R4306-TIME-J
                                                                     .
       975-TO-R4306-TIME-J-EXIT.
           EXIT.
