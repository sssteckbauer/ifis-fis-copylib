      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FUTWM07                              04/24/00  12:48  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FUTWM07.
           02  ACTN-CODE-GROUP-UT07.
               03  ACTN-CODE-UT07           OCCURS 12   PIC X(1).
           02  LONG-DESC-6137-UT07          OCCURS 12   PIC X(30).
           02  SHORT-DESC-6137-UT07         OCCURS 12   PIC X(10).
           02  DSPLY-PRTY-SEQ-6137-UT07     OCCURS 12   PIC XX.
           02  ADR-TYPE-CODE-6137-UT07      OCCURS 12   PIC X(2).
