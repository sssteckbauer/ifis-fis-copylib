      *%---------------------------------------------------------------*
      *%                                                               *
      *%                   200-FSYIA05 ROUTINE                         *
      *%                                                               *
      *%---------------------------------------------------------------*
       200-FSYIA05.

      ******************************************************************
      ******
      ***  CHANGE APRVL TEMPLATE CODE
      ***  THIS INCLUDE MODULE MUST BE PART OF A SUBROUTINE.
      ***  THE FOLLOWING FIELDS MUST BE SET BY THE DIALOG THAT USES THIS
      ***  INCLUDE
      ***   SEQ-NMBR-SY08, DCMNT-NMBR-SY08, CHNG-SEQ-NMBR-SY08
      ***      APRVL-IND-SY08, APRVL-TMPLT-CODE-SY08
      ***  THE FOLLOWING SUBROUTINES MUST BE PART OF THE DIALOG THAT USE
      *S THIS
      ***      APRVLERR
      ***
      ******
      *
           MOVE UNVRS-CODE-SY08 TO UNVRS-CODE-4212.
           MOVE SEQ-NMBR-SY08 TO SEQ-NMBR-4212.
           MOVE DCMNT-NMBR-SY08 TO DCMNT-NMBR-4212.
           MOVE CHNG-SEQ-NMBR-SY08 TO CHNG-SEQ-NMBR-4212.
           MOVE FSCL-YR-SY08 TO FSCL-YR-4212.
      *%   OBTAIN CALC R4212-APRVL-HDR.
           MOVE 0750 TO DBLINK-CURR-IO-NUM
           MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
           MOVE DBLINK-CALC TO DBLINK-TYPE-GET
           MOVE FSCL-YR-4212 OF R4212-APRVL-HDR TO APH-FSCL-YR
           MOVE SEQ-NMBR-4212 OF R4212-APRVL-HDR TO APH-SEQ-NBR
           MOVE DCMNT-NMBR-4212 OF R4212-APRVL-HDR TO APH-DOC-NBR
           MOVE CHNG-SEQ-NMBR-4212 OF R4212-APRVL-HDR TO
               APH-CHNG-SEQ-NBR
           PERFORM 906-05-R4212-APRVL-HDR
              THRU 906-05-R4212-APRVL-HDR-EXIT
           PERFORM 999-SQL-STATUS
              THRU 999-SQL-STATUS-EXIT.
           IF DB-REC-NOT-FOUND
               IF (SEQ-NMBR-SY08 = '0007' OR
                  SEQ-NMBR-SY08 = '0012')
                   MOVE SPACES TO FSCL-YR-4212
      *%           OBTAIN CALC R4212-APRVL-HDR
                   MOVE 0751 TO DBLINK-CURR-IO-NUM
                   MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
                   MOVE DBLINK-CALC TO DBLINK-TYPE-GET
                   MOVE FSCL-YR-4212 OF R4212-APRVL-HDR TO APH-FSCL-YR
                   MOVE SEQ-NMBR-4212 OF R4212-APRVL-HDR TO APH-SEQ-NBR
                   MOVE DCMNT-NMBR-4212 OF R4212-APRVL-HDR TO
                       APH-DOC-NBR
                   MOVE CHNG-SEQ-NMBR-4212 OF R4212-APRVL-HDR TO
                       APH-CHNG-SEQ-NBR
                   PERFORM 906-05-R4212-APRVL-HDR
                      THRU 906-05-R4212-APRVL-HDR-EXIT
                   PERFORM 999-SQL-STATUS
                      THRU 999-SQL-STATUS-EXIT
                   IF DB-REC-NOT-FOUND
                       MOVE 001543 TO MSG-ID-SY00
                       PERFORM 200-ZZYII03-GETMESGS
                          THRU 200-ZZYII03-GETMESGS-EXIT
                       MOVE 'Y' TO APRVL-ERROR-SY08
                       MOVE AC99W-YES-DISPLAY TO AC99W-FLAG-DISPLAY
                       GO TO 200-FSYIA05-EXIT
                   END-IF
               ELSE
                   MOVE 001543 TO MSG-ID-SY00
                   PERFORM 200-ZZYII03-GETMESGS
                      THRU 200-ZZYII03-GETMESGS-EXIT
                   MOVE 'Y' TO APRVL-ERROR-SY08
                   MOVE AC99W-YES-DISPLAY TO AC99W-FLAG-DISPLAY
      *** UNAPRVD REC NOT FOUND - SYSTEM PROBLEM
                      GO TO 200-FSYIA05-EXIT
               END-IF
           END-IF.
      *
           IF  APH-APL-SETF   = 'N'
               MOVE 510017 TO MSG-ID-SY00
               PERFORM 200-ZZYII03-GETMESGS
                  THRU 200-ZZYII03-GETMESGS-EXIT
               MOVE 'Y' TO APRVL-ERROR-SY08
               MOVE AC99W-YES-DISPLAY TO AC99W-FLAG-DISPLAY
      **005DMW CHG TMPLT NOT ALLOWED AFTER FINAL APPVL
               GO TO 200-FSYIA05-EXIT
           END-IF.
      *
      *%  ERASE R4214-APRVL-AUD
           MOVE 0752 TO DBLINK-CURR-IO-NUM
           EXEC SQL
               DELETE FROM APD_APRVL_AUD_V
               WHERE FK_APH_FSCL_YR = :APH-FSCL-YR
                 AND FK_APH_SEQ_NBR = :APH-SEQ-NBR
                 AND FK_APH_DOC_NBR = :APH-DOC-NBR
                 AND FK_APH_CHG_SEQ_NBR = :APH-CHNG-SEQ-NBR
           END-EXEC.
           IF SQLCODE NOT = +0 AND +100
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
           END-IF

           MOVE SEQ-NMBR-SY08       TO SEQ-NMBR-4203
                                       DBLINK-S-4204-4215-01
           MOVE AGR-USER-ID         TO USER-ID-4204
                                       DBLINK-S-4204-4215-02.
      *
           MOVE APRVL-TMPLT-CODE-SY08 TO APRVL-TMPLT-CODE-4215.
           IF APRVL-TMPLT-CODE-SY08 NOT = SPACES
      *%       OBTAIN R4215-APRV-TMPLT WITHIN S-4204-4215 USING
      *%        APRVL-TMPLT-CODE-4215
               MOVE 0753 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-SORTED TO DBLINK-TYPE-GET
               MOVE APRVL-TMPLT-CODE-4215 TO DBLINK-S-4204-4215-03
               PERFORM 906-02-S-4204-4215
                  THRU 906-02-S-4204-4215-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
               IF DB-REC-NOT-FOUND
                   MOVE 002289 TO MSG-ID-SY00
                   PERFORM 200-ZZYII03-GETMESGS
                      THRU 200-ZZYII03-GETMESGS-EXIT
                   MOVE 'Y' TO APRVL-ERROR-SY08
      *** UNAPRVD REC NOT FOUND
      *%           ROLLBACK
                   MOVE 0754 TO DBLINK-CURR-IO-NUM
                   MOVE +0 TO SQLCODE
                   MOVE ZERO TO ERROR-STATUS
                   PERFORM 986-RESET-KEYS
                      THRU 986-RESET-KEYS-EXIT
                   PERFORM 997-ROLLBACK
                      THRU 997-ROLLBACK-EXIT
                   PERFORM 999-SQL-STATUS
                      THRU 999-SQL-STATUS-EXIT
                   MOVE AC99W-YES-DISPLAY TO AC99W-FLAG-DISPLAY
                   GO TO 200-FSYIA05-EXIT
               END-IF
           ELSE
      *%       OBTAIN FIRST R4215-APRV-TMPLT WITHIN S-4204-4215
               MOVE 0755 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-FIRST TO DBLINK-TYPE-GET
               PERFORM 906-00N-S-4204-4215
                  THRU 906-00N-S-4204-4215-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
               IF DB-END-OF-SET
                   MOVE 002289 TO MSG-ID-SY00
                   PERFORM 200-ZZYII03-GETMESGS
                      THRU 200-ZZYII03-GETMESGS-EXIT
      *** UNAPRVD REC NOT FOUND
      *%           ROLLBACK
                   MOVE 0756 TO DBLINK-CURR-IO-NUM
                   MOVE +0 TO SQLCODE
                   MOVE ZERO TO ERROR-STATUS
                   PERFORM 986-RESET-KEYS
                      THRU 986-RESET-KEYS-EXIT
                   PERFORM 997-ROLLBACK
                      THRU 997-ROLLBACK-EXIT
                   PERFORM 999-SQL-STATUS
                      THRU 999-SQL-STATUS-EXIT
                      MOVE 'Y' TO APRVL-ERROR-SY08
                      MOVE AC99W-YES-DISPLAY TO AC99W-FLAG-DISPLAY
                      GO TO 200-FSYIA05-EXIT
               END-IF
               MOVE APRVL-TMPLT-CODE-4215 TO APRVL-TMPLT-CODE-SY08
           END-IF.
      *
      *%   OBTAIN NEXT R4205-APRVL-LVL WITHIN S-4215-4205.
           MOVE 0757 TO DBLINK-CURR-IO-NUM
           MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
           MOVE DBLINK-NEXT TO DBLINK-TYPE-GET
           PERFORM 906-00N-S-4215-4205
              THRU 906-00N-S-4215-4205-EXIT
           PERFORM 999-SQL-STATUS
              THRU 999-SQL-STATUS-EXIT.
           IF DB-END-OF-SET
               MOVE 001200 TO MSG-ID-SY00
               PERFORM 200-ZZYII03-GETMESGS
                  THRU 200-ZZYII03-GETMESGS-EXIT

      *** UNAPRVD REC NOT FOUND
      *%       ROLLBACK
               MOVE 0758 TO DBLINK-CURR-IO-NUM
               MOVE +0 TO SQLCODE
               MOVE ZERO TO ERROR-STATUS
               PERFORM 986-RESET-KEYS
                  THRU 986-RESET-KEYS-EXIT
               PERFORM 997-ROLLBACK
                  THRU 997-ROLLBACK-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
                  MOVE 'Y' TO APRVL-ERROR-SY08
                  MOVE AC99W-YES-DISPLAY TO AC99W-FLAG-DISPLAY
                  GO TO 200-FSYIA05-EXIT
           END-IF.


           MOVE 'N' TO APRVL-IND-4212.
           MOVE APRVL-ID-4205 TO APRVL-ID-4212.
           MOVE APRVL-TMPLT-CODE-SY08 TO APRVL-TMPLT-CODE-4212.
           MOVE AGR-USER-ID TO USER-ID-4212.
      *%   MODIFY R4212-APRVL-HDR.
           MOVE 0759 TO DBLINK-CURR-IO-NUM
           MOVE DBLINK-MODIFY TO DBLINK-TYPE-SELECT
           MOVE DBLINK-MODIFY TO DBLINK-TYPE-GET
           PERFORM 976-FROM-R4212-APRVL-HDR
              THRU 976-FROM-R4212-APRVL-HDR-EXIT
           PERFORM 904-UPDT-R4212-APRVL-HDR
              THRU 904-UPDT-R4212-APRVL-HDR-EXIT
           PERFORM 999-SQL-STATUS
              THRU 999-SQL-STATUS-EXIT.

           IF ANY-ERROR-STATUS
               MOVE 992889 TO MSG-ID-SY00
               PERFORM 200-ZZYII03-GETMESGS
                  THRU 200-ZZYII03-GETMESGS-EXIT
               MOVE 'Y' TO APRVL-ERROR-SY08
               MOVE AC99W-YES-DISPLAY TO AC99W-FLAG-DISPLAY
               GO TO 200-FSYIA05-EXIT
           END-IF.

           MOVE DBLINK-S-4205-4212-01  TO APH-FK-APL-SEQ-NBR
           MOVE DBLINK-S-4205-4212-02  TO APH-FK-APL-USER-ID
           MOVE DBLINK-S-4205-4212-03  TO APH-FK-APL-APV-TPLT-CD
           MOVE DBLINK-S-4205-4212-04  TO APH-FK-APL-LVL-SEQ-NBR

           PERFORM 998-GET-TIMESTAMP
              THRU 998-GET-TIMESTAMP-EXIT

           MOVE DBLINK-TIMESTAMP       TO APH-APL-SET-TS
                                          APH-IDX-APRVL-TS
           MOVE DBLINK-MEMBER          TO APH-APL-SETF
                                          APH-IDX-APRVL-SETF
                                          APH-IDX-UNAPRVD-SETF.

           MOVE 0750 TO DBLINK-CURR-IO-NUM
           MOVE ZERO TO SQLCODE
                        ERROR-STATUS.

           EXEC SQL
               UPDATE APH_APRVL_HDR_V
               SET      APRVL_ID =
                 :APH-APRVL-ID
                   ,FK_APL_SEQ_NBR =
                 :APH-FK-APL-SEQ-NBR
                   ,FK_APL_USER_ID =
                 :APH-FK-APL-USER-ID
                   ,FK_APL_APV_TPLT_CD =
                 :APH-FK-APL-APV-TPLT-CD
                   ,FK_APL_LVL_SEQ_NBR =
                 :APH-FK-APL-LVL-SEQ-NBR
                   ,APL_SET_TS =
                 :APH-APL-SET-TS
                   ,APL_SETF =
                 :DBLINK-MEMBER

              WHERE FSCL_YR = :DBLINK-R4212-APRVL-HDR-01
                AND SEQ_NBR = :DBLINK-R4212-APRVL-HDR-02
                AND DOC_NBR = :DBLINK-R4212-APRVL-HDR-03
                AND CHNG_SEQ_NBR = :DBLINK-R4212-APRVL-HDR-04
           END-EXEC.

              IF SQLCODE NOT = 0
                 MOVE 992889 TO MSG-ID-SY00
                 PERFORM 200-ZZYII03-GETMESGS
                    THRU 200-ZZYII03-GETMESGS-EXIT
                 MOVE 'Y' TO APRVL-ERROR-SY08
                 MOVE AC99W-YES-DISPLAY TO AC99W-FLAG-DISPLAY
                 GO TO 200-FSYIA05-EXIT
           END-IF.

           MOVE 'N' TO APRVL-IND-SY08.
      *
       200-FSYIA05-EXIT.
           EXIT.
