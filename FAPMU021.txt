    ***Created by Convert/DC version V8R03 on 12/04/00 at 13:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-NMBR-4150-AP02 OF FAPWK02 TO WK01-NMBR-4150-AP02.
      *%--------------------------------------------------------------%*
           MOVE VNDR-CODE-4150-AP02 OF FAPWM02 TO WK01-CODE-4150-AP02.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-AP02 OF FAPWM02 TO WK01-CODE-AP02.
      *%--------------------------------------------------------------%*
           MOVE CMDTY-CODE-4151-AP02 OF FAPWM02 TO WK01-CODE-4151-AP02.
      *%--------------------------------------------------------------%*
           MOVE UNIT-MEA-CODE-4151-AP02 OF FAPWM02 TO
                WK01-MEA-CODE-4151-AP02.
      *%--------------------------------------------------------------%*
           MOVE VNDR-NAME-AP02 OF FAPWM02 TO WK01-NAME-AP02.
      *%--------------------------------------------------------------%*
           MOVE CMDTY-DESC-4151-AP02 OF FAPWM02 TO WK01-DESC-4151-AP02.
      *%--------------------------------------------------------------%*
           MOVE UNIT-MEA-DESC-4075-AP02 OF FAPWM02 TO
                WK01-MEA-DESC-4075-AP02.
      *%--------------------------------------------------------------%*
           MOVE ORDRD-QTY-4085-AP02 OF FAPWM02 TO WK01-QTY-4085-AP02.
      *%--------------------------------------------------------------%*
           MOVE ORDRD-UNIT-PRICE-4085-AP02 OF FAPWM02 TO
                WK01-UNIT-PRICE-4085-AP02.
      *%--------------------------------------------------------------%*
           MOVE ORDRD-EXTND-PRICE-AP02 OF FAPWM02 TO
                WK01-EXTND-PRICE-AP02.
      *%--------------------------------------------------------------%*
           MOVE ACCPT-QTY-4151-AP02 OF FAPWM02 TO WK01-QTY-4151-AP02.
      *%--------------------------------------------------------------%*
           MOVE ACCPT-UNIT-PRICE-4151-AP02 OF FAPWM02 TO
                WK01-UNIT-PRICE-4151-AP02.
      *%--------------------------------------------------------------%*
           MOVE ACCPT-EXTND-PRICE-AP02 OF FAPWM02 TO
                WK01-EXTND-PRICE-AP0A.
      *%--------------------------------------------------------------%*
           MOVE PREV-PAID-AMT-AP02 OF FAPWM02 TO WK01-PAID-AMT-AP02.
      *%--------------------------------------------------------------%*
           MOVE TO-BE-PAID-AMT-AP02 OF FAPWM02 TO WK01-BE-PAID-AMT-AP02.
      *%--------------------------------------------------------------%*
           MOVE INVD-QTY-4151-AP02 OF FAPWM02 TO WK01-QTY-4151-AP0A.
      *%--------------------------------------------------------------%*
           MOVE INVD-UNIT-PRICE-4151-AP02 OF FAPWM02 TO
                WK01-UNIT-PRICE-4151-AP0A.
      *%--------------------------------------------------------------%*
           MOVE INVD-EXTND-PRICE-AP02 OF FAPWM02 TO
                WK01-EXTND-PRICE-AP0B.
      *%--------------------------------------------------------------%*
           MOVE APRVD-QTY-4151-AP02 OF FAPWM02 TO WK01-QTY-4151-AP0B.
      *%--------------------------------------------------------------%*
           MOVE APRVD-UNIT-PRICE-4151-AP02 OF FAPWM02 TO
                WK01-UNIT-PRICE-4151-AP0B.
      *%--------------------------------------------------------------%*
           MOVE APRVD-EXTND-PRICE-AP02 OF FAPWM02 TO
                WK01-EXTND-PRICE-AP0C.
      *%--------------------------------------------------------------%*
           MOVE PRD-DSCNT-AMT-4151-AP02 OF FAPWM02
                                            TO WK01-PRD-AMT-4151-AP02.
      *%--------------------------------------------------------------%*
           MOVE DSCNT-AMT-4151-AP02 OF FAPWM02 TO WK01-AMT-4151-AP02.
      *%--------------------------------------------------------------%*
           MOVE TRADE-IN-AMT-AP02 OF FAPWM02 TO WK01-TRADE-IN-AMT-AP02.
      *%--------------------------------------------------------------%*
           MOVE TAX-AMT-4151-AP02 OF FAPWM02 TO WK01-AMT-4151-AP0A.
      *%--------------------------------------------------------------%*
           MOVE ADDL-AMT-AP02 OF FAPWM02 TO WK01-AMT-AP02.
      *%--------------------------------------------------------------%*
           MOVE PO-ITEM-NMBR-4151-AP02 OF FAPWM02 TO
                WK01-ITEM-NMBR-4151-AP02.
      *%--------------------------------------------------------------%*
           MOVE DTL-ERROR-IND-AP02 OF FAPWM02 TO WK01-ERROR-IND-AP02.
      *%--------------------------------------------------------------%*
DEVMJM*
           MOVE DOC-TEXT-IND-AP02 OF FAPWM02
                TO WK01-DOC-TEXT-IND-AP02.
      *%--------------------------------------------------------------%*
           MOVE AWARD-TEXT-IND-AP02 OF FAPWM02
                TO WK01-AWARD-TEXT-IND-AP02.
      *%--------------------------------------------------------------%*
DEVMJM*
           MOVE DTL-CNTR-ACCT-AP02 OF FAPWM02 TO WK01-CNTR-ACCT-AP02.
      *%--------------------------------------------------------------%*
           MOVE TRADE-IN-IND-AP02 OF FAPWM02
                TO WK01-TRADE-IN-IND-AP02.
      *%--------------------------------------------------------------%*
           MOVE CMDTY-ADD-IND-AP02 OF FAPWM02 TO WK01-ADD-IND-AP02.
      *%--------------------------------------------------------------%*
           MOVE PO-NMBR-4150-AP02 OF FAPWM02 TO WK01-NMBR-4150-AP0A.
      *%--------------------------------------------------------------%*
           MOVE ITEM-NMBR-4151-AP02 OF FAPWK02 TO WK01-NMBR-4151-AP02.
      *%--------------------------------------------------------------%*
           MOVE TLRNC-OVRDE-IND-4151-AP02 OF FAPWM02 TO
                WK01-OVRDE-IND-4151-AP02.
      *%--------------------------------------------------------------%*
           MOVE TOTAL-AMT-ORDERED-AP02 OF FAPWM02 TO
                WK01-AMT-ORDERED-AP02.
      *%--------------------------------------------------------------%*
           MOVE PO-DSCNT-AMT-AP02 OF FAPWM02 TO WK01-DSCNT-AMT-AP02.
      *%--------------------------------------------------------------%*
           MOVE ITEM-DSCNT-AMT-AP02 OF FAPWM02 TO WK01-DSCNT-AMT-AP0A.
      *%--------------------------------------------------------------%*
           MOVE TRADE-IN-AMT-2-AP02 OF FAPWM02 TO
                     WK01-TRADE-IN-AMT-2-AP02.
      *%--------------------------------------------------------------%*
           MOVE TOTAL-NET-AMT-AP02 OF FAPWM02 TO WK01-NET-AMT-AP02.
      *%--------------------------------------------------------------%*
           MOVE PO-ITEM-TOTAL-AP02 OF FAPWM02 TO WK01-ITEM-TOTAL-AP02.
      *%--------------------------------------------------------------%*
           MOVE PO-ITEM-LEFT-AP02 OF FAPWM02 TO WK01-ITEM-LEFT-AP02.
      *%--------------------------------------------------------------%*
           MOVE TAX-IND-AP02 OF FAPWM02 TO WK01-IND-AP02.
      *%--------------------------------------------------------------%*
           MOVE TOTAL-C-O-AP02 OF FAPWM02 TO WK01-C-O-AP02.
