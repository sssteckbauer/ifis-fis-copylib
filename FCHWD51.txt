      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWD51                              04/24/00  13:03  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWD51.
           02  SAVE-DBKEY-ID-4031-CH51
                                     COMP   OCCURS 9    PIC S9(8).
           02  COUNT-LOCK-CH51              OCCURS 9    PIC 9(3).
           02  SYSTEM-DATE-CH51               COMP-3    PIC S9(8).
           02  NEXT-CHNG-DATE-CH51                      PIC X(6).
           02  START-DATE-4064-CH51                     PIC X(6).
           02  SAVE-DBKEY-ID-4064-CH51        COMP      PIC S9(8).
           02  SAVE-NEW-DBKEY-ID-4064-CH51    COMP      PIC S9(8).
           02  SAVE-CURR-DBKEY-ID-4031-CH51   COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-PLUS-ONE-CH51    COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-MINUS-ONE-CH51   COMP      PIC S9(8).
           02  CNTRB-TOT-WK-PCT-4031-CH51     COMP-3    PIC 9(3)V999.
           02  PAGE-STATUS-CODE-CH51                    PIC X(1).
           02  PAGE-FLAG-CH51                           PIC X(1).
