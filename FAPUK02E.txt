      *----------------------------------------------------------------*
      *    LAYOUT FOR: FAPUK02E - EXTRACT RECORD LAYOUT CREATED BY     *
      *                           PROGRAM FAPUK02                      *
      *    NOTE:                                                       *
      *        ANY MODIFICATIONS/ADDITIONS TO THIS COPY BOOK MUST TAKE *
      *        PROGRAM FAPUK02, FAPCK03.                               *
      *                                                                *
      *                         INTO CONSIDERATION                     *
      *----------------------------------------------------------------*
001NG * 05/05/94 NG  AP096                                             *
001NG * 10/16/95 MLJ AP045 - PAY AUTH                                  *
      * 04/23/96 MLJ AP165 - ACH CHECK TEXT                            *
001MLJ* 11/14/05 MLJ 42664 - ADD FDRL-WTHLD-PCT-C
      *----------------------------------------------------------------*
       01  FAPUK02E-RECORD.
           03 FAPUK02E-SORT-KEY.
              05  FAPUK02E-FIXED-KEY.
                  07  FAPUK02E-UNVRS-CODE             PIC X(002).
                  07  FAPUK02E-PROGRAM-NAME           PIC X(008).
                  07  FAPUK02E-USER-ID                PIC X(008).
                  07  FAPUK02E-ORGN-CODE              PIC X(004).
              05  FAPUK02E-VARIABLE-KEY               PIC X(054).
              05  FAPUK02E-OPTION-1 REDEFINES FAPUK02E-VARIABLE-KEY.
                  07  FAPUK02E-RQSTR-NAME-1           PIC X(035).
                  07  FAPUK02E-RQSTR-INTRL-SEQ-1      PIC 9(008).
                  07  FAPUK02E-RQSTR-ID-1             PIC X(010).
                  07  FAPUK02E-REC-TYPE               PIC X(001).
           03 FAPUK02E-RECORD-BODY.
      *       05  FILLER                              PIC X(002).
              05  FAPUK02E-TEXT-LINE-CTR              PIC X(002).
001MLJ        05  FAPUK02E-VARIABLE-DATA              PIC X(266).
              05  FAPUK02E-REC-TYPE-A REDEFINES FAPUK02E-VARIABLE-DATA
                                                      PIC X(080).
              05  FAPUK02E-STB-NTRY-B REDEFINES FAPUK02E-VARIABLE-DATA.
                  07  FAPUK02E-DCMNT-NMBR-B           PIC X(008).
                  07  FAPUK02E-DCMNT-TYPE-SEQ-NMBR-B  PIC 9(004).
                  07  FAPUK02E-DCMNT-DATE-B           PIC 9(008).
                  07  FAPUK02E-VNDR-INVC-NMBR-B       PIC X(009).
                  07  FAPUK02E-ADJMT-CODE-B           PIC X(002).
                  07  FAPUK02E-GROSS-AMT-B            PIC S9(010)V99.
                  07  FAPUK02E-DSCNT-AMT-B            PIC S9(010)V99.
                  07  FAPUK02E-TAX-AMT-B              PIC S9(010)V99.
                  07  FAPUK02E-ADDL-CHRG-B            PIC S9(010)V99.
                  07  FAPUK02E-NET-AMT-B              PIC S9(010)V99.
                  07  FAPUK02E-SALES-USE-TAX-IND-B    PIC X(001).
001MLJ            07  FILLER                          PIC X(174).
              05  FAPUK02E-CHK-NTRY-C REDEFINES FAPUK02E-VARIABLE-DATA.
                  07  FAPUK02E-TOTAL-DCMNTS-C         PIC 9(004).
                  07  FAPUK02E-CHECK-DATE-C           PIC 9(008).
                  07  FAPUK02E-CHECK-NMBR-C           PIC X(008).
                  07  FAPUK02E-CHECK-AMT-C            PIC S9(010)V99.
                  07  FAPUK02E-RQSTR-ID-C             PIC X(010).
                  07  FAPUK02E-ADR-TYPE-C             PIC X(002).
                  07  FAPUK02E-RQSTR-ADDR1-C          PIC X(035).
                  07  FAPUK02E-RQSTR-ADDR2-C          PIC X(035).
                  07  FAPUK02E-RQSTR-ADDR3-C          PIC X(035).
                  07  FAPUK02E-RQSTR-ADDR4-C          PIC X(035).
                  07  FAPUK02E-RQSTR-ADDR5-C          PIC X(035).
                  07  FAPUK02E-RQSTR-ADDR6-C          PIC X(035).
                  07  FAPUK02E-DBKEY-4157-C           PIC S9(008) COMP.
001NG             07  FAPUK02E-ACH-TYPE-C             PIC X(002).
001MLJ            07  FAPUK02E-FDRL-WTHLD-PCT-C       PIC 9(003)V999.
              05  FAPUK02E-TEXT-D REDEFINES FAPUK02E-VARIABLE-DATA.
                  07  FAPUK02E-DCMNT-NMBR-D           PIC X(008).
                  07  FAPUK02E-TEXT-LINE-1-D          PIC X(055).
                  07  FAPUK02E-TEXT-LINE-2-D          PIC X(055).
                  07  FAPUK02E-TEXT-LINE-3-D          PIC X(055).
                  07  FAPUK02E-TEXT-LINE-4-D          PIC X(055).
                  07  FILLER                          PIC X(030).
                  07  FAPUK02E-ACH-TYPE-D             PIC X(002).
                  07  FILLER                          PIC X(002).


      *-----------------------  END OF FAPUK02E  ----------------------*
