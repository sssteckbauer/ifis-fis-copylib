       975-TO-PROGCMT-050 SECTION.
           MOVE PGC-IDD-SEQ TO
               IDD-SEQ-050 OF PROGCMT-050
                                                                     .
           MOVE PGC-CMT-INFO-1 TO
               CMT-INFO-050 OF PROGCMT-050 (1)
                                                                     .
           MOVE PGC-CMT-INFO-2 TO
               CMT-INFO-050 OF PROGCMT-050 (2)
                                                                     .
           MOVE PGC-CMT-ID TO
               CMT-ID-050 OF PROGCMT-050
                                                                     .
       975-TO-PROGCMT-050-EXIT.
           EXIT.
