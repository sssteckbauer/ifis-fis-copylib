    ***Created by Convert/DC version V8R03 on 11/21/00 at 11:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4070-PU04-Z = 'Y'
               MOVE WK01-CODE-4070-PU04 TO RQST-CODE-4070-PU04 OF
                FPUWK04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-4070-PU04-Z = 'Y'
               MOVE WK01-NAME-4070-PU04 TO RQST-NAME-4070-PU04 OF
                FPUWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4074-PU04-Z = 'Y'
               MOVE WK01-CODE-4074-PU04 TO CMDTY-CODE-4074-PU04 OF
                FPUWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4074-PU04-Z = 'Y'
               MOVE WK01-DESC-4074-PU04 TO CMDTY-DESC-4074-PU04 OF
                FPUWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-NMBR-PU04-Z = 'Y'
               MOVE WK01-SEQ-NMBR-PU04 TO RQST-SEQ-NMBR-PU04 OF FPUWK04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4080-PU041-Z = 'Y'
               MOVE WK01-NMBR-4080-PU041 TO SEQ-NMBR-4080-PU04 OF
                FPUWM04(0001)
           END-IF.
           MOVE WK01-NMBR-4080-PU041-F TO WK01-NMBR-4080-PU04-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4071-PU04-Z = 'Y'
               MOVE WK01-NMBR-4071-PU04 TO ITEM-NMBR-4071-PU04 OF
                FPUWK04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4080-PU041-Z = 'Y'
               MOVE WK01-CODE-4080-PU041 TO COA-CODE-4080-PU04 OF
                FPUWM04(0001)
           END-IF.
           MOVE WK01-CODE-4080-PU041-F TO WK01-CODE-4080-PU04-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-4080-PU041-Z = 'Y'
               MOVE WK01-INDX-CODE-4080-PU041 TO
                ACCT-INDX-CODE-4080-PU04 OF FPUWM04(0001)
           END-IF.
           MOVE WK01-INDX-CODE-4080-PU041-F TO
                WK01-INDX-CODE-4080-PU04-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4080-PU0A1-Z = 'Y'
               MOVE WK01-CODE-4080-PU0A1 TO FUND-CODE-4080-PU04 OF
                FPUWM04(0001)
           END-IF.
           MOVE WK01-CODE-4080-PU0A1-F TO WK01-CODE-4080-PU0A-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4080-PU0B1-Z = 'Y'
               MOVE WK01-CODE-4080-PU0B1 TO ORGZN-CODE-4080-PU04 OF
                FPUWM04(0001)
           END-IF.
           MOVE WK01-CODE-4080-PU0B1-F TO WK01-CODE-4080-PU0B-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4080-PU0C1-Z = 'Y'
               MOVE WK01-CODE-4080-PU0C1 TO ACCT-CODE-4080-PU04 OF
                FPUWM04(0001)
           END-IF.
           MOVE WK01-CODE-4080-PU0C1-F TO WK01-CODE-4080-PU0C-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4080-PU0D1-Z = 'Y'
               MOVE WK01-CODE-4080-PU0D1 TO PRGRM-CODE-4080-PU04 OF
                FPUWM04(0001)
           END-IF.
           MOVE WK01-CODE-4080-PU0D1-F TO WK01-CODE-4080-PU0D-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4080-PU0E1-Z = 'Y'
               MOVE WK01-CODE-4080-PU0E1 TO ACTVY-CODE-4080-PU04 OF
                FPUWM04(0001)
           END-IF.
           MOVE WK01-CODE-4080-PU0E1-F TO WK01-CODE-4080-PU0E-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4080-PU0F1-Z = 'Y'
               MOVE WK01-CODE-4080-PU0F1 TO LCTN-CODE-4080-PU04 OF
                FPUWM04(0001)
           END-IF.
           MOVE WK01-CODE-4080-PU0F1-F TO WK01-CODE-4080-PU0F-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DSTBN-4080-PU041-Z = 'Y'
               MOVE WK01-DSTBN-4080-PU041 TO PCT-DSTBN-4080-PU04 OF
                FPUWM04(0001)
           END-IF.
           MOVE WK01-DSTBN-4080-PU041-F TO WK01-DSTBN-4080-PU04-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4080-PU041-Z = 'Y'
               MOVE WK01-AMT-4080-PU041 TO ACCT-AMT-4080-PU04 OF
                FPUWM04(0001)
           END-IF.
           MOVE WK01-AMT-4080-PU041-F TO WK01-AMT-4080-PU04-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4080-PU042-Z = 'Y'
               MOVE WK01-NMBR-4080-PU042 TO SEQ-NMBR-4080-PU04 OF
                FPUWM04(0002)
           END-IF.
           MOVE WK01-NMBR-4080-PU042-F TO WK01-NMBR-4080-PU04-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4080-PU043-Z = 'Y'
               MOVE WK01-NMBR-4080-PU043 TO SEQ-NMBR-4080-PU04 OF
                FPUWM04(0003)
           END-IF.
           MOVE WK01-NMBR-4080-PU043-F TO WK01-NMBR-4080-PU04-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4080-PU042-Z = 'Y'
               MOVE WK01-CODE-4080-PU042 TO COA-CODE-4080-PU04 OF
                FPUWM04(0002)
           END-IF.
           MOVE WK01-CODE-4080-PU042-F TO WK01-CODE-4080-PU04-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-4080-PU042-Z = 'Y'
               MOVE WK01-INDX-CODE-4080-PU042 TO
                ACCT-INDX-CODE-4080-PU04 OF FPUWM04(0002)
           END-IF.
           MOVE WK01-INDX-CODE-4080-PU042-F TO
                WK01-INDX-CODE-4080-PU04-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4080-PU0A2-Z = 'Y'
               MOVE WK01-CODE-4080-PU0A2 TO FUND-CODE-4080-PU04 OF
                FPUWM04(0002)
           END-IF.
           MOVE WK01-CODE-4080-PU0A2-F TO WK01-CODE-4080-PU0A-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4080-PU0B2-Z = 'Y'
               MOVE WK01-CODE-4080-PU0B2 TO ORGZN-CODE-4080-PU04 OF
                FPUWM04(0002)
           END-IF.
           MOVE WK01-CODE-4080-PU0B2-F TO WK01-CODE-4080-PU0B-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4080-PU0C2-Z = 'Y'
               MOVE WK01-CODE-4080-PU0C2 TO ACCT-CODE-4080-PU04 OF
                FPUWM04(0002)
           END-IF.
           MOVE WK01-CODE-4080-PU0C2-F TO WK01-CODE-4080-PU0C-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4080-PU0D2-Z = 'Y'
               MOVE WK01-CODE-4080-PU0D2 TO PRGRM-CODE-4080-PU04 OF
                FPUWM04(0002)
           END-IF.
           MOVE WK01-CODE-4080-PU0D2-F TO WK01-CODE-4080-PU0D-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4080-PU0E2-Z = 'Y'
               MOVE WK01-CODE-4080-PU0E2 TO ACTVY-CODE-4080-PU04 OF
                FPUWM04(0002)
           END-IF.
           MOVE WK01-CODE-4080-PU0E2-F TO WK01-CODE-4080-PU0E-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4080-PU0F2-Z = 'Y'
               MOVE WK01-CODE-4080-PU0F2 TO LCTN-CODE-4080-PU04 OF
                FPUWM04(0002)
           END-IF.
           MOVE WK01-CODE-4080-PU0F2-F TO WK01-CODE-4080-PU0F-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DSTBN-4080-PU042-Z = 'Y'
               MOVE WK01-DSTBN-4080-PU042 TO PCT-DSTBN-4080-PU04 OF
                FPUWM04(0002)
           END-IF.
           MOVE WK01-DSTBN-4080-PU042-F TO WK01-DSTBN-4080-PU04-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4080-PU042-Z = 'Y'
               MOVE WK01-AMT-4080-PU042 TO ACCT-AMT-4080-PU04 OF
                FPUWM04(0002)
           END-IF.
           MOVE WK01-AMT-4080-PU042-F TO WK01-AMT-4080-PU04-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4080-PU043-Z = 'Y'
               MOVE WK01-CODE-4080-PU043 TO COA-CODE-4080-PU04 OF
                FPUWM04(0003)
           END-IF.
           MOVE WK01-CODE-4080-PU043-F TO WK01-CODE-4080-PU04-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-4080-PU043-Z = 'Y'
               MOVE WK01-INDX-CODE-4080-PU043 TO
                ACCT-INDX-CODE-4080-PU04 OF FPUWM04(0003)
           END-IF.
           MOVE WK01-INDX-CODE-4080-PU043-F TO
                WK01-INDX-CODE-4080-PU04-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4080-PU0A3-Z = 'Y'
               MOVE WK01-CODE-4080-PU0A3 TO FUND-CODE-4080-PU04 OF
                FPUWM04(0003)
           END-IF.
           MOVE WK01-CODE-4080-PU0A3-F TO WK01-CODE-4080-PU0A-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4080-PU0B3-Z = 'Y'
               MOVE WK01-CODE-4080-PU0B3 TO ORGZN-CODE-4080-PU04 OF
                FPUWM04(0003)
           END-IF.
           MOVE WK01-CODE-4080-PU0B3-F TO WK01-CODE-4080-PU0B-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4080-PU0C3-Z = 'Y'
               MOVE WK01-CODE-4080-PU0C3 TO ACCT-CODE-4080-PU04 OF
                FPUWM04(0003)
           END-IF.
           MOVE WK01-CODE-4080-PU0C3-F TO WK01-CODE-4080-PU0C-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4080-PU0D3-Z = 'Y'
               MOVE WK01-CODE-4080-PU0D3 TO PRGRM-CODE-4080-PU04 OF
                FPUWM04(0003)
           END-IF.
           MOVE WK01-CODE-4080-PU0D3-F TO WK01-CODE-4080-PU0D-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4080-PU0E3-Z = 'Y'
               MOVE WK01-CODE-4080-PU0E3 TO ACTVY-CODE-4080-PU04 OF
                FPUWM04(0003)
           END-IF.
           MOVE WK01-CODE-4080-PU0E3-F TO WK01-CODE-4080-PU0E-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4080-PU0F3-Z = 'Y'
               MOVE WK01-CODE-4080-PU0F3 TO LCTN-CODE-4080-PU04 OF
                FPUWM04(0003)
           END-IF.
           MOVE WK01-CODE-4080-PU0F3-F TO WK01-CODE-4080-PU0F-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DSTBN-4080-PU043-Z = 'Y'
               MOVE WK01-DSTBN-4080-PU043 TO PCT-DSTBN-4080-PU04 OF
                FPUWM04(0003)
           END-IF.
           MOVE WK01-DSTBN-4080-PU043-F TO WK01-DSTBN-4080-PU04-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4080-PU043-Z = 'Y'
               MOVE WK01-AMT-4080-PU043 TO ACCT-AMT-4080-PU04 OF
                FPUWM04(0003)
           END-IF.
           MOVE WK01-AMT-4080-PU043-F TO WK01-AMT-4080-PU04-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PU041-Z = 'Y'
               MOVE WK01-CODE-PU041 TO ACTN-CODE-PU04 OF FPUWM04(0001)
           END-IF.
           MOVE WK01-CODE-PU041-F TO WK01-CODE-PU04-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PU042-Z = 'Y'
               MOVE WK01-CODE-PU042 TO ACTN-CODE-PU04 OF FPUWM04(0002)
           END-IF.
           MOVE WK01-CODE-PU042-F TO WK01-CODE-PU04-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PU043-Z = 'Y'
               MOVE WK01-CODE-PU043 TO ACTN-CODE-PU04 OF FPUWM04(0003)
           END-IF.
           MOVE WK01-CODE-PU043-F TO WK01-CODE-PU04-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-TOTAL-PU04-Z = 'Y'
               MOVE WK01-TOTAL-PU04 TO DIST-TOTAL-PU04 OF FPUWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PRICE-4071-PU04-Z = 'Y'
               MOVE WK01-PRICE-4071-PU04 TO EXTENDED-PRICE-4071-PU04 OF
                FPUWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PCT-DSTBN-4080-PU04-Z = 'Y'
               MOVE WK01-PCT-DSTBN-4080-PU04 TO TOT-PCT-DSTBN-4080-PU04
                OF FPUWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-4080-PU041-Z = 'Y'
               MOVE WK01-ERROR-IND-4080-PU041 TO
                ACCT-ERROR-IND-4080-PU04 OF FPUWM04(0001)
           END-IF.
           MOVE WK01-ERROR-IND-4080-PU041-F TO
                WK01-ERROR-IND-4080-PU04-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-4080-PU041-Z = 'Y'
               MOVE WK01-CLASS-CODE-4080-PU041 TO
                RULE-CLASS-CODE-4080-PU04 OF FPUWM04(0001)
           END-IF.
           MOVE WK01-CLASS-CODE-4080-PU041-F TO
                WK01-CLASS-CODE-4080-PU04-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-4080-PU042-Z = 'Y'
               MOVE WK01-CLASS-CODE-4080-PU042 TO
                RULE-CLASS-CODE-4080-PU04 OF FPUWM04(0002)
           END-IF.
           MOVE WK01-CLASS-CODE-4080-PU042-F TO
                WK01-CLASS-CODE-4080-PU04-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-4080-PU042-Z = 'Y'
               MOVE WK01-ERROR-IND-4080-PU042 TO
                ACCT-ERROR-IND-4080-PU04 OF FPUWM04(0002)
           END-IF.
           MOVE WK01-ERROR-IND-4080-PU042-F TO
                WK01-ERROR-IND-4080-PU04-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-4080-PU043-Z = 'Y'
               MOVE WK01-CLASS-CODE-4080-PU043 TO
                RULE-CLASS-CODE-4080-PU04 OF FPUWM04(0003)
           END-IF.
           MOVE WK01-CLASS-CODE-4080-PU043-F TO
                WK01-CLASS-CODE-4080-PU04-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-4080-PU043-Z = 'Y'
               MOVE WK01-ERROR-IND-4080-PU043 TO
                ACCT-ERROR-IND-4080-PU04 OF FPUWM04(0003)
           END-IF.
           MOVE WK01-ERROR-IND-4080-PU043-F TO
                WK01-ERROR-IND-4080-PU04-F (3).
