      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM31                              04/24/00  12:24  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM31.
           02  ACTN-CODE-CH31                           PIC X(1).
           02  CNTRL-PRD-CODE-4017-CH31                 PIC X(1).
           02  CNTRL-SVRTY-CODE-4017-CH31               PIC X(1).
           02  FUND-TITLE-4005-CH31                     PIC X(35).
           02  ORGZN-TITLE-4010-CH31                    PIC X(35).
           02  NEXT-CHNG-DATE-CH31                      PIC X(6).
           02  STATUS-DESC-CH31                         PIC X(8).
           02  END-DATE-4017-CH31                       PIC X(6).
           02  ACTVY-DATE-4016-CH31           COMP-3    PIC S9(8).
           02  TIME-STAMP-4017-CH31                     PIC X(6).
           02  AM-PM-FLAG-CH31                          PIC X(1).
           02  CNTRL-FUND-4017-CH31                     PIC X(6).
           02  CNTRL-ORGZN-4017-CH31                    PIC X(6).
           02  CNTRL-PRD-DESC-CH31                      PIC X(35).
           02  CNTRL-SVRTY-DESC-CH31                    PIC X(35).
           02  CNTRL-FUND-TITLE-4005-CH31               PIC X(35).
           02  CNTRL-ORGZN-TITLE-4010-CH31              PIC X(35).
