       01  R4059-RQST-RPT-J.
           02  DB-PROC-ID-4059.
               03  USER-CODE-4059                       PIC X(8).
               03  LAST-ACTVY-DATE-4059                 PIC X(5).
               03  TRMNL-ID-4059                        PIC X(8).
               03  PURGE-FLAG-4059                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  END-DATE-4059                  COMP-3    PIC S9(8).
           02  NMBR-OF-COPIES-4059                      PIC 9(3).
           02  RPRT-CODE-4059                           PIC X(8).
