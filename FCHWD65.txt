      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWD65                              04/24/00  12:57  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWD65.
           02  WORK-SLCTN-DATE-CH65           COMP-3    PIC S9(8).
           02  WORK-SYSTEM-DATE-CH65          COMP-3    PIC S9(8).
           02  WORK-SAVE-DBKEY-ID-4019-CH65
                                     COMP   OCCURS 11   PIC S9(8).
           02  WK-SET-SORT-KEY-4019-CH65.
               03  WK-UNVRS-CODE-CH65                   PIC X(2).
               03  WK-MGR-PID-CH65.
                   04  WK-MGR-DIGIT-ONE-CH65            PIC X(1).
                   04  WK-MGR-LAST-NINE-CH65            PIC X(9).
           02  WORK-SAVE-DBKEY-ID-4020-CH65
                                     COMP   OCCURS 11   PIC S9(8).
           02  WORK-LOOP-FLAG-CH65                      PIC X(1).
           02  WORK-SAVE-DBKEY-CH65           COMP      PIC S9(8).
           02  ACTN-FLAG-CH65                           PIC X(1).
           02  WORK-ADD-FLAG-CH65                       PIC X(1).
           02  WORK-LIST-FLAG-CH65                      PIC X(1).
           02  WORK-SEL-FLAG-CH65                       PIC X(1).
           02  SEL-FLAG-CH65                            PIC X(1).
