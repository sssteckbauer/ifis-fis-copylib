    ***Created by Convert/DC version V8R03 on 11/01/00 at 08:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH05-Z = 'Y'
               MOVE WK01-CODE-CH05 TO ACTN-CODE-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH05-Z = 'Y'
               MOVE WK01-CODE-4001-CH05 TO COA-CODE-4001-CH05 OF FCHWK05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH05-Z = 'Y'
               MOVE WK01-DATE-4005-CH05 TO START-DATE-4005-CH05 OF
                FCHWK05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH0A-Z = 'Y'
               MOVE WK01-DATE-4005-CH0A TO END-DATE-4005-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-4005-CH05-Z = 'Y'
               MOVE WK01-TYPE-CODE-4005-CH05 TO
                FUND-TYPE-CODE-4005-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4005-CH05-Z = 'Y'
               MOVE WK01-CODE-4005-CH05 TO PREDCSR-CODE-4005-CH05 OF
                FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-CODE-4005-CH05-Z = 'Y'
               MOVE WK01-ACCT-CODE-4005-CH05 TO
                BANK-ACCT-CODE-4005-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-CODE-4062-CH05-Z = 'Y'
               MOVE WK01-ACCT-CODE-4062-CH05 TO
                CASH-ACCT-CODE-4062-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-4005-CH05-Z = 'Y'
               MOVE WK01-ACCT-4005-CH05 TO RVNU-ACCT-4005-CH05 OF
                FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-CODE-4005-CH0A-Z = 'Y'
               MOVE WK01-ACCT-CODE-4005-CH0A TO
                CPTLZN-ACCT-CODE-4005-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-CH05-Z = 'Y'
               MOVE WK01-IND-CH05 TO TEXT-IND-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4005-CH05-Z = 'Y'
               MOVE WK01-IND-4005-CH05 TO CMPLT-IND-4005-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH0A-Z = 'Y'
               MOVE WK01-CODE-CH0A TO NVGT-CODE-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH0A-Z = 'Y'
               MOVE WK01-CODE-4001-CH0A TO FUND-CODE-4001-CH05 OF
                FCHWK05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CHNG-DATE-CH05-Z = 'Y'
               MOVE WK01-CHNG-DATE-CH05 TO NEXT-CHNG-DATE-CH05 OF
                FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-CH05-Z = 'Y'
               MOVE WK01-DESC-CH05 TO STATUS-DESC-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STAMP-4005-CH05-Z = 'Y'
               MOVE WK01-STAMP-4005-CH05 TO TIME-STAMP-4005-CH05 OF
                FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-CH05-Z = 'Y'
               MOVE WK01-PM-FLAG-CH05 TO AM-PM-FLAG-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-DESC-4009-CH05-Z = 'Y'
               MOVE WK01-TYPE-DESC-4009-CH05 TO
                FUND-TYPE-DESC-4009-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4005-CH05-Z = 'Y'
               MOVE WK01-TITLE-4005-CH05 TO PREDCSR-TITLE-4005-CH05 OF
                FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-DESC-4062-CH05-Z = 'Y'
               MOVE WK01-CODE-DESC-4062-CH05 TO
                BANK-CODE-DESC-4062-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FLW-THRGH-IND-4005-CH0-Z = 'Y'
               MOVE WK01-FLW-THRGH-IND-4005-CH0 TO
                FDRL-FLOW-THRGH-IND-4005-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ENTRY-IND-4005-CH05-Z = 'Y'
               MOVE WK01-ENTRY-IND-4005-CH05 TO
                DATA-ENTRY-IND-4005-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-LAST-NINE-4005-CH05-Z = 'Y'
               MOVE WK01-LAST-NINE-4005-CH05 TO MGR-LAST-NINE-4005-CH05
                OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6117-CH05-Z = 'Y'
               MOVE WK01-KEY-6117-CH05 TO NAME-KEY-6117-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-4005-CH0A-Z = 'Y'
               MOVE WK01-ACCT-4005-CH0A TO ACRL-ACCT-4005-CH05 OF
                FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-TITLE-4034-CH05-Z = 'Y'
               MOVE WK01-ACCT-TITLE-4034-CH05 TO
                ACRL-ACCT-TITLE-4034-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUND-CODE-4005-CH05-Z = 'Y'
               MOVE WK01-FUND-CODE-4005-CH05 TO
                CPTLZN-FUND-CODE-4005-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUND-TITLE-4005-CH05-Z = 'Y'
               MOVE WK01-FUND-TITLE-4005-CH05 TO
                CPTLZN-FUND-TITLE-4005-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-BDGT-IND-4005-CH05-Z = 'Y'
               MOVE WK01-BDGT-IND-4005-CH05 TO ROLL-BDGT-IND-4005-CH05
                OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4001-CH05-Z = 'Y'
               MOVE WK01-DATE-4001-CH05 TO ACTVY-DATE-4001-CH05 OF
                FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-CH05-Z = 'Y'
               MOVE WK01-NMBR-CH05 TO SEQ-NMBR-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH0B-Z = 'Y'
               MOVE WK01-CODE-CH0B TO XTRNL-CODE-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-DESC-CH05-Z = 'Y'
               MOVE WK01-CODE-DESC-CH05 TO XTRNL-CODE-DESC-CH05 OF
                FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-LONG-DESC-CH05-Z = 'Y'
               MOVE WK01-LONG-DESC-CH05 TO STNDD-LONG-DESC-CH05 OF
                FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CH05-Z = 'Y'
               MOVE WK01-CH05 TO CONTINUE-CH05 OF FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4005-CH0A-Z = 'Y'
               MOVE WK01-TITLE-4005-CH0A TO FUND-TITLE-4005-CH05 OF
                FCHWM05
           END-IF.
      *%--------------------------------------------------------------%*
FP7768     IF WK01-DFCT-TMPLT-CD-4005-CH05-Z = 'Y'
FP7768         MOVE WK01-DFCT-TMPLT-CD-4005-CH05
FP7768                                   TO DFCT-TMPLT-CD-4005-CH05
FP7768          OF FCHWM05
FP7768     END-IF.
      *%--------------------------------------------------------------%*
R85572     IF WK01-USER-CODE-4005-CH05-Z = 'Y'
R85572         MOVE WK01-USER-CODE-4005-CH05
R85572           TO USER-CODE-4005-CH05 OF FCHWM05
R85572     END-IF.
