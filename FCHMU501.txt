    ***Created by Convert/DC version V8R03 on 12/12/00 at 13:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE NAME-KEY-6311-CH50 OF FCHWM50 TO WK01-KEY-6311-CH50.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4046-CH50 OF FCHWM50 TO WK01-4046-CH50.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH50 OF FCHWM50 TO WK01-CODE-CH50.
      *%--------------------------------------------------------------%*
           MOVE BANK-ID-LAST-NINE-4046-CH50 OF FCHWK50 TO
                WK01-ID-LAST-NINE-4046-CH50.
      *%--------------------------------------------------------------%*
           MOVE CNTCT-NAME-4046-CH50 OF FCHWM50 TO WK01-NAME-4046-CH50.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-AREA-CODE-4046-CH50 OF FCHWM50 TO
                WK01-AREA-CODE-4046-CH50.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XCHNG-ID-4046-CH50 OF FCHWM50 TO
                WK01-XCHNG-ID-4046-CH50.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-SEQ-ID-4046-CH50 OF FCHWM50 TO
                WK01-SEQ-ID-4046-CH50.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XTNSN-ID-4046-CH50 OF FCHWM50 TO
                WK01-XTNSN-ID-4046-CH50.
