      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWD30                              04/24/00  13:02  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWD30.
           02  SAVE-DBKEY-ID-4037-CH30
                                     COMP   OCCURS 4    PIC S9(8).
           02  WORK-RCRD-NMBR-CH30                      PIC 9(4).
           02  PAGE-STATUS-CODE-CH30                    PIC X(1).
           02  SAVE-DBKEY-MINUS-ONE-CH30      COMP      PIC S9(8).
           02  SAVE-DBKEY-PLUS-ONE-CH30       COMP      PIC S9(8).
           02  PAGE-FLAG-CH30                           PIC X(1).
           02  DCMNT-TYPE-CH30                          PIC X(3).
           02  DCMNT-TYPE-SEQ-NMBR-CH30                 PIC 9(4).
