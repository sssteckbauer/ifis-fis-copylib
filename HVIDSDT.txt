       975-TO-R4012-SYSDAT-DTL SECTION.
           MOVE SDT-USER-CD TO
               USER-CODE-4012 OF R4012-SYSDAT-DTL
                                                                     .
           MOVE SDT-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4012 OF R4012-SYSDAT-DTL
                                                                     .
           MOVE SDT-OPTN-1-CD TO
               OPTN-1-CODE-4012 OF R4012-SYSDAT-DTL
                                                                     .
           MOVE SDT-OPTN-2-CD TO
               OPTN-2-CODE-4012 OF R4012-SYSDAT-DTL
                                                                     .
           MOVE SDT-LEVEL-NBR TO
               LEVEL-NMBR-4012 OF R4012-SYSDAT-DTL
                                                                     .
           MOVE SDT-SYSDT-SHORT-DESC TO
               SYSDAT-SHORT-DESC-4012 OF R4012-SYSDAT-DTL
                                                                     .
           MOVE SDT-STD-LONG-DESC TO
               STNDD-LONG-DESC-4012 OF R4012-SYSDAT-DTL
                                                                     .
           MOVE SDT-DATA-DESC TO
               DATA-DESC-4012 OF R4012-SYSDAT-DTL
                                                                     .
       975-TO-R4012-SYSDAT-DTL-EXIT.
           EXIT.
