      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FUTWD13                              04/24/00  13:20  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FUTWD13.
           02  ENTY-ID-KEY-UT13.
               03  UNVRS-CODE-ACH-UT13                  PIC X(2).
               03  ENTY-ID-ACH-UT13.
                   04  ENTY-ID-DIGIT-ONE-ACH-UT13       PIC X(1).
                   04  ENTY-ID-LAST-NINE-ACH-UT13       PIC X(9).
           02  START-DATE-UT13                COMP-3    PIC S9(8).
           02  ACH-ADR-TYPE-HOLD-UT13                   PIC X(2).
