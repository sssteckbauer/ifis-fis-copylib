       976-FROM-R4085-PO-DTL SECTION.

FCCCE**        MOVE USER-CODE-4085 OF R4085-PO-DTL
FCCCE          MOVE DBLINK-USER-CD
               TO POD-USER-CD.

               MOVE LAST-ACTVY-DATE-4085 OF R4085-PO-DTL
               TO POD-LAST-ACTVY-DT.

               MOVE UNVRS-CODE-4085 OF R4085-PO-DTL
               TO POD-UNVRS-CD.

           IF ITEM-NMBR-4085 OF R4085-PO-DTL  NOT NUMERIC
               MOVE ZEROS TO POD-ITEM-NBR
           ELSE
               MOVE ITEM-NMBR-4085 OF R4085-PO-DTL
               TO POD-ITEM-NBR
           END-IF.

               MOVE CMDTY-CODE-4085 OF R4085-PO-DTL
               TO POD-FK-CDY-CMDTY-CD.

               MOVE CMDTY-DESC-4085 OF R4085-PO-DTL
               TO POD-CMDTY-DESC.

               MOVE UNIT-MEA-CODE-4085 OF R4085-PO-DTL
               TO POD-UNIT-MEA-CD.

           IF ACTVY-DATE-4085 OF R4085-PO-DTL  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO POD-ACTVY-DT
           ELSE
               MOVE ACTVY-DATE-4085 OF R4085-PO-DTL
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO POD-ACTVY-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO POD-ACTVY-DT
               END-IF
           END-IF.

               MOVE LIQDTN-IND-4085 OF R4085-PO-DTL
               TO POD-LIQDTN-IND.

           IF QTY-4085 OF R4085-PO-DTL  NOT NUMERIC
               MOVE ZEROS TO POD-QTY
           ELSE
               MOVE QTY-4085 OF R4085-PO-DTL
               TO POD-QTY
           END-IF.

           IF DTL-CNTR-ACCT-4085 OF R4085-PO-DTL  NOT NUMERIC
               MOVE ZEROS TO POD-DTL-CNTR-ACCT
           ELSE
               MOVE DTL-CNTR-ACCT-4085 OF R4085-PO-DTL
               TO POD-DTL-CNTR-ACCT
           END-IF.

               MOVE DTL-ERROR-IND-4085 OF R4085-PO-DTL
               TO POD-DTL-ERROR-IND.

           IF UNIT-PRICE-4085 OF R4085-PO-DTL  NOT NUMERIC
               MOVE ZEROS TO POD-UNIT-PRICE
           ELSE
               MOVE UNIT-PRICE-4085 OF R4085-PO-DTL
               TO POD-UNIT-PRICE
           END-IF.

           IF BLNKT-QTY-RMNG-4085 OF R4085-PO-DTL  NOT NUMERIC
               MOVE ZEROS TO POD-BLNKT-QTY-RMNG
           ELSE
               MOVE BLNKT-QTY-RMNG-4085 OF R4085-PO-DTL
               TO POD-BLNKT-QTY-RMNG
           END-IF.

           IF TOTAL-QTY-RCVD-4085 OF R4085-PO-DTL  NOT NUMERIC
               MOVE ZEROS TO POD-TOTAL-QTY-RCVD
           ELSE
               MOVE TOTAL-QTY-RCVD-4085 OF R4085-PO-DTL
               TO POD-TOTAL-QTY-RCVD
           END-IF.

           IF TOTAL-QTY-RJCTD-4085 OF R4085-PO-DTL  NOT NUMERIC
               MOVE ZEROS TO POD-TOTAL-QTY-RJCTD
           ELSE
               MOVE TOTAL-QTY-RJCTD-4085 OF R4085-PO-DTL
               TO POD-TOTAL-QTY-RJCTD
           END-IF.

               MOVE DLVR-TO-CODE-4085 OF R4085-PO-DTL
               TO POD-DLVR-TO-CD.

               MOVE TAX-IND-4085 OF R4085-PO-DTL
               TO POD-TAX-IND.

               MOVE MODEL-NMBR-4085 OF R4085-PO-DTL
               TO POD-MODEL-NBR.

               MOVE MFR-NAME-4085 OF R4085-PO-DTL
               TO POD-MFR-NAME.

           IF TAX-AMT-4085 OF R4085-PO-DTL  NOT NUMERIC
               MOVE ZEROS TO POD-TAX-AMT
           ELSE
               MOVE TAX-AMT-4085 OF R4085-PO-DTL
               TO POD-TAX-AMT
           END-IF.

               MOVE PART-NMBR-4085 OF R4085-PO-DTL
               TO POD-PART-NBR.

               MOVE LIEN-CLOSED-CODE-4085 OF R4085-PO-DTL
               TO POD-LIEN-CLOSED-CD.

           IF ITEM-DSCNT-AMT-4085 OF R4085-PO-DTL  NOT NUMERIC
               MOVE ZEROS TO POD-ITEM-DSCNT-AMT
           ELSE
               MOVE ITEM-DSCNT-AMT-4085 OF R4085-PO-DTL
               TO POD-ITEM-DSCNT-AMT
           END-IF.

               MOVE DSCNT-BEFORE-TAX-IND-4085 OF R4085-PO-DTL
               TO POD-DSCNT-BFR-TX-IND.

           IF PO-DSCNT-AMT-4085 OF R4085-PO-DTL  NOT NUMERIC
               MOVE ZEROS TO POD-PO-DSCNT-AMT
           ELSE
               MOVE PO-DSCNT-AMT-4085 OF R4085-PO-DTL
               TO POD-PO-DSCNT-AMT
           END-IF.

               MOVE CNSLDTN-IND-4085 OF R4085-PO-DTL
               TO POD-CNSLDTN-IND.

               MOVE PRICE-NEG-SIGN-4085 OF R4085-PO-DTL
               TO POD-PRICE-NEG-SIGN.

               MOVE GOVNMT-OWNED-IND-4085 OF R4085-PO-DTL
               TO POD-GOV-OWND-IND.
       976-FROM-R4085-PO-DTL-EXIT.
           EXIT.
