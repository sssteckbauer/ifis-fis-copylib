    ***Created by Convert/DC version V8R03 on 12/04/00 at 09:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PU11-Z = 'Y'
               MOVE WK01-CODE-PU11 TO ACTN-CODE-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4083-PU11-Z = 'Y'
               MOVE WK01-NMBR-4083-PU11 TO PO-NMBR-4083-PU11 OF FPUWK11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-NMBR-4096-PU11-Z = 'Y'
               MOVE WK01-SEQ-NMBR-4096-PU11 TO CHNG-SEQ-NMBR-4096-PU11
                OF FPUWK11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4096-PU11-Z = 'Y'
               MOVE WK01-DATE-4096-PU11 TO ORDER-DATE-4096-PU11 OF
                FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4096-PU11-Z = 'Y'
               MOVE WK01-AMT-4096-PU11 TO TOTAL-AMT-4096-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4072-PU11-Z = 'Y'
               MOVE WK01-CODE-4072-PU11 TO BUYER-CODE-4072-PU11 OF
                FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-4072-PU11-Z = 'Y'
               MOVE WK01-NAME-4072-PU11 TO BUYER-NAME-4072-PU11 OF
                FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AREA-CODE-4072-PU11-Z = 'Y'
               MOVE WK01-AREA-CODE-4072-PU11 TO
                TLPHN-AREA-CODE-4072-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-XCHNG-ID-4072-PU11-Z = 'Y'
               MOVE WK01-XCHNG-ID-4072-PU11 TO TLPHN-XCHNG-ID-4072-PU11
                OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-ID-4072-PU11-Z = 'Y'
               MOVE WK01-SEQ-ID-4072-PU11 TO TLPHN-SEQ-ID-4072-PU11 OF
                FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-XTNSN-ID-4072-PU11-Z = 'Y'
               MOVE WK01-XTNSN-ID-4072-PU11 TO TLPHN-XTNSN-ID-4072-PU11
                OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4002-PU11-Z = 'Y'
               MOVE WK01-CODE-4002-PU11 TO COA-CODE-4002-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4002-PU1A-Z = 'Y'
               MOVE WK01-CODE-4002-PU1A TO ORGZN-CODE-4002-PU11 OF
                FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4010-PU11-Z = 'Y'
               MOVE WK01-TITLE-4010-PU11 TO ORGZN-TITLE-4010-PU11 OF
                FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NIN-4073-P11-Z = 'Y'
               MOVE WK01-ID-LAST-NIN-4073-P11 TO
                VNDR-ID-LAST-NINE-4073-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6311-PU11-Z = 'Y'
               MOVE WK01-KEY-6311-PU11 TO NAME-KEY-6311-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
DEVMJM*
           IF WK01-MP-LIT-PU11-Z = 'Y'
               MOVE WK01-MP-LIT-PU11 TO MP-LIT-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
DEVMJM*
           IF WK01-TYPE-CODE-4083-PU11-Z = 'Y'
               MOVE WK01-TYPE-CODE-4083-PU11 TO ADR-TYPE-CODE-4083-PU11
                OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PU11-Z = 'Y'
               MOVE WK01-DESC-6137-PU11 TO LONG-DESC-6137-PU11 OF
                FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6139-PU11-Z = 'Y'
               MOVE WK01-NAME-6139-PU11 TO CITY-NAME-6139-PU11 OF
                FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6151-PU11-Z = 'Y'
               MOVE WK01-CODE-6151-PU11 TO STATE-CODE-6151-PU11 OF
                FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6152-PU11-Z = 'Y'
               MOVE WK01-CODE-6152-PU11 TO ZIP-CODE-6152-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-4073-PU11-Z = 'Y'
               MOVE WK01-NAME-4073-PU11 TO CNTCT-NAME-4073-PU11 OF
                FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AREA-CODE-4073-PU11-Z = 'Y'
               MOVE WK01-AREA-CODE-4073-PU11 TO
                TLPHN-AREA-CODE-4073-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-XCHNG-ID-4073-PU11-Z = 'Y'
               MOVE WK01-XCHNG-ID-4073-PU11 TO TLPHN-XCHNG-ID-4073-PU11
                OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-ID-4073-PU11-Z = 'Y'
               MOVE WK01-SEQ-ID-4073-PU11 TO TLPHN-SEQ-ID-4073-PU11 OF
                FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CMPLT-IND-4096-PU11-Z = 'Y'
               MOVE WK01-CMPLT-IND-4096-PU11 TO PO-CMPLT-IND-4096-PU11
                OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4096-PU11-Z = 'Y'
               MOVE WK01-IND-4096-PU11 TO APRVL-IND-4096-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-PU11-Z = 'Y'
               MOVE WK01-FLAG-PU11 TO PRINT-FLAG-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4096-PU1A-Z = 'Y'
               MOVE WK01-DATE-4096-PU1A TO PRINT-DATE-4096-PU11 OF
                FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-4096-PU11-Z = 'Y'
               MOVE WK01-ERROR-IND-4096-PU11 TO HDR-ERROR-IND-4096-PU11
                OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CNTR-DTL-4096-PU11-Z = 'Y'
               MOVE WK01-CNTR-DTL-4096-PU11 TO HDR-CNTR-DTL-4096-PU11
                OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CNTR-ACCT-4096-PU11-Z = 'Y'
               MOVE WK01-CNTR-ACCT-4096-PU11 TO HDR-CNTR-ACCT-4096-PU11
                OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PU1A-Z = 'Y'
               MOVE WK01-CODE-PU1A TO PRNTR-CODE-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4096-PU1A-Z = 'Y'
               MOVE WK01-IND-4096-PU1A TO CNCL-IND-4096-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4096-PU1B-Z = 'Y'
               MOVE WK01-DATE-4096-PU1B TO CNCL-DATE-4096-PU11 OF
                FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ADR-6139-PU111-Z = 'Y'
               MOVE WK01-ADR-6139-PU111 TO LINE-ADR-6139-PU11 OF
                FPUWM11(0001)
           END-IF.
           MOVE WK01-ADR-6139-PU111-F TO WK01-ADR-6139-PU11-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-6139-PU112-Z = 'Y'
               MOVE WK01-ADR-6139-PU112 TO LINE-ADR-6139-PU11 OF
                FPUWM11(0002)
           END-IF.
           MOVE WK01-ADR-6139-PU112-F TO WK01-ADR-6139-PU11-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-6139-PU113-Z = 'Y'
               MOVE WK01-ADR-6139-PU113 TO LINE-ADR-6139-PU11 OF
                FPUWM11(0003)
           END-IF.
           MOVE WK01-ADR-6139-PU113-F TO WK01-ADR-6139-PU11-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-6139-PU114-Z = 'Y'
               MOVE WK01-ADR-6139-PU114 TO LINE-ADR-6139-PU11 OF
                FPUWM11(0004)
           END-IF.
           MOVE WK01-ADR-6139-PU114-F TO WK01-ADR-6139-PU11-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6153-PU11-Z = 'Y'
               MOVE WK01-CODE-6153-PU11 TO CNTRY-CODE-6153-PU11 OF
                FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TERM-DATE-4083-PU11-Z = 'Y'
               MOVE WK01-TERM-DATE-4083-PU11 TO
                BLNKT-TERM-DATE-4083-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4096-PU1C-Z = 'Y'
               MOVE WK01-DATE-4096-PU1C TO ACTVY-DATE-4096-PU11 OF
                FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-4083-PU11-Z = 'Y'
               MOVE WK01-CLASS-CODE-4083-PU11 TO
                PO-CLASS-CODE-4083-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ORDER-IND-4083-PU11-Z = 'Y'
               MOVE WK01-ORDER-IND-4083-PU11 TO
                BLNKT-ORDER-IND-4083-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ORDER-CODE-4083-PU11-Z = 'Y'
               MOVE WK01-ORDER-CODE-4083-PU11 TO
                BLNKT-ORDER-CODE-4083-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-C-O-PU11-Z = 'Y'
               MOVE WK01-C-O-PU11 TO TOTAL-C-O-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-PU11-Z = 'Y'
               MOVE WK01-AMT-PU11 TO TAX-AMT-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-PU1A-Z = 'Y'
               MOVE WK01-AMT-PU1A TO ADDL-AMT-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-PU1B-Z = 'Y'
               MOVE WK01-AMT-PU1B TO NET-AMT-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-LIT-PU11-Z = 'Y'
               MOVE WK01-LIT-PU11 TO PO-LIT-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CO-LIT-PU11-Z = 'Y'
               MOVE WK01-CO-LIT-PU11 TO PO-CO-LIT-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CO-LIT-PU1A-Z = 'Y'
               MOVE WK01-CO-LIT-PU1A TO PO-CO-LIT-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-PU1A-Z = 'Y'
               MOVE WK01-FLAG-PU1A TO PYMT-FLAG-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-PU1B-Z = 'Y'
               MOVE WK01-FLAG-PU1B TO ARCHIVE-FLAG-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4083-PU11-Z = 'Y'
               MOVE WK01-DATE-4083-PU11 TO EXPRT-DATE-4083-PU11 OF
                FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TMPLT-CODE-PU11-Z = 'Y'
               MOVE WK01-TMPLT-CODE-PU11 TO APRVL-TMPLT-CODE-PU11 OF
                FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TOTAL-PU11-Z = 'Y'
               MOVE WK01-TOTAL-PU11 TO ITEM-TOTAL-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DELETE-PU11-Z = 'Y'
               MOVE WK01-DELETE-PU11 TO CONFIRM-DELETE-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-LIT-PU11-Z = 'Y'
               MOVE WK01-DEL-LIT-PU11 TO DELETE-LIT-PU11 OF FPUWM11
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-LITERAL-PU11-Z = 'Y'
               MOVE WK01-LITERAL-PU11 TO TEXT-LITERAL-PU11 OF FPUWM11
           END-IF.
