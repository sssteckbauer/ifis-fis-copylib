       01  R6439-ENTY-DTCD.
           02  DB-PROC-ID-6439.
               03  USER-CODE-6439                       PIC X(8).
               03  LAST-ACTVY-DATE-6439                 PIC X(5).
               03  TRMNL-ID-6439                        PIC X(8).
               03  PURGE-FLAG-6439                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  ENTY-DATA-CODE-KEY-6439.
               03  UNVRS-CODE-6439                      PIC X(2).
               03  ENTY-DATA-CODE-6439                  PIC X(6).
           02  SHORT-DESC-6439                          PIC X(10).
           02  LONG-DESC-6439                           PIC X(30).
           02  RQRD-ENTY-DATA-VALUE-FLAG-6439           PIC X(1).
