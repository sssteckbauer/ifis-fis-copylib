      ******************************************************************
      *    **** COPYBOOK WGA002 - USED FOR RULEEDIT WEB SERVICE ****   *
      *    PARAMETER AREA USED BY DUALCOB (BATCH OR CICS) SUBROUTINE   *
      *    WGA002 TO VALIDATE IFOAPAL AGAINST THE RULE EDIT PROCESS.   *
      ******************************************************************

      ******************************************************************
      *    AFTER CHANGES ARE MADE TO THIS COPYBOOK, IT MUST BE COPIED  *
      *    INTO A WEBSPHERE DEVELOPER PROJECT FOLDER, FROM WHICH THE   *
      *    WEB SERVICE INTERFACE PROGRAM, WSDL, & WSBIND ARE GENERATED.*
      *                                                                *
      *    NOTE: BEFORE DOING THE GENERATE, RENAME THIS MEMBER IN THE  *
      *    PROJECT FOLDER AS WGA002X.CPY.  THE 'X' SUFFIX ASSOCIATES   *
      *    IT TO THE WGA002X WEB SERVICE STUB PROGRAM WHICH ACTUALLY   *
      *    CALLS THE WGA002 DUALCOB PROGRAM.                           *
      ******************************************************************

       01  RULEEDIT-PARM-AREA.

           05  RULEEDIT-CHANNEL-HEADER.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND AND OUTBOUND  *
      *      ***********************************************************

               10  RULEEDIT-CHANNEL-ID        PIC X(08).
      *            *****************************************************
      *            * MUST BE 'RULEEDIT'                                *
      *            *****************************************************

               10  RULEEDIT-VERS-RESP-CD      PIC X(08).
      *            *****************************************************
      *            * INCOMING: COMMAREA VERSION = 01.00.00             *
      *            * OUTGOING: RETURN RESPONSE FOR WFGA                *
      *            *                          (WEB/IFIS GA SUBSYSTEM)  *
      *            *****************************************************
                   88  RULEEDIT-RESPONSE-SUCCESSFUL    VALUE 'WFGA000I'.
                   88  RULEEDIT-RESPONSE-ERROR         VALUE 'WFGA999E'.

           05  RULEEDIT-INPUT-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND ONLY          *
      *      ***********************************************************

               10  RULEEDIT-SOURCE-SYSTEM      PIC X(03).
      *            *****************************************************
      *            * WEB = CALLED FROM THE WEB                         *
      *            * FIS = CALLED FROM THE MAINFRAME IFIS SYSTEM       *
      *            * FIG = CALLED FROM THE MAINFRAME IFIS GRANTS SYSTEM*
      *            * SIS = CALLED FROM THE MAINFRAME ISIS SYSTEM       *
      *            *****************************************************

               10  RULEEDIT-COA-CD             PIC X(01).
               10  RULEEDIT-JRNL-TYP           PIC X(04).

               10  RULEEDIT-TRANS-DT           PIC X(10).
      *            *****************************************************
      *            * YYYY-MM-DD OR MM/DD/YYYY; DEFAULT = CURRENT DATE  *
      *            *****************************************************

               10  RULEEDIT-DOC-NBR            PIC X(08).
               10  RULEEDIT-DOC-TYP-SEQ-NBR    PIC 9(04).
               10  RULEEDIT-DOC-ITEM-NBR       PIC 9(04).
               10  RULEEDIT-DOC-SEQ-NBR        PIC 9(04).

               10  RULEEDIT-ENCMBR-NBR         PIC X(08).
               10  RULEEDIT-ENC-DOC-TYP-SEQ-NBR
                                               PIC 9(04).
               10  RULEEDIT-ENCMBR-ITEM-NBR    PIC 9(04).
               10  RULEEDIT-ENCMBR-SEQ-NBR     PIC 9(04).

               10  RULEEDIT-SBMSN-NBR          PIC 9(04).
               10  RULEEDIT-DEBT-CRDT-IND      PIC X(01).
               10  RULEEDIT-TRANS-AMT          PIC +9(10).99.
               10  RULEEDIT-TRANS-DESC         PIC X(35).
               10  RULEEDIT-PRJCT-CD           PIC X(08).

               10  RULEEDIT-ACCEPT-INACTIVE-INDEX
                                               PIC X(01).
      *            *****************************************************
      *            *  Y = ACCEPT ACCT-INDX-CD INACTIVE (BATCH APP'S)   *
      * DEFAULT==> * ELSE REJECT ACCT-INDX-CD INACTIVE (STATUS = I)    *
      *            *   (FGAIU03 DATANAME = INDX-ERR-OVRDE-IND-GA03)    *
      *            *****************************************************

               10  RULEEDIT-ACCEPT-INACTIVE-ACCT
                                               PIC X(01).
      *            *****************************************************
      *            *  Y = ACCEPT ACCT-CD INACTIVE (BATCH APP'S)        *
      * DEFAULT==> * ELSE REJECT ACCT-CD INACTIVE (STATUS = I)         *
      *            *   (FGAIU03 DATANAME = ENPET-DOCUMENT-FLAG-GA03)   *
      *            *****************************************************

               10  RULEEDIT-FILLER-1           PIC X(60).

           05  RULEEDIT-INPUT-OUTPUT-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND AND OUTBOUND  *
      *      ***********************************************************
      *      *     THE FOAPAL FIELDS WHICH ARE BLANK AS INPUT MAY      *
      *      *     HAVE VALUES PASSED BACK TO CALLING PROGRAM.         *
      *      ***********************************************************
               10  RULEEDIT-ACCT-INDX-CD       PIC X(10).
               10  RULEEDIT-FUND-CD            PIC X(06).
               10  RULEEDIT-ORGN-CD            PIC X(06).
               10  RULEEDIT-ACCT-CD            PIC X(06).
               10  RULEEDIT-PRGRM-CD           PIC X(06).
               10  RULEEDIT-ACTVY-CD           PIC X(06).
               10  RULEEDIT-LCTN-CD            PIC X(06).
               10  RULEEDIT-BANK-ACCT-CD       PIC X(02).
               10  RULEEDIT-FILLER-2           PIC X(20).

           05  RULEEDIT-OUTPUT-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: OUTBOUND ONLY         *
      *      ***********************************************************
      *      *     PARAMETERS NOT USED AS INPUT BUT MAY HAVE VALUES    *
      *      *     PASSED BACK TO CALLING PROGRAM.                     *
      *      ***********************************************************
               10  RULEEDIT-FSCL-CC-YR.
                   15  RULEEDIT-FSCL-CC        PIC X(02).
                   15  RULEEDIT-FSCL-YR        PIC X(02).
               10  RULEEDIT-ACTG-PRD           PIC X(02).
               10  RULEEDIT-BDGT-PRD           PIC X(02).
               10  RULEEDIT-FUND-ACCT-ERR-TYP  PIC X(01).
               10  RULEEDIT-EARLY-INACT-ERR-TYP
                                               PIC X(01).
               10  RULEEDIT-FILLER-3           PIC X(20).

           05  RULEEDIT-NOT-USED-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: NONE                  *
      *      ***********************************************************
      *      *     RULEEDIT INPUT-ONLY PARAMETERS NOT USED AS OF 4/2007*
      *      ***********************************************************
               10  RULEEDIT-BDGT-ID            PIC X(06).
               10  RULEEDIT-DPST-NBR           PIC X(08).

DEVHV1*      ====> ACTIVATED IN 2008 FOR JOURNAL VOUCHER:
               10  RULEEDIT-ENCMBR-ACTN-IND    PIC X(01).

               10  RULEEDIT-AUTO-JRNL-ID       PIC X(03).
               10  RULEEDIT-RVRSL-IND          PIC X(01).
               10  RULEEDIT-DSTBN-PCT          PIC 9(03).999.
               10  RULEEDIT-BDGT-DSPSN         PIC X(01).
               10  RULEEDIT-CMTMNT-TYP         PIC X(01).
               10  RULEEDIT-CMTMNT-PCT         PIC 9(03).999.

DEVHV1*      ====> ACTIVATED IN 2008 FOR JOURNAL VOUCHER:
               10  RULEEDIT-ENCMBR-TYP         PIC X(01).

               10  RULEEDIT-VNDR-ID.
                   15 RULEEDIT-VNDR-ID-DGT-ONE PIC X(01).
                   15 RULEEDIT-VNDR-ID-LST-NINE
                                               PIC X(09).
DEVHV1*      ====> ACTIVATED IN 2008 FOR JOURNAL VOUCHER:
               10  RULEEDIT-ACRL-IND           PIC X(01).

               10  RULEEDIT-ONE-TIME-IND       PIC X(01).

DEVHV1*      ====> ACTIVATED IN 2008 FOR JOURNAL VOUCHER:
               10  RULEEDIT-DOC-REF-NBR        PIC X(10).

               10  RULEEDIT-FILLER-4           PIC X(20).

           05  RULEEDIT-RETURN-STATUS-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: OUTBOUND ONLY         *
      *      ***********************************************************

               10  RULEEDIT-RETURN-STATUS-CODE PIC X(01).
      *            *****************************************************
      *            * S = SUCCESSFUL, E = DATA ERROR(S), F = FATAL ERROR*
      *            *****************************************************

               10  RULEEDIT-RETURN-MSG-COUNT   PIC 9(01).
      *            *****************************************************
      *            * NUMBER OF RETURN-MSG-AREA(S) POPULATED (1 TO 4)   *
      *            *****************************************************

               10  RULEEDIT-RETURN-MSG-AREA    OCCURS 4 TIMES.
      *            *****************************************************
      *            * FOR STATUS 'S', SUCCESS MESSAGE NUMBER & TEXT     *
      *            * FOR STATUS 'E', ERROR MSG NUMBER, FIELD & TEXT    *
      *            * FOR STATUS 'F', FATAL SQLCODE, CURR-IO-NUM WITH   *
      *            *                 PARAGRAPH NAME, & ASSOC DB2 TABLE *
      *            *****************************************************
                   15  RULEEDIT-RETURN-MSG-NUM PIC X(06).
                   15  RULEEDIT-RETURN-MSG-FIELD
                                               PIC X(30).
                   15  RULEEDIT-RETURN-MSG-TEXT
                                               PIC X(40).

           05  RULEEDIT-ACCOMM-FILLER          PIC X(270).
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: NONE                  *
      *      ***********************************************************
      *      *     FILLER NEEDED TO MATCH THE IFIS DFHCOMMAREA SIZE.   *
      *      *     TOTAL LENGTH OF COPYBOOK MUST ALWAYS BE 950 BYTES.  *
      *      ***********************************************************

