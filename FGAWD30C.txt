      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FGAWD30C                             04/24/00  13:08  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FGAWD30C.
           02  FUND-CODE-BAL-GA30                       PIC X(6).
           02  FUND-JRNL-TYPE-GA30                      PIC X(4).
           02  FUND-BAL-AMT-DR-GA30           COMP-3    PIC S9(10)V99.
           02  FUND-BAL-AMT-CR-GA30           COMP-3    PIC S9(10)V99.
           02  BUDG-CODE-BAL-GA30                       PIC X(6).
           02  BUDG-JRNL-TYPE-GA30                      PIC X(4).
           02  BUDG-BAL-AMT-DR-GA30           COMP-3    PIC S9(10)V99.
           02  BUDG-BAL-AMT-CR-GA30           COMP-3    PIC S9(10)V99.
           02  PERM-CODE-BAL-GA30                       PIC X(6).
           02  PERM-JRNL-TYPE-GA30                      PIC X(4).
           02  PERM-BAL-AMT-DR-GA30           COMP-3    PIC S9(10)V99.
           02  PERM-BAL-AMT-CR-GA30           COMP-3    PIC S9(10)V99.
           02  CL-CL-CODE-GA30                          PIC X(1).
           02  CL-TYPE-CODE-GA30                        PIC X(2).
           02  CL-BAL-AMT-DR-GA30             COMP-3    PIC S9(10)V99.
           02  CL-BAL-AMT-CR-GA30             COMP-3    PIC S9(10)V99.
