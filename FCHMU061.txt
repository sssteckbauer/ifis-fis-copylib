    ***Created by Convert/DC version V8R03 on 11/20/00 at 09:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE DCSD-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY01.
      *%--------------------------------------------------------------%*
           MOVE CNFDL-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0A.
      *%--------------------------------------------------------------%*
           MOVE HOLDS-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0B.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH06 OF FCHWM06 TO WK01-CODE-CH06.
      *%--------------------------------------------------------------%*
           MOVE MGR-LAST-NINE-4019-CH06 OF FCHWK06 TO
                WK01-LAST-NINE-4019-CH06.
      *%--------------------------------------------------------------%*
           MOVE MGR-TITLE-4019-CH06 OF FCHWM06 TO WK01-TITLE-4019-CH06.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4019-CH06 OF FCHWM06 TO WK01-CODE-4019-CH06.
      *%--------------------------------------------------------------%*
           MOVE DFLT-LCTN-CODE-4019-CH06 OF FCHWM06 TO
                WK01-LCTN-CODE-4019-CH06.
      *%--------------------------------------------------------------%*
           MOVE DFLT-ORGZN-CODE-4019-CH06 OF FCHWM06 TO
                WK01-ORGZN-CODE-4019-CH06.
      *%--------------------------------------------------------------%*
           MOVE ORGZN-TITLE-4019-CH06 OF FCHWM06 TO
                WK01-TITLE-4019-CH0A.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4019-CH06 OF FCHWM06 TO WK01-DATE-4019-CH06.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4019-CH06 OF FCHWM06 TO WK01-DATE-4019-CH0A.
      *%--------------------------------------------------------------%*
           MOVE LAST-ACTVY-DATE-4019-CH06 OF FCHWM06 TO
                WK01-ACTVY-DATE-4019-CH06.
      *%--------------------------------------------------------------%*
           MOVE NAME-KEY-6117-CH06 OF FCHWM06 TO WK01-KEY-6117-CH06.
      *%--------------------------------------------------------------%*
           MOVE LCTN-TITLE-4019-CH06 OF FCHWM06 TO WK01-TITLE-4019-CH0B.
