       01  R4216-APRVL-TRAN.
           02  DB-PROC-ID-4216.
               03  USER-CODE-4216                       PIC X(8).
               03  LAST-ACTVY-DATE-4216                 PIC X(5).
               03  SYSTEM-TIME-STAMP-4216               PIC X(8).
               03  TRMNL-ID-4216                        PIC X(8).
               03  PURGE-FLAG-4216                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  APRVL-TRAN-KEY-4216.
               03  USER-ID-4216                         PIC X(8).
               03  DCMNT-TYPE-4216                      PIC X(3).
               03  APRVL-TMPLT-CODE-4216                PIC X(3).
               03  LEVEL-SEQ-NMBR-4216                  PIC 9(4).
               03  APRVL-ID-4216                        PIC X(8).
           02  APRVL-TYPE-4216                          PIC X(1).
           02  FILLER02                                 PIC X(15).
