    ***Created by Convert/DC version V8R03 on 12/04/00 at 10:00***

      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE RESPONSE-DESC-UT77 OF FUTWM77 TO WK01-DESC-UT77.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RESPONSE-UT77 OF FUTWM77 TO WK01-RESPONSE-UT77.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-UT77 OF FUTWM77(0001) TO WK01-CODE-UT771.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-UT77 OF FUTWM77(0002) TO WK01-CODE-UT772.
      *%--------------------------------------------------------------%*
           MOVE RSPNS-ID-UT77 OF FUTWM77(0001) TO WK01-ID-UT771.
      *%--------------------------------------------------------------%*
           MOVE RSPNS-ID-UT77 OF FUTWM77(0002) TO WK01-ID-UT772.
      *%--------------------------------------------------------------%*
           MOVE LONG-DESC-UT77 OF FUTWM77(0001) TO WK01-DESC-UT8A11.
      *%--------------------------------------------------------------%*
           MOVE LONG-DESC-UT77 OF FUTWM77(0002) TO WK01-DESC-UT8A12.
      *%--------------------------------------------------------------%*
           MOVE TEXT-IND-UT77 OF FUTWM77(0001) TO WK01-IND-UT771.
      *%--------------------------------------------------------------%*
           MOVE TEXT-IND-UT77 OF FUTWM77(0002) TO WK01-IND-UT772.
