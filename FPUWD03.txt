      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWD03                              04/24/00  13:13  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWD03.
           02  SAVE-DBKEY-ID-4071-PU03                  PIC X(11).
           02  SAVE-DBKEY-ID-4070-PU03                  PIC X(8).
           02  SAVE-DBKEY-ID-MINUS-ONE-PU03   COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-PLUS-ONE-PU03    COMP      PIC S9(8).
           02  PAGE-STATUS-CODE-PU03                    PIC X(1).
           02  SET-SORT-KEY-6311-PU03.
               03  UNVRS-CODE-6311-PU03                 PIC X(2).
               03  INTRL-REF-ID-6311-PU03     COMP-3    PIC S9(7).
           02  WORK-EXTENDED-PRICE-PU03       COMP-3    PIC 9(10)V99.
           02  WORK-PRICE-PU03                COMP-3    PIC S9(16)V99.
           02  PREV-EXTENDED-PRICE-PU03       COMP-3    PIC 9(10)V99.
           02  SYSTEM-DATE-PU03               COMP-3    PIC S9(8).
           02  VALID-CMDTY-FLAG-PU03                    PIC X(1).
           02  CMDTY-ERR-FLAG-PU03                      PIC X(1).
           02  ERR-FLAG-PU03                            PIC X(1).
           02  ACTN-ERR-FLAG-PU03                       PIC X(1).
           02  SUSP-FLAG-PU03                           PIC X(1).
           02  ADD-CMDTY-FLAG-PU03                      PIC X(1).
           02  VALID-MEA-CODE-PU03                      PIC X(1).
           02  VALID-VNDR-PU03                          PIC X(1).
           02  VALID-VNDR-AGRMT-PU03                    PIC X(1).
           02  VALID-VNDR-CMD-PU03                      PIC X(1).
           02  CMDTY-DESC-PU03                          PIC X(35).
           02  UNIT-MEA-DESC-PU03                       PIC X(35).
           02  INTRL-REF-ID-PU03              COMP-3    PIC S9(7).
           02  QTY-PU03                       COMP-3    PIC 9(6)V99.
           02  ITEM-NMBR-PU03                 COMP-3    PIC 9(4).
           02  NAME-KEY-PU03                            PIC X(35).
           02  SET-SORT-KEY-4012-PU03.
               03  OPTN-1-CODE-PU03                     PIC X(8).
               03  OPTN-2-CODE-PU03                     PIC X(8).
               03  LEVEL-NMBR-PU03                      PIC 9(2).
           02  ENTY-PRSN-IND-PU03                       PIC X(1).
           02  EFCTV-KEY-PU03.
               03  EFCTV-DATE-PU03            COMP-3    PIC S9(8).
               03  TIME-STAMP-PU03                      PIC X(6).
           02  APRVL-IND-PU03                           PIC X(1).
           02  APRVL-TMPLT-CODE-PU03                    PIC X(3).
