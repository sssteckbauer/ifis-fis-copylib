       01  R4014-ACTT.
           02  DB-PROC-ID-4014.
               03  USER-CODE-4014                       PIC X(8).
               03  LAST-ACTVY-DATE-4014                 PIC X(5).
               03  TRMNL-ID-4014                        PIC X(8).
               03  PURGE-FLAG-4014                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  ACCT-TYPE-KEY-4014.
               03  UNVRS-CODE-4014                      PIC X(2).
               03  COA-CODE-4014                        PIC X(1).
               03  ACCT-TYPE-CODE-4014                  PIC X(2).
           02  ACTVY-DATE-4014                COMP-3    PIC S9(8).
