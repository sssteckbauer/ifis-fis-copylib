      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPEWD36                              04/24/00  13:11  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPEWD36.
           02  SAVE-DBKEY-ID-6157-PE36        COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-6117-PE36        COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-2158-PE36        COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-2351-PE36        COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-2159-PE36
                                     COMP   OCCURS 4    PIC S9(8).
           02  SAVE-DBKEY-ID-2160-PE36
                                     COMP   OCCURS 4    PIC S9(8).
           02  DUP-SPCL-NEED-PE36                       PIC X(1).
           02  SAVE-DBKEY-ID-2368-PE36        COMP      PIC S9(8).
           02  PAN-FLAG-6117-PE36                       PIC X(1).
           02  SEED-NMBR-PE36                           PIC 9(9).
           02  RNDM-NMBR-PE36 REDEFINES
               SEED-NMBR-PE36                           PIC V9(9).
           02  PAN-NMBR-PE36 REDEFINES
               SEED-NMBR-PE36                           PIC X(9).
           02  INTRL-REF-ID-PE36                        PIC 9(7).
           02  DUPE-DBKEY-ID-6157-PE36        COMP      PIC S9(8).
