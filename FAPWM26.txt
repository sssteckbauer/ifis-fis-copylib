      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWM26                              04/24/00  12:16  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWM26.
           02  ACTN-CODE-AP26                           PIC X(1).
           02  SEQ-NMBR-4058-AP26                       PIC X(4).
           02  ACTVY-DATE-4058-AP26                     PIC X(6).
           02  AS-OF-DATE-4058-AP26                     PIC X(6).
           02  DLT-IND-4058-AP26                        PIC X(1).
           02  HOLD-IND-4058-AP26                       PIC X(1).
           02  TAX-RATE-CODE-4058-AP26                  PIC X(3).
           02  CHECK-GRPNG-IND-4058-AP26                PIC X(1).
           02  FROM-CHECK-DATE-4058-AP26                PIC X(6).
           02  TO-CHECK-DATE-4058-AP26                  PIC X(6).
           02  NMBR-DAYS-4058-AP26          OCCURS 3    PIC X(3).
           02  VNDR-TYPE-CODE-4058-AP26     OCCURS 10   PIC X(2).
           02  1099-RPRT-ID-4058-AP26                   PIC X(1).
           02  INV-DATE-4058-AP26                       PIC X(6).
           02  FROM-VNDR-CODE-AP26.
               03  FROM-VNDR-DIGIT-ONE-AP26             PIC X(1).
               03  FROM-VNDR-LAST-NINE-AP26             PIC X(9).
           02  TO-VNDR-CODE-AP26.
               03  TO-VNDR-DIGIT-ONE-AP26               PIC X(1).
               03  TO-VNDR-LAST-NINE-AP26               PIC X(9).
           02  NEXT-CHK-RUN-DATE-AP26                   PIC X(6).
