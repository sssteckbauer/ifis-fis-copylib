       975-TO-R4410-TRVL-INV SECTION.
           MOVE TRI-USER-CD TO
               USER-CODE-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-UNVRS-CD TO
               UNVRS-CODE-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-DOC-NBR TO
               DCMNT-NMBR-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-VNDR-INV-NBR TO
               VNDR-INV-NMBR-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-INV-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               INV-DATE-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-PYMT-DUE-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               PYMT-DUE-DATE-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-ADR-TYP-CD TO
               ADR-TYPE-CODE-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-BILL-TYP-IND TO
               BILL-TYPE-IND-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-CRDT-MEMO TO
               CRDT-MEMO-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-BANK-ACCT-CD TO
               BANK-ACCT-CODE-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-OPEN-PAID-HOLD-IND TO
               OPEN-PAID-HOLD-IND-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-INVD-AMT TO
               INVD-AMT-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-CMPLT-IND TO
               CMPLT-IND-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-APRVL-IND TO
               APRVL-IND-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-CNCL-IND TO
               CNCL-IND-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-CNCL-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               CNCL-DATE-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-HDR-ERROR-IND TO
               HDR-ERROR-IND-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-HDR-CNTR-DTL TO
               HDR-CNTR-DTL-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-NSF-OVRDE TO
               NSF-OVRDE-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-CHECK-GRPNG-IND TO
               CHECK-GRPNG-IND-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-VNDR-ID-DGT-ONE TO
               VNDR-ID-DIGIT-ONE-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-VNDR-ID-LST-NINE TO
               VNDR-ID-LAST-NINE-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-CHECK-CNTR TO
               CHECK-CNTR-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-VNDR-INV-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               VNDR-INV-DATE-4410 OF R4410-TRVL-INV
                                                                     .
           MOVE TRI-AMT-ACTIN-IND TO
               AMT-ACTION-IND-4410 OF R4410-TRVL-INV
                                                                     .
       975-TO-R4410-TRVL-INV-EXIT.
           EXIT.
