      **********************************************************
      *  CREATED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.
      *  COPY MODULE:  WSTIMEC2
      *  FORMATS:      PRE-EDIT-TIME (HHMMSSHS)
      **********************************************************

           05  PRE-EDIT-TIME.
               10  TIME-HHMMSS.
                   13  TIME-HHMM.
                       15  TIME-HH          PIC 9(002) VALUE ZEROES.
                       15  TIME-MM          PIC 9(002) VALUE ZEROES.
                   13  TIME-SS              PIC 9(002) VALUE ZEROES.
               10  TIME-HS                  PIC 9(002) VALUE ZEROES.

           05  PRE-EDIT-TIME-DISP.
               10  TIME-HH-DISP             PIC 9(002) VALUE ZEROES.
               10  FILLER                   PIC X(001) VALUE ':'.
               10  TIME-MM-DISP             PIC 9(002) VALUE ZEROES.
               10  FILLER                   PIC X(001) VALUE ':'.
               10  TIME-SS-DISP             PIC 9(002) VALUE ZEROES.

