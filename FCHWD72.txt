      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWD72                              04/24/00  12:57  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWD72.
           02  WORK-SLCTN-DATE-CH72           COMP-3    PIC S9(8).
           02  WORK-SYSTEM-DATE-CH72          COMP-3    PIC S9(8).
           02  WORK-SAVE-DBKEY-ID-4050-CH72
                                     COMP   OCCURS 11   PIC S9(8).
           02  WORK-SAVE-DBKEY-ID-4066-CH72
                                     COMP   OCCURS 11   PIC S9(8).
           02  WORK-LOOP-FLAG-CH72                      PIC X(1).
           02  WORK-SAVE-DBKEY-CH72           COMP      PIC S9(8).
           02  ACTN-FLAG-CH72                           PIC X(1).
           02  WORK-ADD-FLAG-CH72                       PIC X(1).
           02  WORK-LIST-FLAG-CH72                      PIC X(1).
           02  WORK-SEL-FLAG-CH72                       PIC X(1).
           02  SEL-FLAG-CH72                            PIC X(1).
