       975-TO-R4158-CHK-HDR SECTION.
           MOVE CKH-USER-CD TO
               USER-CODE-4158 OF R4158-CHK-HDR
                                                                     .
           MOVE CKH-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4158 OF R4158-CHK-HDR
                                                                     .
           MOVE CKH-UNVRS-CD TO
               UNVRS-CODE-4158 OF R4158-CHK-HDR
                                                                     .
           MOVE CKH-BANK-ACCT-CD TO
               BANK-ACCT-CODE-4158 OF R4158-CHK-HDR
                                                                     .
           MOVE CKH-CHECK-NBR TO
               CHECK-NMBR-4158 OF R4158-CHK-HDR
                                                                     .
           MOVE CKH-CHECK-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               CHECK-DATE-4158 OF R4158-CHK-HDR
                                                                     .
           MOVE CKH-CHECK-TYP-CD TO
               CHECK-TYPE-CODE-4158 OF R4158-CHK-HDR
                                                                     .
           MOVE CKH-CHECK-AMT TO
               CHECK-AMT-4158 OF R4158-CHK-HDR
                                                                     .
           MOVE CKH-CHECK-RGSTR-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               CHECK-RGSTR-DATE-4158 OF R4158-CHK-HDR
                                                                     .
           MOVE CKH-CNCL-IND TO
               CNCL-IND-4158 OF R4158-CHK-HDR
                                                                     .
           MOVE CKH-PRCSD-IND TO
               PRCSD-IND-4158 OF R4158-CHK-HDR
                                                                     .
           MOVE CKH-RCNCLTN-IND TO
               RCNCLTN-IND-4158 OF R4158-CHK-HDR
                                                                     .
           MOVE CKH-CHECK-RCNCLTN-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               CHECK-RCNCLTN-DATE-4158 OF R4158-CHK-HDR
                                                                     .
           MOVE CKH-DISCREPANCY-IND TO
               DISCREPANCY-IND-4158 OF R4158-CHK-HDR
                                                                     .
           MOVE CKH-CNCL-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               CNCL-DATE-4158 OF R4158-CHK-HDR
                                                                     .
           MOVE CKH-STOP-PAID-IND TO
               STOP-PAID-IND-4158 OF R4158-CHK-HDR
                                                                     .
           MOVE CKH-ISSUE-BANK-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ISSUE-BANK-DATE-4158 OF R4158-CHK-HDR
                                                                     .
           MOVE CKH-ISSU-RGSTR-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ISSUE-RGSTR-DATE-4158 OF R4158-CHK-HDR
                                                                     .
           MOVE CKH-CHECK-CNCL-CD TO
               CHK-CNCL-CODE-4158 OF R4158-CHK-HDR
                                                                     .
       975-TO-R4158-CHK-HDR-EXIT.
           EXIT.
