       975-TO-R4215-APRV-TMPLT SECTION.
           MOVE APV-USER-CD TO
               USER-CODE-4215 OF R4215-APRV-TMPLT
                                                                     .
           MOVE APV-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4215 OF R4215-APRV-TMPLT
                                                                     .
           MOVE APV-APV-TPLT-CD TO
               APRVL-TMPLT-CODE-4215 OF R4215-APRV-TMPLT
                                                                     .
           MOVE APV-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACTVY-DATE-4215 OF R4215-APRV-TMPLT
                                                                     .
           MOVE APV-APV-TPLT-DESC TO
               APRVL-TMPLT-DESC-4215 OF R4215-APRV-TMPLT
                                                                     .
       975-TO-R4215-APRV-TMPLT-EXIT.
           EXIT.
