       975-TO-R4029-FOAP-VALID SECTION.
           MOVE FVL-USER-CD TO
               USER-CODE-4029 OF R4029-FOAP-VALID
                                                                     .
           MOVE FVL-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4029 OF R4029-FOAP-VALID
                                                                     .
           MOVE FVL-SYSTEM-TIME-STMP TO
               SYSTEM-TIME-STAMP-4029 OF R4029-FOAP-VALID
                                                                     .
           MOVE FVL-UNVRS-CD TO
               UNVRS-CODE-4029 OF R4029-FOAP-VALID
                                                                     .
           MOVE FVL-COA-CD TO
               COA-CODE-4029 OF R4029-FOAP-VALID
                                                                     .
           MOVE FVL-FOAP-VALID-TYP TO
               FOAP-VALID-TYPE-4029 OF R4029-FOAP-VALID
                                                                     .
           MOVE FVL-FUND-CD TO
               FUND-CODE-4029 OF R4029-FOAP-VALID
                                                                     .
           MOVE FVL-ORGN-CD TO
               ORGZN-CODE-4029 OF R4029-FOAP-VALID
                                                                     .
           MOVE FVL-ACCT-CD TO
               ACCT-CODE-4029 OF R4029-FOAP-VALID
                                                                     .
           MOVE FVL-PRGRM-CD TO
               PRGRM-CODE-4029 OF R4029-FOAP-VALID
                                                                     .
           MOVE FVL-ACCT-TYP-CD TO
               ACCT-TYPE-CODE-4029 OF R4029-FOAP-VALID
                                                                     .
           MOVE FVL-FUND-TYP-CD TO
               FUND-TYPE-CODE-4029 OF R4029-FOAP-VALID
                                                                     .
           MOVE FVL-STATUS TO
               STATUS-4029 OF R4029-FOAP-VALID
                                                                     .
           MOVE FVL-FOAP-VAL-INVAL TO
               FOAP-VAL-INVAL-4029 OF R4029-FOAP-VALID
                                                                     .
           MOVE FVL-FOAP-EDIT-TYP TO
               FOAP-EDIT-TYPE-4029 OF R4029-FOAP-VALID
                                                                     .
       975-TO-R4029-FOAP-VALID-EXIT.
           EXIT.
