    ***Created by Convert/DC version V8R03 on 12/01/00 at 17:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RQST-CODE-AP21-Z = 'Y'
               MOVE WK01-RQST-CODE-AP21 TO RQST-RQST-CODE-AP21 OF
                FAPWK21
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4150-AP211-Z = 'Y'
               MOVE WK01-NMBR-4150-AP211 TO DCMNT-NMBR-4150-AP21 OF
                FAPWM21(0001)
           END-IF.
           MOVE WK01-NMBR-4150-AP211-F TO WK01-NMBR-4150-AP21-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4150-AP212-Z = 'Y'
               MOVE WK01-NMBR-4150-AP212 TO DCMNT-NMBR-4150-AP21 OF
                FAPWM21(0002)
           END-IF.
           MOVE WK01-NMBR-4150-AP212-F TO WK01-NMBR-4150-AP21-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4150-AP213-Z = 'Y'
               MOVE WK01-NMBR-4150-AP213 TO DCMNT-NMBR-4150-AP21 OF
                FAPWM21(0003)
           END-IF.
           MOVE WK01-NMBR-4150-AP213-F TO WK01-NMBR-4150-AP21-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4150-AP214-Z = 'Y'
               MOVE WK01-NMBR-4150-AP214 TO DCMNT-NMBR-4150-AP21 OF
                FAPWM21(0004)
           END-IF.
           MOVE WK01-NMBR-4150-AP214-F TO WK01-NMBR-4150-AP21-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4150-AP215-Z = 'Y'
               MOVE WK01-NMBR-4150-AP215 TO DCMNT-NMBR-4150-AP21 OF
                FAPWM21(0005)
           END-IF.
           MOVE WK01-NMBR-4150-AP215-F TO WK01-NMBR-4150-AP21-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4150-AP216-Z = 'Y'
               MOVE WK01-NMBR-4150-AP216 TO DCMNT-NMBR-4150-AP21 OF
                FAPWM21(0006)
           END-IF.
           MOVE WK01-NMBR-4150-AP216-F TO WK01-NMBR-4150-AP21-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4150-AP217-Z = 'Y'
               MOVE WK01-NMBR-4150-AP217 TO DCMNT-NMBR-4150-AP21 OF
                FAPWM21(0007)
           END-IF.
           MOVE WK01-NMBR-4150-AP217-F TO WK01-NMBR-4150-AP21-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4150-AP218-Z = 'Y'
               MOVE WK01-NMBR-4150-AP218 TO DCMNT-NMBR-4150-AP21 OF
                FAPWM21(0008)
           END-IF.
           MOVE WK01-NMBR-4150-AP218-F TO WK01-NMBR-4150-AP21-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4150-AP219-Z = 'Y'
               MOVE WK01-NMBR-4150-AP219 TO DCMNT-NMBR-4150-AP21 OF
                FAPWM21(0009)
           END-IF.
           MOVE WK01-NMBR-4150-AP219-F TO WK01-NMBR-4150-AP21-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4150-AP2110-Z = 'Y'
               MOVE WK01-NMBR-4150-AP2110 TO DCMNT-NMBR-4150-AP21 OF
                FAPWM21(0010)
           END-IF.
           MOVE WK01-NMBR-4150-AP2110-F TO WK01-NMBR-4150-AP21-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4150-AP2111-Z = 'Y'
               MOVE WK01-NMBR-4150-AP2111 TO DCMNT-NMBR-4150-AP21 OF
                FAPWM21(0011)
           END-IF.
           MOVE WK01-NMBR-4150-AP2111-F TO WK01-NMBR-4150-AP21-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP211-Z = 'Y'
               MOVE WK01-AMT-4150-AP211 TO APRVD-AMT-4150-AP21 OF
                FAPWM21(0001)
           END-IF.
           MOVE WK01-AMT-4150-AP211-F TO WK01-AMT-4150-AP21-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP212-Z = 'Y'
               MOVE WK01-AMT-4150-AP212 TO APRVD-AMT-4150-AP21 OF
                FAPWM21(0002)
           END-IF.
           MOVE WK01-AMT-4150-AP212-F TO WK01-AMT-4150-AP21-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP213-Z = 'Y'
               MOVE WK01-AMT-4150-AP213 TO APRVD-AMT-4150-AP21 OF
                FAPWM21(0003)
           END-IF.
           MOVE WK01-AMT-4150-AP213-F TO WK01-AMT-4150-AP21-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP214-Z = 'Y'
               MOVE WK01-AMT-4150-AP214 TO APRVD-AMT-4150-AP21 OF
                FAPWM21(0004)
           END-IF.
           MOVE WK01-AMT-4150-AP214-F TO WK01-AMT-4150-AP21-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP215-Z = 'Y'
               MOVE WK01-AMT-4150-AP215 TO APRVD-AMT-4150-AP21 OF
                FAPWM21(0005)
           END-IF.
           MOVE WK01-AMT-4150-AP215-F TO WK01-AMT-4150-AP21-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP216-Z = 'Y'
               MOVE WK01-AMT-4150-AP216 TO APRVD-AMT-4150-AP21 OF
                FAPWM21(0006)
           END-IF.
           MOVE WK01-AMT-4150-AP216-F TO WK01-AMT-4150-AP21-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP217-Z = 'Y'
               MOVE WK01-AMT-4150-AP217 TO APRVD-AMT-4150-AP21 OF
                FAPWM21(0007)
           END-IF.
           MOVE WK01-AMT-4150-AP217-F TO WK01-AMT-4150-AP21-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP218-Z = 'Y'
               MOVE WK01-AMT-4150-AP218 TO APRVD-AMT-4150-AP21 OF
                FAPWM21(0008)
           END-IF.
           MOVE WK01-AMT-4150-AP218-F TO WK01-AMT-4150-AP21-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP219-Z = 'Y'
               MOVE WK01-AMT-4150-AP219 TO APRVD-AMT-4150-AP21 OF
                FAPWM21(0009)
           END-IF.
           MOVE WK01-AMT-4150-AP219-F TO WK01-AMT-4150-AP21-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP2110-Z = 'Y'
               MOVE WK01-AMT-4150-AP2110 TO APRVD-AMT-4150-AP21 OF
                FAPWM21(0010)
           END-IF.
           MOVE WK01-AMT-4150-AP2110-F TO WK01-AMT-4150-AP21-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP2111-Z = 'Y'
               MOVE WK01-AMT-4150-AP2111 TO APRVD-AMT-4150-AP21 OF
                FAPWM21(0011)
           END-IF.
           MOVE WK01-AMT-4150-AP2111-F TO WK01-AMT-4150-AP21-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP2A1-Z = 'Y'
               MOVE WK01-AMT-4150-AP2A1 TO INVD-AMT-4150-AP21 OF
                FAPWM21(0001)
           END-IF.
           MOVE WK01-AMT-4150-AP2A1-F TO WK01-AMT-4150-AP2A-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP2A2-Z = 'Y'
               MOVE WK01-AMT-4150-AP2A2 TO INVD-AMT-4150-AP21 OF
                FAPWM21(0002)
           END-IF.
           MOVE WK01-AMT-4150-AP2A2-F TO WK01-AMT-4150-AP2A-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP2A3-Z = 'Y'
               MOVE WK01-AMT-4150-AP2A3 TO INVD-AMT-4150-AP21 OF
                FAPWM21(0003)
           END-IF.
           MOVE WK01-AMT-4150-AP2A3-F TO WK01-AMT-4150-AP2A-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP2A4-Z = 'Y'
               MOVE WK01-AMT-4150-AP2A4 TO INVD-AMT-4150-AP21 OF
                FAPWM21(0004)
           END-IF.
           MOVE WK01-AMT-4150-AP2A4-F TO WK01-AMT-4150-AP2A-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP2A5-Z = 'Y'
               MOVE WK01-AMT-4150-AP2A5 TO INVD-AMT-4150-AP21 OF
                FAPWM21(0005)
           END-IF.
           MOVE WK01-AMT-4150-AP2A5-F TO WK01-AMT-4150-AP2A-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP2A6-Z = 'Y'
               MOVE WK01-AMT-4150-AP2A6 TO INVD-AMT-4150-AP21 OF
                FAPWM21(0006)
           END-IF.
           MOVE WK01-AMT-4150-AP2A6-F TO WK01-AMT-4150-AP2A-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP2A7-Z = 'Y'
               MOVE WK01-AMT-4150-AP2A7 TO INVD-AMT-4150-AP21 OF
                FAPWM21(0007)
           END-IF.
           MOVE WK01-AMT-4150-AP2A7-F TO WK01-AMT-4150-AP2A-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP2A8-Z = 'Y'
               MOVE WK01-AMT-4150-AP2A8 TO INVD-AMT-4150-AP21 OF
                FAPWM21(0008)
           END-IF.
           MOVE WK01-AMT-4150-AP2A8-F TO WK01-AMT-4150-AP2A-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP2A9-Z = 'Y'
               MOVE WK01-AMT-4150-AP2A9 TO INVD-AMT-4150-AP21 OF
                FAPWM21(0009)
           END-IF.
           MOVE WK01-AMT-4150-AP2A9-F TO WK01-AMT-4150-AP2A-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP2A10-Z = 'Y'
               MOVE WK01-AMT-4150-AP2A10 TO INVD-AMT-4150-AP21 OF
                FAPWM21(0010)
           END-IF.
           MOVE WK01-AMT-4150-AP2A10-F TO WK01-AMT-4150-AP2A-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4150-AP2A11-Z = 'Y'
               MOVE WK01-AMT-4150-AP2A11 TO INVD-AMT-4150-AP21 OF
                FAPWM21(0011)
           END-IF.
           MOVE WK01-AMT-4150-AP2A11-F TO WK01-AMT-4150-AP2A-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-ACTVY-DATE-4150-AP211-Z = 'Y'
               MOVE WK01-ACTVY-DATE-4150-AP211 TO
                LAST-ACTVY-DATE-4150-AP21 OF FAPWM21(0001)
           END-IF.
           MOVE WK01-ACTVY-DATE-4150-AP211-F TO
                WK01-ACTVY-DATE-4150-AP21-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-ACTVY-DATE-4150-AP212-Z = 'Y'
               MOVE WK01-ACTVY-DATE-4150-AP212 TO
                LAST-ACTVY-DATE-4150-AP21 OF FAPWM21(0002)
           END-IF.
           MOVE WK01-ACTVY-DATE-4150-AP212-F TO
                WK01-ACTVY-DATE-4150-AP21-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-ACTVY-DATE-4150-AP213-Z = 'Y'
               MOVE WK01-ACTVY-DATE-4150-AP213 TO
                LAST-ACTVY-DATE-4150-AP21 OF FAPWM21(0003)
           END-IF.
           MOVE WK01-ACTVY-DATE-4150-AP213-F TO
                WK01-ACTVY-DATE-4150-AP21-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-ACTVY-DATE-4150-AP214-Z = 'Y'
               MOVE WK01-ACTVY-DATE-4150-AP214 TO
                LAST-ACTVY-DATE-4150-AP21 OF FAPWM21(0004)
           END-IF.
           MOVE WK01-ACTVY-DATE-4150-AP214-F TO
                WK01-ACTVY-DATE-4150-AP21-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-ACTVY-DATE-4150-AP215-Z = 'Y'
               MOVE WK01-ACTVY-DATE-4150-AP215 TO
                LAST-ACTVY-DATE-4150-AP21 OF FAPWM21(0005)
           END-IF.
           MOVE WK01-ACTVY-DATE-4150-AP215-F TO
                WK01-ACTVY-DATE-4150-AP21-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-ACTVY-DATE-4150-AP216-Z = 'Y'
               MOVE WK01-ACTVY-DATE-4150-AP216 TO
                LAST-ACTVY-DATE-4150-AP21 OF FAPWM21(0006)
           END-IF.
           MOVE WK01-ACTVY-DATE-4150-AP216-F TO
                WK01-ACTVY-DATE-4150-AP21-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-ACTVY-DATE-4150-AP217-Z = 'Y'
               MOVE WK01-ACTVY-DATE-4150-AP217 TO
                LAST-ACTVY-DATE-4150-AP21 OF FAPWM21(0007)
           END-IF.
           MOVE WK01-ACTVY-DATE-4150-AP217-F TO
                WK01-ACTVY-DATE-4150-AP21-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-ACTVY-DATE-4150-AP218-Z = 'Y'
               MOVE WK01-ACTVY-DATE-4150-AP218 TO
                LAST-ACTVY-DATE-4150-AP21 OF FAPWM21(0008)
           END-IF.
           MOVE WK01-ACTVY-DATE-4150-AP218-F TO
                WK01-ACTVY-DATE-4150-AP21-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-ACTVY-DATE-4150-AP219-Z = 'Y'
               MOVE WK01-ACTVY-DATE-4150-AP219 TO
                LAST-ACTVY-DATE-4150-AP21 OF FAPWM21(0009)
           END-IF.
           MOVE WK01-ACTVY-DATE-4150-AP219-F TO
                WK01-ACTVY-DATE-4150-AP21-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-ACTVY-DATE-4150-AP2110-Z = 'Y'
               MOVE WK01-ACTVY-DATE-4150-AP2110 TO
                LAST-ACTVY-DATE-4150-AP21 OF FAPWM21(0010)
           END-IF.
           MOVE WK01-ACTVY-DATE-4150-AP2110-F TO
                WK01-ACTVY-DATE-4150-AP21-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-ACTVY-DATE-4150-AP2111-Z = 'Y'
               MOVE WK01-ACTVY-DATE-4150-AP2111 TO
                LAST-ACTVY-DATE-4150-AP21 OF FAPWM21(0011)
           END-IF.
           MOVE WK01-ACTVY-DATE-4150-AP2111-F TO
                WK01-ACTVY-DATE-4150-AP21-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-IND-4150-AP211-Z = 'Y'
               MOVE WK01-IND-4150-AP211 TO APRVL-IND-4150-AP21 OF
                FAPWM21(0001)
           END-IF.
           MOVE WK01-IND-4150-AP211-F TO WK01-IND-4150-AP21-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-IND-4150-AP212-Z = 'Y'
               MOVE WK01-IND-4150-AP212 TO APRVL-IND-4150-AP21 OF
                FAPWM21(0002)
           END-IF.
           MOVE WK01-IND-4150-AP212-F TO WK01-IND-4150-AP21-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-IND-4150-AP213-Z = 'Y'
               MOVE WK01-IND-4150-AP213 TO APRVL-IND-4150-AP21 OF
                FAPWM21(0003)
           END-IF.
           MOVE WK01-IND-4150-AP213-F TO WK01-IND-4150-AP21-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-IND-4150-AP214-Z = 'Y'
               MOVE WK01-IND-4150-AP214 TO APRVL-IND-4150-AP21 OF
                FAPWM21(0004)
           END-IF.
           MOVE WK01-IND-4150-AP214-F TO WK01-IND-4150-AP21-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-IND-4150-AP215-Z = 'Y'
               MOVE WK01-IND-4150-AP215 TO APRVL-IND-4150-AP21 OF
                FAPWM21(0005)
           END-IF.
           MOVE WK01-IND-4150-AP215-F TO WK01-IND-4150-AP21-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-IND-4150-AP216-Z = 'Y'
               MOVE WK01-IND-4150-AP216 TO APRVL-IND-4150-AP21 OF
                FAPWM21(0006)
           END-IF.
           MOVE WK01-IND-4150-AP216-F TO WK01-IND-4150-AP21-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-IND-4150-AP217-Z = 'Y'
               MOVE WK01-IND-4150-AP217 TO APRVL-IND-4150-AP21 OF
                FAPWM21(0007)
           END-IF.
           MOVE WK01-IND-4150-AP217-F TO WK01-IND-4150-AP21-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-IND-4150-AP218-Z = 'Y'
               MOVE WK01-IND-4150-AP218 TO APRVL-IND-4150-AP21 OF
                FAPWM21(0008)
           END-IF.
           MOVE WK01-IND-4150-AP218-F TO WK01-IND-4150-AP21-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-IND-4150-AP219-Z = 'Y'
               MOVE WK01-IND-4150-AP219 TO APRVL-IND-4150-AP21 OF
                FAPWM21(0009)
           END-IF.
           MOVE WK01-IND-4150-AP219-F TO WK01-IND-4150-AP21-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-IND-4150-AP2110-Z = 'Y'
               MOVE WK01-IND-4150-AP2110 TO APRVL-IND-4150-AP21 OF
                FAPWM21(0010)
           END-IF.
           MOVE WK01-IND-4150-AP2110-F TO WK01-IND-4150-AP21-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-IND-4150-AP2111-Z = 'Y'
               MOVE WK01-IND-4150-AP2111 TO APRVL-IND-4150-AP21 OF
                FAPWM21(0011)
           END-IF.
           MOVE WK01-IND-4150-AP2111-F TO WK01-IND-4150-AP21-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-PAID-IND-4150-AP211-Z = 'Y'
               MOVE WK01-PAID-IND-4150-AP211 TO OPEN-PAID-IND-4150-AP21
                OF FAPWM21(0001)
           END-IF.
           MOVE WK01-PAID-IND-4150-AP211-F TO WK01-PAID-IND-4150-AP21-F
                (1).
      *%--------------------------------------------------------------%*
           IF WK01-PAID-IND-4150-AP212-Z = 'Y'
               MOVE WK01-PAID-IND-4150-AP212 TO OPEN-PAID-IND-4150-AP21
                OF FAPWM21(0002)
           END-IF.
           MOVE WK01-PAID-IND-4150-AP212-F TO WK01-PAID-IND-4150-AP21-F
                (2).
      *%--------------------------------------------------------------%*
           IF WK01-PAID-IND-4150-AP213-Z = 'Y'
               MOVE WK01-PAID-IND-4150-AP213 TO OPEN-PAID-IND-4150-AP21
                OF FAPWM21(0003)
           END-IF.
           MOVE WK01-PAID-IND-4150-AP213-F TO WK01-PAID-IND-4150-AP21-F
                (3).
      *%--------------------------------------------------------------%*
           IF WK01-PAID-IND-4150-AP214-Z = 'Y'
               MOVE WK01-PAID-IND-4150-AP214 TO OPEN-PAID-IND-4150-AP21
                OF FAPWM21(0004)
           END-IF.
           MOVE WK01-PAID-IND-4150-AP214-F TO WK01-PAID-IND-4150-AP21-F
                (4).
      *%--------------------------------------------------------------%*
           IF WK01-PAID-IND-4150-AP215-Z = 'Y'
               MOVE WK01-PAID-IND-4150-AP215 TO OPEN-PAID-IND-4150-AP21
                OF FAPWM21(0005)
           END-IF.
           MOVE WK01-PAID-IND-4150-AP215-F TO WK01-PAID-IND-4150-AP21-F
                (5).
      *%--------------------------------------------------------------%*
           IF WK01-PAID-IND-4150-AP216-Z = 'Y'
               MOVE WK01-PAID-IND-4150-AP216 TO OPEN-PAID-IND-4150-AP21
                OF FAPWM21(0006)
           END-IF.
           MOVE WK01-PAID-IND-4150-AP216-F TO WK01-PAID-IND-4150-AP21-F
                (6).
      *%--------------------------------------------------------------%*
           IF WK01-PAID-IND-4150-AP217-Z = 'Y'
               MOVE WK01-PAID-IND-4150-AP217 TO OPEN-PAID-IND-4150-AP21
                OF FAPWM21(0007)
           END-IF.
           MOVE WK01-PAID-IND-4150-AP217-F TO WK01-PAID-IND-4150-AP21-F
                (7).
      *%--------------------------------------------------------------%*
           IF WK01-PAID-IND-4150-AP218-Z = 'Y'
               MOVE WK01-PAID-IND-4150-AP218 TO OPEN-PAID-IND-4150-AP21
                OF FAPWM21(0008)
           END-IF.
           MOVE WK01-PAID-IND-4150-AP218-F TO WK01-PAID-IND-4150-AP21-F
                (8).
      *%--------------------------------------------------------------%*
           IF WK01-PAID-IND-4150-AP219-Z = 'Y'
               MOVE WK01-PAID-IND-4150-AP219 TO OPEN-PAID-IND-4150-AP21
                OF FAPWM21(0009)
           END-IF.
           MOVE WK01-PAID-IND-4150-AP219-F TO WK01-PAID-IND-4150-AP21-F
                (9).
      *%--------------------------------------------------------------%*
           IF WK01-PAID-IND-4150-AP2110-Z = 'Y'
               MOVE WK01-PAID-IND-4150-AP2110 TO
                OPEN-PAID-IND-4150-AP21 OF FAPWM21(0010)
           END-IF.
           MOVE WK01-PAID-IND-4150-AP2110-F TO
                WK01-PAID-IND-4150-AP21-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-PAID-IND-4150-AP2111-Z = 'Y'
               MOVE WK01-PAID-IND-4150-AP2111 TO
                OPEN-PAID-IND-4150-AP21 OF FAPWM21(0011)
           END-IF.
           MOVE WK01-PAID-IND-4150-AP2111-F TO
                WK01-PAID-IND-4150-AP21-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-MEMO-4150-AP211-Z = 'Y'
               MOVE WK01-MEMO-4150-AP211 TO CRDT-MEMO-4150-AP21 OF
                FAPWM21(0001)
           END-IF.
           MOVE WK01-MEMO-4150-AP211-F TO WK01-MEMO-4150-AP21-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-MEMO-4150-AP212-Z = 'Y'
               MOVE WK01-MEMO-4150-AP212 TO CRDT-MEMO-4150-AP21 OF
                FAPWM21(0002)
           END-IF.
           MOVE WK01-MEMO-4150-AP212-F TO WK01-MEMO-4150-AP21-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-MEMO-4150-AP213-Z = 'Y'
               MOVE WK01-MEMO-4150-AP213 TO CRDT-MEMO-4150-AP21 OF
                FAPWM21(0003)
           END-IF.
           MOVE WK01-MEMO-4150-AP213-F TO WK01-MEMO-4150-AP21-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-MEMO-4150-AP214-Z = 'Y'
               MOVE WK01-MEMO-4150-AP214 TO CRDT-MEMO-4150-AP21 OF
                FAPWM21(0004)
           END-IF.
           MOVE WK01-MEMO-4150-AP214-F TO WK01-MEMO-4150-AP21-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-MEMO-4150-AP215-Z = 'Y'
               MOVE WK01-MEMO-4150-AP215 TO CRDT-MEMO-4150-AP21 OF
                FAPWM21(0005)
           END-IF.
           MOVE WK01-MEMO-4150-AP215-F TO WK01-MEMO-4150-AP21-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-MEMO-4150-AP216-Z = 'Y'
               MOVE WK01-MEMO-4150-AP216 TO CRDT-MEMO-4150-AP21 OF
                FAPWM21(0006)
           END-IF.
           MOVE WK01-MEMO-4150-AP216-F TO WK01-MEMO-4150-AP21-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-MEMO-4150-AP217-Z = 'Y'
               MOVE WK01-MEMO-4150-AP217 TO CRDT-MEMO-4150-AP21 OF
                FAPWM21(0007)
           END-IF.
           MOVE WK01-MEMO-4150-AP217-F TO WK01-MEMO-4150-AP21-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-MEMO-4150-AP218-Z = 'Y'
               MOVE WK01-MEMO-4150-AP218 TO CRDT-MEMO-4150-AP21 OF
                FAPWM21(0008)
           END-IF.
           MOVE WK01-MEMO-4150-AP218-F TO WK01-MEMO-4150-AP21-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-MEMO-4150-AP219-Z = 'Y'
               MOVE WK01-MEMO-4150-AP219 TO CRDT-MEMO-4150-AP21 OF
                FAPWM21(0009)
           END-IF.
           MOVE WK01-MEMO-4150-AP219-F TO WK01-MEMO-4150-AP21-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-MEMO-4150-AP2110-Z = 'Y'
               MOVE WK01-MEMO-4150-AP2110 TO CRDT-MEMO-4150-AP21 OF
                FAPWM21(0010)
           END-IF.
           MOVE WK01-MEMO-4150-AP2110-F TO WK01-MEMO-4150-AP21-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-MEMO-4150-AP2111-Z = 'Y'
               MOVE WK01-MEMO-4150-AP2111 TO CRDT-MEMO-4150-AP21 OF
                FAPWM21(0011)
           END-IF.
           MOVE WK01-MEMO-4150-AP2111-F TO WK01-MEMO-4150-AP21-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-AP21-Z = 'Y'
               MOVE WK01-FLAG-AP21 TO SELECT-FLAG-AP21 OF FAPWK21
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP211-Z = 'Y'
               MOVE WK01-CODE-AP211 TO ACTN-CODE-AP21 OF FAPWM21(0001)
           END-IF.
           MOVE WK01-CODE-AP211-F TO WK01-CODE-AP21-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP212-Z = 'Y'
               MOVE WK01-CODE-AP212 TO ACTN-CODE-AP21 OF FAPWM21(0002)
           END-IF.
           MOVE WK01-CODE-AP212-F TO WK01-CODE-AP21-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP213-Z = 'Y'
               MOVE WK01-CODE-AP213 TO ACTN-CODE-AP21 OF FAPWM21(0003)
           END-IF.
           MOVE WK01-CODE-AP213-F TO WK01-CODE-AP21-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP214-Z = 'Y'
               MOVE WK01-CODE-AP214 TO ACTN-CODE-AP21 OF FAPWM21(0004)
           END-IF.
           MOVE WK01-CODE-AP214-F TO WK01-CODE-AP21-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP215-Z = 'Y'
               MOVE WK01-CODE-AP215 TO ACTN-CODE-AP21 OF FAPWM21(0005)
           END-IF.
           MOVE WK01-CODE-AP215-F TO WK01-CODE-AP21-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP216-Z = 'Y'
               MOVE WK01-CODE-AP216 TO ACTN-CODE-AP21 OF FAPWM21(0006)
           END-IF.
           MOVE WK01-CODE-AP216-F TO WK01-CODE-AP21-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP217-Z = 'Y'
               MOVE WK01-CODE-AP217 TO ACTN-CODE-AP21 OF FAPWM21(0007)
           END-IF.
           MOVE WK01-CODE-AP217-F TO WK01-CODE-AP21-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP218-Z = 'Y'
               MOVE WK01-CODE-AP218 TO ACTN-CODE-AP21 OF FAPWM21(0008)
           END-IF.
           MOVE WK01-CODE-AP218-F TO WK01-CODE-AP21-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP219-Z = 'Y'
               MOVE WK01-CODE-AP219 TO ACTN-CODE-AP21 OF FAPWM21(0009)
           END-IF.
           MOVE WK01-CODE-AP219-F TO WK01-CODE-AP21-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP2110-Z = 'Y'
               MOVE WK01-CODE-AP2110 TO ACTN-CODE-AP21 OF FAPWM21(0010)
           END-IF.
           MOVE WK01-CODE-AP2110-F TO WK01-CODE-AP21-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP2111-Z = 'Y'
               MOVE WK01-CODE-AP2111 TO ACTN-CODE-AP21 OF FAPWM21(0011)
           END-IF.
           MOVE WK01-CODE-AP2111-F TO WK01-CODE-AP21-F (11).
