       01  R6353-ENTY-CHG.
           02  DB-PROC-ID-6353.
               03  USER-CODE-6353                       PIC X(8).
               03  LAST-ACTVY-DATE-6353                 PIC X(5).
               03  TRMNL-ID-6353                        PIC X(8).
               03  PURGE-FLAG-6353                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  CHANGE-DATE-6353               COMP-3    PIC 9(8).
