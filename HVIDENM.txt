       975-TO-R6352-ENTY-NAME SECTION.
           MOVE ENM-USER-CD TO
               USER-CODE-6352 OF R6352-ENTY-NAME
                                                                     .
           MOVE ENM-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-6352 OF R6352-ENTY-NAME
                                                                     .
           MOVE ENM-UNVRS-CD TO
               UNVRS-CODE-6352 OF R6352-ENTY-NAME
                                                                     .
           MOVE ENM-NAME-KEY TO
               NAME-KEY-6352 OF R6352-ENTY-NAME
                                                                     .
           MOVE ENM-CHANGE-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               CHANGE-DATE-6352 OF R6352-ENTY-NAME
                                                                     .
           MOVE ENM-NAME-CD TO
               NAME-CODE-6352 OF R6352-ENTY-NAME
                                                                     .
       975-TO-R6352-ENTY-NAME-EXIT.
           EXIT.
