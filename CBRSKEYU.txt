     *****   MAINTENANCE DESCRIPTION   **************
     **      DECEMBER 2003 -- CHANGES FOR FUTDI04, FUTDI06, FUTDI13
     **==============================================================
           03  DBLINK-RECORD-SET-KEYS.
           04  DBLINK-RECORD-KEYS.                                      235 DOVH
           05  DBLINK-S-6311-6185-KEY.                                  240 DOVH
             08  DBLINK-S-6311-6185-KEY-O.                              240 DOVH
               10  DBLINK-S-6311-6185-01    PIC X(1)        VALUE SPACE.240 DOVH
               10  DBLINK-S-6311-6185-02    PIC X(9)        VALUE SPACE.240 DOVH
             08  DBLINK-S-6311-6185-KEY-M.                              240 DOVH
             09  DBLINK-S-6311-6185-KEY-U.                              240 DOVH
               10  DBLINK-S-6311-6185-03    PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-S-6311-6185-04    PIC X(10)       VALUE ZERO. 240 DOVH
           05  DBLINK-WS-S-6311-6185-KEY.                               240 DOVH
               10  DBLINK-WS-S-6311-6185-03 PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-WS-S-6311-6185-04 PIC S9(8)       COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
           05  DBLINK-S-6311-6186-KEY.                                  240 DOVH
             08  DBLINK-S-6311-6186-KEY-O.                              240 DOVH
               10  DBLINK-S-6311-6186-01    PIC X(1)        VALUE SPACE.240 DOVH
               10  DBLINK-S-6311-6186-02    PIC X(9)        VALUE SPACE.240 DOVH
             08  DBLINK-S-6311-6186-KEY-M.                              240 DOVH
             09  DBLINK-S-6311-6186-KEY-U.                              240 DOVH
               10  DBLINK-S-6311-6186-03    PIC X(10)       VALUE ZERO. 240 DOVH
             09  DBLINK-S-6311-6186-KEY-S.                              240 DOVH
               10  DBLINK-S-6311-6186-04    PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-WS-S-6311-6186-KEY.                               240 DOVH
               10  DBLINK-WS-S-6311-6186-03 PIC 9(8)        COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
             09  DBLINK-S-6311-6186-KEY-S.                              240 DOVH
               10  DBLINK-WS-S-6311-6186-04 PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-6311-6274-KEY.                                  240 DOVH
             08  DBLINK-S-6311-6274-KEY-O.                              240 DOVH
               10  DBLINK-S-6311-6274-01    PIC X(1)        VALUE SPACE.240 DOVH
               10  DBLINK-S-6311-6274-02    PIC X(9)        VALUE SPACE.240 DOVH
             08  DBLINK-S-6311-6274-KEY-M.                              240 DOVH
             09  DBLINK-S-6311-6274-KEY-U.                              240 DOVH
               10  DBLINK-S-6311-6274-03    PIC X(10)       VALUE ZERO. 240 DOVH
               10  DBLINK-S-6311-6274-04    PIC S9(6)       COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
               10  DBLINK-S-6311-6274-05    PIC X(4)        VALUE SPACE.240 DOVH
             09  DBLINK-S-6311-6274-KEY-S.                              240 DOVH
               10  DBLINK-S-6311-6274-06    PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-WS-S-6311-6274-KEY.                               240 DOVH
               10  DBLINK-WS-S-6311-6274-03 PIC S9(8)       COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
               10  DBLINK-WS-S-6311-6274-04 PIC S9(6)       COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
               10  DBLINK-WS-S-6311-6274-05 PIC X(4)        VALUE SPACE.240 DOVH
             09  DBLINK-S-6311-6274-KEY-S.                              240 DOVH
               10  DBLINK-WS-S-6311-6274-06 PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-6311-6350-KEY.                                  240 DOVH
             08  DBLINK-S-6311-6350-KEY-O.                              240 DOVH
               10  DBLINK-S-6311-6350-01    PIC X(1)        VALUE SPACE.240 DOVH
               10  DBLINK-S-6311-6350-02    PIC X(9)        VALUE SPACE.240 DOVH
             08  DBLINK-S-6311-6350-KEY-M.                              240 DOVH
             09  DBLINK-S-6311-6350-KEY-U.                              240 DOVH
               10  DBLINK-S-6311-6350-03    PIC X(4)        VALUE SPACE.240 DOVH
               10  DBLINK-S-6311-6350-04    PIC X(4)        VALUE SPACE.240 DOVH
           05  DBLINK-S-6311-6352-KEY.                                  240 DOVH
             08  DBLINK-S-6311-6352-KEY-O.                              240 DOVH
               10  DBLINK-S-6311-6352-01    PIC X(1)        VALUE SPACE.240 DOVH
               10  DBLINK-S-6311-6352-02    PIC X(9)        VALUE SPACE.240 DOVH
             08  DBLINK-S-6311-6352-KEY-M.                              240 DOVH
             09  DBLINK-S-6311-6352-KEY-U.                              240 DOVH
               10  DBLINK-S-6311-6352-03    PIC X(10)       VALUE ZERO. 240 DOVH
             09  DBLINK-S-6311-6352-KEY-S.                              240 DOVH
               10  DBLINK-S-6311-6352-04    PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-WS-S-6311-6352-KEY.                               240 DOVH
               10  DBLINK-WS-S-6311-6352-03 PIC 9(8)        COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
             09  DBLINK-S-6311-6352-KEY-S.                              240 DOVH
               10  DBLINK-WS-S-6311-6352-04 PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-6311-6353-KEY.                                  240 DOVH
             08  DBLINK-S-6311-6353-KEY-O.                              240 DOVH
               10  DBLINK-S-6311-6353-01    PIC X(1)        VALUE SPACE.240 DOVH
               10  DBLINK-S-6311-6353-02    PIC X(9)        VALUE SPACE.240 DOVH
             08  DBLINK-S-6311-6353-KEY-M.                              240 DOVH
             09  DBLINK-S-6311-6353-KEY-U.                              240 DOVH
               10  DBLINK-S-6311-6353-03    PIC X(10)       VALUE ZERO. 240 DOVH
             09  DBLINK-S-6311-6353-KEY-S.                              240 DOVH
               10  DBLINK-S-6311-6353-04    PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-WS-S-6311-6353-KEY.                               240 DOVH
               10  DBLINK-WS-S-6311-6353-03 PIC 9(8)        COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
             09  DBLINK-S-6311-6353-KEY-S.                              240 DOVH
               10  DBLINK-WS-S-6311-6353-04 PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-6311-6185-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-6311-6186-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-6311-6274-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-6311-6350-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-6311-6352-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-6311-6353-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-6117-6139-KEY.                                  240 DOVH
             08  DBLINK-S-6117-6139-KEY-O.                              240 DOVH
               10  DBLINK-S-6117-6139-01    PIC S9(7)       COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
             08  DBLINK-S-6117-6139-KEY-M.                              240 DOVH
             09  DBLINK-S-6117-6139-KEY-U.                              240 DOVH
               10  DBLINK-S-6117-6139-02    PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-S-6117-6139-03    PIC X(10)       VALUE ZERO. 240 DOVH
           05  DBLINK-WS-S-6117-6139-KEY.                               240 DOVH
               10  DBLINK-WS-S-6117-6139-02 PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-WS-S-6117-6139-03 PIC S9(8)       COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
           05  DBLINK-S-6117-6156-KEY.                                  240 DOVH
             08  DBLINK-S-6117-6156-KEY-O.                              240 DOVH
               10  DBLINK-S-6117-6156-01    PIC S9(7)       COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
             08  DBLINK-S-6117-6156-KEY-M.                              240 DOVH
             09  DBLINK-S-6117-6156-KEY-U.                              240 DOVH
               10  DBLINK-S-6117-6156-02    PIC X(10)       VALUE ZERO. 240 DOVH
             09  DBLINK-S-6117-6156-KEY-S.                              240 DOVH
               10  DBLINK-S-6117-6156-03    PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-WS-S-6117-6156-KEY.                               240 DOVH
               10  DBLINK-WS-S-6117-6156-02 PIC 9(8)        COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
             09  DBLINK-S-6117-6156-KEY-S.                              240 DOVH
               10  DBLINK-WS-S-6117-6156-03 PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-6117-6157-KEY.                                  240 DOVH
             08  DBLINK-S-6117-6157-KEY-O.                              240 DOVH
               10  DBLINK-S-6117-6157-01    PIC S9(7)       COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
             08  DBLINK-S-6117-6157-KEY-M.                              240 DOVH
             09  DBLINK-S-6117-6157-KEY-U.                              240 DOVH
               10  DBLINK-S-6117-6157-02    PIC X(10)       VALUE ZERO. 240 DOVH
             09  DBLINK-S-6117-6157-KEY-S.                              240 DOVH
               10  DBLINK-S-6117-6157-03    PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-WS-S-6117-6157-KEY.                               240 DOVH
               10  DBLINK-WS-S-6117-6157-02 PIC 9(8)        COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
             09  DBLINK-S-6117-6157-KEY-S.                              240 DOVH
               10  DBLINK-WS-S-6117-6157-03 PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-6117-6274-KEY.                                  240 DOVH
             08  DBLINK-S-6117-6274-KEY-O.                              240 DOVH
               10  DBLINK-S-6117-6274-01    PIC S9(7)       COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
             08  DBLINK-S-6117-6274-KEY-M.                              240 DOVH
             09  DBLINK-S-6117-6274-KEY-U.                              240 DOVH
               10  DBLINK-S-6117-6274-02    PIC X(10)       VALUE ZERO. 240 DOVH
               10  DBLINK-S-6117-6274-03    PIC S9(6)       COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
               10  DBLINK-S-6117-6274-04    PIC X(4)        VALUE SPACE.240 DOVH
             09  DBLINK-S-6117-6274-KEY-S.                              240 DOVH
               10  DBLINK-S-6117-6274-05    PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-WS-S-6117-6274-KEY.                               240 DOVH
               10  DBLINK-WS-S-6117-6274-02 PIC S9(8)       COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
               10  DBLINK-WS-S-6117-6274-03 PIC S9(6)       COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
               10  DBLINK-WS-S-6117-6274-04 PIC X(4)        VALUE SPACE.240 DOVH
             09  DBLINK-S-6117-6274-KEY-S.                              240 DOVH
               10  DBLINK-WS-S-6117-6274-05 PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-6117-6313-KEY.                                  240 DOVH
             08  DBLINK-S-6117-6313-KEY-O.                              240 DOVH
               10  DBLINK-S-6117-6313-01    PIC S9(7)       COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
             08  DBLINK-S-6117-6313-KEY-M.                              240 DOVH
             09  DBLINK-S-6117-6313-KEY-U.                              240 DOVH
               10  DBLINK-S-6117-6313-02    PIC X(10)       VALUE ZERO. 240 DOVH
             09  DBLINK-S-6117-6313-KEY-S.                              240 DOVH
               10  DBLINK-S-6117-6313-03    PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-WS-S-6117-6313-KEY.                               240 DOVH
               10  DBLINK-WS-S-6117-6313-02 PIC 9(8)        COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
             09  DBLINK-S-6117-6313-KEY-S.                              240 DOVH
               10  DBLINK-WS-S-6117-6313-03 PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-6117-6139-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-6117-6156-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-6117-6157-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-6117-6274-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-6117-6313-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-6200-6201-KEY.                                  240 DOVH
             08  DBLINK-S-6200-6201-KEY-O.                              240 DOVH
               10  DBLINK-S-6200-6201-01    PIC S9(7)       COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
               10  DBLINK-S-6200-6201-02    PIC X(1)        VALUE SPACE.240 DOVH
             08  DBLINK-S-6200-6201-KEY-M.                              240 DOVH
             09  DBLINK-S-6200-6201-KEY-U.                              240 DOVH
               10  DBLINK-S-6200-6201-03    PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-S-6200-6201-04    PIC X(10)       VALUE ZERO. 240 DOVH
           05  DBLINK-WS-S-6200-6201-KEY.                               240 DOVH
               10  DBLINK-WS-S-6200-6201-03 PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-WS-S-6200-6201-04 PIC S9(8)       COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
           05  DBLINK-S-6200-6202-KEY.                                  240 DOVH
             08  DBLINK-S-6200-6202-KEY-O.                              240 DOVH
               10  DBLINK-S-6200-6202-01    PIC S9(7)       COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
               10  DBLINK-S-6200-6202-02    PIC X(1)        VALUE SPACE.240 DOVH
             08  DBLINK-S-6200-6202-KEY-M.                              240 DOVH
             09  DBLINK-S-6200-6202-KEY-U.                              240 DOVH
               10  DBLINK-S-6200-6202-03    PIC X(4)        VALUE SPACE.240 DOVH
               10  DBLINK-S-6200-6202-04    PIC X(4)        VALUE SPACE.240 DOVH
               10  DBLINK-S-6200-6202-05    PIC X(10)       VALUE ZERO. 240 DOVH
           05  DBLINK-WS-S-6200-6202-KEY.                               240 DOVH
               10  DBLINK-WS-S-6200-6202-03 PIC X(4)        VALUE SPACE.240 DOVH
               10  DBLINK-WS-S-6200-6202-04 PIC X(4)        VALUE SPACE.240 DOVH
               10  DBLINK-WS-S-6200-6202-05 PIC S9(8)       COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
           05  DBLINK-S-6200-6201-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-6200-6202-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-R4173-FIMS-NAME-KEY.                              235 DOVH
               10  DBLINK-R4173-FIMS-NAME-01                            235 DOVH
                                            PIC S9(7)       COMP-3      235 DOVH
                       VALUE ZERO.                                      235 DOVH
               10  DBLINK-R4173-FIMS-NAME-02                            235 DOVH
                                            PIC X(1)        VALUE SPACE.235 DOVH
           05  DBLINK-R6185-ENTY-ADR-KEY.                               235 DOVH
               10  DBLINK-R6185-ENTY-ADR-01 PIC X(1)        VALUE SPACE.235 DOVH
               10  DBLINK-R6185-ENTY-ADR-02 PIC X(9)        VALUE SPACE.235 DOVH
               10  DBLINK-R6185-ENTY-ADR-03 PIC X(2)        VALUE SPACE.235 DOVH
               10  DBLINK-R6185-ENTY-ADR-04 PIC X(10)       VALUE ZERO. 235 DOVH
           05  DBLINK-R6186-ENTY-ID-KEY.                                235 DOVH
               10  DBLINK-R6186-ENTY-ID-01  PIC X(1)        VALUE SPACE.235 DOVH
               10  DBLINK-R6186-ENTY-ID-02  PIC X(9)        VALUE SPACE.235 DOVH
               10  DBLINK-R6186-ENTY-ID-03  PIC X(10)       VALUE ZERO. 235 DOVH
               10  DBLINK-R6186-ENTY-ID-04  PIC X(26)       VALUE ZERO. 235 DOVH
           05  DBLINK-R6352-ENTY-NAME-KEY.                              235 DOVH
               10  DBLINK-R6352-ENTY-NAME-01                            235 DOVH
                                            PIC X(1)        VALUE SPACE.235 DOVH
               10  DBLINK-R6352-ENTY-NAME-02                            235 DOVH
                                            PIC X(9)        VALUE SPACE.235 DOVH
               10  DBLINK-R6352-ENTY-NAME-03                            235 DOVH
                                            PIC X(10)       VALUE ZERO. 235 DOVH
               10  DBLINK-R6352-ENTY-NAME-04                            235 DOVH
                                            PIC X(26)       VALUE ZERO. 235 DOVH
           05  DBLINK-R6139-PRSN-ADR-KEY.                               235 DOVH
               10  DBLINK-R6139-PRSN-ADR-01 PIC S9(7)       COMP-3      235 DOVH
                       VALUE ZERO.                                      235 DOVH
               10  DBLINK-R6139-PRSN-ADR-02 PIC X(2)        VALUE SPACE.235 DOVH
               10  DBLINK-R6139-PRSN-ADR-03 PIC X(10)       VALUE ZERO. 235 DOVH
           05  DBLINK-R6156-PRSN-PRVNM-KEY.                             235 DOVH
               10  DBLINK-R6156-PRSN-PRVNM-01                           235 DOVH
                                            PIC S9(7)       COMP-3      235 DOVH
                       VALUE ZERO.                                      235 DOVH
               10  DBLINK-R6156-PRSN-PRVNM-02                           235 DOVH
                                            PIC X(10)       VALUE ZERO. 235 DOVH
               10  DBLINK-R6156-PRSN-PRVNM-03                           235 DOVH
                                            PIC X(26)       VALUE ZERO. 235 DOVH
           05  DBLINK-R6157-PRSN-PRVID-KEY.                             235 DOVH
               10  DBLINK-R6157-PRSN-PRVID-01                           235 DOVH
                                            PIC S9(7)       COMP-3      235 DOVH
                       VALUE ZERO.                                      235 DOVH
               10  DBLINK-R6157-PRSN-PRVID-02                           235 DOVH
                                            PIC X(10)       VALUE ZERO. 235 DOVH
               10  DBLINK-R6157-PRSN-PRVID-03                           235 DOVH
                                            PIC X(26)       VALUE ZERO. 235 DOVH
           05  DBLINK-R6201-ACH-EFCTV-KEY.                              235 DOVH
               10  DBLINK-R6201-ACH-EFCTV-01                            235 DOVH
                                            PIC S9(7)       COMP-3      235 DOVH
                       VALUE ZERO.                                      235 DOVH
               10  DBLINK-R6201-ACH-EFCTV-02                            235 DOVH
                                            PIC X(1)        VALUE SPACE.235 DOVH
               10  DBLINK-R6201-ACH-EFCTV-03                            235 DOVH
                                            PIC X(2)        VALUE SPACE.235 DOVH
               10  DBLINK-R6201-ACH-EFCTV-04                            235 DOVH
                                            PIC X(10)       VALUE ZERO. 235 DOVH
           05  DBLINK-S-INDX-AGNT-NAME-KEY.                             240 DOVH
             08  DBLINK-S-INDX-AGNT-NAME-KEY-M.                         240 DOVH
             09  DBLINK-S-INDX-AGNT-NAME-KEY-U.                         240 DOVH
UNVCD          10  FILLER                   PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-AGNT-NAME-01                           240 DOVH
                                            PIC X(35)       VALUE SPACE.240 DOVH
             09  DBLINK-S-INDX-AGNT-NAME-KEY-S.                         240 DOVH
               10  DBLINK-S-INDX-AGNT-NAME-02                           240 DOVH
                                            PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-INDX-AGNT-NAME-F    PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-BNK-NAME-KEY.                              240 DOVH
             08  DBLINK-S-INDX-BNK-NAME-KEY-M.                          240 DOVH
             09  DBLINK-S-INDX-BNK-NAME-KEY-U.                          240 DOVH
UNVCD          10  FILLER                   PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-BNK-NAME-01                            240 DOVH
                                            PIC X(35)       VALUE SPACE.240 DOVH
             09  DBLINK-S-INDX-BNK-NAME-KEY-S.                          240 DOVH
               10  DBLINK-S-INDX-BNK-NAME-02                            240 DOVH
                                            PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-INDX-BNK-NAME-F     PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-BUYR-NAME-KEY.                             240 DOVH
             08  DBLINK-S-INDX-BUYR-NAME-KEY-M.                         240 DOVH
             09  DBLINK-S-INDX-BUYR-NAME-KEY-U.                         240 DOVH
UNVCD          10  FILLER                   PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-BUYR-NAME-01                           240 DOVH
                                            PIC X(35)       VALUE SPACE.240 DOVH
             09  DBLINK-S-INDX-BUYR-NAME-KEY-S.                         240 DOVH
               10  DBLINK-S-INDX-BUYR-NAME-02                           240 DOVH
                                            PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-INDX-BUYR-NAME-F    PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-CUST-NAME-KEY.                             240 DOVH
             08  DBLINK-S-INDX-CUST-NAME-KEY-M.                         240 DOVH
             09  DBLINK-S-INDX-CUST-NAME-KEY-U.                         240 DOVH
UNVCD          10  FILLER                   PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-CUST-NAME-01                           240 DOVH
                                            PIC X(35)       VALUE SPACE.240 DOVH
             09  DBLINK-S-INDX-CUST-NAME-KEY-S.                         240 DOVH
               10  DBLINK-S-INDX-CUST-NAME-02                           240 DOVH
                                            PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-INDX-CUST-NAME-F    PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-FNCL-NAME-KEY.                             240 DOVH
             08  DBLINK-S-INDX-FNCL-NAME-KEY-M.                         240 DOVH
             09  DBLINK-S-INDX-FNCL-NAME-KEY-U.                         240 DOVH
UNVCD          10  FILLER                   PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-FNCL-NAME-01                           240 DOVH
                                            PIC X(35)       VALUE SPACE.240 DOVH
             09  DBLINK-S-INDX-FNCL-NAME-KEY-S.                         240 DOVH
               10  DBLINK-S-INDX-FNCL-NAME-02                           240 DOVH
                                            PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-INDX-FNCL-NAME-F    PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-TRVL-NAME-KEY.                             240 DOVH
             08  DBLINK-S-INDX-TRVL-NAME-KEY-M.                         240 DOVH
             09  DBLINK-S-INDX-TRVL-NAME-KEY-U.                         240 DOVH
UNVCD          10  FILLER                   PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-TRVL-NAME-01                           240 DOVH
                                            PIC X(35)       VALUE SPACE.240 DOVH
             09  DBLINK-S-INDX-TRVL-NAME-KEY-S.                         240 DOVH
               10  DBLINK-S-INDX-TRVL-NAME-02                           240 DOVH
                                            PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-INDX-TRVL-NAME-F    PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-VNDR-NAME-KEY.                             240 DOVH
             08  DBLINK-S-INDX-VNDR-NAME-KEY-M.                         240 DOVH
             09  DBLINK-S-INDX-VNDR-NAME-KEY-U.                         240 DOVH
UNVCD          10  FILLER                   PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-VNDR-NAME-01                           240 DOVH
                                            PIC X(35)       VALUE SPACE.240 DOVH
             09  DBLINK-S-INDX-VNDR-NAME-KEY-S.                         240 DOVH
               10  DBLINK-S-INDX-VNDR-NAME-02                           240 DOVH
                                            PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-INDX-VNDR-NAME-F    PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-ENTY-KEY.                                  240 DOVH
             08  DBLINK-S-INDX-ENTY-KEY-M.                              240 DOVH
             09  DBLINK-S-INDX-ENTY-KEY-U.                              240 DOVH
UNVCD          10  FILLER                   PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-ENTY-01    PIC S9(7)       COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
           05  DBLINK-WS-S-INDX-ENTY-KEY.                               240 DOVH
UNVCD          10  FILLER                   PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-WS-S-INDX-ENTY-01 PIC S9(7)       COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
           05  DBLINK-S-INDX-ENTY-ADR1-KEY.                             240 DOVH
             08  DBLINK-S-INDX-ENTY-ADR1-KEY-M.                         240 DOVH
             09  DBLINK-S-INDX-ENTY-ADR1-KEY-U.                         240 DOVH
               10  DBLINK-S-INDX-ENTY-ADR1-01                           240 DOVH
                                            PIC X(35)       VALUE SPACE.240 DOVH
             09  DBLINK-S-INDX-ENTY-ADR1-KEY-S.                         240 DOVH
               10  DBLINK-S-INDX-ENTY-ADR1-02                           240 DOVH
                                            PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-INDX-ENTY-ID-KEY.                               240 DOVH
             08  DBLINK-S-INDX-ENTY-ID-KEY-M.                           240 DOVH
             09  DBLINK-S-INDX-ENTY-ID-KEY-U.                           240 DOVH
UNVCD          10  FILLER                   PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-ENTY-ID-01 PIC X(1)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-ENTY-ID-02 PIC X(9)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-ENTY-NAME-KEY.                             240 DOVH
             08  DBLINK-S-INDX-ENTY-NAME-KEY-M.                         240 DOVH
             09  DBLINK-S-INDX-ENTY-NAME-KEY-U.                         240 DOVH
UNVCD          10  FILLER                   PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-ENTY-NAME-01                           240 DOVH
                                            PIC X(35)       VALUE SPACE.240 DOVH
             09  DBLINK-S-INDX-ENTY-NAME-KEY-S.                         240 DOVH
               10  DBLINK-S-INDX-ENTY-NAME-02                           240 DOVH
                                            PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-INDX-ENTY-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-ENTY-ADR1-F    PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-ENTY-ID-F      PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-ENTY-NAME-F    PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-R6117-PRSN-KEY.                                   235 DOVH
               10  DBLINK-R6117-PRSN-01     PIC S9(7)       COMP-3      235 DOVH
                       VALUE ZERO.                                      235 DOVH
           05  DBLINK-S-6001-6117-KEY.                                  240 DOVH
             08  DBLINK-S-6001-6117-KEY-O.                              240 DOVH
               10  DBLINK-S-6001-6117-01    PIC X(2)        VALUE SPACE.240 DOVH
             08  DBLINK-S-6001-6117-KEY-M.                              240 DOVH
             09  DBLINK-S-6001-6117-KEY-S.                              240 DOVH
               10  DBLINK-S-6001-6117-02    PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-INDX-SOC-SCRTY-KEY.                             240 DOVH
             08  DBLINK-S-INDX-SOC-SCRTY-KEY-M.                         240 DOVH
             09  DBLINK-S-INDX-SOC-SCRTY-KEY-U.                         240 DOVH
               10  DBLINK-S-INDX-SOC-SCRTY-01                           240 DOVH
                                            PIC X(1)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-SOC-SCRTY-02                           240 DOVH
                                            PIC X(9)        VALUE SPACE.240 DOVH
             09  DBLINK-S-INDX-SOC-SCRTY-KEY-S.                         240 DOVH
               10  DBLINK-S-INDX-SOC-SCRTY-03                           240 DOVH
                                            PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-6313-6156-KEY.                                  240 DOVH
             08  DBLINK-S-6313-6156-KEY-O.                              240 DOVH
               10  DBLINK-S-6313-6156-01    PIC S9(7)       COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
               10  DBLINK-S-6313-6156-02    PIC X(10)       VALUE ZERO. 240 DOVH
               10  DBLINK-S-6313-6156-03    PIC X(26)       VALUE ZERO. 240 DOVH
             08  DBLINK-S-6313-6156-KEY-M.                              240 DOVH
             09  DBLINK-S-6313-6156-KEY-S.                              240 DOVH
               10  DBLINK-S-6313-6156-04    PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-INDX-PRSN-NAME-KEY.                             240 DOVH
             08  DBLINK-S-INDX-PRSN-NAME-KEY-M.                         240 DOVH
             09  DBLINK-S-INDX-PRSN-NAME-KEY-U.                         240 DOVH
UNVCD          10  FILLER                   PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-PRSN-NAME-01                           240 DOVH
                                            PIC X(35)       VALUE SPACE.240 DOVH
             09  DBLINK-S-INDX-PRSN-NAME-KEY-S.                         240 DOVH
               10  DBLINK-S-INDX-PRSN-NAME-02                           240 DOVH
                                            PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-6313-6157-KEY.                                  240 DOVH
             08  DBLINK-S-6313-6157-KEY-O.                              240 DOVH
               10  DBLINK-S-6313-6157-01    PIC S9(7)       COMP-3      240 DOVH
                       VALUE ZERO.                                      240 DOVH
               10  DBLINK-S-6313-6157-02    PIC X(10)       VALUE ZERO. 240 DOVH
               10  DBLINK-S-6313-6157-03    PIC X(26)       VALUE ZERO. 240 DOVH
             08  DBLINK-S-6313-6157-KEY-M.                              240 DOVH
             09  DBLINK-S-6313-6157-KEY-S.                              240 DOVH
               10  DBLINK-S-6313-6157-04    PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-INDX-PRSN-ID-KEY.                               240 DOVH
             08  DBLINK-S-INDX-PRSN-ID-KEY-M.                           240 DOVH
             09  DBLINK-S-INDX-PRSN-ID-KEY-U.                           240 DOVH
UNVCD          10  FILLER                   PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-PRSN-ID-01 PIC X(1)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-PRSN-ID-02 PIC X(9)        VALUE SPACE.240 DOVH
           05  DBLINK-S-6353-6352-KEY.                                  240 DOVH
             08  DBLINK-S-6353-6352-KEY-O.                              240 DOVH
               10  DBLINK-S-6353-6352-01    PIC X(1)        VALUE SPACE.240 DOVH
               10  DBLINK-S-6353-6352-02    PIC X(9)        VALUE SPACE.240 DOVH
               10  DBLINK-S-6353-6352-03    PIC X(10)       VALUE ZERO. 240 DOVH
               10  DBLINK-S-6353-6352-04    PIC X(26)       VALUE ZERO. 240 DOVH
             08  DBLINK-S-6353-6352-KEY-M.                              240 DOVH
             09  DBLINK-S-6353-6352-KEY-S.                              240 DOVH
               10  DBLINK-S-6353-6352-05    PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-6353-6186-KEY.                                  240 DOVH
             08  DBLINK-S-6353-6186-KEY-O.                              240 DOVH
               10  DBLINK-S-6353-6186-01    PIC X(1)        VALUE SPACE.240 DOVH
               10  DBLINK-S-6353-6186-02    PIC X(9)        VALUE SPACE.240 DOVH
               10  DBLINK-S-6353-6186-03    PIC X(10)       VALUE ZERO. 240 DOVH
               10  DBLINK-S-6353-6186-04    PIC X(26)       VALUE ZERO. 240 DOVH
             08  DBLINK-S-6353-6186-KEY-M.                              240 DOVH
             09  DBLINK-S-6353-6186-KEY-S.                              240 DOVH
               10  DBLINK-S-6353-6186-05    PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-R6311-ENTY-KEY.                                   235 DOVH
               10  DBLINK-R6311-ENTY-01     PIC X(1)        VALUE SPACE.235 DOVH
               10  DBLINK-R6311-ENTY-02     PIC X(9)        VALUE SPACE.235 DOVH
           05  DBLINK-S-6001-6117-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-ACH-KEY.                                   240 DOVH
             08  DBLINK-S-INDX-ACH-KEY-M.                               240 DOVH
             09  DBLINK-S-INDX-ACH-KEY-U.                               240 DOVH
UNVCD          10  FILLER                   PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-ACH-01     PIC X(1)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-ACH-02     PIC X(9)        VALUE SPACE.240 DOVH
           05  DBLINK-S-6152-6139-KEY.                                  240 DOVH
             08  DBLINK-S-6152-6139-KEY-O.                              240 DOVH
               10  DBLINK-S-6152-6139-01    PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-S-6152-6139-02    PIC X(10)       VALUE SPACE.240 DOVH
             08  DBLINK-S-6152-6139-KEY-M.                              240 DOVH
             09  DBLINK-S-6152-6139-KEY-S.                              240 DOVH
               10  DBLINK-S-6152-6139-03    PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-6152-6139-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-PRSN-ADR1-KEY.                             240 DOVH
             08  DBLINK-S-INDX-PRSN-ADR1-KEY-M.                         240 DOVH
             09  DBLINK-S-INDX-PRSN-ADR1-KEY-U.                         240 DOVH
               10  DBLINK-S-INDX-PRSN-ADR1-01                           240 DOVH
                                            PIC X(35)       VALUE SPACE.240 DOVH
             09  DBLINK-S-INDX-PRSN-ADR1-KEY-S.                         240 DOVH
               10  DBLINK-S-INDX-PRSN-ADR1-02                           240 DOVH
                                            PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-6152-6185-KEY.                                  240 DOVH
             08  DBLINK-S-6152-6185-KEY-O.                              240 DOVH
               10  DBLINK-S-6152-6185-01    PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-S-6152-6185-02    PIC X(10)       VALUE SPACE.240 DOVH
             08  DBLINK-S-6152-6185-KEY-M.                              240 DOVH
             09  DBLINK-S-6152-6185-KEY-S.                              240 DOVH
               10  DBLINK-S-6152-6185-03    PIC X(26)       VALUE ZERO. 240 DOVH
           05  DBLINK-S-6152-6185-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-R6200-ACH-DATA-KEY.                               235 DOVH
               10  DBLINK-R6200-ACH-DATA-01 PIC S9(7)       COMP-3      235 DOVH
                       VALUE ZERO.                                      235 DOVH
               10  DBLINK-R6200-ACH-DATA-02 PIC X(1)        VALUE SPACE.235 DOVH
