      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FTAWD28                              04/24/00  13:20  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FTAWD28.
           02  SAVE-DBKEY-ID-4400-TA28                  PIC X(05).
           02  SAVE-DBKEY-ID-4401-TA28                  PIC X(08).
           02  EVENT-BAL-TA28                 COMP-3    PIC S9(10)V99.
           02  4401-END-FLAG-TA28                       PIC X(1).
           02  GREG-DATE-TA28.
               03  YEAR-DATE-TA28                       PIC 9(2).
               03  MONTH-DATE-TA28                      PIC 9(2).
               03  DAY-DATE-TA28                        PIC 9(2).
           02  SHORT-DESC-TA28                          PIC X(10).
           02  NMRC-SHORT-DESC-TA28 REDEFINES
               SHORT-DESC-TA28                          PIC 9(10).
           02  WORK-DATE-TA28.
               03  WORK-MONTH-DATE-TA28                 PIC X(2).
               03  WORK-DAY-DATE-TA28                   PIC X(2).
               03  WORK-YEAR-DATE-TA28                  PIC X(2).
           02  SAVE-START-DATE-TA28           COMP-3    PIC S9(8).
