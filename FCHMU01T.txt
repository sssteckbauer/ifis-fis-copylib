       01  MAP-CONTROL.

    ***Created by Convert/DC version V8R03 on 12/11/00 at 12:00***

           02  FCHMU01-MAP-CONTROL.
           03  FCHMU01T.
           04  WK01-DCXMAPL-PARMS.
               10  WK01-MAPNAME                  PIC X(8)
                                                 VALUE 'FCHMU01I'.
               10  WK01-IKE-LENGTH               PIC S9(8)  COMP
                                                 VALUE 11549.
               10  WK01-TINA-LENGTH              PIC S9(4)  COMP
                                                 VALUE 903.
           05  WK01-ATTRIBUTES                   PIC X(60)
                                                 VALUE SPACE.
           05  WK01-ATTRIBUTES-ERROR             PIC X(60) VALUE
               'U  BIF'.
           05  WK01-CURSOR-DFLD                  PIC X(36)
                                                 VALUE SPACES.
           05  WK01-CURSOR-DFLD-QUAL             PIC X(30)
                                                 VALUE SPACES.
           05  WK01-ROW                          PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-COLUMN                       PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-ERROR-MSG                    PIC X(80)
                                                 VALUE SPACES.
           05  WK01-MAP-DATA-LEN                 PIC S9(6)
                                                 VALUE ZERO.
           05  WK01-1ST-FLAG                     PIC X(1)
                                                 VALUE 'Y'.
           05  WK01-FLAG-OUTPUT-DATA             PIC X(1)
                                                 VALUE 'Y'.
      *    05  WK01-STATUS-RETURN-FLAG           PIC X(1)
      *                                          VALUE SPACES.
           05  WK01-ALL-BUT-FLAG                 PIC X(1)
                                                 VALUE SPACES.
           05  WK01-FLAG-ALL-FIELDS-CHANGED      PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-CHANGED      PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERROR        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERROR        PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.

           05  WK01-VARDATA                      PIC X(11880)
                                                 VALUE SPACES.
           05  FILLER                            PIC X(100)
                                                 VALUE SPACES.
           04  WK01-ELEMENT-AREA.

               10  WK01-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-6001-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00       PIC X(35)
                                               VALUE SPACES.
               10  WK01-DATE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-HH-MM-TIME-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00       PIC X(4)
                                               VALUE SPACES.
               10  WK01-PM-FLAG-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-USER-ID-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID       PIC X(32)
                                               VALUE SPACES.
               10  WK01-RSPNS-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-FUNC-DESCRIPTION-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION       PIC X(28)
                                               VALUE SPACES.
               10  WK01-STATUS-CODE-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-CODE-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-CH01       PIC X(1)
                                               VALUE SPACES.
               10  WK01-CODE-4000-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4000-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4000-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4000-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4000-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4000-CH01       PIC X(1)
                                               VALUE SPACES.
               10  WK01-TITLE-4076-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4076-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4076-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4076-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4076-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4076-CH01       PIC X(35)
                                               VALUE SPACES.
               10  WK01-DATE-4076-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4076-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4076-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4076-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4076-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4076-CH01       PIC X(6)
                                               VALUE SPACES.
               10  WK01-CHNG-DATE-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CHNG-DATE-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CHNG-DATE-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CHNG-DATE-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CHNG-DATE-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CHNG-DATE-CH01       PIC X(6)
                                               VALUE SPACES.
               10  WK01-DESC-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-CH01       PIC X(8)
                                               VALUE SPACES.
               10  WK01-DATE-4076-CH0A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4076-CH0A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4076-CH0A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4076-CH0A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4076-CH0A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4076-CH0A       PIC X(6)
                                               VALUE SPACES.
               10  WK01-DATE-4000-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4000-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4000-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4000-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4000-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4000-CH01       PIC X(6)
                                               VALUE SPACES.
               10  WK01-STAMP-4076-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STAMP-4076-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STAMP-4076-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STAMP-4076-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STAMP-4076-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STAMP-4076-CH01       PIC X(4)
                                               VALUE SPACES.
               10  WK01-PM-FLAG-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-CH01       PIC X(1)
                                               VALUE SPACES.
               10  WK01-MTHD-4076-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MTHD-4076-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MTHD-4076-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MTHD-4076-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MTHD-4076-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MTHD-4076-CH01       PIC X(1)
                                               VALUE SPACES.
               10  WK01-YR-START-PRD-4076-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-YR-START-PRD-4076-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-YR-START-PRD-4076-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-YR-START-PRD-4076-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-YR-START-PRD-4076-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-YR-START-PRD-4076-CH01       PIC X(4)
                                               VALUE SPACES.
               10  WK01-YR-END-PRD-4076-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-YR-END-PRD-4076-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-YR-END-PRD-4076-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-YR-END-PRD-4076-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-YR-END-PRD-4076-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-YR-END-PRD-4076-CH01       PIC X(4)
                                               VALUE SPACES.
               10  WK01-INDX-BDGT-CNTRL-4076-C-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-BDGT-CNTRL-4076-C-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-BDGT-CNTRL-4076-C-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-BDGT-CNTRL-4076-C-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-BDGT-CNTRL-4076-C-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-BDGT-CNTRL-4076-C       PIC X(1)
                                               VALUE SPACES.
               10  WK01-INDX-BDGT-DESC-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-BDGT-DESC-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-BDGT-DESC-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-BDGT-DESC-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-BDGT-DESC-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-BDGT-DESC-CH01       PIC X(30)
                                               VALUE SPACES.
               10  WK01-BDGT-CNTRL-4076-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-CNTRL-4076-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-CNTRL-4076-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-CNTRL-4076-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-CNTRL-4076-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-CNTRL-4076-CH01       PIC X(1)
                                               VALUE SPACES.
               10  WK01-BDGT-DESC-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-DESC-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-DESC-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-DESC-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-DESC-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-DESC-CH01       PIC X(30)
                                               VALUE SPACES.
               10  WK01-BDGT-CNTRL-4076-CH0A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-CNTRL-4076-CH0A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-CNTRL-4076-CH0A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-CNTRL-4076-CH0A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-CNTRL-4076-CH0A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-CNTRL-4076-CH0A       PIC X(1)
                                               VALUE SPACES.
               10  WK01-BDGT-DESC-CH0A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-DESC-CH0A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-DESC-CH0A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-DESC-CH0A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-DESC-CH0A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-DESC-CH0A       PIC X(30)
                                               VALUE SPACES.
               10  WK01-BDGT-CNTRL-4076-CH0B-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-CNTRL-4076-CH0B-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-CNTRL-4076-CH0B-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-CNTRL-4076-CH0B-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-CNTRL-4076-CH0B-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-CNTRL-4076-CH0B       PIC X(1)
                                               VALUE SPACES.
               10  WK01-BDGT-DESC-CH0B-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-DESC-CH0B-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-DESC-CH0B-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-DESC-CH0B-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-DESC-CH0B-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-DESC-CH0B       PIC X(30)
                                               VALUE SPACES.
               10  WK01-BDGT-CNTRL-4076-CH0C-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-CNTRL-4076-CH0C-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-CNTRL-4076-CH0C-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-CNTRL-4076-CH0C-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-CNTRL-4076-CH0C-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-CNTRL-4076-CH0C       PIC X(1)
                                               VALUE SPACES.
               10  WK01-BDGT-DESC-CH0C-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-DESC-CH0C-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-DESC-CH0C-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-DESC-CH0C-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-DESC-CH0C-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BDGT-DESC-CH0C       PIC X(30)
                                               VALUE SPACES.
               10  WK01-PRD-CODE-4076-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PRD-CODE-4076-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PRD-CODE-4076-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PRD-CODE-4076-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PRD-CODE-4076-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PRD-CODE-4076-CH01       PIC X(1)
                                               VALUE SPACES.
               10  WK01-PRD-DESC-4012-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PRD-DESC-4012-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PRD-DESC-4012-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PRD-DESC-4012-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PRD-DESC-4012-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PRD-DESC-4012-CH01       PIC X(35)
                                               VALUE SPACES.
               10  WK01-SVRTY-CODE-4076-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SVRTY-CODE-4076-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SVRTY-CODE-4076-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SVRTY-CODE-4076-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SVRTY-CODE-4076-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SVRTY-CODE-4076-CH01       PIC X(1)
                                               VALUE SPACES.
               10  WK01-SVRTY-DESC-4012-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SVRTY-DESC-4012-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SVRTY-DESC-4012-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SVRTY-DESC-4012-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SVRTY-DESC-4012-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SVRTY-DESC-4012-CH01       PIC X(35)
                                               VALUE SPACES.
               10  WK01-LDGR-RULE-4076-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LDGR-RULE-4076-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LDGR-RULE-4076-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LDGR-RULE-4076-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LDGR-RULE-4076-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LDGR-RULE-4076-CH01       PIC X(4)
                                               VALUE SPACES.
               10  WK01-CLASS-DESC-4181-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CLASS-DESC-4181-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CLASS-DESC-4181-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CLASS-DESC-4181-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CLASS-DESC-4181-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CLASS-DESC-4181-CH01       PIC X(35)
                                               VALUE SPACES.
               10  WK01-IND-4076-CH01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-4076-CH01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-4076-CH01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-4076-CH01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-4076-CH01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-4076-CH01       PIC X(1)
                                               VALUE SPACES.
               10  WK01-ZZMESSAGE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE       PIC X(159)
                                               VALUE SPACES.
               10  WK01-PASSED-ONE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE       PIC X(32)
                                               VALUE SPACES.
               10  WK01-MAP-RESPONSE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-TEXT-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00       PIC X(14)
                                               VALUE SPACES.
