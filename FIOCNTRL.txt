      *----------------------------------------------------------------*
      * LAYOUT FOR : IO-MOD-CONTROL USED TO PASS PARAMETERS BETWEEN    *
      *              BATCH PROGRAMS AND IO MODULES                     *
      *----------------------------------------------------------------*
       01  IO-MOD-CONTROL.
           03  IO-MOD-COMMAND                  PIC X(6).
           03  IO-MOD-RESPONSE-CODE            PIC X(1).
               88  IO-MOD-OK                     VALUE '0'.
               88  IO-MOD-EOF                    VALUE '1'.
               88  IO-MOD-WRONG-FILE             VALUE '6'.
