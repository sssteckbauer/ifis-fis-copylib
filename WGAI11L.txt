      ******************************************************************
      *    **** COPYBOOK WGAI11L - USED FOR LOADJLH WEB SERVICE ****   *
      *                                                                *
      *    WEB/CICS COMMAREA TO LOAD THE JLH JOURNAL HEADER TABLE      *
      ******************************************************************

      ******************************************************************
      *    AFTER CHANGES ARE MADE TO THIS COPYBOOK, IT MUST BE COPIED  *
      *    INTO A WEBSPHERE DEVELOPER PROJECT FOLDER, FROM WHICH THE   *
      *    WEB SERVICE INTERFACE PROGRAM, WSDL, & WSBIND ARE GENERATED.*
      ******************************************************************

       01  LOADJLH-PARM-AREA.

         02  LOADJLH-CHANNEL-HEADER.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND AND OUTBOUND  *
      *      ***********************************************************

           05  LOADJLH-CHANNEL-ID             PIC X(08).
      *        *********************************************************
      *        * MUST BE 'LOADJLH '                                    *
      *        *********************************************************

           05  LOADJLH-VERS-RESP-CD           PIC X(08).
      *        *********************************************************
      *        * INCOMING: COMMAREA VERSION = 01.00.00                 *
      *        * OUTGOING: RETURN RESPONSE FOR WFGA(WEB/IFIS GA SYSTEM)*
      *        *********************************************************
               88  LOADJLH-RESPONSE-SUCCESSFUL         VALUE 'WFGA000I'.
               88  LOADJLH-RESPONSE-ERROR              VALUE 'WFGA999E'.

         02  LOADJLH-INPUT-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND ONLY          *
      *      ***********************************************************

           05  LOADJLH-FUNCTION               PIC X(08).
      *        *********************************************************
      *        * ADD      = INSERT JLH ROW                             *
      *        * CHGXXXXX = UPDATE JLH FOR XXXXX                       *
      *        *********************************************************

           05  LOADJLH-USER-CD                PIC X(08).
NOSEND*****05  LOADJLH-LAST-ACTVY-DT          PIC X(05).
NOSEND*****05  LOADJLH-UNVRS-CD               PIC X(02).
           05  LOADJLH-DOC-NBR                PIC X(08).
NOSEND*****05  LOADJLH-RVRSL-IND              PIC X(01).
NOSEND*****05  LOADJLH-ACTVY-DT               PIC X(10).
           05  LOADJLH-DOC-DESC               PIC X(35).
           05  LOADJLH-DOC-AMT                PIC +9(10).99.
NOSEND*****05  LOADJLH-HDR-ERROR-IND          PIC X(01).
NOSEND*****05  LOADJLH-HDR-CNTR-ACCT          PIC +9(4).
NOSEND*****05  LOADJLH-APRVL-IND              PIC X(01).
NOSEND*****05  LOADJLH-PSTNG-IND              PIC X(01).
           05  LOADJLH-TRANS-DT               PIC X(10).
      *        *********************************************************
      *        * DATE FORMAT = YYYY-MM-DD OR MM/DD/YYYY                *
      *        *********************************************************

NOSEND*****05  LOADJLH-CMPLT-IND              PIC X(01).
NOSEND*****05  LOADJLH-DSTBN-IND              PIC X(01).
NOSEND*****05  LOADJLH-UPDT-ACTVY-DT          PIC X(10).
NOSEND*****05  LOADJLH-UPDT-TIME-STMP         PIC X(08).
NOSEND*****05  LOADJLH-JV-DEBIT-AMT           PIC +9(10).99.
NOSEND*****05  LOADJLH-JV-CRDT-AMT            PIC +9(10).99.
NOSEND*****05  LOADJLH-SUBSYSTEM-ID           PIC X(08).
NOSEND*****05  LOADJLH-ACTG-PRD               PIC X(02).
NOSEND*****05  LOADJLH-SUB-DOC-TYP            PIC X(03).
NOSEND*****05  LOADJLH-REEDIT-STATUS          PIC X(01).
NOSEND*****05  LOADJLH-AJL-SETF               PIC X(01).
NOSEND*****05  LOADJLH-FK-AJL-AUTO-JRNLID     PIC X(03).
NOSEND*****05  LOADJLH-AJL-SET-TS             PIC X(26).
NOSEND*****05  LOADJLH-IDX-SETF               PIC X(01).

           05  LOADJLH-APV-TPLT-CD            PIC X(03).

         02  LOADJLH-RETURN-STATUS-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: OUTBOUND ONLY         *
      *      ***********************************************************

           05  LOADJLH-RETURN-STATUS-CODE     PIC X(01).
      *        *********************************************************
      *        * S = SUCCESSFUL, E = DATA ERROR(S), F = FATAL ERROR    *
      *        *********************************************************

           05  LOADJLH-RETURN-MSG-COUNT       PIC 9(01).
      *        *********************************************************
      *        * 1 = NUMBER OF RETURN-MSG-AREA(S)                      *
      *        *********************************************************

           05  LOADJLH-RETURN-MSG-AREA.
      *        *********************************************************
      *        * FOR STATUS 'S', SUCCESS MESSAGE NUMBER & TEXT         *
      *        * FOR STATUS 'E', ERROR MSG NUMBER, FIELD & TEXT        *
      *        * FOR STATUS 'F', FATAL SQLCODE, CURR-IO-NUM WITH       *
      *        *                 PARAGRAPH NAME, & ASSOC DB2 TABLE     *
      *        *********************************************************
               10  LOADJLH-RETURN-MSG-NUM     PIC X(06).
               10  LOADJLH-RETURN-MSG-FIELD   PIC X(30).
               10  LOADJLH-RETURN-MSG-TEXT    PIC X(40).

