      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM28                              04/24/00  12:24  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM28.
           02  ACTN-CODE-GROUP-CH28.
               03  ACTN-CODE-CH28           OCCURS 12   PIC X(1).
           02  DATA-CODE-4038-CH28          OCCURS 12   PIC X(6).
           02  SHORT-DESC-4038-CH28         OCCURS 12   PIC X(10).
           02  LONG-DESC-4038-CH28          OCCURS 12   PIC X(35).
           02  RQRD-DATA-FLAG-4038-CH28     OCCURS 12   PIC X(1).
           02  STD-TEXT-CH28                OCCURS 12   PIC X(1).
