000100***************************************************************   00010000
000200**  STDWS - BEGIN WORKING STORAGE FOR STANDARD CONTROL REPORT     00020000
000300**                                                                00030004
000400**  MODIFICATIONS:                                                00040003
000500**                                                                00050003
000600**  FI054 - DMR - 10/05/94 - ADDED 7/24 COMMIT PARM AND COUNTER   00060004
000700***************************************************************   00070000
000800                                                                  00080000
000900***************************************************************   00090000
001000* VARIABLES WHICH MUST BE SET BEFORE 99999-STD-CRPT-BEGIN         00100000
001100* IS PERFORMED                                                    00110000
001200***************************************************************   00120000
001300 01  STD-WS-CRPT-DESCR-1            PIC X(60) VALUE ' '.          00130000
001400 01  STD-WS-PROGRAM-NAME            PIC X(21) VALUE ' '.          00140000
001500* STD-WS-CRPT-RETAIN-DAYS MAY BE SET                              00150000
001600* MAY BE SET BEFORE 99999-STD-CRPT-BEGIN                          00160000
001700*   IF > 0, CONTROL REPORT RETENTION DATA IS PRINTED,             00170000
001800*   IF = 0, NO CONTROL REPORT RETENTION DATA IS PRINTED.          00180000
001900 01  STD-WS-CRPT-RETAIN-DAYS        PIC 9999 VALUE ZEROS.         00190000
002000                                                                  00200000
002100                                                                  00210000
002200***************************************************************   00220000
002300* VARIABLES WHICH ARE USED BY THE 99999-STD-CRPT- PROCESSES       00230000
002400***************************************************************   00240000
002500 01  STD-WS-CRPT-DDNAME             PIC X(21) VALUE ' '.          00250000
002600 01  STD-WS-CRPT-DESCR-2            PIC X(60) VALUE ' '.          00260000
002700 01  STD-WS-CRPT-DML-SEQUENCE       PIC 99999999.                 00270000
002800 01  STD-WS-CRPT-LINE               PIC X(132).                   00280000
002900 01  STD-WS-CRPT-LINE-CNT           PIC 9999 VALUE 0.             00290000
003000 01  STD-WS-CRPT-LINE-CNT-MAX       PIC 9999 VALUE 56.            00300000
003100 01  STD-WS-CRPT-PAGE-CNT           PIC 99999.                    00310000
003200 01  STD-WS-CRPT-RETENTION          PIC X(31) VALUE ' '.          00320000
003300 01  STD-WS-CRPT-RUN-DATE           PIC X(08) VALUE ' '.          00330000
003400 01  STD-WS-CRPT-RUN-TIME           PIC X(08) VALUE ' '.          00340000
003500                                                                  00350000
003600***************************************************************   00360000
003700* VARIABLES WHICH ARE USED BY THE STD-RPT1- PROCESSES             00370000
003800***************************************************************   00380000
003900 01  STD-WS-RPT1-DDNAME             PIC X(21) VALUE ' '.          00390000
004000 01  STD-WS-RPT1-DESCR-1            PIC X(60) VALUE ' '.          00400000
004100 01  STD-WS-RPT1-DESCR-2            PIC X(60) VALUE ' '.          00410000
004200 01  STD-WS-RPT1-LINE               PIC X(132).                   00420000
004300 01  STD-WS-RPT1-LINE-CNT           PIC 9999.                     00430000
004400 01  STD-WS-RPT1-LINE-CNT-MAX       PIC 9999 VALUE 56.            00440000
004500 01  STD-WS-RPT1-PAGE-CNT           PIC 9999.                     00450000
004600 01  STD-WS-RPT1-RETAIN-DAYS        PIC 9999 VALUE 0.             00460000
004700 01  STD-WS-RPT1-RETENTION          PIC X(31) VALUE ' '.          00470000
004800 01  STD-WS-RPT1-RUN-DATE           PIC X(08) VALUE ' '.          00480000
004900 01  STD-WS-RPT1-RUN-TIME           PIC X(08) VALUE ' '.          00490000
005000                                                                  00500000
005100***************************************************************   00510000
005200* VARIABLES AND CONSTANTS WHICH ARE USED BY THE BOTH THE          00520000
005300* 99999-STD-CRPT- AND STD-RPT1- PROCESSES                         00530000
005400***************************************************************   00540000
005500 01  STD-WS-PAGE-CNT                PIC 99999.                    00550000
005600 01  STD-WS-DDNAME                  PIC X(21) VALUE ' '.          00560000
005700 01  STD-WS-DESCR-1                 PIC X(60) VALUE ' '.          00570000
005800 01  STD-WS-DESCR-2                 PIC X(60) VALUE ' '.          00580000
005900 01  STD-WS-LINE-CNT                PIC 9999 VALUE 0.             00590000
006000 01  STD-WS-LINE-CNT-MAX            PIC 9999 VALUE 56.            00600000
006100 01  STD-WS-PROGRAM-FAILED          PIC X(01) VALUE 'N'.          00610000
006200 01  STD-WS-RETAIN-DAYS             PIC 9999 VALUE ZEROS.         00620000
006300 01  STD-WS-RETENTION               PIC X(31) VALUE ' '.          00630000
006400 01  STD-WS-RUN-DATE                PIC X(08) VALUE ' '.          00640000
006500 01  STD-WS-RUN-TIME                PIC X(08) VALUE ' '.          00650000
006600 01  STD-WS-SAVE-LINE               PIC X(132).                   00660000
006700 01  STD-WS-FIELD-TO-CENTER         PIC X(132).                   00670000
006800 01  STD-WS-FIELD-TO-CENTER-LEN     PIC 99999.                    00680000
006900 01  STD-WS-FIELD-TO-CENTER-CNT     PIC 99999.                    00690000
007000 01  STD-WS-FIELD-TO-CENTER-PTR     PIC 99999.                    00700000
007100 01  STD-WS-FIELD-CENTERED          PIC X(132).                   00710000
007200 01  STD-WS-DDD                     PIC 999.                      00720000
007300                                                                  00730000
007400 01  STD-WS-RESULT                  PIC 99.                       00740000
007500 01  STD-WS-LEAP-YEAR-IND           PIC 9.                        00750000
007600                                                                  00760000
007700 01  STD-WS-MONTH-NDX               PIC 99.                       00770000
007800 01  STD-WS-MONTHDAYS               PIC X(24) VALUE               00780000
007900        '310031303130313130313031'.                               00790000
008000 01  FILLER REDEFINES STD-WS-MONTHDAYS.                           00800000
008100     03  STD-WS-MONTH-DAYS          PIC 99 OCCURS 12.             00810000
008200                                                                  00820000
008300***************************************************************   00830000
008400* 7/24 COMMIT CONTROL PARAMETER AND COUNTER FOR USE BY PROGRAM    00840002
008500***************************************************************   00850000
008600                                                                  00860000
008700 01  STD-WS-COMMIT-CTR              PIC 9(04) VALUE ZEROS.        00870006
008800 01  STD-WS-COMMIT-PARAMETER.                                     00880000
008900     03  STD-WS-COMMIT-PARM-CTR-LIT PIC X(16).                    00890006
009000         88  STD-WS-COMMIT-PARM-LITERAL                           00900006
009100             VALUE '*COMMIT COUNTER '.                            00910006
009200     03  STD-WS-COMMIT-PARM-CTR     PIC 9(04).                    00920006
009300                                                                  00930000
009400***************************************************************   00940000
009500* DATE AND TIME VARIABLES AVAILABLE FOR USE BY PROGRAM            00950000
009600***************************************************************   00960000
009700                                                                  00970000
009800* STD-WS-DATE-CCYYMMDD, STD-WS-GREG-DATE, AND                     00980000
009900* STD-WS-JULIAN-DATE-CCYYDDD AND ASSOCIATED PARTS                 00990000
010000* OBTAINED BY PERFORMING 99999-STD-ACCEPT-DATE                    01000000
010100                                                                  01010000
010200* MAY ALSO BE SET BY PERFORMING 99999-CNV-CCYYMMDD-CCYYDDD        01020000
010300* AND 99999-CNV-CCYYDDD-CCYYMMDD                                  01030000
010400                                                                  01040000
010500 01  STD-WS-DATE-CCYYMMDD           PIC 99999999 VALUE ZEROS.     01050000
010600 01  FILLER REDEFINES STD-WS-DATE-CCYYMMDD.                       01060000
010700     03  STD-WS-DATE-CCYY           PIC 9999.                     01070006
010800     03  FILLER REDEFINES STD-WS-DATE-CCYY.                       01080000
010900         05  STD-WS-DATE-CC         PIC 99.                       01090006
011000         05  STD-WS-DATE-YY         PIC 99.                       01100006
011100     03  STD-WS-DATE-MM             PIC 99.                       01110000
011200     03  STD-WS-DATE-DD             PIC 99.                       01120000
011300 01  FILLER REDEFINES STD-WS-DATE-CCYYMMDD.                       01130000
011400     03  FILLER                     PIC 99.                       01140000
011500     03  STD-WS-DATE-YYMMDD         PIC 999999.                   01150000
011600                                                                  01160000
011700 01  STD-WS-GREG-DATE-MMDDYY.                                     01170000
011800     03  STD-WS-GREG-MM             PIC 99.                       01180000
011900     03  FILLER                     PIC X  VALUE '/'.             01190000
012000     03  STD-WS-GREG-DD             PIC 99.                       01200000
012100     03  FILLER                     PIC X  VALUE '/'.             01210000
012200     03  STD-WS-GREG-YY             PIC 99.                       01220000
012300                                                                  01230000
012400 01  STD-WS-JULIAN-CCYYDDD          PIC 9999999 VALUE ZEROS.      01240000
012500 01  FILLER REDEFINES STD-WS-JULIAN-CCYYDDD.                      01250000
012600     03  STD-WS-JULIAN-CCYY         PIC 9999.                     01260000
012700     03  FILLER REDEFINES STD-WS-JULIAN-CCYY.                     01270000
012800         05  STD-WS-JULIAN-CC       PIC 99.                       01280000
012900         05  STD-WS-JULIAN-YY       PIC 99.                       01290000
013000     03  STD-WS-JULIAN-DDD          PIC 999.                      01300000
013100                                                                  01310000
013200* STD-WS-TIME AND ASSOCIATED PARTS                                01320000
013300* OBTAINED BY PERFORMING 99999-STD-ACCEPT-TIME                    01330000
013400                                                                  01340000
013500 01  STD-WS-TIME-HHMMSSHS           PIC 99999999 VALUE ZEROS.     01350000
013600 01  FILLER REDEFINES STD-WS-TIME-HHMMSSHS.                       01360000
013700     03  STD-WS-TIME-HHMMSS         PIC 999999.                   01370000
013800     03  FILLER REDEFINES STD-WS-TIME-HHMMSS.                     01380000
013900         05  STD-WS-TIME-HH         PIC 9(002).                   01390000
014000         05  STD-WS-TIME-MM         PIC 9(002).                   01400000
014100         05  STD-WS-TIME-SS         PIC 9(002).                   01410000
014200     03  STD-WS-TIME-HS             PIC 9(002).                   01420000
014300                                                                  01430000
014400 01  STD-WS-GREG-TIME-HHMMSSHS.                                   01440000
014500     03  STD-WS-GREG-TIME-HHMMSS.                                 01450000
014600         05  STD-WS-GREG-TIME-HH    PIC 9(002).                   01460000
014700         05  FILLER                 PIC X(01) VALUE ':'.          01470000
014800         05  STD-WS-GREG-TIME-MM    PIC 9(002).                   01480000
014900         05  FILLER                 PIC X(01) VALUE ':'.          01490000
015000         05  STD-WS-GREG-TIME-SS    PIC 9(002).                   01500000
015100     03  FILLER                     PIC X(01) VALUE ':'.          01510000
015200     03  STD-WS-GREG-TIME-HS        PIC 9(002).                   01520000
015300                                                                  01530000
015400* STD-WS-CMPLD-DATA, STD-WS-CMPLED-DATA-TIME                      01540000
015500* STD-WS-CMPLD-DATE, STD-WS-CMPLED-DATA-C2,                       01550000
015600* STD-WS-CMPLD-C2-DATE AND STD-WS-CMPLED-C2-TIME                  01560000
015700* OBTAINED BY 99999-STD-GET-WHEN-COMPILED                         01570000
015800                                                                  01580000
015900 01  STD-WS-CMPLD-DATA              VALUE SPACES.                 01590000
016000     03  STD-WS-CMPLD-TIME          PIC X(008).                   01600000
016100     03  STD-WS-CMPLD-DATE.                                       01610000
016200         05  STD-WS-CMPLD-DATE-1-1  PIC X(01).                    01620000
016300         05  STD-WS-CMPLD-DATE-2-13 PIC X(12).                    01630000
016400 01  STD-WS-CMPLD-DATA-C2 REDEFINES STD-WS-CMPLD-DATA.            01640000
016500     03  STD-WS-CMPLD-C2-DATE       PIC X(008).                   01650000
016600     03  STD-WS-CMPLD-C2-TIME       PIC X(008).                   01660000
016700                                                                  01670000
016800 01  STD-WS-RET-GREG-DATE-MMDDYY    PIC X(08).                    01680000
016900                                                                  01690000
017000 01  STD-WS-WHEN-COMPILED           PIC X(45) VALUE ' '.          01700000
017100/                                                                 01710000
017200***************************************************************   01720000
017300* CONTROL REPORT HEADING FIELDS                                   01730000
017400***************************************************************   01740000
017500                                                                  01750000
017600***************************************************************   01760000
017700* STANDARD REPORT HEADINGS                                        01770000
017800***************************************************************   01780000
017900 01  STDR-HD1.                                                    01790000
018000     03  FILLER                     PIC X(10) VALUE 'PROGRAM:'.   01800006
018100     03  STDR-HD1-PROGRAM-NAME      PIC X(21) VALUE ' '.          01810006
018200     03  FILLER                     PIC X(60) VALUE               01820006
018300         '             UNIVERSITY OF CALIFORNIA, SAN DIEGO'.      01830000
018400     03  FILLER                     PIC X(19) VALUE ' '.          01840006
018500     03  FILLER                     PIC X(07) VALUE 'PAGE:'.      01850006
018600     03  STDR-HD1-PAGE-CNT          PIC ZZZZ9.                    01860006
018700                                                                  01870000
018800 01  STDR-HD2.                                                    01880000
018900     03  FILLER                     PIC X(10) VALUE 'REPORT:'.    01890006
019000     03  STDR-HD2-DDNAME            PIC X(21) VALUE ' '.          01900006
019100     03  STDR-HD2-DESCR-1           PIC X(60) VALUE ' '.          01910006
019200     03  STDR-HD2-RETENTION         PIC X(31) VALUE ' '.          01920006
019300                                                                  01930000
019400 01  STDR-HD3.                                                    01940000
019500     03  FILLER                     PIC X(07) VALUE 'RUN ON '.    01950006
019600     03  STDR-HD3-RUN-DATE          PIC X(08) VALUE ' '.          01960006
019700     03  FILLER                     PIC X(04) VALUE ' AT '.       01970006
019800     03  STDR-HD3-RUN-TIME          PIC X(08) VALUE ' '.          01980006
019900     03  FILLER                     PIC X(04) VALUE ' '.          01990006
020000     03  STDR-HD3-DESCR-2           PIC X(60) VALUE ' '.          02000006
020100     03  FILLER                     PIC X(31) VALUE ' '.          02010006
020200                                                                  02020000
020300                                                                  02030000
020400***************************************************************   02040000
020500* THE FOLLOWING IS FOR INTERNAL USE ONLY!                         02050000
020600***************************************************************   02060000
020700 01  STD-WS-WORKDATE-CCYYMMDD       PIC 99999999 VALUE ZEROS.     02070000
020800 01  FILLER REDEFINES STD-WS-WORKDATE-CCYYMMDD.                   02080000
020900     03  STD-WS-WORKDATE-CCYY       PIC 9999.                     02090006
021000     03  FILLER REDEFINES STD-WS-WORKDATE-CCYY.                   02100000
021100         05  STD-WS-WORKDATE-CC     PIC 99.                       02110006
021200         05  STD-WS-WORKDATE-YY     PIC 99.                       02120006
021300     03  STD-WS-WORKDATE-MM         PIC 99.                       02130000
021400     03  STD-WS-WORKDATE-DD         PIC 99.                       02140000
021500 01  FILLER REDEFINES STD-WS-WORKDATE-CCYYMMDD.                   02150000
021600     03  FILLER                     PIC 99.                       02160000
021700     03  STD-WS-WORKDATE-YYMMDD     PIC 999999.                   02170000
021800***************************************************************   02180000
021900**  STDWS -   END WORKING STORAGE FOR STANDARD CONTROL REPORT     02190000
022000***************************************************************   02200000
