       01  FPUDU11-CURSOR-CODES.
           10  DBLINK-S-4002-4100-N-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-4083-4096-N-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-4085-4061-N-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-4096-4085-N-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-4192-4013-N-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-4192-4020-N-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-4204-4215-N-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-4205-4211-N-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-4212-4214-N-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-4215-4205-N-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-6117-6139-N-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-6311-6185-N-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-INDX-FSCL-N-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-4002-4010-P-N-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-4002-4010-P-P-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-4083-4096-P-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-4085-4300-E-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-4096-4085-E-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-4083-4096-F-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-4096-4085-F-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-4120-4121-N-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-4180-4181-P-CODE
                                        PIC X(1)        VALUE 'C'.
           10  DBLINK-S-4180-4181-N-CODE
                                        PIC X(1)        VALUE 'C'.
