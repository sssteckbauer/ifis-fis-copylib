      *%---------------------------------------------------------------*
      *%                                                               *
      *%                   200-ZZYII02 ROUTINE                         *
      *%                                                               *
      *%---------------------------------------------------------------*
       200-ZZYII02.

       200-ZZYII02-EXIT.
           EXIT.
      *%---------------------------------------------------------------*
      *%                                                               *
      *%                   200-ZZYII02-RECERROR ROUTINE                *
      *%                                                               *
      *%---------------------------------------------------------------*
       200-ZZYII02-RECERROR.

           MOVE SPACES TO MAP-ELMNT-NAME-SY00.
           MOVE SPACES TO MSG-TEXT-GROUP-SY00.
           MOVE ZEROS TO MSG-QTY-SY00.
           MOVE 990314 TO MSG-ID-SY00.
           PERFORM 200-ZZYII03-GETMESGS
              THRU 200-ZZYII03-GETMESGS-EXIT.
           IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
              GO TO 200-ZZYII02-RECERROR-EXIT
           END-IF.
      ***  RECORD DELETED BY ANOTHER USER.
           MOVE 990443 TO MSG-ID-SY00.
           PERFORM 200-ZZYII03-GETMESGS
              THRU 200-ZZYII03-GETMESGS-EXIT.
           IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
              GO TO 200-ZZYII02-RECERROR-EXIT
           END-IF.
      ***  SCREEN MUST BE REFRESHED
           PERFORM 200-DISPMAP-S01
              THRU 200-DISPMAP-S01-EXIT.
           IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
              GO TO 200-ZZYII02-RECERROR-EXIT
           END-IF.

       200-ZZYII02-RECERROR-EXIT.
           EXIT.
