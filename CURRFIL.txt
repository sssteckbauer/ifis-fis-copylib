      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4228-FILE-CNTL' TO DBLINK-CURR-PARAGRAPH.    276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4228-FILE-CNTL' TO RECORD-NAME.                       276 BRTN
YYY991     MOVE 'F-EXPR-FILE' TO AREA-NAME.                             276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4228-FILE-CNTL TO DBLINK-RECORD-MADE-CURRENT.   276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4228-FILE-CNTL-EXIT                      276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE FIL-VNDR-ID-DGT-ONE TO DBLINK-R4228-FILE-CNTL-01    276 BRTN
YYY991         MOVE FIL-VNDR-ID-LST-NINE TO DBLINK-R4228-FILE-CNTL-02   276 BRTN
YYY991         MOVE FIL-CMDTY-CD TO DBLINK-R4228-FILE-CNTL-03           276 BRTN
YYY991         MOVE FIL-FILE-NBR TO DBLINK-R4228-FILE-CNTL-04           276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4228-FILE-CNTL TO DBLINK-VIEW-NAME-CURRENT.     276 BRTN
YYY991     MOVE DBLINK-R4228-FILE-CNTL-KEY TO DBLINK-VIEW-200-CURRENT.  276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4228-4229                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4228-FILE-CNTL-01 TO DBLINK-S-4228-4229-01.     276 BRTN
YYY991     MOVE DBLINK-R4228-FILE-CNTL-02 TO DBLINK-S-4228-4229-02.     276 BRTN
YYY991     MOVE DBLINK-R4228-FILE-CNTL-03 TO DBLINK-S-4228-4229-03.     276 BRTN
YYY991     MOVE DBLINK-R4228-FILE-CNTL-04 TO DBLINK-S-4228-4229-04.     276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4228-4229-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-EXPR-FILE                                            276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE FIL-VNDR-ID-DGT-ONE TO DBLINK-S-INDX-EXPR-FILE-01.      276 BRTN
YYY991     MOVE FIL-VNDR-ID-LST-NINE TO DBLINK-S-INDX-EXPR-FILE-02.     276 BRTN
YYY991     MOVE FIL-CMDTY-CD TO DBLINK-S-INDX-EXPR-FILE-03.             276 BRTN
YYY991     MOVE FIL-FILE-NBR TO DBLINK-S-INDX-EXPR-FILE-04.             276 BRTN
