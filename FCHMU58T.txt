       01  MAP-CONTROL.

    ***Created by Convert/DC version V8R03 on 12/12/00 at 16:00***

           02  FCHMU58-MAP-CONTROL.
           03  FCHMU58T.
           04  WK01-DCXMAPL-PARMS.
               10  WK01-MAPNAME                  PIC X(8)
                                                 VALUE 'FCHMU58I'.
               10  WK01-IKE-LENGTH               PIC S9(8)  COMP
                                                 VALUE 8919.
               10  WK01-TINA-LENGTH              PIC S9(4)  COMP
                                                 VALUE 785.
           05  WK01-ATTRIBUTES                   PIC X(60)
                                                 VALUE SPACE.
           05  WK01-ATTRIBUTES-ERROR             PIC X(60) VALUE
               'U  BIF'.
           05  WK01-CURSOR-DFLD                  PIC X(36)
                                                 VALUE SPACES.
           05  WK01-CURSOR-DFLD-QUAL             PIC X(30)
                                                 VALUE SPACES.
           05  WK01-ROW                          PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-COLUMN                       PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-ERROR-MSG                    PIC X(80)
                                                 VALUE SPACES.
           05  WK01-MAP-DATA-LEN                 PIC S9(6)
                                                 VALUE ZERO.
           05  WK01-1ST-FLAG                     PIC X(1)
                                                 VALUE 'Y'.
           05  WK01-FLAG-OUTPUT-DATA             PIC X(1)
                                                 VALUE 'Y'.
      *    05  WK01-STATUS-RETURN-FLAG           PIC X(1)
      *                                          VALUE SPACES.
           05  WK01-ALL-BUT-FLAG                 PIC X(1)
                                                 VALUE SPACES.
           05  WK01-FLAG-ALL-FIELDS-CHANGED      PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-CHANGED      PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERROR        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERROR        PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.

           05  WK01-VARDATA                      PIC X(11880)
                                                 VALUE SPACES.
           05  FILLER                            PIC X(100)
                                                 VALUE SPACES.
           04  WK01-ELEMENT-AREA.

               10  WK01-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-6001-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00       PIC X(35)
                                               VALUE SPACES.
               10  WK01-DATE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-HH-MM-TIME-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00       PIC X(4)
                                               VALUE SPACES.
               10  WK01-PM-FLAG-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-USER-ID-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID       PIC X(32)
                                               VALUE SPACES.
               10  WK01-RSPNS-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-FUNC-DESCRIPTION-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION       PIC X(28)
                                               VALUE SPACES.
               10  WK01-STATUS-CODE-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-ID-LAST-NN-4046-CH58-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-LAST-NN-4046-CH58-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-LAST-NN-4046-CH58-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-LAST-NN-4046-CH58-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-LAST-NN-4046-CH58-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-LAST-NN-4046-CH58       PIC X(9)
                                               VALUE SPACES.
               10  WK01-KEY-6311-CH58-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-KEY-6311-CH58-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-KEY-6311-CH58-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-KEY-6311-CH58-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-KEY-6311-CH58-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-KEY-6311-CH58       PIC X(35)
                                               VALUE SPACES.
               10  WK01-TYPE-CODE-6137-CH58-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TYPE-CODE-6137-CH58-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TYPE-CODE-6137-CH58-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TYPE-CODE-6137-CH58-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TYPE-CODE-6137-CH58-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TYPE-CODE-6137-CH58       PIC X(2)
                                               VALUE SPACES.
               10  WK01-DESC-6137-CH58-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-6137-CH58-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-6137-CH58-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-6137-CH58-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-6137-CH58-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-6137-CH58       PIC X(30)
                                               VALUE SPACES.
               10  WK01-RQST-DATE-CH58-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RQST-DATE-CH58-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RQST-DATE-CH58-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RQST-DATE-CH58-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RQST-DATE-CH58-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RQST-DATE-CH58       PIC X(6)
                                               VALUE SPACES.
               10  WK01-CODE-CH58-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-CH58-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-CH58-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-CH58-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-CH58-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-CH58       PIC X(1)
                                               VALUE SPACES.
               10  WK01-ADR-6185-CH581-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6185-CH581-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6185-CH581-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6185-CH581-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6185-CH581-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6185-CH581       PIC X(35)
                                               VALUE SPACES.
               10  WK01-ADR-6185-CH582-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6185-CH582-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6185-CH582-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6185-CH582-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6185-CH582-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6185-CH582       PIC X(35)
                                               VALUE SPACES.
               10  WK01-ADR-6185-CH583-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6185-CH583-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6185-CH583-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6185-CH583-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6185-CH583-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6185-CH583       PIC X(35)
                                               VALUE SPACES.
               10  WK01-ADR-6185-CH584-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6185-CH584-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6185-CH584-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6185-CH584-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6185-CH584-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6185-CH584       PIC X(35)
                                               VALUE SPACES.
               10  WK01-NAME-6185-CH58-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6185-CH58-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6185-CH58-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6185-CH58-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6185-CH58-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6185-CH58       PIC X(18)
                                               VALUE SPACES.
               10  WK01-CODE-6151-CH58-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6151-CH58-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6151-CH58-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6151-CH58-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6151-CH58-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6151-CH58       PIC X(2)
                                               VALUE SPACES.
               10  WK01-CODE-6152-CH58-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6152-CH58-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6152-CH58-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6152-CH58-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6152-CH58-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6152-CH58       PIC X(10)
                                               VALUE SPACES.
               10  WK01-CODE-6153-CH58-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6153-CH58-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6153-CH58-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6153-CH58-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6153-CH58-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6153-CH58       PIC X(2)
                                               VALUE SPACES.
               10  WK01-AREA-CODE-CH58-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AREA-CODE-CH58-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AREA-CODE-CH58-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AREA-CODE-CH58-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AREA-CODE-CH58-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AREA-CODE-CH58       PIC X(3)
                                               VALUE SPACES.
               10  WK01-XCHNG-ID-CH58-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XCHNG-ID-CH58-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XCHNG-ID-CH58-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XCHNG-ID-CH58-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XCHNG-ID-CH58-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XCHNG-ID-CH58       PIC X(3)
                                               VALUE SPACES.
               10  WK01-SEQ-ID-CH58-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-ID-CH58-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-ID-CH58-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-ID-CH58-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-ID-CH58-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-ID-CH58       PIC X(4)
                                               VALUE SPACES.
               10  WK01-XTNSN-ID-CH58-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XTNSN-ID-CH58-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XTNSN-ID-CH58-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XTNSN-ID-CH58-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XTNSN-ID-CH58-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XTNSN-ID-CH58       PIC X(4)
                                               VALUE SPACES.
               10  WK01-START-DATE-CH58-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-START-DATE-CH58-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-START-DATE-CH58-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-START-DATE-CH58-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-START-DATE-CH58-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-START-DATE-CH58       PIC X(6)
                                               VALUE SPACES.
               10  WK01-END-DATE-CH58-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-END-DATE-CH58-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-END-DATE-CH58-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-END-DATE-CH58-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-END-DATE-CH58-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-END-DATE-CH58       PIC X(6)
                                               VALUE SPACES.
               10  WK01-ZZMESSAGE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE       PIC X(160)
                                               VALUE SPACES.
               10  WK01-PASSED-ONE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE       PIC X(32)
                                               VALUE SPACES.
               10  WK01-MAP-RESPONSE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-TEXT-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00       PIC X(14)
                                               VALUE SPACES.
           04  WK01-SUBSCRIPTED-WORK-AREA.
               05  FILLER                      PIC X(1).
               05  WK01-ADR-6185-CH58-F             PIC X(1)
                                               OCCURS 4.
