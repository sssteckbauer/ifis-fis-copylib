    ***Created by Convert/DC version V8R03 on 12/11/00 at 11:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY01-Z = 'Y'
               MOVE WK01-TEXT-SY01 TO DCSD-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0A-Z = 'Y'
               MOVE WK01-TEXT-SY0A TO CNFDL-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0B-Z = 'Y'
               MOVE WK01-TEXT-SY0B TO HOLDS-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH04-Z = 'Y'
               MOVE WK01-CODE-CH04 TO ACTN-CODE-CH04 OF FCHWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4030-CH04-Z = 'Y'
               MOVE WK01-CODE-4030-CH04 TO COA-CODE-4030-CH04 OF FCHWK04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-CODE-4030-CH04-Z = 'Y'
               MOVE WK01-SHARE-CODE-4030-CH04 TO
                COST-SHARE-CODE-4030-CH04 OF FCHWK04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-DESC-4064-CH04-Z = 'Y'
               MOVE WK01-SHARE-DESC-4064-CH04 TO
                COST-SHARE-DESC-4064-CH04 OF FCHWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH04-Z = 'Y'
               MOVE WK01-DATE-4064-CH04 TO START-DATE-4064-CH04 OF
                FCHWK04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CHNG-DATE-CH04-Z = 'Y'
               MOVE WK01-CHNG-DATE-CH04 TO NEXT-CHNG-DATE-CH04 OF
                FCHWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4064-CH04-Z = 'Y'
               MOVE WK01-DESC-4064-CH04 TO STATUS-DESC-4064-CH04 OF
                FCHWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4064-CH0A-Z = 'Y'
               MOVE WK01-DATE-4064-CH0A TO END-DATE-4064-CH04 OF FCHWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4030-CH04-Z = 'Y'
               MOVE WK01-DATE-4030-CH04 TO ACTVY-DATE-4030-CH04 OF
                FCHWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STAMP-4064-CH04-Z = 'Y'
               MOVE WK01-STAMP-4064-CH04 TO TIME-STAMP-4064-CH04 OF
                FCHWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-CH04-Z = 'Y'
               MOVE WK01-PM-FLAG-CH04 TO AM-PM-FLAG-CH04 OF FCHWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-BASIS-4064-CH04-Z = 'Y'
               MOVE WK01-SHARE-BASIS-4064-CH04 TO
                COST-SHARE-BASIS-4064-CH04 OF FCHWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SHARE-MM-IND-4064-CH04-Z = 'Y'
               MOVE WK01-SHARE-MM-IND-4064-CH04 TO
                COST-SHARE-MEMO-IND-4064-CH04 OF FCHWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TO-BASIS-IND-4064-CH04-Z = 'Y'
               MOVE WK01-TO-BASIS-IND-4064-CH04 TO
                APPLY-TO-BASIS-IND-4064-CH04 OF FCHWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SHR-STNDD-PCT-4064-CH0-Z = 'Y'
               MOVE WK01-SHR-STNDD-PCT-4064-CH0 TO
                COST-SHARE-STNDD-PCT-4064-CH04 OF FCHWM04
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4064-CH04-Z = 'Y'
               MOVE WK01-IND-4064-CH04 TO CMPLT-IND-4064-CH04 OF FCHWM04
           END-IF.
