      ******************************************************************
      *    **** COPYBOOK WGA001 - USED FOR VALIFOAP WEB SERVICE ****   *
      *    PARAMETER AREA USED BY DUALCOB (BATCH OR CICS) SUBROUTINE   *
      *    WGA001 TO VALIDATE IFOAPAL/ACCOUNT INDEX APPLICATION.       *
      ******************************************************************

      ******************************************************************
      *    AFTER CHANGES ARE MADE TO THIS COPYBOOK, IT MUST BE COPIED  *
      *    INTO A WEBSPHERE DEVELOPER PROJECT FOLDER, FROM WHICH THE   *
      *    WEB SERVICE INTERFACE PROGRAM, WSDL, & WSBIND ARE GENERATED.*
      *                                                                *
      *    NOTE: BEFORE DOING THE GENERATE, RENAME THIS MEMBER IN THE  *
      *    PROJECT FOLDER AS WGA001X.CPY.  THE 'X' SUFFIX ASSOCIATES   *
      *    IT TO THE WGA001X WEB SERVICE STUB PROGRAM WHICH ACTUALLY   *
      *    CALLS THE WGA001 DUALCOB PROGRAM.                           *
      ******************************************************************

       01  VALIFOAP-PARM-AREA.

           05  VALIFOAP-CHANNEL-HEADER.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND AND OUTBOUND  *
      *      ***********************************************************

               10  VALIFOAP-CHANNEL-ID        PIC X(08).
      *            *****************************************************
      *            * MUST BE 'VALIFOAP'                                *
      *            *****************************************************

               10  VALIFOAP-VERS-RESP-CD      PIC X(08).
      *            *****************************************************
      *            * INCOMING: COMMAREA VERSION = 01.00.00             *
      *            * OUTGOING: RETURN RESPONSE FOR WFGA                *
      *            *                          (WEB/IFIS GA SUBSYSTEM)  *
      *            *****************************************************
                   88  VALIFOAP-RESPONSE-SUCCESSFUL    VALUE 'WFGA000I'.
                   88  VALIFOAP-RESPONSE-ERROR         VALUE 'WFGA999E'.

           05  VALIFOAP-INPUT-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND ONLY          *
      *      ***********************************************************

               10  VALIFOAP-SOURCE-SYSTEM      PIC X(03).
      *            *****************************************************
      *            * WEB = CALLED FROM THE WEB                         *
      *            * FIS = CALLED FROM THE MAINFRAME IFIS SYSTEM       *
      *            * SIS = CALLED FROM THE MAINFRAME ISIS SYSTEM       *
      *            *****************************************************

               10  VALIFOAP-ACCT-INDX-CD       PIC X(10).
               10  VALIFOAP-START-DT           PIC X(10).
      *            *****************************************************
      *            * YYYY-MM-DD OR MM/DD/YYYY; DEFAULT = CURRENT DATE  *
      *            *****************************************************

               10  VALIFOAP-GET-DEFAULT-OVERRIDES
                                               PIC X(01).
      *            *****************************************************
      *            *  Y = APPLY/EDIT IFOAPAL DEFAULTS AND OVERRIDES    *
      * DEFAULT==> * ELSE BYPASS IFOAPAL DEFAULT/OVERRIDE LOGIC        *
      *            *   (UGAII01 DATANAME = DEFAULT-PERFORMED-GA01)     *
      *            *****************************************************

               10  VALIFOAP-ACCEPT-NOT-ENTERABLE
                                               PIC X(01).
      *            *****************************************************
      *            *  Y = ACCEPT FUND/ORGN/ACCT/PRGM NOT DATA ENTERABLE*
      * DEFAULT==> * ELSE REJECT FUND/ORGN/ACCT/PRGM NOT DATA ENTERABLE*
      *            *   (UGAII01 DATANAME = DATA-ENTRY-PERFORMED-GA01)  *
      *            *****************************************************

               10  VALIFOAP-ACCEPT-INACTIVE-INDEX
                                               PIC X(01).
      *            *****************************************************
      *            *  Y = ACCEPT ACCT-INDX-CD INACTIVE (BATCH APP'S)   *
      * DEFAULT==> * ELSE REJECT ACCT-INDX-CD INACTIVE (STATUS = I)    *
      *            *   (UGAII01 DATANAME = INDX-ERR-OVRDE-IND-GA03)    *
      *            *****************************************************

           05  VALIFOAP-INPUT-OUTPUT-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND AND OUTBOUND  *
      *      ***********************************************************
      *      *     THE FOAPAL FIELDS WHICH ARE BLANK AS INPUT MAY BE   *
      *      *     POPULATED WITH DEFAULT VALUES WHEN RETURNED.        *
      *      ***********************************************************

               10  VALIFOAP-FUND-CD            PIC X(06).
               10  VALIFOAP-ORGN-CD            PIC X(06).
               10  VALIFOAP-ACCT-CD            PIC X(06).
                 88  VALIFOAP-ACCT-CD-IS-FOR-GL          VALUE '1     '
                                                          THRU '499999'.
                 88  VALIFOAP-ACCT-CD-IS-FOR-OL          VALUE '5     '
                                                          THRU '899999'.
               10  VALIFOAP-PRGRM-CD           PIC X(06).
               10  VALIFOAP-ACTVY-CD           PIC X(06).
               10  VALIFOAP-LCTN-CD            PIC X(06).

           05  VALIFOAP-RETURN-STATUS-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: OUTBOUND ONLY         *
      *      ***********************************************************

               10  VALIFOAP-RETURN-STATUS-CODE PIC X(01).
      *            *****************************************************
      *            * S = SUCCESSFUL, E = DATA ERROR(S), F = FATAL ERROR*
      *            *****************************************************

               10  VALIFOAP-RETURN-MSG-COUNT   PIC 9(01).
      *            *****************************************************
      *            * NUMBER OF RETURN-MSG-AREA(S) POPULATED (1 TO 4)   *
      *            *****************************************************

               10  VALIFOAP-RETURN-MSG-AREA    OCCURS 4 TIMES.
      *            *****************************************************
      *            * FOR STATUS 'S', SUCCESS MESSAGE NUMBER & TEXT     *
      *            * FOR STATUS 'E', ERROR MSG NUMBER, FIELD & TEXT    *
      *            * FOR STATUS 'F', FATAL SQLCODE, CURR-IO-NUM WITH   *
      *            *                 PARAGRAPH NAME, & ASSOC DB2 TABLE *
      *            *****************************************************
                   15  VALIFOAP-RETURN-MSG-NUM PIC X(06).
                   15  VALIFOAP-RETURN-MSG-FIELD
                                               PIC X(30).
                   15  VALIFOAP-RETURN-MSG-TEXT
                                               PIC X(40).

           05  VALIFOAP-ACCOMM-FILLER          PIC X(566).
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: NONE                  *
      *      ***********************************************************
      *      *     FILLER NEEDED TO MATCH THE IFIS DFHCOMMAREA SIZE.   *
      *      *     TOTAL LENGTH OF COPYBOOK MUST ALWAYS BE 950 BYTES.  *
      *      ***********************************************************

