       975-TO-R4213-BANK-ACCT SECTION.
           MOVE BKA-USER-CD TO
               USER-CODE-4213 OF R4213-BANK-ACCT
                                                                     .
           MOVE BKA-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4213 OF R4213-BANK-ACCT
                                                                     .
           MOVE BKA-BANK-ACCT-NBR TO
               BANK-ACCT-NMBR-4213 OF R4213-BANK-ACCT
                                                                     .
           MOVE BKA-BANK-ACCT-DESC TO
               BANK-ACCT-DESC-4213 OF R4213-BANK-ACCT
                                                                     .
           MOVE BKA-STATUS TO
               STATUS-4213 OF R4213-BANK-ACCT
                                                                     .
       975-TO-R4213-BANK-ACCT-EXIT.
           EXIT.
