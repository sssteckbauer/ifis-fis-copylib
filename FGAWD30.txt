      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FGAWD30                              04/24/00  13:08  *
      *                                                               *
      *---------------------------------------------------------------*
      ****----------------------------------------------------------****
      **  AUTHOR    DATE    DESCRIPTION                                *
      **  ------- --------  ----------------------------------------   *
      **  DEVBWS  05/20/02  -501 ABEND FIX                             *
      ****----------------------------------------------------------****
       01  FGAWD30.
           02  SYSTEM-DATE-GA30               COMP-3    PIC S9(8).
           02  PERM-EXTR-DATE-GA30                      PIC X(8).
           02  CURR-PSTNG-DATE-GA30                     PIC X(8).
           02  TRANS-DATE-NMRC-GA30           COMP-3    PIC S9(8).
DEVBWS     02  SAVE-PMKEY-ID-4174-GA30                  PIC X(8).
           02  TOTAL-AMT-DR-GA30              COMP-3    PIC S9(10)V99.
           02  PERM-TOTAL-AMT-DR-GA30         COMP-3    PIC S9(10)V99.
           02  TOTAL-AMT-CR-GA30              COMP-3    PIC S9(10)V99.
           02  PERM-TOTAL-AMT-CR-GA30         COMP-3    PIC S9(10)V99.
           02  TOTAL-AMT-NET-GA30             COMP-3    PIC S9(10)V99.
           02  PERM-TOTAL-AMT-NET-GA30        COMP-3    PIC S9(10)V99.
           02  WK-TOTAL-TRANS-AMT-GA30        COMP-3    PIC S9(10)V99.
           02  TMPLT-CHG-FLAG-GA30                      PIC X(1).
           02  PERM-TMPLT-CHG-FLAG-GA30                 PIC X(1).
           02  CMPLT-FLAG-GA30                          PIC X(1).
           02  ONEUP-FLAG-GA30                          PIC X(1).
           02  DCMNT-AMT-NMRC-GA30            COMP-3    PIC S9(10)V99.
           02  DCMNT-ERROR-FLAG-GA30                    PIC X(1).
           02  DOCBAL-ERROR-FLAG-GA30                   PIC X(1).
           02  SYSDAT-FLAG-GA30                         PIC X(1).
           02  ERR-FLAG-GA30                            PIC X(1).
           02  JRNL-DETL-FLAG-GA30                      PIC X(1).
           02  WORK-DR-AMT-GA30                         PIC X(15).
           02  WORK-CR-AMT-GA30                         PIC X(15).
           02  WORK-DCMNT-NMBR-GA30                     PIC X(7).
           02  SET-SORT-KEY-4012-GA30.
               03  OPTN-1-CODE-GA30                     PIC X(8).
               03  OPTN-2-CODE-GA30                     PIC X(8).
               03  LEVEL-NMBR-GA30                      PIC 9(2).
           02  ACCT-WARNING-IND-GA30                    PIC X(1).
           02  FUND-WARNING-IND-GA30                    PIC X(1).
           02  SYSTEM-VERSION-GA30                      PIC 9(4).
DEVBWS     02  SAVE-PMKEY-ID-GA30                       PIC X(11).
DEVMLJ     02  BYPASS-ERRS-FLAG-GA30                    PIC X(1).
