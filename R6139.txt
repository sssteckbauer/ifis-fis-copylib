       01  R6139-PRSN-ADR.
           02  DB-PROC-ID-6139.
               03  USER-CODE-6139                       PIC X(8).
               03  LAST-ACTVY-DATE-6139                 PIC X(5).
               03  TRMNL-ID-6139                        PIC X(8).
               03  PURGE-FLAG-6139                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  ADR-SORT-KEY-6139.
               03  ADR-TYPE-CODE-6139                   PIC X(2).
               03  END-DATE-6139              COMP-3    PIC S9(8).
           02  START-DATE-6139                COMP-3    PIC S9(8).
           02  LINE-ADRS-6139.
               03  LINE-ADR-1-6139                      PIC X(35).
               03  LINE-ADR-2-6139                      PIC X(35).
               03  LINE-ADR-3-6139                      PIC X(35).
               03  LINE-ADR-4-6139                      PIC X(35).
           02  LINE-ADRS-OCC-6139 REDEFINES
               LINE-ADRS-6139.
               03  LINE-ADR-6139            OCCURS 4    PIC X(35).
           02  CITY-NAME-6139                           PIC X(18).
           02  TLPHN-ID-6139.
               03  BASIC-TLPHN-ID-6139.
                   04  TLPHN-AREA-CODE-6139             PIC X(3).
                   04  TLPHN-XCHNG-ID-6139              PIC X(3).
                   04  TLPHN-SEQ-ID-6139                PIC X(4).
               03  TLPHN-XTNSN-ID-6139                  PIC X(4).
           02  CNTY-CODE-6139                           PIC X(4).
           02  STATE-CODE-6139                          PIC X(2).
           02  ZIP-CODE-6139                            PIC X(10).
           02  CNTRY-CODE-6139                          PIC X(2).
           02  LINE-ADR-1-UC-6139                       PIC X(35).
           02  FAX-TLPHN-ID-6139.
               03  FAX-AREA-CODE-6139                   PIC X(3).
               03  FAX-XCHNG-ID-6139                    PIC X(3).
               03  FAX-SEQ-ID-6139                      PIC X(4).
           02  EMADR-LINE-6139                          PIC X(70).
           02  FAX-STOP-6139                            PIC X(1).
SN6132     02  POSTAL-CODE-6139                         PIC X(10).
           02  FILLER02                                 PIC X(9).
