      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWD91                              04/24/00  12:59  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWD91.
           02  SAVE-FUND-CODE-CH91          OCCURS 5    PIC X(6).
           02  SAVE-FUND-TITLE-CH91         OCCURS 5    PIC X(35).
           02  LEVEL-CNTR-CH91                          PIC 9(1).
           02  LOOP-FLAG-CH91                           PIC X(1).
           02  START-DATE-CH91                COMP-3    PIC S9(8).
           02  SAVE-DBKEY-ID-4005-CH91        COMP      PIC S9(8).
