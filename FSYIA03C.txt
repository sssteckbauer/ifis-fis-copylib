      *%---------------------------------------------------------------*
      *%                                                               *
      *%                   200-FSYIA03 ROUTINE                         *
      *%                                                               *
      *%---------------------------------------------------------------*
       200-FSYIA03.

      ******************************************************************
      ***  VALDIATE DOCUMENT CHANGES THROUGH THE APPROVAL HIEREARCHY
      ***  THIS INCLUDE MODULE MUST BE PART OF A SUBROUTINE.
      ***  THE FOLLOWING FIELDS MUST BE SET BY THE DIALOG THAT USES THIS
      * INCLUDE
      ***      SEQ-NMBR-SY08, DCMNT-NMBR-SY08, CHNG-SEQ-NMBR-SY08, UNVRS
      *-CODE-SY08
      ***      APRVL-IND-SY08, FSCL-YR-SY08
      ***  THE FOLLOWING SUBROUTINES MUST BE PART OF THE DIALOG THAT USE
      *S THIS
      ***      APRVLERR
      *** 001  HAC   01/31/94  NEW CALC KEY FOR R4212                  F
      *I041 *
      *** 002  HAC   02/22/94  MOVE FSCL-YR-SY08 TO FSCL-YR-4212       G
      *A158 *
      *** 003  BAC   09/24/94  FOR BOTH DOCS 0007 AND 0012 CHECK R4212 G
      *A039 *
      ***                      FOR BOTH A YEAR AND SPACES              G
      *A039 *
      ******************************************************************
      *
           MOVE UNVRS-CODE-SY08 TO UNVRS-CODE-4212.
           MOVE SEQ-NMBR-SY08 TO SEQ-NMBR-4212.
           MOVE DCMNT-NMBR-SY08 TO DCMNT-NMBR-4212.
           MOVE CHNG-SEQ-NMBR-SY08 TO CHNG-SEQ-NMBR-4212.
           MOVE 'N' TO USER-FOUND-SY08.
           MOVE FSCL-YR-SY08 TO FSCL-YR-4212.
      *%   OBTAIN CALC R4212-APRVL-HDR.
           MOVE 0152 TO DBLINK-CURR-IO-NUM
           MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
           MOVE DBLINK-CALC TO DBLINK-TYPE-GET
           MOVE FSCL-YR-4212 OF R4212-APRVL-HDR TO APH-FSCL-YR
           MOVE SEQ-NMBR-4212 OF R4212-APRVL-HDR TO APH-SEQ-NBR
           MOVE DCMNT-NMBR-4212 OF R4212-APRVL-HDR TO APH-DOC-NBR
           MOVE CHNG-SEQ-NMBR-4212 OF R4212-APRVL-HDR TO
               APH-CHNG-SEQ-NBR
           PERFORM 906-05-R4212-APRVL-HDR
              THRU 906-05-R4212-APRVL-HDR-EXIT
           PERFORM 999-SQL-STATUS
              THRU 999-SQL-STATUS-EXIT.
           IF DB-REC-NOT-FOUND
               IF (SEQ-NMBR-SY08 = '0007' OR
                  SEQ-NMBR-SY08 = '0012')
                   MOVE SPACES TO FSCL-YR-4212
      *%           OBTAIN CALC R4212-APRVL-HDR
                   MOVE 0153 TO DBLINK-CURR-IO-NUM
                   MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
                   MOVE DBLINK-CALC TO DBLINK-TYPE-GET
                   MOVE FSCL-YR-4212 OF R4212-APRVL-HDR TO APH-FSCL-YR
                   MOVE SEQ-NMBR-4212 OF R4212-APRVL-HDR TO APH-SEQ-NBR
                   MOVE DCMNT-NMBR-4212 OF R4212-APRVL-HDR TO
                       APH-DOC-NBR
                   MOVE CHNG-SEQ-NMBR-4212 OF R4212-APRVL-HDR TO
                       APH-CHNG-SEQ-NBR
                   PERFORM 906-05-R4212-APRVL-HDR
                      THRU 906-05-R4212-APRVL-HDR-EXIT
                   PERFORM 999-SQL-STATUS
                      THRU 999-SQL-STATUS-EXIT
                   IF DB-REC-NOT-FOUND
                       MOVE 001543 TO MSG-ID-SY00
                       PERFORM 200-ZZYII03-GETMESGS
                          THRU 200-ZZYII03-GETMESGS-EXIT
                       IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                          GO TO 200-FSYIA03-EXIT
                       END-IF
      *** UNAPRVD REC NOT FOUND - SYSTEM PROBLEM
                       PERFORM 200-DISPMAP-S01
                          THRU 200-DISPMAP-S01-EXIT
                       IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                          GO TO 200-FSYIA03-EXIT
                       END-IF
                   END-IF
               ELSE
                   MOVE 001543 TO MSG-ID-SY00
                   PERFORM 200-ZZYII03-GETMESGS
                      THRU 200-ZZYII03-GETMESGS-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYIA03-EXIT
                   END-IF
      *** UNAPRVD REC NOT FOUND - SYSTEM PROBLEM
                   PERFORM 200-DISPMAP-S01
                      THRU 200-DISPMAP-S01-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYIA03-EXIT
                   END-IF
               END-IF
           END-IF.
      *
           IF APRVL-IND-SY08 NOT = 'Y' AND
              AGR-USER-ID NOT = USER-ID-4212
      *%       IF S-4205-4212 MEMBER
               MOVE 0154 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-IF TO DBLINK-TYPE-SELECT
               MOVE DBLINK-MEMBER TO DBLINK-TYPE-GET
               IF DBLINK-S-4205-4212-F = DBLINK-MEMBER
                   MOVE ZERO TO ERROR-STATUS
               ELSE
                   MOVE DBLINK-AREA-NOT-READY-IF TO ERROR-STATUS
               END-IF
               IF ERROR-STATUS = ZERO
                   CONTINUE
               ELSE
                   MOVE 001543 TO MSG-ID-SY00
                   PERFORM 200-ZZYII03-GETMESGS
                      THRU 200-ZZYII03-GETMESGS-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYIA03-EXIT
                   END-IF
      *** UNAPRVD REC NOT FOUND - SYSTEM PROBL
                   PERFORM 200-DISPMAP-S01
                      THRU 200-DISPMAP-S01-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYIA03-EXIT
                   END-IF
               END-IF
      *%       OBTAIN OWNER WITHIN S-4205-4212
               MOVE 0155 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-OWNER TO DBLINK-TYPE-GET
               PERFORM 906-04-S-4205-4212
                  THRU 906-04-S-4205-4212-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
               IF DB-REC-NOT-FOUND
                   MOVE 001543 TO MSG-ID-SY00
                   PERFORM 200-ZZYII03-GETMESGS
                      THRU 200-ZZYII03-GETMESGS-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYIA03-EXIT
                   END-IF
      *** UNAPRVD REC NOT FOUND - SYSTEM PROBL
                   PERFORM 200-DISPMAP-S01
                      THRU 200-DISPMAP-S01-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYIA03-EXIT
                   END-IF
               END-IF
      *%       OBTAIN OWNER WITHIN S-4215-4205
               MOVE 0156 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-OWNER TO DBLINK-TYPE-GET
               PERFORM 906-04-S-4215-4205
                  THRU 906-04-S-4215-4205-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
      *%       OBTAIN FIRST R4205-APRVL-LVL WITHIN S-4215-4205
               MOVE 0157 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-FIRST TO DBLINK-TYPE-GET
               PERFORM 906-00N-S-4215-4205
                  THRU 906-00N-S-4215-4205-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
               MOVE 'N' TO AC99W-FLAG-GOBACK
               PERFORM 200-FSYIA03-020
                  THRU 200-FSYIA03-020-EXIT
               IF AC99W-FLAG-GOBACK = 'Y'
                   GO TO 200-FSYIA03-EXIT
               END-IF
               IF USER-FOUND-SY08 = 'N'
                   MOVE 001545 TO MSG-ID-SY00
                   PERFORM 200-ZZYII03-GETMESGS
                      THRU 200-ZZYII03-GETMESGS-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYIA03-EXIT
                   END-IF
      *** USER NOT IN APRVL HIER - CANNOT CHAN
                   PERFORM 200-DISPMAP-S01
                      THRU 200-DISPMAP-S01-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYIA03-EXIT
                   END-IF
               END-IF
           END-IF.
      *
      *** IN CASE OF FINAL APPROVAL, THE SET NO LONGER EXISTS
           IF APRVL-IND-SY08 = 'Y' AND
              AGR-USER-ID NOT = USER-ID-4212
               MOVE UNVRS-CODE-SY08 TO UNVRS-CODE-4203
               MOVE SEQ-NMBR-SY08 TO SEQ-NMBR-4203
      *%       OBTAIN CALC R4203-DOCUMENT
               MOVE 0158 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-CALC TO DBLINK-TYPE-GET
               MOVE SEQ-NMBR-4203 OF R4203-DOCUMENT TO DOC-SEQ-NBR
               PERFORM 906-05-R4203-DOCUMENT
                  THRU 906-05-R4203-DOCUMENT-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
               IF DB-REC-NOT-FOUND
                   MOVE 001546 TO MSG-ID-SY00
                   PERFORM 200-APRVLERR-S01
                      THRU 200-APRVLERR-S01-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYIA03-EXIT
                   END-IF
      *** DOCUMENT NOT DEFINED IN THE SYSTEM
                   PERFORM 200-DISPMAP-S01
                      THRU 200-DISPMAP-S01-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYIA03-EXIT
                   END-IF
               END-IF
               MOVE USER-ID-4212 TO USER-ID-4204
      *%       FIND R4204-APRVL-J WITHIN S-4203-4204 USING USER-ID-4204
               MOVE 0159 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-FIND TO DBLINK-TYPE-SELECT
               MOVE DBLINK-SORTED TO DBLINK-TYPE-GET
               MOVE USER-ID-4204 TO DBLINK-S-4203-4204-KEY-U
               PERFORM 906-02-S-4203-4204
                  THRU 906-02-S-4203-4204-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
               IF DB-REC-NOT-FOUND
                   MOVE 001200 TO MSG-ID-SY00
                   PERFORM 200-APRVLERR-S01
                      THRU 200-APRVLERR-S01-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYIA03-EXIT
                   END-IF
      *** APPROVAL HIERARCHY UNAVIALBLE FOR US
                   PERFORM 200-DISPMAP-S01
                      THRU 200-DISPMAP-S01-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYIA03-EXIT
                   END-IF
               END-IF
               MOVE APRVL-TMPLT-CODE-4212 TO APRVL-TMPLT-CODE-4215
      *%       OBTAIN R4215-APRV-TMPLT WITHIN S-4204-4215 USING
      *%        APRVL-TMPLT-CODE-4215
               MOVE 0160 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-SORTED TO DBLINK-TYPE-GET
               MOVE APRVL-TMPLT-CODE-4215 TO DBLINK-S-4204-4215-KEY-U
               PERFORM 906-02-S-4204-4215
                  THRU 906-02-S-4204-4215-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
               IF DB-REC-NOT-FOUND
                   MOVE 001200 TO MSG-ID-SY00
                   PERFORM 200-APRVLERR-S01
                      THRU 200-APRVLERR-S01-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYIA03-EXIT
                   END-IF
      *** APPROVAL HIERARCHY UNAVIALBLE FOR US
                   PERFORM 200-DISPMAP-S01
                      THRU 200-DISPMAP-S01-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYIA03-EXIT
                   END-IF
               END-IF
      *%       OBTAIN NEXT R4205-APRVL-LVL WITHIN S-4215-4205
               MOVE 0161 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-NEXT TO DBLINK-TYPE-GET
               PERFORM 906-00N-S-4215-4205
                  THRU 906-00N-S-4215-4205-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
               IF DB-END-OF-SET
                   MOVE 001200 TO MSG-ID-SY00
                   PERFORM 200-APRVLERR-S01
                      THRU 200-APRVLERR-S01-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYIA03-EXIT
                   END-IF
      *** APPROVAL HIERARCHY UNAVAILABLE FOR U
                   PERFORM 200-DISPMAP-S01
                      THRU 200-DISPMAP-S01-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYIA03-EXIT
                   END-IF
               END-IF
               MOVE 'N' TO AC99W-FLAG-GOBACK
               PERFORM 200-FSYIA03-040
                  THRU 200-FSYIA03-040-EXIT
               IF AC99W-FLAG-GOBACK = 'Y'
                   GO TO 200-FSYIA03-EXIT
               END-IF
               IF USER-FOUND-SY08 = 'N'
                   MOVE 001545 TO MSG-ID-SY00
                   PERFORM 200-ZZYII03-GETMESGS
                      THRU 200-ZZYII03-GETMESGS-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYIA03-EXIT
                   END-IF
      *** USER NOT IN APRVL HIER - CANNOT CHAN
                   PERFORM 200-DISPMAP-S01
                      THRU 200-DISPMAP-S01-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYIA03-EXIT
                   END-IF
               END-IF
           END-IF.
      *

       200-FSYIA03-EXIT.
           EXIT.
      *%---------------------------------------------------------------*
      *%                                                               *
      *%               200-FSYIA03-020 ROUTINE                         *
      *%                                                               *
      *%---------------------------------------------------------------*
       200-FSYIA03-020.

           PERFORM WITH TEST BEFORE
                   UNTIL NOT ( NOT DB-END-OF-SET   AND
              USER-FOUND-SY08 = 'N' )
               IF AGR-USER-ID = APRVL-ID-4205
                   MOVE 'Y' TO USER-FOUND-SY08
                   GO TO 200-FSYIA03-020-EXIT
               END-IF
      *%       OBTAIN FIRST R4211-APRVL-ALT WITHIN S-4205-4211
               MOVE 0162 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-FIRST TO DBLINK-TYPE-GET
               PERFORM 906-00N-S-4205-4211
                  THRU 906-00N-S-4205-4211-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
                   PERFORM 200-FSYIA03-030
                      THRU 200-FSYIA03-030-EXIT
                   IF AC99W-FLAG-GOBACK = 'Y'
                       GO TO 200-FSYIA03-020-EXIT
                   END-IF
      *%       OBTAIN NEXT R4205-APRVL-LVL WITHIN S-4215-4205
               MOVE 0163 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-NEXT TO DBLINK-TYPE-GET
               PERFORM 906-00N-S-4215-4205
                  THRU 906-00N-S-4215-4205-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
           END-PERFORM.

       200-FSYIA03-020-EXIT.
           EXIT.
      *%---------------------------------------------------------------*
      *%                                                               *
      *%               200-FSYIA03-030 ROUTINE                         *
      *%                                                               *
      *%---------------------------------------------------------------*
       200-FSYIA03-030.

           PERFORM WITH TEST BEFORE
                   UNTIL NOT ( NOT DB-END-OF-SET   )
               IF ALTRNT-APRVL-CODE-4211 = AGR-USER-ID
                   MOVE 'Y' TO USER-FOUND-SY08
                   GO TO 200-FSYIA03-030-EXIT
               END-IF
      *%       OBTAIN NEXT WITHIN S-4205-4211
               MOVE 0164 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-NEXT TO DBLINK-TYPE-GET
               PERFORM 906-00N-S-4205-4211
                  THRU 906-00N-S-4205-4211-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
           END-PERFORM.

       200-FSYIA03-030-EXIT.
           EXIT.
      *%---------------------------------------------------------------*
      *%                                                               *
      *%               200-FSYIA03-040 ROUTINE                         *
      *%                                                               *
      *%---------------------------------------------------------------*
       200-FSYIA03-040.

           PERFORM WITH TEST BEFORE
                   UNTIL NOT ( NOT DB-END-OF-SET   AND
              USER-FOUND-SY08 = 'N' )
               IF AGR-USER-ID = APRVL-ID-4205
                   MOVE 'Y' TO USER-FOUND-SY08
                   GO TO 200-FSYIA03-040-EXIT
               END-IF
      *%       OBTAIN FIRST R4211-APRVL-ALT WITHIN S-4205-4211
               MOVE 0165 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-FIRST TO DBLINK-TYPE-GET
               PERFORM 906-00N-S-4205-4211
                  THRU 906-00N-S-4205-4211-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
                   PERFORM 200-FSYIA03-050
                      THRU 200-FSYIA03-050-EXIT
                   IF AC99W-FLAG-GOBACK = 'Y'
                       GO TO 200-FSYIA03-040-EXIT
                   END-IF
      *%       OBTAIN NEXT R4205-APRVL-LVL WITHIN S-4215-4205
               MOVE 0166 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-NEXT TO DBLINK-TYPE-GET
               PERFORM 906-00N-S-4215-4205
                  THRU 906-00N-S-4215-4205-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
           END-PERFORM.

       200-FSYIA03-040-EXIT.
           EXIT.
      *%---------------------------------------------------------------*
      *%                                                               *
      *%               200-FSYIA03-050 ROUTINE                         *
      *%                                                               *
      *%---------------------------------------------------------------*
       200-FSYIA03-050.

           PERFORM WITH TEST BEFORE
                   UNTIL NOT ( NOT DB-END-OF-SET   )
               IF ALTRNT-APRVL-CODE-4211 = AGR-USER-ID
                   MOVE 'Y' TO USER-FOUND-SY08
                   GO TO 200-FSYIA03-050-EXIT
               END-IF
      *%       OBTAIN NEXT WITHIN S-4205-4211
               MOVE 0167 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-NEXT TO DBLINK-TYPE-GET
               PERFORM 906-00N-S-4205-4211
                  THRU 906-00N-S-4205-4211-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
           END-PERFORM.

       200-FSYIA03-050-EXIT.
           EXIT.
