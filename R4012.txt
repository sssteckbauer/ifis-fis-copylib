       01  R4012-SYSDAT-DTL.
           02  DB-PROC-ID-4012.
               03  USER-CODE-4012                       PIC X(8).
               03  LAST-ACTVY-DATE-4012                 PIC X(5).
               03  TRMNL-ID-4012                        PIC X(8).
               03  PURGE-FLAG-4012                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  OPTN-1-CODE-4012                         PIC X(8).
           02  OPTN-2-CODE-4012                         PIC X(8).
           02  LEVEL-NMBR-4012                          PIC 9(2).
           02  SYSDAT-SHORT-DESC-4012                   PIC X(20).
           02  STNDD-LONG-DESC-4012                     PIC X(35).
           02  DATA-DESC-4012                           PIC X(15).
