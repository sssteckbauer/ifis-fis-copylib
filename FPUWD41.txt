      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWD41                              04/24/00  13:11  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWD41.
           02  SAVE-DBKEY-ID-4085-PU41
                                     COMP   OCCURS 2    PIC S9(8).
           02  EXTENDED-PRICE-PU41            COMP-3    PIC 9(10)V9999.
           02  WRK-PO-KEY-PU41.
               03  PO-NMBR-PU41                         PIC X(8).
               03  CHNG-SEQ-NMBR-PU41                   PIC X(3).
           02  SET-SORT-KEY-6311-PU41.
               03  UNVRS-CODE-PU41                      PIC X(2).
               03  INTRL-REF-ID-PU41          COMP-3    PIC S9(7).
