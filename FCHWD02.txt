      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWD02                              04/24/00  13:00  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWD02.
           02  SYSTEM-DATE-CH02               COMP-3    PIC S9(8).
           02  START-DATE-CH02                COMP-3    PIC S9(8).
           02  END-DATE-CH02                  COMP-3    PIC S9(8).
           02  SAVE-DBKEY-ID-4010-CH02                  PIC X(24).
           02  SET-SORT-KEY-4012-CH02.
               03  OPTN-1-CODE-4012-CH02                PIC X(8).
               03  OPTN-2-CODE-4012-CH02                PIC X(8).
               03  LEVEL-NMBR-4012-CH02                 PIC 9(2).
           02  SET-SORT-KEY-4019-CH02.
               03  UNVRS-CODE-4019-CH02                 PIC X(2).
               03  MGR-PID-4019-CH02.
                   04  MGR-DIGIT-ONE-4019-CH02          PIC X(1).
                   04  MGR-LAST-NINE-4019-CH02          PIC X(9).
           02  RQST-FLAG-CH02                           PIC X(1).
           02  LOOP-FLAG-CH02                           PIC X(1).
           02  LEVEL-CNTR-CH02                          PIC 9(1).
           02  ORGZN-CODE-CH02              OCCURS 8    PIC X(6).
           02  INTRL-REF-ID-CH02              COMP-3    PIC S9(7).
           02  WS-XTRNL-EXIST-IND                       PIC X(1).
           02  WS-UCA-EXIST-IND                         PIC X(1).
           02  WS-EXTRCD.
               03 WS-FIRST-CH-EXTRCD                    PIC X(01).
               03 WS-EXTRCD-6CH.
                  04 WS-FIRST-2CHAR-EXTRCD              PIC X(02).
                  04 WS-4CH-EXTRCD                      PIC X(04).
           02  WS-CURR-FSCL-YR-DATE.
               03 WS-CURR-FSCL-CC                      PIC 9(02).
               03 WS-CURR-FSCL-YY                      PIC 9(02).
               03 WS-CURR-FSCL-MM                      PIC 9(02).
               03 WS-CURR-FSCL-DD                      PIC 9(02).
           02  WS-ACG                                  PIC X(06).
           02  WS-SAVED-FIELDS.
               03  EXTRCD-ORIG-LINE.
                   05 EXTRCD-ORIG                       PIC X(7).
                   05 EXTRCD-TITLE-ORIG                 PIC X(35).
               03  NON-CORE-ORIG-LINE.
                   05 NON-ARC-ORIG                      PIC X(06).
                   05 NON-UAS-ORIG                      PIC X(06).
                   05 NON-UAS-ACD-ORIG                  PIC X(03).
               03  ORG41-640-ORIG-LINE.
                   05 ORG41-640-IND-ORIG                PIC X(1).
                   05 EXTRCD-ORG41-640                  PIC X(07).
                   05 ORG41-640-ARC-ORIG                PIC X(06).
                   05 ORG41-640-UAS-ORIG                PIC X(06).
                   05 ORG41-640-UAS-ACD-ORIG            PIC X(03).
               03  ORG41-644-ORIG-LINE.
                   05 ORG41-644-IND-ORIG                PIC X(1).
                   05 EXTRCD-ORG41-644                  PIC X(07).
                   03 ORG41-644-ARC-ORIG                PIC X(06).
                   03 ORG41-644-UAS-ORIG                PIC X(06).
                   03 ORG41-644-UAS-ACD-ORIG            PIC X(03).
                   03 ORG41-644-NSF-ORIG                PIC X(3).
               03  ORG41-662-ORIG-LINE.
                   05 ORG41-662-IND-ORIG               PIC X(1).
                   05 EXTRCD-ORG41-662                 PIC X(07).
                   05 ORG41-662-ARC-ORIG               PIC X(06).
                   05 ORG41-662-UAS-ORIG               PIC X(06).
                   05 ORG41-662-UAS-ACD-ORIG           PIC X(03).
               03  ORG-RESEARCH-UNIT-IND-ORIG           PIC X(1).
               03  ORG-UNIT-CODE-ORIG                   PIC X(6).
               03  LOC2-ORIG                            PIC X(1).
               03  LOC3-ORIG                            PIC X(1).
