      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FUTWD02                              04/24/00  13:23  *
      *                                                               *
      *---------------------------------------------------------------*
      ****----------------------------------------------------------****
      **  AUTHOR    DATE    DESCRIPTION                                *
      **  ------- --------  ----------------------------------------   *
      **  DEVBWS  06/08/02  FIX +100 ABEND                             *
      **  DEVBWS  06/02/04  EFFICIENCY MODIFICATIONS                   *
      ****----------------------------------------------------------****
       01  FUTWD02.
           02  WORK-DATE-UT02                 COMP-3    PIC S9(8).
           02  INDX-KEY-WORK-UT02                       PIC X(30).
DEVBWS     02  SAVE-DBKEY-ID-4204-UT02                  PIC X(11).
DEVBWS     02  SAVE-DBKEY-ID-UT02           OCCURS 6    PIC X(43).
           02  WK-APRVL-LIMIT-UT02   COMP-3 OCCURS 5    PIC S9(10)V99.
           02  PRE-APRVL-LIMIT-UT02           COMP-3    PIC S9(10)V99.
DEVBWS     02  SAVE-DBKEY-ID-4203-UT02                  PIC X(3).
DEVBWS     02  SAVE-DBKEY-ID-4215-UT02                  PIC X(14).
           02  HOLD-START-DATE-UT02           COMP-3    PIC S9(8).
           02  HOLD-END-DATE-UT02             COMP-3    PIC S9(8).
           02  ORIGINAL-START-DATE-UT02       COMP-3    PIC S9(8).
           02  SUB-1-UT02                     COMP-3    PIC 9(3).
           02  SUB-2-UT02                     COMP-3    PIC 9(3).
           02  SUB-3-UT02                     COMP-3    PIC 9(3).
           02  ALTAPR-DEL-FLAG-UT02                     PIC X(1).
           02  ALTAPR-CHG-FLAG-UT02                     PIC X(1).
           02  PRIMARY-ALT-APRVL-FLAG-UT02              PIC X(1).
           02  SAVE-APRVL-ID-4205-UT02                  PIC X(8).
           02  WK-APRVL-ID-UT02                         PIC X(8).
           02  APRVL-TRAN-KEY-4216-UT02.
               03  USER-ID-4216-UT02                    PIC X(8).
               03  DCMNT-TYPE-4216-UT02                 PIC X(3).
               03  APRVL-TMPLT-CODE-4216-UT02           PIC X(3).
               03  LEVEL-SEQ-NMBR-4216-UT02             PIC 9(4).
               03  APRVL-ID-4216-UT02                   PIC X(8).
           02  SYSTEM-VERSION-UT02                      PIC 9(4).
DEVBWS     02  VALID-KEY-FLAG-UT02                      PIC X(1).
