      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM72                              04/24/00  12:19  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM72.
           02  ACTN-CODE-GROUP-CH72.
               03  ACTN-CODE-CH72           OCCURS 11   PIC X(1).
           02  COA-CODE-4050-CH72           OCCURS 11   PIC X(1).
           02  ACTVY-CODE-4050-CH72         OCCURS 11   PIC X(6).
           02  ACTVY-TITLE-4066-CH72        OCCURS 11   PIC X(35).
           02  STATUS-4066-CH72             OCCURS 11   PIC X(1).
           02  START-DATE-4066-CH72         OCCURS 11   PIC X(6).
           02  END-DATE-4066-CH72           OCCURS 11   PIC X(6).
