      *      CREATED BY CONVERT/DB V8R01 ON 05/03/00 AT 10:54         *
YYY991     MOVE '985-CURR-SDES-044' TO DBLINK-CURR-PARAGRAPH.           276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'SDES-044' TO RECORD-NAME.                              276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-SDES-044 TO DBLINK-RECORD-MADE-CURRENT.          276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-SDES-044-EXIT                             276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE SDE-FK-SDR-SR-NAM TO DBLINK-SDES-044-01             276 BRTN
YYY991         MOVE SDE-FK-SDR-PKEY-TS TO DBLINK-SDES-044-02            276 BRTN
YYY991         MOVE SDE-FK-SDR-SR-SDR TO DBLINK-SDES-044-03             276 BRTN
YYY991         MOVE SDE-SDR-TS TO DBLINK-SDES-044-04                    276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-SDES-044 TO DBLINK-VIEW-NAME-CURRENT.            276 BRTN
YYY991     MOVE DBLINK-SDES-044-KEY TO DBLINK-VIEW-200-CURRENT.         276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET SDR-SDES                                                    276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE SDE-FK-SDR-SR-NAM TO DBLINK-SDR-SDES-01.                276 BRTN
YYY991     MOVE SDE-FK-SDR-PKEY-TS TO DBLINK-SDR-SDES-02.               276 BRTN
YYY991     MOVE SDE-FK-SDR-SR-SDR TO DBLINK-SDR-SDES-03.                276 BRTN
YYY991     MOVE SDE-SDR-TS TO DBLINK-SDR-SDES-04.                       276 BRTN
