      *%---------------------------------------------------------------*
      *%                                                               *
      *%                   200-FSYIA04 ROUTINE                         *
      *%                                                               *
      *%---------------------------------------------------------------*
       200-FSYIA04.

      ******************************************************************
      ***  VALIDATE ADD IN APPROVAL PROCESS
      ***  THIS INCLUDE MODULE MUST BE PART OF A SUBROUTINE.
      ***  THE FOLLOWING FIELDS MUST BE SET BY THE DIALOG THAT USES THIS
      * INCLUDE
      ***      UNVRS-CODE-SY08, SEQ-NMBR-SY08
      ***  THE FOLLOWING SUBROUTINES MUST BE PART OF THE DIALOG THAT USE
      *S THIS
      ***      APRVLERR
      ******************************************************************
           MOVE UNVRS-CODE-SY08 TO UNVRS-CODE-4203.
           MOVE SEQ-NMBR-SY08 TO SEQ-NMBR-4203.
      *%   OBTAIN CALC R4203-DOCUMENT.
           MOVE 0129 TO DBLINK-CURR-IO-NUM
           MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
           MOVE DBLINK-CALC TO DBLINK-TYPE-GET
           MOVE SEQ-NMBR-4203 OF R4203-DOCUMENT TO DOC-SEQ-NBR
           PERFORM 906-05-R4203-DOCUMENT
              THRU 906-05-R4203-DOCUMENT-EXIT
           PERFORM 999-SQL-STATUS
              THRU 999-SQL-STATUS-EXIT.
           IF DB-REC-NOT-FOUND
               MOVE 001546 TO MSG-ID-SY00
               PERFORM 200-APRVLERR-S10
                  THRU 200-APRVLERR-S10-EXIT
               IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                  GO TO 200-FSYIA04-EXIT
               END-IF
      *** DOCUMENT NOT DEFINED IN THE SYSTEM
               GO TO 200-FSYIA04-EXIT
           END-IF.
      *
           MOVE AGR-USER-ID TO USER-ID-4204.
      *%   FIND R4204-APRVL-J WITHIN S-4203-4204 USING USER-ID-4204.
           MOVE 0130 TO DBLINK-CURR-IO-NUM
           MOVE DBLINK-FIND TO DBLINK-TYPE-SELECT
           MOVE DBLINK-SORTED TO DBLINK-TYPE-GET
           MOVE USER-ID-4204 TO DBLINK-S-4203-4204-KEY-U
           PERFORM 906-02-S-4203-4204
              THRU 906-02-S-4203-4204-EXIT
           PERFORM 999-SQL-STATUS
              THRU 999-SQL-STATUS-EXIT.
           IF DB-REC-NOT-FOUND
               MOVE 001200 TO MSG-ID-SY00
               PERFORM 200-APRVLERR-S10
                  THRU 200-APRVLERR-S10-EXIT
               IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                  GO TO 200-FSYIA04-EXIT
               END-IF
      *** APPROVAL HIERARCHY UNAVAILABLE FOR USE
               GO TO 200-FSYIA04-EXIT
           END-IF.
      *
           IF APRVL-TMPLT-CODE-SY08 NOT = SPACES
               MOVE APRVL-TMPLT-CODE-SY08 TO APRVL-TMPLT-CODE-4215
      *%       OBTAIN R4215-APRV-TMPLT WITHIN S-4204-4215 USING
      *%        APRVL-TMPLT-CODE-4215
               MOVE 0131 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-SORTED TO DBLINK-TYPE-GET
               MOVE APRVL-TMPLT-CODE-4215 TO DBLINK-S-4204-4215-KEY-U
               PERFORM 906-02-S-4204-4215
                  THRU 906-02-S-4204-4215-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
               IF DB-REC-NOT-FOUND
                   MOVE 002289 TO MSG-ID-SY00
                   PERFORM 200-APRVLERR-S10
                      THRU 200-APRVLERR-S10-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYIA04-EXIT
                   END-IF
      *** APPROVAL TEMPLATE UNAVAILABLE
                   GO TO 200-FSYIA04-EXIT
               END-IF
           ELSE
      *%       OBTAIN FIRST R4215-APRV-TMPLT WITHIN S-4204-4215
               MOVE 0132 TO DBLINK-CURR-IO-NUM
               MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
               MOVE DBLINK-FIRST TO DBLINK-TYPE-GET
               PERFORM 906-00N-S-4204-4215
                  THRU 906-00N-S-4204-4215-EXIT
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
               IF DB-REC-NOT-FOUND
                   MOVE 002289 TO MSG-ID-SY00
                   PERFORM 200-APRVLERR-S10
                      THRU 200-APRVLERR-S10-EXIT
                   IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                      GO TO 200-FSYIA04-EXIT
                   END-IF
      *** APPROVAL TEMPLATE UNAVAILABLE
                   GO TO 200-FSYIA04-EXIT
               END-IF
               MOVE APRVL-TMPLT-CODE-4215 TO APRVL-TMPLT-CODE-SY08
           END-IF.
      *
      *%   OBTAIN NEXT R4205-APRVL-LVL WITHIN S-4215-4205.
           MOVE 0133 TO DBLINK-CURR-IO-NUM
           MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
           MOVE DBLINK-NEXT TO DBLINK-TYPE-GET
           PERFORM 906-00N-S-4215-4205
              THRU 906-00N-S-4215-4205-EXIT
           PERFORM 999-SQL-STATUS
              THRU 999-SQL-STATUS-EXIT.
           IF DB-END-OF-SET
               MOVE 001200 TO MSG-ID-SY00
               PERFORM 200-APRVLERR-S10
                  THRU 200-APRVLERR-S10-EXIT
               IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                  GO TO 200-FSYIA04-EXIT
               END-IF
      *** APPROVAL HIERARCHY UNAVAILABLE FOR USE
               GO TO 200-FSYIA04-EXIT
           END-IF.
      *

       200-FSYIA04-EXIT.
           EXIT.
