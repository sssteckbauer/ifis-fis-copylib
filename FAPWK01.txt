      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWK01                              04/24/00  12:15  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWK01.
           02  DCMNT-NMBR-4150-AP01                     PIC X(8).
           02  PO-NMBR-4150-AP01                        PIC X(8).
           02  VNDR-CODE-4073-AP01.
               03  VNDR-ID-DIGIT-ONE-4073-AP01          PIC X(1).
               03  VNDR-ID-LAST-NINE-4073-AP01          PIC X(9).
           02  EFCTV-RCRD-KEY-4154-AP01.
               03  KEY-TAX-TYPE-4154-AP01               PIC X(1).
               03  KEY-TAX-CODE-IND-4154-AP01           PIC X(1).
               03  KEY-START-DATE-4154-AP01   COMP-3    PIC S9(8).
               03  KEY-TIME-STAMP-4154-AP01             PIC X(6).
DEVMLJ     02  TRD-IN-AMT-NUM-AP01            COMP-3    PIC 9(10)V99.
