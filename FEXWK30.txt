      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FEXWK30                              04/24/00  12:29  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FEXWK30.
           02  VNDR-CODE-EX30.
               03  VNDR-ID-DIGIT-ONE-EX30               PIC X(1).
               03  VNDR-ID-LAST-NINE-EX30               PIC X(9).
           02  VNDR-NAME-EX30                           PIC X(35).
           02  CMDTY-CODE-EX30                          PIC X(8).
           02  CMDTY-DESC-EX30                          PIC X(35).
           02  ACCT-CODE-EX30                           PIC X(6).
           02  ACCT-CODE-TITLE-EX30                     PIC X(35).
           02  FILE-NMBR-EX30                           PIC 9(5).
           02  ADR-TYPE-CODE-EX30                       PIC X(2).
