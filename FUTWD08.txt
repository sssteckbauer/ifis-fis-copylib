      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FUTWD08                              04/24/00  13:23  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FUTWD08.
           02  SAVE-MINUS-ONE-UT08            COMP      PIC S9(8).
           02  SAVE-CLAUSE-DESC-UT08        OCCURS 14   PIC X(35).
           02  SYSTEM-DATE-UT08               COMP-3    PIC S9(8).
           02  SAVE-PLUS-ONE-UT08             COMP      PIC S9(8).
           02  SAVE-PAGE-STATUS-UT08                    PIC X(1).
           02  TEXT-TYPE-UT08                           PIC X(7).
           02  FIRST-CLAUSE-FLAG-UT08                   PIC X(1).
           02  FLAG-1-UT08                              PIC X(1).
           02  REFRESH-SW-UT08                          PIC X(1).
           02  COMMENT-START-UT08                       PIC X(1).
           02  COMMENT-END-UT08                         PIC X(1).
           02  DATA-FLAG-UT08                           PIC X(1).
           02  TOTAL-C-O-UT08                           PIC 9(3).
           02  TOTAL-C-O-WK-UT08                        PIC 9(3).
           02  TOTAL-ITEMS-UT08               COMP-3    PIC 9(4).
           02  TOTAL-ITEMS-WK-UT08            COMP-3    PIC 9(4).
           02  SAVE-PAGE-NUM-UT08             COMP-3    PIC S9(4).
           02  SAVE-SUB-UT08                  COMP-3    PIC 9(3).
           02  PRINT-FLAG-UT08                          PIC X(1).
           02  SUB-2-UT08                               PIC 9(3).
           02  SUB-4-UT08                               PIC 9(3).
           02  SUB-5-UT08                     COMP-3    PIC 9(4).
