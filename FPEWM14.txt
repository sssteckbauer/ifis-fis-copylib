      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPEWM14                              04/24/00  12:34  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPEWM14.
           02  ACTN-CODE-GROUP-PE14.
               03  ACTN-CODE-PE14           OCCURS 10   PIC X(1).
           02  LONG-DESC-6137-PE14          OCCURS 10   PIC X(30).
           02  ADR-TYPE-CODE-6137-PE14      OCCURS 10   PIC X(2).
           02  NAME-KEY-6117-PE14                       PIC X(35).
           02  MAP-START-DATE-6139-PE14     OCCURS 10   PIC X(6).
           02  MAP-END-DATE-6139-PE14       OCCURS 10   PIC X(6).
