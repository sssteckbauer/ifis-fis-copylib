    ***Created by Convert/DC version V8R03 on 12/12/00 at 12:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY01-Z = 'Y'
               MOVE WK01-TEXT-SY01 TO DCSD-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0A-Z = 'Y'
               MOVE WK01-TEXT-SY0A TO CNFDL-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0B-Z = 'Y'
               MOVE WK01-TEXT-SY0B TO HOLDS-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH32-Z = 'Y'
               MOVE WK01-CODE-CH32 TO ACTN-CODE-CH32 OF FCHWM32
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4000-CH32-Z = 'Y'
               MOVE WK01-CODE-4000-CH32 TO COA-CODE-4000-CH32 OF FCHWK32
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-4021-CH32-Z = 'Y'
               MOVE WK01-TYPE-CODE-4021-CH32 TO
                ACCT-TYPE-CODE-4021-CH32 OF FCHWK32
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-DESC-4021-CH32-Z = 'Y'
               MOVE WK01-TYPE-DESC-4021-CH32 TO
                ACCT-TYPE-DESC-4021-CH32 OF FCHWM32
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-TYPE-CD-4021-CH32-Z = 'Y'
               MOVE WK01-ACCT-TYPE-CD-4021-CH32 TO
                CNTRL-ACCT-TYPE-CODE-4021-CH32 OF FCHWK32
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-LONG-DESC-4021-CH32-Z = 'Y'
               MOVE WK01-LONG-DESC-4021-CH32 TO
                STNDD-LONG-DESC-4021-CH32 OF FCHWM32
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4022-CH32-Z = 'Y'
               MOVE WK01-DATE-4022-CH32 TO START-DATE-4022-CH32 OF
                FCHWK32
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CHNG-DATE-CH32-Z = 'Y'
               MOVE WK01-CHNG-DATE-CH32 TO NEXT-CHNG-DATE-CH32 OF
                FCHWM32
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4022-CH32-Z = 'Y'
               MOVE WK01-DESC-4022-CH32 TO STATUS-DESC-4022-CH32 OF
                FCHWM32
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4022-CH3A-Z = 'Y'
               MOVE WK01-DATE-4022-CH3A TO END-DATE-4022-CH32 OF FCHWM32
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4021-CH32-Z = 'Y'
               MOVE WK01-DATE-4021-CH32 TO ACTVY-DATE-4021-CH32 OF
                FCHWM32
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STAMP-4022-CH32-Z = 'Y'
               MOVE WK01-STAMP-4022-CH32 TO TIME-STAMP-4022-CH32 OF
                FCHWM32
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-CH32-Z = 'Y'
               MOVE WK01-PM-FLAG-CH32 TO AM-PM-FLAG-CH32 OF FCHWM32
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-CODE-4022-CH32-Z = 'Y'
               MOVE WK01-ACCT-CODE-4022-CH32 TO
                CNTRL-ACCT-CODE-4022-CH32 OF FCHWM32
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-TITLE-4022-CH32-Z = 'Y'
               MOVE WK01-ACCT-TITLE-4022-CH32 TO
                CNTRL-ACCT-TITLE-4022-CH32 OF FCHWM32
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-CODE-4022-CH3A-Z = 'Y'
               MOVE WK01-ACCT-CODE-4022-CH3A TO
                OFST-ACCT-CODE-4022-CH32 OF FCHWM32
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-TITLE-4022-CH3A-Z = 'Y'
               MOVE WK01-ACCT-TITLE-4022-CH3A TO
                OFST-ACCT-TITLE-4022-CH32 OF FCHWM32
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-YR-ACCT-CODE-4022-CH32-Z = 'Y'
               MOVE WK01-YR-ACCT-CODE-4022-CH32 TO
                PRIOR-YR-ACCT-CODE-4022-CH32 OF FCHWM32
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-YR-ACCT-TITL-4022-CH32-Z = 'Y'
               MOVE WK01-YR-ACCT-TITL-4022-CH32 TO
                PRIOR-YR-ACCT-TITLE-4022-CH32 OF FCHWM32
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-YR-OFST-CODE-4022-CH32-Z = 'Y'
               MOVE WK01-YR-OFST-CODE-4022-CH32 TO
                PRIOR-YR-OFST-CODE-4022-CH32 OF FCHWM32
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-YR-OFST-TITL-4022-CH32-Z = 'Y'
               MOVE WK01-YR-OFST-TITL-4022-CH32 TO
                PRIOR-YR-OFST-TITLE-4022-CH32 OF FCHWM32
           END-IF.
