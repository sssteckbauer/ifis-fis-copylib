    ***Created by Convert/DC version V8R03 on 11/20/00 at 13:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PU02-Z = 'Y'
               MOVE WK01-CODE-PU02 TO ACTN-CODE-PU02 OF FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4070-PU02-Z = 'Y'
               MOVE WK01-CODE-4070-PU02 TO RQST-CODE-4070-PU02 OF
                FPUWK02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4070-PU02-Z = 'Y'
               MOVE WK01-DATE-4070-PU02 TO TRANS-DATE-4070-PU02 OF
                FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-4070-PU02-Z = 'Y'
               MOVE WK01-NAME-4070-PU02 TO RQST-NAME-4070-PU02 OF
                FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4070-PU0A-Z = 'Y'
               MOVE WK01-DATE-4070-PU0A TO DLVRY-DATE-4070-PU02 OF
                FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CMPLT-IND-4070-PU02-Z = 'Y'
               MOVE WK01-CMPLT-IND-4070-PU02 TO
                RQST-CMPLT-IND-4070-PU02 OF FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-4070-PU02-Z = 'Y'
               MOVE WK01-ERROR-IND-4070-PU02 TO HDR-ERROR-IND-4070-PU02
                OF FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CNTR-DTL-4070-PU02-Z = 'Y'
               MOVE WK01-CNTR-DTL-4070-PU02 TO HDR-CNTR-DTL-4070-PU02
                OF FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CNTR-ACCT-4070-PU02-Z = 'Y'
               MOVE WK01-CNTR-ACCT-4070-PU02 TO HDR-CNTR-ACCT-4070-PU02
                OF FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4070-PU0B-Z = 'Y'
               MOVE WK01-DATE-4070-PU0B TO ACTVY-DATE-4070-PU02 OF
                FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AREA-CODE-4070-PU02-Z = 'Y'
               MOVE WK01-AREA-CODE-4070-PU02 TO
                TLPHN-AREA-CODE-4070-PU02 OF FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-XCHNG-ID-4070-PU02-Z = 'Y'
               MOVE WK01-XCHNG-ID-4070-PU02 TO TLPHN-XCHNG-ID-4070-PU02
                OF FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4070-PU02-Z = 'Y'
               MOVE WK01-IND-4070-PU02 TO CNCL-IND-4070-PU02 OF FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TOTAL-4070-PU02-Z = 'Y'
               MOVE WK01-TOTAL-4070-PU02 TO RQST-TOTAL-4070-PU02 OF
                FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4070-PU0A-Z = 'Y'
               MOVE WK01-IND-4070-PU0A TO APRVL-IND-4070-PU02 OF FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4070-PU0A-Z = 'Y'
               MOVE WK01-CODE-4070-PU0A TO ORGZN-CODE-4070-PU02 OF
                FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4070-PU0B-Z = 'Y'
               MOVE WK01-CODE-4070-PU0B TO COA-CODE-4070-PU02 OF FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4010-PU02-Z = 'Y'
               MOVE WK01-TITLE-4010-PU02 TO ORGZN-TITLE-4010-PU02 OF
                FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4070-PU0C-Z = 'Y'
               MOVE WK01-DATE-4070-PU0C TO CNCL-DATE-4070-PU02 OF
                FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-ID-4070-PU02-Z = 'Y'
               MOVE WK01-SEQ-ID-4070-PU02 TO TLPHN-SEQ-ID-4070-PU02 OF
                FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-XTNSN-ID-4070-PU02-Z = 'Y'
               MOVE WK01-XTNSN-ID-4070-PU02 TO TLPHN-XTNSN-ID-4070-PU02
                OF FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PU02-Z = 'Y'
               MOVE WK01-PU02 TO PRINTER-PU02 OF FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-KEY-PU02-Z = 'Y'
               MOVE WK01-NAME-KEY-PU02 TO VNDR-NAME-KEY-PU02 OF FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-PU02-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-PU02 TO VNDR-ID-LAST-NINE-PU02 OF
                FPUWM02
           END-IF.
DEVMJM*
      *%--------------------------------------------------------------%*
           IF WK01-EMAIL-ADR-4070-PU02-Z = 'Y'
               MOVE WK01-EMAIL-ADR-4070-PU02
                 TO EMAIL-ADR-4070-PU02 OF FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PREV-PO-NBR-4070-PU02-Z = 'Y'
               MOVE WK01-PREV-PO-NBR-4070-PU02
                 TO PREV-PO-NBR-4070-PU02 OF FPUWM02
           END-IF.
DEVMJM*
      *%--------------------------------------------------------------%*
           IF WK01-FAX-AREA-CODE-4070-PU02-Z = 'Y'
               MOVE WK01-FAX-AREA-CODE-4070-PU02 TO
                FAX-TLPHN-AREA-CODE-4070-PU02 OF FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FAX-XCHNG-ID-4070-PU02-Z = 'Y'
               MOVE WK01-FAX-XCHNG-ID-4070-PU02 TO
                FAX-TLPHN-XCHNG-ID-4070-PU02 OF FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FAX-SEQ-ID-4070-PU02-Z = 'Y'
               MOVE WK01-FAX-SEQ-ID-4070-PU02 TO
                FAX-TLPHN-SEQ-ID-4070-PU02 OF FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-VDR-EMAIL-ADR-4070-PU02-Z = 'Y'
               MOVE WK01-VDR-EMAIL-ADR-4070-PU02
                 TO VDR-EMAIL-ADR-4070-PU02 OF FPUWM02
001MLJ     END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RQST-4070-PU02-Z = 'Y'
               MOVE WK01-RQST-4070-PU02 TO PRINT-RQST-4070-PU02 OF
                FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-PU02-Z = 'Y'
               MOVE WK01-FLAG-PU02 TO TEXT-FLAG-PU02 OF FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PU0A-Z = 'Y'
               MOVE WK01-PU0A TO MAILCODE-PU02 OF FPUWM02
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TMPLT-CODE-PU02-Z = 'Y'
               MOVE WK01-TMPLT-CODE-PU02 TO APRVL-TMPLT-CODE-PU02 OF
                FPUWM02
           END-IF.
