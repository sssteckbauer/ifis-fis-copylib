       976-FROM-R4096-PO-HDR-SEQ SECTION.

FCCCE**        MOVE USER-CODE-4096 OF R4096-PO-HDR-SEQ
FCCCE          MOVE DBLINK-USER-CD
               TO POS-USER-CD.

               MOVE LAST-ACTVY-DATE-4096 OF R4096-PO-HDR-SEQ
               TO POS-LAST-ACTVY-DT.

               MOVE CHNG-SEQ-NMBR-4096 OF R4096-PO-HDR-SEQ
               TO POS-CHNG-SEQ-NBR.

               MOVE PO-CMPLT-IND-4096 OF R4096-PO-HDR-SEQ
               TO POS-PO-CMPLT-IND.

           IF PRINT-DATE-4096 OF R4096-PO-HDR-SEQ  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO POS-PRINT-DT
           ELSE
               MOVE PRINT-DATE-4096 OF R4096-PO-HDR-SEQ
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO POS-PRINT-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO POS-PRINT-DT
               END-IF
           END-IF.

               MOVE PO-PRINT-FLAG-4096 OF R4096-PO-HDR-SEQ
               TO POS-PO-PRINT-FLAG.

           IF DLVRY-BY-DATE-4096 OF R4096-PO-HDR-SEQ  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO POS-DLVRY-BY-DT
           ELSE
               MOVE DLVRY-BY-DATE-4096 OF R4096-PO-HDR-SEQ
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO POS-DLVRY-BY-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO POS-DLVRY-BY-DT
               END-IF
           END-IF.

           IF ACKNWDG-DATE-4096 OF R4096-PO-HDR-SEQ  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO POS-ACKNWDG-DT
           ELSE
               MOVE ACKNWDG-DATE-4096 OF R4096-PO-HDR-SEQ
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO POS-ACKNWDG-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO POS-ACKNWDG-DT
               END-IF
           END-IF.

           IF ORDER-DATE-4096 OF R4096-PO-HDR-SEQ  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO POS-ORDER-DT
           ELSE
               MOVE ORDER-DATE-4096 OF R4096-PO-HDR-SEQ
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO POS-ORDER-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO POS-ORDER-DT
               END-IF
           END-IF.

               MOVE APRVL-IND-4096 OF R4096-PO-HDR-SEQ
               TO POS-APRVL-IND.

           IF HDR-CNTR-DTL-4096 OF R4096-PO-HDR-SEQ  NOT NUMERIC
               MOVE ZEROS TO POS-HDR-CNTR-DTL
           ELSE
               MOVE HDR-CNTR-DTL-4096 OF R4096-PO-HDR-SEQ
               TO POS-HDR-CNTR-DTL
           END-IF.

           IF HDR-CNTR-ACCT-4096 OF R4096-PO-HDR-SEQ  NOT NUMERIC
               MOVE ZEROS TO POS-HDR-CNTR-ACCT
           ELSE
               MOVE HDR-CNTR-ACCT-4096 OF R4096-PO-HDR-SEQ
               TO POS-HDR-CNTR-ACCT
           END-IF.

               MOVE HDR-ERROR-IND-4096 OF R4096-PO-HDR-SEQ
               TO POS-HDR-ERROR-IND.

           IF TOTAL-AMT-4096 OF R4096-PO-HDR-SEQ  NOT NUMERIC
               MOVE ZEROS TO POS-TOTAL-AMT
           ELSE
               MOVE TOTAL-AMT-4096 OF R4096-PO-HDR-SEQ
               TO POS-TOTAL-AMT
           END-IF.

           IF ACTVY-DATE-4096 OF R4096-PO-HDR-SEQ  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO POS-ACTVY-DT
           ELSE
               MOVE ACTVY-DATE-4096 OF R4096-PO-HDR-SEQ
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO POS-ACTVY-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO POS-ACTVY-DT
               END-IF
           END-IF.

               MOVE CNCL-IND-4096 OF R4096-PO-HDR-SEQ
               TO POS-CNCL-IND.

           IF CNCL-DATE-4096 OF R4096-PO-HDR-SEQ  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO POS-CNCL-DT
           ELSE
               MOVE CNCL-DATE-4096 OF R4096-PO-HDR-SEQ
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO POS-CNCL-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO POS-CNCL-DT
               END-IF
           END-IF.

           IF ADDL-AMT-4096 OF R4096-PO-HDR-SEQ  NOT NUMERIC
               MOVE ZEROS TO POS-ADDL-AMT
           ELSE
               MOVE ADDL-AMT-4096 OF R4096-PO-HDR-SEQ
               TO POS-ADDL-AMT
           END-IF.

           IF DSCNT-AMT-4096 OF R4096-PO-HDR-SEQ  NOT NUMERIC
               MOVE ZEROS TO POS-DSCNT-AMT
           ELSE
               MOVE DSCNT-AMT-4096 OF R4096-PO-HDR-SEQ
               TO POS-DSCNT-AMT
           END-IF.

           IF ITEM-COUNT-4096 OF R4096-PO-HDR-SEQ  NOT NUMERIC
               MOVE ZEROS TO POS-ITEM-COUNT
           ELSE
               MOVE ITEM-COUNT-4096 OF R4096-PO-HDR-SEQ
               TO POS-ITEM-COUNT
           END-IF.

               MOVE BUYER-CODE-4096 OF R4096-PO-HDR-SEQ
               TO POS-BUYER-CD.

               MOVE VNDR-CNTCT-NAME-4096 OF R4096-PO-HDR-SEQ
               TO POS-VNDR-CNTCT-NM.

               MOVE MAILCODE-4096 OF R4096-PO-HDR-SEQ
               TO POS-MAILCODE.

           IF PO-DSCNT-AMT-4096 OF R4096-PO-HDR-SEQ  NOT NUMERIC
               MOVE ZEROS TO POS-PO-DSCNT-AMT
           ELSE
               MOVE PO-DSCNT-AMT-4096 OF R4096-PO-HDR-SEQ
               TO POS-PO-DSCNT-AMT
           END-IF.

               MOVE DSCNT-BEFORE-TAX-IND-4096 OF R4096-PO-HDR-SEQ
               TO POS-DSCNT-BFR-TX-IND.

           IF PO-DSCNT-PCT-4096 OF R4096-PO-HDR-SEQ  NOT NUMERIC
               MOVE ZEROS TO POS-PO-DSCNT-PCT
           ELSE
               MOVE PO-DSCNT-PCT-4096 OF R4096-PO-HDR-SEQ
               TO POS-PO-DSCNT-PCT
           END-IF.

               MOVE TAX-CODE-4096 OF R4096-PO-HDR-SEQ
               TO POS-TAX-CD.

               MOVE DSCNT-CODE-4096 OF R4096-PO-HDR-SEQ
               TO POS-DSCNT-CD.

               MOVE PYMT-CODE-4096 OF R4096-PO-HDR-SEQ
               TO POS-PYMT-CD.

               MOVE SHIP-TO-CODE-4096 OF R4096-PO-HDR-SEQ
               TO POS-SHIP-TO-CD.

               MOVE TRNST-RISK-CODE-4096 OF R4096-PO-HDR-SEQ
               TO POS-TRNST-RISK-CD.

               MOVE ACKNWDG-IND-4096 OF R4096-PO-HDR-SEQ
               TO POS-ACKNWDG-IND.

               MOVE ADR-TYPE-CODE-4096 OF R4096-PO-HDR-SEQ
               TO POS-ADR-TYP-CD.

           IF END-DATE-4096 OF R4096-PO-HDR-SEQ  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO POS-END-DT
           ELSE
               MOVE END-DATE-4096 OF R4096-PO-HDR-SEQ
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO POS-END-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO POS-END-DT
               END-IF
           END-IF.

               MOVE ACCT-INDX-CODE-4096 OF R4096-PO-HDR-SEQ
               TO POS-ACCT-INDX-CD.
       976-FROM-R4096-PO-HDR-SEQ-EXIT.
           EXIT.
