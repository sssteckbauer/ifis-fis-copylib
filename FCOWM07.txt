      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCOWM07                              04/24/00  12:28  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCOWM07.
           02  COMM-VIEW-6257-CO07                      PIC X(8).
           02  LONG-DESC-6257-CO07                      PIC X(30).
           02  COMM-TYPE-6258-CO07                      PIC X(4).
           02  LONG-DESC-6258-CO07                      PIC X(30).
           02  START-DATE-6259-CO07                     PIC X(6).
           02  LONG-DESC-6259-CO07                      PIC X(30).
           02  END-DATE-6259-CO07                       PIC X(6).
           02  ACTN-CODE-GROUP-CO07.
               03  ACTN-CODE-CO07           OCCURS 9    PIC X.
           02  TEXT-CODE-SEQ-ID-6270-CO07   OCCURS 9    PIC X(3).
           02  TEXT-CODE-6260-CO07          OCCURS 9    PIC X(4).
           02  SHORT-DESC-6260-CO07         OCCURS 9    PIC X(10).
           02  LONG-DESC-6260-CO07          OCCURS 9    PIC X(30).
           02  PLUS-DAYS-6270-CO07          OCCURS 9    PIC X(3).
           02  TEXT-FLAG-6261-CO07          OCCURS 9    PIC X(1).
           02  DIFF-BATCH-FLAG-6270-CO07    OCCURS 9    PIC X(1).
           02  SAME-BATCH-FLAG-6270-CO07    OCCURS 9    PIC X(1).
           02  DIFF-BATCH-MAX-6270-CO07     OCCURS 9    PIC X(3).
