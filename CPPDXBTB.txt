                                                                        CPPDXBTB
      *9000-BUDGET-TABLE-LOAD                                           CPPDXBTB
                                                                        CPPDXBTB
      *      **  STANDARD BUDGET TABLE LOAD ROUTINE.                    CPPDXBTB
      *      **  ACCEPTS TABLE-NO-ARG = ID OF TABLE TO BE LOADED.       CPPDXBTB
      *      **  RETURNS TABLE-LOAD-ERROR-FLAG =                        CPPDXBTB
      *      **     'N' IF NO LOAD ERRORS (TABLE-LOAD-OK)               CPPDXBTB
      *      **     'Y' IF LOAD ERROR (TABLE-LOAD-ERROR)                CPPDXBTB
                                                                        CPPDXBTB
           IF BUDGET-TABLES-BEGIN                                       CPPDXBTB
               SET TAB-INDX TO +1                                       CPPDXBTB
               SET TAB-END-INDX TO +1                                   CPPDXBTB
               SET TAB-NO-INDX  TO +1                                   CPPDXBTB
               MOVE SPACES TO TAB-TABLE-NO                              CPPDXBTB
           ELSE                                                         CPPDXBTB
               SET TAB-INDX TO TAB-END-INDX.                            CPPDXBTB
           MOVE 'N' TO END-OF-TABLE-FLAG.                               CPPDXBTB
                                                                        CPPDXBTB
           PERFORM 9010-STORE-BUDGET-TABLE-ITEMS                        CPPDXBTB
               UNTIL END-OF-TABLE  OR  TABLE-LOAD-ERROR.                CPPDXBTB
                                                                        CPPDXBTB
           IF TABLE-LOAD-OK                                             CPPDXBTB
               IF  TAB-INDX = TAB-END-INDX                              CPPDXBTB
                   DISPLAY 'TABLE NO ARG - ' TABLE-NO-ARG               CPPDXBTB
                   DISPLAY  'NO ENTRIES IN TABLE FILE'                  CPPDXBTB
                   MOVE 'Y' TO TABLE-LOAD-ERROR-FLAG                    CPPDXBTB
               ELSE                                                     CPPDXBTB
                   MOVE TABLE-NO-ARG  TO INDX-TABLE-NO  (TAB-NO-INDX)   CPPDXBTB
                   SET INDX-TABLE-BEG (TAB-NO-INDX) TO TAB-INDX         CPPDXBTB
                   SET INDX-TABLE-END (TAB-NO-INDX) TO TAB-END-INDX     CPPDXBTB
                   SUBTRACT +1 FROM INDX-TABLE-END (TAB-NO-INDX)        CPPDXBTB
                   DISPLAY 'TABLE NO ARG - ' TABLE-NO-ARG               CPPDXBTB
                   DISPLAY 'INDX TABLE BEG - '                          CPPDXBTB
                           INDX-TABLE-BEG (TAB-NO-INDX)                 CPPDXBTB
                   DISPLAY 'INDX TABLE END - '                          CPPDXBTB
                           INDX-TABLE-END (TAB-NO-INDX)                 CPPDXBTB
                   SET TAB-NO-INDX UP BY +1.                            CPPDXBTB
                                                                        CPPDXBTB
                                                                        CPPDXBTB
                                                                        CPPDXBTB
       9010-STORE-BUDGET-TABLE-ITEMS.                                   CPPDXBTB
                                                                        CPPDXBTB
           IF  TAB-TABLE-NO NOT = TABLE-NO-ARG                          CPPDXBTB
               PERFORM 9020-READ-BUDGET-TABLES-FILE.                    CPPDXBTB
                                                                        CPPDXBTB
           IF TABLE-LOAD-ERROR                                          CPPDXBTB
               NEXT SENTENCE                                            CPPDXBTB
           ELSE                                                         CPPDXBTB
           IF (BUDGET-TABLES-EOF  OR                                    CPPDXBTB
               TAB-TABLE-NO GREATER THAN TABLE-NO-ARG)                  CPPDXBTB
               MOVE 'Y' TO END-OF-TABLE-FLAG                            CPPDXBTB
           ELSE                                                         CPPDXBTB
           IF  TAB-TABLE-NO = TABLE-NO-ARG                              CPPDXBTB
               SET TABLE-SIZE TO TAB-END-INDX                           CPPDXBTB
               IF TABLE-SIZE-OVERFLOW                                   CPPDXBTB
                   DISPLAY 'TABLE NO ARG - ' TABLE-NO-ARG               CPPDXBTB
                   DISPLAY 'INTERNAL TABLE SIZE EXCEEDED'               CPPDXBTB
                   MOVE 'Y' TO TABLE-LOAD-ERROR-FLAG                    CPPDXBTB
               ELSE                                                     CPPDXBTB
                                                                        CPPDXBTB
               MOVE TAB-TABLE-NO    TO TABLE-NO         (TAB-END-INDX)  CPPDXBTB
               MOVE TAB-RANGE-FROM  TO TABLE-RANGE-FROM (TAB-END-INDX)  CPPDXBTB
               MOVE TAB-RANGE-TO    TO TABLE-RANGE-TO   (TAB-END-INDX)  CPPDXBTB
               MOVE TAB-CLASS-CODE  TO TABLE-CLASS-CODE (TAB-END-INDX)  CPPDXBTB
               MOVE TAB-CLASS-TITLE TO TABLE-CLASS-TITLE                CPPDXBTB
                                                        (TAB-END-INDX)  CPPDXBTB
               MOVE SPACES          TO TAB-TABLE-NO                     CPPDXBTB
               SET TAB-END-INDX UP BY +1.                               CPPDXBTB
       9020-READ-BUDGET-TABLES-FILE.                                    CPPDXBTB
                                                                        CPPDXBTB
           IF  BUDGET-TABLES-BEGIN                                      CPPDXBTB
               MOVE 'R' TO BUDGET-TABLES-FILE-STATUS-FLAG               CPPDXBTB
               READ BUDGET-TABLES-FILE INTO BUDGET-TABLES-RECORD        CPPDXBTB
                   AT END MOVE 'Y' TO TABLE-LOAD-ERROR-FLAG             CPPDXBTB
                          DISPLAY 'BUDGET TABLES EMPTY'                 CPPDXBTB
           ELSE                                                         CPPDXBTB
               READ BUDGET-TABLES-FILE INTO BUDGET-TABLES-RECORD        CPPDXBTB
                   AT END MOVE 'E' TO BUDGET-TABLES-FILE-STATUS-FLAG.   CPPDXBTB
                                                                        CPPDXBTB
                                                                        CPPDXBTB
       9100-BUDGET-TABLE-SEARCH.                                        CPPDXBTB
                                                                        CPPDXBTB
      *      **  STANDARD BUDGET TABLE SEARCH ROUTINE.                  CPPDXBTB
      *      **  SEARCHES TABLES LOADED BY STANDARD TABLE LOAD ROUTINE. CPPDXBTB
      *      **                                                         CPPDXBTB
      *      **  ACCEPTS TABLE-NO-ARG = ID OF TABLE TO BE SEARCHED      CPPDXBTB
      *      **          TABLE-ENTRY-ARG = SEARCH KEY                   CPPDXBTB
      *      **          TABLE-SRCH-TYPE-ARG =                          CPPDXBTB
      *      **                'MATCH' FOR A MATCH SEARCH               CPPDXBTB
      *      **                'RANGE' FOR A RANGE SEARCH               CPPDXBTB
      *      **  RETURNS TABLE-ENTRY-FOUND-FLAG =                       CPPDXBTB
      *      **                'Y' IF ENTRY IS FOUND                    CPPDXBTB
      *      **                'N' IF ENTRY IS NOT FOUND                CPPDXBTB
      *      **          TAB-INDX = TABLE LOCATION OF FOUND ENTRY       CPPDXBTB
      *      **          TABLE-SEARCH-ERROR-FLAG =                      CPPDXBTB
      *      **                'N'  IF  NO SEARCH ERRORS                CPPDXBTB
      *      **                'Y' IF SEARCH ERROR (TABLE-SEARCH-ERROR) CPPDXBTB
                                                                        CPPDXBTB
           MOVE 'N' TO TABLE-ENTRY-FOUND-FLAG.                          CPPDXBTB
           MOVE 'N' TO TABLE-SEARCH-ERROR-FLAG.                         CPPDXBTB
           SET TAB-NO-INDX TO +1.                                       CPPDXBTB
                                                                        CPPDXBTB
           SEARCH BUDGET-TABLE-INDEXES                                  CPPDXBTB
                   AT END MOVE 'Y' TO TABLE-SEARCH-ERROR-FLAG           CPPDXBTB
                         DISPLAY TABLE-NO-ARG ' BUDGET TABLE NOT LOADED'CPPDXBTB
                   WHEN INDX-TABLE-NO (TAB-NO-INDX) = TABLE-NO-ARG      CPPDXBTB
                       SET TAB-INDX TO INDX-TABLE-BEG (TAB-NO-INDX)     CPPDXBTB
                       SET TAB-END-INDX                                 CPPDXBTB
                                    TO INDX-TABLE-END (TAB-NO-INDX).    CPPDXBTB
                                                                        CPPDXBTB
           IF NOT TABLE-SEARCH-ERROR                                    CPPDXBTB
               IF TABLE-SEARCH-TYPE-ARG = 'MATCH'                       CPPDXBTB
                                                                        CPPDXBTB
                 SEARCH BUDGET-TABLE                                    CPPDXBTB
                   AT END MOVE 'N' TO TABLE-ENTRY-FOUND-FLAG            CPPDXBTB
                   WHEN TABLE-RANGE-FROM (TAB-INDX) = TABLE-ENTRY-ARG   CPPDXBTB
                        MOVE 'Y' TO TABLE-ENTRY-FOUND-FLAG              CPPDXBTB
                   WHEN TAB-INDX = TAB-END-INDX                         CPPDXBTB
                        MOVE 'N' TO TABLE-ENTRY-FOUND-FLAG              CPPDXBTB
                                                                        CPPDXBTB
               ELSE                                                     CPPDXBTB
               IF TABLE-SEARCH-TYPE-ARG = 'RANGE'                       CPPDXBTB
                                                                        CPPDXBTB
                 SEARCH BUDGET-TABLE                                    CPPDXBTB
                   AT END MOVE 'N' TO TABLE-ENTRY-FOUND-FLAG            CPPDXBTB
                   WHEN TABLE-RANGE-FROM (TAB-INDX) NOT GREATER THAN    CPPDXBTB
                                                     TABLE-ENTRY-ARG    CPPDXBTB
                   AND  TABLE-RANGE-TO   (TAB-INDX) NOT LESS    THAN    CPPDXBTB
                                                     TABLE-ENTRY-ARG    CPPDXBTB
                        MOVE 'Y' TO TABLE-ENTRY-FOUND-FLAG              CPPDXBTB
                   WHEN TAB-INDX = TAB-END-INDX                         CPPDXBTB
                        MOVE 'N' TO TABLE-ENTRY-FOUND-FLAG.             CPPDXBTB
