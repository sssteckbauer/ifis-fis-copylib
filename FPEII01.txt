      *%---------------------------------------------------------------*
      *%                                                               *
      *%                   200-FPEII01 ROUTINE                         *
      *%                                                               *
      *%---------------------------------------------------------------*
       200-FPEII01.

      ******************************************************************
      ***  RELEASM ALL RECORD LOCKS AND SET ALL CURRENCIES TO NULL
      ******************************************************************
      *%   COMMIT ALL.
           MOVE 0027 TO DBLINK-CURR-IO-NUM
           MOVE +0 TO SQLCODE
           MOVE ZERO TO ERROR-STATUS
           PERFORM 986-RESET-KEYS
              THRU 986-RESET-KEYS-EXIT
           PERFORM 996-COMMIT
              THRU 996-COMMIT-EXIT
           PERFORM 999-SQL-STATUS
              THRU 999-SQL-STATUS-EXIT.
      ******************************************************************
      ***  VALIDATE DEFAULT UNIVERSITY
      ***  UNIVERSITY HAS BEEN MOVED TO PE PROFILE THROUGH MENU PROCESSI
      *NG
      ******************************************************************
           MOVE UNVRS-CODE-6001-PE00 TO UNVRS-CODE-6001.
      *%   OBTAIN CALC R6001-UNVRS.
           MOVE 0028 TO DBLINK-CURR-IO-NUM
           MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
           MOVE DBLINK-CALC TO DBLINK-TYPE-GET
           MOVE UNVRS-CODE-6001 OF R6001-UNVRS TO UNV-UNVRS-CD
           PERFORM 906-05-R6001-UNVRS
              THRU 906-05-R6001-UNVRS-EXIT
           PERFORM 999-SQL-STATUS
              THRU 999-SQL-STATUS-EXIT.
      *** UNIVERSITY REC
           IF DB-REC-NOT-FOUND
      *** UNIVERSITY NOT FOUND FOR WHATEVER REASON
               MOVE SPACES TO MAP-ELMNT-NAME-SY00
               MOVE 990387 TO MSG-ID-SY00
               PERFORM 200-ZZYII01-GETMESGS
                  THRU 200-ZZYII01-GETMESGS-EXIT
               IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                  GO TO 200-FPEII01-EXIT
               END-IF
      ***  SYSTEM PROBLEM. CONTACT THE DATA CENTER
               PERFORM 200-DISPMAP-S01
                  THRU 200-DISPMAP-S01-EXIT
               IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                  GO TO 200-FPEII01-EXIT
               END-IF
           END-IF.
      *
      ******************************************************************

       200-FPEII01-EXIT.
           EXIT.
