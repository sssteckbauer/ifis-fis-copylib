       975-TO-R4224-DLVRY-HDR SECTION.
           MOVE DLH-USER-CD TO
               USER-CODE-4224 OF R4224-DLVRY-HDR
                                                                     .
           MOVE DLH-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4224 OF R4224-DLVRY-HDR
                                                                     .
           MOVE DLH-UNVRS-CD TO
               UNVRS-CODE-4224 OF R4224-DLVRY-HDR
                                                                     .
           MOVE DLH-DOC-REF-NBR TO
               DCMNT-REF-NMBR-4224 OF R4224-DLVRY-HDR
                                                                     .
           MOVE DLH-PO-ITEM-NBR TO
               PO-ITEM-NMBR-4224 OF R4224-DLVRY-HDR
                                                                     .
           MOVE DLH-RQST-CD TO
               RQST-CODE-4224 OF R4224-DLVRY-HDR
                                                                     .
           MOVE DLH-RQST-ITEM-NBR TO
               RQST-ITEM-NMBR-4224 OF R4224-DLVRY-HDR
                                                                     .
       975-TO-R4224-DLVRY-HDR-EXIT.
           EXIT.
