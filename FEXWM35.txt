      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FEXWM35                              04/24/00  12:29  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FEXWM35.
           02  ACTN-CODE-GROUP-EX35.
               03  ACTN-CODE-EX35           OCCURS 10   PIC X(1).
           02  CMDTY-CODE-EX35              OCCURS 10   PIC X(8).
           02  FILE-NMBR-EX35               OCCURS 10   PIC 9(5).
           02  ACCT-CODE-EX35               OCCURS 10   PIC X(6).
           02  HDR-ERROR-IND-EX35           OCCURS 10   PIC X(1).
           02  START-DATE-EX35       COMP-3 OCCURS 10   PIC S9(8).
           02  END-DATE-EX35         COMP-3 OCCURS 10   PIC S9(8).
