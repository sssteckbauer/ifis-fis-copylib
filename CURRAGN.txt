      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4056-AGNC' TO DBLINK-CURR-PARAGRAPH.         276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4056-AGNC' TO RECORD-NAME.                            276 BRTN
YYY991     MOVE 'F-TABLE' TO AREA-NAME.                                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4056-AGNC TO DBLINK-RECORD-MADE-CURRENT.        276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4056-AGNC-EXIT                           276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE AGN-IREF-ID TO DBLINK-R4056-AGNC-01                 276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4056-AGNC TO DBLINK-VIEW-NAME-CURRENT.          276 BRTN
YYY991     MOVE DBLINK-R4056-AGNC-KEY TO DBLINK-VIEW-200-CURRENT.       276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4056-4051-O                                               276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4056-AGNC-01 TO DBLINK-S-4056-4051-O-01.        276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4056-4051-O-KEY-M.               276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4056-4051-P                                               276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4056-AGNC-01 TO DBLINK-S-4056-4051-P-01.        276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4056-4051-P-KEY-M.               276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-AGNC                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE AGN-AGNCY-DGT-ONE TO DBLINK-S-INDX-AGNC-01.             276 BRTN
YYY991     MOVE AGN-AGNCY-LAST-NINE TO DBLINK-S-INDX-AGNC-02.           276 BRTN
