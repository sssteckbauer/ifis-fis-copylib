       01  R6157-PRSN-PRVID.
           02  DB-PROC-ID-6157.
               03  USER-CODE-6157                       PIC X(8).
               03  LAST-ACTVY-DATE-6157                 PIC X(5).
               03  TRMNL-ID-6157                        PIC X(8).
               03  PURGE-FLAG-6157                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  PRSN-ID-KEY-6157.
               03  UNVRS-CODE-6157                      PIC X(2).
               03  PRSN-ID-6157.
                   04  PRSN-ID-DIGIT-ONE-6157           PIC X(1).
                   04  PRSN-ID-LAST-NINE-6157           PIC X(9).
           02  CHANGE-DATE-6157               COMP-3    PIC 9(8).
