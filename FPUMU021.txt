    ***Created by Convert/DC version V8R03 on 11/20/00 at 13:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PU02 OF FPUWM02 TO WK01-CODE-PU02.
      *%--------------------------------------------------------------%*
           MOVE RQST-CODE-4070-PU02 OF FPUWK02 TO WK01-CODE-4070-PU02.
      *%--------------------------------------------------------------%*
           MOVE TRANS-DATE-4070-PU02 OF FPUWM02 TO WK01-DATE-4070-PU02.
      *%--------------------------------------------------------------%*
           MOVE RQST-NAME-4070-PU02 OF FPUWM02 TO WK01-NAME-4070-PU02.
      *%--------------------------------------------------------------%*
           MOVE DLVRY-DATE-4070-PU02 OF FPUWM02 TO WK01-DATE-4070-PU0A.
      *%--------------------------------------------------------------%*
           MOVE RQST-CMPLT-IND-4070-PU02 OF FPUWM02 TO
                WK01-CMPLT-IND-4070-PU02.
      *%--------------------------------------------------------------%*
           MOVE HDR-ERROR-IND-4070-PU02 OF FPUWM02 TO
                WK01-ERROR-IND-4070-PU02.
      *%--------------------------------------------------------------%*
           MOVE HDR-CNTR-DTL-4070-PU02 OF FPUWM02 TO
                WK01-CNTR-DTL-4070-PU02.
      *%--------------------------------------------------------------%*
           MOVE HDR-CNTR-ACCT-4070-PU02 OF FPUWM02 TO
                WK01-CNTR-ACCT-4070-PU02.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-DATE-4070-PU02 OF FPUWM02 TO WK01-DATE-4070-PU0B.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-AREA-CODE-4070-PU02 OF FPUWM02 TO
                WK01-AREA-CODE-4070-PU02.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XCHNG-ID-4070-PU02 OF FPUWM02 TO
                WK01-XCHNG-ID-4070-PU02.
      *%--------------------------------------------------------------%*
           MOVE CNCL-IND-4070-PU02 OF FPUWM02 TO WK01-IND-4070-PU02.
      *%--------------------------------------------------------------%*
           MOVE RQST-TOTAL-4070-PU02 OF FPUWM02 TO WK01-TOTAL-4070-PU02.
      *%--------------------------------------------------------------%*
           MOVE APRVL-IND-4070-PU02 OF FPUWM02 TO WK01-IND-4070-PU0A.
      *%--------------------------------------------------------------%*
           MOVE ORGZN-CODE-4070-PU02 OF FPUWM02 TO WK01-CODE-4070-PU0A.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4070-PU02 OF FPUWM02 TO WK01-CODE-4070-PU0B.
      *%--------------------------------------------------------------%*
           MOVE ORGZN-TITLE-4010-PU02 OF FPUWM02 TO
                WK01-TITLE-4010-PU02.
      *%--------------------------------------------------------------%*
           MOVE CNCL-DATE-4070-PU02 OF FPUWM02 TO WK01-DATE-4070-PU0C.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-SEQ-ID-4070-PU02 OF FPUWM02 TO
                WK01-SEQ-ID-4070-PU02.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XTNSN-ID-4070-PU02 OF FPUWM02 TO
                WK01-XTNSN-ID-4070-PU02.
      *%--------------------------------------------------------------%*
           MOVE PRINTER-PU02 OF FPUWM02 TO WK01-PU02.
      *%--------------------------------------------------------------%*
           MOVE VNDR-NAME-KEY-PU02 OF FPUWM02 TO WK01-NAME-KEY-PU02.
      *%--------------------------------------------------------------%*
           MOVE VNDR-ID-LAST-NINE-PU02 OF FPUWM02 TO
                WK01-ID-LAST-NINE-PU02.
      *%--------------------------------------------------------------%*
           MOVE PRINT-RQST-4070-PU02 OF FPUWM02 TO WK01-RQST-4070-PU02.
      *%--------------------------------------------------------------%*
           MOVE TEXT-FLAG-PU02 OF FPUWM02 TO WK01-FLAG-PU02.
DEVMJM*
      *%--------------------------------------------------------------%*
           MOVE EMAIL-ADR-4070-PU02 OF FPUWM02
             TO WK01-EMAIL-ADR-4070-PU02.
      *%--------------------------------------------------------------%*
           MOVE PREV-PO-NBR-4070-PU02 OF FPUWM02
             TO WK01-PREV-PO-NBR-4070-PU02.
DEVMJM*
      *%--------------------------------------------------------------%*
           MOVE FAX-TLPHN-AREA-CODE-4070-PU02 OF FPUWM02 TO
                WK01-FAX-AREA-CODE-4070-PU02.
      *%--------------------------------------------------------------%*
           MOVE FAX-TLPHN-XCHNG-ID-4070-PU02 OF FPUWM02 TO
                WK01-FAX-XCHNG-ID-4070-PU02.
      *%--------------------------------------------------------------%*
           MOVE FAX-TLPHN-SEQ-ID-4070-PU02 OF FPUWM02 TO
                WK01-FAX-SEQ-ID-4070-PU02.
      *%--------------------------------------------------------------%*
           MOVE VDR-EMAIL-ADR-4070-PU02 OF FPUWM02
001MLJ       TO WK01-VDR-EMAIL-ADR-4070-PU02.
      *%--------------------------------------------------------------%*
           MOVE MAILCODE-PU02 OF FPUWM02 TO WK01-PU0A.
      *%--------------------------------------------------------------%*
           MOVE APRVL-TMPLT-CODE-PU02 OF FPUWM02 TO
                WK01-TMPLT-CODE-PU02.
