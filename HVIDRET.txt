       975-TO-R4094-RETURN-DTL SECTION.
           MOVE RET-USER-CD TO
               USER-CODE-4094 OF R4094-RETURN-DTL
                                                                     .
           MOVE RET-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4094 OF R4094-RETURN-DTL
                                                                     .
           MOVE RET-RTRN-QTY TO
               RTRN-QTY-4094 OF R4094-RETURN-DTL
                                                                     .
           MOVE RET-RTRN-RSN-CD TO
               RTRN-RSN-CODE-4094 OF R4094-RETURN-DTL
                                                                     .
           MOVE RET-CMDTY-CD TO
               CMDTY-CODE-4094 OF R4094-RETURN-DTL
                                                                     .
           MOVE RET-CMDTY-DESC TO
               CMDTY-DESC-4094 OF R4094-RETURN-DTL
                                                                     .
           MOVE RET-ITEM-NBR TO
               ITEM-NMBR-4094 OF R4094-RETURN-DTL
                                                                     .
       975-TO-R4094-RETURN-DTL-EXIT.
           EXIT.
