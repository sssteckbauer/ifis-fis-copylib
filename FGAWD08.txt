      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FGAWD08                              04/24/00  13:07  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FGAWD08.
           02  SYSTEM-DATE-GA08               COMP-3    PIC S9(8).
           02  TAKE-FLAG-GA08                           PIC X(1).
           02  WK1-BDGT-ADJMT-AMT-GA08
                                     COMP-3 OCCURS 15   PIC S9(12).
           02  WK-BDGT-ADJMT-AMT-GA08
                                     COMP-3 OCCURS 15   PIC S9(12)V99.
           02  WK1-BDGT-RSRVTN-AMT-GA08
                                     COMP-3 OCCURS 15   PIC S9(12).
           02  WK-BDGT-RSRVTN-AMT-GA08
                                     COMP-3 OCCURS 15   PIC S9(12)V99.
           02  WK1-YTD-ACTVY-AMT-GA08
                                     COMP-3 OCCURS 15   PIC S9(12).
           02  WK-YTD-ACTVY-AMT-GA08
                                     COMP-3 OCCURS 15   PIC S9(12)V99.
           02  WK1-AVLBL-BAL-AMT-GA08
                                     COMP-3 OCCURS 15   PIC S9(12).
           02  WK-AVLBL-BAL-AMT-GA08
                                     COMP-3 OCCURS 15   PIC S9(12)V99.
           02  APLCN-DATA-KEY-GA08                      PIC X(53).
