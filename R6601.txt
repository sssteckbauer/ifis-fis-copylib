       01  R6601-TABL-DTL.
           02  DB-PROC-ID-6601.
               03  USER-CODE-6601                       PIC X(8).
               03  LAST-ACTVY-DATE-6601                 PIC X(5).
               03  TRMNL-ID-6601                        PIC X(8).
               03  PURGE-FLAG-6601                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  DTL-CODE-6601                            PIC X(8).
           02  SHORT-DESC-6601                          PIC X(10).
           02  LONG-DESC-6601                           PIC X(30).
           02  FLAG-1-6601                              PIC X(1).
           02  FLAG-2-6601                              PIC X(1).
           02  ACTV-INATV-CODE-FLAG-6601                PIC X(1).
           02  START-TERM-CODE-6601.
               03  START-SESN-CODE-6601                 PIC X(2).
               03  START-YEAR-DATE-6601                 PIC 9(2).
           02  END-TERM-CODE-6601.
               03  END-SESN-CODE-6601                   PIC X(2).
               03  END-YEAR-DATE-6601                   PIC 9(2).
           02  ALT-ACCESS1-6601                         PIC X(8).
           02  ALT-ACCESS2-6601                         PIC X(8).
           02  SUB-DATA-6601                            PIC X(60).
           02  LGCL-DLT-FLAG-6601                       PIC X(1).
