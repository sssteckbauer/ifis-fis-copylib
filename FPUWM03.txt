      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWM03                              04/24/00  12:38  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWM03.
           02  RQST-NAME-4070-PU03                      PIC X(35).
           02  ITEM-COUNT-4070-PU03                     PIC X(4).
           02  RQST-CMPLT-IND-4070-PU03                 PIC X(1).
           02  APRVL-IND-4070-PU03                      PIC X(1).
           02  CNCL-DATE-4070-PU03                      PIC X(6).
           02  ACTN-CODE-PU03                           PIC X(1).
           02  DTL-ERROR-IND-4071-PU03                  PIC X(1).
           02  DTL-CNTR-ACCT-4071-PU03                  PIC X(4).
           02  ITEM-NMBR-4071-PU03                      PIC X(4).
           02  CMDTY-CODE-4074-PU03                     PIC X(8).
           02  CMDTY-DESC-4074-PU03                     PIC X(35).
           02  ADD-CMDTY-PU03                           PIC X(1).
           02  ACTVY-DATE-4071-PU03                     PIC X(6).
           02  TRADE-IN-IND-PU03                        PIC X(1).
           02  TRADE-IN-UCID-PU03                       PIC X(9).
           02  ACCESSORY-IND-PU03                       PIC X(1).
           02  ACCESSORY-UCID-PU03                      PIC X(9).
           02  FABRICATION-IND-PU03                     PIC X(1).
           02  FABRICATION-NUM-PU03                     PIC X(4).
           02  QTY-4071-PU03                            PIC X(9).
           02  UNIT-MEA-CODE-4071-PU03                  PIC X(3).
           02  UNIT-MEA-DESC-4075-PU03                  PIC X(35).
           02  UNIT-PRICE-4071-PU03                     PIC X(16).
           02  EXTENDED-PRICE-PU03                      PIC X(16).
           02  SHIP-TO-CODE-4071-PU03                   PIC X(6).
           02  AGRMT-CODE-4071-PU03                     PIC X(15).
           02  PRJCT-CODE-4071-PU03                     PIC X(8).
           02  PO-NMBR-4083-PU03                        PIC X(8).
           02  PO-ITEM-NMBR-4085-PU03                   PIC X(4).
           02  TEXT-FLAG-PU03                           PIC X(1).
