      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWK79                              04/24/00  12:19  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWK79.
           02  RQST-COA-CODE-4052-CH79                  PIC X(1).
           02  RQST-XTRNL-ENTY-CODE-4052-CH79           PIC X(4).
           02  RQST-INTRL-ENTY-CODE-CH79                PIC X(4).
           02  RQST-XTRNL-CODE-CH79                     PIC X(10).
           02  RQST-INTRL-CODE-CH79                     PIC X(10).
