      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWM22                              04/24/00  12:06  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWM22.
           02  ACTN-CODE-GROUP-AP22.
               03  ACTN-CODE-AP22           OCCURS 13   PIC X(1).
           02  VNDR-CODE-4073-AP22          OCCURS 13   PIC X(9).
           02  ENTY-PRSN-IND-4073-AP22      OCCURS 13   PIC X(1).
           02  NAME-KEY-6311-R-6117-AP22    OCCURS 13   PIC X(35).
           02  EFCTV-DATE-4172-AP22         OCCURS 13   PIC X(6).
           02  CARR-IND-4073-AP22           OCCURS 13   PIC X(1).
