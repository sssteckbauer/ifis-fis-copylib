       01  FGA009-RECORD.
           03  FGA009-SORT-KEY.
               05  FGA009-FIXED-KEY.
                   07  FGA009-UNVRS-ID         PIC X(02).
                   07  FGA009-USER-ID          PIC X(08).
                   07  FGA009-RPRT-CODE        PIC X(08).
                   07  FGA009-SEQ-NMBR         PIC 9(04).
               05  FGA009-VAR-KEY.
                   07  FGA009-COA-CODE         PIC X(01).
                   07  FGA009-FUND-CODE        PIC X(06).
                   07  FGA009-ACCT-CODE        PIC X(06).
                   07  FGA009-TRANS-DATE       PIC 9(08)  COMP-3.
                   07  FGA009-JRNL-TYPE        PIC X(04).
                   07  FGA009-DCMNT-NMBR       PIC X(08).
                   07  FGA009-DCMNT-REF-NMBR   PIC X(10).
                   07  FILLER                  PIC X(17).
               05  FGA009-RCRD-TYPE            PIC X(01).
           03  FGA009-DATA                     PIC X(360).
           03  FGA009-ONE-TIME-DATA REDEFINES
               FGA009-DATA.
               05  FGA009-AS-OF-DATE           PIC 9(08)    COMP-3.
               05  FGA009-UNVRS-TITLE          PIC X(35).
               05  FGA009-USER-NAME            PIC X(35).
               05  FGA009-COA-DESC             PIC X(35).
               05  FGA009-CRNT-PRD             PIC 9(02).
               05  FGA009-PARM-FSCL-YR         PIC X(02).
               05  FGA009-PARM-PRINT-FUND-TOT  PIC X(01).
               05  FILLER                      PIC X(245).
           03  FGA009-FUNS-DATA REDEFINES
               FGA009-DATA.
               05  FGA009-FUND-TITLE           PIC X(35).
               05  FGA009-ACCT-CODE-TITLE      PIC X(35).
               05  FGA009-PREDCSR-ACCT-TYPE    PIC X(02).
               05  FGA009-PREDCSR-ACTT-DESC    PIC X(35).
               05  FGA009-ACCT-NRML-BAL-IND    PIC X(01).
               05  FGA009-ACTT-NRML-BAL-IND    PIC X(01).
               05  FGA009-SMRY-PRDC-DEBITS     PIC S9(11)V99 COMP-3.
               05  FGA009-SMRY-PRDC-CRDTS      PIC S9(11)V99 COMP-3.
               05  FILLER                      PIC X(237).
           03  FGA009-TRANSACTION-DATA REDEFINES
               FGA009-DATA.
               05  FGA009-TRANS-DESC           PIC X(35).
               05  FGA009-TRANS-AMT            PIC S9(11)V99 COMP-3.
               05  FGA009-FIELD-IND            PIC X(02).
               05  FILLER                      PIC X(316).
