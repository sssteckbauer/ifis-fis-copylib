      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FGAWK05                              04/24/00  12:31  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FGAWK05.
           02  COA-CODE-GA05                            PIC X(1).
           02  FSCL-YR-GA05                             PIC X(2).
           02  FUND-CODE-GA05                           PIC X(6).
           02  ACCT-TYPE-CODE-4008-GA05                 PIC X(2).
           02  ACCT-CODE-4003-GA05                      PIC X(6).
           02  ACCT-PSTN-IND-GA05                       PIC X(1).
           02  ACTG-PRD-GA05                            PIC X(2).
           02  PRD-START-DATE-4024-GA05                 PIC X(6).
           02  PRD-END-DATE-4024-GA05                   PIC X(6).
