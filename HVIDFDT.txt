       975-TO-R4008-FNDT SECTION.
           MOVE FDT-USER-CD TO
               USER-CODE-4008 OF R4008-FNDT
                                                                     .
           MOVE FDT-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4008 OF R4008-FNDT
                                                                     .
           MOVE FDT-UNVRS-CD TO
               UNVRS-CODE-4008 OF R4008-FNDT
                                                                     .
           MOVE FDT-COA-CD TO
               COA-CODE-4008 OF R4008-FNDT
                                                                     .
           MOVE FDT-FUND-TYP-CD TO
               FUND-TYPE-CODE-4008 OF R4008-FNDT
                                                                     .
           MOVE FDT-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACTVY-DATE-4008 OF R4008-FNDT
                                                                     .
           MOVE FDT-GOV-OWND-IND TO
               GOVNMT-OWNED-IND-4008 OF R4008-FNDT
                                                                     .
       975-TO-R4008-FNDT-EXIT.
           EXIT.
