       975-TO-R4184-RULE-EDITS SECTION.
           MOVE RED-USER-CD TO
               USER-CODE-4184 OF R4184-RULE-EDITS
                                                                     .
           MOVE RED-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4184 OF R4184-RULE-EDITS
                                                                     .
           MOVE RED-SEQ-NBR TO
               SEQ-NMBR-4184 OF R4184-RULE-EDITS
                                                                     .
           MOVE RED-EDIT-CD TO
               EDIT-CODE-4184 OF R4184-RULE-EDITS
                                                                     .
           MOVE RED-ERROR-SVRTY-IND TO
               ERROR-SVRTY-IND-4184 OF R4184-RULE-EDITS
                                                                     .
           MOVE RED-CNTNU-ERROR-IND TO
               CNTNU-ERROR-IND-4184 OF R4184-RULE-EDITS
                                                                     .
           MOVE RED-ERROR-MSG TO
               ERROR-MSG-4184 OF R4184-RULE-EDITS
                                                                     .
           MOVE RED-OPER TO
               OPER-4184 OF R4184-RULE-EDITS
                                                                     .
           MOVE RED-LTRL-FIELD-1 TO
               LTRL-FIELD-1-4184 OF R4184-RULE-EDITS
                                                                     .
           MOVE RED-LTRL-FIELD-2 TO
               LTRL-FIELD-2-4184 OF R4184-RULE-EDITS
                                                                     .
           MOVE RED-ELMNT-NAME TO
               ELMNT-NAME-4184 OF R4184-RULE-EDITS
                                                                     .
       975-TO-R4184-RULE-EDITS-EXIT.
           EXIT.
