       975-TO-R6161-LSTASGNPID SECTION.
           MOVE LPI-USER-CD TO
               USER-CODE-6161 OF R6161-LSTASGNPID
                                                                     .
           MOVE LPI-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-6161 OF R6161-LSTASGNPID
                                                                     .
           MOVE LPI-UNVRS-CD TO
               UNVRS-CODE-6161 OF R6161-LSTASGNPID
                                                                     .
           MOVE LPI-ALPHA-PREFIX TO
               ALPHA-PREFIX-6161 OF R6161-LSTASGNPID
                                                                     .
           MOVE LPI-LST-ASSGND-PID TO
               LAST-ASSGND-PID-6161 OF R6161-LSTASGNPID
                                                                     .
       975-TO-R6161-LSTASGNPID-EXIT.
           EXIT.
