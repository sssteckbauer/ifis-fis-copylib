      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FSYWG30D                             04/24/00  12:53  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FSYWG30D.
           02  UNVRS-CODE-SY30                          PIC X(2).
           02  FIMS-ENTY-CODE-SY30                      PIC X(8).
           02  OPTN-1-CODE-SY30                         PIC X(8).
           02  SYSTEM-DATE-SY30               COMP-3    PIC S9(8).
           02  RULE-CLASS-CODE-SY30                     PIC X(4).
