      ******************************************************************
      *  ***** COPYBOOK WAP008I - USED FOR INQINV1 WEB SERVICE *****   *
      *                                                                *
      *  WEB/CICS COMMAREA TO INQUIRE SCIQUEST INVOICES BY DATE RANGE  *
      ******************************************************************

      ******************************************************************
      *    AFTER CHANGES ARE MADE TO THIS COPYBOOK, IT MUST BE COPIED  *
      *    INTO A WEBSPHERE DEVELOPER PROJECT FOLDER, FROM WHICH THE   *
      *    WEB SERVICE INTERFACE PROGRAM, WSDL, & WSBIND ARE GENERATED.*
      ******************************************************************

       01  INQINV1-PARM-AREA.

         02  INQINV1-CHANNEL-HEADER.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND AND OUTBOUND  *
      *      ***********************************************************

           05  INQINV1-CHANNEL-ID             PIC X(08).
      *        *********************************************************
      *        * MUST BE 'INQINV1 '                                    *
      *        *********************************************************

           05  INQINV1-VERS-RESP-CD           PIC X(08).
      *        *********************************************************
      *        * INCOMING: COMMAREA VERSION = 01.00.00                 *
      *        * OUTGOING: RETURN RESPONSE FOR WFAP(WEB/IFIS AP SYSTEM)*
      *        *********************************************************
               88  INQINV1-RESPONSE-SUCCESSFUL         VALUE 'WFAP000I'.
               88  INQINV1-RESPONSE-WARNING            VALUE 'WFAP999W'.
               88  INQINV1-RESPONSE-ERROR              VALUE 'WFAP999E'.

         02  INQINV1-INPUT-OUTPUT-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND AND OUTBOUND  *
      *      ***********************************************************
      *      *         DATE FORMAT = YYYY-MM-DD OR MM/DD/YYYY          *
      *      ***********************************************************

           05  INQINV1-FROM-DT                PIC X(10).
           05  INQINV1-THRU-DT                PIC X(10).
           05  INQINV1-STARTING-DOC-NBR       PIC X(08).

         02  INQINV1-RETURN-STATUS-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: OUTBOUND ONLY         *
      *      ***********************************************************

           05  INQINV1-RETURN-STATUS-CODE     PIC X(01).
      *        *********************************************************
      *        * S = SUCCESSFUL, E = DATA ERROR(S), F = FATAL ERROR    *
      *        *********************************************************

           05  INQINV1-RETURN-MSG-COUNT       PIC 9(01).
      *        *********************************************************
      *        * 1 = NUMBER OF RETURN-MSG-AREA(S)                      *
      *        *********************************************************

           05  INQINV1-RETURN-MSG-AREA.
      *        *********************************************************
      *        * FOR STATUS 'S', SUCCESS MESSAGE NUMBER & TEXT         *
      *        * FOR STATUS 'E', ERROR MSG NUMBER, FIELD & TEXT        *
      *        * FOR STATUS 'F', FATAL SQLCODE, CURR-IO-NUM WITH       *
      *        *                 PARAGRAPH NAME, & ASSOC DB2 TABLE     *
      *        *********************************************************
               10  INQINV1-RETURN-MSG-NUM     PIC X(06).
               10  INQINV1-RETURN-MSG-FIELD   PIC X(30).
               10  INQINV1-RETURN-MSG-TEXT    PIC X(40).

         02  INQINV1-OUTPUT-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: OUTBOUND ONLY         *
      *      ***********************************************************

           05  INQINV1-RECORD-COUNT           PIC 9(04).
      *        *********************************************************
      *        * NUMBER OF REPORT-RECORD(S)                            *
      *        *********************************************************

           05  INQINV1-REPORT-RECORD          OCCURS 200 TIMES.

               10  INQINV1-DOC-NBR            PIC X(08).
               10  INQINV1-APRVD-AMT          PIC +9(9).99.
               10  INQINV1-SCIQ-BATCH-ID      PIC X(25).

