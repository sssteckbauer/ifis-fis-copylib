      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWD58                              04/24/00  13:04  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWD58.
           02  SAVE-DBKEY-ID-6185-CH58        COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-4046-CH58        COMP      PIC S9(8).
           02  INDX-KEY-WORK-CH58                       PIC X(30).
           02  SAVE-DBKEY-ID-6311-CH58        COMP      PIC S9(8).
           02  WORK-DATE-CH58                 COMP-3    PIC S9(8).
           02  SAVE-DBKEY-ID-6186-CH58        COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-6137-CH58        COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-6151-CH58        COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-6152-CH58        COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-CH58             COMP      PIC S9(8).
