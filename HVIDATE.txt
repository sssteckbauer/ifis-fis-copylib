       975-TO-R4015-ACTT-EFCTV SECTION.
           MOVE ATE-USER-CD TO
               USER-CODE-4015 OF R4015-ACTT-EFCTV
                                                                     .
           MOVE ATE-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4015 OF R4015-ACTT-EFCTV
                                                                     .
           MOVE ATE-START-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               START-DATE-4015 OF R4015-ACTT-EFCTV
                                                                     .
           MOVE ATE-TIME-STMP TO
               DBLINKX-INPUT-PARM
           MOVE '4' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-TIME-FORMAT
           MOVE '51' TO DBLINKX-IDMS-TIME-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-PARM TO
               TIME-STAMP-4015 OF R4015-ACTT-EFCTV
                                                                     .
           MOVE ATE-END-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               END-DATE-4015 OF R4015-ACTT-EFCTV
                                                                     .
           MOVE ATE-STATUS TO
               STATUS-4015 OF R4015-ACTT-EFCTV
                                                                     .
           MOVE ATE-ACCT-TYP-DESC TO
               ACCT-TYPE-DESC-4015 OF R4015-ACTT-EFCTV
                                                                     .
           MOVE ATE-INTRL-ACCT-TYP-CD TO
               INTRL-ACCT-TYPE-CODE-4015 OF R4015-ACTT-EFCTV
                                                                     .
           MOVE ATE-PRDCSR-ACCT-TYP TO
               PREDCSR-ACCT-TYPE-4015 OF R4015-ACTT-EFCTV
                                                                     .
           MOVE ATE-SBRDT-ACCT-TYP TO
               SBRDT-ACCT-TYPE-4015 OF R4015-ACTT-EFCTV
                                                                     .
           MOVE ATE-NRML-BAL-IND TO
               NRML-BAL-IND-4015 OF R4015-ACTT-EFCTV
                                                                     .
       975-TO-R4015-ACTT-EFCTV-EXIT.
           EXIT.
