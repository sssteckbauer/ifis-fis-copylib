      *%---------------------------------------------------------------*
      *%                                                               *
      *%                   200-FSYII11A ROUTINE                        *
      *% DEVDMW - 11/01/01 COMMENTED OUT CALLING ACXRIGHT FOR          *
      *%                   CURR-RSPNS-ID-SY00.  THIS WILL PREVENT      *
      *%                   CREATING SCRATCH RECORD WITH TWO SPACES
      *%                   AFTER TERMID                                *
      *%                                                               *
      *%---------------------------------------------------------------*
       200-FSYII11A.

      ******************************************************************
      ******
      ***  INITIALIZE PAGING FIELDS IN GLOBAL RECORD FIRST TIME OR ON RE
      *FRESH
      ******************************************************************
      ******
           IF AC99W-FLAG-FIRST-TIME = AC99W-YES
               MOVE SPACES TO PAGE-FLAG-SY00
               MOVE ZEROS TO SAVE-DBKEY-ID-PLUS-ONE-SY00
               MOVE ZEROS TO SAVE-DBKEY-ID-MINUS-ONE-SY00
           END-IF.
      *
      ******************************************************************
      *******
      *** SAVE THE NAME OF THIS RESPONSE IN CASE THE USER DECIDES TO COM
      *E
      *** BACK TO IT FROM THE NEXT DIALOG. MAINTAIN 10 ENTRIES OF PREVIO
      *US
      *** DIALOGS. DON'T SAVE IF SAME DIALOG, IF REFRESHING SCREEN, OR I
      *F
      *** THE DIALOG IS BEING LINKED TO FOR LIST VALUES PROCESSING.
      ******************************************************************
      *******
           IF RSPNS-QTY-SY00 = ZEROS
               MOVE 1 TO RSPNS-QTY-SY00
           END-IF.
      *
           IF AC99W-FLAG-FIRST-TIME = AC99W-YES   AND
              AGR-CURRENT-RESPONSE NOT = PRVS-RSPNS-ID-SY00
               (RSPNS-QTY-SY00) AND
              AGR-CURRENT-RESPONSE NOT = 'REFRESH' AND
              AGR-PASSED-FOUR = SPACES
      ** 1 *
               IF RSPNS-QTY-SY00 = 10
      ** 2 *
                   MOVE SPACES TO PRVS-RSPNS-ID-SY00( 1 )
                   STRING PRVS-RSPNS-GROUP-SY00 DELIMITED SIZE
                          HIGH-VALUE DELIMITED SIZE
                          INTO ACWLEFT-PARM-IN
FXXCHG             CALL 'ACXLEFT' USING ACWLEFT-PARMS
                   MOVE ACWLEFT-PARM-OUT TO PRVS-RSPNS-GROUP-SY00
                   SUBTRACT 1 FROM RSPNS-QTY-SY00
               END-IF
      ** 2 *
               ADD 1 TO RSPNS-QTY-SY00
      ***   IF THIS DIALOG IS A MENU, CHECK FOR RESPONSE OF 'POP'
               IF AGR-CURRENT-RESPONSE = 'POP'
                   MOVE AGR-DEFAULT-RESPONSE TO AGR-CURRENT-RESPONSE
                   IF RSPNS-QTY-SY00 > 1
                       IF AGR-CURRENT-RESPONSE = PRVS-RSPNS-ID-SY00(
                          RSPNS-QTY-SY00  -  1 )
                           SUBTRACT 1 FROM RSPNS-QTY-SY00
                       END-IF
                   END-IF
               END-IF
               MOVE AGR-CURRENT-RESPONSE TO PRVS-RSPNS-ID-SY00
                    (RSPNS-QTY-SY00)
               MOVE AGR-CURRENT-RESPONSE TO CURR-RSPNS-ID-SY00
      *** FOR DISPLAY ON MAP HEADER
           END-IF.
      ** 1 *
      *
      ******************************************************************
      *******
      ***  INITIALIZE FIELDS EVERY TIME THROUGH
      ******************************************************************
      *******

DEVDMW*    STRING CURR-RSPNS-ID-SY00 DELIMITED BY SIZE
DEVDMW*           HIGH-VALUE  DELIMITED BY SIZE
DEVDMW*      INTO ACWRIGHT-PARM-IN.
DEVDMW*    CALL 'ACXRIGHT' USING ACWRIGHT-PARMS.
DEVDMW*    MOVE ACWRIGHT-PARM-OUT TO CURR-RSPNS-ID-SY00.

           MOVE SPACES TO PAGE-STATUS-CODE-SY00.
           MOVE SPACES TO VALID-KEY-FLAG-SY00.
           MOVE SPACES TO AGR-MAP-RESPONSE.

       200-FSYII11A-EXIT.
           EXIT.
