      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWD27                              04/24/00  12:53  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWD27.
           02  WORK-SAVE-DBKEY-ID-4157-AP27
                                     COMP   OCCURS 11   PIC S9(8).
           02  WORK-LOOP-FLAG-AP27                      PIC X(1).
           02  WORK-SAVE-DBKEY-AP27           COMP      PIC S9(8).
           02  ACTN-FLAG-AP27                           PIC X(1).
           02  WORK-SEL-FLAG-AP27                       PIC X(1).
           02  SEL-FLAG-AP27                            PIC X(1).
           02  WORK-SYSTEM-DATE-AP27          COMP-3    PIC S9(8).
           02  RQSTR-ID-KEY-AP27.
               03  UNVRS-CODE-K-AP27                    PIC X(2).
               03  RQSTR-CODE-K-AP27.
                   04  RQSTR-ID-DIGIT-ONE-K-AP27        PIC X(1).
                   04  RQSTR-ID-LAST-NINE-K-AP27        PIC X(9).
               03  SRVCD-DATE-K-AP27          COMP-3    PIC S9(8).
