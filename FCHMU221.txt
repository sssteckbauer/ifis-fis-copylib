    ***Created by Convert/DC version V8R03 on 12/13/00 at 08:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE DCSD-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY01.
      *%--------------------------------------------------------------%*
           MOVE CNFDL-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0A.
      *%--------------------------------------------------------------%*
           MOVE HOLDS-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0B.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH22 OF FCHWM22 TO WK01-CODE-CH22.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4000-CH22 OF FCHWK22 TO WK01-CODE-4000-CH22.
      *%--------------------------------------------------------------%*
           MOVE XTRNL-ENTY-CODE-4052-CH22 OF FCHWK22 TO
                WK01-ENTY-CODE-4052-CH22.
      *%--------------------------------------------------------------%*
           MOVE XTRNL-ENTY-DESC-4052-CH22 OF FCHWM22 TO
                WK01-ENTY-DESC-4052-CH22.
      *%--------------------------------------------------------------%*
           MOVE INTRL-ENTY-CODE-4053-CH22 OF FCHWK22 TO
                WK01-ENTY-CODE-4053-CH22.
      *%--------------------------------------------------------------%*
           MOVE INTRL-ENTY-DESC-4053-CH22 OF FCHWM22 TO
                WK01-ENTY-DESC-4053-CH22.
      *%--------------------------------------------------------------%*
           MOVE XTRNL-CODE-4054-CH22 OF FCHWK22 TO WK01-CODE-4054-CH22.
      *%--------------------------------------------------------------%*
           MOVE XTRNL-CODE-DESC-4054-CH22 OF FCHWM22 TO
                WK01-CODE-DESC-4054-CH22.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-DATE-4055-CH22 OF FCHWM22 TO WK01-DATE-4055-CH22.
      *%--------------------------------------------------------------%*
           MOVE INTRL-CODE-4055-CH22 OF FCHWK22 TO WK01-CODE-4055-CH22.
      *%--------------------------------------------------------------%*
           MOVE INTRL-CODE-DESC-4055-CH22 OF FCHWM22 TO
                WK01-CODE-DESC-4055-CH22.
