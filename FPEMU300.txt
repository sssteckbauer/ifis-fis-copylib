    ***Created by Convert/DC version V8R03 on 12/11/00 at 08:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PE30-Z = 'Y'
               MOVE WK01-CODE-PE30 TO ACTN-CODE-PE30 OF FPEWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ADR-6185-PE301-Z = 'Y'
               MOVE WK01-ADR-6185-PE301 TO LINE-ADR-6185-PE30 OF
                FPEWM30(0001)
           END-IF.
           MOVE WK01-ADR-6185-PE301-F TO WK01-ADR-6185-PE30-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-6185-PE302-Z = 'Y'
               MOVE WK01-ADR-6185-PE302 TO LINE-ADR-6185-PE30 OF
                FPEWM30(0002)
           END-IF.
           MOVE WK01-ADR-6185-PE302-F TO WK01-ADR-6185-PE30-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-6185-PE303-Z = 'Y'
               MOVE WK01-ADR-6185-PE303 TO LINE-ADR-6185-PE30 OF
                FPEWM30(0003)
           END-IF.
           MOVE WK01-ADR-6185-PE303-F TO WK01-ADR-6185-PE30-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-6185-PE304-Z = 'Y'
               MOVE WK01-ADR-6185-PE304 TO LINE-ADR-6185-PE30 OF
                FPEWM30(0004)
           END-IF.
           MOVE WK01-ADR-6185-PE304-F TO WK01-ADR-6185-PE30-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6185-PE30-Z = 'Y'
               MOVE WK01-NAME-6185-PE30 TO CITY-NAME-6185-PE30 OF
                FPEWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6151-PE30-Z = 'Y'
               MOVE WK01-CODE-6151-PE30 TO STATE-CODE-6151-PE30 OF
                FPEWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6153-PE30-Z = 'Y'
               MOVE WK01-CODE-6153-PE30 TO CNTRY-CODE-6153-PE30 OF
                FPEWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AREA-CODE-6185-PE30-Z = 'Y'
               MOVE WK01-AREA-CODE-6185-PE30 TO
                TLPHN-AREA-CODE-6185-PE30 OF FPEWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-XCHNG-ID-6185-PE30-Z = 'Y'
               MOVE WK01-XCHNG-ID-6185-PE30 TO TLPHN-XCHNG-ID-6185-PE30
                OF FPEWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-ID-6185-PE30-Z = 'Y'
               MOVE WK01-SEQ-ID-6185-PE30 TO TLPHN-SEQ-ID-6185-PE30 OF
                FPEWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-XTNSN-ID-6185-PE30-Z = 'Y'
               MOVE WK01-XTNSN-ID-6185-PE30 TO TLPHN-XTNSN-ID-6185-PE30
                OF FPEWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6185-PE30-Z = 'Y'
               MOVE WK01-TYPE-CODE-6185-PE30 TO ADR-TYPE-CODE-6185-PE30
                OF FPEWK30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-PE30-Z = 'Y'
               MOVE WK01-DESC-6137-PE30 TO LONG-DESC-6137-PE30 OF
                FPEWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6185-PE30-Z = 'Y'
               MOVE WK01-DATE-6185-PE30 TO START-DATE-6185-PE30 OF
                FPEWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6185-PE3A-Z = 'Y'
               MOVE WK01-DATE-6185-PE3A TO END-DATE-6185-PE30 OF FPEWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-PE30-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-PE30 TO ENTY-ID-LAST-NINE-PE30 OF
                FPEWK30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6352-PE30-Z = 'Y'
               MOVE WK01-KEY-6352-PE30 TO NAME-KEY-6352-PE30 OF FPEWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6185-PE3B-Z = 'Y'
               MOVE WK01-DATE-6185-PE3B TO RQST-DATE-6185-PE30 OF
                FPEWK30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6152-PE30-Z = 'Y'
               MOVE WK01-CODE-6152-PE30 TO ZIP-CODE-6152-PE30 OF FPEWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-LINE-6185-PE30-Z = 'Y'
               MOVE WK01-LINE-6185-PE30 TO EMADR-LINE-6185-PE30 OF
                FPEWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6151-PE30-Z = 'Y'
               MOVE WK01-NAME-6151-PE30 TO FULL-NAME-6151-PE30 OF
                FPEWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6153-PE30-Z = 'Y'
               MOVE WK01-NAME-6153-PE30 TO FULL-NAME-6153-PE30 OF
                FPEWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AREA-CODE-PE30-Z = 'Y'
               MOVE WK01-AREA-CODE-PE30 TO FAX-AREA-CODE-PE30 OF FPEWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-XCHNG-ID-PE30-Z = 'Y'
               MOVE WK01-XCHNG-ID-PE30 TO FAX-XCHNG-ID-PE30 OF FPEWM30
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-ID-PE30-Z = 'Y'
               MOVE WK01-SEQ-ID-PE30 TO FAX-SEQ-ID-PE30 OF FPEWM30
           END-IF.
