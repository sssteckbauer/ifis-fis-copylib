       975-TO-R4182-RULE-ACTN SECTION.
           MOVE RLA-USER-CD TO
               USER-CODE-4182 OF R4182-RULE-ACTN
                                                                     .
           MOVE RLA-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4182 OF R4182-RULE-ACTN
                                                                     .
           MOVE RLA-SEQ-NBR TO
               SEQ-NMBR-4182 OF R4182-RULE-ACTN
                                                                     .
           MOVE RLA-PRCS-CD TO
               PRCS-CODE-4182 OF R4182-RULE-ACTN
                                                                     .
           MOVE RLA-PSTNG-ACTN-IND TO
               PSTNG-ACTN-IND-4182 OF R4182-RULE-ACTN
                                                                     .
           MOVE RLA-ACRL-IMPCT-IND TO
               ACRL-IMPCT-IND-4182 OF R4182-RULE-ACTN
                                                                     .
           MOVE RLA-COA-CD TO
               COA-CODE-4182 OF R4182-RULE-ACTN
                                                                     .
           MOVE RLA-ACCT-INDX-CD TO
               ACCT-INDX-CODE-4182 OF R4182-RULE-ACTN
                                                                     .
           MOVE RLA-FUND-CD TO
               FUND-CODE-4182 OF R4182-RULE-ACTN
                                                                     .
           MOVE RLA-ORGN-CD TO
               ORGZN-CODE-4182 OF R4182-RULE-ACTN
                                                                     .
           MOVE RLA-ACCT-CD TO
               ACCT-CODE-4182 OF R4182-RULE-ACTN
                                                                     .
           MOVE RLA-PRGRM-CD TO
               PRGRM-CODE-4182 OF R4182-RULE-ACTN
                                                                     .
           MOVE RLA-ACTVY-CD TO
               ACTVY-CODE-4182 OF R4182-RULE-ACTN
                                                                     .
           MOVE RLA-LCTN-CD TO
               LCTN-CODE-4182 OF R4182-RULE-ACTN
                                                                     .
       975-TO-R4182-RULE-ACTN-EXIT.
           EXIT.
