      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWD85                              04/24/00  12:58  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWD85.
           02  WORK-SYSTEM-DATE-CH85                    PIC X(6).
           02  WORK-START-DATE-CH85                     PIC X(6).
           02  WORK-END-DATE-CH85                       PIC X(6).
           02  SAVE-DBKEY-ID-4024-CH85
                                     COMP   OCCURS 10   PIC S9(8).
           02  WORK-PRD-FLAG1-CH85                      PIC X(1).
           02  WORK-PRD-FLAG2-CH85                      PIC X(1).
           02  WORK-PRD-FLAG3-CH85                      PIC X(1).
           02  ACTN-FLAG-CH85                           PIC X(1).
           02  WORK-ADD-FLAG-CH85                       PIC X(1).
           02  WORK-LIST-FLAG-CH85                      PIC X(1).
           02  WORK-SEL-FLAG-CH85                       PIC X(1).
           02  SEL-FLAG-CH85                            PIC X(1).
           02  WORK-PRD-SUB-CH85                        PIC 9(2).
