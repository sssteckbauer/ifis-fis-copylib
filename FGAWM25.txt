      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FGAWM25                              04/24/00  12:33  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FGAWM25.
           02  ACTN-CODE-GROUP-GA25.
               03  ACTN-CODE-GA25                       PIC X(1).
           02  JRNL-TYPE-4068-GA25                      PIC X(4).
           02  RULE-CLASS-DESC-4181-GA25                PIC X(35).
           02  DCMNT-REF-NMBR-4068-GA25                 PIC X(10).
           02  DPST-NMBR-4068-GA25                      PIC X(8).
           02  DSTBN-PCT-4068-GA25                      PIC X(7).
           02  ENCMBRNC-ACTN-IND-4068-GA25              PIC X(1).
           02  ENCMBRNC-ACTN-DESC-4012-GA25             PIC X(20).
           02  BANK-ACCT-CODE-4068-GA25                 PIC X(2).
           02  TRANS-AMT-4068-GA25                      PIC X(17).
           02  TRANS-DESC-4068-GA25                     PIC X(35).
           02  PRJCT-CODE-4068-GA25                     PIC X(8).
           02  CASH-ACCT-CODE-4062-GA25                 PIC X(6).
           02  ACRL-IND-4068-GA25                       PIC X(1).
           02  COA-CODE-4068-GA25                       PIC X(1).
           02  ACCT-INDX-CODE-4068-GA25                 PIC X(10).
           02  FUND-CODE-4068-GA25                      PIC X(6).
           02  ORGZN-CODE-4068-GA25                     PIC X(6).
           02  ACCT-CODE-4068-GA25                      PIC X(6).
           02  PRGRM-CODE-4068-GA25                     PIC X(6).
           02  ACTVY-CODE-4068-GA25                     PIC X(6).
           02  LCTN-CODE-4068-GA25                      PIC X(6).
           02  SMRY-AMT-DR-GA25                         PIC X(16).
           02  SMRY-AMT-CR-GA25                         PIC X(16).
           02  SMRY-AMT-NET-GA25                        PIC X(17).
           02  SMRY-PCT-DR-GA25                         PIC X(7).
           02  SMRY-PCT-CR-GA25                         PIC X(7).
           02  ENCMBRNC-NMBR-4068-GA25                  PIC X(8).
           02  DCMNT-TYPE-SEQ-NMBR-4068-GA25            PIC 9(4).
           02  DCMNT-TYPE-4191-GA25                     PIC X(3).
           02  DEBIT-CRDT-IND-4068-GA25                 PIC X(1).
           02  BDGT-PRD-4068-GA25                       PIC X(2).
           02  ENCMBRNC-ITEM-NMBR-4068-GA25             PIC X(4).
           02  LAST-SEQ-USED-GA25                       PIC X(4).
           02  LAST-SEQ-NMBR-USED-GA25                  PIC 9(4).
           02  ACCT-ERROR-IND-4068-GA25                 PIC X(1).
           02  SEQ-NMBR-4068-GA25                       PIC X(4).
           02  ENCMBRNC-SEQ-NMBR-4068-GA25              PIC X(4).
           02  UPDT-ACTVY-DATE-GA25           COMP-3    PIC S9(8).
           02  UPDT-TIME-STAMP-GA25                     PIC X(6).
           02  ACTVY-DATE-4068-GA25                     PIC X(8).
           02  ENPET-TRANS-GA25                         PIC X(8).
