    ***Created by Convert/DC version V8R03 on 11/09/00 at 14:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH73 OF FCHWM73(0001) TO WK01-CODE-CH731.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH73 OF FCHWM73(0002) TO WK01-CODE-CH732.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH73 OF FCHWM73(0003) TO WK01-CODE-CH733.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH73 OF FCHWM73(0004) TO WK01-CODE-CH734.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH73 OF FCHWM73(0005) TO WK01-CODE-CH735.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH73 OF FCHWM73(0006) TO WK01-CODE-CH736.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH73 OF FCHWM73(0007) TO WK01-CODE-CH737.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH73 OF FCHWM73(0008) TO WK01-CODE-CH738.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH73 OF FCHWM73(0009) TO WK01-CODE-CH739.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH73 OF FCHWM73(0010) TO WK01-CODE-CH7310.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH73 OF FCHWM73(0011) TO WK01-CODE-CH7311.
      *%--------------------------------------------------------------%*
           MOVE SLCTN-DATE-CH73 OF FCHWK73 TO WK01-DATE-CH73.
      *%--------------------------------------------------------------%*
           MOVE SLCTN-STATUS-CH73 OF FCHWK73 TO WK01-STATUS-CH73.
      *%--------------------------------------------------------------%*
           MOVE RQST-COA-CODE-CH73 OF FCHWK73 TO WK01-COA-CODE-CH73.
      *%--------------------------------------------------------------%*
           MOVE RQST-ACCT-CODE-CH73 OF FCHWK73 TO WK01-ACCT-CODE-CH73.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4003-CH73 OF FCHWM73(0001) TO
                WK01-CODE-4003-CH731.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4003-CH73 OF FCHWM73(0002) TO
                WK01-CODE-4003-CH732.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4003-CH73 OF FCHWM73(0003) TO
                WK01-CODE-4003-CH733.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4003-CH73 OF FCHWM73(0004) TO
                WK01-CODE-4003-CH734.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4003-CH73 OF FCHWM73(0005) TO
                WK01-CODE-4003-CH735.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4003-CH73 OF FCHWM73(0006) TO
                WK01-CODE-4003-CH736.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4003-CH73 OF FCHWM73(0007) TO
                WK01-CODE-4003-CH737.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4003-CH73 OF FCHWM73(0008) TO
                WK01-CODE-4003-CH738.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4003-CH73 OF FCHWM73(0009) TO
                WK01-CODE-4003-CH739.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4003-CH73 OF FCHWM73(0010) TO
                WK01-CODE-4003-CH7310.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4003-CH73 OF FCHWM73(0011) TO
                WK01-CODE-4003-CH7311.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-4003-CH73 OF FCHWM73(0001) TO
                WK01-CODE-4003-CH7A1.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-4003-CH73 OF FCHWM73(0002) TO
                WK01-CODE-4003-CH7A2.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-4003-CH73 OF FCHWM73(0003) TO
                WK01-CODE-4003-CH7A3.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-4003-CH73 OF FCHWM73(0004) TO
                WK01-CODE-4003-CH7A4.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-4003-CH73 OF FCHWM73(0005) TO
                WK01-CODE-4003-CH7A5.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-4003-CH73 OF FCHWM73(0006) TO
                WK01-CODE-4003-CH7A6.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-4003-CH73 OF FCHWM73(0007) TO
                WK01-CODE-4003-CH7A7.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-4003-CH73 OF FCHWM73(0008) TO
                WK01-CODE-4003-CH7A8.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-4003-CH73 OF FCHWM73(0009) TO
                WK01-CODE-4003-CH7A9.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-4003-CH73 OF FCHWM73(0010) TO
                WK01-CODE-4003-CH7A10.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-4003-CH73 OF FCHWM73(0011) TO
                WK01-CODE-4003-CH7A11.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-CH73 OF FCHWM73(0001) TO
                WK01-CODE-TITLE-4034-CH731.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-CH73 OF FCHWM73(0002) TO
                WK01-CODE-TITLE-4034-CH732.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-CH73 OF FCHWM73(0003) TO
                WK01-CODE-TITLE-4034-CH733.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-CH73 OF FCHWM73(0004) TO
                WK01-CODE-TITLE-4034-CH734.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-CH73 OF FCHWM73(0005) TO
                WK01-CODE-TITLE-4034-CH735.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-CH73 OF FCHWM73(0006) TO
                WK01-CODE-TITLE-4034-CH736.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-CH73 OF FCHWM73(0007) TO
                WK01-CODE-TITLE-4034-CH737.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-CH73 OF FCHWM73(0008) TO
                WK01-CODE-TITLE-4034-CH738.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-CH73 OF FCHWM73(0009) TO
                WK01-CODE-TITLE-4034-CH739.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-CH73 OF FCHWM73(0010) TO
                WK01-CODE-TITLE-4034-CH7310.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-TITLE-4034-CH73 OF FCHWM73(0011) TO
                WK01-CODE-TITLE-4034-CH7311.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4034-CH73 OF FCHWM73(0001) TO WK01-4034-CH731.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4034-CH73 OF FCHWM73(0002) TO WK01-4034-CH732.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4034-CH73 OF FCHWM73(0003) TO WK01-4034-CH733.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4034-CH73 OF FCHWM73(0004) TO WK01-4034-CH734.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4034-CH73 OF FCHWM73(0005) TO WK01-4034-CH735.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4034-CH73 OF FCHWM73(0006) TO WK01-4034-CH736.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4034-CH73 OF FCHWM73(0007) TO WK01-4034-CH737.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4034-CH73 OF FCHWM73(0008) TO WK01-4034-CH738.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4034-CH73 OF FCHWM73(0009) TO WK01-4034-CH739.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4034-CH73 OF FCHWM73(0010) TO WK01-4034-CH7310.
      *%--------------------------------------------------------------%*
           MOVE STATUS-4034-CH73 OF FCHWM73(0011) TO WK01-4034-CH7311.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4034-CH73 OF FCHWM73(0001) TO
                WK01-DATE-4034-CH731.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4034-CH73 OF FCHWM73(0002) TO
                WK01-DATE-4034-CH732.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4034-CH73 OF FCHWM73(0003) TO
                WK01-DATE-4034-CH733.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4034-CH73 OF FCHWM73(0004) TO
                WK01-DATE-4034-CH734.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4034-CH73 OF FCHWM73(0005) TO
                WK01-DATE-4034-CH735.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4034-CH73 OF FCHWM73(0006) TO
                WK01-DATE-4034-CH736.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4034-CH73 OF FCHWM73(0007) TO
                WK01-DATE-4034-CH737.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4034-CH73 OF FCHWM73(0008) TO
                WK01-DATE-4034-CH738.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4034-CH73 OF FCHWM73(0009) TO
                WK01-DATE-4034-CH739.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4034-CH73 OF FCHWM73(0010) TO
                WK01-DATE-4034-CH7310.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4034-CH73 OF FCHWM73(0011) TO
                WK01-DATE-4034-CH7311.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4034-CH73 OF FCHWM73(0001) TO
                WK01-DATE-4034-CH7A1.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4034-CH73 OF FCHWM73(0002) TO
                WK01-DATE-4034-CH7A2.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4034-CH73 OF FCHWM73(0003) TO
                WK01-DATE-4034-CH7A3.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4034-CH73 OF FCHWM73(0004) TO
                WK01-DATE-4034-CH7A4.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4034-CH73 OF FCHWM73(0005) TO
                WK01-DATE-4034-CH7A5.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4034-CH73 OF FCHWM73(0006) TO
                WK01-DATE-4034-CH7A6.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4034-CH73 OF FCHWM73(0007) TO
                WK01-DATE-4034-CH7A7.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4034-CH73 OF FCHWM73(0008) TO
                WK01-DATE-4034-CH7A8.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4034-CH73 OF FCHWM73(0009) TO
                WK01-DATE-4034-CH7A9.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4034-CH73 OF FCHWM73(0010) TO
                WK01-DATE-4034-CH7A10.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4034-CH73 OF FCHWM73(0011) TO
                WK01-DATE-4034-CH7A11.
