      *01  XPCB-PROGRAM-CONTROL-BLOCK.                                  CPWSXPCB
           05  XPCB-BLOCK-KEY.                                          CPWSXPCB
               10  XPCB-JOB-NAME               PIC X(08).               CPWSXPCB
               10  XPCB-PROGRAM-NAME           PIC X(08).               CPWSXPCB
               10  XPCB-SEQUENCE-OPTION        PIC X(01).               CPWSXPCB
           05  XPCB-VERSION-NO                 PIC XX.                  CPWSXPCB
           05  XPCB-PARM-EDIT-STATUS           PIC X(05).               CPWSXPCB
               88  XPCB-PARM-EDIT-ERROR        VALUE 'ERROR'.           CPWSXPCB
           05  XPCB-PROGRAM-RUN-STATUS         PIC X(09).               CPWSXPCB
           05  XPCB-FILLER                     PIC X(31).               CPWSXPCB
           05  XPCB-ITEM                       OCCURS 12.               CPWSXPCB
               10  XPCB-ITEM-ID                PIC XX.                  CPWSXPCB
               10  XPCB-ITEM-DESCRIPTION       PIC X(29).               CPWSXPCB
               10  XPCB-ITEM-VALUE             PIC X(20).               CPWSXPCB
               10  XPCB-ITEM-MESSAGE           PIC X(29).               CPWSXPCB
