      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWK50                              04/24/00  12:37  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWK50.
           02  PO-NMBR-PU50                             PIC X(8).
           02  CHNG-SEQ-NMBR-PU50                       PIC X(3).
           02  TOTAL-C-O-PU50                           PIC X(3).
           02  VNDR-ID-LAST-NINE-PU50                   PIC X(9).
           02  NAME-KEY-PU50                            PIC X(35).
           02  ORDER-DATE-PU50                COMP-3    PIC S9(8).
           02  CNCL-DATE-PU50                 COMP-3    PIC S9(8).
           02  DATA-DESC-PU50                           PIC X(15).
           02  TOTAL-PO-AMT-PU50              COMP-3    PIC S9(10)V99.
           02  PO-AMT-PU50                    COMP-3    PIC S9(10)V99.
           02  TOTAL-INV-AMT-PU50             COMP-3    PIC S9(10)V99.
           02  INV-AMT-PU50                   COMP-3    PIC S9(10)V99.
           02  PO-TAX-AMT-PU50                COMP-3    PIC S9(10)V99.
           02  INV-TAX-AMT-PU50               COMP-3    PIC S9(10)V99.
           02  PO-ADDL-AMT-PU50               COMP-3    PIC S9(10)V99.
           02  INV-ADDL-AMT-PU50              COMP-3    PIC S9(10)V99.
           02  CHK-IND-PU50                             PIC X(1).
