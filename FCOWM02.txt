      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCOWM02                              04/24/00  12:27  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCOWM02.
           02  LONG-DESC-6257-CO02                      PIC X(30).
           02  INSRT-ELMNT-ID-6262-CO02     OCCURS 11   PIC X(3).
           02  LONG-DESC-6262-CO02          OCCURS 11   PIC X(30).
           02  CODE-LGTH-6262-CO02          OCCURS 11   PIC XX.
           02  SHORT-DESC-LGTH-6262-CO02    OCCURS 11   PIC XX.
           02  LONG-DESC-LGTH-6262-CO02     OCCURS 11   PIC XX.
           02  FRMT-OPTN-1-6262-CO02        OCCURS 11   PIC X(1).
           02  FRMT-OPTN-2-6262-CO02        OCCURS 11   PIC X(1).
           02  FRMT-OPTN-3-6262-CO02        OCCURS 11   PIC X(1).
           02  FRMT-OPTN-4-6262-CO02        OCCURS 11   PIC X(1).
           02  FRMT-OPTN-5-6262-CO02        OCCURS 11   PIC X(1).
           02  FRMT-OPTN-6-6262-CO02        OCCURS 11   PIC X(1).
           02  FRMT-OPTN-7-6262-CO02        OCCURS 11   PIC X(1).
           02  FRMT-OPTN-8-6262-CO02        OCCURS 11   PIC X(1).
           02  FRMT-OPTN-9-6262-CO02        OCCURS 11   PIC X(1).
           02  CODE-OCCUR-6262-CO02         OCCURS 11   PIC XX.
