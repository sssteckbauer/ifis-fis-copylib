      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWM45.                             04/24/00  12:15  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWM45.
           02  ACTN-CODE-AP45                           PIC X(1).
           02  DCMNT-NMBR-4150-AP45                     PIC X(8).
           02  PO-CLASS-CODE-AP45                       PIC X(1).
           02  PO-NMBR-4150-AP45                        PIC X(8).
           02  TOTAL-C-O-AP45                           PIC X(3).
           02  ORIG-PO-AMT-AP45                         PIC X(13).
           02  VNDR-INV-NMBR-4150-AP45                  PIC X(9).
           02  VNDR-INV-DATE-4150-AP45                  PIC X(6).
           02  INVD-AMT-4150-AP45                       PIC X(13).
           02  TRANS-DATE-4150-AP45                     PIC X(6).
           02  UNPAID-PO-BAL-AP45                       PIC X(13).
           02  DCMNT-REF-NMBR-AP45                      PIC X(10).
           02  PAYMENT-DOC-NBR-AP45                     PIC X(08).
           02  VNDR-CODE-4073-AP45.
               03  VNDR-ID-DIGIT-ONE-4073-AP45          PIC X(1).
               03  VNDR-ID-LAST-NINE-4073-AP45          PIC X(9).
           02  VNDR-NAME-GP-AP45                        PIC X(35).
           02  RULE-CLASS-AP45                          PIC X(4).
           02  TOTAL-INV-AMT-AP45                       PIC X(13).
001MLJ     02  OFFSET-INDEX-AP45                        PIC X(10).
MJM003     02  FISCAL-YR-AP45                           PIC X(2).
           02  TEXT-IND-AP45                            PIC X(1).
           02  CMPLT-IND-4150-AP45                      PIC X(1).
           02  APRVL-IND-4150-AP45                      PIC X(1).
           02  APRVL-TMPLT-CODE-AP45                    PIC X(3).
           02  CNCL-IND-4150-AP45                       PIC X(1).
           02  DELETE-LIT-AP45                          PIC X(8).
           02  CONFIRM-DELETE-AP45                      PIC X(1).
           02  ACTN-CODE-GROUP-AP45.
               03 ITEM-ACTN-CODE-AP45  OCCURS 5 TIMES   PIC X(1).
           02  DISTRIBUTION-LINES.
               03 ITEM-NMBR-AP45       OCCURS 5 TIMES   PIC X(4).
               03 SEQ-NMBR-AP45        OCCURS 5 TIMES   PIC X(4).
               03 ACCT-INDX-CD-AP45    OCCURS 5 TIMES   PIC X(10).
               03 FUND-CODE-AP45       OCCURS 5 TIMES   PIC X(6).
               03 ORGZN-CODE-AP45      OCCURS 5 TIMES   PIC X(6).
               03 ACCT-CODE-AP45       OCCURS 5 TIMES   PIC X(6).
               03 PROG-CODE-AP45       OCCURS 5 TIMES   PIC X(6).
               03 LCTN-CODE-4152-AP45  OCCURS 5 TIMES   PIC X(6).
               03 TRAN-AMT-AP45        OCCURS 5 TIMES   PIC X(13).
               03 DEBIT-CREDIT-IND-AP45 OCCURS 5 TIMES  PIC X(1).
