      *%---------------------------------------------------------------*
      *%                                                               *
      *%                   200-FSYSC10 ROUTINE                         *
      *%                                                               *
      *%---------------------------------------------------------------*
       200-FSYSC10.

      ******************************************************************
      ******
      ***  INITIALIZE SCRATCH WORK RECORD
      ******************************************************************
      ******
      *%   GET SCRATCH AREA ID RSP-NAME-SY99 KEEP INTO FSYWG99S FIRST.
           MOVE '0000' TO ERROR-STATUS
           MOVE RSP-NAME-SY99 TO DCGSCRCH-ID-5-8
           MOVE +1 TO DCGSCRCH-ITEM
           PERFORM 850-GET-SCRATCH
              THRU 850-GET-SCRATCH-EXIT
           IF ERROR-STATUS = ZEROS
               MOVE DCGSCRCH-DATA TO FSYWG99S
           END-IF.
      *%   ON (ERROR-STATUS =  '4303'
      *%            OR
      *%       ERROR-STATUS = '4305')
           IF  NOT (ERROR-STATUS = '4303' OR ERROR-STATUS = '4305')
               PERFORM 999-SQL-STATUS
                  THRU 999-SQL-STATUS-EXIT
           ELSE
               CONTINUE.
      *
           IF SCRATCH-AREA-NOT-FOUND   OR
              SCRATCH-REC-NOT-FOUND
               MOVE 'N' TO PAGE-FLAG-SY00
           END-IF.
      *

       200-FSYSC10-EXIT.
           EXIT.
