      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWM31                              04/24/00  12:14  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWM31.
           02  RQSTR-NAME-6311-6117-AP31                PIC X(35).
           02  CHECK-DATE-4158-AP31         OCCURS 12   PIC X(8).
           02  CHECK-AMT-4158-AP31          OCCURS 12   PIC X(17).
           02  ORGN-CODE-DESC-S-4012-AP31   OCCURS 12   PIC X(10).
           02  ORGNT-DCMNT-NMBR-AP31        OCCURS 12   PIC X(14).
           02  DCMNT-STATUS-IND-AP31        OCCURS 12   PIC X(3).
           02  CHECK-NMBR-4158-AP31         OCCURS 12   PIC X(8).
           02  ACTN-CODE-GROUP-AP31.
               03  ACTN-CODE-AP31           OCCURS 12   PIC X(1).
