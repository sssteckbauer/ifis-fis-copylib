      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4071-RQST-DTL' TO DBLINK-CURR-PARAGRAPH.     276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4071-RQST-DTL' TO RECORD-NAME.                        276 BRTN
YYY991     MOVE 'F-RQST' TO AREA-NAME.                                  276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4071-RQST-DTL TO DBLINK-RECORD-MADE-CURRENT.    276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4071-RQST-DTL-EXIT                       276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE RQD-FK-RQT-RQST-CD TO DBLINK-R4071-RQST-DTL-01      276 BRTN
YYY991         MOVE RQD-ITEM-NBR TO DBLINK-R4071-RQST-DTL-02            276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4071-RQST-DTL TO DBLINK-VIEW-NAME-CURRENT.      276 BRTN
YYY991     MOVE DBLINK-R4071-RQST-DTL-KEY TO DBLINK-VIEW-200-CURRENT.   276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4071-4080                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4071-RQST-DTL-01 TO DBLINK-S-4071-4080-01.      276 BRTN
YYY991     MOVE DBLINK-R4071-RQST-DTL-02 TO DBLINK-S-4071-4080-02.      276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4071-4080-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4070-4071                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE RQD-FK-RQT-RQST-CD TO DBLINK-S-4070-4071-01.            276 BRTN
YYY991     MOVE RQD-ITEM-NBR TO DBLINK-S-4070-4071-02.                  276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4072-4071                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-S-4072-4071-F               276 BRTN
YYY991     IF RQD-BYR-SETF = DBLINK-MEMBER                              276 BRTN
YYY991         MOVE RQD-BYR-SETF TO DBLINK-S-4072-4071-F                276 BRTN
YYY991         MOVE RQD-FK-BYR-BUYER-CD TO DBLINK-S-4072-4071-01        276 BRTN
YYY991         MOVE RQD-FK-RQT-RQST-CD TO DBLINK-S-4072-4071-02         276 BRTN
YYY991         MOVE RQD-ITEM-NBR TO DBLINK-S-4072-4071-03               276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4074-4071                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-S-4074-4071-F               276 BRTN
YYY991     IF RQD-CDY-SETF = DBLINK-MEMBER                              276 BRTN
YYY991         MOVE RQD-CDY-SETF TO DBLINK-S-4074-4071-F                276 BRTN
YYY991         MOVE RQD-FK-CDY-CMDTY-CD TO DBLINK-S-4074-4071-01        276 BRTN
YYY991         MOVE RQD-FK-RQT-RQST-CD TO DBLINK-S-4074-4071-02         276 BRTN
YYY991         MOVE RQD-CDY-SET-TS TO DBLINK-S-4074-4071-03             276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4085-4071                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-S-4085-4071-F               276 BRTN
YYY991     IF RQD-POD-SETF = DBLINK-MEMBER                              276 BRTN
YYY991         MOVE RQD-POD-SETF TO DBLINK-S-4085-4071-F                276 BRTN
YYY991         MOVE RQD-FK-POD-PO-NBR TO DBLINK-S-4085-4071-01          276 BRTN
YYY991         MOVE RQD-FK-POD-CHG-SEQ-NBR TO DBLINK-S-4085-4071-02     276 BRTN
YYY991         MOVE RQD-FK-POD-ITEM-NBR TO DBLINK-S-4085-4071-03        276 BRTN
YYY991         MOVE RQD-POD-SET-TS TO DBLINK-S-4085-4071-04             276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4089-4071                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-S-4089-4071-F               276 BRTN
YYY991     IF RQD-BDD-SETF = DBLINK-MEMBER                              276 BRTN
YYY991         MOVE RQD-BDD-SETF TO DBLINK-S-4089-4071-F                276 BRTN
YYY991         MOVE RQD-FK-BDD-BID-NBR TO DBLINK-S-4089-4071-01         276 BRTN
YYY991         MOVE RQD-FK-BDD-ITEM-NBR TO DBLINK-S-4089-4071-02        276 BRTN
YYY991         MOVE RQD-BDD-SET-TS TO DBLINK-S-4089-4071-03             276 BRTN
YYY991     END-IF.                                                      276 BRTN
