      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FGAWM01                              04/24/00  12:30  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FGAWM01.
           02  ENCMBRNC-DESC-4191-GA01                  PIC X(35).
           02  ENCMBRNC-TYPE-4191-GA01                  PIC X(1).
           02  ENCMBRNC-STATUS-4191-GA01                PIC X(1).
           02  BALANCE-GA01                             PIC X(16).
           02  ORIG-ENCMBRNC-AMT-GA01                   PIC X(16).
           02  ESTBLD-DATE-4191-GA01                    PIC X(6).
           02  VENDOR-CODE-4191-GA01.
               03  VNDR-ID-DIGIT-ONE-GA01               PIC X(1).
               03  VNDR-ID-LAST-NINE-GA01               PIC X(9).
           02  PRJCT-CODE-4190-GA01                     PIC X(8).
           02  TRANS-DATE-4189-GA01         OCCURS 7    PIC X(6).
           02  JRNL-TYPE-4189-GA01          OCCURS 7    PIC X(4).
           02  DCMNT-NMBR-4189-GA01         OCCURS 7    PIC X(8).
           02  ENCMBRNC-ACTN-IND-4188-GA01  OCCURS 7    PIC X(1).
           02  AMT-4189-GA01                OCCURS 7    PIC X(16).
           02  REMAIN-BALANCE-GA01          OCCURS 7    PIC X(16).
           02  COA-CODE-4190-GA01                       PIC X(1).
           02  ACCT-INDX-CODE-4190-GA01                 PIC X(10).
           02  FUND-CODE-4190-GA01                      PIC X(6).
           02  ORGZN-CODE-4190-GA01                     PIC X(6).
           02  ACCT-CODE-4190-GA01                      PIC X(6).
           02  PRGRM-CODE-4190-GA01                     PIC X(6).
           02  ACTVY-CODE-4190-GA01                     PIC X(6).
           02  LCTN-CODE-4190-GA01                      PIC X(6).
           02  ENTY-PERS-NAME-GA01                      PIC X(55).
           02  FSCL-ENCMBRNC-BAL-GA01                   PIC X(16).
           02  SMRY-ENCMBRNC-LIQDTN-GA01                PIC X(16).
           02  SMRY-ENCMBRNC-ORIG-GA01                  PIC X(16).
