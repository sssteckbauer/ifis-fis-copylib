       976-FROM-R4158-CHK-HDR SECTION.

FCCCE**        MOVE USER-CODE-4158 OF R4158-CHK-HDR
FCCCE          MOVE DBLINK-USER-CD
               TO CKH-USER-CD.

               MOVE LAST-ACTVY-DATE-4158 OF R4158-CHK-HDR
               TO CKH-LAST-ACTVY-DT.

               MOVE UNVRS-CODE-4158 OF R4158-CHK-HDR
               TO CKH-UNVRS-CD.

               MOVE BANK-ACCT-CODE-4158 OF R4158-CHK-HDR
               TO CKH-BANK-ACCT-CD.

               MOVE CHECK-NMBR-4158 OF R4158-CHK-HDR
               TO CKH-CHECK-NBR.

           IF CHECK-DATE-4158 OF R4158-CHK-HDR  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO CKH-CHECK-DT
           ELSE
               MOVE CHECK-DATE-4158 OF R4158-CHK-HDR
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO CKH-CHECK-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO CKH-CHECK-DT
               END-IF
           END-IF.

               MOVE CHECK-TYPE-CODE-4158 OF R4158-CHK-HDR
               TO CKH-CHECK-TYP-CD.

           IF CHECK-AMT-4158 OF R4158-CHK-HDR  NOT NUMERIC
               MOVE ZEROS TO CKH-CHECK-AMT
           ELSE
               MOVE CHECK-AMT-4158 OF R4158-CHK-HDR
               TO CKH-CHECK-AMT
           END-IF.

           IF CHECK-RGSTR-DATE-4158 OF R4158-CHK-HDR  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO CKH-CHECK-RGSTR-DT
           ELSE
               MOVE CHECK-RGSTR-DATE-4158 OF R4158-CHK-HDR
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO CKH-CHECK-RGSTR-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO CKH-CHECK-RGSTR-DT
               END-IF
           END-IF.

               MOVE CNCL-IND-4158 OF R4158-CHK-HDR
               TO CKH-CNCL-IND.

               MOVE PRCSD-IND-4158 OF R4158-CHK-HDR
               TO CKH-PRCSD-IND.

               MOVE RCNCLTN-IND-4158 OF R4158-CHK-HDR
               TO CKH-RCNCLTN-IND.

           IF CHECK-RCNCLTN-DATE-4158 OF R4158-CHK-HDR  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO CKH-CHECK-RCNCLTN-DT
           ELSE
               MOVE CHECK-RCNCLTN-DATE-4158 OF R4158-CHK-HDR
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO CKH-CHECK-RCNCLTN-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO CKH-CHECK-RCNCLTN-DT
               END-IF
           END-IF.

               MOVE DISCREPANCY-IND-4158 OF R4158-CHK-HDR
               TO CKH-DISCREPANCY-IND.

           IF CNCL-DATE-4158 OF R4158-CHK-HDR  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO CKH-CNCL-DT
           ELSE
               MOVE CNCL-DATE-4158 OF R4158-CHK-HDR
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO CKH-CNCL-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO CKH-CNCL-DT
               END-IF
           END-IF.

               MOVE STOP-PAID-IND-4158 OF R4158-CHK-HDR
               TO CKH-STOP-PAID-IND.

           IF ISSUE-BANK-DATE-4158 OF R4158-CHK-HDR  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO CKH-ISSUE-BANK-DT
           ELSE
               MOVE ISSUE-BANK-DATE-4158 OF R4158-CHK-HDR
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO CKH-ISSUE-BANK-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO CKH-ISSUE-BANK-DT
               END-IF
           END-IF.

           IF ISSUE-RGSTR-DATE-4158 OF R4158-CHK-HDR  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO CKH-ISSU-RGSTR-DT
           ELSE
               MOVE ISSUE-RGSTR-DATE-4158 OF R4158-CHK-HDR
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO CKH-ISSU-RGSTR-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO CKH-ISSU-RGSTR-DT
               END-IF
           END-IF.

               MOVE CHK-CNCL-CODE-4158 OF R4158-CHK-HDR
               TO CKH-CHECK-CNCL-CD.
       976-FROM-R4158-CHK-HDR-EXIT.
           EXIT.
