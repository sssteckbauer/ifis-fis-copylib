    ***Created by Convert/DC version V8R03 on 11/20/00 at 15:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PU49 OF FPUWM49(0001) TO WK01-CODE-PU491.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PU49 OF FPUWM49(0002) TO WK01-CODE-PU492.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PU49 OF FPUWM49(0003) TO WK01-CODE-PU493.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PU49 OF FPUWM49(0004) TO WK01-CODE-PU494.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PU49 OF FPUWM49(0005) TO WK01-CODE-PU495.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PU49 OF FPUWM49(0006) TO WK01-CODE-PU496.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PU49 OF FPUWM49(0007) TO WK01-CODE-PU497.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PU49 OF FPUWM49(0008) TO WK01-CODE-PU498.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PU49 OF FPUWM49(0009) TO WK01-CODE-PU499.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PU49 OF FPUWM49(0010) TO WK01-CODE-PU4910.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PU49 OF FPUWM49(0011) TO WK01-CODE-PU4911.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PU49 OF FPUWM49(0012) TO WK01-CODE-PU4912.
      *%--------------------------------------------------------------%*
           MOVE BUYER-CODE-4072-PU49 OF FPUWM49(0001) TO
                WK01-CODE-4072-PU491.
      *%--------------------------------------------------------------%*
           MOVE BUYER-CODE-4072-PU49 OF FPUWM49(0002) TO
                WK01-CODE-4072-PU492.
      *%--------------------------------------------------------------%*
           MOVE BUYER-CODE-4072-PU49 OF FPUWM49(0003) TO
                WK01-CODE-4072-PU493.
      *%--------------------------------------------------------------%*
           MOVE BUYER-CODE-4072-PU49 OF FPUWM49(0004) TO
                WK01-CODE-4072-PU494.
      *%--------------------------------------------------------------%*
           MOVE BUYER-CODE-4072-PU49 OF FPUWM49(0005) TO
                WK01-CODE-4072-PU495.
      *%--------------------------------------------------------------%*
           MOVE BUYER-CODE-4072-PU49 OF FPUWM49(0006) TO
                WK01-CODE-4072-PU496.
      *%--------------------------------------------------------------%*
           MOVE BUYER-CODE-4072-PU49 OF FPUWM49(0007) TO
                WK01-CODE-4072-PU497.
      *%--------------------------------------------------------------%*
           MOVE BUYER-CODE-4072-PU49 OF FPUWM49(0008) TO
                WK01-CODE-4072-PU498.
      *%--------------------------------------------------------------%*
           MOVE BUYER-CODE-4072-PU49 OF FPUWM49(0009) TO
                WK01-CODE-4072-PU499.
      *%--------------------------------------------------------------%*
           MOVE BUYER-CODE-4072-PU49 OF FPUWM49(0010) TO
                WK01-CODE-4072-PU4910.
      *%--------------------------------------------------------------%*
           MOVE BUYER-CODE-4072-PU49 OF FPUWM49(0011) TO
                WK01-CODE-4072-PU4911.
      *%--------------------------------------------------------------%*
           MOVE BUYER-CODE-4072-PU49 OF FPUWM49(0012) TO
                WK01-CODE-4072-PU4912.
      *%--------------------------------------------------------------%*
           MOVE BUYER-NAME-4072-PU49 OF FPUWM49(0001) TO
                WK01-NAME-4072-PU491.
      *%--------------------------------------------------------------%*
           MOVE BUYER-NAME-4072-PU49 OF FPUWM49(0002) TO
                WK01-NAME-4072-PU492.
      *%--------------------------------------------------------------%*
           MOVE BUYER-NAME-4072-PU49 OF FPUWM49(0003) TO
                WK01-NAME-4072-PU493.
      *%--------------------------------------------------------------%*
           MOVE BUYER-NAME-4072-PU49 OF FPUWM49(0004) TO
                WK01-NAME-4072-PU494.
      *%--------------------------------------------------------------%*
           MOVE BUYER-NAME-4072-PU49 OF FPUWM49(0005) TO
                WK01-NAME-4072-PU495.
      *%--------------------------------------------------------------%*
           MOVE BUYER-NAME-4072-PU49 OF FPUWM49(0006) TO
                WK01-NAME-4072-PU496.
      *%--------------------------------------------------------------%*
           MOVE BUYER-NAME-4072-PU49 OF FPUWM49(0007) TO
                WK01-NAME-4072-PU497.
      *%--------------------------------------------------------------%*
           MOVE BUYER-NAME-4072-PU49 OF FPUWM49(0008) TO
                WK01-NAME-4072-PU498.
      *%--------------------------------------------------------------%*
           MOVE BUYER-NAME-4072-PU49 OF FPUWM49(0009) TO
                WK01-NAME-4072-PU499.
      *%--------------------------------------------------------------%*
           MOVE BUYER-NAME-4072-PU49 OF FPUWM49(0010) TO
                WK01-NAME-4072-PU4910.
      *%--------------------------------------------------------------%*
           MOVE BUYER-NAME-4072-PU49 OF FPUWM49(0011) TO
                WK01-NAME-4072-PU4911.
      *%--------------------------------------------------------------%*
           MOVE BUYER-NAME-4072-PU49 OF FPUWM49(0012) TO
                WK01-NAME-4072-PU4912.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-AREA-CODE-PU49 OF FPUWM49(0001) TO
                WK01-AREA-CODE-PU491.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-AREA-CODE-PU49 OF FPUWM49(0002) TO
                WK01-AREA-CODE-PU492.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-AREA-CODE-PU49 OF FPUWM49(0003) TO
                WK01-AREA-CODE-PU493.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-AREA-CODE-PU49 OF FPUWM49(0004) TO
                WK01-AREA-CODE-PU494.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-AREA-CODE-PU49 OF FPUWM49(0005) TO
                WK01-AREA-CODE-PU495.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-AREA-CODE-PU49 OF FPUWM49(0006) TO
                WK01-AREA-CODE-PU496.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-AREA-CODE-PU49 OF FPUWM49(0007) TO
                WK01-AREA-CODE-PU497.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-AREA-CODE-PU49 OF FPUWM49(0008) TO
                WK01-AREA-CODE-PU498.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-AREA-CODE-PU49 OF FPUWM49(0009) TO
                WK01-AREA-CODE-PU499.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-AREA-CODE-PU49 OF FPUWM49(0010) TO
                WK01-AREA-CODE-PU4910.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-AREA-CODE-PU49 OF FPUWM49(0011) TO
                WK01-AREA-CODE-PU4911.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-AREA-CODE-PU49 OF FPUWM49(0012) TO
                WK01-AREA-CODE-PU4912.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XCHNG-ID-PU49 OF FPUWM49(0001) TO
                WK01-XCHNG-ID-PU491.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XCHNG-ID-PU49 OF FPUWM49(0002) TO
                WK01-XCHNG-ID-PU492.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XCHNG-ID-PU49 OF FPUWM49(0003) TO
                WK01-XCHNG-ID-PU493.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XCHNG-ID-PU49 OF FPUWM49(0004) TO
                WK01-XCHNG-ID-PU494.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XCHNG-ID-PU49 OF FPUWM49(0005) TO
                WK01-XCHNG-ID-PU495.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XCHNG-ID-PU49 OF FPUWM49(0006) TO
                WK01-XCHNG-ID-PU496.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XCHNG-ID-PU49 OF FPUWM49(0007) TO
                WK01-XCHNG-ID-PU497.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XCHNG-ID-PU49 OF FPUWM49(0008) TO
                WK01-XCHNG-ID-PU498.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XCHNG-ID-PU49 OF FPUWM49(0009) TO
                WK01-XCHNG-ID-PU499.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XCHNG-ID-PU49 OF FPUWM49(0010) TO
                WK01-XCHNG-ID-PU4910.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XCHNG-ID-PU49 OF FPUWM49(0011) TO
                WK01-XCHNG-ID-PU4911.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XCHNG-ID-PU49 OF FPUWM49(0012) TO
                WK01-XCHNG-ID-PU4912.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-SEQ-ID-PU49 OF FPUWM49(0001) TO WK01-SEQ-ID-PU491.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-SEQ-ID-PU49 OF FPUWM49(0002) TO WK01-SEQ-ID-PU492.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-SEQ-ID-PU49 OF FPUWM49(0003) TO WK01-SEQ-ID-PU493.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-SEQ-ID-PU49 OF FPUWM49(0004) TO WK01-SEQ-ID-PU494.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-SEQ-ID-PU49 OF FPUWM49(0005) TO WK01-SEQ-ID-PU495.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-SEQ-ID-PU49 OF FPUWM49(0006) TO WK01-SEQ-ID-PU496.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-SEQ-ID-PU49 OF FPUWM49(0007) TO WK01-SEQ-ID-PU497.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-SEQ-ID-PU49 OF FPUWM49(0008) TO WK01-SEQ-ID-PU498.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-SEQ-ID-PU49 OF FPUWM49(0009) TO WK01-SEQ-ID-PU499.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-SEQ-ID-PU49 OF FPUWM49(0010) TO
                WK01-SEQ-ID-PU4910.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-SEQ-ID-PU49 OF FPUWM49(0011) TO
                WK01-SEQ-ID-PU4911.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-SEQ-ID-PU49 OF FPUWM49(0012) TO
                WK01-SEQ-ID-PU4912.
      *%--------------------------------------------------------------%*
DEVBWS     MOVE RQST-BUYER-NAME-PU49 OF FPUWK49 TO WK01-BUYER-NAME-PU49.
DEVBWS*%--------------------------------------------------------------%*
           MOVE RQST-BUYER-CODE-PU49 OF FPUWK49 TO WK01-BUYER-CODE-PU49.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XTNSN-ID-PU49 OF FPUWM49(0001) TO
                WK01-XTNSN-ID-PU491.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XTNSN-ID-PU49 OF FPUWM49(0002) TO
                WK01-XTNSN-ID-PU492.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XTNSN-ID-PU49 OF FPUWM49(0003) TO
                WK01-XTNSN-ID-PU493.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XTNSN-ID-PU49 OF FPUWM49(0004) TO
                WK01-XTNSN-ID-PU494.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XTNSN-ID-PU49 OF FPUWM49(0005) TO
                WK01-XTNSN-ID-PU495.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XTNSN-ID-PU49 OF FPUWM49(0006) TO
                WK01-XTNSN-ID-PU496.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XTNSN-ID-PU49 OF FPUWM49(0007) TO
                WK01-XTNSN-ID-PU497.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XTNSN-ID-PU49 OF FPUWM49(0008) TO
                WK01-XTNSN-ID-PU498.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XTNSN-ID-PU49 OF FPUWM49(0009) TO
                WK01-XTNSN-ID-PU499.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XTNSN-ID-PU49 OF FPUWM49(0010) TO
                WK01-XTNSN-ID-PU4910.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XTNSN-ID-PU49 OF FPUWM49(0011) TO
                WK01-XTNSN-ID-PU4911.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XTNSN-ID-PU49 OF FPUWM49(0012) TO
                WK01-XTNSN-ID-PU4912.
      *%--------------------------------------------------------------%*
           MOVE MAIL-CODE-PU49 OF FPUWM49(0001) TO WK01-CODE-PU4A1.
      *%--------------------------------------------------------------%*
           MOVE MAIL-CODE-PU49 OF FPUWM49(0002) TO WK01-CODE-PU4A2.
      *%--------------------------------------------------------------%*
           MOVE MAIL-CODE-PU49 OF FPUWM49(0003) TO WK01-CODE-PU4A3.
      *%--------------------------------------------------------------%*
           MOVE MAIL-CODE-PU49 OF FPUWM49(0004) TO WK01-CODE-PU4A4.
      *%--------------------------------------------------------------%*
           MOVE MAIL-CODE-PU49 OF FPUWM49(0005) TO WK01-CODE-PU4A5.
      *%--------------------------------------------------------------%*
           MOVE MAIL-CODE-PU49 OF FPUWM49(0006) TO WK01-CODE-PU4A6.
      *%--------------------------------------------------------------%*
           MOVE MAIL-CODE-PU49 OF FPUWM49(0007) TO WK01-CODE-PU4A7.
      *%--------------------------------------------------------------%*
           MOVE MAIL-CODE-PU49 OF FPUWM49(0008) TO WK01-CODE-PU4A8.
      *%--------------------------------------------------------------%*
           MOVE MAIL-CODE-PU49 OF FPUWM49(0009) TO WK01-CODE-PU4A9.
      *%--------------------------------------------------------------%*
           MOVE MAIL-CODE-PU49 OF FPUWM49(0010) TO WK01-CODE-PU4A10.
      *%--------------------------------------------------------------%*
           MOVE MAIL-CODE-PU49 OF FPUWM49(0011) TO WK01-CODE-PU4A11.
      *%--------------------------------------------------------------%*
           MOVE MAIL-CODE-PU49 OF FPUWM49(0012) TO WK01-CODE-PU4A12.
