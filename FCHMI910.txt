    ***Created by Convert/DC version V8R03 on 11/01/00 at 08:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4012-CH911-Z = 'Y'
               MOVE WK01-TITLE-4012-CH911 TO HRCHY-TITLE-4012-CH91 OF
                FCHWM91(0001)
           END-IF.
           MOVE WK01-TITLE-4012-CH911-F TO WK01-TITLE-4012-CH91-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4012-CH912-Z = 'Y'
               MOVE WK01-TITLE-4012-CH912 TO HRCHY-TITLE-4012-CH91 OF
                FCHWM91(0002)
           END-IF.
           MOVE WK01-TITLE-4012-CH912-F TO WK01-TITLE-4012-CH91-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4012-CH913-Z = 'Y'
               MOVE WK01-TITLE-4012-CH913 TO HRCHY-TITLE-4012-CH91 OF
                FCHWM91(0003)
           END-IF.
           MOVE WK01-TITLE-4012-CH913-F TO WK01-TITLE-4012-CH91-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4012-CH914-Z = 'Y'
               MOVE WK01-TITLE-4012-CH914 TO HRCHY-TITLE-4012-CH91 OF
                FCHWM91(0004)
           END-IF.
           MOVE WK01-TITLE-4012-CH914-F TO WK01-TITLE-4012-CH91-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4012-CH915-Z = 'Y'
               MOVE WK01-TITLE-4012-CH915 TO HRCHY-TITLE-4012-CH91 OF
                FCHWM91(0005)
           END-IF.
           MOVE WK01-TITLE-4012-CH915-F TO WK01-TITLE-4012-CH91-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-FUND-4001-CH911-Z = 'Y'
               MOVE WK01-FUND-4001-CH911 TO HRCHY-FUND-4001-CH91 OF
                FCHWM91(0001)
           END-IF.
           MOVE WK01-FUND-4001-CH911-F TO WK01-FUND-4001-CH91-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-FUND-4001-CH912-Z = 'Y'
               MOVE WK01-FUND-4001-CH912 TO HRCHY-FUND-4001-CH91 OF
                FCHWM91(0002)
           END-IF.
           MOVE WK01-FUND-4001-CH912-F TO WK01-FUND-4001-CH91-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-FUND-4001-CH913-Z = 'Y'
               MOVE WK01-FUND-4001-CH913 TO HRCHY-FUND-4001-CH91 OF
                FCHWM91(0003)
           END-IF.
           MOVE WK01-FUND-4001-CH913-F TO WK01-FUND-4001-CH91-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-FUND-4001-CH914-Z = 'Y'
               MOVE WK01-FUND-4001-CH914 TO HRCHY-FUND-4001-CH91 OF
                FCHWM91(0004)
           END-IF.
           MOVE WK01-FUND-4001-CH914-F TO WK01-FUND-4001-CH91-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-FUND-4001-CH915-Z = 'Y'
               MOVE WK01-FUND-4001-CH915 TO HRCHY-FUND-4001-CH91 OF
                FCHWM91(0005)
           END-IF.
           MOVE WK01-FUND-4001-CH915-F TO WK01-FUND-4001-CH91-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH91-Z = 'Y'
               MOVE WK01-CODE-4001-CH91 TO COA-CODE-4001-CH91 OF FCHWM91
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH9A-Z = 'Y'
               MOVE WK01-CODE-4001-CH9A TO FUND-CODE-4001-CH91 OF
                FCHWM91
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4005-CH91-Z = 'Y'
               MOVE WK01-TITLE-4005-CH91 TO FUND-TITLE-4005-CH91 OF
                FCHWM91
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4005-CH91-Z = 'Y'
               MOVE WK01-CODE-4005-CH91 TO PREDCSR-CODE-4005-CH91 OF
                FCHWM91
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4005-CH9A-Z = 'Y'
               MOVE WK01-TITLE-4005-CH9A TO PREDCSR-TITLE-4005-CH91 OF
                FCHWM91
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUND-TITLE-4005-CH911-Z = 'Y'
               MOVE WK01-FUND-TITLE-4005-CH911 TO
                HRCHY-FUND-TITLE-4005-CH91 OF FCHWM91(0001)
           END-IF.
           MOVE WK01-FUND-TITLE-4005-CH911-F TO
                WK01-FUND-TITLE-4005-CH91-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-FUND-TITLE-4005-CH912-Z = 'Y'
               MOVE WK01-FUND-TITLE-4005-CH912 TO
                HRCHY-FUND-TITLE-4005-CH91 OF FCHWM91(0002)
           END-IF.
           MOVE WK01-FUND-TITLE-4005-CH912-F TO
                WK01-FUND-TITLE-4005-CH91-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-FUND-TITLE-4005-CH913-Z = 'Y'
               MOVE WK01-FUND-TITLE-4005-CH913 TO
                HRCHY-FUND-TITLE-4005-CH91 OF FCHWM91(0003)
           END-IF.
           MOVE WK01-FUND-TITLE-4005-CH913-F TO
                WK01-FUND-TITLE-4005-CH91-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-FUND-TITLE-4005-CH914-Z = 'Y'
               MOVE WK01-FUND-TITLE-4005-CH914 TO
                HRCHY-FUND-TITLE-4005-CH91 OF FCHWM91(0004)
           END-IF.
           MOVE WK01-FUND-TITLE-4005-CH914-F TO
                WK01-FUND-TITLE-4005-CH91-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-FUND-TITLE-4005-CH915-Z = 'Y'
               MOVE WK01-FUND-TITLE-4005-CH915 TO
                HRCHY-FUND-TITLE-4005-CH91 OF FCHWM91(0005)
           END-IF.
           MOVE WK01-FUND-TITLE-4005-CH915-F TO
                WK01-FUND-TITLE-4005-CH91-F (5).
