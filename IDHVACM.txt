       976-FROM-R6202-ACH-COMM SECTION.

FCYI **        MOVE USER-CODE-6202 OF R6202-ACH-COMM
FCYI           MOVE DBLINK-USER-CD
               TO ACM-USER-CD.

               MOVE LAST-ACTVY-DATE-6202 OF R6202-ACH-COMM
               TO ACM-LAST-ACTVY-DT.

               MOVE COMM-PLAN-CODE-6202 OF R6202-ACH-COMM
               TO ACM-COMM-PLAN-CD.

               MOVE TEXT-CODE-6202 OF R6202-ACH-COMM
               TO ACM-TEXT-CD.

           IF END-DATE-6202 OF R6202-ACH-COMM  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO ACM-END-DT
           ELSE
               MOVE END-DATE-6202 OF R6202-ACH-COMM
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO ACM-END-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO ACM-END-DT
               END-IF
           END-IF.

           IF START-DATE-6202 OF R6202-ACH-COMM  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO ACM-START-DT
           ELSE
               MOVE START-DATE-6202 OF R6202-ACH-COMM
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO ACM-START-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO ACM-START-DT
               END-IF
           END-IF.

           IF ACTVY-DATE-6202 OF R6202-ACH-COMM  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO ACM-ACTVY-DT
           ELSE
               MOVE ACTVY-DATE-6202 OF R6202-ACH-COMM
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO ACM-ACTVY-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO ACM-ACTVY-DT
               END-IF
           END-IF.
       976-FROM-R6202-ACH-COMM-EXIT.
           EXIT.
