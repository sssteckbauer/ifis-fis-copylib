000000**************************************************************/   3202RRRR
000001*  COPYMEMBER: CPWSFAUS                                      */   3202RRRR
000002*  RELEASE: ___RRRR______ SERVICE REQUEST(S): ____13202____  */   3202RRRR
000003*  NAME:_______WJG_______ MODIFICATION DATE:  ___MM/DD/YY__  */   3202RRRR
000004*  DESCRIPTION:                                              */   3202RRRR
000600*    INITIAL RELEASE OF COPYMEMBER REDEFINING COMPONENTS OF  */   3202RRRR
000700*    STANDARD FAU.                                           */   3202RRRR
000800**************************************************************/   3202RRRR
000900*                                                             *   CPWSFAUS
001000*  THIS COPYMEMBER IS ONE OF THE FULL ACCOUNTING UNIT MODULES *   CPWSFAUS
001100*  WHICH CAMPUSES MAY NEED TO MODIFY TO ACCOMODATE THEIR OWN  *   CPWSFAUS
001200*  CHART OF ACCOUNTS STRUCTURE.                               *   CPWSFAUS
001300*                                                             *   CPWSFAUS
001400*  IN PARTICULAR, THIS COPYMEMBER MUST REDEFINE THE VARIOUS   *   CPWSFAUS
001500*  FAUS THAT ARE INVOLVED IN THE STAFFING PROCESS, PERMITTING *   CPWSFAUS
001600*  TRANSLATIONS FROM ONE TO ANOTHER.                          *   CPWSFAUS
001700***************************************************************   CPWSFAUS
001800*01  FAUS-FAU-REDEFINITIONS.                                      CPWSFAUS
001900     03  FAUS-PPS-FAU                PIC X(30).                   CPWSFAUS
002000     03  FILLER                      REDEFINES                    CPWSFAUS
002100         FAUS-PPS-FAU.                                            CPWSFAUS
002300         05 FAUS-PPS-LOCATION        PIC X(01).                   CPWSFAUS
002310         05 FAUS-PPS-ACCOUNT         PIC X(06).                   CPWSFAUS
002400         05 FAUS-PPS-COST-CENTER     PIC X(04).                   CPWSFAUS
002500         05 FAUS-PPS-FUND            PIC X(05).                   CPWSFAUS
002600         05 FAUS-PPS-PROJECT-CODE    PIC X(06).                   CPWSFAUS
002700         05 FAUS-PPS-SUB-BUDGET      PIC X(01).                   CPWSFAUS
002900         05 FAUS-PPS-FILLER          PIC X(07).                   CPWSFAUS
002910     03  FAUS-PROV-FAU               PIC X(31).                   CPWSFAUS
002920     03  FILLER                      REDEFINES                    CPWSFAUS
002930         FAUS-PROV-FAU.                                           CPWSFAUS
002940         05 FAUS-PROV-LOCATION       PIC X(02).                   CPWSFAUS
002941         05 FAUS-PROV-SAU            PIC X(01).                   CPWSFAUS
002942         05 FAUS-PROV-SUB-CAMPUS     PIC X(01).                   CPWSFAUS
002950         05 FAUS-PROV-ACCOUNT        PIC X(07).                   CPWSFAUS
002960         05 FAUS-PROV-COST-CENTER    PIC X(05).                   CPWSFAUS
002970         05 FAUS-PROV-FUND           PIC X(06).                   CPWSFAUS
002980         05 FAUS-PROV-PROJECT-CODE   PIC X(07).                   CPWSFAUS
002990         05 FAUS-PROV-SUB-BUDGET     PIC X(02).                   CPWSFAUS
003000     03  FAUS-CORP-FAU               PIC X(19).                   CPWSFAUS
003100     03  FILLER                      REDEFINES                    CPWSFAUS
003200         FAUS-CORP-FAU.                                           CPWSFAUS
003300         05 FAUS-CORP-LOCATION       PIC X(02).                   CPWSFAUS
003301         05 FAUS-CORP-SAU            PIC X(01).                   CPWSFAUS
003302         05 FAUS-CORP-SUB-CAMPUS     PIC X(01).                   CPWSFAUS
003310         05 FAUS-CORP-ACCOUNT        PIC X(07).                   CPWSFAUS
003500         05 FAUS-CORP-FUND           PIC X(06).                   CPWSFAUS
003700         05 FAUS-CORP-SUB-BUDGET     PIC X(02).                   CPWSFAUS
003800     03  FAUS-P040-FAU               PIC X(19).                   CPWSFAUS
003900     03  FILLER                      REDEFINES                    CPWSFAUS
004000         FAUS-P040-FAU.                                           CPWSFAUS
004100         05 FAUS-P040-LOCATION       PIC X(02).                   CPWSFAUS
004200         05 FAUS-P040-SAU            PIC X(01).                   CPWSFAUS
004400         05 FAUS-P040-ACCOUNT        PIC X(07).                   CPWSFAUS
004500         05 FAUS-P040-FUND           PIC X(06).                   CPWSFAUS
004600         05 FAUS-P040-SUB-BUDGET     PIC X(02).                   CPWSFAUS
004700         05 FAUS-P040-SUB-CAMPUS     PIC X(01).                   CPWSFAUS
