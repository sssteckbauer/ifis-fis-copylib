      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPEWM44                              04/24/00  12:34  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPEWM44.
           02  FICE-CODE-6001-PE44                      PIC X(6).
           02  HOLD-CODE-2134-PE44                      PIC X(4).
           02  HOLD-TYPE-CODE-2132-PE44                 PIC X(2).
           02  LONG-DESC-2132-PE44                      PIC X(30).
           02  LONG-DESC-2134-PE44                      PIC X(30).
           02  ENTY-ID-6186-PE44.
               03  ENTY-ID-DIGIT-ONE-6186-PE44          PIC X(1).
               03  ENTY-ID-LAST-NINE-6186-PE44          PIC X(9).
           02  NAME-KEY-6311-PE44                       PIC X(35).
           02  CNTRY-CODE-6153-PE44                     PIC X(2).
           02  FULL-NAME-6153-PE44                      PIC X(35).
           02  STATE-CODE-6151-PE44                     PIC X(2).
           02  FULL-NAME-6151-PE44                      PIC X(35).
           02  ENTY-TYPE-CODE-6182-PE44                 PIC X(4).
           02  LONG-DESC-6182-PE44                      PIC X(30).
           02  ENTY-CODE-6349-PE44                      PIC X(4).
           02  LONG-DESC-6349-PE44                      PIC X(30).
           02  CMNT-TYPE-CODE-6140-PE44                 PIC X(4).
           02  LONG-DESC-6140-PE44                      PIC X(30).
           02  CMNT-CODE-6345-PE44                      PIC X(4).
           02  LONG-DESC-6345-PE44                      PIC X(30).
           02  WORK-PRSN-ID-PE44.
               03  WORK-PRSN-ID-ONE-PE44                PIC X(3).
               03  WORK-PRSN-ID-TWO-PE44                PIC X(2).
               03  WORK-PRSN-ID-THREE-PE44              PIC X(4).
           02  NAME-KEY-6117-PE44                       PIC X(35).
           02  ADR-TYPE-CODE-6137-PE44                  PIC X(2).
           02  LONG-DESC-6137-PE44                      PIC X(30).
           02  MAP-RQST-DATE-PE44                       PIC X(6).
           02  MAP-TERM-CODE-PE44                       PIC X(4).
           02  OFC-CODE-2155-PE44                       PIC X(4).
           02  FULL-NAME-2155-PE44                      PIC X(35).
