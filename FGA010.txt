       01  FGA010-RECORD.
           03  FGA010-SORT-KEY.
               05  FGA010-FIXED-KEY.
                   07  FGA010-UNVRS-ID         PIC X(02).
                   07  FGA010-USER-ID          PIC X(08).
                   07  FGA010-RPRT-CODE        PIC X(08).
                   07  FGA010-SEQ-NMBR         PIC 9(04).
               05  FGA010-VAR-KEY.
                   07  FGA010-COA-CODE         PIC X(01).
                   07  FGA010-ORGZN-CODE       PIC X(06).
                   07  FGA010-FUND-CODE        PIC X(06).
                   07  FGA010-PRGRM-CODE       PIC X(06).
                   07  FGA010-ACCT-CODE        PIC X(06).
                   07  FGA010-TRANS-DATE       PIC 9(08)  COMP-3.
                   07  FGA010-JRNL-TYPE        PIC X(04).
                   07  FGA010-DCMNT-NMBR       PIC X(08).
                   07  FGA010-DCMNT-REF-NMBR   PIC X(10).
                   07  FILLER                  PIC X(05).
               05  FGA010-RCRD-TYPE            PIC X(01).
           03  FGA010-DATA                     PIC X(360).
           03  FGA010-ONE-TIME-DATA REDEFINES
               FGA010-DATA.
               05  FGA010-AS-OF-DATE           PIC 9(08)    COMP-3.
               05  FGA010-UNVRS-TITLE          PIC X(35).
               05  FGA010-USER-NAME            PIC X(35).
               05  FGA010-COA-DESC             PIC X(35).
               05  FGA010-CRNT-PRD             PIC 9(02).
               05  FGA010-PARM-FSCL-YR         PIC X(02).
               05  FGA010-PARM-PRINT-ORGZN-TOT PIC X(01).
               05  FILLER                      PIC X(245).
           03  FGA010-ORGN-DATA REDEFINES
               FGA010-DATA.
               05  FGA010-FUND-TITLE           PIC X(35).
               05  FGA010-ORGZN-TITLE          PIC X(35).
               05  FGA010-PRGRM-TITLE          PIC X(35).
               05  FGA010-ACCT-CODE-TITLE      PIC X(35).
               05  FGA010-PREDCSR-ACCT-TYPE    PIC X(02).
               05  FGA010-PREDCSR-ACTT-DESC    PIC X(35).
               05  FGA010-ACCT-BDGT-AMT        PIC S9(11)V99 COMP-3.
               05  FGA010-PRD-ACTVY-AMT        PIC S9(11)V99 COMP-3.
               05  FGA010-BDGT-RSRVTN-AMT      PIC S9(11)V99 COMP-3.
               05  FGA010-ENCMBRNC-AMT         PIC S9(11)V99 COMP-3.
               05  FILLER                      PIC X(155).
           03  FGA010-TRANSACTION-DATA REDEFINES
               FGA010-DATA.
               05  FGA010-TRANS-DESC           PIC X(35).
               05  FGA010-TRANS-AMT            PIC S9(11)V99 COMP-3.
               05  FGA010-FIELD-IND            PIC X(02).
               05  FILLER                      PIC X(316).
