      ***************************************************************
      *                   AC1TMA - ADSA                             *
      *                                                             *
      *          COPY INSERT FOR 010-INITIALIZE SECTION             *
      ***************************************************************

           MOVE +19999 TO AC99W-RECORD-LENGTH-MAX.
           IF EIBCALEN = ZERO
               IF AC99W-MAIN-MENU NOT = SPACES
                   IF AC99W-PROGRAM-NAME NOT = AC99W-MAIN-MENU

                       EXEC CICS
                           XCTL
                           PROGRAM(AC99W-MAIN-MENU)
                       END-EXEC

                     ELSE
                       MOVE AC99W-COMMAREA-LENGTH TO ACCOMM-LENGTH
                   END-IF
                 ELSE
                   MOVE AC99W-COMMAREA-LENGTH TO ACCOMM-LENGTH
               END-IF
             ELSE
               MOVE EIBCALEN TO ACCOMM-LENGTH
               MOVE DFHCOMMAREA TO ACCOMM
               MOVE EIBCALEN TO ACCOMM-LENGTH
           END-IF.

      *:********************************
      *:IF RETURNING FROM A LOWER-LEVEL DIALOG
      *:********************************
           IF ACCOMM-FROM-PROGRAM = AC99W-RETURN
               MOVE AC99W-PROGRAM-NAME TO ACCOMM-CURRENT-PROGRAM

      *:********************************
      *:RESTORE THIS DIALOGS BUFFER AREAS FIRST
      *:********************************
               MOVE ACCOMM-CNT-PGM-STACK TO AC99W-CONTEXT-QUEUE-L-7-8
               PERFORM 302-RESTORE-CONTEXT-L
                  THRU 302-RESTORE-CONTEXT-L-EXIT

      *:********************************
      *:THEN RESTORE THE QUEUES FROM CURRENT + 1 TO FROM - 1
      *:********************************
               ADD +1 TO AC99W-CONTEXT-QUEUE-L-7-8
               SUBTRACT +1 FROM ACCOMM-CNT-PGM-STACK-FROM
                  GIVING AC99W-END-RESTORE
               PERFORM WITH TEST BEFORE
                 UNTIL NOT (AC99W-CONTEXT-QUEUE-L-7-8 NOT GREATER
                        AC99W-END-RESTORE)
                   PERFORM 302-RESTORE-CONTEXT-L
                      THRU 302-RESTORE-CONTEXT-L-EXIT
                   ADD +1 TO AC99W-CONTEXT-QUEUE-L-7-8
               END-PERFORM

      *:********************************
      *:FINALLY RESTORE QUEUES FROM THE HIGHEST LEVEL DOWN
      *:********************************
               MOVE ACCOMM-CNT-PGM-STACK-HIGH
                   TO AC99W-CONTEXT-QUEUE-L-7-8
               PERFORM WITH TEST BEFORE
                 UNTIL NOT (AC99W-CONTEXT-QUEUE-L-7-8 NOT LESS
                        ACCOMM-CNT-PGM-STACK-FROM)
                   PERFORM 302-RESTORE-CONTEXT-L
                      THRU 302-RESTORE-CONTEXT-L-EXIT
                   SUBTRACT +1 FROM AC99W-CONTEXT-QUEUE-L-7-8
               END-PERFORM

      *:********************************
      *:IF AT TOP OF THE STACK - DELETE ALL QUEUES
      *:********************************
               IF ACCOMM-CNT-PGM-STACK = ZERO
                   MOVE ZEROS TO AC99W-CONTEXT-QUEUE-L-7-8
                   PERFORM WITH TEST BEFORE
                     UNTIL NOT (AC99W-CONTEXT-QUEUE-L-7-8 NOT GREATER
                            ACCOMM-CNT-PGM-STACK-FROM)
                       PERFORM 312-DELETE-CONTEXT-L
                          THRU 312-DELETE-CONTEXT-L-EXIT
                       ADD +1 TO AC99W-CONTEXT-QUEUE-L-7-8
                   END-PERFORM
FCMCS              PERFORM VARYING ACCOMM-CNT-PGM-STACK
FCMCS                        FROM ACCOMM-CNT-PGM-STACK-FROM BY -1
FCMCS                       UNTIL ACCOMM-CNT-PGM-STACK = 0
FCMCS                  PERFORM 310-DELETE-CONTEXT-M
FCMCS                     THRU 310-DELETE-CONTEXT-M-EXIT
FCMCS              END-PERFORM
                   MOVE ZEROS TO ACCOMM-CNT-PGM-STACK-HIGH
                 ELSE
                   MOVE ACCOMM-PGM-STACK(ACCOMM-CNT-PGM-STACK)
                       TO   AC99W-HIGHER-DIALOG-NAME
               END-IF
           END-IF.

      *:********************************
      *:IF THIS DIALOG WAS INVOKED OR TRANSFERED TO
      *:********************************
           IF ACCOMM-CNT-PGM-STACK GREATER ZERO
            AND ACCOMM-FROM-PROGRAM NOT = AC99W-RETURN
               IF ACCOMM-RECALL-CODE = AC99W-INVOKE
                OR ACCOMM-RECALL-CODE = AC99W-TRANSFER

      *:********************************
      *:RESTORE ALL HIGHER-LEVEL BUFFERS
      *:********************************
                   SUBTRACT +1 FROM ACCOMM-CNT-PGM-STACK
                      GIVING AC99W-END-RESTORE
                   MOVE ZEROS TO AC99W-CONTEXT-QUEUE-L-7-8
                   PERFORM WITH TEST BEFORE
                     UNTIL NOT (AC99W-CONTEXT-QUEUE-L-7-8 NOT GREATER
                            AC99W-END-RESTORE)
                       PERFORM 302-RESTORE-CONTEXT-L
                          THRU 302-RESTORE-CONTEXT-L-EXIT
                       ADD +1 TO AC99W-CONTEXT-QUEUE-L-7-8
                   END-PERFORM

                   IF ACCOMM-RECALL-CODE = AC99W-TRANSFER
                       MOVE AC99W-YES TO AC99W-FLAG-RESTORE-GBL
                       MOVE ACCOMM-CNT-PGM-STACK
                            TO AC99W-CONTEXT-QUEUE-L-7-8
                       PERFORM 302-RESTORE-CONTEXT-L
                          THRU 302-RESTORE-CONTEXT-L-EXIT
                       MOVE AC99W-NO TO AC99W-FLAG-RESTORE-GBL
                   END-IF
               END-IF
           END-IF.

      *:********************************
      *:SET UP THIS DIALOGS QUEUE NAMES
      *:********************************
           MOVE ACCOMM-CNT-PGM-STACK TO AC99W-CONTEXT-QUEUE-L-7-8.
           IF ACCOMM-CURRENT-PROGRAM NOT = AC99W-PROGRAM-NAME
               MOVE AC99W-PROGRAM-NAME TO ACCOMM-CURRENT-PROGRAM
               MOVE AC99W-YES TO WK01-1ST-FLAG
               PERFORM 312-DELETE-CONTEXT-L
                  THRU 312-DELETE-CONTEXT-L-EXIT
               IF EIBCALEN GREATER ZERO
                   PERFORM 306-RESTORE-CONTEXT-G
                      THRU 306-RESTORE-CONTEXT-G-EXIT
FCBCC              MOVE AGR-NEXT-FUNCTION TO AGR-CURRENT-FUNCTION
FCBCC              MOVE AGR-NEXT-FUNC-DESCRIPTION
FCBCC                TO AGR-FUNC-DESCRIPTION
               END-IF
               MOVE ACCOMM-PGM-STACK(ACCOMM-CNT-PGM-STACK)
                   TO AC99W-HIGHER-DIALOG-NAME

FCBCC *        PERFORM 385-SET-RESPONSE
FCBCC *           THRU 385-SET-RESPONSE-EXIT
FCBCC          PERFORM 385-SET-DEFAULT-RESPONSE
FCBCC             THRU 385-SET-DEFAULT-RESPONSE-EXIT
           ELSE
               MOVE AC99W-NO TO AC99W-FLAG-FIRST-TIME
               MOVE AC99W-NO TO WK01-1ST-FLAG
FCMCS          IF AC99W-FLAG-CONVERSE NOT = AC99W-YES
FCBCC              PERFORM 306-RESTORE-CONTEXT-G
FCBCC                 THRU 306-RESTORE-CONTEXT-G-EXIT
FCMCS          END-IF
FCBCC *        IF ACCOMM-FROM-PROGRAM NOT = AC99W-RETURN
FCBCC *            PERFORM 306-RESTORE-CONTEXT-G
FCBCC *               THRU 306-RESTORE-CONTEXT-G-EXIT
FCBCC          IF ACCOMM-FROM-PROGRAM = AC99W-RETURN
FCBCC *            MOVE ACCOMM-FUNC-STACK(ACCOMM-CNT-PGM-STACK)
FCSLA              MOVE ACCOMM-FUNC-STACK(ACCOMM-CNT-PGM-STACK + 1)
FCBCC                TO AGR-CURRENT-FUNCTION
FCBCC              MOVE ACCOMM-FUNC-DESC-STACK
FCBCC *                            (ACCOMM-CNT-PGM-STACK)
FCSLA                              (ACCOMM-CNT-PGM-STACK + 1)
FCBCC                TO AGR-FUNC-DESCRIPTION
FCBCC              MOVE ACCOMM-LINK-RETURN-STACK
FCBCC                              (ACCOMM-CNT-PGM-STACK + 1)
FCBCC                TO ACCOMM-LINK-RETURN-ID
FCBCC          ELSE
FCMCS              IF AC99W-FLAG-CONVERSE NOT = AC99W-YES
                       PERFORM 302-RESTORE-CONTEXT-L
                          THRU 302-RESTORE-CONTEXT-L-EXIT
                       PERFORM 312-DELETE-CONTEXT-L
                          THRU 312-DELETE-CONTEXT-L-EXIT
FCMCS              END-IF
               END-IF
           END-IF.

FCAMH      IF AGR-PASSED-ONE = AGR-CURRENT-RESPONSE
FCAMH         MOVE SPACES TO AGR-PASSED-ONE
FCAMH      END-IF.

           PERFORM 500-INIT-PROGRAM
              THRU 500-INIT-PROGRAM-EXIT.

