    ***Created by Convert/DC version V8R03 on 11/01/00 at 09:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE DCSD-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY01.
      *%--------------------------------------------------------------%*
           MOVE CNFDL-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0A.
      *%--------------------------------------------------------------%*
           MOVE HOLDS-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0B.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4001-CH16 OF FCHWK16 TO WK01-CODE-4001-CH16.
      *%--------------------------------------------------------------%*
           MOVE FUND-CODE-4001-CH16 OF FCHWK16 TO WK01-CODE-4001-CH1A.
      *%--------------------------------------------------------------%*
           MOVE FUND-TITLE-4005-CH16 OF FCHWM16 TO WK01-TITLE-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4005-CH16 OF FCHWK16 TO WK01-DATE-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-DATE-4001-CH16 OF FCHWM16 TO WK01-DATE-4001-CH16.
      *%--------------------------------------------------------------%*
           MOVE TIME-STAMP-4005-CH16 OF FCHWM16 TO WK01-STAMP-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-CH16 OF FCHWM16 TO WK01-PM-FLAG-CH16.
      *%--------------------------------------------------------------%*
           MOVE FROM-GRANT-DATE-4005-CH16 OF FCHWM16 TO
                WK01-GRANT-DATE-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE TO-GRANT-DATE-4005-CH16 OF FCHWM16 TO
                WK01-GRANT-DATE-4005-CH1A.
      *%--------------------------------------------------------------%*
           MOVE GRANT-CNTRCT-NMBR-4005-CH16 OF FCHWM16 TO
                WK01-CNTRCT-NMBR-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE FROM-BDGT-DATE-4005-CH16 OF FCHWM16 TO
                WK01-BDGT-DATE-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE TO-BDGT-DATE-4005-CH16 OF FCHWM16 TO
                WK01-BDGT-DATE-4005-CH1A.
      *%--------------------------------------------------------------%*
           MOVE INVGR-CODE-4005-CH16 OF FCHWM16 TO WK01-CODE-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE INVGR-NAME-4005-CH16 OF FCHWM16 TO WK01-NAME-4005-CH16.
      *%--------------------------------------------------------------%*
DEVNDP*    MOVE CO-INVGR-CODE-4005-CH16 OF FCHWM16 TO
DEVNDP*         WK01-INVGR-CODE-4005-CH16.
DEVNDP*%--------------------------------------------------------------%*
DEVNDP*    MOVE CO-INVGR-NAME-4005-CH16 OF FCHWM16 TO
DEVNDP*         WK01-INVGR-NAME-4005-CH16.
DEVNDP*%--------------------------------------------------------------%*
           MOVE PRIMARY-ORG-4005-CH16 OF FCHWM16 TO
                WK01-PRI-ORG-4005-CH16.
           MOVE PRI-ORG-DESC-4005-CH16 OF FCHWM16 TO
                WK01-PRIDESC-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE COST-SHARE-CODE-4005-CH16 OF FCHWM16 TO
                WK01-SHARE-CODE-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE COST-SHARE-DESC-4005-CH16 OF FCHWM16 TO
                WK01-SHARE-DESC-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE COST-SHARE-AMT-4005-CH16 OF FCHWM16 TO
                WK01-SHARE-AMT-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-CODE-4005-CH16 OF FCHWM16 TO
                WK01-COST-CODE-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-DESC-4005-CH16 OF FCHWM16 TO
                WK01-COST-DESC-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-CODE-4005-CH16 OF FCHWM16 TO
                WK01-MTHD-CODE-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE PAY-MTHD-DESC-4005-CH16 OF FCHWM16 TO
                WK01-MTHD-DESC-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE AGNCY-LAST-NINE-4005-CH16 OF FCHWM16 TO
                WK01-LAST-NINE-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE AGNCY-NAME-4005-CH16 OF FCHWM16 TO WK01-NAME-4005-CH1A.
      *%--------------------------------------------------------------%*
           MOVE ATHRD-FNDNG-AMT-4005-CH16 OF FCHWM16 TO
                WK01-FNDNG-AMT-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE CUM-AUTH-AMT-4005-CH16 OF FCHWM16 TO
                WK01-AUTH-AMT-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE PMS-CODE-4005-CH16 OF FCHWM16 TO WK01-CODE-4005-CH1A.
      *%--------------------------------------------------------------%*
           MOVE RPRT-CYCLE-4005-CH16 OF FCHWM16 TO WK01-CYCLE-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE RPRT-CYCLE-DESC-4005-CH16 OF FCHWM16 TO
                WK01-CYCLE-DESC-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE BLLNG-FRMT-4005-CH16 OF FCHWM16 TO WK01-FRMT-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE BLLNG-FRMT-DESC-4005-CH16 OF FCHWM16 TO
                WK01-FRMT-DESC-4005-CH16.
      *%--------------------------------------------------------------%*
           MOVE CMPLT-IND-4005-CH16 OF FCHWM16 TO WK01-IND-4005-CH16.
