       976-FROM-R4033-BANK SECTION.

FCYI **        MOVE USER-CODE-4033 OF R4033-BANK
FCYI           MOVE DBLINK-USER-CD
               TO BNK-USER-CD.

               MOVE LAST-ACTVY-DATE-4033 OF R4033-BANK
               TO BNK-LAST-ACTVY-DT.

               MOVE UNVRS-CODE-4033 OF R4033-BANK
               TO BNK-UNVRS-CD.

               MOVE BANK-ACCT-CODE-4033 OF R4033-BANK
               TO BNK-BANK-ACCT-CD.

           IF ACTVY-DATE-4033 OF R4033-BANK  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO BNK-ACTVY-DT
           ELSE
               MOVE ACTVY-DATE-4033 OF R4033-BANK
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO BNK-ACTVY-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO BNK-ACTVY-DT
               END-IF
           END-IF.
       976-FROM-R4033-BANK-EXIT.
           EXIT.
