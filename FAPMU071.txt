    ***Created by Convert/DC version V8R03 on 11/20/00 at 12:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE DCSD-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY01.
      *%--------------------------------------------------------------%*
           MOVE CNFDL-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0A.
      *%--------------------------------------------------------------%*
           MOVE HOLDS-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0B.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-AP07 OF FAPWM07 TO WK01-CODE-AP07.
      *%--------------------------------------------------------------%*
           MOVE NAME-KEY-6311-R-6117-AP07 OF FAPWM07 TO
                WK01-KEY-6311-R-6117-AP07.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-4073-AP07 OF FAPWM07 TO WK01-DATE-4073-AP07.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-DATE-4073-AP07 OF FAPWM07 TO WK01-DATE-4073-AP0A.
      *%--------------------------------------------------------------%*
           MOVE END-DATE-4073-AP07 OF FAPWM07 TO WK01-DATE-4073-AP0B.
      *%--------------------------------------------------------------%*
           MOVE DSCNT-CODE-4073-AP07 OF FAPWM07 TO WK01-CODE-4073-AP07.
      *%--------------------------------------------------------------%*
           MOVE 1099-RPT-ID-4073-AP07 OF FAPWM07 TO
                WK01-RPT-ID-4073-AP07.
      *%--------------------------------------------------------------%*
           MOVE 1099-IND-4150-AP07 OF FAPWM07 TO WK01-IND-4150-AP07.
      *%--------------------------------------------------------------%*
           MOVE 592-IND-AP07 OF FAPWM07 TO WK01-592-IND-AP07.
      *%--------------------------------------------------------------%*
           MOVE 592-STA-AP07 OF FAPWM07 TO WK01-592-STA-AP07.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-CODE-4073-AP07 OF FAPWM07 TO
                WK01-RATE-CODE-4073-AP07.
      *%--------------------------------------------------------------%*
           MOVE INCM-TYPE-CODE-4073-AP07 OF FAPWM07 TO
                WK01-TYPE-CODE-4073-AP07.
      *%--------------------------------------------------------------%*
           MOVE FDRL-WTHHLD-PCT-4073-AP07 OF FAPWM07 TO
                WK01-WTHHLD-PCT-4073-AP07.
      *%--------------------------------------------------------------%*
           MOVE STATE-WTHHLD-PCT-4073-AP07 OF FAPWM07 TO
                WK01-WTHHLD-PCT-4073-AP0A.
      *%--------------------------------------------------------------%*
           MOVE EMP-NBR-AP07           OF FAPWM07 TO
                WK01-EMP-NBR-AP07.
      *%--------------------------------------------------------------%*
           MOVE CNTCT-NAME-4073-AP07 OF FAPWM07 TO WK01-NAME-4073-AP07.
      *%--------------------------------------------------------------%*
           MOVE VNDR-TYPE-DESC-4166-AP07 OF FAPWM07(0001) TO
                WK01-TYPE-DESC-4166-AP071.
      *%--------------------------------------------------------------%*
           MOVE VNDR-TYPE-DESC-4166-AP07 OF FAPWM07(0002) TO
                WK01-TYPE-DESC-4166-AP072.
      *%--------------------------------------------------------------%*
FP4914*    MOVE VNDR-TYPE-DESC-4166-AP07 OF FAPWM07(0003) TO
FP4914*         WK01-TYPE-DESC-4166-AP073.
      *%--------------------------------------------------------------%*
           MOVE VNDR-TYPE-CODE-4166-AP07 OF FAPWM07(0001) TO
                WK01-TYPE-CODE-4166-AP071.
      *%--------------------------------------------------------------%*
           MOVE VNDR-TYPE-CODE-4166-AP07 OF FAPWM07(0002) TO
                WK01-TYPE-CODE-4166-AP072.
      *%--------------------------------------------------------------%*
FP4914*    MOVE VNDR-TYPE-CODE-4166-AP07 OF FAPWM07(0003) TO
FP4914*         WK01-TYPE-CODE-4166-AP073.
      *%--------------------------------------------------------------%*
           MOVE WK-VNDR-ID-LAST-NINE-AP07 OF FAPWK07 TO
                WK01-VNDR-ID-LAST-NN-AP07.
      *%--------------------------------------------------------------%*
           MOVE WK-VNDR-ID-REPL-NINE-AP07 OF FAPWM07 TO
                WK01-VNDR-ID-REPL-NN-AP07.
      *%--------------------------------------------------------------%*
           MOVE WK-REPL-NAME-KEY-AP07 OF FAPWM07 TO
                WK01-REPL-NAME-KEY-AP07.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-AREA-CODE-AP07 OF FAPWM07 TO WK01-AREA-CODE-AP07.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XCHNG-ID-AP07 OF FAPWM07 TO WK01-XCHNG-ID-AP07.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-SEQ-ID-AP07 OF FAPWM07 TO WK01-SEQ-ID-AP07.
      *%--------------------------------------------------------------%*
           MOVE TLPHN-XTNSN-ID-AP07 OF FAPWM07 TO WK01-XTNSN-ID-AP07.
      *%--------------------------------------------------------------%*
           MOVE CHECK-GRPNG-IND-4073-AP07 OF FAPWM07 TO
                WK01-GRPNG-IND-4073-AP07.
      *%--------------------------------------------------------------%*
           MOVE YRLY-TH-AMT-AP07 OF FAPWM07
             TO WK01-YRLY-TH-AMT-AP07.
      *%--------------------------------------------------------------%*
           MOVE TA-CRDT-BLNC-IND-AP07 OF FAPWM07 TO
                WK01-CRDT-BLNC-IND-AP07.
      *%--------------------------------------------------------------%*
           MOVE AP-CRDT-BLNC-IND-AP07 OF FAPWM07 TO
                WK01-CRDT-BLNC-IND-AP0A.
      *%--------------------------------------------------------------%*
           MOVE TAX-RATE-DESC-AP07 OF FAPWM07 TO WK01-RATE-DESC-AP07.
      *%--------------------------------------------------------------%*
           MOVE ACH-ADR-TYPE-AP07 OF FAPWM07 TO WK01-ADR-TYPE-AP07.
      *%--------------------------------------------------------------%*
           MOVE WRITE-OFF-DATE-AP07 OF FAPWM07 TO WK01-OFF-DATE-AP07.
      *%--------------------------------------------------------------%*
           MOVE DEFAULT-ADR-TYPE-AP07 OF FAPWM07 TO WK01-ADR-TYPE-AP0A.
      *%--------------------------------------------------------------%*
           MOVE WRITE-OFF-IND-AP07 OF FAPWM07 TO WK01-OFF-IND-AP07.
      *%--------------------------------------------------------------%*
            MOVE UPDT-USER-ID-AP07          TO WK01-UPDT-USER-ID-AP07
      *%--------------------------------------------------------------%*
            MOVE VDR-PAYEE-TYPE-AP07        TO WK01-VDR-PAYEE-TYPE
      *%--------------------------------------------------------------%*
            MOVE VDR-PAYEE-TYPE-DESC-AP07   TO
                                WK01-VDR-PAYEE-TYPE-DESC
      *%--------------------------------------------------------------%*
            MOVE DUNS-NBR-AP07                TO WK01-DUNS-NBR
      *%--------------------------------------------------------------%*
            MOVE ENCUM-IND-AP07               TO WK01-ENCUM-IND
      *%--------------------------------------------------------------%*
002MLJ      MOVE PYMT-METHOD-TYP-AP07         TO WK01-PYMT-METHOD-TYP
      *%--------------------------------------------------------------%*
            MOVE SCIQ-VDR-ID-AP07             TO WK01-SCIQ-VDR-ID
      *%--------------------------------------------------------------%*
            MOVE SQVDR-STATUS-AP07            TO WK01-SQVDR-STATUS
      *%--------------------------------------------------------------%*
            MOVE DEFAULT-ACCT-CD-AP07         TO WK01-DEFAULT-ACCT-CD
      *%--------------------------------------------------------------%*
            MOVE ETH-DESC-AP07           TO WK01-ETH-DESC
      *%--------------------------------------------------------------%*
            MOVE ETH-CD-AP07             TO WK01-ETH-CD
      *%--------------------------------------------------------------%*
            MOVE GENDER-AP07             TO WK01-GENDER
      *%--------------------------------------------------------------%*
            MOVE GENDER-DESC-AP07        TO WK01-GENDER-DESC.
      *%--------------------------------------------------------------%*
            MOVE DSCN-CODE-DESC-AP07     TO WK01-DSCN-CODE-DESC.
      *%--------------------------------------------------------------%*
FP4914      MOVE VNDR-URL-CODE-AP07      TO WK01-VNDR-URL-CODE-AP07.
      *%--------------------------------------------------------------%*
