       01  R6349-ENTY-CD.
           02  DB-PROC-ID-6349.
               03  USER-CODE-6349                       PIC X(8).
               03  LAST-ACTVY-DATE-6349                 PIC X(5).
               03  TRMNL-ID-6349                        PIC X(8).
               03  PURGE-FLAG-6349                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  ENTY-KEY-6349.
               03  UNVRS-CODE-6349                      PIC X(2).
               03  ENTY-TYPE-CODE-6349                  PIC X(4).
               03  ENTY-CODE-6349                       PIC X(4).
           02  SHORT-DESC-6349                          PIC X(10).
           02  LONG-DESC-6349                           PIC X(30).
