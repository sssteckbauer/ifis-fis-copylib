      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWD32                              04/24/00  12:56  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWD32.
           02  SYSTEM-DATE-AP32               COMP-3    PIC S9(8).
           02  CHK-DTL-KEY-AP32.
               03  WK-DCMNT-NMBR-AP32                   PIC X(8).
               03  WK-ITEM-NMBR-AP32          COMP-3    PIC 9(4).
               03  WK-SEQ-NMBR-AP32                     PIC 9(4).
           02  WK-TOTAL-AMT-AP32              COMP-3    PIC S9(10)V99.
           02  ACTG-PRD-NUM-AP32                        PIC 9(2).
