    ***Created by Convert/DC version V8R03 on 12/11/00 at 11:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY01-Z = 'Y'
               MOVE WK01-TEXT-SY01 TO DCSD-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0A-Z = 'Y'
               MOVE WK01-TEXT-SY0A TO CNFDL-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0B-Z = 'Y'
               MOVE WK01-TEXT-SY0B TO HOLDS-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH10-Z = 'Y'
               MOVE WK01-CODE-CH10 TO ACTN-CODE-CH10 OF FCHWM10
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4000-CH10-Z = 'Y'
               MOVE WK01-CODE-4000-CH10 TO COA-CODE-4000-CH10 OF FCHWK10
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4004-CH10-Z = 'Y'
               MOVE WK01-CODE-4004-CH10 TO PRGRM-CODE-4004-CH10 OF
                FCHWK10
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4105-CH10-Z = 'Y'
               MOVE WK01-TITLE-4105-CH10 TO PRGRM-TITLE-4105-CH10 OF
                FCHWM10
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4105-CH10-Z = 'Y'
               MOVE WK01-DATE-4105-CH10 TO START-DATE-4105-CH10 OF
                FCHWK10
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CHNG-DATE-CH10-Z = 'Y'
               MOVE WK01-CHNG-DATE-CH10 TO NEXT-CHNG-DATE-CH10 OF
                FCHWM10
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4105-CH10-Z = 'Y'
               MOVE WK01-DESC-4105-CH10 TO STATUS-DESC-4105-CH10 OF
                FCHWM10
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4105-CH1A-Z = 'Y'
               MOVE WK01-DATE-4105-CH1A TO END-DATE-4105-CH10 OF FCHWM10
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4004-CH10-Z = 'Y'
               MOVE WK01-DATE-4004-CH10 TO ACTVY-DATE-4004-CH10 OF
                FCHWM10
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STAMP-4105-CH10-Z = 'Y'
               MOVE WK01-STAMP-4105-CH10 TO TIME-STAMP-4105-CH10 OF
                FCHWM10
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-CH10-Z = 'Y'
               MOVE WK01-PM-FLAG-CH10 TO AM-PM-FLAG-CH10 OF FCHWM10
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4004-CH1A-Z = 'Y'
               MOVE WK01-CODE-4004-CH1A TO PREDCSR-CODE-4004-CH10 OF
                FCHWM10
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PRGRM-TITL-4105-CH10-Z = 'Y'
               MOVE WK01-PRGRM-TITL-4105-CH10 TO
                PREDCSR-PRGRM-TITLE-4105-CH10 OF FCHWM10
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ENTRY-IND-4105-CH10-Z = 'Y'
               MOVE WK01-ENTRY-IND-4105-CH10 TO
                DATA-ENTRY-IND-4105-CH10 OF FCHWM10
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CH101-Z = 'Y'
               MOVE WK01-CH101 TO LABEL-CH10 OF FCHWM10(0001)
           END-IF.
           MOVE WK01-CH101-F TO WK01-CH10-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CH102-Z = 'Y'
               MOVE WK01-CH102 TO LABEL-CH10 OF FCHWM10(0002)
           END-IF.
           MOVE WK01-CH102-F TO WK01-CH10-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CH103-Z = 'Y'
               MOVE WK01-CH103 TO LABEL-CH10 OF FCHWM10(0003)
           END-IF.
           MOVE WK01-CH103-F TO WK01-CH10-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CH104-Z = 'Y'
               MOVE WK01-CH104 TO LABEL-CH10 OF FCHWM10(0004)
           END-IF.
           MOVE WK01-CH104-F TO WK01-CH10-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CH105-Z = 'Y'
               MOVE WK01-CH105 TO LABEL-CH10 OF FCHWM10(0005)
           END-IF.
           MOVE WK01-CH105-F TO WK01-CH10-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-PRGRM-CODE-4004-CH101-Z = 'Y'
               MOVE WK01-PRGRM-CODE-4004-CH101 TO
                HIER-PRGRM-CODE-4004-CH10 OF FCHWM10(0001)
           END-IF.
           MOVE WK01-PRGRM-CODE-4004-CH101-F TO
                WK01-PRGRM-CODE-4004-CH10-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-PRGRM-CODE-4004-CH102-Z = 'Y'
               MOVE WK01-PRGRM-CODE-4004-CH102 TO
                HIER-PRGRM-CODE-4004-CH10 OF FCHWM10(0002)
           END-IF.
           MOVE WK01-PRGRM-CODE-4004-CH102-F TO
                WK01-PRGRM-CODE-4004-CH10-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-PRGRM-CODE-4004-CH103-Z = 'Y'
               MOVE WK01-PRGRM-CODE-4004-CH103 TO
                HIER-PRGRM-CODE-4004-CH10 OF FCHWM10(0003)
           END-IF.
           MOVE WK01-PRGRM-CODE-4004-CH103-F TO
                WK01-PRGRM-CODE-4004-CH10-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-PRGRM-CODE-4004-CH104-Z = 'Y'
               MOVE WK01-PRGRM-CODE-4004-CH104 TO
                HIER-PRGRM-CODE-4004-CH10 OF FCHWM10(0004)
           END-IF.
           MOVE WK01-PRGRM-CODE-4004-CH104-F TO
                WK01-PRGRM-CODE-4004-CH10-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-PRGRM-CODE-4004-CH105-Z = 'Y'
               MOVE WK01-PRGRM-CODE-4004-CH105 TO
                HIER-PRGRM-CODE-4004-CH10 OF FCHWM10(0005)
           END-IF.
           MOVE WK01-PRGRM-CODE-4004-CH105-F TO
                WK01-PRGRM-CODE-4004-CH10-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-PRGRM-TITL-4105-CH1A11-Z = 'Y'
               MOVE WK01-PRGRM-TITL-4105-CH1A11 TO
                HIER-PRGRM-TITLE-4105-CH10 OF FCHWM10(0001)
           END-IF.
           MOVE WK01-PRGRM-TITL-4105-CH1A11-F TO
                WK01-PRGRM-TITL-4105-CH1A1-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-PRGRM-TITL-4105-CH1A12-Z = 'Y'
               MOVE WK01-PRGRM-TITL-4105-CH1A12 TO
                HIER-PRGRM-TITLE-4105-CH10 OF FCHWM10(0002)
           END-IF.
           MOVE WK01-PRGRM-TITL-4105-CH1A12-F TO
                WK01-PRGRM-TITL-4105-CH1A1-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-PRGRM-TITL-4105-CH1A13-Z = 'Y'
               MOVE WK01-PRGRM-TITL-4105-CH1A13 TO
                HIER-PRGRM-TITLE-4105-CH10 OF FCHWM10(0003)
           END-IF.
           MOVE WK01-PRGRM-TITL-4105-CH1A13-F TO
                WK01-PRGRM-TITL-4105-CH1A1-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-PRGRM-TITL-4105-CH1A14-Z = 'Y'
               MOVE WK01-PRGRM-TITL-4105-CH1A14 TO
                HIER-PRGRM-TITLE-4105-CH10 OF FCHWM10(0004)
           END-IF.
           MOVE WK01-PRGRM-TITL-4105-CH1A14-F TO
                WK01-PRGRM-TITL-4105-CH1A1-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-PRGRM-TITL-4105-CH1A15-Z = 'Y'
               MOVE WK01-PRGRM-TITL-4105-CH1A15 TO
                HIER-PRGRM-TITLE-4105-CH10 OF FCHWM10(0005)
           END-IF.
           MOVE WK01-PRGRM-TITL-4105-CH1A15-F TO
                WK01-PRGRM-TITL-4105-CH1A1-F (5).
