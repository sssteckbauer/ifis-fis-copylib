       01  R4060-RPT.
           02  DB-PROC-ID-4060.
               03  USER-CODE-4060                       PIC X(8).
               03  LAST-ACTVY-DATE-4060                 PIC X(5).
               03  TRMNL-ID-4060                        PIC X(8).
               03  PURGE-FLAG-4060                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  RPRT-KEY-4060.
               03  UNVRS-CODE-4060                      PIC X(2).
               03  RPRT-CODE-4060                       PIC X(8).
           02  ACTVY-DATE-4060                COMP-3    PIC S9(8).
           02  RPRT-TITLE-4060                          PIC X(60).
           02  NMBR-OF-COPIES-4060                      PIC 9(3).
