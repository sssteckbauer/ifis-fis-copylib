      *%---------------------------------------------------------------*
      *%                                                               *
      *%                   200-FSYSC07 ROUTINE                         *
      *%                                                               *
      *%---------------------------------------------------------------*
       200-FSYSC07.

      ******************************************************************
      ******
      ***  CONTROL PF7 AND PF8 FOR SCRATCH PAGING
      ******************************************************************
      ******
           IF AGR-PF7
               MOVE 'B' TO PAGE-FLAG-SY00
               MOVE AC99W-YES-DISPLAY TO AC99W-FLAG-DISPLAY
               MOVE AC99W-YES-DISPLAY TO AC99W-FLAG-DISPLAY-CONTINUE
               MOVE AC99W-NO TO AC99W-FLAG-FIRST-TIME
               GO TO 200-FSYSC07-EXIT
           END-IF.
           IF AGR-PF8
               MOVE 'F' TO PAGE-FLAG-SY00
               MOVE AC99W-YES-DISPLAY TO AC99W-FLAG-DISPLAY
               MOVE AC99W-YES-DISPLAY TO AC99W-FLAG-DISPLAY-CONTINUE
               MOVE AC99W-NO TO AC99W-FLAG-FIRST-TIME
               GO TO 200-FSYSC07-EXIT
           END-IF.

       200-FSYSC07-EXIT.
           EXIT.
