      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FTAWD20A                             04/24/00  13:19  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FTAWD20A.
           02  SAVE-DBKEY-ID-4404-PLUS-ON-T20
                                                        PIC  X(8).
           02  SAVE-DBKEY-ID-4405-PLUS-ON-T20
                                                        PIC  X(8).
           02  SAVE-DBKEY-ID-4411-PLUS-ON-T20
                                                        PIC  X(11).
           02  SAVE-DBKEY-ID-4404-MNS-ONE-T20
                                                        PIC  X(8).
           02  SAVE-DBKEY-ID-4405-MNS-ONE-T20
                                                        PIC  X(8).
           02  SAVE-DBKEY-ID-4411-MNS-ONE-T20
                                                        PIC  X(11).
