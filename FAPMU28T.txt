       01  MAP-CONTROL.

    ***Created by Convert/DC version V8R03 on 12/05/00 at 08:00***

           02  FAPMU28-MAP-CONTROL.
           03  FAPMU28T.
           04  WK01-DCXMAPL-PARMS.
               10  WK01-MAPNAME                  PIC X(8)
                                                 VALUE 'FAPMU28I'.
               10  WK01-IKE-LENGTH               PIC S9(8)  COMP
                                                 VALUE 12601.
               10  WK01-TINA-LENGTH              PIC S9(4)  COMP
                                                 VALUE 943.
           05  WK01-ATTRIBUTES                   PIC X(60)
                                                 VALUE SPACE.
           05  WK01-ATTRIBUTES-ERROR             PIC X(60) VALUE
               'U  BIF'.
           05  WK01-CURSOR-DFLD                  PIC X(36)
                                                 VALUE SPACES.
           05  WK01-CURSOR-DFLD-QUAL             PIC X(30)
                                                 VALUE SPACES.
           05  WK01-ROW                          PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-COLUMN                       PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-ERROR-MSG                    PIC X(80)
                                                 VALUE SPACES.
           05  WK01-MAP-DATA-LEN                 PIC S9(6)
                                                 VALUE ZERO.
           05  WK01-1ST-FLAG                     PIC X(1)
                                                 VALUE 'Y'.
           05  WK01-FLAG-OUTPUT-DATA             PIC X(1)
                                                 VALUE 'Y'.
      *    05  WK01-STATUS-RETURN-FLAG           PIC X(1)
      *                                          VALUE SPACES.
           05  WK01-ALL-BUT-FLAG                 PIC X(1)
                                                 VALUE SPACES.
           05  WK01-FLAG-ALL-FIELDS-CHANGED      PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-CHANGED      PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERROR        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERROR        PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.

           05  WK01-VARDATA                      PIC X(11880)
                                                 VALUE SPACES.
           05  FILLER                            PIC X(100)
                                                 VALUE SPACES.
           04  WK01-ELEMENT-AREA.

               10  WK01-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-6001-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00       PIC X(35)
                                               VALUE SPACES.
               10  WK01-DATE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-HH-MM-TIME-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00       PIC X(4)
                                               VALUE SPACES.
               10  WK01-PM-FLAG-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-USER-ID-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID       PIC X(32)
                                               VALUE SPACES.
               10  WK01-RSPNS-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-FUNC-DESCRIPTION-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION       PIC X(28)
                                               VALUE SPACES.
               10  WK01-STATUS-CODE-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-TEXT-SY01-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY01-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY01-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY01-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY01-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY01       PIC X(8)
                                               VALUE SPACES.
               10  WK01-TEXT-SY0A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY0A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY0A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY0A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY0A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY0A       PIC X(11)
                                               VALUE SPACES.
               10  WK01-TEXT-SY0B-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY0B-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY0B-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY0B-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY0B-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY0B       PIC X(5)
                                               VALUE SPACES.
               10  WK01-CODE-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-AP28       PIC X(1)
                                               VALUE SPACES.
               10  WK01-ORGN-CODE-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ORGN-CODE-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ORGN-CODE-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ORGN-CODE-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ORGN-CODE-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ORGN-CODE-AP28       PIC X(4)
                                               VALUE SPACES.
               10  WK01-CODE-DESC-4012-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-DESC-4012-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-DESC-4012-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-DESC-4012-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-DESC-4012-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-DESC-4012-AP28       PIC X(35)
                                               VALUE SPACES.
               10  WK01-DCMNT-NMBR-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DCMNT-NMBR-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DCMNT-NMBR-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DCMNT-NMBR-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DCMNT-NMBR-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DCMNT-NMBR-AP28       PIC X(8)
                                               VALUE SPACES.
               10  WK01-BANK-ACCT-CODE-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BANK-ACCT-CODE-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BANK-ACCT-CODE-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BANK-ACCT-CODE-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BANK-ACCT-CODE-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-BANK-ACCT-CODE-AP28       PIC X(2)
                                               VALUE SPACES.
               10  WK01-CODE-DESC-4062-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-DESC-4062-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-DESC-4062-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-DESC-4062-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-DESC-4062-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-DESC-4062-AP28       PIC X(35)
                                               VALUE SPACES.
               10  WK01-CODE-4073-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4073-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4073-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4073-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4073-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4073-AP28       PIC X(10)
                                               VALUE SPACES.
               10  WK01-NAME-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-AP28       PIC X(35)
                                               VALUE SPACES.
               10  WK01-AMT-4159-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-4159-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-4159-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-4159-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-4159-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-4159-AP28       PIC X(16)
                                               VALUE SPACES.
               10  WK01-AMT-4159-AP2A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-4159-AP2A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-4159-AP2A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-4159-AP2A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-4159-AP2A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-4159-AP2A       PIC X(16)
                                               VALUE SPACES.
               10  WK01-AMT-4159-AP2B-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-4159-AP2B-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-4159-AP2B-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-4159-AP2B-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-4159-AP2B-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-4159-AP2B       PIC X(16)
                                               VALUE SPACES.
               10  WK01-CHRG-4159-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CHRG-4159-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CHRG-4159-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CHRG-4159-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CHRG-4159-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CHRG-4159-AP28       PIC X(16)
                                               VALUE SPACES.
               10  WK01-TRD-IN-AMT-AP28-Z             PIC X(1)
                                               VALUE SPACE.
               10  WK01-TRD-IN-AMT-AP28-F             PIC X(1)
                                               VALUE SPACE.
               10  WK01-TRD-IN-AMT-AP28-A             PIC X(1)
                                               VALUE SPACE.
               10  WK01-TRD-IN-AMT-AP28-V             PIC X(1)
                                               VALUE SPACE.
               10  WK01-TRD-IN-AMT-AP28-O             PIC X(1)
                                               VALUE SPACE.
               10  WK01-TRD-IN-AMT-AP28      PIC X(16)
                                               VALUE SPACES.
               10  WK01-WTHHLD-AMT-4159-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-WTHHLD-AMT-4159-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-WTHHLD-AMT-4159-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-WTHHLD-AMT-4159-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-WTHHLD-AMT-4159-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-WTHHLD-AMT-4159-AP28       PIC X(10)
                                               VALUE SPACES.
               10  WK01-WTHHLD-PCT-4159-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-WTHHLD-PCT-4159-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-WTHHLD-PCT-4159-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-WTHHLD-PCT-4159-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-WTHHLD-PCT-4159-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-WTHHLD-PCT-4159-AP28       PIC X(7)
                                               VALUE SPACES.
               10  WK01-LITERAL-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LITERAL-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LITERAL-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LITERAL-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LITERAL-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LITERAL-AP28       PIC X(1)
                                               VALUE SPACES.
               10  WK01-WTHHLD-AMT-4159-AP2A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-WTHHLD-AMT-4159-AP2A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-WTHHLD-AMT-4159-AP2A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-WTHHLD-AMT-4159-AP2A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-WTHHLD-AMT-4159-AP2A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-WTHHLD-AMT-4159-AP2A       PIC X(10)
                                               VALUE SPACES.
               10  WK01-WTHHLD-PCT-4159-AP2A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-WTHHLD-PCT-4159-AP2A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-WTHHLD-PCT-4159-AP2A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-WTHHLD-PCT-4159-AP2A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-WTHHLD-PCT-4159-AP2A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-WTHHLD-PCT-4159-AP2A       PIC X(7)
                                               VALUE SPACES.
               10  WK01-LITERAL-AP2A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LITERAL-AP2A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LITERAL-AP2A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LITERAL-AP2A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LITERAL-AP2A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LITERAL-AP2A       PIC X(1)
                                               VALUE SPACES.
               10  WK01-AMT-4158-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-4158-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-4158-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-4158-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-4158-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AMT-4158-AP28       PIC X(17)
                                               VALUE SPACES.
               10  WK01-NMBR-4158-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-4158-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-4158-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-4158-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-4158-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-4158-AP28       PIC X(8)
                                               VALUE SPACES.
               10  WK01-DATE-4158-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4158-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4158-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4158-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4158-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4158-AP28       PIC S9(8)
                                               VALUE ZEROS.
               10  WK01-TYPE-CODE-4158-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TYPE-CODE-4158-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TYPE-CODE-4158-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TYPE-CODE-4158-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TYPE-CODE-4158-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TYPE-CODE-4158-AP28       PIC X(1)
                                               VALUE SPACES.
               10  WK01-START-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-START-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-START-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-START-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-START-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-START-AP28       PIC X(8)
                                               VALUE SPACES.
               10  WK01-END-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-END-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-END-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-END-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-END-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-END-AP28       PIC X(8)
                                               VALUE SPACES.
               10  WK01-TEST-PATRN-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEST-PATRN-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEST-PATRN-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEST-PATRN-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEST-PATRN-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEST-PATRN-AP28       PIC X(1)
                                               VALUE SPACES.
               10  WK01-CHECK-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CHECK-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CHECK-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CHECK-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CHECK-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CHECK-AP28       PIC X(1)
                                               VALUE SPACES.
               10  WK01-LIT-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LIT-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LIT-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LIT-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LIT-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LIT-AP28       PIC X(8)
                                               VALUE SPACES.
               10  WK01-CONFIRM-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CONFIRM-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CONFIRM-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CONFIRM-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CONFIRM-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CONFIRM-AP28       PIC X(1)
                                               VALUE SPACES.
               10  WK01-DEST-CODE-4167-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DEST-CODE-4167-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DEST-CODE-4167-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DEST-CODE-4167-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DEST-CODE-4167-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DEST-CODE-4167-AP28       PIC X(8)
                                               VALUE SPACES.
               10  WK01-LCTN-DESC-4167-AP28-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LCTN-DESC-4167-AP28-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LCTN-DESC-4167-AP28-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LCTN-DESC-4167-AP28-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LCTN-DESC-4167-AP28-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LCTN-DESC-4167-AP28       PIC X(35)
                                               VALUE SPACES.
               10  WK01-ZZMESSAGE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE       PIC X(160)
                                               VALUE SPACES.
               10  WK01-PASSED-ONE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE       PIC X(32)
                                               VALUE SPACES.
               10  WK01-MAP-RESPONSE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-TEXT-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00       PIC X(14)
                                               VALUE SPACES.
