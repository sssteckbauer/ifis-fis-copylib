      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FEXWM10                              04/24/00  12:30  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FEXWM10.
           02  VNDR-CODE-EX10 OCCURS 10.
               03  VNDR-ID-DIGIT-ONE-EX10               PIC X(1).
               03  VNDR-ID-LAST-NINE-EX10               PIC X(9).
           02  VNDR-NAME-EX10               OCCURS 10   PIC X(35).
           02  ACTN-CODE-GROUP-EX10.
               03  ACTN-CODE-EX10           OCCURS 10   PIC X(1).
           02  SAVE-DBKEY-ID-EX10           OCCURS 10   PIC X(10).
