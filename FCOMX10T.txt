       01  MAP-CONTROL.

    ***Created by Convert/DC version V8R03 on 12/13/00 at 16:00***

           02  FCOMX10-MAP-CONTROL.
           03  FCOMX10T.
           04  WK01-DCXMAPL-PARMS.
               10  WK01-MAPNAME                  PIC X(8)
                                                 VALUE 'FCOMX10I'.
               10  WK01-IKE-LENGTH               PIC S9(8)  COMP
                                                 VALUE 8393.
               10  WK01-TINA-LENGTH              PIC S9(4)  COMP
                                                 VALUE 1563.
           05  WK01-ATTRIBUTES                   PIC X(60)
                                                 VALUE SPACE.
           05  WK01-ATTRIBUTES-ERROR             PIC X(60) VALUE
               'U  BIF'.
           05  WK01-CURSOR-DFLD                  PIC X(36)
                                                 VALUE SPACES.
           05  WK01-CURSOR-DFLD-QUAL             PIC X(30)
                                                 VALUE SPACES.
           05  WK01-ROW                          PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-COLUMN                       PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-ERROR-MSG                    PIC X(80)
                                                 VALUE SPACES.
           05  WK01-MAP-DATA-LEN                 PIC S9(6)
                                                 VALUE ZERO.
           05  WK01-1ST-FLAG                     PIC X(1)
                                                 VALUE 'Y'.
           05  WK01-FLAG-OUTPUT-DATA             PIC X(1)
                                                 VALUE 'Y'.
      *    05  WK01-STATUS-RETURN-FLAG           PIC X(1)
      *                                          VALUE SPACES.
           05  WK01-ALL-BUT-FLAG                 PIC X(1)
                                                 VALUE SPACES.
           05  WK01-FLAG-ALL-FIELDS-CHANGED      PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-CHANGED      PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERROR        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERROR        PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.

           05  WK01-VARDATA                      PIC X(11880)
                                                 VALUE SPACES.
           05  FILLER                            PIC X(100)
                                                 VALUE SPACES.
           04  WK01-ELEMENT-AREA.

               10  WK01-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-6001-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00       PIC X(35)
                                               VALUE SPACES.
               10  WK01-DATE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-HH-MM-TIME-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00       PIC X(4)
                                               VALUE SPACES.
               10  WK01-PM-FLAG-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-USER-ID-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID       PIC X(32)
                                               VALUE SPACES.
               10  WK01-RSPNS-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-FUNC-DESCRIPTION-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION       PIC X(28)
                                               VALUE SPACES.
               10  WK01-STATUS-CODE-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-CODE-KEY-CO10-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-KEY-CO10-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-KEY-CO10-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-KEY-CO10-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-KEY-CO10-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-KEY-CO10       PIC X(4)
                                               VALUE SPACES.
               10  WK01-DESC-6268-CO10-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-6268-CO10-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-6268-CO10-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-6268-CO10-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-6268-CO10-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-6268-CO10       PIC X(35)
                                               VALUE SPACES.
               10  WK01-CODE-6268-CO10-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6268-CO10-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6268-CO10-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6268-CO10-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6268-CO10-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6268-CO10       PIC X(4)
                                               VALUE SPACES.
               10  WK01-NAME-2155-CO10-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-2155-CO10-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-2155-CO10-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-2155-CO10-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-2155-CO10-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-2155-CO10       PIC X(35)
                                               VALUE SPACES.
               10  WK01-VIEW-6268-CO10-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-VIEW-6268-CO10-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-VIEW-6268-CO10-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-VIEW-6268-CO10-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-VIEW-6268-CO10-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-VIEW-6268-CO10       PIC X(8)
                                               VALUE SPACES.
               10  WK01-DESC-6257-CO10-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-6257-CO10-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-6257-CO10-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-6257-CO10-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-6257-CO10-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-6257-CO10       PIC X(35)
                                               VALUE SPACES.
               10  WK01-TEXT-6269-CO101-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO101-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO101-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO101-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO101-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO101       PIC X(79)
                                               VALUE SPACES.
               10  WK01-TEXT-6269-CO102-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO102-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO102-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO102-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO102-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO102       PIC X(79)
                                               VALUE SPACES.
               10  WK01-TEXT-6269-CO103-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO103-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO103-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO103-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO103-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO103       PIC X(79)
                                               VALUE SPACES.
               10  WK01-TEXT-6269-CO104-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO104-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO104-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO104-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO104-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO104       PIC X(79)
                                               VALUE SPACES.
               10  WK01-TEXT-6269-CO105-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO105-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO105-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO105-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO105-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO105       PIC X(79)
                                               VALUE SPACES.
               10  WK01-TEXT-6269-CO106-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO106-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO106-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO106-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO106-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO106       PIC X(79)
                                               VALUE SPACES.
               10  WK01-TEXT-6269-CO107-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO107-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO107-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO107-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO107-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO107       PIC X(79)
                                               VALUE SPACES.
               10  WK01-TEXT-6269-CO108-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO108-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO108-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO108-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO108-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO108       PIC X(79)
                                               VALUE SPACES.
               10  WK01-TEXT-6269-CO109-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO109-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO109-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO109-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO109-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO109       PIC X(79)
                                               VALUE SPACES.
               10  WK01-TEXT-6269-CO1010-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO1010-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO1010-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO1010-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO1010-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO1010       PIC X(79)
                                               VALUE SPACES.
               10  WK01-TEXT-6269-CO1011-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO1011-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO1011-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO1011-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO1011-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO1011       PIC X(79)
                                               VALUE SPACES.
               10  WK01-TEXT-6269-CO1012-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO1012-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO1012-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO1012-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO1012-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-6269-CO1012       PIC X(79)
                                               VALUE SPACES.
               10  WK01-ZZMESSAGE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE       PIC X(160)
                                               VALUE SPACES.
               10  WK01-PASSED-ONE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE       PIC X(32)
                                               VALUE SPACES.
               10  WK01-MAP-RESPONSE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-TEXT-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00       PIC X(14)
                                               VALUE SPACES.
           04  WK01-SUBSCRIPTED-WORK-AREA.
               05  FILLER                      PIC X(1).
               05  WK01-TEXT-6269-CO10-F             PIC X(1)
                                               OCCURS 12.
