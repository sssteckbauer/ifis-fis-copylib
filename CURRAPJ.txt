      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4204-APRVL-J' TO DBLINK-CURR-PARAGRAPH.      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4204-APRVL-J' TO RECORD-NAME.                         276 BRTN
YYY991     MOVE 'F-APPROVAL' TO AREA-NAME.                              276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4204-APRVL-J TO DBLINK-RECORD-MADE-CURRENT.     276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4204-APRVL-J-EXIT                        276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE APJ-FK-DOC-SEQ-NBR TO DBLINK-R4204-APRVL-J-01       276 BRTN
YYY991         MOVE APJ-USER-ID TO DBLINK-R4204-APRVL-J-02              276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4204-APRVL-J TO DBLINK-VIEW-NAME-CURRENT.       276 BRTN
YYY991     MOVE DBLINK-R4204-APRVL-J-KEY TO DBLINK-VIEW-200-CURRENT.    276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4204-4215                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4204-APRVL-J-01 TO DBLINK-S-4204-4215-01.       276 BRTN
YYY991     MOVE DBLINK-R4204-APRVL-J-02 TO DBLINK-S-4204-4215-02.       276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4204-4215-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4192-4204                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE APJ-FK-UPR-USER-ID TO DBLINK-S-4192-4204-01.            276 BRTN
YYY991     MOVE APJ-UPR-SET-TS TO DBLINK-S-4192-4204-02.                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4203-4204                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE APJ-FK-DOC-SEQ-NBR TO DBLINK-S-4203-4204-01.            276 BRTN
YYY991     MOVE APJ-USER-ID TO DBLINK-S-4203-4204-02.                   276 BRTN
