    ***Created by Convert/DC version V8R03 on 01/04/01 at 09:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT02-Z = 'Y'
               MOVE WK01-NAME-UT02 TO TEMPL-OWNER-NAME-UT29
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-UT02-Z = 'Y'
               MOVE WK01-ID-UT02 TO TEMPL-OWNER-USER-ID-UT29
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-UT02-Z = 'Y'
               MOVE WK01-TYPE-UT02 TO DCMNT-TYPE-UT29
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-UT02-Z = 'Y'
               MOVE WK01-DESC-UT02 TO DCMNT-DESC-UT29
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TMPLT-CODE-UT02-Z = 'Y'
               MOVE WK01-TMPLT-CODE-UT02 TO APRVL-TMPLT-CODE-UT29
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TMPLT-DESC-UT02-Z = 'Y'
               MOVE WK01-TMPLT-DESC-UT02 TO APRVL-TMPLT-DESC-UT29
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-UT021-Z = 'Y'
               MOVE WK01-CODE-UT021 TO ACTN-CODE-UT29
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-UT021-Z = 'Y'
               MOVE WK01-NMBR-UT021 TO SEQ-NMBR-UT29
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-UT0A11-Z = 'Y'
               MOVE WK01-ID-UT0A11 TO PRIM-LEVEL-USER-ID
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT0A11-Z = 'Y'
               MOVE WK01-NAME-UT0A11 TO PRIM-LEVEL-FULL-NAME-UT29
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-LIMIT-UT29-Z = 'Y'
               MOVE WK01-LIMIT-UT29 TO APRVL-LIMIT-UT29
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-APRVL-CODE-UT021-Z = 'Y'
               MOVE WK01-APRVL-CODE-UT021 TO ALTRNT-APRVL-CODE-UT29 (1)
           END-IF.
           MOVE WK01-APRVL-CODE-UT021-F TO WK01-APRVL-CODE-UT02-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-APRVL-CODE-UT022-Z = 'Y'
               MOVE WK01-APRVL-CODE-UT022 TO ALTRNT-APRVL-CODE-UT29 (2)

           END-IF.
           MOVE WK01-APRVL-CODE-UT022-F TO WK01-APRVL-CODE-UT02-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-APRVL-CODE-UT023-Z = 'Y'
               MOVE WK01-APRVL-CODE-UT023 TO ALTRNT-APRVL-CODE-UT29 (3)
           END-IF.
           MOVE WK01-APRVL-CODE-UT023-F TO WK01-APRVL-CODE-UT02-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-APRVL-CODE-UT024-Z = 'Y'
               MOVE WK01-APRVL-CODE-UT024 TO ALTRNT-APRVL-CODE-UT29 (4)
           END-IF.
           MOVE WK01-APRVL-CODE-UT024-F TO WK01-APRVL-CODE-UT02-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-APRVL-CODE-UT025-Z = 'Y'
               MOVE WK01-APRVL-CODE-UT025 TO ALTRNT-APRVL-CODE-UT29 (5)
           END-IF.
           MOVE WK01-APRVL-CODE-UT025-F TO WK01-APRVL-CODE-UT02-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-APRVL-CODE-UT026-Z = 'Y'
               MOVE WK01-APRVL-CODE-UT026 TO ALTRNT-APRVL-CODE-UT29 (6)
           END-IF.
           MOVE WK01-APRVL-CODE-UT026-F TO WK01-APRVL-CODE-UT02-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-APRVL-CODE-UT027-Z = 'Y'
               MOVE WK01-APRVL-CODE-UT027 TO ALTRNT-APRVL-CODE-UT29 (7)
           END-IF.
           MOVE WK01-APRVL-CODE-UT027-F TO WK01-APRVL-CODE-UT02-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-APRVL-CODE-UT028-Z = 'Y'
               MOVE WK01-APRVL-CODE-UT028 TO ALTRNT-APRVL-CODE-UT29 (8)
           END-IF.
           MOVE WK01-APRVL-CODE-UT028-F TO WK01-APRVL-CODE-UT02-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-APRVL-CODE-UT029-Z = 'Y'
               MOVE WK01-APRVL-CODE-UT029 TO ALTRNT-APRVL-CODE-UT29 (9)
           END-IF.
           MOVE WK01-APRVL-CODE-UT029-F TO WK01-APRVL-CODE-UT02-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT0B11-Z = 'Y'
               MOVE WK01-NAME-UT0B11 TO ALTRNT-FULL-NAME-UT29 (1)
           END-IF.
           MOVE WK01-NAME-UT0B11-F TO WK01-NAME-UT0A1-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT0A12-Z = 'Y'
               MOVE WK01-NAME-UT0A12 TO ALTRNT-FULL-NAME-UT29 (2)
           END-IF.
           MOVE WK01-NAME-UT0A12-F TO WK01-NAME-UT0A1-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT0A13-Z = 'Y'
               MOVE WK01-NAME-UT0A13 TO ALTRNT-FULL-NAME-UT29 (3)
           END-IF.
           MOVE WK01-NAME-UT0A13-F TO WK01-NAME-UT0A1-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT0A14-Z = 'Y'
               MOVE WK01-NAME-UT0A14 TO ALTRNT-FULL-NAME-UT29 (4)
           END-IF.
           MOVE WK01-NAME-UT0A14-F TO WK01-NAME-UT0A1-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT0A15-Z = 'Y'
               MOVE WK01-NAME-UT0A15 TO ALTRNT-FULL-NAME-UT29 (5)
           END-IF.
           MOVE WK01-NAME-UT0A15-F TO WK01-NAME-UT0A1-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT0A16-Z = 'Y'
               MOVE WK01-NAME-UT0A16 TO ALTRNT-FULL-NAME-UT29 (6)
           END-IF.
           MOVE WK01-NAME-UT0A16-F TO WK01-NAME-UT0A1-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT0A17-Z = 'Y'
               MOVE WK01-NAME-UT0A17 TO ALTRNT-FULL-NAME-UT29 (7)
           END-IF.
           MOVE WK01-NAME-UT0A17-F TO WK01-NAME-UT0A1-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT0A18-Z = 'Y'
               MOVE WK01-NAME-UT0A18 TO ALTRNT-FULL-NAME-UT29 (8)
           END-IF.
           MOVE WK01-NAME-UT0A18-F TO WK01-NAME-UT0A1-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-UT0A19-Z = 'Y'
               MOVE WK01-NAME-UT0A19 TO ALTRNT-FULL-NAME-UT29 (9)
           END-IF.
           MOVE WK01-NAME-UT0A19-F TO WK01-NAME-UT0A1-F (9).
