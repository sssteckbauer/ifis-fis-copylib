       975-TO-R4174-TOF-HDR SECTION.
           MOVE TFH-USER-CD TO
               USER-CODE-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-UNVRS-CD TO
               UNVRS-CODE-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-DOC-NBR TO
               DCMNT-NMBR-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACTVY-DATE-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-DOC-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               DCMNT-DATE-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-DOC-DESC TO
               DCMNT-DESC-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-DOC-AMT TO
               DCMNT-AMT-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-CURR-DEBT-AMT TO
               CURR-DEBIT-AMT-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-CURR-CRDT-AMT TO
               CURR-CRDT-AMT-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-PERM-DEBT-AMT TO
               PERM-DEBIT-AMT-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-PERM-CRDT-AMT TO
               PERM-CRDT-AMT-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-HDR-ERROR-IND TO
               HDR-ERROR-IND-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-HDR-CNTR-ACCT TO
               HDR-CNTR-ACCT-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-PSTNG-IND TO
               PSTNG-IND-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-CURR-APRVL-IND TO
               CURR-APRVL-IND-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-PERM-APRVL-IND TO
               PERM-APRVL-IND-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-UPDT-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               UPDT-ACTVY-DATE-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-UPDT-TIME-STMP TO
               DBLINKX-INPUT-PARM
           MOVE '4' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-TIME-FORMAT
           MOVE '51' TO DBLINKX-IDMS-TIME-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-PARM TO
               UPDT-TIME-STAMP-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-CURR-PSTNG-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               CURR-PSTNG-DATE-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-PERM-EXTR-DAT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               PERM-EXTR-DATE-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-CMPLT-IND TO
               CMPLT-IND-4174 OF R4174-TOF-HDR
                                                                     .
           MOVE TFH-SUB-DOC-TYP TO
               SUB-DCMNT-TYPE-4174 OF R4174-TOF-HDR
                                                                     .
       975-TO-R4174-TOF-HDR-EXIT.
           EXIT.
