       01  R4017-BC-EFCTV.
           02  DB-PROC-ID-4017.
               03  USER-CODE-4017                       PIC X(8).
               03  LAST-ACTVY-DATE-4017                 PIC X(5).
               03  TRMNL-ID-4017                        PIC X(8).
               03  PURGE-FLAG-4017                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  EFCTV-RCRD-KEY-4017.
               03  START-DATE-4017            COMP-3    PIC S9(8).
               03  TIME-STAMP-4017                      PIC X(6).
           02  END-DATE-4017                  COMP-3    PIC S9(8).
           02  STATUS-4017                              PIC X(1).
           02  CNTRL-FUND-4017                          PIC X(6).
           02  CNTRL-ORGZN-4017                         PIC X(6).
           02  CNTRL-PRD-CODE-4017                      PIC X(1).
           02  CNTRL-SVRTY-CODE-4017                    PIC X(1).
