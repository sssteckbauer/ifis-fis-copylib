       976-FROM-R6268 SECTION.

FCCCE**        MOVE USER-CODE-6268 OF R6268
FCCCE          MOVE DBLINK-USER-CD
               TO CST-USER-CD.

               MOVE LAST-ACTVY-DATE-6268 OF R6268
               TO CST-LAST-ACTVY-DT.

               MOVE UNVRS-CODE-6268 OF R6268
               TO CST-UNVRS-CD.

               MOVE PARA-CODE-6268 OF R6268
               TO CST-PARA-CD.

               MOVE SHORT-DESC-6268 OF R6268
               TO CST-SHORT-DESC.

               MOVE LONG-DESC-6268 OF R6268
               TO CST-LONG-DESC.

               MOVE COMM-VIEW-6268 OF R6268
               TO CST-COMM-VIEW.

               MOVE OFC-CODE-6268 OF R6268
               TO CST-OFC-CD.
       976-FROM-R6268-EXIT.
           EXIT.
