       01  R4121-TXT-DTL.
           02  DB-PROC-ID-4121.
               03  USER-CODE-4121                       PIC X(8).
               03  LAST-ACTVY-DATE-4121                 PIC X(5).
               03  TRMNL-ID-4121                        PIC X(8).
               03  PURGE-FLAG-4121                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  CLAUSE-CODE-4121                         PIC X(8).
           02  CMNT-TEXT-4121                           PIC X(55).
           02  PRINT-FLAG-4121                          PIC X(1).
