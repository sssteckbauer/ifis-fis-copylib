    ***Created by Convert/DC version V8R03 on 12/08/00 at 14:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY01-Z = 'Y'
               MOVE WK01-TEXT-SY01 TO DCSD-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0A-Z = 'Y'
               MOVE WK01-TEXT-SY0A TO CNFDL-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0B-Z = 'Y'
               MOVE WK01-TEXT-SY0B TO HOLDS-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4060-TA35-Z = 'Y'
               MOVE WK01-CODE-4060-TA35 TO RPRT-CODE-4060-TA35 OF
                FTAWK35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4060-TA35-Z = 'Y'
               MOVE WK01-TITLE-4060-TA35 TO RPRT-TITLE-4060-TA35 OF
                FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-4058-TA35-Z = 'Y'
               MOVE WK01-ID-4058-TA35 TO USER-ID-4058-TA35 OF FTAWK35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-TA35-Z = 'Y'
               MOVE WK01-CODE-TA35 TO ACTN-CODE-TA35 OF FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACTVY-DATE-4058-TA35-Z = 'Y'
               MOVE WK01-ACTVY-DATE-4058-TA35 TO
                LAST-ACTVY-DATE-4058-TA35 OF FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4058-TA35-Z = 'Y'
               MOVE WK01-NMBR-4058-TA35 TO SEQ-NMBR-4058-TA35 OF FTAWK35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-NMBR-TA35-Z = 'Y'
               MOVE WK01-SEQ-NMBR-TA35 TO RQST-SEQ-NMBR-TA35 OF FTAWK35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RPT-LIT-TA35-Z = 'Y'
               MOVE WK01-RPT-LIT-TA35 TO DUNNING-RPT-LIT-TA35 OF FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PARM-LIT-TA35-Z = 'Y'
               MOVE WK01-PARM-LIT-TA35 TO SORT-PARM-LIT-TA35 OF FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PLAN-LIT-TA35-Z = 'Y'
               MOVE WK01-PLAN-LIT-TA35 TO COMM-PLAN-LIT-TA35 OF FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEL-LIT-TA35-Z = 'Y'
               MOVE WK01-SEL-LIT-TA35 TO LABEL-SEL-LIT-TA35 OF FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ADR-TYPE-CODE-LT-T35-Z = 'Y'
               MOVE WK01-ADR-TYPE-CODE-LT-T35 TO
                TA-ADR-TYPE-CODE-LIT-TA35 OF FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEL-DATE-LIT-TA35-Z = 'Y'
               MOVE WK01-SEL-DATE-LIT-TA35 TO ADR-SEL-DATE-LIT-TA35 OF
                FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-LITERAL-LIT-TA35-Z = 'Y'
               MOVE WK01-LITERAL-LIT-TA35 TO LABEL-LITERAL-LIT-TA35 OF
                FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4058-TA35-Z = 'Y'
               MOVE WK01-IND-4058-TA35 TO HOLD-IND-4058-TA35 OF FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-OF-DATE-4058-TA35-Z = 'Y'
               MOVE WK01-OF-DATE-4058-TA35 TO AS-OF-DATE-4058-TA35 OF
                FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RPT-IND-4058-TA35-Z = 'Y'
               MOVE WK01-RPT-IND-4058-TA35 TO DUNNING-RPT-IND-4058-TA35
                OF FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PARM-4058-TA35-Z = 'Y'
               MOVE WK01-PARM-4058-TA35 TO SORT-PARM-4058-TA35 OF
                FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PLAN-4058-TA35-Z = 'Y'
               MOVE WK01-PLAN-4058-TA35 TO COMM-PLAN-4058-TA35 OF
                FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEL-IND-4058-TA35-Z = 'Y'
               MOVE WK01-SEL-IND-4058-TA35 TO LABEL-SEL-IND-4058-TA35
                OF FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ADR-TYPE-CD-4058-T351-Z = 'Y'
               MOVE WK01-ADR-TYPE-CD-4058-T351 TO
                TA-ADR-TYPE-CODE-4058-TA35 OF FTAWM35(0001)
           END-IF.
           MOVE WK01-ADR-TYPE-CD-4058-T351-F TO
                WK01-ADR-TYPE-CD-4058-T35-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-TYPE-CD-4058-T353-Z = 'Y'
               MOVE WK01-ADR-TYPE-CD-4058-T353 TO
                TA-ADR-TYPE-CODE-4058-TA35 OF FTAWM35(0003)
           END-IF.
           MOVE WK01-ADR-TYPE-CD-4058-T353-F TO
                WK01-ADR-TYPE-CD-4058-T35-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-TYPE-CD-4058-T355-Z = 'Y'
               MOVE WK01-ADR-TYPE-CD-4058-T355 TO
                TA-ADR-TYPE-CODE-4058-TA35 OF FTAWM35(0005)
           END-IF.
           MOVE WK01-ADR-TYPE-CD-4058-T355-F TO
                WK01-ADR-TYPE-CD-4058-T35-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-SEL-DATE-4058-TA35-Z = 'Y'
               MOVE WK01-SEL-DATE-4058-TA35 TO ADR-SEL-DATE-4058-TA35
                OF FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-LITERAL-4058-TA35-Z = 'Y'
               MOVE WK01-LITERAL-4058-TA35 TO LABEL-LITERAL-4058-TA35
                OF FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6259-TA35-Z = 'Y'
               MOVE WK01-DESC-6259-TA35 TO SHORT-DESC-6259-TA35 OF
                FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-TA351-Z = 'Y'
               MOVE WK01-DESC-6137-TA351 TO SHORT-DESC-6137-TA35 OF
                FTAWM35(0001)
           END-IF.
           MOVE WK01-DESC-6137-TA351-F TO WK01-DESC-6137-TA35-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-TA353-Z = 'Y'
               MOVE WK01-DESC-6137-TA353 TO SHORT-DESC-6137-TA35 OF
                FTAWM35(0003)
           END-IF.
           MOVE WK01-DESC-6137-TA353-F TO WK01-DESC-6137-TA35-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-NOTICES-LIT-TA35-Z = 'Y'
               MOVE WK01-NOTICES-LIT-TA35 TO DUNNING-NOTICES-LIT-TA35
                OF FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SORT-PARM-LIT-TA35-Z = 'Y'
               MOVE WK01-SORT-PARM-LIT-TA35 TO LABEL-SORT-PARM-LIT-TA35
                OF FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ADR-TYPE-CD-4058-T352-Z = 'Y'
               MOVE WK01-ADR-TYPE-CD-4058-T352 TO
                TA-ADR-TYPE-CODE-4058-TA35 OF FTAWM35(0002)
           END-IF.
           MOVE WK01-ADR-TYPE-CD-4058-T352-F TO
                WK01-ADR-TYPE-CD-4058-T35-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-TYPE-CD-4058-T354-Z = 'Y'
               MOVE WK01-ADR-TYPE-CD-4058-T354 TO
                TA-ADR-TYPE-CODE-4058-TA35 OF FTAWM35(0004)
           END-IF.
           MOVE WK01-ADR-TYPE-CD-4058-T354-F TO
                WK01-ADR-TYPE-CD-4058-T35-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-TOP-LINE-LIT-TA35-Z = 'Y'
               MOVE WK01-TOP-LINE-LIT-TA35 TO PRINT-TOP-LINE-LIT-TA35
                OF FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4058-TA3A-Z = 'Y'
               MOVE WK01-IND-4058-TA3A TO DLT-IND-4058-TA35 OF FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEL-IND-4058-TA3A-Z = 'Y'
               MOVE WK01-SEL-IND-4058-TA3A TO DATA-SEL-IND-4058-TA35 OF
                FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NOTICES-IND-4058-T35-Z = 'Y'
               MOVE WK01-NOTICES-IND-4058-T35 TO
                DUNNING-NOTICES-IND-4058-TA35 OF FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SORT-PARM-4058-TA35-Z = 'Y'
               MOVE WK01-SORT-PARM-4058-TA35 TO
                LABEL-SORT-PARM-4058-TA35 OF FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-TA352-Z = 'Y'
               MOVE WK01-DESC-6137-TA352 TO SHORT-DESC-6137-TA35 OF
                FTAWM35(0002)
           END-IF.
           MOVE WK01-DESC-6137-TA352-F TO WK01-DESC-6137-TA35-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-TA354-Z = 'Y'
               MOVE WK01-DESC-6137-TA354 TO SHORT-DESC-6137-TA35 OF
                FTAWM35(0004)
           END-IF.
           MOVE WK01-DESC-6137-TA354-F TO WK01-DESC-6137-TA35-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-TOP-LIN-IND-4058-T35-Z = 'Y'
               MOVE WK01-TOP-LIN-IND-4058-T35 TO
                PRINT-TOP-LINE-IND-4058-TA35 OF FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEL-LIT-TA3A-Z = 'Y'
               MOVE WK01-SEL-LIT-TA3A TO DATA-SEL-LIT-TA35 OF FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-EML-ADR-TYP-CD-4058-T31-Z = 'Y'
               MOVE WK01-EML-ADR-TYP-CD-4058-T31 TO
                TA-EMAIL-ADR-TYPE-COD-4058-T35 OF FTAWM35(0001)
           END-IF.
           MOVE WK01-EML-ADR-TYP-CD-4058-T31-F TO
                WK01-EML-ADR-TYP-CD-4058-T3-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-TA356-Z = 'Y'
               MOVE WK01-DESC-6137-TA356 TO SHORT-DESC-6137-TA35 OF
                FTAWM35(0006)
           END-IF.
           MOVE WK01-DESC-6137-TA356-F TO WK01-DESC-6137-TA35-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-EML-ADR-TYP-CD-4058-T32-Z = 'Y'
               MOVE WK01-EML-ADR-TYP-CD-4058-T32 TO
                TA-EMAIL-ADR-TYPE-COD-4058-T35 OF FTAWM35(0002)
           END-IF.
           MOVE WK01-EML-ADR-TYP-CD-4058-T32-F TO
                WK01-EML-ADR-TYP-CD-4058-T3-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-TA357-Z = 'Y'
               MOVE WK01-DESC-6137-TA357 TO SHORT-DESC-6137-TA35 OF
                FTAWM35(0007)
           END-IF.
           MOVE WK01-DESC-6137-TA357-F TO WK01-DESC-6137-TA35-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-EML-ADR-TYP-CD-4058-T33-Z = 'Y'
               MOVE WK01-EML-ADR-TYP-CD-4058-T33 TO
                TA-EMAIL-ADR-TYPE-COD-4058-T35 OF FTAWM35(0003)
           END-IF.
           MOVE WK01-EML-ADR-TYP-CD-4058-T33-F TO
                WK01-EML-ADR-TYP-CD-4058-T3-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-TA358-Z = 'Y'
               MOVE WK01-DESC-6137-TA358 TO SHORT-DESC-6137-TA35 OF
                FTAWM35(0008)
           END-IF.
           MOVE WK01-DESC-6137-TA358-F TO WK01-DESC-6137-TA35-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-EML-ADR-TYP-CD-4058-T34-Z = 'Y'
               MOVE WK01-EML-ADR-TYP-CD-4058-T34 TO
                TA-EMAIL-ADR-TYPE-COD-4058-T35 OF FTAWM35(0004)
           END-IF.
           MOVE WK01-EML-ADR-TYP-CD-4058-T34-F TO
                WK01-EML-ADR-TYP-CD-4058-T3-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-TA359-Z = 'Y'
               MOVE WK01-DESC-6137-TA359 TO SHORT-DESC-6137-TA35 OF
                FTAWM35(0009)
           END-IF.
           MOVE WK01-DESC-6137-TA359-F TO WK01-DESC-6137-TA35-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-EMAIL-TYPE-CD-LT-T35-Z = 'Y'
               MOVE WK01-EMAIL-TYPE-CD-LT-T35 TO
                TA-EMAIL-TYPE-CODE-LIT-TA35 OF FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TO-DATE-4058-TA35-Z = 'Y'
               MOVE WK01-TO-DATE-4058-TA35 TO TA-TO-DATE-4058-TA35 OF
                FTAWM35
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-TA355-Z = 'Y'
               MOVE WK01-DESC-6137-TA355 TO SHORT-DESC-6137-TA35 OF
                FTAWM35(0005)
           END-IF.
           MOVE WK01-DESC-6137-TA355-F TO WK01-DESC-6137-TA35-F (5).
