      ******************************************************************
      *  CREATED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.
      *  COPY MODULE:  WSCOMPC2
      *  REMARKS: IF THE CALLING PROGRAM IS NOT COMPILED UNDER COBOL-II,
      *           THE FIRST DATA DESCRIPTION SHOULD BE USED, AND THE
      *           REMAINING SHOULD BE COMMENTED OUT.
      ******************************************************************

           05  COMPILED-DATA.
               10  COMPILED-DATA-TIME       PIC X(008) VALUE SPACES.
               10  COMPILED-DATA-DATE       PIC X(013) VALUE SPACES.

      *    05  COMPILED-DATA.
      *        10  COMPILED-DATA-DATE       PIC X(008) VALUE SPACES.
      *        10  COMPILED-DATA-TIME       PIC X(008) VALUE SPACES.

