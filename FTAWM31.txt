      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FTAWM31                              04/24/00  12:45  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FTAWM31.
           02  SEL-IND-TA31                             PIC X(1).
           02  ACTN-CODE-GROUP-TA31.
               03  ACTN-CODE-TA31           OCCURS 11   PIC X(1).
           02  DCMNT-NMBR-TA31              OCCURS 11   PIC X(8).
           02  APRVD-AMT-TA31               OCCURS 11   PIC X(12).
           02  INVD-AMT-TA31                OCCURS 11   PIC X(12).
           02  LAST-ACTVY-DATE-TA31         OCCURS 11   PIC X(6).
           02  APRVL-IND-TA31               OCCURS 11   PIC X(1).
           02  CRDT-MEMO-TA31               OCCURS 11   PIC X(1).
           02  OPEN-PAID-IND-TA31           OCCURS 11   PIC X(1).
