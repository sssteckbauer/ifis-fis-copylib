      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWD13                              04/24/00  12:55  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWD13.
           02  SYSTEM-DATE-AP13               COMP-3    PIC S9(8).
           02  INDX-KEY-4155-AP13.
               03  UNVRS-CODE-AP13                      PIC X(2).
               03  SEQ-NMBR-KEY-AP13                    PIC 9(4).
           02  PREV-INCM-TYPE-AP13          OCCURS 15   PIC X(2).
