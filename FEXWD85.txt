      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FEXWD85                              04/24/00  13:06  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FEXWD85.
           02  INDX-KEY-4073-EX85.
               03  UNVRS-CODE-KEY-EX85                  PIC X(2).
               03  VNDR-CODE-KEY-EX85.
                   04  VNDR-ID-DIGIT-ONE-KEY-EX85       PIC X(1).
                   04  VNDR-ID-LAST-NINE-KEY-EX85       PIC X(9).
           02  SET-SORT-KEY-6311-EX85.
               03  UNVRS-CODE-EX85                      PIC X(2).
               03  INTRL-REF-ID-EX85          COMP-3    PIC S9(7).
           02  SYSTEM-DATE-EX85               COMP-3    PIC S9(8).
