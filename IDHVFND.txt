       976-FROM-R4001-FUND SECTION.

FCCCE**        MOVE USER-CODE-4001 OF R4001-FUND
FCCCE          MOVE DBLINK-USER-CD
               TO FND-USER-CD.

               MOVE LAST-ACTVY-DATE-4001 OF R4001-FUND
               TO FND-LAST-ACTVY-DT.

               MOVE UNVRS-CODE-4001 OF R4001-FUND
               TO FND-UNVRS-CD.

               MOVE COA-CODE-4001 OF R4001-FUND
               TO FND-COA-CD.

               MOVE FUND-CODE-4001 OF R4001-FUND
               TO FND-FUND-CD.

           IF ACTVY-DATE-4001 OF R4001-FUND  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO FND-ACTVY-DT
           ELSE
               MOVE ACTVY-DATE-4001 OF R4001-FUND
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO FND-ACTVY-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO FND-ACTVY-DT
               END-IF
           END-IF.
       976-FROM-R4001-FUND-EXIT.
           EXIT.
