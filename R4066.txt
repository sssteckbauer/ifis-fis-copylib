       01  R4066-ACTV-EFCTV.
           02  DB-PROC-ID-4066.
               03  USER-CODE-4066                       PIC X(8).
               03  LAST-ACTVY-DATE-4066                 PIC X(5).
               03  TRMNL-ID-4066                        PIC X(8).
               03  PURGE-FLAG-4066                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  EFCTV-RCRD-KEY-4066.
               03  START-DATE-4066            COMP-3    PIC S9(8).
               03  TIME-STAMP-4066                      PIC X(6).
           02  END-DATE-4066                  COMP-3    PIC S9(8).
           02  STATUS-4066                              PIC X(1).
           02  ACTVY-TITLE-4066                         PIC X(35).
