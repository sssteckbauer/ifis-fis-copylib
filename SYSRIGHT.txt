       200-SYSRIGHT-S01.

           INSPECT WK-SYS-ID-PASSED
             CONVERTING 'abcdefghijklmnopqrstuvwxyz'
                     TO 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.

           MOVE SPACES TO ACWLEFT-PARM-IN.
           STRING WK-SYS-ID-PASSED
                              DELIMITED BY SIZE
             INTO ACWLEFT-PARM-IN.
           CALL 'ACXLEFT' USING ACWLEFT-PARMS.
           MOVE  ACWLEFT-PARM-OUT
                  TO WK-SYS-ID-PASSED.

           IF WK-SYS-ID-PASSED (1:3) = FPECURID-DUP-TYPE (1)
              MOVE FPECURID-DUP-ID(1)
                TO WK-SYS-ID-PASSED
              GO TO 200-SYSRIGHT-S01-EXIT
           END-IF.

           IF WK-SYS-ID-PASSED (1:3) = FPECURID-DUP-TYPE (2)
              MOVE FPECURID-DUP-ID(2)
                TO WK-SYS-ID-PASSED
              GO TO 200-SYSRIGHT-S01-EXIT
           END-IF.

           IF WK-SYS-ID-PASSED (1:3) = FPECURID-DUP-TYPE (3)
              MOVE FPECURID-DUP-ID(3)
                TO WK-SYS-ID-PASSED
              GO TO 200-SYSRIGHT-S01-EXIT
           END-IF.

           MOVE ZEROES TO WS-LEADING-SPACES
                          WS-LEADING-ZEROES
                          WS-ACCUM-SYS-ID-LNG.

           INSPECT WK-SYS-ID-PASSED
                   TALLYING WS-LEADING-SPACES FOR LEADING SPACES.

           INSPECT WK-SYS-ID-PASSED
                   TALLYING WS-LEADING-ZEROES FOR ALL SPACES.

           IF WS-LEADING-ZEROES EQUAL ZERO
              MOVE LENGTH OF WK-SYS-ID-PASSED
                TO WS-ACCUM-SYS-ID-LNG
           ELSE
              COMPUTE WS-ACCUM-SYS-ID-LNG
                    = LENGTH OF WK-SYS-ID-PASSED
                    - WS-LEADING-ZEROES
              END-COMPUTE
           END-IF.

           COMPUTE WS-LEADING-SPACES
                 = WS-LEADING-SPACES
                 + +1
           END-COMPUTE.

           COMPUTE WS-LEADING-ZEROES
                 = WS-LEADING-ZEROES
                 + +1
           END-COMPUTE.

           MOVE ZEROES TO WK-SYS-ID-LST-NINE

           MOVE WK-SYS-ID-PASSED
                (WS-LEADING-SPACES:WS-ACCUM-SYS-ID-LNG)
             TO WK-SYS-ID-LST-NINE-X
                (WS-LEADING-ZEROES:WS-ACCUM-SYS-ID-LNG).

           MOVE WK-SYS-ID-LST-NINE-X
             TO WK-SYS-ID-PASSED.

       200-SYSRIGHT-S01-EXIT.  EXIT.
