      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM33                              04/24/00  12:21  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM33.
           02  ACTN-CODE-CH33                           PIC X(1).
           02  ACCT-INDX-TITLE-4067-CH33                PIC X(35).
           02  STATUS-DESC-CH33                         PIC X(8).
           02  END-DATE-4067-CH33                       PIC X(6).
           02  EARLY-INACTV-DATE-4067-CH33              PIC X(6).
           02  NXT-CHANGE-DATE-CH33                     PIC X(6).
           02  TIME-STAMP-4067-CH33                     PIC X(6).
           02  AM-PM-FLAG-CH33                          PIC X(1).
           02  FUND-CODE-4067-CH33                      PIC X(6).
           02  FUND-TITLE-4005-CH33                     PIC X(35).
           02  FUND-OVRDE-4067-CH33                     PIC X(1).
           02  ORGZN-CODE-4067-CH33                     PIC X(6).
           02  ORGZN-TITLE-4010-CH33                    PIC X(35).
           02  ACCT-CODE-4067-CH33                      PIC X(6).
           02  ACCT-CODE-TITLE-4034-CH33                PIC X(35).
           02  PRGRM-CODE-4067-CH33                     PIC X(6).
           02  PRGRM-TITLE-4105-CH33                    PIC X(35).
           02  ACTVY-CODE-4067-CH33                     PIC X(6).
           02  ACTVY-TITLE-4066-CH33                    PIC X(35).
           02  LCTN-CODE-4067-CH33                      PIC X(6).
           02  LCTN-TITLE-4007-CH33                     PIC X(35).
           02  LAST-ACTVY-DATE-4045-CH33                PIC X(6).
           02  EMAIL-MSG-LIT-CH33                       PIC X(31).
           02  EMAIL-MSG-IND-CH33                       PIC X(01).
           02  EMAIL-MSG-LIT2-CH33                      PIC X(07).
           02  EMAIL-MSG-ADDR-CH33                      PIC X(32).
           02  UCOP-FUND-CH33                           PIC X(05).
           02  UCOP-FUND-DESC-CH33                      PIC X(35).
           02  SPNSR-CODE-CH33                          PIC X(04).
           02  SPNSR-DESC-CH33                          PIC X(35).
           02  IDC-RATE-CH33                            PIC X(08).
           02  WS-NEED-TO-ADD-XTC-SW                    PIC X(01).
           02  WS-NEED-TO-ADD-UCF-SW                    PIC X(01).
           02  WS-FROM-PROGRAM                          PIC X(08).
           02  USER-CD-4067-CH33                        PIC X(08).
