      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM88                              04/24/00  12:20  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM88.
           02  ACTN-CODE-GROUP-CH88.
               03  ACTN-CODE-CH88           OCCURS 11   PIC X(1).
           02  COA-CODE-4045-CH88           OCCURS 11   PIC X(1).
           02  ACCT-INDX-CODE-4045-CH88     OCCURS 11   PIC X(10).
           02  ACCT-INDX-TITLE-4067-CH88    OCCURS 11   PIC X(35).
           02  STATUS-4067-CH88             OCCURS 11   PIC X(1).
           02  START-DATE-4067-CH88         OCCURS 11   PIC X(6).
           02  END-DATE-4067-CH88           OCCURS 11   PIC X(6).
