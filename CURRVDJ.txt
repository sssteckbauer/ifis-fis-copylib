      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4165-VNDR-J' TO DBLINK-CURR-PARAGRAPH.       276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4165-VNDR-J' TO RECORD-NAME.                          276 BRTN
YYY991     MOVE 'F-VENDOR' TO AREA-NAME.                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4165-VNDR-J TO DBLINK-RECORD-MADE-CURRENT.      276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4165-VNDR-J-EXIT                         276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE VDJ-FK-VDR-IREF-ID TO DBLINK-R4165-VNDR-J-01        276 BRTN
YYY991         MOVE VDJ-FK-VDR-ENTPSN-IND TO DBLINK-R4165-VNDR-J-02     276 BRTN
YYY991         MOVE VDJ-VDR-SET-TS TO DBLINK-R4165-VNDR-J-03            276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4165-VNDR-J TO DBLINK-VIEW-NAME-CURRENT.        276 BRTN
YYY991     MOVE DBLINK-R4165-VNDR-J-KEY TO DBLINK-VIEW-200-CURRENT.     276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4073-4165                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE VDJ-FK-VDR-IREF-ID TO DBLINK-S-4073-4165-01.            276 BRTN
YYY991     MOVE VDJ-FK-VDR-ENTPSN-IND TO DBLINK-S-4073-4165-02.         276 BRTN
YYY991     MOVE VDJ-VDR-SET-TS TO DBLINK-S-4073-4165-03.                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4166-4165                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE VDJ-FK-VDT-VNDR-TYP-CD TO DBLINK-S-4166-4165-01.        276 BRTN
YYY991     MOVE VDJ-VDT-SET-TS TO DBLINK-S-4166-4165-02.                276 BRTN
