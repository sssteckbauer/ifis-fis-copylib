      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FEXWM50                              04/24/00  12:30  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FEXWM50.
           02  CATALOG-NMBR-EX50                        PIC X(14).
           02  CATALOG-DESC-EX50                        PIC X(35).
           02  UNIT-MEA-CODE-EX50                       PIC X(3).
           02  QTY-EX50                       COMP-3    PIC S9(6)V99.
           02  UNIT-PRICE-EX50                COMP-3    PIC 9(10)V9999.
           02  TOTAL-AMT-EX50                 COMP-3    PIC S9(10)V99.
           02  DEBIT-CRDT-IND-EX50                      PIC X(1).
           02  TOT-DEBIT-CRDT-IND-EX50                  PIC X(1).
           02  DTL-DTL-ERROR-IND-EX50                   PIC X(1).
           02  ACTN-CODE-EX50                           PIC X(1).
           02  EQP-IND-EX50                             PIC X(1).
           02  PART-NMBR-EX50                           PIC X(15).
