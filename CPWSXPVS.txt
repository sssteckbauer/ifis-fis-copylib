      * /************************************************************/  $
      * /*   CHANGE LOG -- CPWSXPVS                                 */  $
      * /*   NAME            DATE      COMMENTS                     */  $
      * /*..........................................................*/  $
      * /*   BROWN          05/23/85  MODIFIED FOR COLLECTIVE BARG. */  $
      * /*   B. ORTIZ       05/07/85  MODIFIED FOR GFAU (3202RRRR)  */  $
      * /*   I. ACKS        09/23/98  ADD DATE STAMP - CCYYMM FBP086*/  $
      * /************************************************************/  $
      *01  XPVS-PROVISION-SEGMENT.                                      CPWSXPVS
           05  XPVS-FILE-KEY.                                           CPWSXPVS
      ****     10  XPVS-LOCATION               PIC X(02).               3202RRRR
      ****     10  XPVS-SAU                    PIC X(01).               3202RRRR
      ****     10  XPVS-SUB-CAMPUS             PIC X(01).               3202RRRR
      ****     10  XPVS-ACCOUNT                PIC X(07).               3202RRRR
      ****     10  XPVS-FUND                   PIC X(06).               3202RRRR
      ****     10  XPVS-SUB-BUDGET             PIC X(02).               3202RRRR
001410         10  XPVS-FAU.                                            3202RRRR
001411             15  XPVS-LOCATION           PIC X(02).               3202RRRR
001420             15  XPVS-SAU                PIC X(01).               3202RRRR
001430             15  XPVS-SUB-CAMPUS         PIC X(01).               3202RRRR
001440             15  XPVS-ACCOUNT            PIC X(07).               3202RRRR
001450             15  XPVS-FUND               PIC X(06).               3202RRRR
001460             15  XPVS-SUB-BUDGET         PIC X(02).               3202RRRR
               10  XPVS-SEGMENT-SORT-TYPE      PIC X(01).               CPWSXPVS
           05  XPVS-SEGMENT-KEY.                                        CPWSXPVS
               10  XPVS-TITLE                  PIC X(04).               CPWSXPVS
               10  XPVS-SORT-CONSTANT          PIC X(01).               CPWSXPVS
      *                               ALWAYS HIGH-VALUES, TO SORT       CPWSXPVS
      *                               PROVISION RECS AFTER PAYROLL      CPWSXPVS
               10  XPVS-PROVISION-NO           PIC X(06).               CPWSXPVS
           05  FILLER                          PIC X(01).               CPWSXPVS
           05  XPVS-PROVISION-TYPE             PIC X(01).               CPWSXPVS
           05  XPVS-EFFECTIVE-DATE.                                     CPWSXPVS
               10  XPVS-EFFECTIVE-YEAR         PIC X(02).               CPWSXPVS
               10  XPVS-EFFECTIVE-MONTH        PIC X(02).               CPWSXPVS
               10  XPVS-EFFECTIVE-DAY          PIC X(02).               CPWSXPVS
           05  XPVS-PROVISION-DESCR            PIC X(30).               CPWSXPVS
           05  XPVS-PROVISION-RATE             PIC S9(5)V99.            CPWSXPVS
           05  XPVS-PROVISION-FTE              PIC S9(4)V99.            CPWSXPVS
           05  XPVS-PROVISION-AMT              PIC S9(7).               CPWSXPVS
           05  XPVS-PERB-CODE                  PIC X(02).               $
           05  XPVS-TITLE-SPECL-HANDLING-CODE  PIC X(01).               $
           05  XPVS-APPT-REPRESENTATION-CODE   PIC X(01).               $
           05  XPVS-RANGE-ADJ-DISTR-UNIT-CODE  PIC X(01).               $
ILA01      05  XPVS-DATE-STAMP.                                         $PWSXPVS
ILA01          10  XPVS-DATE-STAMP-CC          PIC X(02).               $PWSXPVS
ILA01          10  XPVS-DATE-STAMP-YY          PIC X(02).               $PWSXPVS
ILA01          10  XPVS-DATE-STAMP-MM          PIC X(02).               $PWSXPVS
ILA01      05  FILLER                          PIC X(14).               $PWSXPVS
           05  XPVS-PROVISION-SEGMENT-TYPE     PIC X(01).               CPWSXPVS
           05  XPVS-IFIS-IFOAPAL.
               10  XPVS-IFIS-INDEX             PIC X(10).
               10  XPVS-IFIS-FUND              PIC X(06).
               10  XPVS-IFIS-ORG               PIC X(06).
               10  XPVS-IFIS-ACCOUNT           PIC X(06).
               10  XPVS-IFIS-PROGRAM           PIC X(06).
               10  XPVS-IFIS-ACTIVITY          PIC X(06).
               10  XPVS-IFIS-LOCATION          PIC X(06).
           05  XPVS-IFIS-TITLES.
               10  XPVS-IFIS-INDEX-TITLE       PIC X(35).
               10  XPVS-IFIS-FUND-TITLE        PIC X(35).
               10  XPVS-IFIS-ORG-TITLE         PIC X(35).
               10  XPVS-IFIS-ACCOUNT-TITLE     PIC X(35).
               10  XPVS-IFIS-PROGRAM-TITLE     PIC X(35).
               10  XPVS-IFIS-ACTIVITY-TITLE    PIC X(35).
               10  XPVS-IFIS-LOCATION-TITLE    PIC X(35).
       01  XPVS-FILE-CONTROL-TOTALS            REDEFINES                CPWSXPVS
           XPVS-PROVISION-SEGMENT.                                      CPWSXPVS
           05  XPVS-CTL-REC-KEY                PIC  X(33).              CPWSXPVS
               88  XPVS-CTL-REC                VALUE HIGH-VALUES.       CPWSXPVS
           05  XPVS-CTL-RECS                   PIC S9(05).              CPWSXPVS
           05  XPVS-CTL-FTE                    PIC S9(05)V99.           CPWSXPVS
           05  XPVS-CTL-AMT                    PIC S9(11).              CPWSXPVS
           05  FILLER                          PIC  X(105).             CPWSXPVS
