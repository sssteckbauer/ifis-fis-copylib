       01  R4104-VENDOR-CMD.
           02  DB-PROC-ID-4104.
               03  USER-CODE-4104                       PIC X(8).
               03  LAST-ACTVY-DATE-4104                 PIC X(5).
               03  TRMNL-ID-4104                        PIC X(8).
               03  PURGE-FLAG-4104                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  CMDTY-CODE-4104                          PIC X(8).
