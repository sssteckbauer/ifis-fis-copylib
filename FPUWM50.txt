      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWM50                              04/24/00  12:37  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWM50.
           02  ITEM-NMBR-PU50        COMP-3 OCCURS 10   PIC 9(4).
           02  INV-ITEM-NMBR-PU50    COMP-3 OCCURS 10   PIC 9(4).
           02  UNIT-PRICE-PU50       COMP-3 OCCURS 10   PIC 9(10)V99.
           02  INV-UNIT-PRICE-PU50   COMP-3 OCCURS 10   PIC 9(10)V9999.
           02  DCMNT-NMBR-PU50              OCCURS 10   PIC X(8).
           02  VNDR-INV-NMBR-PU50           OCCURS 10   PIC X(9).
           02  CNCL-IND-PU50                OCCURS 10   PIC X(1).
           02  QP-SIGN-PU50                 OCCURS 10   PIC X(1).
           02  CHK-DATE-PU50                OCCURS 10   PIC X(8).
           02  CHK-CASH-DATE-PU50           OCCURS 10   PIC X(8).
           02  R-IND-PU50                   OCCURS 10   PIC X(1).
           02  CHK-VNDR-DESC1-PU50                      PIC X(10).
           02  CHK-VNDR-DESC2-PU50                      PIC X(10).
           02  INDEX-KEY-4159-PU50.
               03  UNVRS-CODE-4159-PU50                 PIC X(2).
               03  DCMNT-NMBR-4159-PU50                 PIC X(8).
