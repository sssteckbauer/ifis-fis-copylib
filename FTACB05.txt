           04  DBLINK-RECORD-KEYS.
           05  DBLINK-R4400-TRVL-KEY.
               10  DBLINK-R4400-TRVL-01     PIC S9(7)       COMP-3
                       VALUE ZERO.
               10  DBLINK-R4400-TRVL-02     PIC X(1)        VALUE SPACE.
           05  DBLINK-R4405-EXPNS-KEY.
               10  DBLINK-R4405-EXPNS-01    PIC X(8)        VALUE SPACE.
           05  DBLINK-R4408-EXPNS-DIST-KEY.
               10  DBLINK-R4408-EXPNS-DIST-01
                                            PIC X(8)        VALUE SPACE.
               10  DBLINK-R4408-EXPNS-DIST-02
                                            PIC S9(4)       COMP-3
                       VALUE ZERO.
           05  DBLINK-R4212-APRVL-HDR-KEY.
               10  DBLINK-R4212-APRVL-HDR-01
                                            PIC X(2)        VALUE SPACE.
               10  DBLINK-R4212-APRVL-HDR-02
                                            PIC S9(4)       COMP-3
                       VALUE ZERO.
               10  DBLINK-R4212-APRVL-HDR-03
                                            PIC X(8)        VALUE SPACE.
               10  DBLINK-R4212-APRVL-HDR-04
                                            PIC X(3)        VALUE SPACE.
           05  DBLINK-R4401-EVENT-KEY.
               10  DBLINK-R4401-EVENT-01    PIC X(8)        VALUE SPACE.
           05  DBLINK-R4406-RECON-KEY.
               10  DBLINK-R4406-RECON-01    PIC X(8)        VALUE SPACE.
           05  DBLINK-R6117-PRSN-KEY.
               10  DBLINK-R6117-PRSN-01     PIC S9(7)       COMP-3
                       VALUE ZERO.
           05  DBLINK-R6311-ENTY-KEY.
               10  DBLINK-R6311-ENTY-01     PIC X(1)        VALUE SPACE.
               10  DBLINK-R6311-ENTY-02     PIC X(9)        VALUE SPACE.
           05  DBLINK-R4000-COA-KEY.
               10  DBLINK-R4000-COA-01      PIC X(2)        VALUE SPACE.
               10  DBLINK-R4000-COA-02      PIC X(1)        VALUE SPACE.
           05  DBLINK-R4011-SYSDAT-KEY.
               10  DBLINK-R4011-SYSDAT-01   PIC X(8)        VALUE SPACE.
               10  DBLINK-R4011-SYSDAT-02   PIC X(30)       VALUE SPACE.
           05  DBLINK-R4012-SYSDAT-DTL-KEY.
               10  DBLINK-R4012-SYSDAT-DTL-01
                                            PIC X(8)        VALUE SPACE.
               10  DBLINK-R4012-SYSDAT-DTL-02
                                            PIC X(30)       VALUE SPACE.
               10  DBLINK-R4012-SYSDAT-DTL-03
                                            PIC X(8)        VALUE SPACE.
               10  DBLINK-R4012-SYSDAT-DTL-04
                                            PIC X(8)        VALUE SPACE.
               10  DBLINK-R4012-SYSDAT-DTL-05
                                            PIC S9(2)       COMP-3
                       VALUE ZERO.
           05  DBLINK-R4013-USER-DTL-KEY.
               10  DBLINK-R4013-USER-DTL-01 PIC X(8)        VALUE SPACE.
               10  DBLINK-R4013-USER-DTL-02 PIC X(1)        VALUE SPACE.
           05  DBLINK-R4076-COA-EFCTV-KEY.
               10  DBLINK-R4076-COA-EFCTV-01
                                            PIC X(2)        VALUE SPACE.
               10  DBLINK-R4076-COA-EFCTV-02
                                            PIC X(1)        VALUE SPACE.
               10  DBLINK-R4076-COA-EFCTV-03
                                            PIC X(10)       VALUE ZERO.
               10  DBLINK-R4076-COA-EFCTV-04
                                            PIC X(8)        VALUE ZERO.
           05  DBLINK-R4180-RULE-CLASS-KEY.
               10  DBLINK-R4180-RULE-CLASS-01
                                            PIC X(4)        VALUE SPACE.
           05  DBLINK-R4181-RULE-EFCTV-KEY.
               10  DBLINK-R4181-RULE-EFCTV-01
                                            PIC X(4)        VALUE SPACE.
               10  DBLINK-R4181-RULE-EFCTV-02
                                            PIC X(10)       VALUE ZERO.
               10  DBLINK-R4181-RULE-EFCTV-03
                                            PIC X(8)        VALUE ZERO.
           05  DBLINK-R4192-USER-PRFL-KEY.
               10  DBLINK-R4192-USER-PRFL-01
                                            PIC X(8)        VALUE SPACE.
           05  DBLINK-R4203-DOCUMENT-KEY.
               10  DBLINK-R4203-DOCUMENT-01 PIC S9(4)       COMP-3
                       VALUE ZERO.
           05  DBLINK-R4204-APRVL-J-KEY.
               10  DBLINK-R4204-APRVL-J-01  PIC S9(4)       COMP-3
                       VALUE ZERO.
               10  DBLINK-R4204-APRVL-J-02  PIC X(8)        VALUE SPACE.
           05  DBLINK-R4205-APRVL-LVL-KEY.
               10  DBLINK-R4205-APRVL-LVL-01
                                            PIC S9(4)       COMP-3
                       VALUE ZERO.
               10  DBLINK-R4205-APRVL-LVL-02
                                            PIC X(8)        VALUE SPACE.
               10  DBLINK-R4205-APRVL-LVL-03
                                            PIC X(3)        VALUE SPACE.
               10  DBLINK-R4205-APRVL-LVL-04
                                            PIC S9(4)       COMP-3
                       VALUE ZERO.
           05  DBLINK-R4211-APRVL-ALT-KEY.
               10  DBLINK-R4211-APRVL-ALT-01
                                            PIC S9(4)       COMP-3
                       VALUE ZERO.
               10  DBLINK-R4211-APRVL-ALT-02
                                            PIC X(8)        VALUE SPACE.
               10  DBLINK-R4211-APRVL-ALT-03
                                            PIC X(3)        VALUE SPACE.
               10  DBLINK-R4211-APRVL-ALT-04
                                            PIC S9(4)       COMP-3
                       VALUE ZERO.
               10  DBLINK-R4211-APRVL-ALT-05
                                            PIC X(26)       VALUE ZERO.
           05  DBLINK-R4215-APRV-TMPLT-KEY.
               10  DBLINK-R4215-APRV-TMPLT-01
                                            PIC S9(4)       COMP-3
                       VALUE ZERO.
               10  DBLINK-R4215-APRV-TMPLT-02
                                            PIC X(8)        VALUE SPACE.
               10  DBLINK-R4215-APRV-TMPLT-03
                                            PIC X(3)        VALUE SPACE.
           05  DBLINK-R4402-TRIP-KEY.
               10  DBLINK-R4402-TRIP-01     PIC X(8)        VALUE SPACE.
           05  DBLINK-R4403-TRIP-ENC-KEY.
               10  DBLINK-R4403-TRIP-ENC-01 PIC X(8)        VALUE SPACE.
               10  DBLINK-R4403-TRIP-ENC-02 PIC S9(4)       COMP-3
                       VALUE ZERO.
           04  DBLINK-SET-KEYS.
           05  DBLINK-S-4401-4406-KEY.
             08  DBLINK-S-4401-4406-KEY-O.
               10  DBLINK-S-4401-4406-01    PIC X(8)        VALUE SPACE.
             08  DBLINK-S-4401-4406-KEY-M.
             09  DBLINK-S-4401-4406-KEY-S.
               10  DBLINK-S-4401-4406-02    PIC X(26)       VALUE ZERO.
           05  DBLINK-S-4405-4408-KEY.
             08  DBLINK-S-4405-4408-KEY-O.
               10  DBLINK-S-4405-4408-01    PIC X(8)        VALUE SPACE.
             08  DBLINK-S-4405-4408-KEY-M.
             09  DBLINK-S-4405-4408-KEY-U.
               10  DBLINK-S-4405-4408-02    PIC S9(4)       COMP-3
                       VALUE ZERO.
           05  DBLINK-S-4401-4402-KEY.
             08  DBLINK-S-4401-4402-KEY-O.
               10  DBLINK-S-4401-4402-01    PIC X(8)        VALUE SPACE.
             08  DBLINK-S-4401-4402-KEY-M.
             09  DBLINK-S-4401-4402-KEY-S.
               10  DBLINK-S-4401-4402-02    PIC X(26)       VALUE ZERO.
           05  DBLINK-WS-S-4405-4408-KEY.
               10  DBLINK-WS-S-4405-4408-02 PIC 9(4)        VALUE ZERO.
           05  DBLINK-S-4400-4405-KEY.
             08  DBLINK-S-4400-4405-KEY-O.
               10  DBLINK-S-4400-4405-01    PIC S9(7)       COMP-3
                       VALUE ZERO.
               10  DBLINK-S-4400-4405-02    PIC X(1)        VALUE SPACE.
             08  DBLINK-S-4400-4405-KEY-M.
             09  DBLINK-S-4400-4405-KEY-U.
               10  DBLINK-S-4400-4405-03    PIC X(8)        VALUE SPACE.
           05  DBLINK-S-4401-4405-KEY.
             08  DBLINK-S-4401-4405-KEY-O.
               10  DBLINK-S-4401-4405-01    PIC X(8)        VALUE SPACE.
             08  DBLINK-S-4401-4405-KEY-M.
             09  DBLINK-S-4401-4405-KEY-S.
               10  DBLINK-S-4401-4405-02    PIC X(26)       VALUE ZERO.
           05  DBLINK-S-4212-4214-KEY.
             08  DBLINK-S-4212-4214-KEY-O.
               10  DBLINK-S-4212-4214-01    PIC X(2)        VALUE SPACE.
               10  DBLINK-S-4212-4214-02    PIC S9(4)       COMP-3
                       VALUE ZERO.
               10  DBLINK-S-4212-4214-03    PIC X(8)        VALUE SPACE.
               10  DBLINK-S-4212-4214-04    PIC X(3)        VALUE SPACE.
             08  DBLINK-S-4212-4214-KEY-M.
             09  DBLINK-S-4212-4214-KEY-U.
               10  DBLINK-S-4212-4214-05    PIC S9(4)       COMP-3
                       VALUE ZERO.
           05  DBLINK-S-4205-4212-F         PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4205-4212-KEY.
             08  DBLINK-S-4205-4212-KEY-O.
               10  DBLINK-S-4205-4212-01    PIC S9(4)       COMP-3
                       VALUE ZERO.
               10  DBLINK-S-4205-4212-02    PIC X(8)        VALUE SPACE.
               10  DBLINK-S-4205-4212-03    PIC X(3)        VALUE SPACE.
               10  DBLINK-S-4205-4212-04    PIC S9(4)       COMP-3
                       VALUE ZERO.
             08  DBLINK-S-4205-4212-KEY-M.
             09  DBLINK-S-4205-4212-KEY-S.
               10  DBLINK-S-4205-4212-05    PIC X(26)       VALUE ZERO.
           05  DBLINK-S-INDX-APRVL-F        PIC X(1)        VALUE SPACE.
           05  DBLINK-S-INDX-APRVL-KEY.
             08  DBLINK-S-INDX-APRVL-KEY-M.
             09  DBLINK-S-INDX-APRVL-KEY-U.
               10  DBLINK-S-INDX-APRVL-01   PIC S9(4)       COMP-3
                       VALUE ZERO.
               10  DBLINK-S-INDX-APRVL-02   PIC X(8)        VALUE SPACE.
               10  DBLINK-S-INDX-APRVL-03   PIC X(3)        VALUE SPACE.
             09  DBLINK-S-INDX-APRVL-KEY-S.
               10  DBLINK-S-INDX-APRVL-04   PIC X(26)       VALUE ZERO.
           05  DBLINK-S-INDX-UNAPRVD-F      PIC X(1)        VALUE SPACE.
           05  DBLINK-S-INDX-UNAPRVD-KEY.
             08  DBLINK-S-INDX-UNAPRVD-KEY-M.
             09  DBLINK-S-INDX-UNAPRVD-KEY-U.
               10  DBLINK-S-INDX-UNAPRVD-01 PIC X(8)        VALUE SPACE.
               10  DBLINK-S-INDX-UNAPRVD-02 PIC S9(4)       COMP-3
                       VALUE ZERO.
               10  DBLINK-S-INDX-UNAPRVD-03 PIC X(8)        VALUE SPACE.
               10  DBLINK-S-INDX-UNAPRVD-04 PIC X(3)        VALUE SPACE.
           05  DBLINK-S-4400-4401-KEY.
             08  DBLINK-S-4400-4401-KEY-O.
               10  DBLINK-S-4400-4401-01    PIC S9(7)       COMP-3
                       VALUE ZERO.
               10  DBLINK-S-4400-4401-02    PIC X(1)        VALUE SPACE.
             08  DBLINK-S-4400-4401-KEY-M.
             09  DBLINK-S-4400-4401-KEY-U.
               10  DBLINK-S-4400-4401-03    PIC X(8)        VALUE SPACE.
           05  DBLINK-S-4400-4402-KEY.
             08  DBLINK-S-4400-4402-KEY-O.
               10  DBLINK-S-4400-4402-01    PIC S9(7)       COMP-3
                       VALUE ZERO.
               10  DBLINK-S-4400-4402-02    PIC X(1)        VALUE SPACE.
             08  DBLINK-S-4400-4402-KEY-M.
             09  DBLINK-S-4400-4402-KEY-U.
               10  DBLINK-S-4400-4402-03    PIC X(8)        VALUE SPACE.
           05  DBLINK-S-4400-4404-KEY.
             08  DBLINK-S-4400-4404-KEY-O.
               10  DBLINK-S-4400-4404-01    PIC S9(7)       COMP-3
                       VALUE ZERO.
               10  DBLINK-S-4400-4404-02    PIC X(1)        VALUE SPACE.
             08  DBLINK-S-4400-4404-KEY-M.
             09  DBLINK-S-4400-4404-KEY-U.
               10  DBLINK-S-4400-4404-03    PIC X(8)        VALUE SPACE.
           05  DBLINK-S-INDX-TRVL-KEY.
             08  DBLINK-S-INDX-TRVL-KEY-M.
             09  DBLINK-S-INDX-TRVL-KEY-U.
               10  FILLER                   PIC X(2)        VALUE SPACE.
               10  DBLINK-S-INDX-TRVL-01    PIC X(1)        VALUE SPACE.
               10  DBLINK-S-INDX-TRVL-02    PIC X(9)        VALUE SPACE.
           05  DBLINK-S-4401-4404-KEY.
             08  DBLINK-S-4401-4404-KEY-O.
               10  DBLINK-S-4401-4404-01    PIC X(8)        VALUE SPACE.
             08  DBLINK-S-4401-4404-KEY-M.
             09  DBLINK-S-4401-4404-KEY-S.
               10  DBLINK-S-4401-4404-02    PIC X(26)       VALUE ZERO.
           05  DBLINK-S-4401-4411-KEY.
             08  DBLINK-S-4401-4411-KEY-O.
               10  DBLINK-S-4401-4411-01    PIC X(8)        VALUE SPACE.
             08  DBLINK-S-4401-4411-KEY-M.
             09  DBLINK-S-4401-4411-KEY-S.
               10  DBLINK-S-4401-4411-02    PIC X(26)       VALUE ZERO.
           05  DBLINK-S-4401-4405-F         PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4406-4418-KEY.
             08  DBLINK-S-4406-4418-KEY-O.
               10  DBLINK-S-4406-4418-01    PIC X(8)        VALUE SPACE.
             08  DBLINK-S-4406-4418-KEY-M.
             09  DBLINK-S-4406-4418-KEY-U.
               10  DBLINK-S-4406-4418-02    PIC S9(4)       COMP-3
                       VALUE ZERO.
           05  DBLINK-S-6117-6139-KEY.
             08  DBLINK-S-6117-6139-KEY-O.
               10  DBLINK-S-6117-6139-01    PIC S9(7)       COMP-3
                       VALUE ZERO.
             08  DBLINK-S-6117-6139-KEY-M.
             09  DBLINK-S-6117-6139-KEY-U.
               10  DBLINK-S-6117-6139-02    PIC X(2)        VALUE SPACE.
               10  DBLINK-S-6117-6139-03    PIC X(10)       VALUE ZERO.
           05  DBLINK-S-6117-6156-KEY.
             08  DBLINK-S-6117-6156-KEY-O.
               10  DBLINK-S-6117-6156-01    PIC S9(7)       COMP-3
                       VALUE ZERO.
             08  DBLINK-S-6117-6156-KEY-M.
             09  DBLINK-S-6117-6156-KEY-U.
               10  DBLINK-S-6117-6156-02    PIC X(10)       VALUE ZERO.
             09  DBLINK-S-6117-6156-KEY-S.
               10  DBLINK-S-6117-6156-03    PIC X(26)       VALUE ZERO.
           05  DBLINK-S-6117-6157-KEY.
             08  DBLINK-S-6117-6157-KEY-O.
               10  DBLINK-S-6117-6157-01    PIC S9(7)       COMP-3
                       VALUE ZERO.
             08  DBLINK-S-6117-6157-KEY-M.
             09  DBLINK-S-6117-6157-KEY-U.
               10  DBLINK-S-6117-6157-02    PIC X(10)       VALUE ZERO.
             09  DBLINK-S-6117-6157-KEY-S.
               10  DBLINK-S-6117-6157-03    PIC X(26)       VALUE ZERO.
           05  DBLINK-S-6117-6274-KEY.
             08  DBLINK-S-6117-6274-KEY-O.
               10  DBLINK-S-6117-6274-01    PIC S9(7)       COMP-3
                       VALUE ZERO.
             08  DBLINK-S-6117-6274-KEY-M.
             09  DBLINK-S-6117-6274-KEY-U.
               10  DBLINK-S-6117-6274-02    PIC X(10)       VALUE ZERO.
               10  DBLINK-S-6117-6274-03    PIC S9(6)       COMP-3
                       VALUE ZERO.
               10  DBLINK-S-6117-6274-04    PIC X(4)        VALUE SPACE.
             09  DBLINK-S-6117-6274-KEY-S.
               10  DBLINK-S-6117-6274-05    PIC X(26)       VALUE ZERO.
           05  DBLINK-S-6117-6313-KEY.
             08  DBLINK-S-6117-6313-KEY-O.
               10  DBLINK-S-6117-6313-01    PIC S9(7)       COMP-3
                       VALUE ZERO.
             08  DBLINK-S-6117-6313-KEY-M.
             09  DBLINK-S-6117-6313-KEY-U.
               10  DBLINK-S-6117-6313-02    PIC X(10)       VALUE ZERO.
             09  DBLINK-S-6117-6313-KEY-S.
               10  DBLINK-S-6117-6313-03    PIC X(26)       VALUE ZERO.
           05  DBLINK-S-6001-6117-F         PIC X(1)        VALUE SPACE.
           05  DBLINK-S-6001-6117-KEY.
             08  DBLINK-S-6001-6117-KEY-O.
               10  DBLINK-S-6001-6117-01    PIC X(2)        VALUE SPACE.
             08  DBLINK-S-6001-6117-KEY-M.
             09  DBLINK-S-6001-6117-KEY-S.
               10  DBLINK-S-6001-6117-02    PIC X(26)       VALUE ZERO.
           05  DBLINK-S-INDX-SOC-SCRTY-KEY.
             08  DBLINK-S-INDX-SOC-SCRTY-KEY-M.
             09  DBLINK-S-INDX-SOC-SCRTY-KEY-U.
               10  DBLINK-S-INDX-SOC-SCRTY-01
                                            PIC X(1)        VALUE SPACE.
               10  DBLINK-S-INDX-SOC-SCRTY-02
                                            PIC X(9)        VALUE SPACE.
             09  DBLINK-S-INDX-SOC-SCRTY-KEY-S.
               10  DBLINK-S-INDX-SOC-SCRTY-03
                                            PIC X(26)       VALUE ZERO.
           05  DBLINK-S-6311-6185-KEY.
             08  DBLINK-S-6311-6185-KEY-O.
               10  DBLINK-S-6311-6185-01    PIC X(1)        VALUE SPACE.
               10  DBLINK-S-6311-6185-02    PIC X(9)        VALUE SPACE.
             08  DBLINK-S-6311-6185-KEY-M.
             09  DBLINK-S-6311-6185-KEY-U.
               10  DBLINK-S-6311-6185-03    PIC X(2)        VALUE SPACE.
               10  DBLINK-S-6311-6185-04    PIC X(10)       VALUE ZERO.
           05  DBLINK-S-6311-6186-KEY.
             08  DBLINK-S-6311-6186-KEY-O.
               10  DBLINK-S-6311-6186-01    PIC X(1)        VALUE SPACE.
               10  DBLINK-S-6311-6186-02    PIC X(9)        VALUE SPACE.
             08  DBLINK-S-6311-6186-KEY-M.
             09  DBLINK-S-6311-6186-KEY-U.
               10  DBLINK-S-6311-6186-03    PIC X(10)       VALUE ZERO.
             09  DBLINK-S-6311-6186-KEY-S.
               10  DBLINK-S-6311-6186-04    PIC X(26)       VALUE ZERO.
           05  DBLINK-S-6311-6274-KEY.
             08  DBLINK-S-6311-6274-KEY-O.
               10  DBLINK-S-6311-6274-01    PIC X(1)        VALUE SPACE.
               10  DBLINK-S-6311-6274-02    PIC X(9)        VALUE SPACE.
             08  DBLINK-S-6311-6274-KEY-M.
             09  DBLINK-S-6311-6274-KEY-U.
               10  DBLINK-S-6311-6274-03    PIC X(10)       VALUE ZERO.
               10  DBLINK-S-6311-6274-04    PIC S9(6)       COMP-3
                       VALUE ZERO.
               10  DBLINK-S-6311-6274-05    PIC X(4)        VALUE SPACE.
             09  DBLINK-S-6311-6274-KEY-S.
               10  DBLINK-S-6311-6274-06    PIC X(26)       VALUE ZERO.
           05  DBLINK-S-6311-6350-KEY.
             08  DBLINK-S-6311-6350-KEY-O.
               10  DBLINK-S-6311-6350-01    PIC X(1)        VALUE SPACE.
               10  DBLINK-S-6311-6350-02    PIC X(9)        VALUE SPACE.
             08  DBLINK-S-6311-6350-KEY-M.
             09  DBLINK-S-6311-6350-KEY-U.
               10  DBLINK-S-6311-6350-03    PIC X(4)        VALUE SPACE.
               10  DBLINK-S-6311-6350-04    PIC X(4)        VALUE SPACE.
           05  DBLINK-S-6311-6352-KEY.
             08  DBLINK-S-6311-6352-KEY-O.
               10  DBLINK-S-6311-6352-01    PIC X(1)        VALUE SPACE.
               10  DBLINK-S-6311-6352-02    PIC X(9)        VALUE SPACE.
             08  DBLINK-S-6311-6352-KEY-M.
             09  DBLINK-S-6311-6352-KEY-U.
               10  DBLINK-S-6311-6352-03    PIC X(10)       VALUE ZERO.
             09  DBLINK-S-6311-6352-KEY-S.
               10  DBLINK-S-6311-6352-04    PIC X(26)       VALUE ZERO.
           05  DBLINK-S-6311-6353-KEY.
             08  DBLINK-S-6311-6353-KEY-O.
               10  DBLINK-S-6311-6353-01    PIC X(1)        VALUE SPACE.
               10  DBLINK-S-6311-6353-02    PIC X(9)        VALUE SPACE.
             08  DBLINK-S-6311-6353-KEY-M.
             09  DBLINK-S-6311-6353-KEY-U.
               10  DBLINK-S-6311-6353-03    PIC X(10)       VALUE ZERO.
             09  DBLINK-S-6311-6353-KEY-S.
               10  DBLINK-S-6311-6353-04    PIC X(26)       VALUE ZERO.
           05  DBLINK-S-INDX-ENTY-KEY.
             08  DBLINK-S-INDX-ENTY-KEY-M.
             09  DBLINK-S-INDX-ENTY-KEY-U.
               10  FILLER                   PIC X(2)        VALUE SPACE.
               10  DBLINK-S-INDX-ENTY-01    PIC S9(7)       COMP-3
                       VALUE ZERO.
           05  DBLINK-S-4205-4211-KEY.
             08  DBLINK-S-4205-4211-KEY-O.
               10  DBLINK-S-4205-4211-01    PIC S9(4)       COMP-3
                       VALUE ZERO.
               10  DBLINK-S-4205-4211-02    PIC X(8)        VALUE SPACE.
               10  DBLINK-S-4205-4211-03    PIC X(3)        VALUE SPACE.
               10  DBLINK-S-4205-4211-04    PIC S9(4)       COMP-3
                       VALUE ZERO.
             08  DBLINK-S-4205-4211-KEY-M.
             09  DBLINK-S-4205-4211-KEY-S.
               10  DBLINK-S-4205-4211-05    PIC X(26)       VALUE ZERO.
           05  DBLINK-S-4215-4205-KEY.
             08  DBLINK-S-4215-4205-KEY-O.
               10  DBLINK-S-4215-4205-01    PIC S9(4)       COMP-3
                       VALUE ZERO.
               10  DBLINK-S-4215-4205-02    PIC X(8)        VALUE SPACE.
               10  DBLINK-S-4215-4205-03    PIC X(3)        VALUE SPACE.
             08  DBLINK-S-4215-4205-KEY-M.
             09  DBLINK-S-4215-4205-KEY-U.
               10  DBLINK-S-4215-4205-04    PIC S9(4)       COMP-3
                       VALUE ZERO.
           05  DBLINK-S-4402-4403-KEY.
             08  DBLINK-S-4402-4403-KEY-O.
               10  DBLINK-S-4402-4403-01    PIC X(8)        VALUE SPACE.
             08  DBLINK-S-4402-4403-KEY-M.
             09  DBLINK-S-4402-4403-KEY-U.
               10  DBLINK-S-4402-4403-02    PIC S9(4)       COMP-3
                       VALUE ZERO.
           05  DBLINK-S-4000-4076-KEY.
             08  DBLINK-S-4000-4076-KEY-O.
               10  DBLINK-S-4000-4076-01    PIC X(2)        VALUE SPACE.
               10  DBLINK-S-4000-4076-02    PIC X(1)        VALUE SPACE.
             08  DBLINK-S-4000-4076-KEY-M.
             09  DBLINK-S-4000-4076-KEY-U.
               10  DBLINK-S-4000-4076-03    PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4000-4076-04    PIC X(8)        VALUE ZERO.
           05  DBLINK-S-4180-4181-KEY.
             08  DBLINK-S-4180-4181-KEY-O.
               10  DBLINK-S-4180-4181-01    PIC X(4)        VALUE SPACE.
             08  DBLINK-S-4180-4181-KEY-M.
             09  DBLINK-S-4180-4181-KEY-U.
               10  DBLINK-S-4180-4181-02    PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4180-4181-03    PIC X(8)        VALUE ZERO.
           05  DBLINK-S-4011-4012-KEY.
             08  DBLINK-S-4011-4012-KEY-O.
               10  DBLINK-S-4011-4012-01    PIC X(8)        VALUE SPACE.
               10  DBLINK-S-4011-4012-02    PIC X(30)       VALUE SPACE.
             08  DBLINK-S-4011-4012-KEY-M.
             09  DBLINK-S-4011-4012-KEY-U.
               10  DBLINK-S-4011-4012-03    PIC X(8)        VALUE SPACE.
               10  DBLINK-S-4011-4012-04    PIC X(8)        VALUE SPACE.
               10  DBLINK-S-4011-4012-05    PIC S9(2)       COMP-3
                       VALUE ZERO.
           05  DBLINK-S-4192-4013-KEY.
             08  DBLINK-S-4192-4013-KEY-O.
               10  DBLINK-S-4192-4013-01    PIC X(8)        VALUE SPACE.
             08  DBLINK-S-4192-4013-KEY-M.
             09  DBLINK-S-4192-4013-KEY-U.
               10  DBLINK-S-4192-4013-02    PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4203-4204-KEY.
             08  DBLINK-S-4203-4204-KEY-O.
               10  DBLINK-S-4203-4204-01    PIC S9(4)       COMP-3
                       VALUE ZERO.
             08  DBLINK-S-4203-4204-KEY-M.
             09  DBLINK-S-4203-4204-KEY-U.
               10  DBLINK-S-4203-4204-02    PIC X(8)        VALUE SPACE.
           05  DBLINK-S-4204-4215-KEY.
             08  DBLINK-S-4204-4215-KEY-O.
               10  DBLINK-S-4204-4215-01    PIC S9(4)       COMP-3
                       VALUE ZERO.
               10  DBLINK-S-4204-4215-02    PIC X(8)        VALUE SPACE.
             08  DBLINK-S-4204-4215-KEY-M.
             09  DBLINK-S-4204-4215-KEY-U.
               10  DBLINK-S-4204-4215-03    PIC X(3)        VALUE SPACE.
           05  DBLINK-WS-S-4402-4403-KEY.
               10  DBLINK-WS-S-4402-4403-02 PIC 9(4)        VALUE ZERO.
           05  DBLINK-WS-S-4011-4012-KEY.
               10  DBLINK-WS-S-4011-4012-03 PIC X(8)        VALUE SPACE.
               10  DBLINK-WS-S-4011-4012-04 PIC X(8)        VALUE SPACE.
               10  DBLINK-WS-S-4011-4012-05 PIC 9(2)        VALUE ZERO.
           05  DBLINK-WS-S-4180-4181-KEY.
               10  DBLINK-WS-S-4180-4181-02 PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4180-4181-03 PIC X(6)        VALUE SPACE.
           05  DBLINK-WS-S-4000-4076-KEY.
               10  DBLINK-WS-S-4000-4076-03 PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4000-4076-04 PIC X(6)        VALUE SPACE.
           05  DBLINK-S-INDX-COA-KEY.
             08  DBLINK-S-INDX-COA-KEY-M.
             09  DBLINK-S-INDX-COA-KEY-U.
               10  DBLINK-S-INDX-COA-01     PIC X(2)        VALUE SPACE.
               10  DBLINK-S-INDX-COA-02     PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4011-4018-KEY.
             08  DBLINK-S-4011-4018-KEY-O.
               10  DBLINK-S-4011-4018-01    PIC X(8)        VALUE SPACE.
               10  DBLINK-S-4011-4018-02    PIC X(30)       VALUE SPACE.
             08  DBLINK-S-4011-4018-KEY-M.
             09  DBLINK-S-4011-4018-KEY-U.
               10  DBLINK-S-4011-4018-03    PIC X(8)        VALUE SPACE.
               10  DBLINK-S-4011-4018-04    PIC X(8)        VALUE SPACE.
               10  DBLINK-S-4011-4018-05    PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4011-4018-06    PIC X(8)        VALUE ZERO.
           05  DBLINK-S-INDX-SYSDAT-KEY.
             08  DBLINK-S-INDX-SYSDAT-KEY-M.
             09  DBLINK-S-INDX-SYSDAT-KEY-U.
               10  FILLER                   PIC X(2)        VALUE SPACE.
               10  DBLINK-S-INDX-SYSDAT-01  PIC X(8)        VALUE SPACE.
               10  DBLINK-S-INDX-SYSDAT-02  PIC X(30)       VALUE SPACE.
               10  FILLER                   PIC X(1)        VALUE SPACE.
           05  DBLINK-S-INDX-RULE-CLSS-KEY.
             08  DBLINK-S-INDX-RULE-CLSS-KEY-M.
             09  DBLINK-S-INDX-RULE-CLSS-KEY-U.
               10  FILLER                   PIC X(2)        VALUE SPACE.
               10  DBLINK-S-INDX-RULE-CLSS-01
                                            PIC X(4)        VALUE SPACE.
           05  DBLINK-S-4181-4182-KEY.
             08  DBLINK-S-4181-4182-KEY-O.
               10  DBLINK-S-4181-4182-01    PIC X(4)        VALUE SPACE.
               10  DBLINK-S-4181-4182-02    PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4181-4182-03    PIC X(8)        VALUE ZERO.
             08  DBLINK-S-4181-4182-KEY-M.
             09  DBLINK-S-4181-4182-KEY-U.
               10  DBLINK-S-4181-4182-04    PIC S9(4)       COMP-3
                       VALUE ZERO.
           05  DBLINK-S-4181-4184-KEY.
             08  DBLINK-S-4181-4184-KEY-O.
               10  DBLINK-S-4181-4184-01    PIC X(4)        VALUE SPACE.
               10  DBLINK-S-4181-4184-02    PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4181-4184-03    PIC X(8)        VALUE ZERO.
             08  DBLINK-S-4181-4184-KEY-M.
             09  DBLINK-S-4181-4184-KEY-U.
               10  DBLINK-S-4181-4184-04    PIC S9(4)       COMP-3
                       VALUE ZERO.
           05  DBLINK-S-4192-4020-KEY.
             08  DBLINK-S-4192-4020-KEY-O.
               10  DBLINK-S-4192-4020-01    PIC X(8)        VALUE SPACE.
             08  DBLINK-S-4192-4020-KEY-M.
             09  DBLINK-S-4192-4020-KEY-U.
               10  FILLER                   PIC X(1)        VALUE SPACE.
               10  DBLINK-S-4192-4020-02    PIC X(2)        VALUE SPACE.
               10  DBLINK-S-4192-4020-03    PIC X(6)        VALUE SPACE.
           05  DBLINK-S-4192-4204-KEY.
             08  DBLINK-S-4192-4204-KEY-O.
               10  DBLINK-S-4192-4204-01    PIC X(8)        VALUE SPACE.
             08  DBLINK-S-4192-4204-KEY-M.
             09  DBLINK-S-4192-4204-KEY-S.
               10  DBLINK-S-4192-4204-02    PIC X(26)       VALUE ZERO.
           05  DBLINK-S-INDX-USER-PRFL-KEY.
             08  DBLINK-S-INDX-USER-PRFL-KEY-M.
             09  DBLINK-S-INDX-USER-PRFL-KEY-U.
               10  FILLER                   PIC X(2)        VALUE SPACE.
               10  DBLINK-S-INDX-USER-PRFL-01
                                            PIC X(8)        VALUE SPACE.
           05  DBLINK-S-INDX-DCMNT-KEY.
             08  DBLINK-S-INDX-DCMNT-KEY-M.
             09  DBLINK-S-INDX-DCMNT-KEY-U.
               10  FILLER                   PIC X(2)        VALUE SPACE.
               10  DBLINK-S-INDX-DCMNT-01   PIC X(3)        VALUE SPACE.
           05  DBLINK-S-INDX-SEQ-NMBR-KEY.
             08  DBLINK-S-INDX-SEQ-NMBR-KEY-M.
             09  DBLINK-S-INDX-SEQ-NMBR-KEY-U.
               10  FILLER                   PIC X(2)        VALUE SPACE.
               10  DBLINK-S-INDX-SEQ-NMBR-01
                                            PIC S9(4) COMP-3 VALUE ZERO.
           05  DBLINK-S-4401-4402-F         PIC X(1)        VALUE SPACE.
