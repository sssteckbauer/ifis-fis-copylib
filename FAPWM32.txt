      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWM32                              04/24/00  12:17  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWM32.
           02  ACCT-INDX-CODE-AP32                      PIC X(10).
           02  APRVD-AMT-AP32                 COMP-3    PIC S9(10)V99.
           02  DSCNT-AMT-AP32                 COMP-3    PIC S9(10)V99.
           02  TAX-AMT-AP32                   COMP-3    PIC S9(10)V99.
           02  ADDL-CHRG-AP32                 COMP-3    PIC S9(10)V99.
           02  TOTAL-AMT-AP32                 COMP-3    PIC S9(10)V99.
           02  RULE-CLASS-CODE-AP32                     PIC X(4).
           02  DSCNT-RULE-CLASS-AP32                    PIC X(4).
           02  TAX-RULE-CLASS-AP32                      PIC X(4).
           02  ADDL-CHRG-RULE-CLASS-AP32                PIC X(4).
           02  ACTN-CODE-AP32                           PIC X(1).
           02  POSTING-DATE-AP32                        PIC X(6).
           02  FSCL-YR-AP32                             PIC X(2).
           02  ACTG-PRD-AP32                            PIC X(2).
           02  TRADE-IN-IND-AP32                        PIC X(1).
