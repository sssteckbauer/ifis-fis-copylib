       01  R4206-UNAPPROVED.
           02  DB-PROC-ID-4206.
               03  USER-CODE-4206                       PIC X(8).
               03  LAST-ACTVY-DATE-4206                 PIC X(5).
               03  TRMNL-ID-4206                        PIC X(8).
               03  PURGE-FLAG-4206                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  UNAPRVD-KEY-4206.
               03  UNVRS-CODE-4206                      PIC X(2).
               03  SEQ-NMBR-4206                        PIC 9(4).
               03  DCMNT-NMBR-4206                      PIC X(8).
               03  CHNG-SEQ-NMBR-4206                   PIC X(3).
           02  USER-ID-4206                             PIC X(8).
           02  APRVL-ID-4206                            PIC X(8).
           02  RSPN-PRSN-4206                           PIC X(35).
           02  DCMNT-AMT-4206                 COMP-3    PIC S9(10)V99.
           02  LEVEL-SEQ-NMBR-4206                      PIC 9(4).
