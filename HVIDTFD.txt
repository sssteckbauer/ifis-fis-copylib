       975-TO-R4175-TOF-DTL SECTION.
           MOVE TFD-USER-CD TO
               USER-CODE-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-SEQ-NBR TO
               SEQ-NMBR-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACTVY-DATE-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-TIME-STMP TO
               DBLINKX-INPUT-PARM
           MOVE '4' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-TIME-FORMAT
           MOVE '51' TO DBLINKX-IDMS-TIME-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-PARM TO
               TIME-STAMP-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-JRNL-TYP TO
               JRNL-TYPE-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-COA-CD TO
               COA-CODE-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-FUND-CD TO
               FUND-CODE-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-ORGN-CD TO
               ORGZN-CODE-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-ACCT-CD TO
               ACCT-CODE-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-PRGRM-CD TO
               PRGRM-CODE-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-ACTVY-CD TO
               ACTVY-CODE-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-LCTN-CD TO
               LCTN-CODE-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-ACCT-INDX-CD TO
               ACCT-INDX-CODE-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-FSCL-YR TO
               FSCL-YR-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-PSTNG-PRD TO
               PSTNG-PRD-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-BDGT-PRD TO
               BDGT-PRD-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-TRANS-DESC TO
               TRANS-DESC-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-CURR-AMT TO
               CURR-AMT-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-DR-CR-IND TO
               DEBIT-CRDT-IND-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-SAU-CD TO
               SAU-CODE-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-SC-CD TO
               SC-CODE-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-CL-CD TO
               CL-CODE-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-TYP-CD TO
               TYPE-CODE-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-FTE-AMT TO
               FTE-AMT-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-FTE-DR-CR-IND TO
               FTE-DEBIT-CRDT-IND-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-PERM-AMT TO
               PERM-AMT-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-PERM-DR-CR-IND TO
               PERM-DEBIT-CRDT-IND-4175 OF R4175-TOF-DTL
                                                                     .
           MOVE TFD-PERM-TRANS-DESC TO
               PERM-TRANS-DESC-4175 OF R4175-TOF-DTL
                                                                     .
       975-TO-R4175-TOF-DTL-EXIT.
           EXIT.
