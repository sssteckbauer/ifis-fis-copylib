       975-TO-R6257 SECTION.
           MOVE CVW-USER-CD TO
               USER-CODE-6257 OF R6257
                                                                     .
           MOVE CVW-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-6257 OF R6257
                                                                     .
           MOVE CVW-UNVRS-CD TO
               UNVRS-CODE-6257 OF R6257
                                                                     .
           MOVE CVW-COMM-VIEW TO
               COMM-VIEW-6257 OF R6257
                                                                     .
           MOVE CVW-LONG-DESC TO
               LONG-DESC-6257 OF R6257
                                                                     .
           MOVE CVW-BATCH-FLAG TO
               BATCH-FLAG-6257 OF R6257
                                                                     .
       975-TO-R6257-EXIT.
           EXIT.
