      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWK07                              04/24/00  12:16  *
      *                                                               *
      *---------------------------------------------------------------*
      ****----------------------------------------------------------****
      **  AUTHOR    DATE    DESCRIPTION                                *
      **  ------- --------  ----------------------------------------   *
      **  DEVBWS  06/11/02  FIX ASRA ABEND                    ACR33485 *
      ****----------------------------------------------------------****
       01  FAPWK07.
           02  WORK-PRSN-ID-AP07.
               03  WORK-PRSN-ID-ONE-AP07                PIC X(3).
               03  WORK-PRSN-ID-TWO-AP07                PIC X(2).
               03  WORK-PRSN-ID-THREE-AP07              PIC X(4).
           02  NAME-KEY-6117-AP07                       PIC X(35).
           02  WK-VNDR-CODE-4073-AP07.
               03  WK-VNDR-ID-DIGIT-ONE-AP07            PIC X(1).
               03  WK-VNDR-ID-LAST-NINE-AP07            PIC X(9).
           02  EFCTV-RCRD-KEY-4154-AP07.
               03  KEY-TAX-TYPE-4154-AP07               PIC X(1).
               03  KEY-TAX-CODE-IND-4154-AP07           PIC X(1).
               03  KEY-START-DATE-4154-AP07   COMP-3    PIC S9(8).
               03  KEY-TIME-STAMP-4154-AP07             PIC X(6).
DEVBWS     02  SAVE-PMKEY-ID-MINUS-ONE-AP07             PIC X(31).
DEVBWS     02  SAVE-PMKEY-ID-PLUS-ONE-AP07              PIC X(31).
DEVBWS     02  SAVE-PMKEY-ID-AP07                       PIC X(5).
DEVMJM*
           02  TAX-ID-ORIG                              PIC X(9)
                                                          VALUE SPACES.
           02  R4073-VENDOR-ORIG.
               03  DB-PROC-ID-ORIG.
                   05  USER-CODE-ORIG                   PIC X(8).
                   05  LAST-ACTVY-DATE-ORIG             PIC X(5).
                   05  TRMNL-ID-ORIG                    PIC X(8).
                   05  PURGE-FLAG-ORIG                  PIC X(1).
                   05  FILLER01                         PIC X(3).
               03  VNDR-KEY-ORIG.
                   05  UNVRS-CODE-ORIG                  PIC X(2).
                   05  INTRL-REF-ID-ORIG       COMP-3   PIC S9(7).
               03  ENTY-PRSN-IND-ORIG                   PIC X(1).
               03  VNDR-CODE-ORIG.
                   05  VNDR-ID-DIGIT-ONE-ORIG           PIC X(1).
                   05  VNDR-ID-LAST-NINE-ORIG           PIC X(9).
               03  CNTCT-NAME-ORIG                      PIC X(35).
               03  CNTCT-PHONE-ORIG.
                   05  BASIC-TLPHN-ID-ORIG.
                       10  TLPHN-AREA-CODE-ORIG         PIC X(3).
                       10  TLPHN-XCHNG-ID-ORIG          PIC X(3).
                       10  TLPHN-SEQ-ID-ORIG            PIC X(4).
                   05  TLPHN-XTNSN-ID-ORIG              PIC X(4).
               03  SALES-USE-TAX-IND-ORIG               PIC X(1).
               03  CHECK-GRPNG-IND-ORIG                 PIC X(1).
               03  START-DATE-ORIG             COMP-3   PIC S9(8).
               03  END-DATE-ORIG               COMP-3   PIC S9(8).
               03  ACTVY-DATE-ORIG             COMP-3   PIC S9(8).
               03  FDRL-WTHHLD-PCT-ORIG        COMP-3   PIC 9(3)V999.
               03  STATE-WTHHLD-PCT-ORIG       COMP-3   PIC 9(3)V999.
               03  TAX-RATE-CODE-ORIG                   PIC X(3).
               03  ONE-TIME-IND-ORIG                    PIC X(1).
               03  DSCNT-CODE-ORIG                      PIC X(2).
               03  CARR-IND-ORIG                        PIC X(1).
               03  ADR-TYPE-CODE-ORIG                   PIC X(2).
               03  INCM-TYPE-SEQ-NMBR-ORIG              PIC 9(4).
               03  1099-RPRT-ID-ORIG                    PIC X(9).
               03  AP-CRDT-BLNC-IND-ORIG                PIC X(1).
               03  TA-CRDT-BLNC-IND-ORIG                PIC X(1).
               03  SORT-NAME-ORIG                       PIC X(35).
               03  LAST-CHK-PURG-DATE-ORIG     COMP-3   PIC S9(8).
               03  BUS-IND-FLAG-ORIG                    PIC X(1).
               03  DUNS-NBR-ORIG                        PIC X(9).
               03  SQ-VDR-NBR-ORIG                      PIC X(25).
               03  A1099-IND-ORIG                       PIC X(1).
               03  ETHNIC-IND-ORIG                      PIC X(1).
               03  GENDER-IND-ORIG                      PIC X(1).
               03  PAYMT-METHOD-IND-ORIG                PIC X(4).
               03  ENCMBR-IND-ORIG                      PIC X(1).
               03  SQ-VDR-STATUS-ORIG                   PIC X(1).
               03  ACCT-CD-ORIG                         PIC X(6).
FP3175         03  FTB-592-IND-ORIG                     PIC X(1).
FP3175         03  FTB-592-STA-ORIG                     PIC X(1).
FP4314         03  YRLY-TH-AMT-ORIG            COMP-3   PIC 9(8)V99.
FP4914         03  VNDR-URL-CODE-ORIG                   PIC X(50).
           02  R4073-VENDOR-NEW.
               03  DB-PROC-ID-NEW.
                   05  USER-CODE-NEW                    PIC X(8).
                   05  LAST-ACTVY-DATE-NEW              PIC X(5).
                   05  TRMNL-ID-NEW                     PIC X(8).
                   05  PURGE-FLAG-NEW                   PIC X(1).
                   05  FILLER01                         PIC X(3).
               03  VNDR-KEY-NEW.
                   05  UNVRS-CODE-NEW                   PIC X(2).
                   05  INTRL-REF-ID-NEW        COMP-3   PIC S9(7).
                   05  ENTY-PRSN-IND-NEW                PIC X(1).
               03  VNDR-CODE-NEW.
                   05  VNDR-ID-DIGIT-ONE-NEW            PIC X(1).
                   05  VNDR-ID-LAST-NINE-NEW            PIC X(9).
               03  CNTCT-NAME-NEW                       PIC X(35).
               03  CNTCT-PHONE-NEW.
                   05  BASIC-TLPHN-ID-NEW.
                       10  TLPHN-AREA-CODE-NEW          PIC X(3).
                       10  TLPHN-XCHNG-ID-NEW           PIC X(3).
                       10  TLPHN-SEQ-ID-NEW             PIC X(4).
                   05  TLPHN-XTNSN-ID-NEW               PIC X(4).
               03  SALES-USE-TAX-IND-NEW                PIC X(1).
               03  CHECK-GRPNG-IND-NEW                  PIC X(1).
               03  START-DATE-NEW              COMP-3   PIC S9(8).
               03  END-DATE-NEW                COMP-3   PIC S9(8).
               03  ACTVY-DATE-NEW              COMP-3   PIC S9(8).
               03  FDRL-WTHHLD-PCT-NEW         COMP-3   PIC 9(3)V999.
               03  STATE-WTHHLD-PCT-NEW        COMP-3   PIC 9(3)V999.
               03  TAX-RATE-CODE-NEW                    PIC X(3).
               03  ONE-TIME-IND-NEW                     PIC X(1).
               03  DSCNT-CODE-NEW                       PIC X(2).
               03  CARR-IND-NEW                         PIC X(1).
               03  ADR-TYPE-CODE-NEW                    PIC X(2).
               03  INCM-TYPE-SEQ-NMBR-NEW               PIC 9(4).
               03  1099-RPRT-ID-NEW                     PIC X(9).
               03  AP-CRDT-BLNC-IND-NEW                 PIC X(1).
               03  TA-CRDT-BLNC-IND-NEW                 PIC X(1).
               03  SORT-NAME-NEW                        PIC X(35).
               03  LAST-CHK-PURG-DATE-NEW      COMP-3   PIC S9(8).
               03  BUS-IND-FLAG-NEW                     PIC X(1).
               03  DUNS-NBR-NEW                         PIC X(9).
               03  SQ-VDR-NBR-NEW                       PIC X(25).
               03  A1099-IND-NEW                        PIC X(1).
               03  ETHNIC-IND-NEW                       PIC X(1).
               03  GENDER-IND-NEW                       PIC X(1).
               03  PAYMT-METHOD-IND-NEW                 PIC X(4).
               03  ENCMBR-IND-NEW                       PIC X(1).
               03  SQ-VDR-STATUS-NEW                    PIC X(1).
               03  ACCT-CD-NEW                          PIC X(6).
FP3175         03  FTB-592-IND-NEW                      PIC X(1).
FP3175         03  FTB-592-STA-NEW                      PIC X(1).
FP4314         03  YRLY-TH-AMT-NEW             COMP-3   PIC 9(8)V99.
FP4914         03  VNDR-URL-CODE-NEW                    PIC X(50).
           02  AUDIT-WORK.
               03  INTRL-REF-ID-DISP                    PIC 9(7).
               03  NAME-KEY-ORIG.
                   05 NAME-MAIN-ORIG                    PIC X(13).
                   05 FILLER                            PIC X(05).
                   05 REPL-NINE-ORIG                    PIC X(09).
                   05 FILLER                            PIC X(08).
               03  PAN-FLAG-ORIG                        PIC X(1).
               03  VNDR-TYPE-CODE-ORIG                  PIC X(2)
                                        OCCURS 3 INDEXED BY VTC.
               03  DATE-ORIG-DISP                       PIC 9(8).
               03  FILLER REDEFINES DATE-ORIG-DISP.
                   05  CC-ORIG-DISP                     PIC 9(2).
                   05  YY-ORIG-DISP                     PIC 9(2).
                   05  MM-ORIG-DISP                     PIC 9(2).
                   05  DD-ORIG-DISP                     PIC 9(2).
               03  DATE-NEW-DISP                        PIC 9(8).
               03  FILLER REDEFINES DATE-NEW-DISP.
                   05  CC-NEW-DISP                      PIC 9(2).
                   05  YY-NEW-DISP                      PIC 9(2).
                   05  MM-NEW-DISP                      PIC 9(2).
                   05  DD-NEW-DISP                      PIC 9(2).
               03  INCM-TYPE-CODE-ORIG                  PIC X(2).
               03  WTHHLD-PCT-DISP-ORIG                 PIC ZZ9.999.
               03  WTHHLD-PCT-DISP-NEW                  PIC ZZ9.999.
               03  WRITE-OFF-IND-ORIG                   PIC X(1).
FP4314         03  YRLY-TH-AMT-DISP-ORIG                PIC ZZZZZZZ9.99.
FP4314         03  YRLY-TH-AMT-DISP-NEW                 PIC ZZZZZZZ9.99.
DEVMJM*
