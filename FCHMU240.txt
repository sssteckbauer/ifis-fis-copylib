    ***Created by Convert/DC version V8R03 on 12/11/00 at 14:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY01-Z = 'Y'
               MOVE WK01-TEXT-SY01 TO DCSD-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0A-Z = 'Y'
               MOVE WK01-TEXT-SY0A TO CNFDL-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0B-Z = 'Y'
               MOVE WK01-TEXT-SY0B TO HOLDS-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH24-Z = 'Y'
               MOVE WK01-CODE-CH24 TO ACTN-CODE-CH24 OF FCHWM24
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-LAST-NINE-4056-CH24-Z = 'Y'
               MOVE WK01-LAST-NINE-4056-CH24 TO
                AGNCY-LAST-NINE-4056-CH24 OF FCHWK24
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AGNCY-LAST-NINE-CH24-Z = 'Y'
               MOVE WK01-AGNCY-LAST-NINE-CH24 TO
                PRED-AGNCY-LAST-NINE-CH24 OF FCHWM24
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6311-CH24-Z = 'Y'
               MOVE WK01-KEY-6311-CH24 TO NAME-KEY-6311-CH24 OF FCHWK24
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-KEY-CH24-Z = 'Y'
               MOVE WK01-NAME-KEY-CH24 TO PRED-NAME-KEY-CH24 OF FCHWM24
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AGNCY-LAST-NINE-CH2A11-Z = 'Y'
               MOVE WK01-AGNCY-LAST-NINE-CH2A11 TO
                HIER-AGNCY-LAST-NINE-CH24 OF FCHWM24(0001)
           END-IF.
           MOVE WK01-AGNCY-LAST-NINE-CH2A11-F TO
                WK01-AGNCY-LAST-NINE-CH2A1-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-AGNCY-LAST-NINE-CH2A12-Z = 'Y'
               MOVE WK01-AGNCY-LAST-NINE-CH2A12 TO
                HIER-AGNCY-LAST-NINE-CH24 OF FCHWM24(0002)
           END-IF.
           MOVE WK01-AGNCY-LAST-NINE-CH2A12-F TO
                WK01-AGNCY-LAST-NINE-CH2A1-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-AGNCY-LAST-NINE-CH2A13-Z = 'Y'
               MOVE WK01-AGNCY-LAST-NINE-CH2A13 TO
                HIER-AGNCY-LAST-NINE-CH24 OF FCHWM24(0003)
           END-IF.
           MOVE WK01-AGNCY-LAST-NINE-CH2A13-F TO
                WK01-AGNCY-LAST-NINE-CH2A1-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-AGNCY-LAST-NINE-CH2A14-Z = 'Y'
               MOVE WK01-AGNCY-LAST-NINE-CH2A14 TO
                HIER-AGNCY-LAST-NINE-CH24 OF FCHWM24(0004)
           END-IF.
           MOVE WK01-AGNCY-LAST-NINE-CH2A14-F TO
                WK01-AGNCY-LAST-NINE-CH2A1-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-AGNCY-LAST-NINE-CH2A15-Z = 'Y'
               MOVE WK01-AGNCY-LAST-NINE-CH2A15 TO
                HIER-AGNCY-LAST-NINE-CH24 OF FCHWM24(0005)
           END-IF.
           MOVE WK01-AGNCY-LAST-NINE-CH2A15-F TO
                WK01-AGNCY-LAST-NINE-CH2A1-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-KEY-CH2A11-Z = 'Y'
               MOVE WK01-NAME-KEY-CH2A11 TO HIER-NAME-KEY-CH24 OF
                FCHWM24(0001)
           END-IF.
           MOVE WK01-NAME-KEY-CH2A11-F TO WK01-NAME-KEY-CH2A1-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-KEY-CH2A12-Z = 'Y'
               MOVE WK01-NAME-KEY-CH2A12 TO HIER-NAME-KEY-CH24 OF
                FCHWM24(0002)
           END-IF.
           MOVE WK01-NAME-KEY-CH2A12-F TO WK01-NAME-KEY-CH2A1-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-KEY-CH2A13-Z = 'Y'
               MOVE WK01-NAME-KEY-CH2A13 TO HIER-NAME-KEY-CH24 OF
                FCHWM24(0003)
           END-IF.
           MOVE WK01-NAME-KEY-CH2A13-F TO WK01-NAME-KEY-CH2A1-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-KEY-CH2A14-Z = 'Y'
               MOVE WK01-NAME-KEY-CH2A14 TO HIER-NAME-KEY-CH24 OF
                FCHWM24(0004)
           END-IF.
           MOVE WK01-NAME-KEY-CH2A14-F TO WK01-NAME-KEY-CH2A1-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-NAME-KEY-CH2A15-Z = 'Y'
               MOVE WK01-NAME-KEY-CH2A15 TO HIER-NAME-KEY-CH24 OF
                FCHWM24(0005)
           END-IF.
           MOVE WK01-NAME-KEY-CH2A15-F TO WK01-NAME-KEY-CH2A1-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4056-CH24-Z = 'Y'
               MOVE WK01-DATE-4056-CH24 TO ACTVY-DATE-4056-CH24 OF
                FCHWM24
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-4056-CH24-Z = 'Y'
               MOVE WK01-NAME-4056-CH24 TO CNTCT-NAME-4056-CH24 OF
                FCHWM24
           END-IF.
