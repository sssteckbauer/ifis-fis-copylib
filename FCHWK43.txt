      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWK43                              04/24/00  12:25  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWK43.
           02  COA-CODE-4045-CH43                       PIC X(1).
           02  MSTR-COA-CODE-4045-CH43                  PIC X(1).
           02  ACCT-INDX-CODE-4045-CH43                 PIC X(7).
           02  MSTR-ACCT-INDX-CODE-4045-CH43            PIC X(7).
           02  ACCT-INDX-TITLE-4067-CH43                PIC X(35).
           02  MSTR-ACCT-INDX-TITLE-4067-CH43           PIC X(35).
           02  START-DATE-4067-CH43                     PIC X(6).
