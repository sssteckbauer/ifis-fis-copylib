      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM70                              04/24/00  12:19  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM70.
           02  ACTN-CODE-GROUP-CH70.
               03  ACTN-CODE-CH70           OCCURS 11   PIC X(1).
           02  COA-CODE-4006-CH70           OCCURS 11   PIC X(1).
           02  LCTN-CODE-4006-CH70          OCCURS 11   PIC X(6).
           02  LCTN-TITLE-4007-CH70         OCCURS 11   PIC X(35).
           02  STATUS-4007-CH70             OCCURS 11   PIC X(1).
           02  START-DATE-4007-CH70         OCCURS 11   PIC X(6).
           02  END-DATE-4007-CH70           OCCURS 11   PIC X(6).
