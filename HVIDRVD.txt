       975-TO-R4095-RCVNG-DTL SECTION.
           MOVE RVD-USER-CD TO
               USER-CODE-4095 OF R4095-RCVNG-DTL
                                                                     .
           MOVE RVD-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4095 OF R4095-RCVNG-DTL
                                                                     .
           MOVE RVD-QTY-RCVD TO
               QTY-RCVD-4095 OF R4095-RCVNG-DTL
                                                                     .
           MOVE RVD-QTY-RJCTD TO
               QTY-RJCTD-4095 OF R4095-RCVNG-DTL
                                                                     .
           MOVE RVD-ITEM-NBR TO
               ITEM-NMBR-4095 OF R4095-RCVNG-DTL
                                                                     .
           MOVE RVD-CMDTY-CD TO
               CMDTY-CODE-4095 OF R4095-RCVNG-DTL
                                                                     .
           MOVE RVD-CMDTY-DESC TO
               CMDTY-DESC-4095 OF R4095-RCVNG-DTL
                                                                     .
       975-TO-R4095-RCVNG-DTL-EXIT.
           EXIT.
