       975-TO-R4063-PAY-EFCTV SECTION.
           MOVE PYE-USER-CD TO
               USER-CODE-4063 OF R4063-PAY-EFCTV
                                                                     .
           MOVE PYE-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4063 OF R4063-PAY-EFCTV
                                                                     .
           MOVE PYE-START-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               START-DATE-4063 OF R4063-PAY-EFCTV
                                                                     .
           MOVE PYE-TIME-STMP TO
               DBLINKX-INPUT-PARM
           MOVE '4' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-TIME-FORMAT
           MOVE '51' TO DBLINKX-IDMS-TIME-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-PARM TO
               TIME-STAMP-4063 OF R4063-PAY-EFCTV
                                                                     .
           MOVE PYE-END-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               END-DATE-4063 OF R4063-PAY-EFCTV
                                                                     .
           MOVE PYE-STATUS TO
               STATUS-4063 OF R4063-PAY-EFCTV
                                                                     .
           MOVE PYE-PAY-PRCS-IND TO
               PAY-PRCS-IND-4063 OF R4063-PAY-EFCTV
                                                                     .
           MOVE PYE-PAY-MTHD-DESC TO
               PAY-MTHD-DESC-4063 OF R4063-PAY-EFCTV
                                                                     .
           MOVE PYE-RGN TO
               RGN-4063 OF R4063-PAY-EFCTV
                                                                     .
           MOVE PYE-PAN-ID TO
               PAN-ID-4063 OF R4063-PAY-EFCTV
                                                                     .
           MOVE PYE-FDRL-EMPLR-ID TO
               FDRL-EMPLR-ID-4063 OF R4063-PAY-EFCTV
                                                                     .
           MOVE PYE-PAY-PRD TO
               PAY-PRD-4063 OF R4063-PAY-EFCTV
                                                                     .
           MOVE PYE-PIN TO
               PIN-4063 OF R4063-PAY-EFCTV
                                                                     .
           MOVE PYE-PMS-CD TO
               PMS-CODE-4063 OF R4063-PAY-EFCTV
                                                                     .
           MOVE PYE-ADR-TYP-CD TO
               ADR-TYPE-CODE-4063 OF R4063-PAY-EFCTV
                                                                     .
           MOVE PYE-BANK-ACCT-CD TO
               BANK-ACCT-CODE-4063 OF R4063-PAY-EFCTV
                                                                     .
           MOVE PYE-AGNCY-IREF-ID TO
               AGNCY-INTRL-REF-ID-4063 OF R4063-PAY-EFCTV
                                                                     .
       975-TO-R4063-PAY-EFCTV-EXIT.
           EXIT.
