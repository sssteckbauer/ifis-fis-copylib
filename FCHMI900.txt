    ***Created by Convert/DC version V8R03 on 12/11/00 at 10:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH901-Z = 'Y'
               MOVE WK01-CODE-CH901 TO ACTN-CODE-CH90 OF FCHWM90(0001)
           END-IF.
           MOVE WK01-CODE-CH901-F TO WK01-CODE-CH90-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-4046-CH901-Z = 'Y'
               MOVE WK01-4046-CH901 TO STATUS-4046-CH90 OF FCHWM90(0001)
           END-IF.
           MOVE WK01-4046-CH901-F TO WK01-4046-CH90-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-BANK-ID-LAST-NN-CH90-Z = 'Y'
               MOVE WK01-BANK-ID-LAST-NN-CH90 TO
                RQST-BANK-ID-LAST-NINE-CH90 OF FCHWK90
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH902-Z = 'Y'
               MOVE WK01-CODE-CH902 TO ACTN-CODE-CH90 OF FCHWM90(0002)
           END-IF.
           MOVE WK01-CODE-CH902-F TO WK01-CODE-CH90-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH903-Z = 'Y'
               MOVE WK01-CODE-CH903 TO ACTN-CODE-CH90 OF FCHWM90(0003)
           END-IF.
           MOVE WK01-CODE-CH903-F TO WK01-CODE-CH90-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH904-Z = 'Y'
               MOVE WK01-CODE-CH904 TO ACTN-CODE-CH90 OF FCHWM90(0004)
           END-IF.
           MOVE WK01-CODE-CH904-F TO WK01-CODE-CH90-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH905-Z = 'Y'
               MOVE WK01-CODE-CH905 TO ACTN-CODE-CH90 OF FCHWM90(0005)
           END-IF.
           MOVE WK01-CODE-CH905-F TO WK01-CODE-CH90-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH906-Z = 'Y'
               MOVE WK01-CODE-CH906 TO ACTN-CODE-CH90 OF FCHWM90(0006)
           END-IF.
           MOVE WK01-CODE-CH906-F TO WK01-CODE-CH90-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH907-Z = 'Y'
               MOVE WK01-CODE-CH907 TO ACTN-CODE-CH90 OF FCHWM90(0007)
           END-IF.
           MOVE WK01-CODE-CH907-F TO WK01-CODE-CH90-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH908-Z = 'Y'
               MOVE WK01-CODE-CH908 TO ACTN-CODE-CH90 OF FCHWM90(0008)
           END-IF.
           MOVE WK01-CODE-CH908-F TO WK01-CODE-CH90-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH909-Z = 'Y'
               MOVE WK01-CODE-CH909 TO ACTN-CODE-CH90 OF FCHWM90(0009)
           END-IF.
           MOVE WK01-CODE-CH909-F TO WK01-CODE-CH90-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH9010-Z = 'Y'
               MOVE WK01-CODE-CH9010 TO ACTN-CODE-CH90 OF FCHWM90(0010)
           END-IF.
           MOVE WK01-CODE-CH9010-F TO WK01-CODE-CH90-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH9011-Z = 'Y'
               MOVE WK01-CODE-CH9011 TO ACTN-CODE-CH90 OF FCHWM90(0011)
           END-IF.
           MOVE WK01-CODE-CH9011-F TO WK01-CODE-CH90-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH9012-Z = 'Y'
               MOVE WK01-CODE-CH9012 TO ACTN-CODE-CH90 OF FCHWM90(0012)
           END-IF.
           MOVE WK01-CODE-CH9012-F TO WK01-CODE-CH90-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH9013-Z = 'Y'
               MOVE WK01-CODE-CH9013 TO ACTN-CODE-CH90 OF FCHWM90(0013)
           END-IF.
           MOVE WK01-CODE-CH9013-F TO WK01-CODE-CH90-F (13).
      *%--------------------------------------------------------------%*
           IF WK01-4046-CH902-Z = 'Y'
               MOVE WK01-4046-CH902 TO STATUS-4046-CH90 OF FCHWM90(0002)
           END-IF.
           MOVE WK01-4046-CH902-F TO WK01-4046-CH90-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-4046-CH903-Z = 'Y'
               MOVE WK01-4046-CH903 TO STATUS-4046-CH90 OF FCHWM90(0003)
           END-IF.
           MOVE WK01-4046-CH903-F TO WK01-4046-CH90-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-4046-CH904-Z = 'Y'
               MOVE WK01-4046-CH904 TO STATUS-4046-CH90 OF FCHWM90(0004)
           END-IF.
           MOVE WK01-4046-CH904-F TO WK01-4046-CH90-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-4046-CH905-Z = 'Y'
               MOVE WK01-4046-CH905 TO STATUS-4046-CH90 OF FCHWM90(0005)
           END-IF.
           MOVE WK01-4046-CH905-F TO WK01-4046-CH90-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-4046-CH906-Z = 'Y'
               MOVE WK01-4046-CH906 TO STATUS-4046-CH90 OF FCHWM90(0006)
           END-IF.
           MOVE WK01-4046-CH906-F TO WK01-4046-CH90-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-4046-CH907-Z = 'Y'
               MOVE WK01-4046-CH907 TO STATUS-4046-CH90 OF FCHWM90(0007)
           END-IF.
           MOVE WK01-4046-CH907-F TO WK01-4046-CH90-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-4046-CH908-Z = 'Y'
               MOVE WK01-4046-CH908 TO STATUS-4046-CH90 OF FCHWM90(0008)
           END-IF.
           MOVE WK01-4046-CH908-F TO WK01-4046-CH90-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-4046-CH909-Z = 'Y'
               MOVE WK01-4046-CH909 TO STATUS-4046-CH90 OF FCHWM90(0009)
           END-IF.
           MOVE WK01-4046-CH909-F TO WK01-4046-CH90-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-4046-CH9010-Z = 'Y'
               MOVE WK01-4046-CH9010 TO STATUS-4046-CH90 OF
                FCHWM90(0010)
           END-IF.
           MOVE WK01-4046-CH9010-F TO WK01-4046-CH90-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-4046-CH9011-Z = 'Y'
               MOVE WK01-4046-CH9011 TO STATUS-4046-CH90 OF
                FCHWM90(0011)
           END-IF.
           MOVE WK01-4046-CH9011-F TO WK01-4046-CH90-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-4046-CH9012-Z = 'Y'
               MOVE WK01-4046-CH9012 TO STATUS-4046-CH90 OF
                FCHWM90(0012)
           END-IF.
           MOVE WK01-4046-CH9012-F TO WK01-4046-CH90-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-4046-CH9013-Z = 'Y'
               MOVE WK01-4046-CH9013 TO STATUS-4046-CH90 OF
                FCHWM90(0013)
           END-IF.
           MOVE WK01-4046-CH9013-F TO WK01-4046-CH90-F (13).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NN-4046-CH901-Z = 'Y'
               MOVE WK01-ID-LAST-NN-4046-CH901 TO
                BANK-ID-LAST-NINE-4046-CH90 OF FCHWM90(0001)
           END-IF.
           MOVE WK01-ID-LAST-NN-4046-CH901-F TO
                WK01-ID-LAST-NN-4046-CH90-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6311-CH901-Z = 'Y'
               MOVE WK01-KEY-6311-CH901 TO NAME-KEY-6311-CH90 OF
                FCHWM90(0001)
           END-IF.
           MOVE WK01-KEY-6311-CH901-F TO WK01-KEY-6311-CH90-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NN-4046-CH902-Z = 'Y'
               MOVE WK01-ID-LAST-NN-4046-CH902 TO
                BANK-ID-LAST-NINE-4046-CH90 OF FCHWM90(0002)
           END-IF.
           MOVE WK01-ID-LAST-NN-4046-CH902-F TO
                WK01-ID-LAST-NN-4046-CH90-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NN-4046-CH903-Z = 'Y'
               MOVE WK01-ID-LAST-NN-4046-CH903 TO
                BANK-ID-LAST-NINE-4046-CH90 OF FCHWM90(0003)
           END-IF.
           MOVE WK01-ID-LAST-NN-4046-CH903-F TO
                WK01-ID-LAST-NN-4046-CH90-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NN-4046-CH904-Z = 'Y'
               MOVE WK01-ID-LAST-NN-4046-CH904 TO
                BANK-ID-LAST-NINE-4046-CH90 OF FCHWM90(0004)
           END-IF.
           MOVE WK01-ID-LAST-NN-4046-CH904-F TO
                WK01-ID-LAST-NN-4046-CH90-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NN-4046-CH905-Z = 'Y'
               MOVE WK01-ID-LAST-NN-4046-CH905 TO
                BANK-ID-LAST-NINE-4046-CH90 OF FCHWM90(0005)
           END-IF.
           MOVE WK01-ID-LAST-NN-4046-CH905-F TO
                WK01-ID-LAST-NN-4046-CH90-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NN-4046-CH906-Z = 'Y'
               MOVE WK01-ID-LAST-NN-4046-CH906 TO
                BANK-ID-LAST-NINE-4046-CH90 OF FCHWM90(0006)
           END-IF.
           MOVE WK01-ID-LAST-NN-4046-CH906-F TO
                WK01-ID-LAST-NN-4046-CH90-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NN-4046-CH907-Z = 'Y'
               MOVE WK01-ID-LAST-NN-4046-CH907 TO
                BANK-ID-LAST-NINE-4046-CH90 OF FCHWM90(0007)
           END-IF.
           MOVE WK01-ID-LAST-NN-4046-CH907-F TO
                WK01-ID-LAST-NN-4046-CH90-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NN-4046-CH908-Z = 'Y'
               MOVE WK01-ID-LAST-NN-4046-CH908 TO
                BANK-ID-LAST-NINE-4046-CH90 OF FCHWM90(0008)
           END-IF.
           MOVE WK01-ID-LAST-NN-4046-CH908-F TO
                WK01-ID-LAST-NN-4046-CH90-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NN-4046-CH909-Z = 'Y'
               MOVE WK01-ID-LAST-NN-4046-CH909 TO
                BANK-ID-LAST-NINE-4046-CH90 OF FCHWM90(0009)
           END-IF.
           MOVE WK01-ID-LAST-NN-4046-CH909-F TO
                WK01-ID-LAST-NN-4046-CH90-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NN-4046-CH9010-Z = 'Y'
               MOVE WK01-ID-LAST-NN-4046-CH9010 TO
                BANK-ID-LAST-NINE-4046-CH90 OF FCHWM90(0010)
           END-IF.
           MOVE WK01-ID-LAST-NN-4046-CH9010-F TO
                WK01-ID-LAST-NN-4046-CH90-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NN-4046-CH9011-Z = 'Y'
               MOVE WK01-ID-LAST-NN-4046-CH9011 TO
                BANK-ID-LAST-NINE-4046-CH90 OF FCHWM90(0011)
           END-IF.
           MOVE WK01-ID-LAST-NN-4046-CH9011-F TO
                WK01-ID-LAST-NN-4046-CH90-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NN-4046-CH9012-Z = 'Y'
               MOVE WK01-ID-LAST-NN-4046-CH9012 TO
                BANK-ID-LAST-NINE-4046-CH90 OF FCHWM90(0012)
           END-IF.
           MOVE WK01-ID-LAST-NN-4046-CH9012-F TO
                WK01-ID-LAST-NN-4046-CH90-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NN-4046-CH9013-Z = 'Y'
               MOVE WK01-ID-LAST-NN-4046-CH9013 TO
                BANK-ID-LAST-NINE-4046-CH90 OF FCHWM90(0013)
           END-IF.
           MOVE WK01-ID-LAST-NN-4046-CH9013-F TO
                WK01-ID-LAST-NN-4046-CH90-F (13).
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6311-CH902-Z = 'Y'
               MOVE WK01-KEY-6311-CH902 TO NAME-KEY-6311-CH90 OF
                FCHWM90(0002)
           END-IF.
           MOVE WK01-KEY-6311-CH902-F TO WK01-KEY-6311-CH90-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6311-CH903-Z = 'Y'
               MOVE WK01-KEY-6311-CH903 TO NAME-KEY-6311-CH90 OF
                FCHWM90(0003)
           END-IF.
           MOVE WK01-KEY-6311-CH903-F TO WK01-KEY-6311-CH90-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6311-CH904-Z = 'Y'
               MOVE WK01-KEY-6311-CH904 TO NAME-KEY-6311-CH90 OF
                FCHWM90(0004)
           END-IF.
           MOVE WK01-KEY-6311-CH904-F TO WK01-KEY-6311-CH90-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6311-CH905-Z = 'Y'
               MOVE WK01-KEY-6311-CH905 TO NAME-KEY-6311-CH90 OF
                FCHWM90(0005)
           END-IF.
           MOVE WK01-KEY-6311-CH905-F TO WK01-KEY-6311-CH90-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6311-CH906-Z = 'Y'
               MOVE WK01-KEY-6311-CH906 TO NAME-KEY-6311-CH90 OF
                FCHWM90(0006)
           END-IF.
           MOVE WK01-KEY-6311-CH906-F TO WK01-KEY-6311-CH90-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6311-CH907-Z = 'Y'
               MOVE WK01-KEY-6311-CH907 TO NAME-KEY-6311-CH90 OF
                FCHWM90(0007)
           END-IF.
           MOVE WK01-KEY-6311-CH907-F TO WK01-KEY-6311-CH90-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6311-CH908-Z = 'Y'
               MOVE WK01-KEY-6311-CH908 TO NAME-KEY-6311-CH90 OF
                FCHWM90(0008)
           END-IF.
           MOVE WK01-KEY-6311-CH908-F TO WK01-KEY-6311-CH90-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6311-CH909-Z = 'Y'
               MOVE WK01-KEY-6311-CH909 TO NAME-KEY-6311-CH90 OF
                FCHWM90(0009)
           END-IF.
           MOVE WK01-KEY-6311-CH909-F TO WK01-KEY-6311-CH90-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6311-CH9010-Z = 'Y'
               MOVE WK01-KEY-6311-CH9010 TO NAME-KEY-6311-CH90 OF
                FCHWM90(0010)
           END-IF.
           MOVE WK01-KEY-6311-CH9010-F TO WK01-KEY-6311-CH90-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6311-CH9011-Z = 'Y'
               MOVE WK01-KEY-6311-CH9011 TO NAME-KEY-6311-CH90 OF
                FCHWM90(0011)
           END-IF.
           MOVE WK01-KEY-6311-CH9011-F TO WK01-KEY-6311-CH90-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6311-CH9012-Z = 'Y'
               MOVE WK01-KEY-6311-CH9012 TO NAME-KEY-6311-CH90 OF
                FCHWM90(0012)
           END-IF.
           MOVE WK01-KEY-6311-CH9012-F TO WK01-KEY-6311-CH90-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6311-CH9013-Z = 'Y'
               MOVE WK01-KEY-6311-CH9013 TO NAME-KEY-6311-CH90 OF
                FCHWM90(0013)
           END-IF.
           MOVE WK01-KEY-6311-CH9013-F TO WK01-KEY-6311-CH90-F (13).
