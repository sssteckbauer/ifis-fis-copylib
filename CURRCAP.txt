      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4032-COST-APPL' TO DBLINK-CURR-PARAGRAPH.    276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4032-COST-APPL' TO RECORD-NAME.                       276 BRTN
YYY991     MOVE 'F-COST-SHARE' TO AREA-NAME.                            276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4032-COST-APPL TO DBLINK-RECORD-MADE-CURRENT.   276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4032-COST-APPL-EXIT                      276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE CAP-FK-CSE-COST-SHR-CD TO DBLINK-R4032-COST-APPL-01 276 BRTN
YYY991         MOVE CAP-FK-CSE-START-DT TO DBLINK-R4032-COST-APPL-02    276 BRTN
YYY991         MOVE CAP-FK-CSE-TIME-STMP TO DBLINK-R4032-COST-APPL-03   276 BRTN
YYY991         MOVE CAP-COST-SHR-FRM-ACCT TO DBLINK-R4032-COST-APPL-04  276 BRTN
YYY991         MOVE CAP-CSE-SET-TS TO DBLINK-R4032-COST-APPL-05         276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4032-COST-APPL TO DBLINK-VIEW-NAME-CURRENT.     276 BRTN
YYY991     MOVE DBLINK-R4032-COST-APPL-KEY TO DBLINK-VIEW-200-CURRENT.  276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4064-4032                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE CAP-FK-CSE-COST-SHR-CD TO DBLINK-S-4064-4032-01.        276 BRTN
YYY991     MOVE CAP-FK-CSE-START-DT TO DBLINK-S-4064-4032-02.           276 BRTN
YYY991     MOVE CAP-FK-CSE-TIME-STMP TO DBLINK-S-4064-4032-03.          276 BRTN
YYY991     MOVE CAP-COST-SHR-FRM-ACCT TO DBLINK-S-4064-4032-04.         276 BRTN
YYY991     MOVE CAP-CSE-SET-TS TO DBLINK-S-4064-4032-05.                276 BRTN
