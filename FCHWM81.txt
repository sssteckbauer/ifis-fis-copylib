      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM81                              04/24/00  12:19  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM81.
           02  ACTN-CODE-GROUP-CH81.
               03  ACTN-CODE-CH81           OCCURS 11   PIC X(1).
           02  COA-CODE-4014-CH81           OCCURS 11   PIC X(1).
           02  ACCT-TYPE-CODE-4014-CH81     OCCURS 11   PIC X(2).
           02  ACCT-TYPE-DESC-4015-CH81     OCCURS 11   PIC X(35).
           02  STATUS-4015-CH81             OCCURS 11   PIC X(1).
           02  START-DATE-4015-CH81         OCCURS 11   PIC X(6).
           02  END-DATE-4015-CH81           OCCURS 11   PIC X(6).
