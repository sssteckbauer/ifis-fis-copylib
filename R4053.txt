       01  R4053-INTR-ENTY.
           02  DB-PROC-ID-4053.
               03  USER-CODE-4053                       PIC X(8).
               03  LAST-ACTVY-DATE-4053                 PIC X(5).
               03  TRMNL-ID-4053                        PIC X(8).
               03  PURGE-FLAG-4053                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  INTRL-ENTY-KEY-4053.
               03  UNVRS-CODE-4053                      PIC X(2).
               03  COA-CODE-4053                        PIC X(1).
               03  INTRL-ENTY-CODE-4053                 PIC X(4).
           02  ACTVY-DATE-4053                COMP-3    PIC S9(8).
           02  INTRL-ENTY-DESC-4053                     PIC X(35).
