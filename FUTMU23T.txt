       01  MAP-CONTROL.

    ***Created by Convert/DC version V8R03 on 12/05/00 at 10:00***

           02  FUTMU23-MAP-CONTROL.
           03  FUTMU23T.
           04  WK01-DCXMAPL-PARMS.
               10  WK01-MAPNAME                  PIC X(8)
                                                 VALUE 'FUTMU23I'.
               10  WK01-IKE-LENGTH               PIC S9(8)  COMP
                                                 VALUE 6026.
               10  WK01-TINA-LENGTH              PIC S9(4)  COMP
                                                 VALUE 588.
           05  WK01-ATTRIBUTES                   PIC X(60)
                                                 VALUE SPACE.
           05  WK01-ATTRIBUTES-ERROR             PIC X(60) VALUE
               'U  BIF'.
           05  WK01-CURSOR-DFLD                  PIC X(36)
                                                 VALUE SPACES.
           05  WK01-CURSOR-DFLD-QUAL             PIC X(30)
                                                 VALUE SPACES.
           05  WK01-ROW                          PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-COLUMN                       PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-ERROR-MSG                    PIC X(80)
                                                 VALUE SPACES.
           05  WK01-MAP-DATA-LEN                 PIC S9(6)
                                                 VALUE ZERO.
           05  WK01-1ST-FLAG                     PIC X(1)
                                                 VALUE 'Y'.
           05  WK01-FLAG-OUTPUT-DATA             PIC X(1)
                                                 VALUE 'Y'.
      *    05  WK01-STATUS-RETURN-FLAG           PIC X(1)
      *                                          VALUE SPACES.
           05  WK01-ALL-BUT-FLAG                 PIC X(1)
                                                 VALUE SPACES.
           05  WK01-FLAG-ALL-FIELDS-CHANGED      PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-CHANGED      PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERROR        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERROR        PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.

           05  WK01-VARDATA                      PIC X(11880)
                                                 VALUE SPACES.
           05  FILLER                            PIC X(100)
                                                 VALUE SPACES.
           04  WK01-ELEMENT-AREA.

               10  WK01-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-6001-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00       PIC X(35)
                                               VALUE SPACES.
               10  WK01-DATE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-HH-MM-TIME-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00       PIC X(4)
                                               VALUE SPACES.
               10  WK01-PM-FLAG-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-USER-ID-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID       PIC X(32)
                                               VALUE SPACES.
               10  WK01-RSPNS-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-FUNC-DESCRIPTION-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION       PIC X(28)
                                               VALUE SPACES.
               10  WK01-STATUS-CODE-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-ID-UT23-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-UT23-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-UT23-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-UT23-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-UT23-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-UT23       PIC X(8)
                                               VALUE SPACES.
               10  WK01-FULL-NAME-UT23-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FULL-NAME-UT23-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FULL-NAME-UT23-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FULL-NAME-UT23-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FULL-NAME-UT23-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FULL-NAME-UT23       PIC X(35)
                                               VALUE SPACES.
               10  WK01-ID-UT2A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-UT2A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-UT2A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-UT2A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-UT2A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-UT2A       PIC X(8)
                                               VALUE SPACES.
               10  WK01-ID-FULL-NAME-UT23-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-FULL-NAME-UT23-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-FULL-NAME-UT23-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-FULL-NAME-UT23-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-FULL-NAME-UT23-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-FULL-NAME-UT23       PIC X(35)
                                               VALUE SPACES.
               10  WK01-DCMNT-TYPE-UT23-Z      PIC X(1)
                                               VALUE SPACE.
               10  WK01-DCMNT-TYPE-UT23-F      PIC X(1)
                                               VALUE SPACE.
               10  WK01-DCMNT-TYPE-UT23-A      PIC X(1)
                                               VALUE SPACE.
               10  WK01-DCMNT-TYPE-UT23-V      PIC X(1)
                                               VALUE SPACE.
               10  WK01-DCMNT-TYPE-UT23-O      PIC X(1)
                                               VALUE SPACE.
               10  WK01-DCMNT-TYPE-UT23 PIC X(3)
                                               VALUE SPACES.
               10  WK01-DCMNT-DESC-UT23-Z             PIC X(1)
                                               VALUE SPACE.
               10  WK01-DCMNT-DESC-UT23-F             PIC X(1)
                                               VALUE SPACE.
               10  WK01-DCMNT-DESC-UT23-A             PIC X(1)
                                               VALUE SPACE.
               10  WK01-DCMNT-DESC-UT23-V             PIC X(1)
                                               VALUE SPACE.
               10  WK01-DCMNT-DESC-UT23-O             PIC X(1)
                                               VALUE SPACE.
               10  WK01-DCMNT-DESC-UT23      PIC X(35)
                                               VALUE SPACES.
               10  WK01-TMPLT-CODE-UT23-Z      PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-CODE-UT23-F      PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-CODE-UT23-A      PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-CODE-UT23-V      PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-CODE-UT23-O      PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-CODE-UT23 PIC X(3)
                                               VALUE SPACES.
               10  WK01-TMPLT-DESC-UT23-Z             PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-DESC-UT23-F             PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-DESC-UT23-A             PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-DESC-UT23-V             PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-DESC-UT23-O             PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-DESC-UT23      PIC X(35)
                                               VALUE SPACES.
               10  WK01-IND-UT23-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-UT23-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-UT23-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-UT23-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-UT23-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-UT23       PIC X(1)
                                               VALUE SPACES.
               10  WK01-ZZMESSAGE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE       PIC X(160)
                                               VALUE SPACES.
               10  WK01-PASSED-ONE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE       PIC X(32)
                                               VALUE SPACES.
               10  WK01-MAP-RESPONSE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-TEXT-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00       PIC X(14)
                                               VALUE SPACES.
