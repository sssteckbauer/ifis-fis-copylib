       975-TO-R4017-BC-EFCTV SECTION.
           MOVE BCE-USER-CD TO
               USER-CODE-4017 OF R4017-BC-EFCTV
                                                                     .
           MOVE BCE-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4017 OF R4017-BC-EFCTV
                                                                     .
           MOVE BCE-START-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               START-DATE-4017 OF R4017-BC-EFCTV
                                                                     .
           MOVE BCE-TIME-STMP TO
               DBLINKX-INPUT-PARM
           MOVE '4' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-TIME-FORMAT
           MOVE '51' TO DBLINKX-IDMS-TIME-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-PARM TO
               TIME-STAMP-4017 OF R4017-BC-EFCTV
                                                                     .
           MOVE BCE-END-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               END-DATE-4017 OF R4017-BC-EFCTV
                                                                     .
           MOVE BCE-STATUS TO
               STATUS-4017 OF R4017-BC-EFCTV
                                                                     .
           MOVE BCE-CNTL-FUND TO
               CNTRL-FUND-4017 OF R4017-BC-EFCTV
                                                                     .
           MOVE BCE-CNTL-ORGN TO
               CNTRL-ORGZN-4017 OF R4017-BC-EFCTV
                                                                     .
           MOVE BCE-CNTL-PRD-CD TO
               CNTRL-PRD-CODE-4017 OF R4017-BC-EFCTV
                                                                     .
           MOVE BCE-CNTL-SVRTY-CD TO
               CNTRL-SVRTY-CODE-4017 OF R4017-BC-EFCTV
                                                                     .
       975-TO-R4017-BC-EFCTV-EXIT.
           EXIT.
