      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWM32                              04/24/00  12:39  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWM32.
           02  ACTN-CODE-4072-PU32                      PIC X(1).
           02  BUYER-NAME-4072-PU32                     PIC X(35).
           02  START-DATE-4072-PU32                     PIC X(6).
           02  END-DATE-4072-PU32                       PIC X(6).
           02  ACTVY-DATE-4072-PU32                     PIC X(6).
           02  TLPHN-ID-PU32.
               03  BASIC-TLPHN-ID-PU32.
                   04  TLPHN-AREA-CODE-PU32             PIC X(3).
                   04  TLPHN-XCHNG-ID-PU32              PIC X(3).
                   04  TLPHN-SEQ-ID-PU32                PIC X(4).
               03  TLPHN-XTNSN-ID-PU32                  PIC X(4).
           02  ACTN-CODE-GROUP-4101-PU32.
               03  ACTN-CODE-4101-PU32      OCCURS 8    PIC X(1).
           02  CMDTY-CODE-4101-PU32         OCCURS 8    PIC X(8).
           02  ACTN-CODE-GROUP-4100-PU32.
               03  ACTN-CODE-4100-PU32      OCCURS 8    PIC X(1).
           02  CHART-CODE-4100-PU32         OCCURS 8    PIC X(1).
           02  ORGZN-CODE-4100-PU32         OCCURS 8    PIC X(6).
           02  ACTN-CODE-GROUP-4138-PU32.
               03  ACTN-CODE-4138-PU32      OCCURS 8    PIC X(1).
           02  PO-CLASS-CODE-4138-PU32      OCCURS 8    PIC X(1).
           02  MAX-AMT-4084-PU32            OCCURS 8    PIC X(12).
           02  RQST-CMDTY-CODE-PU32                     PIC X(8).
           02  RQST-COA-CODE-PU32                       PIC X(1).
           02  RQST-ORGZN-CODE-PU32                     PIC X(6).
           02  RQST-PO-CLASS-CODE-PU32                  PIC X(1).
           02  BUYER-PID-PU32.
               03  BUYER-ID-DIGIT-ONE-PU32              PIC X(1).
               03  BUYER-ID-LAST-NINE-PU32              PIC X(9).
           02  EMPID-PU32                               PIC X(09).
           02  TAXID-PU32                               PIC X(09).
           02  EMP-STATUS-PU32                          PIC X(25).
           02  BUYER-EMAIL-ADR-PU32                     PIC X(35).
