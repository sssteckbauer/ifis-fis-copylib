      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWD93                              04/24/00  13:00  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWD93.
           02  SAVE-LCTN-CODE-CH93          OCCURS 5    PIC X(6).
           02  SAVE-LCTN-TITLE-CH93         OCCURS 5    PIC X(35).
           02  LEVEL-CNTR-CH93                          PIC 9(1).
           02  LOOP-FLAG-CH93                           PIC X(1).
           02  START-DATE-CH93                COMP-3    PIC S9(8).
           02  SAVE-DBKEY-ID-4007-CH93        COMP      PIC S9(8).
