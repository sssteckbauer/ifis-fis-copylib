      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPI28C                              05/02/12         *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPI28C.
           02  INDX-KEY-WORK-AP28                       PIC X(30).
           02  INTRL-REF-ID-WORK-AP28         COMP-3    PIC S9(7).
           02  WORK-DATE-AP28                 COMP-3    PIC S9(8).
           02  SYSTEM-DATE-AP28               COMP-3    PIC S9(8).
           02  SET-SORT-KEY-4012-AP28.
               03  OPTN-1-CODE-AP28                     PIC X(8).
               03  OPTN-2-CODE-AP28                     PIC X(8).
               03  LEVEL-NMBR-AP28                      PIC 9(2).
           02  RPRT-AMT-AP28-N                          PIC 9(10)V99.
           02  STATE-WTHHLD-AP28-N                      PIC 9(06)V99.
