      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FUTWK02                              04/24/00  12:50  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FUTWK02.
           02  USER-ID-UT02                             PIC X(8).
           02  DCMNT-TYPE-UT02                          PIC X(3).
           02  APRVL-TMPLT-CODE-UT02                    PIC X(3).
           02  APRVL-TMPLT-DESC-UT02                    PIC X(35).
           02  USER-NAME-UT02                           PIC X(35).
           02  DCMNT-DESC-UT02                          PIC X(35).
DEVBWS     02  SAVE-DBKEY-ID-MINUS-ONE-UT02             PIC X(17).
DEVBWS     02  SAVE-DBKEY-ID-PLUS-ONE-UT02              PIC X(17).
DEVBWS     02  WORK-DBKEY-ID-UT02                       PIC X(17).
           02  WS-LEVEL1-COPIED-IND                     PIC X(1).
