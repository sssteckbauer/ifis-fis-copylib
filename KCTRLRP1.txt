      *------------------------START KCTRLRP1 -----------------------*

       01  CONTROL-CC                        PIC S9(04) VALUE +0 COMP.

       01  CHK-CONTROL-RPT1.
           03  KCTRL1-RPT1-MISC.
               05  KCTRL1-RPT1-TITLE         PIC X(14) VALUE
                    'CONTROL REPORT'.
               05  KCTRL1-VARIABLE-TITLE     PIC X(30) VALUE SPACES.
               05  KCTRL1-RPT1-PROCS-REPORT  PIC X(17) VALUE
                    'PROCESSING REPORT'.
               05  KCTRL1-RPT1-SELEC-PARMS   PIC X(20) VALUE
                    'SELECTION PARAMETERS'.
               05  KCTRL1-RPT1-FAPXK00       PIC X(14) VALUE
                    'EXTRACT DRIVER'.
               05  KCTRL1-RPT1-FAPDK01       PIC X(24) VALUE
                    'REQUEST GENERATOR DRIVER'.
               05  KCTRL1-RPT1-FAPUK02       PIC X(14) VALUE
                    'REQUEST SERVER'.
               05  KCTRL1-RPT1-FAPCK03       PIC X(21) VALUE
                    'CHECK NUMBER ASSIGNER'.
               05  KCTRL1-RPT1-FAPDK04       PIC X(18) VALUE
                    'CHECK PRINT DRIVER'.
               05  KCTRL1-RPT1-FAPUK05       PIC X(22) VALUE
                    'DATABASE UPDATE DRIVER'.
               05  KCTRL1-RPT1-FAPUK06       PIC X(26) VALUE
                    'APPROVED RECORDS GENERATOR'.
               05  KCTRL1-RPT1-FAPUK07       PIC X(29) VALUE
                    'CHECK REGISTER EXTRACT/UPDATE'.
           03  KCTRL1-PROG-MASK.
               05  FILLER                    PIC X(05) VALUE SPACES.
               05  KCTRL1-PROG-SUFFIX        PIC X(02) VALUE SPACES.
               05  FILLER                    PIC X(01) VALUE SPACES.
           03  KCTRL1-LINE-COUNT             PIC 9(03) VALUE ZERO.
           03  KCTRL1-PAGE-COUNT             PIC 9(06) VALUE ZERO.


       01  CHK-CONTROL-RPT1-HEADINGS.
           03  CHK-CONTROL-RPT1-BANNER1.
               05  FILLER             PIC X(02) VALUE SPACES.
               05  KCTRL1-PRGRM-ID    PIC X(08).
               05  FILLER             PIC X(06) VALUE SPACES.
               05  KCTRL1-USER-ID     PIC X(08).
               05  FILLER             PIC X(27) VALUE SPACES.
               05  KCTRL1-UNVRS-NAME  PIC X(35).
               05  FILLER             PIC X(27) VALUE SPACES.
               05  KCTRL1-CURR-DATE   PIC X(08).
               05  FILLER             PIC X(02) VALUE SPACES.
               05  KCTRL1-CURR-TIME   PIC X(07).
               05  FILLER             PIC X(02) VALUE SPACES.
           03  CHK-CONTROL-RPT1-BANNER2.
               05  KCTRL1-TTL-PG-NO   PIC 9(06) VALUE ZERO.
               05  FILLER             PIC X(42) VALUE SPACES.
               05  KCTRL1-RPT-TTL     PIC X(35).
               05  FILLER             PIC X(36) VALUE SPACES.
               05  FILLER             PIC X(06) VALUE
                    'PAGE: '.
               05  KCTRL1-RPT-PG-NO   PIC ZZZ,ZZ9.
           03  CHK-CONTROL-RPT1-BANNER3.
               05  FILLER             PIC X(48) VALUE SPACES.
               05  KCTRL1-RPT-STTL    PIC X(35).
               05  FILLER             PIC X(49) VALUE SPACES.


       01  CHK-CONTROL-RPT1-PARMS.
           03  CHK-CONTROL-RPT1-LINE1.
               05  FILLER                 PIC X(07) VALUE SPACES.
               05  FILLER                 PIC X(19) VALUE
                                          'UNIVERSITY CODE   :'.
               05  FILLER                 PIC X(02) VALUE SPACES.
               05  KCTRL1-UNVRS-CODE      PIC X(02).
               05  FILLER                 PIC X(104) VALUE SPACES.
           03  CHK-CONTROL-RPT1-LINE2.
               05  FILLER                 PIC X(07) VALUE SPACES.
               05  FILLER                 PIC X(19) VALUE
                                          'USER CODE         :'.
               05  FILLER                 PIC X(02) VALUE SPACES.
               05  KCTRL1-USER-CODE       PIC X(08).
               05  FILLER                 PIC X(98) VALUE SPACES.
           03  CHK-CONTROL-RPT1-LINE3.
               05  FILLER                 PIC X(07) VALUE SPACES.
               05  FILLER                 PIC X(19) VALUE
                                          'ORIGIN CODE       :'.
               05  FILLER                 PIC X(02) VALUE SPACES.
               05  KCTRL1-ORGN-CODE       PIC X(04).
               05  FILLER                 PIC X(102) VALUE SPACES.
           03  CHK-CONTROL-RPT1-LINE4.
               05  FILLER                 PIC X(07) VALUE SPACES.
               05  FILLER                 PIC X(19) VALUE
                                          'BANK ACCOUNT CODE :'.
               05  FILLER                 PIC X(02) VALUE SPACES.
               05  KCTRL1-BANK-CODE       PIC X(02).
               05  FILLER                 PIC X(104) VALUE SPACES.
           03  CHK-CONTROL-RPT1-LINE5.
               05  FILLER                 PIC X(07) VALUE SPACES.
               05  FILLER                 PIC X(19) VALUE
                                          'CHECK RUN DATE    :'.
               05  FILLER                 PIC X(02) VALUE SPACES.
               05  KCTRL1-CHK-RUN-DATE    PIC X(08).
               05  FILLER                 PIC X(98) VALUE SPACES.
           03  CHK-CONTROL-RPT1-LINE6.
               05  FILLER                 PIC X(07) VALUE SPACES.
               05  FILLER                 PIC X(19) VALUE
                                          'STARTING CHECK NMR:'.
               05  FILLER                 PIC X(02) VALUE SPACES.
               05  KCTRL1-START-CHK-NMBR  PIC X(08).
               05  FILLER                 PIC X(98) VALUE SPACES.
           03  CHK-CONTROL-RPT1-LINE7.
               05  FILLER                 PIC X(07) VALUE SPACES.
               05  FILLER                 PIC X(12) VALUE
                                          'FICE CODE  :'.
               05  FILLER                 PIC X(02) VALUE SPACES.
               05  KCTRL1-FICE-CODE       PIC X(06).
               05  FILLER                 PIC X(107) VALUE SPACES.
           03  CHK-CONTROL-RPT1-LINE8.
               05  FILLER                 PIC X(07) VALUE SPACES.
               05  FILLER                 PIC X(12) VALUE
                                          'DICTIONARY :'.
               05  FILLER                 PIC X(02) VALUE SPACES.
               05  KCTRL1-DICT-NAME       PIC X(08).
               05  FILLER                 PIC X(107) VALUE SPACES.


       01  CHK-CONTROL-RPT1-RECORDS.
           03  CHK-CONTROL-RPT1-LINE9.
               05  FILLER                 PIC X(05) VALUE SPACES.
               05  FILLER                 PIC X(19) VALUE
                                          'PROCESSED  RECORDS:'.
               05  FILLER                 PIC X(108) VALUE SPACES.
           03  CHK-CONTROL-RPT1-LINE10.
               05  FILLER                 PIC X(10) VALUE SPACES.
               05  KCTRL1-PRCSD-REC       PIC X(20).
               05  FILLER                 PIC X(02) VALUE SPACES.
               05  KCTRL1-REC-LABEL       PIC X(10).
               05  FILLER                 PIC X(90) VALUE SPACES.


      *------------------------  END KCTRLRP1 -----------------------*
