       01  FGA007-RECORD.
           03  FGA007-SORT-KEY.
               05  FGA007-FIXED-KEY.
                   07  FGA007-UNVRS-ID         PIC X(02).
                   07  FGA007-USER-ID          PIC X(08).
                   07  FGA007-RPRT-CODE        PIC X(08).
                   07  FGA007-SEQ-NMBR         PIC 9(04).
               05  FGA007-VAR-KEY.
                   07  FGA007-COA-CODE         PIC X(01).
                   07  FGA007-FUND-CODE        PIC X(06).
                   07  FGA007-PRED-ORGZN-CODE  PIC X(06).
                   07  FGA007-ORGZN-CODE       PIC X(06).
                   07  FGA007-ACCT-TYPE-CODE   PIC X(02).
                   07  FGA007-ACCT-CODE        PIC X(06).
                   07  FGA007-PRD              PIC 9(02).
                   07  FILLER                  PIC X(28).
               05  FGA007-RCRD-TYPE            PIC X(01).
           03  FGA007-DATA                     PIC X(360).
           03  FGA007-ONE-TIME-DATA REDEFINES
               FGA007-DATA.
               05  FGA007-AS-OF-DATE           PIC 9(08)    COMP-3.
               05  FGA007-UNVRS-TITLE          PIC X(35).
               05  FGA007-USER-NAME            PIC X(35).
               05  FGA007-COA-DESC             PIC X(35).
               05  FGA007-CRNT-PRD             PIC 9(02).
               05  FGA007-PARM-FSCL-YR         PIC X(02).
               05  FGA007-PARM-PRINT-RPRT-TOT  PIC X(01).
               05  FGA007-PARM-PRINT-NET-TOT   PIC X(01).
               05  FILLER                      PIC X(244).
           03  FGA007-DETAIL-DATA REDEFINES
               FGA007-DATA.
               05  FGA007-FUND-TITLE           PIC X(35).
               05  FGA007-ORGZN-TITLE          PIC X(35).
               05  FGA007-PREDCSR-ORGZN-TITLE  PIC X(35).
               05  FGA007-ACCT-TYPE-DESC       PIC X(35).
               05  FGA007-INTRL-ACCT-TYPE-CODE PIC X(02).
               05  FGA007-PREDCSR-ACCT-TYPE    PIC X(02).
               05  FGA007-PREDCSR-ACTT-DESC    PIC X(35).
               05  FGA007-ACCT-CODE-TITLE      PIC X(35).
               05  FGA007-ADPTD-BDGT-AMT       PIC S9(11)V99 COMP-3.
               05  FGA007-BDGT-ADJMT-AMT       PIC S9(11)V99 COMP-3.
               05  FGA007-PRD-ACTVY-AMT        PIC S9(11)V99 COMP-3.
               05  FGA007-BDGT-RSRVTN-AMT      PIC S9(11)V99 COMP-3.
               05  FGA007-ENCMBRNC-AMT         PIC S9(11)V99 COMP-3.
               05  FILLER                      PIC X(111).
