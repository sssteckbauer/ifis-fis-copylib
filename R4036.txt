       01  R4036-USER-RCRD.
           02  DB-PROC-ID-4036.
               03  USER-CODE-4036                       PIC X(8).
               03  LAST-ACTVY-DATE-4036                 PIC X(5).
               03  TRMNL-ID-4036                        PIC X(8).
               03  PURGE-FLAG-4036                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  USER-RCRD-KEY-4036.
               03  UNVRS-CODE-4036                      PIC X(2).
               03  COA-CODE-4036                        PIC X(1).
               03  RCRD-NMBR-4036                       PIC 9(4).
               03  ELMNT-VALUE-4036                     PIC X(15).
