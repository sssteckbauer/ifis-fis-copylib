    ***Created by Convert/DC version V8R03 on 12/04/00 at 15:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP131-Z = 'Y'
               MOVE WK01-CODE-AP131 TO ACTN-CODE-AP13 OF FAPWM13(0001)
           END-IF.
           MOVE WK01-CODE-AP131-F TO WK01-CODE-AP13-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP132-Z = 'Y'
               MOVE WK01-CODE-AP132 TO ACTN-CODE-AP13 OF FAPWM13(0002)
           END-IF.
           MOVE WK01-CODE-AP132-F TO WK01-CODE-AP13-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP133-Z = 'Y'
               MOVE WK01-CODE-AP133 TO ACTN-CODE-AP13 OF FAPWM13(0003)
           END-IF.
           MOVE WK01-CODE-AP133-F TO WK01-CODE-AP13-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP134-Z = 'Y'
               MOVE WK01-CODE-AP134 TO ACTN-CODE-AP13 OF FAPWM13(0004)
           END-IF.
           MOVE WK01-CODE-AP134-F TO WK01-CODE-AP13-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP135-Z = 'Y'
               MOVE WK01-CODE-AP135 TO ACTN-CODE-AP13 OF FAPWM13(0005)
           END-IF.
           MOVE WK01-CODE-AP135-F TO WK01-CODE-AP13-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP136-Z = 'Y'
               MOVE WK01-CODE-AP136 TO ACTN-CODE-AP13 OF FAPWM13(0006)
           END-IF.
           MOVE WK01-CODE-AP136-F TO WK01-CODE-AP13-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP137-Z = 'Y'
               MOVE WK01-CODE-AP137 TO ACTN-CODE-AP13 OF FAPWM13(0007)
           END-IF.
           MOVE WK01-CODE-AP137-F TO WK01-CODE-AP13-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP138-Z = 'Y'
               MOVE WK01-CODE-AP138 TO ACTN-CODE-AP13 OF FAPWM13(0008)
           END-IF.
           MOVE WK01-CODE-AP138-F TO WK01-CODE-AP13-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP139-Z = 'Y'
               MOVE WK01-CODE-AP139 TO ACTN-CODE-AP13 OF FAPWM13(0009)
           END-IF.
           MOVE WK01-CODE-AP139-F TO WK01-CODE-AP13-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP1310-Z = 'Y'
               MOVE WK01-CODE-AP1310 TO ACTN-CODE-AP13 OF FAPWM13(0010)
           END-IF.
           MOVE WK01-CODE-AP1310-F TO WK01-CODE-AP13-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP1311-Z = 'Y'
               MOVE WK01-CODE-AP1311 TO ACTN-CODE-AP13 OF FAPWM13(0011)
           END-IF.
           MOVE WK01-CODE-AP1311-F TO WK01-CODE-AP13-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP1312-Z = 'Y'
               MOVE WK01-CODE-AP1312 TO ACTN-CODE-AP13 OF FAPWM13(0012)
           END-IF.
           MOVE WK01-CODE-AP1312-F TO WK01-CODE-AP13-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP1313-Z = 'Y'
               MOVE WK01-CODE-AP1313 TO ACTN-CODE-AP13 OF FAPWM13(0013)
           END-IF.
           MOVE WK01-CODE-AP1313-F TO WK01-CODE-AP13-F (13).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP1314-Z = 'Y'
               MOVE WK01-CODE-AP1314 TO ACTN-CODE-AP13 OF FAPWM13(0014)
           END-IF.
           MOVE WK01-CODE-AP1314-F TO WK01-CODE-AP13-F (14).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP1315-Z = 'Y'
               MOVE WK01-CODE-AP1315 TO ACTN-CODE-AP13 OF FAPWM13(0015)
           END-IF.
           MOVE WK01-CODE-AP1315-F TO WK01-CODE-AP13-F (15).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-AP131-Z = 'Y'
               MOVE WK01-TYPE-CODE-AP131 TO INCM-TYPE-CODE-AP13 OF
                FAPWM13(0001)
           END-IF.
           MOVE WK01-TYPE-CODE-AP131-F TO WK01-TYPE-CODE-AP13-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-AP132-Z = 'Y'
               MOVE WK01-TYPE-CODE-AP132 TO INCM-TYPE-CODE-AP13 OF
                FAPWM13(0002)
           END-IF.
           MOVE WK01-TYPE-CODE-AP132-F TO WK01-TYPE-CODE-AP13-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-AP133-Z = 'Y'
               MOVE WK01-TYPE-CODE-AP133 TO INCM-TYPE-CODE-AP13 OF
                FAPWM13(0003)
           END-IF.
           MOVE WK01-TYPE-CODE-AP133-F TO WK01-TYPE-CODE-AP13-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-AP134-Z = 'Y'
               MOVE WK01-TYPE-CODE-AP134 TO INCM-TYPE-CODE-AP13 OF
                FAPWM13(0004)
           END-IF.
           MOVE WK01-TYPE-CODE-AP134-F TO WK01-TYPE-CODE-AP13-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-AP135-Z = 'Y'
               MOVE WK01-TYPE-CODE-AP135 TO INCM-TYPE-CODE-AP13 OF
                FAPWM13(0005)
           END-IF.
           MOVE WK01-TYPE-CODE-AP135-F TO WK01-TYPE-CODE-AP13-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-AP136-Z = 'Y'
               MOVE WK01-TYPE-CODE-AP136 TO INCM-TYPE-CODE-AP13 OF
                FAPWM13(0006)
           END-IF.
           MOVE WK01-TYPE-CODE-AP136-F TO WK01-TYPE-CODE-AP13-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-AP137-Z = 'Y'
               MOVE WK01-TYPE-CODE-AP137 TO INCM-TYPE-CODE-AP13 OF
                FAPWM13(0007)
           END-IF.
           MOVE WK01-TYPE-CODE-AP137-F TO WK01-TYPE-CODE-AP13-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-AP138-Z = 'Y'
               MOVE WK01-TYPE-CODE-AP138 TO INCM-TYPE-CODE-AP13 OF
                FAPWM13(0008)
           END-IF.
           MOVE WK01-TYPE-CODE-AP138-F TO WK01-TYPE-CODE-AP13-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-AP139-Z = 'Y'
               MOVE WK01-TYPE-CODE-AP139 TO INCM-TYPE-CODE-AP13 OF
                FAPWM13(0009)
           END-IF.
           MOVE WK01-TYPE-CODE-AP139-F TO WK01-TYPE-CODE-AP13-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-AP1310-Z = 'Y'
               MOVE WK01-TYPE-CODE-AP1310 TO INCM-TYPE-CODE-AP13 OF
                FAPWM13(0010)
           END-IF.
           MOVE WK01-TYPE-CODE-AP1310-F TO WK01-TYPE-CODE-AP13-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-AP1311-Z = 'Y'
               MOVE WK01-TYPE-CODE-AP1311 TO INCM-TYPE-CODE-AP13 OF
                FAPWM13(0011)
           END-IF.
           MOVE WK01-TYPE-CODE-AP1311-F TO WK01-TYPE-CODE-AP13-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-AP1312-Z = 'Y'
               MOVE WK01-TYPE-CODE-AP1312 TO INCM-TYPE-CODE-AP13 OF
                FAPWM13(0012)
           END-IF.
           MOVE WK01-TYPE-CODE-AP1312-F TO WK01-TYPE-CODE-AP13-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-AP1313-Z = 'Y'
               MOVE WK01-TYPE-CODE-AP1313 TO INCM-TYPE-CODE-AP13 OF
                FAPWM13(0013)
           END-IF.
           MOVE WK01-TYPE-CODE-AP1313-F TO WK01-TYPE-CODE-AP13-F (13).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-AP1314-Z = 'Y'
               MOVE WK01-TYPE-CODE-AP1314 TO INCM-TYPE-CODE-AP13 OF
                FAPWM13(0014)
           END-IF.
           MOVE WK01-TYPE-CODE-AP1314-F TO WK01-TYPE-CODE-AP13-F (14).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-AP1315-Z = 'Y'
               MOVE WK01-TYPE-CODE-AP1315 TO INCM-TYPE-CODE-AP13 OF
                FAPWM13(0015)
           END-IF.
           MOVE WK01-TYPE-CODE-AP1315-F TO WK01-TYPE-CODE-AP13-F (15).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-AP131-Z = 'Y'
               MOVE WK01-NMBR-AP131 TO SEQ-NMBR-AP13 OF FAPWM13(0001)
           END-IF.
           MOVE WK01-NMBR-AP131-F TO WK01-NMBR-AP13-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-AP132-Z = 'Y'
               MOVE WK01-NMBR-AP132 TO SEQ-NMBR-AP13 OF FAPWM13(0002)
           END-IF.
           MOVE WK01-NMBR-AP132-F TO WK01-NMBR-AP13-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-AP133-Z = 'Y'
               MOVE WK01-NMBR-AP133 TO SEQ-NMBR-AP13 OF FAPWM13(0003)
           END-IF.
           MOVE WK01-NMBR-AP133-F TO WK01-NMBR-AP13-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-AP134-Z = 'Y'
               MOVE WK01-NMBR-AP134 TO SEQ-NMBR-AP13 OF FAPWM13(0004)
           END-IF.
           MOVE WK01-NMBR-AP134-F TO WK01-NMBR-AP13-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-AP135-Z = 'Y'
               MOVE WK01-NMBR-AP135 TO SEQ-NMBR-AP13 OF FAPWM13(0005)
           END-IF.
           MOVE WK01-NMBR-AP135-F TO WK01-NMBR-AP13-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-AP136-Z = 'Y'
               MOVE WK01-NMBR-AP136 TO SEQ-NMBR-AP13 OF FAPWM13(0006)
           END-IF.
           MOVE WK01-NMBR-AP136-F TO WK01-NMBR-AP13-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-AP137-Z = 'Y'
               MOVE WK01-NMBR-AP137 TO SEQ-NMBR-AP13 OF FAPWM13(0007)
           END-IF.
           MOVE WK01-NMBR-AP137-F TO WK01-NMBR-AP13-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-AP138-Z = 'Y'
               MOVE WK01-NMBR-AP138 TO SEQ-NMBR-AP13 OF FAPWM13(0008)
           END-IF.
           MOVE WK01-NMBR-AP138-F TO WK01-NMBR-AP13-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-AP139-Z = 'Y'
               MOVE WK01-NMBR-AP139 TO SEQ-NMBR-AP13 OF FAPWM13(0009)
           END-IF.
           MOVE WK01-NMBR-AP139-F TO WK01-NMBR-AP13-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-AP1310-Z = 'Y'
               MOVE WK01-NMBR-AP1310 TO SEQ-NMBR-AP13 OF FAPWM13(0010)
           END-IF.
           MOVE WK01-NMBR-AP1310-F TO WK01-NMBR-AP13-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-AP1311-Z = 'Y'
               MOVE WK01-NMBR-AP1311 TO SEQ-NMBR-AP13 OF FAPWM13(0011)
           END-IF.
           MOVE WK01-NMBR-AP1311-F TO WK01-NMBR-AP13-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-AP1312-Z = 'Y'
               MOVE WK01-NMBR-AP1312 TO SEQ-NMBR-AP13 OF FAPWM13(0012)
           END-IF.
           MOVE WK01-NMBR-AP1312-F TO WK01-NMBR-AP13-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-AP1313-Z = 'Y'
               MOVE WK01-NMBR-AP1313 TO SEQ-NMBR-AP13 OF FAPWM13(0013)
           END-IF.
           MOVE WK01-NMBR-AP1313-F TO WK01-NMBR-AP13-F (13).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-AP1314-Z = 'Y'
               MOVE WK01-NMBR-AP1314 TO SEQ-NMBR-AP13 OF FAPWM13(0014)
           END-IF.
           MOVE WK01-NMBR-AP1314-F TO WK01-NMBR-AP13-F (14).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-AP1315-Z = 'Y'
               MOVE WK01-NMBR-AP1315 TO SEQ-NMBR-AP13 OF FAPWM13(0015)
           END-IF.
           MOVE WK01-NMBR-AP1315-F TO WK01-NMBR-AP13-F (15).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-DESC-AP131-Z = 'Y'
               MOVE WK01-TYPE-DESC-AP131 TO INCM-TYPE-DESC-AP13 OF
                FAPWM13(0001)
           END-IF.
           MOVE WK01-TYPE-DESC-AP131-F TO WK01-TYPE-DESC-AP13-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-DESC-AP132-Z = 'Y'
               MOVE WK01-TYPE-DESC-AP132 TO INCM-TYPE-DESC-AP13 OF
                FAPWM13(0002)
           END-IF.
           MOVE WK01-TYPE-DESC-AP132-F TO WK01-TYPE-DESC-AP13-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-DESC-AP133-Z = 'Y'
               MOVE WK01-TYPE-DESC-AP133 TO INCM-TYPE-DESC-AP13 OF
                FAPWM13(0003)
           END-IF.
           MOVE WK01-TYPE-DESC-AP133-F TO WK01-TYPE-DESC-AP13-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-DESC-AP134-Z = 'Y'
               MOVE WK01-TYPE-DESC-AP134 TO INCM-TYPE-DESC-AP13 OF
                FAPWM13(0004)
           END-IF.
           MOVE WK01-TYPE-DESC-AP134-F TO WK01-TYPE-DESC-AP13-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-DESC-AP135-Z = 'Y'
               MOVE WK01-TYPE-DESC-AP135 TO INCM-TYPE-DESC-AP13 OF
                FAPWM13(0005)
           END-IF.
           MOVE WK01-TYPE-DESC-AP135-F TO WK01-TYPE-DESC-AP13-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-DESC-AP136-Z = 'Y'
               MOVE WK01-TYPE-DESC-AP136 TO INCM-TYPE-DESC-AP13 OF
                FAPWM13(0006)
           END-IF.
           MOVE WK01-TYPE-DESC-AP136-F TO WK01-TYPE-DESC-AP13-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-DESC-AP137-Z = 'Y'
               MOVE WK01-TYPE-DESC-AP137 TO INCM-TYPE-DESC-AP13 OF
                FAPWM13(0007)
           END-IF.
           MOVE WK01-TYPE-DESC-AP137-F TO WK01-TYPE-DESC-AP13-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-DESC-AP138-Z = 'Y'
               MOVE WK01-TYPE-DESC-AP138 TO INCM-TYPE-DESC-AP13 OF
                FAPWM13(0008)
           END-IF.
           MOVE WK01-TYPE-DESC-AP138-F TO WK01-TYPE-DESC-AP13-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-DESC-AP139-Z = 'Y'
               MOVE WK01-TYPE-DESC-AP139 TO INCM-TYPE-DESC-AP13 OF
                FAPWM13(0009)
           END-IF.
           MOVE WK01-TYPE-DESC-AP139-F TO WK01-TYPE-DESC-AP13-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-DESC-AP1310-Z = 'Y'
               MOVE WK01-TYPE-DESC-AP1310 TO INCM-TYPE-DESC-AP13 OF
                FAPWM13(0010)
           END-IF.
           MOVE WK01-TYPE-DESC-AP1310-F TO WK01-TYPE-DESC-AP13-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-DESC-AP1311-Z = 'Y'
               MOVE WK01-TYPE-DESC-AP1311 TO INCM-TYPE-DESC-AP13 OF
                FAPWM13(0011)
           END-IF.
           MOVE WK01-TYPE-DESC-AP1311-F TO WK01-TYPE-DESC-AP13-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-DESC-AP1312-Z = 'Y'
               MOVE WK01-TYPE-DESC-AP1312 TO INCM-TYPE-DESC-AP13 OF
                FAPWM13(0012)
           END-IF.
           MOVE WK01-TYPE-DESC-AP1312-F TO WK01-TYPE-DESC-AP13-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-DESC-AP1313-Z = 'Y'
               MOVE WK01-TYPE-DESC-AP1313 TO INCM-TYPE-DESC-AP13 OF
                FAPWM13(0013)
           END-IF.
           MOVE WK01-TYPE-DESC-AP1313-F TO WK01-TYPE-DESC-AP13-F (13).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-DESC-AP1314-Z = 'Y'
               MOVE WK01-TYPE-DESC-AP1314 TO INCM-TYPE-DESC-AP13 OF
                FAPWM13(0014)
           END-IF.
           MOVE WK01-TYPE-DESC-AP1314-F TO WK01-TYPE-DESC-AP13-F (14).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-DESC-AP1315-Z = 'Y'
               MOVE WK01-TYPE-DESC-AP1315 TO INCM-TYPE-DESC-AP13 OF
                FAPWM13(0015)
           END-IF.
           MOVE WK01-TYPE-DESC-AP1315-F TO WK01-TYPE-DESC-AP13-F (15).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-AP131-Z = 'Y'
               MOVE WK01-DATE-AP131 TO START-DATE-AP13 OF FAPWM13(0001)
           END-IF.
           MOVE WK01-DATE-AP131-F TO WK01-DATE-AP13-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-AP132-Z = 'Y'
               MOVE WK01-DATE-AP132 TO START-DATE-AP13 OF FAPWM13(0002)
           END-IF.
           MOVE WK01-DATE-AP132-F TO WK01-DATE-AP13-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-AP133-Z = 'Y'
               MOVE WK01-DATE-AP133 TO START-DATE-AP13 OF FAPWM13(0003)
           END-IF.
           MOVE WK01-DATE-AP133-F TO WK01-DATE-AP13-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-AP134-Z = 'Y'
               MOVE WK01-DATE-AP134 TO START-DATE-AP13 OF FAPWM13(0004)
           END-IF.
           MOVE WK01-DATE-AP134-F TO WK01-DATE-AP13-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-AP135-Z = 'Y'
               MOVE WK01-DATE-AP135 TO START-DATE-AP13 OF FAPWM13(0005)
           END-IF.
           MOVE WK01-DATE-AP135-F TO WK01-DATE-AP13-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-AP136-Z = 'Y'
               MOVE WK01-DATE-AP136 TO START-DATE-AP13 OF FAPWM13(0006)
           END-IF.
           MOVE WK01-DATE-AP136-F TO WK01-DATE-AP13-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-AP137-Z = 'Y'
               MOVE WK01-DATE-AP137 TO START-DATE-AP13 OF FAPWM13(0007)
           END-IF.
           MOVE WK01-DATE-AP137-F TO WK01-DATE-AP13-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-AP138-Z = 'Y'
               MOVE WK01-DATE-AP138 TO START-DATE-AP13 OF FAPWM13(0008)
           END-IF.
           MOVE WK01-DATE-AP138-F TO WK01-DATE-AP13-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-AP139-Z = 'Y'
               MOVE WK01-DATE-AP139 TO START-DATE-AP13 OF FAPWM13(0009)
           END-IF.
           MOVE WK01-DATE-AP139-F TO WK01-DATE-AP13-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-AP1310-Z = 'Y'
               MOVE WK01-DATE-AP1310 TO START-DATE-AP13 OF FAPWM13(0010)
           END-IF.
           MOVE WK01-DATE-AP1310-F TO WK01-DATE-AP13-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-AP1311-Z = 'Y'
               MOVE WK01-DATE-AP1311 TO START-DATE-AP13 OF FAPWM13(0011)
           END-IF.
           MOVE WK01-DATE-AP1311-F TO WK01-DATE-AP13-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-AP1312-Z = 'Y'
               MOVE WK01-DATE-AP1312 TO START-DATE-AP13 OF FAPWM13(0012)
           END-IF.
           MOVE WK01-DATE-AP1312-F TO WK01-DATE-AP13-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-AP1313-Z = 'Y'
               MOVE WK01-DATE-AP1313 TO START-DATE-AP13 OF FAPWM13(0013)
           END-IF.
           MOVE WK01-DATE-AP1313-F TO WK01-DATE-AP13-F (13).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-AP1314-Z = 'Y'
               MOVE WK01-DATE-AP1314 TO START-DATE-AP13 OF FAPWM13(0014)
           END-IF.
           MOVE WK01-DATE-AP1314-F TO WK01-DATE-AP13-F (14).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-AP1315-Z = 'Y'
               MOVE WK01-DATE-AP1315 TO START-DATE-AP13 OF FAPWM13(0015)
           END-IF.
           MOVE WK01-DATE-AP1315-F TO WK01-DATE-AP13-F (15).
