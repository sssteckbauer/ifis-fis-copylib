      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM84                              04/24/00  12:20  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM84.
           02  ACTN-CODE-GROUP-CH84.
               03  ACTN-CODE-CH84           OCCURS 11   PIC X(1).
           02  COA-CODE-4000-CH84           OCCURS 11   PIC X(1).
           02  COA-TITLE-4076-CH84          OCCURS 11   PIC X(35).
           02  STATUS-4076-CH84             OCCURS 11   PIC X(1).
           02  START-DATE-4076-CH84         OCCURS 11   PIC X(6).
           02  END-DATE-4076-CH84           OCCURS 11   PIC X(6).
