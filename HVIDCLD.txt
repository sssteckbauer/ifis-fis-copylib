       975-TO-R4041-CLOSE-DTL SECTION.
           MOVE CLD-USER-CD TO
               USER-CODE-4041 OF R4041-CLOSE-DTL
                                                                     .
           MOVE CLD-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4041 OF R4041-CLOSE-DTL
                                                                     .
           MOVE CLD-ENCMB-TYP TO
               ENCMBRNC-TYPE-4041 OF R4041-CLOSE-DTL
                                                                     .
           MOVE CLD-PO-CLS-CD TO
               PO-CLASS-CODE-4041 OF R4041-CLOSE-DTL
                                                                     .
           MOVE CLD-ENCMB-TRTMNT-CD TO
               ENCMBRNC-TRTMNT-CODE-4041 OF R4041-CLOSE-DTL
                                                                     .
       975-TO-R4041-CLOSE-DTL-EXIT.
           EXIT.
