      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWD12                              04/24/00  13:13  *
      *  REDEFINED SAVE-DBKEY-ID-4083 AND 4096 LENGTH --DMW 11/20/01  *
      *---------------------------------------------------------------*
       01  FPUWD12.
           02  WORK-DATE-PU12.
               03  MONTH-DATE-PU12                      PIC 9(2).
               03  DAY-DATE-PU12                        PIC 9(2).
               03  WORK-YEAR-DATE-PU12                  PIC 9(2).
           02  SYSTEM-DATE-PU12               COMP-3    PIC S9(8).
           02  ORDER-DATE-PU12                COMP-3    PIC S9(8).
           02  ACKNWDG-DATE-PU12              COMP-3    PIC S9(8).
           02  TOTAL-TAX-AMT-PU12             COMP-3    PIC S9(10)V99.
           02  ADDL-AMT-PU12                  COMP-3    PIC S9(10)V99.
           02  SAVE-DBKEY-ID-4083-PU12                  PIC X(08).
           02  SAVE-DBKEY-ID-4096-PU12                  PIC X(11).
           02  END-DATE-PU12                  COMP-3    PIC S9(8).
           02  WRK-PO-KEY-PU12.
               03  PO-NMBR-PU12                         PIC X(8).
               03  CHNG-SEQ-NMBR-PU12                   PIC X(3).
           02  SET-SORT-KEY-6311-PU12.
               03  UNVRS-CODE-PU12                      PIC X(2).
               03  INTRL-REF-ID-PU12          COMP-3    PIC S9(7).
           02  EFCTV-KEY-PU12.
               03  EFCTV-DATE-PU12            COMP-3    PIC S9(8).
               03  TIME-STAMP-PU12                      PIC X(6).
           02  ENTY-PRSN-IND-PU12                       PIC X(1).
           02  WORK-EXTENDED-PRICE-PU12       COMP-3    PIC 9(10)V9999.
           02  PREV-EXTENDED-PRICE-PU12       COMP-3    PIC 9(10)V9999.
           02  NET-AMT-NUM-PU12               COMP-3    PIC S9(10)V99.
           02  TAX-AMT-PU12                   COMP-3    PIC S9(10)V99.
           02  PO-DSCNT-AMT-PU12              COMP-3    PIC S9(10)V99.
           02  TOTAL-AMT-PU12                 COMP-3    PIC S9(10)V99.
           02  PO-DSCNT-PCT-PU12              COMP-3    PIC 9(3)V999.
           02  DB-UNIT-PRICE-PU12             COMP-3    PIC S9(10)V9999.
           02  DLVRY-BY-DATE-WK-PU12          COMP-3    PIC S9(8).
           02  TOTAL-TAX-RATE-PU12            COMP-3    PIC 9(3)V999.
           02  TOTAL-TAX-RATE-ALPHA-PU12                PIC X(8).
           02  APRVL-IND-PU12                           PIC X(1).
           02  APRVL-TMPLT-CODE-PU12                    PIC X(3).
           02  SYSDAT-SORTKEY-PU12.
               03  OPTN-1-CODE-PU12                     PIC X(8).
               03  OPTN-2-CODE-PU12                     PIC X(8).
               03  LEVEL-NMBR-PU12                      PIC 9(2).
