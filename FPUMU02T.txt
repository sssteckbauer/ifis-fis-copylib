       01  MAP-CONTROL.

    ***Created by Convert/DC version V8R03 on 11/20/00 at 13:00***

           02  FPUMU02-MAP-CONTROL.
           03  FPUMU02T.
           04  WK01-DCXMAPL-PARMS.
               10  WK01-MAPNAME                  PIC X(8)
                                                 VALUE 'FPUMU02I'.
               10  WK01-IKE-LENGTH               PIC S9(8)  COMP
001MLJ                                           VALUE 12601.
               10  WK01-TINA-LENGTH              PIC S9(4)  COMP
001MLJ                                           VALUE 857.
           05  WK01-ATTRIBUTES                   PIC X(60)
                                                 VALUE SPACE.
           05  WK01-ATTRIBUTES-ERROR             PIC X(60) VALUE
               'U  BIF'.
           05  WK01-CURSOR-DFLD                  PIC X(36)
                                                 VALUE SPACES.
           05  WK01-CURSOR-DFLD-QUAL             PIC X(30)
                                                 VALUE SPACES.
           05  WK01-ROW                          PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-COLUMN                       PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-ERROR-MSG                    PIC X(80)
                                                 VALUE SPACES.
           05  WK01-MAP-DATA-LEN                 PIC S9(6)
                                                 VALUE ZERO.
           05  WK01-1ST-FLAG                     PIC X(1)
                                                 VALUE 'Y'.
           05  WK01-FLAG-OUTPUT-DATA             PIC X(1)
                                                 VALUE 'Y'.
      *    05  WK01-STATUS-RETURN-FLAG           PIC X(1)
      *                                          VALUE SPACES.
           05  WK01-ALL-BUT-FLAG                 PIC X(1)
                                                 VALUE SPACES.
           05  WK01-FLAG-ALL-FIELDS-CHANGED      PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-CHANGED      PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERROR        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERROR        PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.

           05  WK01-VARDATA                      PIC X(11880)
                                                 VALUE SPACES.
           05  FILLER                            PIC X(100)
                                                 VALUE SPACES.
           04  WK01-ELEMENT-AREA.

               10  WK01-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-6001-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00       PIC X(35)
                                               VALUE SPACES.
               10  WK01-DATE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-HH-MM-TIME-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00       PIC X(4)
                                               VALUE SPACES.
               10  WK01-PM-FLAG-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-USER-ID-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID       PIC X(32)
                                               VALUE SPACES.
               10  WK01-RSPNS-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-FUNC-DESCRIPTION-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION       PIC X(28)
                                               VALUE SPACES.
               10  WK01-STATUS-CODE-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-CODE-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-PU02       PIC X(1)
                                               VALUE SPACES.
               10  WK01-CODE-4070-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4070-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4070-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4070-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4070-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4070-PU02       PIC X(8)
                                               VALUE SPACES.
               10  WK01-ERROR-IND-4070-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ERROR-IND-4070-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ERROR-IND-4070-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ERROR-IND-4070-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ERROR-IND-4070-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ERROR-IND-4070-PU02       PIC X(1)
                                               VALUE SPACES.
               10  WK01-CNTR-DTL-4070-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CNTR-DTL-4070-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CNTR-DTL-4070-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CNTR-DTL-4070-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CNTR-DTL-4070-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CNTR-DTL-4070-PU02       PIC X(4)
                                               VALUE SPACES.
               10  WK01-CNTR-ACCT-4070-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CNTR-ACCT-4070-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CNTR-ACCT-4070-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CNTR-ACCT-4070-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CNTR-ACCT-4070-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CNTR-ACCT-4070-PU02       PIC X(4)
                                               VALUE SPACES.
               10  WK01-DATE-4070-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4070-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4070-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4070-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4070-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4070-PU02       PIC X(6)
                                               VALUE SPACES.
               10  WK01-DATE-4070-PU0B-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4070-PU0B-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4070-PU0B-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4070-PU0B-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4070-PU0B-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4070-PU0B       PIC X(6)
                                               VALUE SPACES.
               10  WK01-NAME-4070-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-4070-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-4070-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-4070-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-4070-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-4070-PU02       PIC X(35)
                                               VALUE SPACES.
               10  WK01-AREA-CODE-4070-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AREA-CODE-4070-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AREA-CODE-4070-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AREA-CODE-4070-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AREA-CODE-4070-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AREA-CODE-4070-PU02       PIC X(3)
                                               VALUE SPACES.
               10  WK01-XCHNG-ID-4070-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XCHNG-ID-4070-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XCHNG-ID-4070-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XCHNG-ID-4070-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XCHNG-ID-4070-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XCHNG-ID-4070-PU02       PIC X(3)
                                               VALUE SPACES.
               10  WK01-SEQ-ID-4070-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-ID-4070-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-ID-4070-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-ID-4070-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-ID-4070-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-ID-4070-PU02       PIC X(4)
                                               VALUE SPACES.
               10  WK01-XTNSN-ID-4070-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XTNSN-ID-4070-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XTNSN-ID-4070-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XTNSN-ID-4070-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XTNSN-ID-4070-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XTNSN-ID-4070-PU02       PIC X(4)
                                               VALUE SPACES.
DEVMJM*
               10  WK01-EMAIL-ADR-4070-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-EMAIL-ADR-4070-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-EMAIL-ADR-4070-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-EMAIL-ADR-4070-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-EMAIL-ADR-4070-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-EMAIL-ADR-4070-PU02                PIC X(20)
                                               VALUE SPACES.
DEVMJM*
               10  WK01-CODE-4070-PU0B-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4070-PU0B-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4070-PU0B-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4070-PU0B-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4070-PU0B-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4070-PU0B       PIC X(1)
                                               VALUE SPACES.
               10  WK01-CODE-4070-PU0A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4070-PU0A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4070-PU0A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4070-PU0A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4070-PU0A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4070-PU0A       PIC X(6)
                                               VALUE SPACES.
               10  WK01-TITLE-4010-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4010-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4010-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4010-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4010-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4010-PU02       PIC X(35)
                                               VALUE SPACES.
               10  WK01-PU0A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PU0A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PU0A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PU0A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PU0A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PU0A       PIC X(6)
                                               VALUE SPACES.
               10  WK01-FLAG-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FLAG-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FLAG-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FLAG-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FLAG-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FLAG-PU02       PIC X(1)
                                               VALUE SPACES.
               10  WK01-IND-4070-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-4070-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-4070-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-4070-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-4070-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-4070-PU02       PIC X(1)
                                               VALUE SPACES.
               10  WK01-ID-LAST-NINE-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-LAST-NINE-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-LAST-NINE-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-LAST-NINE-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-LAST-NINE-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-LAST-NINE-PU02       PIC X(9)
                                               VALUE SPACES.
               10  WK01-NAME-KEY-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-KEY-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-KEY-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-KEY-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-KEY-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-KEY-PU02       PIC X(35)
                                               VALUE SPACES.
DEVMJM*
               10  WK01-PREV-PO-NBR-4070-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PREV-PO-NBR-4070-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PREV-PO-NBR-4070-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PREV-PO-NBR-4070-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PREV-PO-NBR-4070-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PREV-PO-NBR-4070-PU02                PIC X(8)
                                               VALUE SPACES.
DEVMJM*
001MLJ         10  WK01-FAX-AREA-CODE-4070-PU02-Z          PIC X(1)
                                               VALUE SPACE.
               10  WK01-FAX-AREA-CODE-4070-PU02-F          PIC X(1)
                                               VALUE SPACE.
               10  WK01-FAX-AREA-CODE-4070-PU02-A          PIC X(1)
                                               VALUE SPACE.
               10  WK01-FAX-AREA-CODE-4070-PU02-V          PIC X(1)
                                               VALUE SPACE.
               10  WK01-FAX-AREA-CODE-4070-PU02-O          PIC X(1)
                                               VALUE SPACE.
               10  WK01-FAX-AREA-CODE-4070-PU02   PIC X(3)
                                               VALUE SPACES.
               10  WK01-FAX-XCHNG-ID-4070-PU02-Z          PIC X(1)
                                               VALUE SPACE.
               10  WK01-FAX-XCHNG-ID-4070-PU02-F          PIC X(1)
                                               VALUE SPACE.
               10  WK01-FAX-XCHNG-ID-4070-PU02-A          PIC X(1)
                                               VALUE SPACE.
               10  WK01-FAX-XCHNG-ID-4070-PU02-V          PIC X(1)
                                               VALUE SPACE.
               10  WK01-FAX-XCHNG-ID-4070-PU02-O          PIC X(1)
                                               VALUE SPACE.
               10  WK01-FAX-XCHNG-ID-4070-PU02   PIC X(3)
                                               VALUE SPACES.
               10  WK01-FAX-SEQ-ID-4070-PU02-Z          PIC X(1)
                                               VALUE SPACE.
               10  WK01-FAX-SEQ-ID-4070-PU02-F          PIC X(1)
                                               VALUE SPACE.
               10  WK01-FAX-SEQ-ID-4070-PU02-A          PIC X(1)
                                               VALUE SPACE.
               10  WK01-FAX-SEQ-ID-4070-PU02-V          PIC X(1)
                                               VALUE SPACE.
               10  WK01-FAX-SEQ-ID-4070-PU02-O          PIC X(1)
                                               VALUE SPACE.
               10  WK01-FAX-SEQ-ID-4070-PU02   PIC X(4)
                                               VALUE SPACES.
               10  WK01-VDR-EMAIL-ADR-4070-PU02-Z          PIC X(1)
                                               VALUE SPACE.
               10  WK01-VDR-EMAIL-ADR-4070-PU02-F          PIC X(1)
                                               VALUE SPACE.
               10  WK01-VDR-EMAIL-ADR-4070-PU02-A          PIC X(1)
                                               VALUE SPACE.
               10  WK01-VDR-EMAIL-ADR-4070-PU02-V          PIC X(1)
                                               VALUE SPACE.
               10  WK01-VDR-EMAIL-ADR-4070-PU02-O          PIC X(1)
                                               VALUE SPACE.
               10  WK01-VDR-EMAIL-ADR-4070-PU02            PIC X(37)
001MLJ                                         VALUE SPACES.
               10  WK01-DATE-4070-PU0A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4070-PU0A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4070-PU0A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4070-PU0A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4070-PU0A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4070-PU0A       PIC X(6)
                                               VALUE SPACES.
               10  WK01-TOTAL-4070-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TOTAL-4070-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TOTAL-4070-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TOTAL-4070-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TOTAL-4070-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TOTAL-4070-PU02       PIC X(16)
                                               VALUE SPACES.
               10  WK01-DATE-4070-PU0C-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4070-PU0C-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4070-PU0C-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4070-PU0C-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4070-PU0C-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4070-PU0C       PIC X(6)
                                               VALUE SPACES.
               10  WK01-CMPLT-IND-4070-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CMPLT-IND-4070-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CMPLT-IND-4070-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CMPLT-IND-4070-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CMPLT-IND-4070-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CMPLT-IND-4070-PU02       PIC X(1)
                                               VALUE SPACES.
               10  WK01-IND-4070-PU0A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-4070-PU0A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-4070-PU0A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-4070-PU0A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-4070-PU0A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-4070-PU0A       PIC X(1)
                                               VALUE SPACES.
               10  WK01-TMPLT-CODE-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-CODE-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-CODE-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-CODE-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-CODE-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TMPLT-CODE-PU02       PIC X(3)
                                               VALUE SPACES.
               10  WK01-RQST-4070-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RQST-4070-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RQST-4070-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RQST-4070-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RQST-4070-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RQST-4070-PU02       PIC X(1)
                                               VALUE SPACES.
               10  WK01-PU02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PU02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PU02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PU02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PU02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PU02       PIC X(4)
                                               VALUE SPACES.
               10  WK01-ZZMESSAGE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE       PIC X(160)
                                               VALUE SPACES.
               10  WK01-PASSED-ONE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE       PIC X(32)
                                               VALUE SPACES.
               10  WK01-MAP-RESPONSE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-TEXT-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00       PIC X(14)
                                               VALUE SPACES.
