       976-FROM-R4169-INV-CLASS SECTION.

FCCCE**        MOVE USER-CODE-4169 OF R4169-INV-CLASS
FCCCE          MOVE DBLINK-USER-CD
               TO IVC-USER-CD.

               MOVE LAST-ACTVY-DATE-4169 OF R4169-INV-CLASS
               TO IVC-LAST-ACTVY-DT.

               MOVE UNVRS-CODE-4169 OF R4169-INV-CLASS
               TO IVC-UNVRS-CD.

               MOVE SUB-DCMNT-TYPE-4169 OF R4169-INV-CLASS
               TO IVC-SUB-DOC-TYP.

               MOVE INV-CLS-DESC-4169 OF R4169-INV-CLASS
               TO IVC-INV-CLS-DESC.

               MOVE ORGN-CODE-4169 OF R4169-INV-CLASS
               TO IVC-ORGN-CD.

           IF START-DATE-4169 OF R4169-INV-CLASS  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO IVC-START-DT
           ELSE
               MOVE START-DATE-4169 OF R4169-INV-CLASS
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO IVC-START-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO IVC-START-DT
               END-IF
           END-IF.

           IF END-DATE-4169 OF R4169-INV-CLASS  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO IVC-END-DT
           ELSE
               MOVE END-DATE-4169 OF R4169-INV-CLASS
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO IVC-END-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO IVC-END-DT
               END-IF
           END-IF.

           IF ACTVY-DATE-4169 OF R4169-INV-CLASS  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO IVC-ACTVY-DT
           ELSE
               MOVE ACTVY-DATE-4169 OF R4169-INV-CLASS
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO IVC-ACTVY-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO IVC-ACTVY-DT
               END-IF
           END-IF.

               MOVE CMDTY-CODE-4169 OF R4169-INV-CLASS
               TO IVC-CMDTY-CD.

               MOVE CLAUSE-CODE-4169 OF R4169-INV-CLASS
               TO IVC-CLAUSE-CD.
       976-FROM-R4169-INV-CLASS-EXIT.
           EXIT.
