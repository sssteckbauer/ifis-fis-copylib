      *%---------------------------------------------------------------*
      *%                   200-FSEII04 ROUTINE                         *
      *%---------------------------------------------------------------*
      ****----------------------------------------------------------****
      **  AUTHOR    DATE    DESCRIPTION                                *
      **  ------- --------  ----------------------------------------   *
      **  DEVRRE  07/07/01  REMOVE LINK TO FSYDU10                     *
      ****----------------------------------------------------------****
       200-FSEII04.

      ******************************************************************
      ***  IF THE USER DOES NOT HAVE DELETE PERMISSION, DISPLAY MESSAGE
      ***  AND DO NOT DO ANY FURTHER DELETE VALIDATION.
      ******************************************************************
           IF DLT-PRMSN-FLAG-SY01 = 'N'
               MOVE 'D' TO SCRTY-VLTN-FLAG-SY01
               MOVE 990586 TO MSG-ID-SY00
               PERFORM 200-ACTNERR-S11
                  THRU 200-ACTNERR-S11-EXIT
               IF AC99W-FLAG-DISPLAY = AC99W-YES-DISPLAY
                  GO TO 200-FSEII04-EXIT
               END-IF
      ***  USER NOT AUTHORIZED FOR DELETE FUNCTION
               GO TO 200-FSEII04-EXIT
           END-IF.
      ******************************************************************

       200-FSEII04-EXIT.  EXIT.
