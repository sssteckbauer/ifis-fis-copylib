    ***Created by Convert/DC version V8R03 on 05/24/00 at 11:00***

FCBCC * SUB1 = MAP FIELD SUB, WITH A MAX OF 15 ENTRIES ON THE SCREEN
FCBCC * SUB2 = MAP WORK RECORD SUB, WITH A MAX OF 50 ENTRIES
FCBCC * SUB3 = SUB OF SPECIFIC MAP LINE SELECTED BY USER

FCBCC      MOVE +1  TO AC99W-SUB1
FCBCC      MOVE ZERO TO AC99W-COUNT1
FCBCC                   AC99W-SUB3
FCBCC      IF ACCOMM-PAGE GREATER +1
FCBCC        COMPUTE AC99W-SUB2 = (ACCOMM-PAGE - 1) * 15 + 1
FCBCC      ELSE
FCBCC        MOVE +1  TO AC99W-SUB2
FCBCC        MOVE +1  TO ACCOMM-PAGE
FCBCC      END-IF
      *%--------------------------------------------------------------%*
FCBCC      PERFORM WITH TEST BEFORE UNTIL AC99W-SUB1 GREATER 15
             IF WK01-SELECT-Z(AC99W-SUB1) = 'Y'
                 MOVE WK01-SELECT(AC99W-SUB1) TO AMR-SELECT OF
                  ADSO-APPLICATION-MENU-RECORD(AC99W-SUB2)
FCBCC            IF WK01-SELECT(AC99W-SUB1) NOT = SPACES
FCBCC               MOVE AC99W-SUB2 TO AC99W-SUB3
FCBCC               ADD +1 TO AC99W-COUNT1
FCBCC            END-IF
             END-IF
FCAMH        ADD +1 TO AC99W-SUB1
FCAMH                  AC99W-SUB2
FCBCC      END-PERFORM.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RESPONSE-FIELD-Z = 'Y'
               MOVE WK01-RESPONSE-FIELD TO AMR-RESPONSE-FIELD OF
                ADSO-APPLICATION-MENU-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
