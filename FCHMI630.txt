    ***Created by Convert/DC version V8R03 on 11/01/00 at 10:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH631-Z = 'Y'
               MOVE WK01-CODE-4001-CH631 TO COA-CODE-4001-CH63 OF
                FCHWM63(0001)
           END-IF.
           MOVE WK01-CODE-4001-CH631-F TO WK01-CODE-4001-CH63-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH6A1-Z = 'Y'
               MOVE WK01-CODE-4001-CH6A1 TO FUND-CODE-4001-CH63 OF
                FCHWM63(0001)
           END-IF.
           MOVE WK01-CODE-4001-CH6A1-F TO WK01-CODE-4001-CH6A-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4005-CH631-Z = 'Y'
               MOVE WK01-TITLE-4005-CH631 TO FUND-TITLE-4005-CH63 OF
                FCHWM63(0001)
           END-IF.
           MOVE WK01-TITLE-4005-CH631-F TO WK01-TITLE-4005-CH63-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-4005-CH631-Z = 'Y'
               MOVE WK01-4005-CH631 TO STATUS-4005-CH63 OF FCHWM63(0001)
           END-IF.
           MOVE WK01-4005-CH631-F TO WK01-4005-CH63-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH631-Z = 'Y'
               MOVE WK01-DATE-4005-CH631 TO START-DATE-4005-CH63 OF
                FCHWM63(0001)
           END-IF.
           MOVE WK01-DATE-4005-CH631-F TO WK01-DATE-4005-CH63-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH6A1-Z = 'Y'
               MOVE WK01-DATE-4005-CH6A1 TO END-DATE-4005-CH63 OF
                FCHWM63(0001)
           END-IF.
           MOVE WK01-DATE-4005-CH6A1-F TO WK01-DATE-4005-CH6A-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-COA-CODE-CH63-Z = 'Y'
               MOVE WK01-COA-CODE-CH63 TO RQST-COA-CODE-CH63 OF FCHWK63
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH631-Z = 'Y'
               MOVE WK01-CODE-CH631 TO ACTN-CODE-CH63 OF FCHWM63(0001)
           END-IF.
           MOVE WK01-CODE-CH631-F TO WK01-CODE-CH63-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CH63-Z = 'Y'
               MOVE WK01-STATUS-CH63 TO SLCTN-STATUS-CH63 OF FCHWK63
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-CH63-Z = 'Y'
               MOVE WK01-DATE-CH63 TO SLCTN-DATE-CH63 OF FCHWK63
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUND-CODE-CH63-Z = 'Y'
               MOVE WK01-FUND-CODE-CH63 TO RQST-FUND-CODE-CH63 OF
                FCHWK63
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH632-Z = 'Y'
               MOVE WK01-CODE-CH632 TO ACTN-CODE-CH63 OF FCHWM63(0002)
           END-IF.
           MOVE WK01-CODE-CH632-F TO WK01-CODE-CH63-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH633-Z = 'Y'
               MOVE WK01-CODE-CH633 TO ACTN-CODE-CH63 OF FCHWM63(0003)
           END-IF.
           MOVE WK01-CODE-CH633-F TO WK01-CODE-CH63-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH634-Z = 'Y'
               MOVE WK01-CODE-CH634 TO ACTN-CODE-CH63 OF FCHWM63(0004)
           END-IF.
           MOVE WK01-CODE-CH634-F TO WK01-CODE-CH63-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH635-Z = 'Y'
               MOVE WK01-CODE-CH635 TO ACTN-CODE-CH63 OF FCHWM63(0005)
           END-IF.
           MOVE WK01-CODE-CH635-F TO WK01-CODE-CH63-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH636-Z = 'Y'
               MOVE WK01-CODE-CH636 TO ACTN-CODE-CH63 OF FCHWM63(0006)
           END-IF.
           MOVE WK01-CODE-CH636-F TO WK01-CODE-CH63-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH637-Z = 'Y'
               MOVE WK01-CODE-CH637 TO ACTN-CODE-CH63 OF FCHWM63(0007)
           END-IF.
           MOVE WK01-CODE-CH637-F TO WK01-CODE-CH63-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH638-Z = 'Y'
               MOVE WK01-CODE-CH638 TO ACTN-CODE-CH63 OF FCHWM63(0008)
           END-IF.
           MOVE WK01-CODE-CH638-F TO WK01-CODE-CH63-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH639-Z = 'Y'
               MOVE WK01-CODE-CH639 TO ACTN-CODE-CH63 OF FCHWM63(0009)
           END-IF.
           MOVE WK01-CODE-CH639-F TO WK01-CODE-CH63-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH6310-Z = 'Y'
               MOVE WK01-CODE-CH6310 TO ACTN-CODE-CH63 OF FCHWM63(0010)
           END-IF.
           MOVE WK01-CODE-CH6310-F TO WK01-CODE-CH63-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH6311-Z = 'Y'
               MOVE WK01-CODE-CH6311 TO ACTN-CODE-CH63 OF FCHWM63(0011)
           END-IF.
           MOVE WK01-CODE-CH6311-F TO WK01-CODE-CH63-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH632-Z = 'Y'
               MOVE WK01-CODE-4001-CH632 TO COA-CODE-4001-CH63 OF
                FCHWM63(0002)
           END-IF.
           MOVE WK01-CODE-4001-CH632-F TO WK01-CODE-4001-CH63-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH633-Z = 'Y'
               MOVE WK01-CODE-4001-CH633 TO COA-CODE-4001-CH63 OF
                FCHWM63(0003)
           END-IF.
           MOVE WK01-CODE-4001-CH633-F TO WK01-CODE-4001-CH63-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH634-Z = 'Y'
               MOVE WK01-CODE-4001-CH634 TO COA-CODE-4001-CH63 OF
                FCHWM63(0004)
           END-IF.
           MOVE WK01-CODE-4001-CH634-F TO WK01-CODE-4001-CH63-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH635-Z = 'Y'
               MOVE WK01-CODE-4001-CH635 TO COA-CODE-4001-CH63 OF
                FCHWM63(0005)
           END-IF.
           MOVE WK01-CODE-4001-CH635-F TO WK01-CODE-4001-CH63-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH636-Z = 'Y'
               MOVE WK01-CODE-4001-CH636 TO COA-CODE-4001-CH63 OF
                FCHWM63(0006)
           END-IF.
           MOVE WK01-CODE-4001-CH636-F TO WK01-CODE-4001-CH63-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH637-Z = 'Y'
               MOVE WK01-CODE-4001-CH637 TO COA-CODE-4001-CH63 OF
                FCHWM63(0007)
           END-IF.
           MOVE WK01-CODE-4001-CH637-F TO WK01-CODE-4001-CH63-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH638-Z = 'Y'
               MOVE WK01-CODE-4001-CH638 TO COA-CODE-4001-CH63 OF
                FCHWM63(0008)
           END-IF.
           MOVE WK01-CODE-4001-CH638-F TO WK01-CODE-4001-CH63-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH639-Z = 'Y'
               MOVE WK01-CODE-4001-CH639 TO COA-CODE-4001-CH63 OF
                FCHWM63(0009)
           END-IF.
           MOVE WK01-CODE-4001-CH639-F TO WK01-CODE-4001-CH63-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH6310-Z = 'Y'
               MOVE WK01-CODE-4001-CH6310 TO COA-CODE-4001-CH63 OF
                FCHWM63(0010)
           END-IF.
           MOVE WK01-CODE-4001-CH6310-F TO WK01-CODE-4001-CH63-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH6311-Z = 'Y'
               MOVE WK01-CODE-4001-CH6311 TO COA-CODE-4001-CH63 OF
                FCHWM63(0011)
           END-IF.
           MOVE WK01-CODE-4001-CH6311-F TO WK01-CODE-4001-CH63-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH6A2-Z = 'Y'
               MOVE WK01-CODE-4001-CH6A2 TO FUND-CODE-4001-CH63 OF
                FCHWM63(0002)
           END-IF.
           MOVE WK01-CODE-4001-CH6A2-F TO WK01-CODE-4001-CH6A-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH6A3-Z = 'Y'
               MOVE WK01-CODE-4001-CH6A3 TO FUND-CODE-4001-CH63 OF
                FCHWM63(0003)
           END-IF.
           MOVE WK01-CODE-4001-CH6A3-F TO WK01-CODE-4001-CH6A-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH6A4-Z = 'Y'
               MOVE WK01-CODE-4001-CH6A4 TO FUND-CODE-4001-CH63 OF
                FCHWM63(0004)
           END-IF.
           MOVE WK01-CODE-4001-CH6A4-F TO WK01-CODE-4001-CH6A-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH6A5-Z = 'Y'
               MOVE WK01-CODE-4001-CH6A5 TO FUND-CODE-4001-CH63 OF
                FCHWM63(0005)
           END-IF.
           MOVE WK01-CODE-4001-CH6A5-F TO WK01-CODE-4001-CH6A-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH6A6-Z = 'Y'
               MOVE WK01-CODE-4001-CH6A6 TO FUND-CODE-4001-CH63 OF
                FCHWM63(0006)
           END-IF.
           MOVE WK01-CODE-4001-CH6A6-F TO WK01-CODE-4001-CH6A-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH6A7-Z = 'Y'
               MOVE WK01-CODE-4001-CH6A7 TO FUND-CODE-4001-CH63 OF
                FCHWM63(0007)
           END-IF.
           MOVE WK01-CODE-4001-CH6A7-F TO WK01-CODE-4001-CH6A-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH6A8-Z = 'Y'
               MOVE WK01-CODE-4001-CH6A8 TO FUND-CODE-4001-CH63 OF
                FCHWM63(0008)
           END-IF.
           MOVE WK01-CODE-4001-CH6A8-F TO WK01-CODE-4001-CH6A-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH6A9-Z = 'Y'
               MOVE WK01-CODE-4001-CH6A9 TO FUND-CODE-4001-CH63 OF
                FCHWM63(0009)
           END-IF.
           MOVE WK01-CODE-4001-CH6A9-F TO WK01-CODE-4001-CH6A-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH6A10-Z = 'Y'
               MOVE WK01-CODE-4001-CH6A10 TO FUND-CODE-4001-CH63 OF
                FCHWM63(0010)
           END-IF.
           MOVE WK01-CODE-4001-CH6A10-F TO WK01-CODE-4001-CH6A-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4001-CH6A11-Z = 'Y'
               MOVE WK01-CODE-4001-CH6A11 TO FUND-CODE-4001-CH63 OF
                FCHWM63(0011)
           END-IF.
           MOVE WK01-CODE-4001-CH6A11-F TO WK01-CODE-4001-CH6A-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4005-CH632-Z = 'Y'
               MOVE WK01-TITLE-4005-CH632 TO FUND-TITLE-4005-CH63 OF
                FCHWM63(0002)
           END-IF.
           MOVE WK01-TITLE-4005-CH632-F TO WK01-TITLE-4005-CH63-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4005-CH633-Z = 'Y'
               MOVE WK01-TITLE-4005-CH633 TO FUND-TITLE-4005-CH63 OF
                FCHWM63(0003)
           END-IF.
           MOVE WK01-TITLE-4005-CH633-F TO WK01-TITLE-4005-CH63-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4005-CH634-Z = 'Y'
               MOVE WK01-TITLE-4005-CH634 TO FUND-TITLE-4005-CH63 OF
                FCHWM63(0004)
           END-IF.
           MOVE WK01-TITLE-4005-CH634-F TO WK01-TITLE-4005-CH63-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4005-CH635-Z = 'Y'
               MOVE WK01-TITLE-4005-CH635 TO FUND-TITLE-4005-CH63 OF
                FCHWM63(0005)
           END-IF.
           MOVE WK01-TITLE-4005-CH635-F TO WK01-TITLE-4005-CH63-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4005-CH636-Z = 'Y'
               MOVE WK01-TITLE-4005-CH636 TO FUND-TITLE-4005-CH63 OF
                FCHWM63(0006)
           END-IF.
           MOVE WK01-TITLE-4005-CH636-F TO WK01-TITLE-4005-CH63-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4005-CH637-Z = 'Y'
               MOVE WK01-TITLE-4005-CH637 TO FUND-TITLE-4005-CH63 OF
                FCHWM63(0007)
           END-IF.
           MOVE WK01-TITLE-4005-CH637-F TO WK01-TITLE-4005-CH63-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4005-CH638-Z = 'Y'
               MOVE WK01-TITLE-4005-CH638 TO FUND-TITLE-4005-CH63 OF
                FCHWM63(0008)
           END-IF.
           MOVE WK01-TITLE-4005-CH638-F TO WK01-TITLE-4005-CH63-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4005-CH639-Z = 'Y'
               MOVE WK01-TITLE-4005-CH639 TO FUND-TITLE-4005-CH63 OF
                FCHWM63(0009)
           END-IF.
           MOVE WK01-TITLE-4005-CH639-F TO WK01-TITLE-4005-CH63-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4005-CH6310-Z = 'Y'
               MOVE WK01-TITLE-4005-CH6310 TO FUND-TITLE-4005-CH63 OF
                FCHWM63(0010)
           END-IF.
           MOVE WK01-TITLE-4005-CH6310-F TO WK01-TITLE-4005-CH63-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4005-CH6311-Z = 'Y'
               MOVE WK01-TITLE-4005-CH6311 TO FUND-TITLE-4005-CH63 OF
                FCHWM63(0011)
           END-IF.
           MOVE WK01-TITLE-4005-CH6311-F TO WK01-TITLE-4005-CH63-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-4005-CH632-Z = 'Y'
               MOVE WK01-4005-CH632 TO STATUS-4005-CH63 OF FCHWM63(0002)
           END-IF.
           MOVE WK01-4005-CH632-F TO WK01-4005-CH63-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-4005-CH633-Z = 'Y'
               MOVE WK01-4005-CH633 TO STATUS-4005-CH63 OF FCHWM63(0003)
           END-IF.
           MOVE WK01-4005-CH633-F TO WK01-4005-CH63-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-4005-CH634-Z = 'Y'
               MOVE WK01-4005-CH634 TO STATUS-4005-CH63 OF FCHWM63(0004)
           END-IF.
           MOVE WK01-4005-CH634-F TO WK01-4005-CH63-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-4005-CH635-Z = 'Y'
               MOVE WK01-4005-CH635 TO STATUS-4005-CH63 OF FCHWM63(0005)
           END-IF.
           MOVE WK01-4005-CH635-F TO WK01-4005-CH63-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-4005-CH636-Z = 'Y'
               MOVE WK01-4005-CH636 TO STATUS-4005-CH63 OF FCHWM63(0006)
           END-IF.
           MOVE WK01-4005-CH636-F TO WK01-4005-CH63-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-4005-CH637-Z = 'Y'
               MOVE WK01-4005-CH637 TO STATUS-4005-CH63 OF FCHWM63(0007)
           END-IF.
           MOVE WK01-4005-CH637-F TO WK01-4005-CH63-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-4005-CH638-Z = 'Y'
               MOVE WK01-4005-CH638 TO STATUS-4005-CH63 OF FCHWM63(0008)
           END-IF.
           MOVE WK01-4005-CH638-F TO WK01-4005-CH63-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-4005-CH639-Z = 'Y'
               MOVE WK01-4005-CH639 TO STATUS-4005-CH63 OF FCHWM63(0009)
           END-IF.
           MOVE WK01-4005-CH639-F TO WK01-4005-CH63-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-4005-CH6310-Z = 'Y'
               MOVE WK01-4005-CH6310 TO STATUS-4005-CH63 OF
                FCHWM63(0010)
           END-IF.
           MOVE WK01-4005-CH6310-F TO WK01-4005-CH63-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-4005-CH6311-Z = 'Y'
               MOVE WK01-4005-CH6311 TO STATUS-4005-CH63 OF
                FCHWM63(0011)
           END-IF.
           MOVE WK01-4005-CH6311-F TO WK01-4005-CH63-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH632-Z = 'Y'
               MOVE WK01-DATE-4005-CH632 TO START-DATE-4005-CH63 OF
                FCHWM63(0002)
           END-IF.
           MOVE WK01-DATE-4005-CH632-F TO WK01-DATE-4005-CH63-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH633-Z = 'Y'
               MOVE WK01-DATE-4005-CH633 TO START-DATE-4005-CH63 OF
                FCHWM63(0003)
           END-IF.
           MOVE WK01-DATE-4005-CH633-F TO WK01-DATE-4005-CH63-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH634-Z = 'Y'
               MOVE WK01-DATE-4005-CH634 TO START-DATE-4005-CH63 OF
                FCHWM63(0004)
           END-IF.
           MOVE WK01-DATE-4005-CH634-F TO WK01-DATE-4005-CH63-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH635-Z = 'Y'
               MOVE WK01-DATE-4005-CH635 TO START-DATE-4005-CH63 OF
                FCHWM63(0005)
           END-IF.
           MOVE WK01-DATE-4005-CH635-F TO WK01-DATE-4005-CH63-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH636-Z = 'Y'
               MOVE WK01-DATE-4005-CH636 TO START-DATE-4005-CH63 OF
                FCHWM63(0006)
           END-IF.
           MOVE WK01-DATE-4005-CH636-F TO WK01-DATE-4005-CH63-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH637-Z = 'Y'
               MOVE WK01-DATE-4005-CH637 TO START-DATE-4005-CH63 OF
                FCHWM63(0007)
           END-IF.
           MOVE WK01-DATE-4005-CH637-F TO WK01-DATE-4005-CH63-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH638-Z = 'Y'
               MOVE WK01-DATE-4005-CH638 TO START-DATE-4005-CH63 OF
                FCHWM63(0008)
           END-IF.
           MOVE WK01-DATE-4005-CH638-F TO WK01-DATE-4005-CH63-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH639-Z = 'Y'
               MOVE WK01-DATE-4005-CH639 TO START-DATE-4005-CH63 OF
                FCHWM63(0009)
           END-IF.
           MOVE WK01-DATE-4005-CH639-F TO WK01-DATE-4005-CH63-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH6310-Z = 'Y'
               MOVE WK01-DATE-4005-CH6310 TO START-DATE-4005-CH63 OF
                FCHWM63(0010)
           END-IF.
           MOVE WK01-DATE-4005-CH6310-F TO WK01-DATE-4005-CH63-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH6311-Z = 'Y'
               MOVE WK01-DATE-4005-CH6311 TO START-DATE-4005-CH63 OF
                FCHWM63(0011)
           END-IF.
           MOVE WK01-DATE-4005-CH6311-F TO WK01-DATE-4005-CH63-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH6A2-Z = 'Y'
               MOVE WK01-DATE-4005-CH6A2 TO END-DATE-4005-CH63 OF
                FCHWM63(0002)
           END-IF.
           MOVE WK01-DATE-4005-CH6A2-F TO WK01-DATE-4005-CH6A-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH6A3-Z = 'Y'
               MOVE WK01-DATE-4005-CH6A3 TO END-DATE-4005-CH63 OF
                FCHWM63(0003)
           END-IF.
           MOVE WK01-DATE-4005-CH6A3-F TO WK01-DATE-4005-CH6A-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH6A4-Z = 'Y'
               MOVE WK01-DATE-4005-CH6A4 TO END-DATE-4005-CH63 OF
                FCHWM63(0004)
           END-IF.
           MOVE WK01-DATE-4005-CH6A4-F TO WK01-DATE-4005-CH6A-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH6A5-Z = 'Y'
               MOVE WK01-DATE-4005-CH6A5 TO END-DATE-4005-CH63 OF
                FCHWM63(0005)
           END-IF.
           MOVE WK01-DATE-4005-CH6A5-F TO WK01-DATE-4005-CH6A-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH6A6-Z = 'Y'
               MOVE WK01-DATE-4005-CH6A6 TO END-DATE-4005-CH63 OF
                FCHWM63(0006)
           END-IF.
           MOVE WK01-DATE-4005-CH6A6-F TO WK01-DATE-4005-CH6A-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH6A7-Z = 'Y'
               MOVE WK01-DATE-4005-CH6A7 TO END-DATE-4005-CH63 OF
                FCHWM63(0007)
           END-IF.
           MOVE WK01-DATE-4005-CH6A7-F TO WK01-DATE-4005-CH6A-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH6A8-Z = 'Y'
               MOVE WK01-DATE-4005-CH6A8 TO END-DATE-4005-CH63 OF
                FCHWM63(0008)
           END-IF.
           MOVE WK01-DATE-4005-CH6A8-F TO WK01-DATE-4005-CH6A-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH6A9-Z = 'Y'
               MOVE WK01-DATE-4005-CH6A9 TO END-DATE-4005-CH63 OF
                FCHWM63(0009)
           END-IF.
           MOVE WK01-DATE-4005-CH6A9-F TO WK01-DATE-4005-CH6A-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH6A10-Z = 'Y'
               MOVE WK01-DATE-4005-CH6A10 TO END-DATE-4005-CH63 OF
                FCHWM63(0010)
           END-IF.
           MOVE WK01-DATE-4005-CH6A10-F TO WK01-DATE-4005-CH6A-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4005-CH6A11-Z = 'Y'
               MOVE WK01-DATE-4005-CH6A11 TO END-DATE-4005-CH63 OF
                FCHWM63(0011)
           END-IF.
           MOVE WK01-DATE-4005-CH6A11-F TO WK01-DATE-4005-CH6A-F (11).
