       976-FROM-R4092-RCVNG-MTHD SECTION.

FCCCE**        MOVE USER-CODE-4092 OF R4092-RCVNG-MTHD
FCCCE          MOVE DBLINK-USER-CD
               TO RCM-USER-CD.

               MOVE LAST-ACTVY-DATE-4092 OF R4092-RCVNG-MTHD
               TO RCM-LAST-ACTVY-DT.

               MOVE UNVRS-CODE-4092 OF R4092-RCVNG-MTHD
               TO RCM-UNVRS-CD.

               MOVE RCVG-MTHD-CODE-4092 OF R4092-RCVNG-MTHD
               TO RCM-RCVG-MTHD-CD.

               MOVE RCVG-MTHD-DESC-4092 OF R4092-RCVNG-MTHD
               TO RCM-RCVG-MTHD-DESC.

           IF START-DATE-4092 OF R4092-RCVNG-MTHD  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO RCM-START-DT
           ELSE
               MOVE START-DATE-4092 OF R4092-RCVNG-MTHD
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO RCM-START-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO RCM-START-DT
               END-IF
           END-IF.

           IF END-DATE-4092 OF R4092-RCVNG-MTHD  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO RCM-END-DT
           ELSE
               MOVE END-DATE-4092 OF R4092-RCVNG-MTHD
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO RCM-END-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO RCM-END-DT
               END-IF
           END-IF.

           IF ACTVY-DATE-4092 OF R4092-RCVNG-MTHD  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO RCM-ACTVY-DT
           ELSE
               MOVE ACTVY-DATE-4092 OF R4092-RCVNG-MTHD
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO RCM-ACTVY-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO RCM-ACTVY-DT
               END-IF
           END-IF.
       976-FROM-R4092-RCVNG-MTHD-EXIT.
           EXIT.
