      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FTAWD05                              04/24/00  13:19  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FTAWD05.
DEVBWS     02  SAVE-DBKEY-ID-4400-TA05                  PIC X(5).
DEVBWS     02  SAVE-DBKEY-ID-TEMP-TA05                  PIC X(5).
DEVBWS     02  SAVE-DBKEY-ID-4405-TA05                  PIC X(8).
DEVBWS     02  SAVE-DBKEY-ID-4408-TA05      OCCURS 3    PIC X(11).
DEVBWS     02  SAVE-DBKEY-PLUS-ONE-TA05                 PIC X(11).
DEVBWS     02  SAVE-DBKEY-MINUS-ONE-TA05                PIC X(11).
           02  VALID-RULE-FLAG-TA05                     PIC X(1).
           02  VALID-ACCT-FLAG-TA05                     PIC X(1).
           02  FSCL-YR-TA05                 OCCURS 3    PIC X(2).
           02  PSTNG-PRD-TA05               OCCURS 3    PIC X(2).
           02  NSF-RQRD-TA05                OCCURS 3    PIC X(1).
           02  WORK-AMT-TA05         COMP-3 OCCURS 3    PIC 9(10)V99.
           02  SEQ-NMBR-TA05                            PIC 9(4).
           02  SYSTEM-DATE-TA05               COMP-3    PIC S9(8).
           02  ERR-FLAG-TA05                            PIC X(1).
           02  SUSP-FLAG-TA05                           PIC X(1).
           02  ACTN-ERR-FLAG-TA05                       PIC X(1).
           02  PAGE-STATUS-CODE-TA05                    PIC X(1).
           02  SET-SORT-KEY-4012-TA05.
               03  OPTN-1-CODE-TA05                     PIC X(8).
               03  OPTN-2-CODE-TA05                     PIC X(8).
               03  LEVEL-NMBR-TA05                      PIC 9(2).
           02  RULE-CLASS-CODE-TA05                     PIC X(4).
           02  EFCTV-KEY-TA05.
               03  START-DATE-TA05            COMP-3    PIC S9(8).
               03  TIME-STAMP-TA05                      PIC X(6).
           02  COPY-FLAG-TA05                           PIC X(1).
           02  VALID-TRIP-FLAG-TA05                     PIC X(1).
           02  APRVL-IND-TA05                           PIC X(1).
           02  APRVL-TMPLT-CODE-TA05                    PIC X(3).
           02  ACCT-WARNING-IND-TA05                    PIC X(1).
           02  FUND-WARNING-IND-TA05                    PIC X(1).
