      * /************************************************************/  $
      * /*   CHANGE LOG -- CPWSXBDS                                 */  $
      * /*   NAME            DATE      COMMENTS                     */  $
      * /*..........................................................*/  $
      * /*   B. ORTIZ       05/07/98   MODIFIED FOR GFAU (REL027)   */  $
      * /*                                                          */  $
      * /************************************************************/  $
      *01  XBDS-BUDGET-DETAIL.                                          CPWSXBDS
           05  XBDS-KEY.                                                CPWSXBDS
               10  XBDS-FILE-KEY.                                       CPWSXBDS
      ****         15  XBDS-LOCATION           PIC X(02).               3202RRRR
      ****         15  XBDS-SAU                PIC X(01).               3202RRRR
      ****         15  XBDS-SUB-CAMPUS         PIC X(01).               3202RRRR
      ****         15  XBDS-ACCOUNT            PIC X(07).               3202RRRR
      ****         15  XBDS-FUND               PIC X(06).               3202RRRR
      ****         15  XBDS-SUB-BUDGET         PIC X(02).               3202RRRR
000910             15  XBDS-FAU.                                        3202RRRR
000920                 20  XBDS-LOCATION       PIC X(02).               3202RRRR
000930                 20  XBDS-SAU            PIC X(01).               3202RRRR
000940                 20  XBDS-SUB-CAMPUS     PIC X(01).               3202RRRR
000950                 20  XBDS-ACCOUNT        PIC X(07).               3202RRRR
000960                 20  XBDS-FUND           PIC X(06).               3202RRRR
000970                 20  XBDS-SUB-BUDGET     PIC X(02).               3202RRRR
                   15  XBDS-SEGMENT-TYPE       PIC X(01).               CPWSXBDS
               10  XBDS-SEGMENT-KEY.                                    CPWSXBDS
                   15  XBDS-DOCUMENT-NUMBER    PIC X(05).               CPWSXBDS
                   15  XBDS-DOCUMENT-SUFFIX    PIC X(02).               CPWSXBDS
                   15  XBDS-TRANSACTION-CLASS  PIC X(01).               CPWSXBDS
                   15  XBDS-TRANSACTION-TYPE   PIC X(02).               CPWSXBDS
                   15  XBDS-DETAIL-AMOUNT      PIC S9(10).              CPWSXBDS
           05  XBDS-DETAIL-FTE                 PIC S9(4)V99.            CPWSXBDS
           05  XBDS-DETAIL-TEXT                PIC X(30).               CPWSXBDS
           05  XBDS-POSTING-DATE.                                       CPWSXBDS
               10  XBDS-POSTING-YEAR           PIC X(02).               CPWSXBDS
               10  XBDS-POSTING-MONTH          PIC X(02).               CPWSXBDS
               10  XBDS-POSTING-DAY            PIC X(02).               CPWSXBDS
           05  XBDS-CURRENT-FILLER             PIC X(33).               CPWSXBDS
           05  XBDS-IFIS-IFOAPAL.
               10  XBDS-IFIS-INDEX             PIC X(10).
               10  XBDS-IFIS-FUND              PIC X(06).
               10  XBDS-IFIS-ORG               PIC X(06).
               10  XBDS-IFIS-ACCOUNT           PIC X(06).
               10  XBDS-IFIS-PROGRAM           PIC X(06).
               10  XBDS-IFIS-ACTIVITY          PIC X(06).
               10  XBDS-IFIS-LOCATION          PIC X(06).
           05  XBDS-FILLER2                    PIC X(245).              CPWSXBDS
