       975-TO-R4025-PRFL-KEY SECTION.
           MOVE PFK-USER-CD TO
               USER-CODE-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-FIMS-USER TO
               FIMS-USER-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-UNVRS-CD TO
               UNVRS-CODE-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-SESN-CD TO
               SESN-CODE-2018-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-SBRDT-TERM-CD TO
               SBRDT-TERM-CODE-2056-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-CENSUS-TYP-CD TO
               CENSUS-TYPE-CODE-2060-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-PRSN-ID-DGT-ONE TO
               PRSN-ID-DIGIT-ONE-2157-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-PRSN-ID-LST-NINE TO
               PRSN-ID-LAST-NINE-2157-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ENTY-ID-DGT-ONE TO
               ENTY-ID-DIGIT-ONE-2186-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ENTY-ID-LST-NINE TO
               ENTY-ID-LAST-NINE-2186-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-CNTRY-CD-2153 TO
               CNTRY-CODE-2153-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ADR-TYP-CD-2137 TO
               ADR-TYPE-CODE-2137-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-WORK-DT TO
               WORK-DATE-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-TXT-TYP-CD TO
               TEXT-TYPE-CODE-2033-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-SCRN-CD TO
               SCRN-CODE-7002-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-SCRTY-USER-CD TO
               SCRTY-USER-CODE-7005-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-TMPLT-CD TO
               TMPLT-CODE-7007-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-PRFL-CD TO
               PRFL-CODE-7013-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-SRC-PRSN-SHORT-NM TO
               SRCE-PRSN-SHORT-NAME-2393-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-RG-QUEUE-REC-ID TO
               RG-QUEUE-REC-ID-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-PAGE-NUM TO
               PAGE-NUM-2531-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-START-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-PARM TO
               START-DATE-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-CLOSE-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-PARM TO
               CLOSE-DATE-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-VNDR-ID-DGT-ONE TO
               VNDR-ID-DIGIT-ONE-4073-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-VNDR-ID-LST-NINE TO
               VNDR-ID-LAST-NINE-4073-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-RQST-CD TO
               RQST-CODE-4070-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ITEM-NBR-4071 TO
               ITEM-NMBR-4071-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-SEQ-NBR-4080 TO
               SEQ-NMBR-4080-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-PO-NBR TO
               PO-NMBR-4083-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ITEM-NBR-4085 TO
               ITEM-NMBR-4085-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-SEQ-NBR-4061 TO
               SEQ-NMBR-4061-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-BID-NBR TO
               BID-NMBR-4088-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ITEM-NBR-4089 TO
               ITEM-NMBR-4089-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-BUYER-CD TO
               BUYER-CODE-4072-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-AGRMT-CD TO
               AGRMT-CODE-4081-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-CMDTY-CD TO
               CMDTY-CODE-4074-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-SHIP-TO-CD TO
               SHIP-TO-CODE-4086-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-RTRN-RSN-CD TO
               RTRN-RSN-CODE-4201-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ORGN-CD-4002 TO
               ORGZN-CODE-4002-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-DOC-NBR-4150 TO
               DCMNT-NMBR-4150-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-CHECK-NBR TO
               CHECK-NMBR-4158-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-BANK-ACCT-CD-4158 TO
               BANK-ACCT-CODE-4158-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-TAX-RATE-CD TO
               TAX-RATE-CODE-4168-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-DSCNT-CD TO
               DSCNT-CODE-4161-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-VNDR-TYP-CD TO
               VNDR-TYPE-CODE-4166-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ADJMT-CD TO
               ADJMT-CODE-4156-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-INCM-TYP-CD TO
               INCM-TYPE-CODE-4155-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ADR-TYP-CD-4073 TO
               ADR-TYPE-CODE-4073-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ITEM-NBR-4151 TO
               ITEM-NMBR-4151-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-SEQ-NBR-4152 TO
               SEQ-NMBR-4152-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-COA-CD TO
               COA-CODE-4000-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-FUND-CD TO
               FUND-CODE-4001-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ACCT-CD TO
               ACCT-CODE-4003-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-PRGRM-CD TO
               PRGRM-CODE-4004-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-LCTN-CD TO
               LCTN-CODE-4006-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-FUND-TYP-CD TO
               FUND-TYPE-CODE-4008-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ACCT-TYP-CD TO
               ACCT-TYPE-CODE-4014-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-MGR-DGT-ONE TO
               MGR-DIGIT-ONE-4019-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-MGR-LST-NINE TO
               MGR-LAST-NINE-4019-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-CNTL-ACCT-TYP TO
               CNTRL-ACCT-TYPE-4021-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-FSCL-YR TO
               FSCL-YR-4024-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-INDRT-COST-CD TO
               INDRT-COST-CODE-4026-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-COST-SHR-CD TO
               COST-SHARE-CODE-4030-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-BANK-ACCT-CD-4033 TO
               BANK-ACCT-CODE-4033-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-PAY-MTHD-CD TO
               PAY-MTHD-CODE-4035-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ACCT-INDX-CD TO
               ACCT-INDX-CODE-4045-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-BANK-ID-DGT-ONE TO
               BANK-ID-DIGIT-ONE-4046-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-BANK-ID-LST-NINE TO
               BANK-ID-LAST-NINE-4046-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ACTVY-CD TO
               ACTVY-CODE-4050-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-XTRNL-ENTY-CD TO
               XTRNL-ENTY-CODE-4052-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-INTRL-ENTY-CD TO
               INTRL-ENTY-CODE-4053-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-AGNCY-DGT-ONE TO
               AGNCY-DIGIT-ONE-4056-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-AGNCY-LST-NINE TO
               AGNCY-LAST-NINE-4056-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-RPRT-RQST-ID TO
               RPRT-RQST-ID-4057-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-RPRT-CD TO
               RPRT-CODE-4060-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-RULE-CLS-CD TO
               RULE-CLASS-CODE-4180-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-USER-ID-4192 TO
               USER-ID-4192-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-RCRD-NBR-4036 TO
               RCRD-NMBR-4036-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ELMNT-VALUE TO
               ELMNT-VALUE-4036-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-DATA-CD TO
               DATA-CODE-4038-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-FIMS-ENTY-CD TO
               FIMS-ENTY-CODE-4011-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ELMNT-NM TO
               ELMNT-NAME-4011-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ENCMB-NBR TO
               ENCMBRNC-NMBR-4191-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-AUTO-JRNL-ID TO
               AUTO-JRNL-ID-4106-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ACTG-PRD TO
               ACTG-PRD-4068-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-DOC-NBR-4069 TO
               DCMNT-NMBR-4069-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ADR-TYP-CD-2185 TO
               ADR-TYPE-CODE-2185-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-INTRL-CD TO
               INTRL-CODE-4055-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-XTRNL-CD TO
               XTRNL-CODE-4054-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-CHNG-SEQ-NBR TO
               CHNG-SEQ-NMBR-4096-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-PO-RTRN-CD TO
               PO-RTRN-CODE-4099-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-DOC-TYP-4203 TO
               DCMNT-TYPE-4203-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-CLAUSE-CD TO
               CLAUSE-CODE-4209-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-PRJCT-CD TO
               PRJCT-CODE-4132-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-CSTMR-DGT-ONE TO
               CSTMR-DIGIT-ONE-4130-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-CSTMR-LST-NINE TO
               CSTMR-LAST-NINE-4130-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-EMPLY-DGT-ONE TO
               EMPLY-DIGIT-ONE-4134-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-EMPLY-LST-NINE TO
               EMPLY-LAST-NINE-4134-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-EQPMT-CD TO
               EQPMT-CODE-4139-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-INVTY-CD TO
               INVTY-CODE-4137-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-COST-TYP TO
               COST-TYPE-4146-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-RATE-CD TO
               RATE-CODE-4128-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-USER-ID-4206 TO
               USER-ID-4206-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-STATE-CD TO
               STATE-CODE-2151-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-CNTRY-CD-2152 TO
               CNTRY-CODE-2152-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-TRVL-ACCT-DGT-ONE TO
               TRVL-ACCT-DIGIT-ONE-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-TRVL-ACCT-LST-NINE TO
               TRVL-ACCT-LAST-NINE-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-EVENT-NBR TO
               EVENT-NMBR-4401-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-TRIP-NBR TO
               TRIP-NMBR-4402-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ADVNC-NBR TO
               ADVNC-NMBR-4404-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-EXPNS-NBR TO
               EXPNS-NMBR-4405-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-RCNCLTN-TYP TO
               RCNCLTN-TYPE-4415-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ADVNC-TYP TO
               ADVNC-TYPE-4417-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-DOC-NBR-4410 TO
               DCMNT-NMBR-4410-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-CHECK-RQST-ISEQ TO
               CHECK-RQST-INTRL-SEQ-4157-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-PRNTR-DEST-CD TO
               PRNTR-DEST-CODE-4167-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-ORGN-CD-4162 TO
               ORGN-CODE-4162-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-BANK-ACCT-CD-4162 TO
               BANK-ACCT-CODE-4162-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-NEXT-SCRN TO
               NEXT-SCRN-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-DOC-TYP-4191 TO
               DCMNT-TYPE-4191-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-APV-TPLT-CD TO
               APRVL-TMPLT-CODE-4215-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-DOC-NBR-4174 TO
               DCMNT-NMBR-4174-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-SEQ-NBR-4175 TO
               SEQ-NMBR-4175-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-RELEASE-NBR TO
               RELEASE-NMBR-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-FILE-NBR TO
               FILE-NMBR-4025 OF R4025-PRFL-KEY
                                                                     .
           MOVE PFK-LINE-NBR TO
               LINE-NMBR-4025 OF R4025-PRFL-KEY
                                                                     .
       975-TO-R4025-PRFL-KEY-EXIT.
           EXIT.
