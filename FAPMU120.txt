    ***Created by Convert/DC version V8R03 on 12/04/00 at 15:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY01-Z = 'Y'
               MOVE WK01-TEXT-SY01 TO DCSD-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0A-Z = 'Y'
               MOVE WK01-TEXT-SY0A TO CNFDL-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0B-Z = 'Y'
               MOVE WK01-TEXT-SY0B TO HOLDS-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP12-Z = 'Y'
               MOVE WK01-CODE-AP12 TO ACTN-CODE-AP12 OF FAPWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4161-AP12-Z = 'Y'
               MOVE WK01-CODE-4161-AP12 TO DSCNT-CODE-4161-AP12 OF
                FAPWK12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4172-AP12-Z = 'Y'
               MOVE WK01-DESC-4172-AP12 TO DSCNT-DESC-4172-AP12 OF
                FAPWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4172-AP12-Z = 'Y'
               MOVE WK01-DATE-4172-AP12 TO START-DATE-4172-AP12 OF
                FAPWK12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CHNG-DATE-AP12-Z = 'Y'
               MOVE WK01-CHNG-DATE-AP12 TO NEXT-CHNG-DATE-AP12 OF
                FAPWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-AP12-Z = 'Y'
               MOVE WK01-DESC-AP12 TO STATUS-DESC-AP12 OF FAPWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4172-AP1A-Z = 'Y'
               MOVE WK01-DATE-4172-AP1A TO END-DATE-4172-AP12 OF FAPWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4172-AP1B-Z = 'Y'
               MOVE WK01-DATE-4172-AP1B TO ACTVY-DATE-4172-AP12 OF
                FAPWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STAMP-4172-AP12-Z = 'Y'
               MOVE WK01-STAMP-4172-AP12 TO TIME-STAMP-4172-AP12 OF
                FAPWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-AP12-Z = 'Y'
               MOVE WK01-PM-FLAG-AP12 TO AM-PM-FLAG-AP12 OF FAPWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PCT-4172-AP12-Z = 'Y'
               MOVE WK01-PCT-4172-AP12 TO DSCNT-PCT-4172-AP12 OF FAPWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DAYS-4172-AP12-Z = 'Y'
               MOVE WK01-DAYS-4172-AP12 TO DSCNT-DAYS-4172-AP12 OF
                FAPWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DAYS-4172-AP1A-Z = 'Y'
               MOVE WK01-DAYS-4172-AP1A TO NET-DAYS-4172-AP12 OF FAPWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-OF-MONTH-IND-4172-AP12-Z = 'Y'
               MOVE WK01-OF-MONTH-IND-4172-AP12 TO
                END-OF-MONTH-IND-4172-AP12 OF FAPWM12
           END-IF.
