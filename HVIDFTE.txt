       975-TO-R4009-FNDT-EFCTV SECTION.
           MOVE FTE-USER-CD TO
               USER-CODE-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-START-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               START-DATE-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-TIME-STMP TO
               DBLINKX-INPUT-PARM
           MOVE '4' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-TIME-FORMAT
           MOVE '51' TO DBLINKX-IDMS-TIME-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-PARM TO
               TIME-STAMP-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-END-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               END-DATE-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-STATUS TO
               STATUS-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-FUND-TYP-DESC TO
               FUND-TYPE-DESC-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-INTRL-FUND-TYP-CD TO
               INTRL-FUND-TYPE-CODE-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-CPTLZN-FUND-CD TO
               CPTLZN-FUND-CODE-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-CPTLZN-ACCT-CD TO
               CPTLZN-ACCT-CODE-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-ACCT-IX-BDGT-CNTL TO
               ACCT-INDX-BDGT-CNTRL-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-FUND-BDGT-CNTL TO
               FUND-BDGT-CNTRL-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-ORGN-BDGT-CNTL TO
               ORGZN-BDGT-CNTRL-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-ACCT-BDGT-CNTL TO
               ACCT-BDGT-CNTRL-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-PRGRM-BDGT-CNTL TO
               PRGRM-BDGT-CNTRL-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-CNTL-PRD-CD TO
               CNTRL-PRD-CODE-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-CNTL-SVRTY-CD TO
               CNTRL-SVRTY-CODE-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-PRDCSR-FUND-TYP TO
               PREDCSR-FUND-TYPE-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-SBRDT-FUND-TYP TO
               SBRDT-FUND-TYPE-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-DFLT-FROM-IND TO
               DFLT-FROM-IND-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-ENCMB-JRNL-TYP TO
               ENCMBRNC-JRNL-TYPE-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-CMTMNT-TYP TO
               CMTMNT-TYPE-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-ROLL-BDGT-IND TO
               ROLL-BDGT-IND-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-BDGT-DSPSN TO
               BDGT-DSPSN-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-ENCMB-PCT TO
               ENCMBRNC-PCT-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-BDGT-JRNL-TYP TO
               BDGT-JRNL-TYPE-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-BDGT-CLSFN TO
               BDGT-CLSFN-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-CARRY-FWD-TYP TO
               CARRY-FRWRD-TYPE-4009 OF R4009-FNDT-EFCTV
                                                                     .
           MOVE FTE-BDGT-PCT TO
               BDGT-PCT-4009 OF R4009-FNDT-EFCTV
                                                                     .
       975-TO-R4009-FNDT-EFCTV-EXIT.
           EXIT.
