      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWK13                              04/24/00  12:39  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWK13.
           02  PO-NMBR-4083-PU13                        PIC X(8).
           02  CHNG-SEQ-NMBR-4096-PU13                  PIC X(3).
           02  RQST-ITEM-NMBR-PU13                      PIC X(4).
           02  EFCTV-RCRD-KEY-4154-PU13.
               03  KEY-TAX-TYPE-4154-PU13               PIC X(1).
               03  KEY-TAX-CODE-IND-4154-PU13           PIC X(1).
               03  KEY-START-DATE-4154-PU13   COMP-3    PIC S9(8).
               03  KEY-TIME-STAMP-4154-PU13             PIC X(6).
