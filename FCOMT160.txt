    ***Created by Convert/DC version V8R03 on 12/13/00 at 15:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FZYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-CODE-CO16-Z = 'Y'
               MOVE WK01-USER-CODE-CO16 TO RQST-USER-CODE-CO16 OF
                FCOWK16
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CO161-Z = 'Y'
               MOVE WK01-CODE-CO161 TO ACTN-CODE-CO16 OF FCOWM16(0001)
           END-IF.
           MOVE WK01-CODE-CO161-F TO WK01-CODE-CO16-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CO162-Z = 'Y'
               MOVE WK01-CODE-CO162 TO ACTN-CODE-CO16 OF FCOWM16(0002)
           END-IF.
           MOVE WK01-CODE-CO162-F TO WK01-CODE-CO16-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CO163-Z = 'Y'
               MOVE WK01-CODE-CO163 TO ACTN-CODE-CO16 OF FCOWM16(0003)
           END-IF.
           MOVE WK01-CODE-CO163-F TO WK01-CODE-CO16-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CO164-Z = 'Y'
               MOVE WK01-CODE-CO164 TO ACTN-CODE-CO16 OF FCOWM16(0004)
           END-IF.
           MOVE WK01-CODE-CO164-F TO WK01-CODE-CO16-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CO165-Z = 'Y'
               MOVE WK01-CODE-CO165 TO ACTN-CODE-CO16 OF FCOWM16(0005)
           END-IF.
           MOVE WK01-CODE-CO165-F TO WK01-CODE-CO16-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CO166-Z = 'Y'
               MOVE WK01-CODE-CO166 TO ACTN-CODE-CO16 OF FCOWM16(0006)
           END-IF.
           MOVE WK01-CODE-CO166-F TO WK01-CODE-CO16-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CO167-Z = 'Y'
               MOVE WK01-CODE-CO167 TO ACTN-CODE-CO16 OF FCOWM16(0007)
           END-IF.
           MOVE WK01-CODE-CO167-F TO WK01-CODE-CO16-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CO168-Z = 'Y'
               MOVE WK01-CODE-CO168 TO ACTN-CODE-CO16 OF FCOWM16(0008)
           END-IF.
           MOVE WK01-CODE-CO168-F TO WK01-CODE-CO16-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CO169-Z = 'Y'
               MOVE WK01-CODE-CO169 TO ACTN-CODE-CO16 OF FCOWM16(0009)
           END-IF.
           MOVE WK01-CODE-CO169-F TO WK01-CODE-CO16-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CO1610-Z = 'Y'
               MOVE WK01-CODE-CO1610 TO ACTN-CODE-CO16 OF FCOWM16(0010)
           END-IF.
           MOVE WK01-CODE-CO1610-F TO WK01-CODE-CO16-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CO1611-Z = 'Y'
               MOVE WK01-CODE-CO1611 TO ACTN-CODE-CO16 OF FCOWM16(0011)
           END-IF.
           MOVE WK01-CODE-CO1611-F TO WK01-CODE-CO16-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CO1612-Z = 'Y'
               MOVE WK01-CODE-CO1612 TO ACTN-CODE-CO16 OF FCOWM16(0012)
           END-IF.
           MOVE WK01-CODE-CO1612-F TO WK01-CODE-CO16-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-6605-CO161-Z = 'Y'
               MOVE WK01-REF-NMBR-6605-CO161 TO COMM-REF-NMBR-6605-CO16
                OF FCOWM16(0001)
           END-IF.
           MOVE WK01-REF-NMBR-6605-CO161-F TO WK01-REF-NMBR-6605-CO16-F
                (1).
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-6605-CO162-Z = 'Y'
               MOVE WK01-REF-NMBR-6605-CO162 TO COMM-REF-NMBR-6605-CO16
                OF FCOWM16(0002)
           END-IF.
           MOVE WK01-REF-NMBR-6605-CO162-F TO WK01-REF-NMBR-6605-CO16-F
                (2).
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-6605-CO163-Z = 'Y'
               MOVE WK01-REF-NMBR-6605-CO163 TO COMM-REF-NMBR-6605-CO16
                OF FCOWM16(0003)
           END-IF.
           MOVE WK01-REF-NMBR-6605-CO163-F TO WK01-REF-NMBR-6605-CO16-F
                (3).
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-6605-CO164-Z = 'Y'
               MOVE WK01-REF-NMBR-6605-CO164 TO COMM-REF-NMBR-6605-CO16
                OF FCOWM16(0004)
           END-IF.
           MOVE WK01-REF-NMBR-6605-CO164-F TO WK01-REF-NMBR-6605-CO16-F
                (4).
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-6605-CO165-Z = 'Y'
               MOVE WK01-REF-NMBR-6605-CO165 TO COMM-REF-NMBR-6605-CO16
                OF FCOWM16(0005)
           END-IF.
           MOVE WK01-REF-NMBR-6605-CO165-F TO WK01-REF-NMBR-6605-CO16-F
                (5).
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-6605-CO166-Z = 'Y'
               MOVE WK01-REF-NMBR-6605-CO166 TO COMM-REF-NMBR-6605-CO16
                OF FCOWM16(0006)
           END-IF.
           MOVE WK01-REF-NMBR-6605-CO166-F TO WK01-REF-NMBR-6605-CO16-F
                (6).
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-6605-CO167-Z = 'Y'
               MOVE WK01-REF-NMBR-6605-CO167 TO COMM-REF-NMBR-6605-CO16
                OF FCOWM16(0007)
           END-IF.
           MOVE WK01-REF-NMBR-6605-CO167-F TO WK01-REF-NMBR-6605-CO16-F
                (7).
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-6605-CO168-Z = 'Y'
               MOVE WK01-REF-NMBR-6605-CO168 TO COMM-REF-NMBR-6605-CO16
                OF FCOWM16(0008)
           END-IF.
           MOVE WK01-REF-NMBR-6605-CO168-F TO WK01-REF-NMBR-6605-CO16-F
                (8).
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-6605-CO169-Z = 'Y'
               MOVE WK01-REF-NMBR-6605-CO169 TO COMM-REF-NMBR-6605-CO16
                OF FCOWM16(0009)
           END-IF.
           MOVE WK01-REF-NMBR-6605-CO169-F TO WK01-REF-NMBR-6605-CO16-F
                (9).
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-6605-CO1610-Z = 'Y'
               MOVE WK01-REF-NMBR-6605-CO1610 TO
                COMM-REF-NMBR-6605-CO16 OF FCOWM16(0010)
           END-IF.
           MOVE WK01-REF-NMBR-6605-CO1610-F TO
                WK01-REF-NMBR-6605-CO16-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-6605-CO1611-Z = 'Y'
               MOVE WK01-REF-NMBR-6605-CO1611 TO
                COMM-REF-NMBR-6605-CO16 OF FCOWM16(0011)
           END-IF.
           MOVE WK01-REF-NMBR-6605-CO1611-F TO
                WK01-REF-NMBR-6605-CO16-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-REF-NMBR-6605-CO1612-Z = 'Y'
               MOVE WK01-REF-NMBR-6605-CO1612 TO
                COMM-REF-NMBR-6605-CO16 OF FCOWM16(0012)
           END-IF.
           MOVE WK01-REF-NMBR-6605-CO1612-F TO
                WK01-REF-NMBR-6605-CO16-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6605-CO161-Z = 'Y'
               MOVE WK01-CODE-6605-CO161 TO TEXT-CODE-6605-CO16 OF
                FCOWM16(0001)
           END-IF.
           MOVE WK01-CODE-6605-CO161-F TO WK01-CODE-6605-CO16-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6605-CO162-Z = 'Y'
               MOVE WK01-CODE-6605-CO162 TO TEXT-CODE-6605-CO16 OF
                FCOWM16(0002)
           END-IF.
           MOVE WK01-CODE-6605-CO162-F TO WK01-CODE-6605-CO16-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6605-CO163-Z = 'Y'
               MOVE WK01-CODE-6605-CO163 TO TEXT-CODE-6605-CO16 OF
                FCOWM16(0003)
           END-IF.
           MOVE WK01-CODE-6605-CO163-F TO WK01-CODE-6605-CO16-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6605-CO164-Z = 'Y'
               MOVE WK01-CODE-6605-CO164 TO TEXT-CODE-6605-CO16 OF
                FCOWM16(0004)
           END-IF.
           MOVE WK01-CODE-6605-CO164-F TO WK01-CODE-6605-CO16-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6605-CO165-Z = 'Y'
               MOVE WK01-CODE-6605-CO165 TO TEXT-CODE-6605-CO16 OF
                FCOWM16(0005)
           END-IF.
           MOVE WK01-CODE-6605-CO165-F TO WK01-CODE-6605-CO16-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6605-CO166-Z = 'Y'
               MOVE WK01-CODE-6605-CO166 TO TEXT-CODE-6605-CO16 OF
                FCOWM16(0006)
           END-IF.
           MOVE WK01-CODE-6605-CO166-F TO WK01-CODE-6605-CO16-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6605-CO167-Z = 'Y'
               MOVE WK01-CODE-6605-CO167 TO TEXT-CODE-6605-CO16 OF
                FCOWM16(0007)
           END-IF.
           MOVE WK01-CODE-6605-CO167-F TO WK01-CODE-6605-CO16-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6605-CO168-Z = 'Y'
               MOVE WK01-CODE-6605-CO168 TO TEXT-CODE-6605-CO16 OF
                FCOWM16(0008)
           END-IF.
           MOVE WK01-CODE-6605-CO168-F TO WK01-CODE-6605-CO16-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6605-CO169-Z = 'Y'
               MOVE WK01-CODE-6605-CO169 TO TEXT-CODE-6605-CO16 OF
                FCOWM16(0009)
           END-IF.
           MOVE WK01-CODE-6605-CO169-F TO WK01-CODE-6605-CO16-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6605-CO1610-Z = 'Y'
               MOVE WK01-CODE-6605-CO1610 TO TEXT-CODE-6605-CO16 OF
                FCOWM16(0010)
           END-IF.
           MOVE WK01-CODE-6605-CO1610-F TO WK01-CODE-6605-CO16-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6605-CO1611-Z = 'Y'
               MOVE WK01-CODE-6605-CO1611 TO TEXT-CODE-6605-CO16 OF
                FCOWM16(0011)
           END-IF.
           MOVE WK01-CODE-6605-CO1611-F TO WK01-CODE-6605-CO16-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6605-CO1612-Z = 'Y'
               MOVE WK01-CODE-6605-CO1612 TO TEXT-CODE-6605-CO16 OF
                FCOWM16(0012)
           END-IF.
           MOVE WK01-CODE-6605-CO1612-F TO WK01-CODE-6605-CO16-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-6605-CO161-Z = 'Y'
               MOVE WK01-FLAG-6605-CO161 TO PRNTD-FLAG-6605-CO16 OF
                FCOWM16(0001)
           END-IF.
           MOVE WK01-FLAG-6605-CO161-F TO WK01-FLAG-6605-CO16-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-6605-CO162-Z = 'Y'
               MOVE WK01-FLAG-6605-CO162 TO PRNTD-FLAG-6605-CO16 OF
                FCOWM16(0002)
           END-IF.
           MOVE WK01-FLAG-6605-CO162-F TO WK01-FLAG-6605-CO16-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-6605-CO163-Z = 'Y'
               MOVE WK01-FLAG-6605-CO163 TO PRNTD-FLAG-6605-CO16 OF
                FCOWM16(0003)
           END-IF.
           MOVE WK01-FLAG-6605-CO163-F TO WK01-FLAG-6605-CO16-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-6605-CO164-Z = 'Y'
               MOVE WK01-FLAG-6605-CO164 TO PRNTD-FLAG-6605-CO16 OF
                FCOWM16(0004)
           END-IF.
           MOVE WK01-FLAG-6605-CO164-F TO WK01-FLAG-6605-CO16-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-6605-CO165-Z = 'Y'
               MOVE WK01-FLAG-6605-CO165 TO PRNTD-FLAG-6605-CO16 OF
                FCOWM16(0005)
           END-IF.
           MOVE WK01-FLAG-6605-CO165-F TO WK01-FLAG-6605-CO16-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-6605-CO166-Z = 'Y'
               MOVE WK01-FLAG-6605-CO166 TO PRNTD-FLAG-6605-CO16 OF
                FCOWM16(0006)
           END-IF.
           MOVE WK01-FLAG-6605-CO166-F TO WK01-FLAG-6605-CO16-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-6605-CO167-Z = 'Y'
               MOVE WK01-FLAG-6605-CO167 TO PRNTD-FLAG-6605-CO16 OF
                FCOWM16(0007)
           END-IF.
           MOVE WK01-FLAG-6605-CO167-F TO WK01-FLAG-6605-CO16-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-6605-CO168-Z = 'Y'
               MOVE WK01-FLAG-6605-CO168 TO PRNTD-FLAG-6605-CO16 OF
                FCOWM16(0008)
           END-IF.
           MOVE WK01-FLAG-6605-CO168-F TO WK01-FLAG-6605-CO16-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-6605-CO169-Z = 'Y'
               MOVE WK01-FLAG-6605-CO169 TO PRNTD-FLAG-6605-CO16 OF
                FCOWM16(0009)
           END-IF.
           MOVE WK01-FLAG-6605-CO169-F TO WK01-FLAG-6605-CO16-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-6605-CO1610-Z = 'Y'
               MOVE WK01-FLAG-6605-CO1610 TO PRNTD-FLAG-6605-CO16 OF
                FCOWM16(0010)
           END-IF.
           MOVE WK01-FLAG-6605-CO1610-F TO WK01-FLAG-6605-CO16-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-6605-CO1611-Z = 'Y'
               MOVE WK01-FLAG-6605-CO1611 TO PRNTD-FLAG-6605-CO16 OF
                FCOWM16(0011)
           END-IF.
           MOVE WK01-FLAG-6605-CO1611-F TO WK01-FLAG-6605-CO16-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-6605-CO1612-Z = 'Y'
               MOVE WK01-FLAG-6605-CO1612 TO PRNTD-FLAG-6605-CO16 OF
                FCOWM16(0012)
           END-IF.
           MOVE WK01-FLAG-6605-CO1612-F TO WK01-FLAG-6605-CO16-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-USER-CODE-6605-CO161-Z = 'Y'
               MOVE WK01-USER-CODE-6605-CO161 TO
                BATCH-USER-CODE-6605-CO16 OF FCOWM16(0001)
           END-IF.
           MOVE WK01-USER-CODE-6605-CO161-F TO
                WK01-USER-CODE-6605-CO16-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-USER-CODE-6605-CO162-Z = 'Y'
               MOVE WK01-USER-CODE-6605-CO162 TO
                BATCH-USER-CODE-6605-CO16 OF FCOWM16(0002)
           END-IF.
           MOVE WK01-USER-CODE-6605-CO162-F TO
                WK01-USER-CODE-6605-CO16-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-USER-CODE-6605-CO163-Z = 'Y'
               MOVE WK01-USER-CODE-6605-CO163 TO
                BATCH-USER-CODE-6605-CO16 OF FCOWM16(0003)
           END-IF.
           MOVE WK01-USER-CODE-6605-CO163-F TO
                WK01-USER-CODE-6605-CO16-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-USER-CODE-6605-CO164-Z = 'Y'
               MOVE WK01-USER-CODE-6605-CO164 TO
                BATCH-USER-CODE-6605-CO16 OF FCOWM16(0004)
           END-IF.
           MOVE WK01-USER-CODE-6605-CO164-F TO
                WK01-USER-CODE-6605-CO16-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-USER-CODE-6605-CO165-Z = 'Y'
               MOVE WK01-USER-CODE-6605-CO165 TO
                BATCH-USER-CODE-6605-CO16 OF FCOWM16(0005)
           END-IF.
           MOVE WK01-USER-CODE-6605-CO165-F TO
                WK01-USER-CODE-6605-CO16-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-USER-CODE-6605-CO166-Z = 'Y'
               MOVE WK01-USER-CODE-6605-CO166 TO
                BATCH-USER-CODE-6605-CO16 OF FCOWM16(0006)
           END-IF.
           MOVE WK01-USER-CODE-6605-CO166-F TO
                WK01-USER-CODE-6605-CO16-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-USER-CODE-6605-CO167-Z = 'Y'
               MOVE WK01-USER-CODE-6605-CO167 TO
                BATCH-USER-CODE-6605-CO16 OF FCOWM16(0007)
           END-IF.
           MOVE WK01-USER-CODE-6605-CO167-F TO
                WK01-USER-CODE-6605-CO16-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-USER-CODE-6605-CO168-Z = 'Y'
               MOVE WK01-USER-CODE-6605-CO168 TO
                BATCH-USER-CODE-6605-CO16 OF FCOWM16(0008)
           END-IF.
           MOVE WK01-USER-CODE-6605-CO168-F TO
                WK01-USER-CODE-6605-CO16-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-USER-CODE-6605-CO169-Z = 'Y'
               MOVE WK01-USER-CODE-6605-CO169 TO
                BATCH-USER-CODE-6605-CO16 OF FCOWM16(0009)
           END-IF.
           MOVE WK01-USER-CODE-6605-CO169-F TO
                WK01-USER-CODE-6605-CO16-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-USER-CODE-6605-CO1610-Z = 'Y'
               MOVE WK01-USER-CODE-6605-CO1610 TO
                BATCH-USER-CODE-6605-CO16 OF FCOWM16(0010)
           END-IF.
           MOVE WK01-USER-CODE-6605-CO1610-F TO
                WK01-USER-CODE-6605-CO16-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-USER-CODE-6605-CO1611-Z = 'Y'
               MOVE WK01-USER-CODE-6605-CO1611 TO
                BATCH-USER-CODE-6605-CO16 OF FCOWM16(0011)
           END-IF.
           MOVE WK01-USER-CODE-6605-CO1611-F TO
                WK01-USER-CODE-6605-CO16-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-USER-CODE-6605-CO1612-Z = 'Y'
               MOVE WK01-USER-CODE-6605-CO1612 TO
                BATCH-USER-CODE-6605-CO16 OF FCOWM16(0012)
           END-IF.
           MOVE WK01-USER-CODE-6605-CO1612-F TO
                WK01-USER-CODE-6605-CO16-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-DLT-FLAG-CO161-Z = 'Y'
               MOVE WK01-DLT-FLAG-CO161 TO CNFRM-DLT-FLAG-CO16 OF
                FCOWM16(0001)
           END-IF.
           MOVE WK01-DLT-FLAG-CO161-F TO WK01-DLT-FLAG-CO16-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DLT-FLAG-CO162-Z = 'Y'
               MOVE WK01-DLT-FLAG-CO162 TO CNFRM-DLT-FLAG-CO16 OF
                FCOWM16(0002)
           END-IF.
           MOVE WK01-DLT-FLAG-CO162-F TO WK01-DLT-FLAG-CO16-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DLT-FLAG-CO163-Z = 'Y'
               MOVE WK01-DLT-FLAG-CO163 TO CNFRM-DLT-FLAG-CO16 OF
                FCOWM16(0003)
           END-IF.
           MOVE WK01-DLT-FLAG-CO163-F TO WK01-DLT-FLAG-CO16-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DLT-FLAG-CO164-Z = 'Y'
               MOVE WK01-DLT-FLAG-CO164 TO CNFRM-DLT-FLAG-CO16 OF
                FCOWM16(0004)
           END-IF.
           MOVE WK01-DLT-FLAG-CO164-F TO WK01-DLT-FLAG-CO16-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DLT-FLAG-CO165-Z = 'Y'
               MOVE WK01-DLT-FLAG-CO165 TO CNFRM-DLT-FLAG-CO16 OF
                FCOWM16(0005)
           END-IF.
           MOVE WK01-DLT-FLAG-CO165-F TO WK01-DLT-FLAG-CO16-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DLT-FLAG-CO166-Z = 'Y'
               MOVE WK01-DLT-FLAG-CO166 TO CNFRM-DLT-FLAG-CO16 OF
                FCOWM16(0006)
           END-IF.
           MOVE WK01-DLT-FLAG-CO166-F TO WK01-DLT-FLAG-CO16-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DLT-FLAG-CO167-Z = 'Y'
               MOVE WK01-DLT-FLAG-CO167 TO CNFRM-DLT-FLAG-CO16 OF
                FCOWM16(0007)
           END-IF.
           MOVE WK01-DLT-FLAG-CO167-F TO WK01-DLT-FLAG-CO16-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DLT-FLAG-CO168-Z = 'Y'
               MOVE WK01-DLT-FLAG-CO168 TO CNFRM-DLT-FLAG-CO16 OF
                FCOWM16(0008)
           END-IF.
           MOVE WK01-DLT-FLAG-CO168-F TO WK01-DLT-FLAG-CO16-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DLT-FLAG-CO169-Z = 'Y'
               MOVE WK01-DLT-FLAG-CO169 TO CNFRM-DLT-FLAG-CO16 OF
                FCOWM16(0009)
           END-IF.
           MOVE WK01-DLT-FLAG-CO169-F TO WK01-DLT-FLAG-CO16-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DLT-FLAG-CO1610-Z = 'Y'
               MOVE WK01-DLT-FLAG-CO1610 TO CNFRM-DLT-FLAG-CO16 OF
                FCOWM16(0010)
           END-IF.
           MOVE WK01-DLT-FLAG-CO1610-F TO WK01-DLT-FLAG-CO16-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-DLT-FLAG-CO1611-Z = 'Y'
               MOVE WK01-DLT-FLAG-CO1611 TO CNFRM-DLT-FLAG-CO16 OF
                FCOWM16(0011)
           END-IF.
           MOVE WK01-DLT-FLAG-CO1611-F TO WK01-DLT-FLAG-CO16-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-DLT-FLAG-CO1612-Z = 'Y'
               MOVE WK01-DLT-FLAG-CO1612 TO CNFRM-DLT-FLAG-CO16 OF
                FCOWM16(0012)
           END-IF.
           MOVE WK01-DLT-FLAG-CO1612-F TO WK01-DLT-FLAG-CO16-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6605-CO161-Z = 'Y'
               MOVE WK01-DATE-6605-CO161 TO COMM-DATE-6605-CO16 OF
                FCOWM16(0001)
           END-IF.
           MOVE WK01-DATE-6605-CO161-F TO WK01-DATE-6605-CO16-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6605-CO162-Z = 'Y'
               MOVE WK01-DATE-6605-CO162 TO COMM-DATE-6605-CO16 OF
                FCOWM16(0002)
           END-IF.
           MOVE WK01-DATE-6605-CO162-F TO WK01-DATE-6605-CO16-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6605-CO163-Z = 'Y'
               MOVE WK01-DATE-6605-CO163 TO COMM-DATE-6605-CO16 OF
                FCOWM16(0003)
           END-IF.
           MOVE WK01-DATE-6605-CO163-F TO WK01-DATE-6605-CO16-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6605-CO164-Z = 'Y'
               MOVE WK01-DATE-6605-CO164 TO COMM-DATE-6605-CO16 OF
                FCOWM16(0004)
           END-IF.
           MOVE WK01-DATE-6605-CO164-F TO WK01-DATE-6605-CO16-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6605-CO165-Z = 'Y'
               MOVE WK01-DATE-6605-CO165 TO COMM-DATE-6605-CO16 OF
                FCOWM16(0005)
           END-IF.
           MOVE WK01-DATE-6605-CO165-F TO WK01-DATE-6605-CO16-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6605-CO166-Z = 'Y'
               MOVE WK01-DATE-6605-CO166 TO COMM-DATE-6605-CO16 OF
                FCOWM16(0006)
           END-IF.
           MOVE WK01-DATE-6605-CO166-F TO WK01-DATE-6605-CO16-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6605-CO167-Z = 'Y'
               MOVE WK01-DATE-6605-CO167 TO COMM-DATE-6605-CO16 OF
                FCOWM16(0007)
           END-IF.
           MOVE WK01-DATE-6605-CO167-F TO WK01-DATE-6605-CO16-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6605-CO168-Z = 'Y'
               MOVE WK01-DATE-6605-CO168 TO COMM-DATE-6605-CO16 OF
                FCOWM16(0008)
           END-IF.
           MOVE WK01-DATE-6605-CO168-F TO WK01-DATE-6605-CO16-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6605-CO169-Z = 'Y'
               MOVE WK01-DATE-6605-CO169 TO COMM-DATE-6605-CO16 OF
                FCOWM16(0009)
           END-IF.
           MOVE WK01-DATE-6605-CO169-F TO WK01-DATE-6605-CO16-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6605-CO1610-Z = 'Y'
               MOVE WK01-DATE-6605-CO1610 TO COMM-DATE-6605-CO16 OF
                FCOWM16(0010)
           END-IF.
           MOVE WK01-DATE-6605-CO1610-F TO WK01-DATE-6605-CO16-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6605-CO1611-Z = 'Y'
               MOVE WK01-DATE-6605-CO1611 TO COMM-DATE-6605-CO16 OF
                FCOWM16(0011)
           END-IF.
           MOVE WK01-DATE-6605-CO1611-F TO WK01-DATE-6605-CO16-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-6605-CO1612-Z = 'Y'
               MOVE WK01-DATE-6605-CO1612 TO COMM-DATE-6605-CO16 OF
                FCOWM16(0012)
           END-IF.
           MOVE WK01-DATE-6605-CO1612-F TO WK01-DATE-6605-CO16-F (12).
