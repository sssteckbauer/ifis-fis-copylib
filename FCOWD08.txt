      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCOWD08                              04/24/00  13:05  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCOWD08.
           02  SAVE-DBKEY-ID-6261-CO08
                                     COMP   OCCURS 10   PIC S9(8).
           02  COPY-DBKEY-ID-6261-CO08        COMP      PIC S9(8).
           02  COPY-DBKEY-ID-6260-CO08        COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-6260-CO08        COMP      PIC S9(8).
           02  CHECK-DBKEY-ID-6261-CO08       COMP      PIC S9(8).
           02  TEXT-CHANGED-FLAG-CO08                   PIC X(1).
           02  PARA-FLAG-CO08                           PIC X(1).
           02  POS-ERROR-CO08                           PIC X(1).
           02  LINE-PAGE-ERROR-CO08                     PIC X(1).
           02  LINE-LGTH-ERROR-CO08                     PIC X(1).
           02  MARGIN-ERROR-CO08                        PIC X(1).
           02  COMP-LINES-PER-PAGE-6260-CO08  COMP-3    PIC S9(3).
           02  COMP-TOP-MARGIN-6260-CO08      COMP-3    PIC S9(2).
           02  PARA-COL-NUM-CO08              COMP-3    PIC 9(3).
           02  DATA-COL-NUM-CO08              COMP-3    PIC 9(3).
           02  CURR-COL-NUM-CO08              COMP-3    PIC 9(3).
           02  TEST-COMM-TEXT-LGTH-CO08       COMP-3    PIC 9(3).
           02  HOLD-COMM-TEXT-CO08.
               03  HOLD-COMM-CHAR-CO08      OCCURS 79   PIC X(1).
           02  TEST-COMM-TEXT-CO08.
               03  TEST-COMM-CHAR-CO08      OCCURS 79   PIC X(1).
           02  ALPHA-NUM-CO08.
               03  HOLD-NUM-CO08            OCCURS 3    PIC X(1).
           02  NUMERIC-NUM-CO08 REDEFINES
               ALPHA-NUM-CO08                           PIC 9(3).
           02  HOLD-DESC-TYPE-CO08                      PIC X(1).
           02  WORK-MSG-TEXT-CO08.
               03  WORK-LINE-LIT-CO08                   PIC X(5).
               03  WORK-LINE-NUM-CO08                   PIC 9(2).
               03  WORK-WORD-LIT-CO08                   PIC X(6).
               03  WORK-WORD-NUM-CO08                   PIC 9(3).
               03  WORK-INV-LIT-CO08                    PIC X(24).
