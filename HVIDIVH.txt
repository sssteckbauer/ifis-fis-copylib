       975-TO-R4150-INV-HDR SECTION.
           MOVE IVH-USER-CD TO
               USER-CODE-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-UNVRS-CD TO
               UNVRS-CODE-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-DOC-NBR TO
               DCMNT-NMBR-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-PYMT-DUE-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               PYMT-DUE-DATE-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-BANK-ACCT-CD TO
               BANK-ACCT-CODE-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-SALES-USE-TAX-IND TO
               SALES-USE-TAX-IND-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-A1099-IND TO
               1099-IND-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-CRDT-MEMO TO
               CRDT-MEMO-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-CHECK-GRPNG-IND TO
               CHECK-GRPNG-IND-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-CMPLT-IND TO
               CMPLT-IND-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-RSBLE-IND TO
               RSBLE-IND-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-ADDL-CHRG TO
               ADDL-CHRG-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-ADJMT-CD TO
               ADJMT-CODE-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-ADJMT-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ADJMT-DATE-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-OPN-PD-HLD-IND TO
               OPEN-PAID-HOLD-IND-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-INVD-AMT TO
               INVD-AMT-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-APRVL-IND TO
               APRVL-IND-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-HDR-ERROR-IND TO
               HDR-ERROR-IND-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-HDR-CNTR-DTL TO
               HDR-CNTR-DTL-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-HDR-CNTR-ACCT TO
               HDR-CNTR-ACCT-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-ADR-TYP-CD TO
               ADR-TYPE-CODE-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-PO-NBR TO
               PO-NMBR-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-DSCNT-CD TO
               DSCNT-CODE-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-VNDR-INV-NBR TO
               VNDR-INV-NMBR-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-TAX-RATE-CD TO
               TAX-RATE-CODE-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-VNDR-ID-DGT-ONE TO
               VNDR-ID-DIGIT-ONE-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-VNDR-ID-LST-NINE TO
               VNDR-ID-LAST-NINE-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-INCM-TYP-SEQ-NBR TO
               INCM-TYPE-SEQ-NMBR-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-A1099-RPRT-ID TO
               1099-RPRT-ID-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-TRANS-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               TRANS-DATE-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-CNCL-IND TO
               CNCL-IND-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-CNCL-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               CNCL-DATE-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-CHECKS-WRITTEN-QTY TO
               CHECKS-WRITTEN-QTY-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-VNDR-INV-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               VNDR-INV-DATE-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-DOC-REF-NBR TO
               DCMNT-REF-NMBR-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-MAILCODE TO
               MAILCODE-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-SUB-DOC-TYP TO
               SUB-DCMNT-TYPE-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-ORGN-CD TO
               ORGN-CODE-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-IDC-APPL-AMT TO
               IDC-APPL-AMT-4150 OF R4150-INV-HDR
                                                                     .
           MOVE IVH-IDC-ACTL-AMT TO
               IDC-ACTL-AMT-4150 OF R4150-INV-HDR
                                                                     .
FP2006     MOVE IVH-FORCE-CHECK    TO
FP2006         FORCE-CHECK-4150        OF R4150-INV-HDR
FP7157     MOVE IVH-TAX-TREATY-IND TO
FP7157         TAX-TREATY-IND-4150     OF R4150-INV-HDR
FP7157     MOVE IVH-IRS-INCOME-CD  TO
FP7157         IRS-INCOME-CD-4150      OF R4150-INV-HDR
S41335     IF IVH-FED-WH-AMT NUMERIC
S41335        MOVE IVH-FED-WH-AMT  TO
S41335            FED-WH-AMT-4150      OF R4150-INV-HDR
S41335     ELSE
S41335        MOVE +0              TO
S41335            FED-WH-AMT-4150      OF R4150-INV-HDR
S41335     END-IF
S41335     IF IVH-FED-RPT-AMT NUMERIC
S41335        MOVE IVH-FED-RPT-AMT TO
S41335            FED-RPT-AMT-4150     OF R4150-INV-HDR
S41335     ELSE
S41335        MOVE +0              TO
S41335            FED-RPT-AMT-4150     OF R4150-INV-HDR
S41335     END-IF
           .
       975-TO-R4150-INV-HDR-EXIT.
           EXIT.
