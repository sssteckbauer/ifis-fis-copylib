      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWM26                              04/24/00  12:37  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWM26.
           02  VNDR-NAME-6311-6117-PU26                 PIC X(35).
           02  PO-NMBR-4083-PU26            OCCURS 13   PIC X(8).
           02  CMDTY-CODE-4074-PU26         OCCURS 13   PIC X(8).
           02  CMDTY-DESC-4074-4085-PU26    OCCURS 13   PIC X(35).
           02  CMDTY-QTY-4085-PU26          OCCURS 13   PIC X(11).
           02  UNIT-MEA-CODE-4085-PU26      OCCURS 13   PIC X(3).
           02  ORDER-DATE-4096-PU26         OCCURS 13   PIC X(8).
