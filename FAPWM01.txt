      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWM01                              04/24/00  12:15  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWM01.
           02  ACTN-CODE-AP01                           PIC X(1).
           02  DSCNT-CODE-4161-AP01                     PIC X(2).
           02  TRANS-DATE-4150-AP01                     PIC X(6).
           02  PYMT-DUE-DATE-4150-AP01                  PIC X(6).
           02  DCMNT-REF-NMBR-AP01                      PIC X(10).
           02  VNDR-INV-NMBR-4150-AP01                  PIC X(9).
           02  VNDR-NAME-GP-AP01                        PIC X(35).
           02  ADR-TYPE-CODE-4150-AP01                  PIC X(2).
           02  ACH-ADR-TYPE-AP01                        PIC X(2).
           02  LINE-ADR-AP01                OCCURS 4    PIC X(35).
           02  CITY-NAME-AP01                           PIC X(18).
           02  STATE-CODE-AP01                          PIC X(2).
           02  ZIP-CODE-AP01                            PIC X(10).
           02  ONE-TIME-IND-4073-AP01                   PIC X(1).
           02  CHECK-GRPNG-IND-4150-AP01                PIC X(1).
           02  1099-IND-4150-AP01                       PIC X(1).
           02  1099-RPRT-ID-4150-AP01                   PIC X(9).
           02  BANK-ACCT-CODE-4150-AP01                 PIC X(2).
           02  BANK-ACCT-DESC-4062-AP01                 PIC X(35).
           02  INCM-TYPE-CODE-4150-AP01                 PIC X(2).
           02  SALES-USE-TAX-IND-4150-AP01              PIC X(1).
           02  CRDT-MEMO-IND-4150-AP01                  PIC X(1).
           02  RSBLE-IND-4150-AP01                      PIC X(1).
           02  TEXT-IND-AP01                            PIC X(1).
           02  OPEN-PAID-IND-4150-AP01                  PIC X(1).
           02  INVD-AMT-4150-AP01                       PIC X(13).
           02  ADJMT-CODE-4150-AP01                     PIC X(2).
           02  ADJMT-DATE-4150-AP01                     PIC X(6).
FP5665     02  PRD-DSCNT-AMT-AP01                       PIC X(13).
           02  DSCNT-AMT-AP01                           PIC X(13).
           02  TAX-AMT-AP01                             PIC X(13).
           02  TAX-RATE-CODE-4150-AP01                  PIC X(3).
           02  ADDL-CHRG-4150-AP01                      PIC X(13).
           02  NET-AMT-AP01                             PIC X(13).
           02  HDR-ERROR-IND-4150-AP01                  PIC X(1).
           02  HDR-CNTR-CMDTY-4150-AP01                 PIC X(4).
           02  HDR-CNTR-ACCT-4150-AP01                  PIC X(4).
           02  CMPLT-IND-4150-AP01                      PIC X(1).
           02  APRVL-IND-4150-AP01                      PIC X(1).
           02  CONFIRM-DELETE-AP01                      PIC X(1).
           02  DELETE-LIT-AP01                          PIC X(8).
           02  CNCL-IND-4150-AP01                       PIC X(1).
           02  PYMT-CYCLE-4163-AP01                     PIC X(1).
           02  ORIG-PO-AMT-AP01                         PIC X(13).
           02  VNDR-INV-DATE-4150-AP01                  PIC X(6).
           02  UNPAID-PO-BAL-AP01                       PIC X(13).
           02  APRVD-AMT-AP01                           PIC X(13).
           02  ENTY-PRSN-IND-AP01                       PIC X(1).
           02  PO-CLASS-CODE-AP01                       PIC X(1).
           02  BLNKT-TERM-DATE-AP01                     PIC X(6).
           02  ITEM-NMBR-AP01                 COMP-3    PIC 9(4).
           02  SEQ-NMBR-AP01                            PIC 9(4).
           02  MAILCODE-AP01                            PIC X(6).
           02  TOTAL-C-O-AP01                           PIC X(3).
           02  TAX-RATE-DESC-AP01                       PIC X(35).
           02  CNTRY-CODE-AP01                          PIC X(2).
           02  APRVL-TMPLT-CODE-AP01                    PIC X(3).
           02  CHECK-TEXT-IND-AP01                      PIC X(1).
           02  DOCT-TEXT-IND-AP01                       PIC X(1).
           02  CHECK-NMBR-AP01                          PIC X(8).
           02  CHECK-DATE-AP01                          PIC X(8).
           02  CHECK-STATUS-AP01                        PIC X(10).
           02  FISCAL-YR-AP01                           PIC X(2).
           02  RCNCLTN-IND-AP01                         PIC X(20).
           02  RCNCLTN-DATE-AP01              COMP-3    PIC S9(8).
           02  TRADE-IN-AMT-AP01                        PIC X(13).
           02  TRADE-IN-IND-AP01                        PIC X(01).
           02  FED-FUND-MSG-AP01                        PIC X(08).
           02  592-IND-AP01                             PIC X(01).
           02  CA-WH-AMT-AP01                           PIC X(13).
           02  CA-RPT-AMT-AP01                          PIC X(13).
