      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPEWK32                              04/24/00  12:36  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPEWK32.
           02  WORK-PRSN-ID-PE32.
               03  WORK-PRSN-ID-ONE-PE32                PIC X(9).
           02  MAP-END-DATE-PE32                        PIC X(6).
           02  WORK-DATE-PE32 REDEFINES
               MAP-END-DATE-PE32.
               03  WORK-MONTH-DATE-PE32                 PIC 9(2).
               03  WORK-DAY-DATE-PE32                   PIC 9(2).
               03  WORK-YEAR-DATE-PE32                  PIC 9(2).
           02  ADR-TYPE-CODE-6137-PE32                  PIC X(2).
