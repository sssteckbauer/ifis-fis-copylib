       01  FUTWM21.
           02  ACTN-CODE-GROUP-UT21.
               03  ACTN-CODE-UT21           OCCURS 12   PIC X(1).
           02  USER-ID-WM-UT21              OCCURS 12   PIC X(9).
           02  NAME-KEY-WM-UT21             OCCURS 12   PIC X(35).
           02  SAVE-DBKEY-WM-UT21    COMP   OCCURS 12   PIC S9(8).
