      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM90                              04/24/00  12:21  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM90.
           02  ACTN-CODE-GROUP-CH90.
               03  ACTN-CODE-CH90           OCCURS 13   PIC X(1).
           02  BANK-ID-LAST-NINE-4046-CH90  OCCURS 13   PIC X(9).
           02  NAME-KEY-6311-CH90           OCCURS 13   PIC X(35).
           02  STATUS-4046-CH90             OCCURS 13   PIC X(8).
