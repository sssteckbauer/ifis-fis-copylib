       01  R4158-CHK-HDR.
           02  DB-PROC-ID-4158.
               03  USER-CODE-4158                       PIC X(8).
               03  LAST-ACTVY-DATE-4158                 PIC X(5).
               03  TRMNL-ID-4158                        PIC X(8).
               03  PURGE-FLAG-4158                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  CHECK-HDR-KEY-4158.
               03  UNVRS-CODE-4158                      PIC X(2).
               03  BANK-ACCT-CODE-4158                  PIC X(2).
               03  CHECK-NMBR-4158                      PIC X(8).
           02  CHECK-DATE-4158                COMP-3    PIC S9(8).
           02  CHECK-TYPE-CODE-4158                     PIC X(1).
           02  CHECK-AMT-4158                 COMP-3    PIC 9(11)V99.
           02  CHECK-RGSTR-DATE-4158          COMP-3    PIC S9(8).
           02  CNCL-IND-4158                            PIC X(1).
           02  PRCSD-IND-4158                           PIC X(1).
           02  RCNCLTN-IND-4158                         PIC X(1).
           02  CHECK-RCNCLTN-DATE-4158        COMP-3    PIC S9(8).
           02  DISCREPANCY-IND-4158                     PIC X(1).
           02  CNCL-DATE-4158                 COMP-3    PIC S9(8).
           02  STOP-PAID-IND-4158                       PIC X(1).
           02  ISSUE-BANK-DATE-4158           COMP-3    PIC S9(8).
           02  ISSUE-RGSTR-DATE-4158          COMP-3    PIC S9(8).
           02  CHK-CNCL-CODE-4158                       PIC X(1).
           02  FILLER02                                 PIC X(9).
