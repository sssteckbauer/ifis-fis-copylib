      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4190-ENCMBR-DTL' TO DBLINK-CURR-PARAGRAPH.   276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4190-ENCMBR-DTL' TO RECORD-NAME.                      276 BRTN
YYY991     MOVE 'F-ENCMBRNC' TO AREA-NAME.                              276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4190-ENCMBR-DTL TO DBLINK-RECORD-MADE-CURRENT.  276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4190-ENCMBR-DTL-EXIT                     276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE ECD-FK-ECH-ENCMBR-NBR TO DBLINK-R4190-ENCMBR-DTL-01 276 BRTN
YYY991         MOVE ECD-FK-ECH-DOCTYP-SEQ TO DBLINK-R4190-ENCMBR-DTL-02 276 BRTN
YYY991         MOVE ECD-ITEM-NBR TO DBLINK-R4190-ENCMBR-DTL-03          276 BRTN
YYY991         MOVE ECD-SEQ-NBR TO DBLINK-R4190-ENCMBR-DTL-04           276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4190-ENCMBR-DTL TO DBLINK-VIEW-NAME-CURRENT.    276 BRTN
YYY991     MOVE DBLINK-R4190-ENCMBR-DTL-KEY TO DBLINK-VIEW-200-CURRENT. 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4190-4183                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4190-ENCMBR-DTL-01 TO DBLINK-S-4190-4183-01.    276 BRTN
YYY991     MOVE DBLINK-R4190-ENCMBR-DTL-02 TO DBLINK-S-4190-4183-02.    276 BRTN
YYY991     MOVE DBLINK-R4190-ENCMBR-DTL-03 TO DBLINK-S-4190-4183-03.    276 BRTN
YYY991     MOVE DBLINK-R4190-ENCMBR-DTL-04 TO DBLINK-S-4190-4183-04.    276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4190-4183-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4191-4190                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ECD-FK-ECH-ENCMBR-NBR TO DBLINK-S-4191-4190-01.         276 BRTN
YYY991     MOVE ECD-FK-ECH-DOCTYP-SEQ TO DBLINK-S-4191-4190-02.         276 BRTN
YYY991     MOVE ECD-ITEM-NBR TO DBLINK-S-4191-4190-03.                  276 BRTN
YYY991     MOVE ECD-SEQ-NBR TO DBLINK-S-4191-4190-04.                   276 BRTN
