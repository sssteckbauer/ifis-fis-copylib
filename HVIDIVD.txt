       975-TO-R4151-INV-DTL SECTION.
           MOVE IVD-USER-CD TO
               USER-CODE-4151 OF R4151-INV-DTL
                                                                     .
           MOVE IVD-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4151 OF R4151-INV-DTL
                                                                     .
           MOVE IVD-ITEM-NBR TO
               ITEM-NMBR-4151 OF R4151-INV-DTL
                                                                     .
           MOVE IVD-CMDTY-CD TO
               CMDTY-CODE-4151 OF R4151-INV-DTL
                                                                     .
           MOVE IVD-OPN-CLS-HLD-IND TO
               OPEN-CLOSE-HOLD-IND-4151 OF R4151-INV-DTL
                                                                     .
           MOVE IVD-ACCPT-QTY TO
               ACCPT-QTY-4151 OF R4151-INV-DTL
                                                                     .
           MOVE IVD-ACCPT-UNIT-PRC TO
               ACCPT-UNIT-PRICE-4151 OF R4151-INV-DTL
                                                                     .
           MOVE IVD-PO-NBR TO
               PO-NMBR-4151 OF R4151-INV-DTL
                                                                     .
           MOVE IVD-CMDTY-DESC TO
               CMDTY-DESC-4151 OF R4151-INV-DTL
                                                                     .
           MOVE IVD-UNIT-MEA-CD TO
               UNIT-MEA-CODE-4151 OF R4151-INV-DTL
                                                                     .
           MOVE IVD-DTL-CNTR-ACCT TO
               DTL-CNTR-ACCT-4151 OF R4151-INV-DTL
                                                                     .
           MOVE IVD-DTL-ERROR-IND TO
               DTL-ERROR-IND-4151 OF R4151-INV-DTL
                                                                     .
           MOVE IVD-DSCNT-AMT TO
               DSCNT-AMT-4151 OF R4151-INV-DTL
                                                                     .
           MOVE IVD-TAX-AMT TO
               TAX-AMT-4151 OF R4151-INV-DTL
                                                                     .
           MOVE IVD-APRVD-QTY TO
               APRVD-QTY-4151 OF R4151-INV-DTL
                                                                     .
           MOVE IVD-APRVD-UNT-PRC TO
               APRVD-UNIT-PRICE-4151 OF R4151-INV-DTL
                                                                     .
           MOVE IVD-INVD-QTY TO
               INVD-QTY-4151 OF R4151-INV-DTL
                                                                     .
           MOVE IVD-INVD-UNIT-PRC TO
               INVD-UNIT-PRICE-4151 OF R4151-INV-DTL
                                                                     .
           MOVE IVD-PO-ITEM-NBR TO
               PO-ITEM-NMBR-4151 OF R4151-INV-DTL
                                                                     .
           MOVE IVD-TLRNC-OVRD-IND TO
               TLRNC-OVRDE-IND-4151 OF R4151-INV-DTL
                                                                     .
           MOVE IVD-PREV-PAID-AMT TO
               PREV-PAID-AMT-4151 OF R4151-INV-DTL
                                                                     .
       975-TO-R4151-INV-DTL-EXIT.
           EXIT.
