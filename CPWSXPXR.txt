      * /************************************************************/  $
      * /*   CHANGE LOG -- CPWSXPXR                                 */  $
      * /*   NAME            DATE      COMMENTS                     */  $
      * /*..........................................................*/  $
      * /*   BROWN          05/27/85  MODIFIED FOR COLLECTIVE BARG. */  $
      * /*   M. JEWETT      01/10/92  ADDED IFIS IFOAPAL            */  $
      * /************************************************************/  $
      *01  XPXR-PAYROLL-XREF.                                           CPWSXPXR
           05  XPXR-KEY.                                                CPWSXPXR
               10  XPXR-FILE-KY.                                        CPWSXPXR
                   15  XPXR-LOCATION           PIC X(02).               CPWSXPXR
                   15  XPXR-SAU                PIC X(01).               CPWSXPXR
                   15  XPXR-SUB-CAMPUS         PIC X(01).               CPWSXPXR
                   15  XPXR-ACCOUNT            PIC X(07).               CPWSXPXR
                   15  XPXR-FUND               PIC X(06).               CPWSXPXR
                   15  XPXR-SUB-BUDGET         PIC X(02).               CPWSXPXR
                   15  XPXR-SEGMENT-SORT-TYPE  PIC X(01).               CPWSXPXR
               10  XPXR-SEGMENT-KEY.                                    CPWSXPXR
                   15  XPXR-TITLE              PIC X(04).               CPWSXPXR
                   15  XPXR-EMPLOYEE-NO        PIC X(09).               CPWSXPXR
                   15  XPXR-DISTR-NO           PIC X(02).               CPWSXPXR
           05  XPXR-XREF-DATA.                                          CPWSXPXR
               10  XPXR-XREF-TITLE             PIC X(04).               CPWSXPXR
               10  XPXR-XREF-LOCATION          PIC X(02).               CPWSXPXR
               10  XPXR-XREF-SAU               PIC X(01).               CPWSXPXR
               10  XPXR-XREF-SUB-CAMPUS        PIC X(01).               CPWSXPXR
               10  XPXR-XREF-ACCOUNT           PIC X(07).               CPWSXPXR
               10  XPXR-XREF-FUND              PIC X(06).               CPWSXPXR
               10  XPXR-XREF-SUB-BUDGET        PIC X(02).               CPWSXPXR
               10  XPXR-XREF-DISTR-NO          PIC X(02).               CPWSXPXR
               10  XPXR-XREF-RANK              PIC X(01).               CPWSXPXR
               10  XPXR-XREF-RANK-YEARS        PIC X(02).               CPWSXPXR
               10  XPXR-XREF-STEP              PIC X(01).               CPWSXPXR
               10  XPXR-XREF-STEP-YEARS        PIC X(02).               CPWSXPXR
               10  XPXR-XREF-DESCR-OF-SERV     PIC X(03).               CPWSXPXR
               10  XPXR-XREF-RATE-CODE         PIC X(01).               CPWSXPXR
               10  XPXR-XREF-MONTHLY-RATE      PIC S9(5)V99.            CPWSXPXR
               10  XPXR-XREF-ANNUAL-RATE       PIC S9(6).               CPWSXPXR
               10  XPXR-XREF-FTE               PIC S9(4)V99.            CPWSXPXR
               10  XPXR-XREF-PAY-END-DATE.                              CPWSXPXR
                   15  XPXR-XREF-PAY-END-YEAR  PIC X(02).               CPWSXPXR
                   15  XPXR-XREF-PAY-END-MONTH PIC X(02).               CPWSXPXR
                   15  XPXR-XREF-PAY-END-DAY   PIC X(02).               CPWSXPXR
               10  XPXR-SPREAD-FLAG            PIC X(01).               CPWSXPXR
                   88  XPXR-NO-SPREAD          VALUE '0'.               CPWSXPXR
                   88  XPXR-YES-SPREAD         VALUE '1'.               CPWSXPXR
               10  XPXR-XREF-EMPL-RELATIONS-CODE PIC X(01).             $
               10  XPXR-XREF-EMPLOYEE-UNIT-CODE PIC X(02).              $
               10  XPXR-XREF-EMPL-SPECL-HNDLG-CD PIC X(01).             $
               10  XPXR-XREF-EMPL-DISTR-UNIT-CODE PIC X(01).            $
               10  XPXR-XREF-EMPL-REPRESENT-CODE PIC X(01).             $
               10  XPXR-XREF-TITLE-UNIT-CODE   PIC X(02).               $
               10  XPXR-XREF-TITLE-SPECL-HNDLG-CD PIC X(01).            $
               10  XPXR-XREF-APPT-REPRESENT-CODE PIC X(01).             $
               10  XPXR-XREF-RANGE-ADJ-DUC     PIC X(01).               $
           05  FILLER                          PIC X(07).               $PWSXPXR
           05  XPXR-PAYROLL-SEGMENT-TYPE       PIC X(01).               CPWSXPXR
           05  XPXR-XREF-IFIS-IFOAPAL.
               10  XPXR-XREF-IFIS-INDEX        PIC X(10).
               10  XPXR-XREF-IFIS-FUND         PIC X(06).
               10  XPXR-XREF-IFIS-ORG          PIC X(06).
               10  XPXR-XREF-IFIS-ACCOUNT      PIC X(06).
               10  XPXR-XREF-IFIS-PROGRAM      PIC X(06).
               10  XPXR-XREF-IFIS-ACTIVITY     PIC X(06).
               10  XPXR-XREF-IFIS-LOCATION     PIC X(06).
