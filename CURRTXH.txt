      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4120-TXT-HDR' TO DBLINK-CURR-PARAGRAPH.      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4120-TXT-HDR' TO RECORD-NAME.                         276 BRTN
YYY991     MOVE 'F-TEXT' TO AREA-NAME.                                  276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4120-TXT-HDR TO DBLINK-RECORD-MADE-CURRENT.     276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4120-TXT-HDR-EXIT                        276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE TXH-DOC-TYP-SEQ-NBR TO DBLINK-R4120-TXT-HDR-01      276 BRTN
YYY991         MOVE TXH-TEXT-ENTY-CD TO DBLINK-R4120-TXT-HDR-02         276 BRTN
YYY991         MOVE TXH-CHNG-SEQ-NBR TO DBLINK-R4120-TXT-HDR-03         276 BRTN
YYY991         MOVE TXH-ITEM-NBR TO DBLINK-R4120-TXT-HDR-04             276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4120-TXT-HDR TO DBLINK-VIEW-NAME-CURRENT.       276 BRTN
YYY991     MOVE DBLINK-R4120-TXT-HDR-KEY TO DBLINK-VIEW-200-CURRENT.    276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-4120-4121                                                 276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4120-TXT-HDR-01 TO DBLINK-S-4120-4121-01.       276 BRTN
YYY991     MOVE DBLINK-R4120-TXT-HDR-02 TO DBLINK-S-4120-4121-02.       276 BRTN
YYY991     MOVE DBLINK-R4120-TXT-HDR-03 TO DBLINK-S-4120-4121-03.       276 BRTN
YYY991     MOVE DBLINK-R4120-TXT-HDR-04 TO DBLINK-S-4120-4121-04.       276 BRTN
YYY991     MOVE LOW-VALUES TO DBLINK-S-4120-4121-KEY-M.                 276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
