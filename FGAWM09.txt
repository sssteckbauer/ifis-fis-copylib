      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FGAWM09                              04/24/00  12:32  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FGAWM09.
           02  DSPLY-OPTN-GA09              OCCURS 4    PIC X(1).
           02  ORGZN-TITLE-4010-GA09                    PIC X(35).
           02  FUND-TITLE-4005-GA09                     PIC X(35).
           02  PRGRM-TITLE-4105-GA09                    PIC X(35).
           02  ACCT-CODE-TITLE-4034-GA09                PIC X(35).
           02  ACCT-INDX-TITLE-4067-GA09                PIC X(35).
           02  ACTVY-TITLE-4066-GA09                    PIC X(35).
           02  LCTN-TITLE-4007-GA09                     PIC X(35).
