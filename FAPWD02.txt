      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWD02                              04/24/00  12:53  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWD02.
           02  SYSTEM-DATE-AP02               COMP-3    PIC S9(8).
           02  START-DATE-AP02                COMP-3    PIC S9(8).
           02  PAGE-STATUS-CODE-AP02                    PIC X(1).
           02  TOTAL-AMT-ORDERED-9-AP02       COMP-3    PIC S9(10)V99.
           02  SAVE-DBKEY-ID-MINUS-ONE-AP02   COMP      PIC S9(8).
           02  TOTAL-NET-AMT-9-AP02           COMP-3    PIC S9(10)V99.
           02  SAVE-DBKEY-ID-PLUS-ONE-AP02    COMP      PIC S9(8).
           02  PO-DSCNT-AMT-9-AP02            COMP-3    PIC S9(10)V99.
           02  SAVE-DBKEY-ID-4150-AP02        COMP      PIC S9(8).
           02  ITEM-DSCNT-AMT-9-AP02          COMP-3    PIC S9(10)V99.
           02  TRADE-IN-AMT-9-AP02            COMP-3    PIC S9(10)V99.
           02  SAVE-DBKEY-ID-4151-AP02        COMP      PIC S9(8).
           02  SLCTN-FLAG-AP02                          PIC X(1).
           02  ITEM-NMBR-AP02                 COMP-3    PIC 9(4).
           02  TOTAL-ADDL-AMT-AP02            COMP-3    PIC S9(10)V99.
           02  ORDRD-PRICE-WORK-AP02          COMP-3    PIC S9(16)V99.
           02  ACCPT-PRICE-WORK-AP02          COMP-3    PIC S9(10)V99.
           02  INVD-PRICE-WORK-AP02           COMP-3    PIC S9(16)V99.
           02  APRVD-PRICE-WORK-AP02          COMP-3    PIC S9(16)V99.
           02  NET-PRICE-WORK-AP02            COMP-3    PIC S9(16)V99.
           02  PREV-EXTENDED-PRICE-AP02       COMP-3    PIC S9(16)V99.
           02  PO-ITEM-AMT-TOTAL-AP02         COMP-3    PIC 9(10)V99.
           02  PO-ITEM-AMT-LEFT-AP02          COMP-3    PIC S9(10)V99.
           02  PO-INV-AMT-TOTAL-AP02          COMP-3    PIC S9(10)V99.
           02  TAX-AMT-AP02                   COMP-3    PIC S9(10)V99.
FP5665     02  PRD-DSCNT-AMT-AP02             COMP-3    PIC S9(10)V99.
           02  DSCNT-AMT-AP02                 COMP-3    PIC S9(10)V99.
           02  SET-SORT-KEY-6311-AP02.
               03  UNVRS-CODE-6311-AP02                 PIC X(2).
               03  INTRL-REF-ID-6311-AP02     COMP-3    PIC S9(7).
           02  SUSP-FLAG-AP02                           PIC X(1).
           02  ACTN-ERR-FLAG-AP02                       PIC X(1).
           02  VALID-CMDTY-FLAG-AP02                    PIC X(1).
           02  CMDTY-ERR-FLAG-AP02                      PIC X(1).
           02  ADD-CMDTY-FLAG-AP02                      PIC X(1).
           02  VALID-MEA-CODE-AP02                      PIC X(1).
           02  ERR-FLAG-AP02                            PIC X(1).
           02  ACPRC-AP02                     COMP-3    PIC 9(10)V9999.
           02  APRVD-UNIT-PRICE-9-AP02        COMP-3    PIC 9(10)V9999.
           02  INQTY-AP02                     COMP-3    PIC S9(6)V99.
           02  INPRC-AP02                     COMP-3    PIC 9(10)V9999.
           02  APQTY-AP02                     COMP-3    PIC S9(6)V99.
           02  APPRC-AP02                     COMP-3    PIC 9(10)V9999.
           02  HOLD-VNDR-NAME-AP02                      PIC X(35).
           02  TO-BE-PAID-AMT-WORK-AP02       COMP-3    PIC S9(10)V99.
           02  CMDTY-DESC-AP02                          PIC X(35).
           02  SEQ-NMBR-4152-AP02                       PIC 9(4).
           02  ORQTY-AP02                     COMP-3    PIC S9(6)V99.
           02  ORPRC-AP02                     COMP-3    PIC 9(10)V9999.
           02  TLRNC-QTY-AP02                 COMP-3    PIC S9(6)V99.
           02  TLRNC-PRC-AP02                 COMP-3    PIC 9(10)V9999.
           02  SET-SORT-KEY-4012-AP02.
               03  OPTN-1-CODE-AP02                     PIC X(8).
               03  OPTN-2-CODE-AP02                     PIC X(8).
               03  LEVEL-NMBR-AP02                      PIC 9(2).
           02  EFCTV-KEY-AP02.
               03  EFCTV-DATE-AP02            COMP-3    PIC S9(8).
               03  TIME-STAMP-AP02                      PIC X(6).
           02  WORK-QTY-AP02                  COMP-3    PIC S9(6)V99.
           02  CLOSE-PO-IND-AP02                        PIC X(1).
           02  LIQDTN-IND-AP02                          PIC X(1).
           02  TAX-RATE-PCT-AP02              COMP-3    PIC 9(3)V999.
           02  APRVL-TMPLT-CODE-AP02                    PIC X(3).
           02  APRVL-IND-AP02                           PIC X(1).
           02  WORK-FIELD-AP02                COMP-3    PIC 9(10)V9999.
