       01  R4096-PO-HDR-SEQ.
           02  DB-PROC-ID-4096.
               03  USER-CODE-4096                       PIC X(8).
               03  LAST-ACTVY-DATE-4096                 PIC X(5).
               03  TRMNL-ID-4096                        PIC X(8).
               03  PURGE-FLAG-4096                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  CHNG-SEQ-NMBR-4096                       PIC X(3).
           02  PO-CMPLT-IND-4096                        PIC X(1).
           02  PRINT-DATE-4096                COMP-3    PIC S9(8).
           02  PO-PRINT-FLAG-4096                       PIC X(1).
           02  DLVRY-BY-DATE-4096             COMP-3    PIC S9(8).
           02  ACKNWDG-DATE-4096              COMP-3    PIC S9(8).
           02  ORDER-DATE-4096                COMP-3    PIC S9(8).
           02  APRVL-IND-4096                           PIC X(1).
           02  HDR-CNTR-DTL-4096                        PIC 9(4).
           02  HDR-CNTR-ACCT-4096                       PIC 9(4).
           02  HDR-ERROR-IND-4096                       PIC X(1).
           02  TOTAL-AMT-4096                 COMP-3    PIC S9(10)V99.
           02  ACTVY-DATE-4096                COMP-3    PIC S9(8).
           02  CNCL-IND-4096                            PIC X(1).
           02  CNCL-DATE-4096                 COMP-3    PIC S9(8).
           02  ADDL-AMT-4096                  COMP-3    PIC S9(10)V99.
           02  DSCNT-AMT-4096                 COMP-3    PIC S9(10)V99.
           02  ITEM-COUNT-4096                COMP-3    PIC 9(4).
           02  BUYER-CODE-4096                          PIC X(4).
           02  VNDR-CNTCT-NAME-4096                     PIC X(35).
           02  MAILCODE-4096                            PIC X(6).
           02  PO-DSCNT-AMT-4096              COMP-3    PIC S9(10)V99.
           02  DSCNT-BEFORE-TAX-IND-4096                PIC X(1).
           02  PO-DSCNT-PCT-4096              COMP-3    PIC 9(3)V999.
           02  TAX-CODE-4096                            PIC X(3).
           02  DSCNT-CODE-4096                          PIC X(2).
           02  PYMT-CODE-4096                           PIC X(2).
           02  SHIP-TO-CODE-4096                        PIC X(6).
           02  TRNST-RISK-CODE-4096                     PIC X(2).
           02  ACKNWDG-IND-4096                         PIC X(1).
           02  ADR-TYPE-CODE-4096                       PIC X(2).
           02  END-DATE-4096                  COMP-3    PIC S9(8).
           02  ACCT-INDX-CODE-4096                      PIC X(10).
