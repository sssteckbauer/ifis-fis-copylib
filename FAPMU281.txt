    ***Created by Convert/DC version V8R03 on 12/05/00 at 08:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE DCSD-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY01.
      *%--------------------------------------------------------------%*
           MOVE CNFDL-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0A.
      *%--------------------------------------------------------------%*
           MOVE HOLDS-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0B.
      *%--------------------------------------------------------------%*
           MOVE ORGNT-ORGN-CODE-AP28 OF FAPWK28 TO WK01-ORGN-CODE-AP28.
      *%--------------------------------------------------------------%*
           MOVE ORGNT-BANK-ACCT-CODE-AP28 OF FAPWK28 TO
                WK01-BANK-ACCT-CODE-AP28.
      *%--------------------------------------------------------------%*
           MOVE BANK-CODE-DESC-4062-AP28 OF FAPWM28 TO
                WK01-CODE-DESC-4062-AP28.
      *%--------------------------------------------------------------%*
           MOVE CHECK-NMBR-4158-AP28 OF FAPWM28 TO WK01-NMBR-4158-AP28.
      *%--------------------------------------------------------------%*
           MOVE CHECK-DATE-4158-AP28 OF FAPWM28 TO WK01-DATE-4158-AP28.
      *%--------------------------------------------------------------%*
           MOVE CHECK-TYPE-CODE-4158-AP28 OF FAPWM28 TO
                WK01-TYPE-CODE-4158-AP28.
      *%--------------------------------------------------------------%*
           MOVE GROSS-AMT-4159-AP28 OF FAPWM28 TO WK01-AMT-4159-AP28.
      *%--------------------------------------------------------------%*
           MOVE DSCNT-AMT-4159-AP28 OF FAPWM28 TO WK01-AMT-4159-AP2A.
      *%--------------------------------------------------------------%*
           MOVE TAX-AMT-4159-AP28 OF FAPWM28 TO WK01-AMT-4159-AP2B.
      *%--------------------------------------------------------------%*
           MOVE ADDL-CHRG-4159-AP28 OF FAPWM28 TO WK01-CHRG-4159-AP28.
      *%--------------------------------------------------------------%*
           MOVE TRADE-IN-AMT-AP28 OF FAPWM28 TO WK01-TRD-IN-AMT-AP28.
      *%--------------------------------------------------------------%*
           MOVE FDRL-WTHHLD-AMT-4159-AP28 OF FAPWM28 TO
                WK01-WTHHLD-AMT-4159-AP28.
      *%--------------------------------------------------------------%*
           MOVE FDRL-WTHHLD-PCT-4159-AP28 OF FAPWM28 TO
                WK01-WTHHLD-PCT-4159-AP28.
      *%--------------------------------------------------------------%*
           MOVE PERCENTAGE-LITERAL-AP28 OF FAPWM28 TO WK01-LITERAL-AP28.
      *%--------------------------------------------------------------%*
           MOVE STATE-WTHHLD-AMT-4159-AP28 OF FAPWM28 TO
                WK01-WTHHLD-AMT-4159-AP2A.
      *%--------------------------------------------------------------%*
           MOVE STATE-WTHHLD-PCT-4159-AP28 OF FAPWM28 TO
                WK01-WTHHLD-PCT-4159-AP2A.
      *%--------------------------------------------------------------%*
           MOVE PERCENTAGE-LITERAL-AP28 OF FAPWM28 TO WK01-LITERAL-AP2A.
      *%--------------------------------------------------------------%*
           MOVE CHECK-AMT-4158-AP28 OF FAPWM28 TO WK01-AMT-4158-AP28.
      *%--------------------------------------------------------------%*
           MOVE PRNTR-DEST-CODE-4167-AP28 OF FAPWM28 TO
                WK01-DEST-CODE-4167-AP28.
      *%--------------------------------------------------------------%*
           MOVE PRINT-TEST-PATRN-AP28 OF FAPWM28 TO
                WK01-TEST-PATRN-AP28.
      *%--------------------------------------------------------------%*
           MOVE PRINT-CHECK-AP28 OF FAPWM28 TO WK01-CHECK-AP28.
      *%--------------------------------------------------------------%*
           MOVE PRINT-CONFIRM-AP28 OF FAPWM28 TO WK01-CONFIRM-AP28.
      *%--------------------------------------------------------------%*
           MOVE PRNTR-LCTN-DESC-4167-AP28 OF FAPWM28 TO
                WK01-LCTN-DESC-4167-AP28.
      *%--------------------------------------------------------------%*
           MOVE ORGN-CODE-DESC-4012-AP28 OF FAPWM28 TO
                WK01-CODE-DESC-4012-AP28.
      *%--------------------------------------------------------------%*
           MOVE VNDR-CODE-4073-AP28 OF FAPWM28 TO WK01-CODE-4073-AP28.
      *%--------------------------------------------------------------%*
           MOVE PAYEE-NAME-AP28 OF FAPWM28 TO WK01-NAME-AP28.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-AP28 OF FAPWM28 TO WK01-CODE-AP28.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE ORGNT-DCMNT-NMBR-AP28 OF FAPWK28 TO
                WK01-DCMNT-NMBR-AP28.
      *%--------------------------------------------------------------%*
           MOVE CHECK-LIT-AP28 OF FAPWM28 TO WK01-LIT-AP28.
      *%--------------------------------------------------------------%*
           MOVE CHK-START-AP28 OF FAPWM28 TO WK01-START-AP28.
      *%--------------------------------------------------------------%*
           MOVE CHK-END-AP28 OF FAPWM28 TO WK01-END-AP28.
