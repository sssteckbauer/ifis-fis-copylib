       01  R4159-CHK-DTL.
           02  DB-PROC-ID-4159.
               03  USER-CODE-4159                       PIC X(8).
               03  LAST-ACTVY-DATE-4159                 PIC X(5).
               03  TRMNL-ID-4159                        PIC X(8).
               03  PURGE-FLAG-4159                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  UNVRS-CODE-4159                          PIC X(2).
           02  DCMNT-NMBR-4159                          PIC X(8).
           02  ITEM-NMBR-4159                 COMP-3    PIC 9(4).
           02  SEQ-NMBR-4159                            PIC 9(4).
           02  COA-CODE-4159                            PIC X(1).
           02  ACCT-INDX-CODE-4159                      PIC X(10).
           02  FUND-CODE-4159                           PIC X(6).
           02  ORGZN-CODE-4159                          PIC X(6).
           02  ACCT-CODE-4159                           PIC X(6).
           02  PRGRM-CODE-4159                          PIC X(6).
           02  ACTVY-CODE-4159                          PIC X(6).
           02  LCTN-CODE-4159                           PIC X(6).
           02  APRVD-AMT-4159                 COMP-3    PIC S9(10)V99.
           02  DSCNT-AMT-4159                 COMP-3    PIC S9(10)V99.
           02  TAX-AMT-4159                   COMP-3    PIC S9(10)V99.
           02  ADDL-CHRG-4159                 COMP-3    PIC S9(10)V99.
           02  PAID-AMT-4159                  COMP-3    PIC S9(10)V99.
           02  FDRL-WTHHLD-4159               COMP-3    PIC 9(6)V99.
           02  STATE-WTHHLD-4159              COMP-3    PIC 9(6)V99.
           02  PSTNG-PRD-4159                           PIC X(2).
           02  FSCL-YR-4159                             PIC X(2).
           02  RULE-CLASS-CODE-4159                     PIC X(4).
           02  DSCNT-RULE-CLASS-4159                    PIC X(4).
           02  TAX-RULE-CLASS-4159                      PIC X(4).
           02  ADDL-CHRG-RULE-CLASS-4159                PIC X(4).
           02  PO-NMBR-4159                             PIC X(8).
           02  PO-ITEM-NMBR-4159              COMP-3    PIC 9(4).
           02  LIQDTN-IND-4159                          PIC X(1).
           02  PRJCT-CODE-4159                          PIC X(8).
           02  DCMNT-TYPE-SEQ-NMBR-4159                 PIC 9(4).
           02  ADJMT-CODE-4159                          PIC X(2).
           02  VNDR-INV-NMBR-4159                       PIC X(9).
           02  VNDR-INV-DATE-4159             COMP-3    PIC S9(8).
           02  DCMNT-REF-NMBR-4159                      PIC X(10).
           02  TAX-RATE-CODE-4159                       PIC X(3).
           02  FTB-592-IND-4159                         PIC X(1).
           02  FTB-592-STATE-WTHHLD-4159       COMP-3   PIC S9(08)V99.
           02  FTB-CHK-RPT-AMT-4159            COMP-3   PIC S9(08)V99.
