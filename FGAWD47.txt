      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FGAWD47                              04/24/00  13:07  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FGAWD47.
           02  WORK-DATE-CCYYDDD-GA47         COMP-3    PIC S9(7).
           02  WORK-DATE-YYDDD-GA47           COMP-3    PIC S9(5).
           02  WORK-DATE-MMDDCCYY-GA47        COMP-3    PIC S9(8).
           02  WORK-SYSTEM-DATE-GA47          COMP-3    PIC S9(8).
           02  WORK-SAVE-DBKEY-ID-4029-GA47
                                     COMP   OCCURS 10   PIC S9(8).
           02  WORK-SEL-FLAG-GA47                       PIC X(1).
           02  WORK-LIST-FLAG-GA47                      PIC X(1).
           02  WORK-ADD-FLAG-GA47                       PIC X(1).
           02  WORK-LOOP-FLAG-GA47                      PIC X(1).
           02  WORK-SAVE-DBKEY-GA47           COMP      PIC S9(8).
           02  ACTN-FLAG-GA47                           PIC X(1).
           02  SEL-FLAG-GA47                            PIC X(1).
