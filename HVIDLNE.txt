       975-TO-R4007-LCTN-EFCTV SECTION.
           MOVE LNE-USER-CD TO
               USER-CODE-4007 OF R4007-LCTN-EFCTV
                                                                     .
           MOVE LNE-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4007 OF R4007-LCTN-EFCTV
                                                                     .
           MOVE LNE-START-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               START-DATE-4007 OF R4007-LCTN-EFCTV
                                                                     .
           MOVE LNE-TIME-STMP TO
               DBLINKX-INPUT-PARM
           MOVE '4' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-TIME-FORMAT
           MOVE '51' TO DBLINKX-IDMS-TIME-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-PARM TO
               TIME-STAMP-4007 OF R4007-LCTN-EFCTV
                                                                     .
           MOVE LNE-END-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               END-DATE-4007 OF R4007-LCTN-EFCTV
                                                                     .
           MOVE LNE-STATUS TO
               STATUS-4007 OF R4007-LCTN-EFCTV
                                                                     .
           MOVE LNE-LCTN-TITLE TO
               LCTN-TITLE-4007 OF R4007-LCTN-EFCTV
                                                                     .
           MOVE LNE-ADR-LINE1 TO
               ADR-LINE1-4007 OF R4007-LCTN-EFCTV
                                                                     .
           MOVE LNE-ADR-LINE2 TO
               ADR-LINE2-4007 OF R4007-LCTN-EFCTV
                                                                     .
           MOVE LNE-ADR-LINE3 TO
               ADR-LINE3-4007 OF R4007-LCTN-EFCTV
                                                                     .
           MOVE LNE-CITY-NAME TO
               CITY-NAME-4007 OF R4007-LCTN-EFCTV
                                                                     .
           MOVE LNE-STATE-CD TO
               STATE-CODE-4007 OF R4007-LCTN-EFCTV
                                                                     .
           MOVE LNE-ZIP-CD TO
               ZIP-CODE-4007 OF R4007-LCTN-EFCTV
                                                                     .
           MOVE LNE-CNTY-CD TO
               CNTY-CODE-4007 OF R4007-LCTN-EFCTV
                                                                     .
           MOVE LNE-CNTRY-CD TO
               CNTRY-CODE-4007 OF R4007-LCTN-EFCTV
                                                                     .
           MOVE LNE-TLPHN-ID TO
               TLPHN-ID-4007 OF R4007-LCTN-EFCTV
                                                                     .
           MOVE LNE-SQR-FTGE TO
               SQR-FTGE-4007 OF R4007-LCTN-EFCTV
                                                                     .
           MOVE LNE-SQR-FTGE-RATE TO
               SQR-FTGE-RATE-4007 OF R4007-LCTN-EFCTV
                                                                     .
           MOVE LNE-PREDCSR-CD TO
               PREDCSR-CODE-4007 OF R4007-LCTN-EFCTV
                                                                     .
           MOVE LNE-LCTN-CD TO
               LCTN-CODE-4007 OF R4007-LCTN-EFCTV
                                                                     .
       975-TO-R4007-LCTN-EFCTV-EXIT.
           EXIT.
