       01  R4054-XTRN-CODE.
           02  DB-PROC-ID-4054.
               03  USER-CODE-4054                       PIC X(8).
               03  LAST-ACTVY-DATE-4054                 PIC X(5).
               03  TRMNL-ID-4054                        PIC X(8).
               03  PURGE-FLAG-4054                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  XTRNL-CODE-KEY-4054.
               03  UNVRS-CODE-4054                      PIC X(2).
               03  COA-CODE-4054                        PIC X(1).
               03  XTRNL-CODE-4054                      PIC X(10).
           02  ACTVY-DATE-4054                COMP-3    PIC S9(8).
           02  STATUS-4054                              PIC X(1).
           02  XTRNL-CODE-DESC-4054                     PIC X(35).
           02  INTRL-ENTY-CODE-4054                     PIC X(4).
