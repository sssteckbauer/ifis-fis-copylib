    ***Created by Convert/DC version V8R03 on 12/13/00 at 10:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE DCSD-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY01.
      *%--------------------------------------------------------------%*
           MOVE CNFDL-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0A.
      *%--------------------------------------------------------------%*
           MOVE HOLDS-TEXT-SY01 OF FSYWG01M TO WK01-TEXT-SY0B.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4026-CH61 OF FCHWK61 TO WK01-CODE-4026-CH61.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-CODE-4026-CH61 OF FCHWK61 TO
                WK01-COST-CODE-4026-CH61.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-DESC-4065-CH61 OF FCHWM61 TO
                WK01-COST-DESC-4065-CH61.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-BASIS-4065-CH61 OF FCHWM61 TO
                WK01-COST-BASIS-4065-CH61.
      *%--------------------------------------------------------------%*
           MOVE APPLY-TO-BASIS-IND-4065-CH61 OF FCHWM61 TO
                WK01-TO-BSS-IND-4065-CH61.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-STNDD-PCT-4065-CH61 OF FCHWM61 TO
                WK01-CST-STNDD-PCT-4065-CH6.
      *%--------------------------------------------------------------%*
           MOVE CMPLT-IND-4065-CH61 OF FCHWM61 TO WK01-IND-4065-CH61.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH61 OF FCHWM61(0001) TO WK01-CODE-CH611.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH61 OF FCHWM61(0002) TO WK01-CODE-CH612.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH61 OF FCHWM61(0003) TO WK01-CODE-CH613.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH61 OF FCHWM61(0004) TO WK01-CODE-CH614.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH61 OF FCHWM61(0005) TO WK01-CODE-CH615.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH61 OF FCHWM61(0006) TO WK01-CODE-CH616.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH61 OF FCHWM61(0007) TO WK01-CODE-CH617.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH61 OF FCHWM61(0008) TO WK01-CODE-CH618.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-CH61 OF FCHWM61(0009) TO WK01-CODE-CH619.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-FROM-ACCT-4027-CH61 OF FCHWM61(0001) TO
                WK01-CST-FRM-ACCT-4027-CH611.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-FROM-ACCT-4027-CH61 OF FCHWM61(0002) TO
                WK01-CST-FRM-ACCT-4027-CH612.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-FROM-ACCT-4027-CH61 OF FCHWM61(0003) TO
                WK01-CST-FRM-ACCT-4027-CH613.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-FROM-ACCT-4027-CH61 OF FCHWM61(0004) TO
                WK01-CST-FRM-ACCT-4027-CH614.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-FROM-ACCT-4027-CH61 OF FCHWM61(0005) TO
                WK01-CST-FRM-ACCT-4027-CH615.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-FROM-ACCT-4027-CH61 OF FCHWM61(0006) TO
                WK01-CST-FRM-ACCT-4027-CH616.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-FROM-ACCT-4027-CH61 OF FCHWM61(0007) TO
                WK01-CST-FRM-ACCT-4027-CH617.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-FROM-ACCT-4027-CH61 OF FCHWM61(0008) TO
                WK01-CST-FRM-ACCT-4027-CH618.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-FROM-ACCT-4027-CH61 OF FCHWM61(0009) TO
                WK01-CST-FRM-ACCT-4027-CH619.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-TO-ACCT-4027-CH61 OF FCHWM61(0001) TO
                WK01-CST-T-ACCT-4027-CH611.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-TO-ACCT-4027-CH61 OF FCHWM61(0002) TO
                WK01-CST-T-ACCT-4027-CH612.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-TO-ACCT-4027-CH61 OF FCHWM61(0003) TO
                WK01-CST-T-ACCT-4027-CH613.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-TO-ACCT-4027-CH61 OF FCHWM61(0004) TO
                WK01-CST-T-ACCT-4027-CH614.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-TO-ACCT-4027-CH61 OF FCHWM61(0005) TO
                WK01-CST-T-ACCT-4027-CH615.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-TO-ACCT-4027-CH61 OF FCHWM61(0006) TO
                WK01-CST-T-ACCT-4027-CH616.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-TO-ACCT-4027-CH61 OF FCHWM61(0007) TO
                WK01-CST-T-ACCT-4027-CH617.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-TO-ACCT-4027-CH61 OF FCHWM61(0008) TO
                WK01-CST-T-ACCT-4027-CH618.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-TO-ACCT-4027-CH61 OF FCHWM61(0009) TO
                WK01-CST-T-ACCT-4027-CH619.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-PCT-4027-CH61 OF FCHWM61(0001) TO
                WK01-COST-PCT-4027-CH611.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-PCT-4027-CH61 OF FCHWM61(0002) TO
                WK01-COST-PCT-4027-CH612.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-PCT-4027-CH61 OF FCHWM61(0003) TO
                WK01-COST-PCT-4027-CH613.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-PCT-4027-CH61 OF FCHWM61(0004) TO
                WK01-COST-PCT-4027-CH614.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-PCT-4027-CH61 OF FCHWM61(0005) TO
                WK01-COST-PCT-4027-CH615.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-PCT-4027-CH61 OF FCHWM61(0006) TO
                WK01-COST-PCT-4027-CH616.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-PCT-4027-CH61 OF FCHWM61(0007) TO
                WK01-COST-PCT-4027-CH617.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-PCT-4027-CH61 OF FCHWM61(0008) TO
                WK01-COST-PCT-4027-CH618.
      *%--------------------------------------------------------------%*
           MOVE INDRT-COST-PCT-4027-CH61 OF FCHWM61(0009) TO
                WK01-COST-PCT-4027-CH619.
      *%--------------------------------------------------------------%*
           MOVE PO-SUBCNT-EXCL-IND-CH61 TO
                       WK01-PO-SUBCNT-EXCL-IND-CH61.
