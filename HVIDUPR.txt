       975-TO-R4192-USER-PRFL SECTION.
           MOVE UPR-USER-CD TO
               USER-CODE-4192 OF R4192-USER-PRFL
                                                                     .
           MOVE UPR-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4192 OF R4192-USER-PRFL
                                                                     .
           MOVE UPR-UNVRS-CD TO
               UNVRS-CODE-4192 OF R4192-USER-PRFL
                                                                     .
           MOVE UPR-USER-ID TO
               USER-ID-4192 OF R4192-USER-PRFL
                                                                     .
       975-TO-R4192-USER-PRFL-EXIT.
           EXIT.
