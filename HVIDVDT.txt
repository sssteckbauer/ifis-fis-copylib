       975-TO-R4166-VNDR-TYPE SECTION.
           MOVE VDT-USER-CD TO
               USER-CODE-4166 OF R4166-VNDR-TYPE
                                                                     .
           MOVE VDT-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4166 OF R4166-VNDR-TYPE
                                                                     .
           MOVE VDT-UNVRS-CD TO
               UNVRS-CODE-4166 OF R4166-VNDR-TYPE
                                                                     .
           MOVE VDT-VNDR-TYP-CD TO
               VNDR-TYPE-CODE-4166 OF R4166-VNDR-TYPE
                                                                     .
           MOVE VDT-VNDR-TYP-DESC TO
               VNDR-TYPE-DESC-4166 OF R4166-VNDR-TYPE
                                                                     .
           MOVE VDT-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACTVY-DATE-4166 OF R4166-VNDR-TYPE
                                                                     .
           MOVE VDT-START-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               START-DATE-4166 OF R4166-VNDR-TYPE
                                                                     .
           MOVE VDT-END-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               END-DATE-4166 OF R4166-VNDR-TYPE
                                                                     .
       975-TO-R4166-VNDR-TYPE-EXIT.
           EXIT.
