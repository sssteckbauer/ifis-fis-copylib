       976-FROM-R4003-ACCT SECTION.

FCYI **        MOVE USER-CODE-4003 OF R4003-ACCT
FCYI           MOVE DBLINK-USER-CD
               TO ACC-USER-CD.

               MOVE LAST-ACTVY-DATE-4003 OF R4003-ACCT
               TO ACC-LAST-ACTVY-DT.

               MOVE UNVRS-CODE-4003 OF R4003-ACCT
               TO ACC-UNVRS-CD.

               MOVE COA-CODE-4003 OF R4003-ACCT
               TO ACC-COA-CD.

               MOVE ACCT-CODE-4003 OF R4003-ACCT
               TO ACC-ACCT-CD.

           IF ACTVY-DATE-4003 OF R4003-ACCT  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO ACC-ACTVY-DT
           ELSE
               MOVE ACTVY-DATE-4003 OF R4003-ACCT
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO ACC-ACTVY-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO ACC-ACTVY-DT
               END-IF
           END-IF.
       976-FROM-R4003-ACCT-EXIT.
           EXIT.
