       01  R6268.
           02  DB-PROC-ID-6268.
               03  USER-CODE-6268                       PIC X(8).
               03  LAST-ACTVY-DATE-6268                 PIC X(5).
               03  TRMNL-ID-6268                        PIC X(8).
               03  PURGE-FLAG-6268                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  PARA-KEY-6268.
               03  UNVRS-CODE-6268                      PIC X(2).
               03  PARA-CODE-6268                       PIC X(4).
           02  SHORT-DESC-6268                          PIC X(10).
           02  LONG-DESC-6268                           PIC X(30).
           02  COMM-VIEW-6268                           PIC X(8).
           02  OFC-CODE-6268                            PIC X(4).
