      *%---------------------------------------------------------------*
      *%                                                               *
      *%                   200-FSYIA07 ROUTINE                         *
      *%                                                               *
      *%---------------------------------------------------------------*
       200-FSYIA07.

      ******************************************************************
      **********************  BEGINNING OF MODULE  *********************
      ******************************************************************
      ******************************************************************
      ***  DIALOG MUST INCLUDE DATA IN THE FOLLOWING FIELDS;
      ***    INTRNL-REF-ID-UT01
      ***    ENTY-PRSN-IND-UT01
      ***    UNVRS-CODE-UT01
      ***    TRANS-DATE-UT01
      ***    ADR-TYPE-CODE-UT01
      ***  MUST BE INCLUDED WITHIN A SUBROUTINE CALLED FROM MAIN DIALOG
      ***
      ******************************************************************
      ******************************************************************
      ***                           CHANGE HISTORY
      ***
      *** REF  AUTH    DATE                  DESCRIPTION               T
      *SIS  *
      *** ---  ----  --------  --------------------------------------  -
      *---- *
      ******************************************************************
      *
           INITIALIZE AHE-AHE-ACH-EFCTV.

      *%**DATE/TIME CNV** IDD KEY (WS)FLD -> DBLINKX-INPUT             *
           MOVE TRANS-DATE-UT01  TO DBLINKX-INPUT-PARM-08
      *%**DATE/TIME CNV**  CONVERT DATE                                *
           MOVE '1' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
FXXCHG     CALL 'IDDTECNV' USING
                   DBLINKX-DATE-TIME-PARMS
           IF NOT DBLINKX-OK
      *%**DATE/TIME CNV**  CONVERT DATE FAILED                         *
           MOVE DBLINKX-NULL-DB2-DATE-ISO TO DBLINKX-OUTPUT-PARM
           END-IF
      *%**DATE/TIME CNV**  DBLINKXD -> KEY (HV)FLD                     *

           MOVE DBLINKX-OUTPUT-PARM TO AHE-END-DT
           MOVE INTRL-REF-ID-UT01   TO AHE-FK-ACD-IREF-ID
           MOVE ENTY-PRSN-IND-UT01  TO AHE-FK-ACD-ENTPSN-IND
           MOVE 'PR'                TO AHE-ADR-TYP-CD
      *
      *%   OBTAIN R6201-ACH-EFCTV(AHE_ACH_EFCTV_V) FOR PAYROLL ADDRESS
      *%   TYPE
      *
           MOVE 0198 TO DBLINK-CURR-IO-NUM

           PERFORM 200-FSYIA07-20
              THRU 200-FSYIA07-20-EXIT.
           IF SQLCODE =  +0
              IF AHE-ACH-STATUS     = 'V' OR 'R'
                 MOVE AHE-ADR-TYP-CD  TO ADR-TYPE-CODE-UT01
                 GO TO 200-FSYIA07-EXIT
              END-IF
           ELSE
               IF SQLCODE = +100
                  MOVE ZERO TO SQLCODE
               ELSE
                  MOVE DBLINK-DB2-ERROR TO ERROR-STATUS
                  PERFORM 999-SQL-STATUS
                     THRU 999-SQL-STATUS-EXIT.

      *
      * OBTAIN AHE RECORD USING THE ADDRESS TYPE FROM MAP
      *
           MOVE ADR-TYPE-CODE-UT01 TO AHE-ADR-TYP-CD.
           MOVE 0199 TO DBLINK-CURR-IO-NUM

           PERFORM 200-FSYIA07-20
              THRU 200-FSYIA07-20-EXIT.

            IF SQLCODE = +0
                 MOVE AHE-ADR-TYP-CD     TO ADR-TYPE-CODE-UT01
                 GO TO 200-FSYIA07-EXIT
            ELSE
                IF SQLCODE = +100
                    CONTINUE
                ELSE
                    MOVE DBLINK-DB2-ERROR TO ERROR-STATUS
                    PERFORM 999-SQL-STATUS
                       THRU 999-SQL-STATUS-EXIT.

      *
           MOVE 'AC' TO AHE-ADR-TYP-CD.
           MOVE 0202 TO DBLINK-CURR-IO-NUM
           PERFORM 200-FSYIA07-20
              THRU 200-FSYIA07-20-EXIT.

            IF SQLCODE = +0
                 MOVE AHE-ADR-TYP-CD     TO ADR-TYPE-CODE-UT01
                 GO TO 200-FSYIA07-EXIT
            ELSE
                IF SQLCODE = +100
                    CONTINUE
                ELSE
                    MOVE DBLINK-DB2-ERROR TO ERROR-STATUS
                    PERFORM 999-SQL-STATUS
                       THRU 999-SQL-STATUS-EXIT.
      *
           MOVE SPACES TO ADR-TYPE-CODE-UT01.
      *

       200-FSYIA07-EXIT.
           EXIT.

       200-FSYIA07-20.
           MOVE ZEROS TO SQLCODE
                         ERROR-STATUS.

           EXEC SQL
               SELECT
                  A. USER_CD
                 ,A.LAST_ACTVY_DT
                 ,A.ADR_TYP_CD
                 ,A.END_DT
                 ,A.START_DT
                 ,A.ACH_RCVNG_DFI_ID
                 ,A.ACH_CHECK_DGT
                 ,A.ACH_DFI_ACCT_NBR
                 ,A.ACH_ACCT_TYP
                 ,A.ACH_STATUS
                 ,A.ACH_ERROR_RSN
                 ,A.ACH_PRENOTE_DOCNBR
                 ,A.ACH_PRENOTE_DT
                 ,A.FK_ACD_IREF_ID
                 ,A.FK_ACD_ENTPSN_IND
               INTO
                  :AHE-USER-CD
                 ,:AHE-LAST-ACTVY-DT
                 ,:AHE-ADR-TYP-CD
                 ,:AHE-END-DT
                 ,:AHE-START-DT
                 ,:AHE-ACH-RCVNG-DFI-ID
                 ,:AHE-ACH-CHECK-DGT
                 ,:AHE-ACH-DFI-ACCT-NBR
                 ,:AHE-ACH-ACCT-TYP
                 ,:AHE-ACH-STATUS
                 ,:AHE-ACH-ERROR-RSN
                 ,:AHE-ACH-PRENOTE-DOCNBR
                 ,:AHE-ACH-PRENOTE-DT
                 ,:AHE-FK-ACD-IREF-ID
                 ,:AHE-FK-ACD-ENTPSN-IND
               FROM AHE_ACH_EFCTV_V A
               WHERE A.FK_ACD_IREF_ID    = :AHE-FK-ACD-IREF-ID
                 AND A.FK_ACD_ENTPSN_IND = :AHE-FK-ACD-ENTPSN-IND
                 AND A.ADR_TYP_CD        = :AHE-ADR-TYP-CD
                 AND A.START_DT  =
                     (SELECT MAX(B.START_DT)
                       FROM AHE_ACH_EFCTV_V B
                      WHERE B.FK_ACD_IREF_ID    = A.FK_ACD_IREF_ID
                        AND B.FK_ACD_ENTPSN_IND = A.FK_ACD_ENTPSN_IND
                        AND B.ADR_TYP_CD        = A.ADR_TYP_CD
                        AND B.START_DT  <= :AHE-END-DT)

                 AND  A.END_DT            >= : AHE-END-DT
           END-EXEC.
       200-FSYIA07-20-EXIT.
           EXIT.
