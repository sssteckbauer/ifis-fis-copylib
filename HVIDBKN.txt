       975-TO-R4046-BANK-NAME SECTION.
           MOVE BKN-USER-CD TO
               USER-CODE-4046 OF R4046-BANK-NAME
                                                                     .
           MOVE BKN-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4046 OF R4046-BANK-NAME
                                                                     .
           MOVE BKN-UNVRS-CD TO
               UNVRS-CODE-4046 OF R4046-BANK-NAME
                                                                     .
           MOVE BKN-IREF-ID TO
               INTRL-REF-ID-4046 OF R4046-BANK-NAME
                                                                     .
           MOVE BKN-BANK-ID-DGT-ONE TO
               BANK-ID-DIGIT-ONE-4046 OF R4046-BANK-NAME
                                                                     .
           MOVE BKN-BANK-ID-LST-NINE TO
               BANK-ID-LAST-NINE-4046 OF R4046-BANK-NAME
                                                                     .
           MOVE BKN-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACTVY-DATE-4046 OF R4046-BANK-NAME
                                                                     .
           MOVE BKN-CNTCT-NAME TO
               CNTCT-NAME-4046 OF R4046-BANK-NAME
                                                                     .
           MOVE BKN-CNTCT-TLPHN-ID TO
               CNTCT-TLPHN-ID-4046 OF R4046-BANK-NAME
                                                                     .
           MOVE BKN-STATUS TO
               STATUS-4046 OF R4046-BANK-NAME
                                                                     .
       975-TO-R4046-BANK-NAME-EXIT.
           EXIT.
