       01  MAP-CONTROL.

    ***Created by Convert/DC version V8R03 on 05/24/00 at 10:00***

           02  FPUMU57-MAP-CONTROL.
           03  FPUMU57T.
           04  WK01-DCXMAPL-PARMS.
               10  WK01-MAPNAME                  PIC X(8)
                                                 VALUE 'FPUMU57I'.
               10  WK01-IKE-LENGTH               PIC S9(8)  COMP
FCAMH *                                          VALUE 8389.
FCAMH                                            VALUE 8393.
               10  WK01-TINA-LENGTH              PIC S9(4)  COMP
                                                 VALUE 635.
           05  WK01-ATTRIBUTES                   PIC X(60)
                                                 VALUE SPACE.
           05  WK01-ATTRIBUTES-ERROR             PIC X(60) VALUE
               ' '.
           05  WK01-CURSOR-DFLD                  PIC X(36)
                                                 VALUE SPACES.
           05  WK01-CURSOR-DFLD-QUAL             PIC X(30)
                                                 VALUE SPACES.
           05  WK01-ROW                          PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-COLUMN                       PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-ERROR-MSG                    PIC X(80)
                                                 VALUE SPACES.
           05  WK01-MAP-DATA-LEN                 PIC S9(6)
                                                 VALUE ZERO.
           05  WK01-1ST-FLAG                     PIC X(1)
                                                 VALUE 'Y'.
           05  WK01-FLAG-OUTPUT-DATA             PIC X(1)
                                                 VALUE 'Y'.
      *    05  WK01-STATUS-RETURN-FLAG           PIC X(1)
      *                                          VALUE SPACES.
           05  WK01-ALL-BUT-FLAG                 PIC X(1)
                                                 VALUE SPACES.
           05  WK01-FLAG-ALL-FIELDS-CHANGED      PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-CHANGED      PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERROR        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERROR        PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.

           05  WK01-VARDATA                      PIC X(11880)
                                                 VALUE SPACES.
           05  FILLER                            PIC X(100)
                                                 VALUE SPACES.
           04  WK01-ELEMENT-AREA.

               10  WK01-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-6001-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00       PIC X(35)
                                               VALUE SPACES.
               10  WK01-DATE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-HH-MM-TIME-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00       PIC X(4)
                                               VALUE SPACES.
               10  WK01-PM-FLAG-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-USER-ID-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID       PIC X(32)
                                               VALUE SPACES.
               10  WK01-RSPNS-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-FUNC-DESCRIPTION-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION       PIC X(28)
                                               VALUE SPACES.
               10  WK01-STATUS-CODE-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-CODE-6001-PU57-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6001-PU57-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6001-PU57-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6001-PU57-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6001-PU57-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6001-PU57       PIC X(6)
                                               VALUE SPACES.
               10  WK01-NAME-6001-PU57-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-PU57-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-PU57-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-PU57-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-PU57-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-PU57       PIC X(35)
                                               VALUE SPACES.
               10  WK01-CODE-4070-PU57-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4070-PU57-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4070-PU57-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4070-PU57-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4070-PU57-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4070-PU57       PIC X(8)
                                               VALUE SPACES.
               10  WK01-ITEM-NMBR-4071-PU57-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ITEM-NMBR-4071-PU57-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ITEM-NMBR-4071-PU57-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ITEM-NMBR-4071-PU57-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ITEM-NMBR-4071-PU57-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ITEM-NMBR-4071-PU57       PIC X(4)
                                               VALUE SPACES.
               10  WK01-CODE-4072-PU57-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4072-PU57-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4072-PU57-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4072-PU57-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4072-PU57-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4072-PU57       PIC X(4)
                                               VALUE SPACES.
               10  WK01-NMBR-4083-PU57-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-4083-PU57-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-4083-PU57-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-4083-PU57-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-4083-PU57-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-4083-PU57       PIC X(8)
                                               VALUE SPACES.
               10  WK01-SEQ-NMBR-4096-PU57-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-NMBR-4096-PU57-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-NMBR-4096-PU57-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-NMBR-4096-PU57-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-NMBR-4096-PU57-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-NMBR-4096-PU57       PIC X(3)
                                               VALUE SPACES.
               10  WK01-ITEM-NMBR-4085-PU57-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ITEM-NMBR-4085-PU57-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ITEM-NMBR-4085-PU57-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ITEM-NMBR-4085-PU57-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ITEM-NMBR-4085-PU57-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ITEM-NMBR-4085-PU57       PIC X(4)
                                               VALUE SPACES.
               10  WK01-CODE-4081-PU57-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4081-PU57-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4081-PU57-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4081-PU57-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4081-PU57-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4081-PU57       PIC X(15)
                                               VALUE SPACES.
               10  WK01-NMBR-4088-PU57-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-4088-PU57-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-4088-PU57-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-4088-PU57-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-4088-PU57-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NMBR-4088-PU57       PIC X(8)
                                               VALUE SPACES.
               10  WK01-ITEM-NMBR-4089-PU57-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ITEM-NMBR-4089-PU57-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ITEM-NMBR-4089-PU57-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ITEM-NMBR-4089-PU57-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ITEM-NMBR-4089-PU57-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ITEM-NMBR-4089-PU57       PIC X(4)
                                               VALUE SPACES.
               10  WK01-CODE-4074-PU57-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4074-PU57-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4074-PU57-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4074-PU57-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4074-PU57-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4074-PU57       PIC X(8)
                                               VALUE SPACES.
               10  WK01-CODE-4099-PU57-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4099-PU57-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4099-PU57-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4099-PU57-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4099-PU57-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4099-PU57       PIC X(4)
                                               VALUE SPACES.
               10  WK01-TO-CODE-4086-PU57-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TO-CODE-4086-PU57-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TO-CODE-4086-PU57-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TO-CODE-4086-PU57-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TO-CODE-4086-PU57-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TO-CODE-4086-PU57       PIC X(6)
                                               VALUE SPACES.
               10  WK01-CODE-4073-PU57-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4073-PU57-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4073-PU57-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4073-PU57-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4073-PU57-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4073-PU57       PIC X(9)
                                               VALUE SPACES.
               10  WK01-CODE-4002-PU57-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4002-PU57-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4002-PU57-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4002-PU57-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4002-PU57-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4002-PU57       PIC X(1)
                                               VALUE SPACES.
               10  WK01-CODE-4002-PU5A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4002-PU5A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4002-PU5A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4002-PU5A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4002-PU5A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4002-PU5A       PIC X(6)
                                               VALUE SPACES.
               10  WK01-DEST-CODE-4167-PU57-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DEST-CODE-4167-PU57-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DEST-CODE-4167-PU57-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DEST-CODE-4167-PU57-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DEST-CODE-4167-PU57-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DEST-CODE-4167-PU57       PIC X(8)
                                               VALUE SPACES.
               10  WK01-ZZMESSAGE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE       PIC X(160)
                                               VALUE SPACES.
               10  WK01-PASSED-ONE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE       PIC X(32)
                                               VALUE SPACES.
               10  WK01-MAP-RESPONSE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-TEXT-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00       PIC X(14)
                                               VALUE SPACES.
