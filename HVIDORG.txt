       975-TO-R4002-ORGN SECTION.
           MOVE ORG-USER-CD TO
               USER-CODE-4002 OF R4002-ORGN
                                                                     .
           MOVE ORG-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4002 OF R4002-ORGN
                                                                     .
           MOVE ORG-UNVRS-CD TO
               UNVRS-CODE-4002 OF R4002-ORGN
                                                                     .
           MOVE ORG-COA-CD TO
               COA-CODE-4002 OF R4002-ORGN
                                                                     .
           MOVE ORG-ORGN-CD TO
               ORGZN-CODE-4002 OF R4002-ORGN
                                                                     .
           MOVE ORG-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACTVY-DATE-4002 OF R4002-ORGN
                                                                     .
           MOVE ORG-ENCMBR-LDGR-IND TO
               ENCMBR-LDGR-IND-4002 OF R4002-ORGN
                                                                     .
           MOVE ORG-ENCMBR-LDGR-USR TO
               ENCMBR-LDGR-USR-4002 OF R4002-ORGN
                                                                     .
           MOVE ORG-OPR-LDGR-IND TO
               OPR-LDGR-IND-4002 OF R4002-ORGN
                                                                     .
           MOVE ORG-OPR-LDGR-USR TO
               OPR-LDGR-USR-4002 OF R4002-ORGN
                                                                     .
           MOVE ORG-DEPT-LVL-IND TO
               DEPT-LVL-IND-4002 OF R4002-ORGN
                                                                     .
       975-TO-R4002-ORGN-EXIT.
           EXIT.
