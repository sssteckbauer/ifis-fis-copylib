    ***Created by Convert/DC version V8R03 on 12/05/00 at 12:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-EX85-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-EX85 TO VNDR-ID-LAST-NINE-EX85 OF
                FEXWK85
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX85-Z = 'Y'
               MOVE WK01-NMBR-EX85 TO STMNT-NMBR-EX85 OF FEXWK85
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-EX85-Z = 'Y'
               MOVE WK01-NAME-EX85 TO VNDR-NAME-EX85 OF FEXWK85
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RUN-DATE-EX85-Z = 'Y'
               MOVE WK01-RUN-DATE-EX85 TO STMNT-RUN-DATE-EX85 OF FEXWK85
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RELEASE-NMBR-EX85-Z = 'Y'
               MOVE WK01-RELEASE-NMBR-EX85 TO REQ-RELEASE-NMBR-EX85 OF
                FEXWK85
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX8A-Z = 'Y'
               MOVE WK01-NMBR-EX8A TO CHECK-NMBR-EX85 OF FEXWK85
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX851-Z = 'Y'
               MOVE WK01-CODE-EX851 TO ACTN-CODE-EX85 OF FEXWM85(0001)
           END-IF.
           MOVE WK01-CODE-EX851-F TO WK01-CODE-EX85-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX852-Z = 'Y'
               MOVE WK01-CODE-EX852 TO ACTN-CODE-EX85 OF FEXWM85(0002)
           END-IF.
           MOVE WK01-CODE-EX852-F TO WK01-CODE-EX85-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX853-Z = 'Y'
               MOVE WK01-CODE-EX853 TO ACTN-CODE-EX85 OF FEXWM85(0003)
           END-IF.
           MOVE WK01-CODE-EX853-F TO WK01-CODE-EX85-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX854-Z = 'Y'
               MOVE WK01-CODE-EX854 TO ACTN-CODE-EX85 OF FEXWM85(0004)
           END-IF.
           MOVE WK01-CODE-EX854-F TO WK01-CODE-EX85-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX855-Z = 'Y'
               MOVE WK01-CODE-EX855 TO ACTN-CODE-EX85 OF FEXWM85(0005)
           END-IF.
           MOVE WK01-CODE-EX855-F TO WK01-CODE-EX85-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX856-Z = 'Y'
               MOVE WK01-CODE-EX856 TO ACTN-CODE-EX85 OF FEXWM85(0006)
           END-IF.
           MOVE WK01-CODE-EX856-F TO WK01-CODE-EX85-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX857-Z = 'Y'
               MOVE WK01-CODE-EX857 TO ACTN-CODE-EX85 OF FEXWM85(0007)
           END-IF.
           MOVE WK01-CODE-EX857-F TO WK01-CODE-EX85-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX858-Z = 'Y'
               MOVE WK01-CODE-EX858 TO ACTN-CODE-EX85 OF FEXWM85(0008)
           END-IF.
           MOVE WK01-CODE-EX858-F TO WK01-CODE-EX85-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX859-Z = 'Y'
               MOVE WK01-CODE-EX859 TO ACTN-CODE-EX85 OF FEXWM85(0009)
           END-IF.
           MOVE WK01-CODE-EX859-F TO WK01-CODE-EX85-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-EX8510-Z = 'Y'
               MOVE WK01-CODE-EX8510 TO ACTN-CODE-EX85 OF FEXWM85(0010)
           END-IF.
           MOVE WK01-CODE-EX8510-F TO WK01-CODE-EX85-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-EX851-Z = 'Y'
               MOVE WK01-DATE-EX851 TO ORDER-DATE-EX85 OF FEXWM85(0001)
           END-IF.
           MOVE WK01-DATE-EX851-F TO WK01-DATE-EX85-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-EX852-Z = 'Y'
               MOVE WK01-DATE-EX852 TO ORDER-DATE-EX85 OF FEXWM85(0002)
           END-IF.
           MOVE WK01-DATE-EX852-F TO WK01-DATE-EX85-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-EX853-Z = 'Y'
               MOVE WK01-DATE-EX853 TO ORDER-DATE-EX85 OF FEXWM85(0003)
           END-IF.
           MOVE WK01-DATE-EX853-F TO WK01-DATE-EX85-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-EX854-Z = 'Y'
               MOVE WK01-DATE-EX854 TO ORDER-DATE-EX85 OF FEXWM85(0004)
           END-IF.
           MOVE WK01-DATE-EX854-F TO WK01-DATE-EX85-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-EX855-Z = 'Y'
               MOVE WK01-DATE-EX855 TO ORDER-DATE-EX85 OF FEXWM85(0005)
           END-IF.
           MOVE WK01-DATE-EX855-F TO WK01-DATE-EX85-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-EX856-Z = 'Y'
               MOVE WK01-DATE-EX856 TO ORDER-DATE-EX85 OF FEXWM85(0006)
           END-IF.
           MOVE WK01-DATE-EX856-F TO WK01-DATE-EX85-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-EX857-Z = 'Y'
               MOVE WK01-DATE-EX857 TO ORDER-DATE-EX85 OF FEXWM85(0007)
           END-IF.
           MOVE WK01-DATE-EX857-F TO WK01-DATE-EX85-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-EX858-Z = 'Y'
               MOVE WK01-DATE-EX858 TO ORDER-DATE-EX85 OF FEXWM85(0008)
           END-IF.
           MOVE WK01-DATE-EX858-F TO WK01-DATE-EX85-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-EX859-Z = 'Y'
               MOVE WK01-DATE-EX859 TO ORDER-DATE-EX85 OF FEXWM85(0009)
           END-IF.
           MOVE WK01-DATE-EX859-F TO WK01-DATE-EX85-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-EX8510-Z = 'Y'
               MOVE WK01-DATE-EX8510 TO ORDER-DATE-EX85 OF FEXWM85(0010)
           END-IF.
           MOVE WK01-DATE-EX8510-F TO WK01-DATE-EX85-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-EX851-Z = 'Y'
               MOVE WK01-DESC-EX851 TO CATALOG-DESC-EX85 OF
                FEXWM85(0001)
           END-IF.
           MOVE WK01-DESC-EX851-F TO WK01-DESC-EX85-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-EX852-Z = 'Y'
               MOVE WK01-DESC-EX852 TO CATALOG-DESC-EX85 OF
                FEXWM85(0002)
           END-IF.
           MOVE WK01-DESC-EX852-F TO WK01-DESC-EX85-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-EX853-Z = 'Y'
               MOVE WK01-DESC-EX853 TO CATALOG-DESC-EX85 OF
                FEXWM85(0003)
           END-IF.
           MOVE WK01-DESC-EX853-F TO WK01-DESC-EX85-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-EX854-Z = 'Y'
               MOVE WK01-DESC-EX854 TO CATALOG-DESC-EX85 OF
                FEXWM85(0004)
           END-IF.
           MOVE WK01-DESC-EX854-F TO WK01-DESC-EX85-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-EX855-Z = 'Y'
               MOVE WK01-DESC-EX855 TO CATALOG-DESC-EX85 OF
                FEXWM85(0005)
           END-IF.
           MOVE WK01-DESC-EX855-F TO WK01-DESC-EX85-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-EX856-Z = 'Y'
               MOVE WK01-DESC-EX856 TO CATALOG-DESC-EX85 OF
                FEXWM85(0006)
           END-IF.
           MOVE WK01-DESC-EX856-F TO WK01-DESC-EX85-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-EX857-Z = 'Y'
               MOVE WK01-DESC-EX857 TO CATALOG-DESC-EX85 OF
                FEXWM85(0007)
           END-IF.
           MOVE WK01-DESC-EX857-F TO WK01-DESC-EX85-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-EX858-Z = 'Y'
               MOVE WK01-DESC-EX858 TO CATALOG-DESC-EX85 OF
                FEXWM85(0008)
           END-IF.
           MOVE WK01-DESC-EX858-F TO WK01-DESC-EX85-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-EX859-Z = 'Y'
               MOVE WK01-DESC-EX859 TO CATALOG-DESC-EX85 OF
                FEXWM85(0009)
           END-IF.
           MOVE WK01-DESC-EX859-F TO WK01-DESC-EX85-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-EX8510-Z = 'Y'
               MOVE WK01-DESC-EX8510 TO CATALOG-DESC-EX85 OF
                FEXWM85(0010)
           END-IF.
           MOVE WK01-DESC-EX8510-F TO WK01-DESC-EX85-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-EX851-Z = 'Y'
               MOVE WK01-EX851 TO QTY-EX85 OF FEXWM85(0001)
           END-IF.
           MOVE WK01-EX851-F TO WK01-EX85-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-EX852-Z = 'Y'
               MOVE WK01-EX852 TO QTY-EX85 OF FEXWM85(0002)
           END-IF.
           MOVE WK01-EX852-F TO WK01-EX85-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-EX853-Z = 'Y'
               MOVE WK01-EX853 TO QTY-EX85 OF FEXWM85(0003)
           END-IF.
           MOVE WK01-EX853-F TO WK01-EX85-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-EX854-Z = 'Y'
               MOVE WK01-EX854 TO QTY-EX85 OF FEXWM85(0004)
           END-IF.
           MOVE WK01-EX854-F TO WK01-EX85-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-EX855-Z = 'Y'
               MOVE WK01-EX855 TO QTY-EX85 OF FEXWM85(0005)
           END-IF.
           MOVE WK01-EX855-F TO WK01-EX85-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-EX856-Z = 'Y'
               MOVE WK01-EX856 TO QTY-EX85 OF FEXWM85(0006)
           END-IF.
           MOVE WK01-EX856-F TO WK01-EX85-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-EX857-Z = 'Y'
               MOVE WK01-EX857 TO QTY-EX85 OF FEXWM85(0007)
           END-IF.
           MOVE WK01-EX857-F TO WK01-EX85-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-EX858-Z = 'Y'
               MOVE WK01-EX858 TO QTY-EX85 OF FEXWM85(0008)
           END-IF.
           MOVE WK01-EX858-F TO WK01-EX85-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-EX859-Z = 'Y'
               MOVE WK01-EX859 TO QTY-EX85 OF FEXWM85(0009)
           END-IF.
           MOVE WK01-EX859-F TO WK01-EX85-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-EX8510-Z = 'Y'
               MOVE WK01-EX8510 TO QTY-EX85 OF FEXWM85(0010)
           END-IF.
           MOVE WK01-EX8510-F TO WK01-EX85-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-PRICE-EX851-Z = 'Y'
               MOVE WK01-PRICE-EX851 TO UNIT-PRICE-EX85 OF FEXWM85(0001)
           END-IF.
           MOVE WK01-PRICE-EX851-F TO WK01-PRICE-EX85-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-PRICE-EX852-Z = 'Y'
               MOVE WK01-PRICE-EX852 TO UNIT-PRICE-EX85 OF FEXWM85(0002)
           END-IF.
           MOVE WK01-PRICE-EX852-F TO WK01-PRICE-EX85-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-PRICE-EX853-Z = 'Y'
               MOVE WK01-PRICE-EX853 TO UNIT-PRICE-EX85 OF FEXWM85(0003)
           END-IF.
           MOVE WK01-PRICE-EX853-F TO WK01-PRICE-EX85-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-PRICE-EX854-Z = 'Y'
               MOVE WK01-PRICE-EX854 TO UNIT-PRICE-EX85 OF FEXWM85(0004)
           END-IF.
           MOVE WK01-PRICE-EX854-F TO WK01-PRICE-EX85-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-PRICE-EX855-Z = 'Y'
               MOVE WK01-PRICE-EX855 TO UNIT-PRICE-EX85 OF FEXWM85(0005)
           END-IF.
           MOVE WK01-PRICE-EX855-F TO WK01-PRICE-EX85-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-PRICE-EX856-Z = 'Y'
               MOVE WK01-PRICE-EX856 TO UNIT-PRICE-EX85 OF FEXWM85(0006)
           END-IF.
           MOVE WK01-PRICE-EX856-F TO WK01-PRICE-EX85-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-PRICE-EX857-Z = 'Y'
               MOVE WK01-PRICE-EX857 TO UNIT-PRICE-EX85 OF FEXWM85(0007)
           END-IF.
           MOVE WK01-PRICE-EX857-F TO WK01-PRICE-EX85-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-PRICE-EX858-Z = 'Y'
               MOVE WK01-PRICE-EX858 TO UNIT-PRICE-EX85 OF FEXWM85(0008)
           END-IF.
           MOVE WK01-PRICE-EX858-F TO WK01-PRICE-EX85-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-PRICE-EX859-Z = 'Y'
               MOVE WK01-PRICE-EX859 TO UNIT-PRICE-EX85 OF FEXWM85(0009)
           END-IF.
           MOVE WK01-PRICE-EX859-F TO WK01-PRICE-EX85-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-PRICE-EX8510-Z = 'Y'
               MOVE WK01-PRICE-EX8510 TO UNIT-PRICE-EX85 OF
                FEXWM85(0010)
           END-IF.
           MOVE WK01-PRICE-EX8510-F TO WK01-PRICE-EX85-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-EX851-Z = 'Y'
               MOVE WK01-AMT-EX851 TO TOTAL-AMT-EX85 OF FEXWM85(0001)
           END-IF.
           MOVE WK01-AMT-EX851-F TO WK01-AMT-EX85-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-EX852-Z = 'Y'
               MOVE WK01-AMT-EX852 TO TOTAL-AMT-EX85 OF FEXWM85(0002)
           END-IF.
           MOVE WK01-AMT-EX852-F TO WK01-AMT-EX85-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-EX853-Z = 'Y'
               MOVE WK01-AMT-EX853 TO TOTAL-AMT-EX85 OF FEXWM85(0003)
           END-IF.
           MOVE WK01-AMT-EX853-F TO WK01-AMT-EX85-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-EX854-Z = 'Y'
               MOVE WK01-AMT-EX854 TO TOTAL-AMT-EX85 OF FEXWM85(0004)
           END-IF.
           MOVE WK01-AMT-EX854-F TO WK01-AMT-EX85-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-EX855-Z = 'Y'
               MOVE WK01-AMT-EX855 TO TOTAL-AMT-EX85 OF FEXWM85(0005)
           END-IF.
           MOVE WK01-AMT-EX855-F TO WK01-AMT-EX85-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-EX856-Z = 'Y'
               MOVE WK01-AMT-EX856 TO TOTAL-AMT-EX85 OF FEXWM85(0006)
           END-IF.
           MOVE WK01-AMT-EX856-F TO WK01-AMT-EX85-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-EX857-Z = 'Y'
               MOVE WK01-AMT-EX857 TO TOTAL-AMT-EX85 OF FEXWM85(0007)
           END-IF.
           MOVE WK01-AMT-EX857-F TO WK01-AMT-EX85-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-EX858-Z = 'Y'
               MOVE WK01-AMT-EX858 TO TOTAL-AMT-EX85 OF FEXWM85(0008)
           END-IF.
           MOVE WK01-AMT-EX858-F TO WK01-AMT-EX85-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-EX859-Z = 'Y'
               MOVE WK01-AMT-EX859 TO TOTAL-AMT-EX85 OF FEXWM85(0009)
           END-IF.
           MOVE WK01-AMT-EX859-F TO WK01-AMT-EX85-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-AMT-EX8510-Z = 'Y'
               MOVE WK01-AMT-EX8510 TO TOTAL-AMT-EX85 OF FEXWM85(0010)
           END-IF.
           MOVE WK01-AMT-EX8510-F TO WK01-AMT-EX85-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-EX851-Z = 'Y'
               MOVE WK01-INDX-CODE-EX851 TO ACCT-INDX-CODE-EX85 OF
                FEXWM85(0001)
           END-IF.
           MOVE WK01-INDX-CODE-EX851-F TO WK01-INDX-CODE-EX85-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-EX852-Z = 'Y'
               MOVE WK01-INDX-CODE-EX852 TO ACCT-INDX-CODE-EX85 OF
                FEXWM85(0002)
           END-IF.
           MOVE WK01-INDX-CODE-EX852-F TO WK01-INDX-CODE-EX85-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-EX853-Z = 'Y'
               MOVE WK01-INDX-CODE-EX853 TO ACCT-INDX-CODE-EX85 OF
                FEXWM85(0003)
           END-IF.
           MOVE WK01-INDX-CODE-EX853-F TO WK01-INDX-CODE-EX85-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-EX854-Z = 'Y'
               MOVE WK01-INDX-CODE-EX854 TO ACCT-INDX-CODE-EX85 OF
                FEXWM85(0004)
           END-IF.
           MOVE WK01-INDX-CODE-EX854-F TO WK01-INDX-CODE-EX85-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-EX855-Z = 'Y'
               MOVE WK01-INDX-CODE-EX855 TO ACCT-INDX-CODE-EX85 OF
                FEXWM85(0005)
           END-IF.
           MOVE WK01-INDX-CODE-EX855-F TO WK01-INDX-CODE-EX85-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-EX856-Z = 'Y'
               MOVE WK01-INDX-CODE-EX856 TO ACCT-INDX-CODE-EX85 OF
                FEXWM85(0006)
           END-IF.
           MOVE WK01-INDX-CODE-EX856-F TO WK01-INDX-CODE-EX85-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-EX857-Z = 'Y'
               MOVE WK01-INDX-CODE-EX857 TO ACCT-INDX-CODE-EX85 OF
                FEXWM85(0007)
           END-IF.
           MOVE WK01-INDX-CODE-EX857-F TO WK01-INDX-CODE-EX85-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-EX858-Z = 'Y'
               MOVE WK01-INDX-CODE-EX858 TO ACCT-INDX-CODE-EX85 OF
                FEXWM85(0008)
           END-IF.
           MOVE WK01-INDX-CODE-EX858-F TO WK01-INDX-CODE-EX85-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-EX859-Z = 'Y'
               MOVE WK01-INDX-CODE-EX859 TO ACCT-INDX-CODE-EX85 OF
                FEXWM85(0009)
           END-IF.
           MOVE WK01-INDX-CODE-EX859-F TO WK01-INDX-CODE-EX85-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-EX8510-Z = 'Y'
               MOVE WK01-INDX-CODE-EX8510 TO ACCT-INDX-CODE-EX85 OF
                FEXWM85(0010)
           END-IF.
           MOVE WK01-INDX-CODE-EX8510-F TO WK01-INDX-CODE-EX85-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX8A11-Z = 'Y'
               MOVE WK01-NMBR-EX8A11 TO RELEASE-NMBR-EX85 OF
                FEXWM85(0001)
           END-IF.
           MOVE WK01-NMBR-EX8A11-F TO WK01-NMBR-EX8A1-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX8A12-Z = 'Y'
               MOVE WK01-NMBR-EX8A12 TO RELEASE-NMBR-EX85 OF
                FEXWM85(0002)
           END-IF.
           MOVE WK01-NMBR-EX8A12-F TO WK01-NMBR-EX8A1-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX8A13-Z = 'Y'
               MOVE WK01-NMBR-EX8A13 TO RELEASE-NMBR-EX85 OF
                FEXWM85(0003)
           END-IF.
           MOVE WK01-NMBR-EX8A13-F TO WK01-NMBR-EX8A1-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX8A14-Z = 'Y'
               MOVE WK01-NMBR-EX8A14 TO RELEASE-NMBR-EX85 OF
                FEXWM85(0004)
           END-IF.
           MOVE WK01-NMBR-EX8A14-F TO WK01-NMBR-EX8A1-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX8A15-Z = 'Y'
               MOVE WK01-NMBR-EX8A15 TO RELEASE-NMBR-EX85 OF
                FEXWM85(0005)
           END-IF.
           MOVE WK01-NMBR-EX8A15-F TO WK01-NMBR-EX8A1-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX8A16-Z = 'Y'
               MOVE WK01-NMBR-EX8A16 TO RELEASE-NMBR-EX85 OF
                FEXWM85(0006)
           END-IF.
           MOVE WK01-NMBR-EX8A16-F TO WK01-NMBR-EX8A1-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX8A17-Z = 'Y'
               MOVE WK01-NMBR-EX8A17 TO RELEASE-NMBR-EX85 OF
                FEXWM85(0007)
           END-IF.
           MOVE WK01-NMBR-EX8A17-F TO WK01-NMBR-EX8A1-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX8A18-Z = 'Y'
               MOVE WK01-NMBR-EX8A18 TO RELEASE-NMBR-EX85 OF
                FEXWM85(0008)
           END-IF.
           MOVE WK01-NMBR-EX8A18-F TO WK01-NMBR-EX8A1-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX8A19-Z = 'Y'
               MOVE WK01-NMBR-EX8A19 TO RELEASE-NMBR-EX85 OF
                FEXWM85(0009)
           END-IF.
           MOVE WK01-NMBR-EX8A19-F TO WK01-NMBR-EX8A1-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-EX8A110-Z = 'Y'
               MOVE WK01-NMBR-EX8A110 TO RELEASE-NMBR-EX85 OF
                FEXWM85(0010)
           END-IF.
           MOVE WK01-NMBR-EX8A110-F TO WK01-NMBR-EX8A1-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-CRDT-IND-EX851-Z = 'Y'
               MOVE WK01-CRDT-IND-EX851 TO DEBIT-CRDT-IND-EX85 OF
                FEXWM85(0001)
           END-IF.
           MOVE WK01-CRDT-IND-EX851-F TO WK01-CRDT-IND-EX85-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CRDT-IND-EX852-Z = 'Y'
               MOVE WK01-CRDT-IND-EX852 TO DEBIT-CRDT-IND-EX85 OF
                FEXWM85(0002)
           END-IF.
           MOVE WK01-CRDT-IND-EX852-F TO WK01-CRDT-IND-EX85-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CRDT-IND-EX853-Z = 'Y'
               MOVE WK01-CRDT-IND-EX853 TO DEBIT-CRDT-IND-EX85 OF
                FEXWM85(0003)
           END-IF.
           MOVE WK01-CRDT-IND-EX853-F TO WK01-CRDT-IND-EX85-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CRDT-IND-EX854-Z = 'Y'
               MOVE WK01-CRDT-IND-EX854 TO DEBIT-CRDT-IND-EX85 OF
                FEXWM85(0004)
           END-IF.
           MOVE WK01-CRDT-IND-EX854-F TO WK01-CRDT-IND-EX85-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CRDT-IND-EX855-Z = 'Y'
               MOVE WK01-CRDT-IND-EX855 TO DEBIT-CRDT-IND-EX85 OF
                FEXWM85(0005)
           END-IF.
           MOVE WK01-CRDT-IND-EX855-F TO WK01-CRDT-IND-EX85-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CRDT-IND-EX856-Z = 'Y'
               MOVE WK01-CRDT-IND-EX856 TO DEBIT-CRDT-IND-EX85 OF
                FEXWM85(0006)
           END-IF.
           MOVE WK01-CRDT-IND-EX856-F TO WK01-CRDT-IND-EX85-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CRDT-IND-EX857-Z = 'Y'
               MOVE WK01-CRDT-IND-EX857 TO DEBIT-CRDT-IND-EX85 OF
                FEXWM85(0007)
           END-IF.
           MOVE WK01-CRDT-IND-EX857-F TO WK01-CRDT-IND-EX85-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CRDT-IND-EX858-Z = 'Y'
               MOVE WK01-CRDT-IND-EX858 TO DEBIT-CRDT-IND-EX85 OF
                FEXWM85(0008)
           END-IF.
           MOVE WK01-CRDT-IND-EX858-F TO WK01-CRDT-IND-EX85-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CRDT-IND-EX859-Z = 'Y'
               MOVE WK01-CRDT-IND-EX859 TO DEBIT-CRDT-IND-EX85 OF
                FEXWM85(0009)
           END-IF.
           MOVE WK01-CRDT-IND-EX859-F TO WK01-CRDT-IND-EX85-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CRDT-IND-EX8510-Z = 'Y'
               MOVE WK01-CRDT-IND-EX8510 TO DEBIT-CRDT-IND-EX85 OF
                FEXWM85(0010)
           END-IF.
           MOVE WK01-CRDT-IND-EX8510-F TO WK01-CRDT-IND-EX85-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-DEBIT-CRDT-IND-EX851-Z = 'Y'
               MOVE WK01-DEBIT-CRDT-IND-EX851 TO
                TOT-DEBIT-CRDT-IND-EX85 OF FEXWM85(0001)
           END-IF.
           MOVE WK01-DEBIT-CRDT-IND-EX851-F TO
                WK01-DEBIT-CRDT-IND-EX85-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DEBIT-CRDT-IND-EX852-Z = 'Y'
               MOVE WK01-DEBIT-CRDT-IND-EX852 TO
                TOT-DEBIT-CRDT-IND-EX85 OF FEXWM85(0002)
           END-IF.
           MOVE WK01-DEBIT-CRDT-IND-EX852-F TO
                WK01-DEBIT-CRDT-IND-EX85-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DEBIT-CRDT-IND-EX853-Z = 'Y'
               MOVE WK01-DEBIT-CRDT-IND-EX853 TO
                TOT-DEBIT-CRDT-IND-EX85 OF FEXWM85(0003)
           END-IF.
           MOVE WK01-DEBIT-CRDT-IND-EX853-F TO
                WK01-DEBIT-CRDT-IND-EX85-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DEBIT-CRDT-IND-EX854-Z = 'Y'
               MOVE WK01-DEBIT-CRDT-IND-EX854 TO
                TOT-DEBIT-CRDT-IND-EX85 OF FEXWM85(0004)
           END-IF.
           MOVE WK01-DEBIT-CRDT-IND-EX854-F TO
                WK01-DEBIT-CRDT-IND-EX85-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DEBIT-CRDT-IND-EX855-Z = 'Y'
               MOVE WK01-DEBIT-CRDT-IND-EX855 TO
                TOT-DEBIT-CRDT-IND-EX85 OF FEXWM85(0005)
           END-IF.
           MOVE WK01-DEBIT-CRDT-IND-EX855-F TO
                WK01-DEBIT-CRDT-IND-EX85-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DEBIT-CRDT-IND-EX856-Z = 'Y'
               MOVE WK01-DEBIT-CRDT-IND-EX856 TO
                TOT-DEBIT-CRDT-IND-EX85 OF FEXWM85(0006)
           END-IF.
           MOVE WK01-DEBIT-CRDT-IND-EX856-F TO
                WK01-DEBIT-CRDT-IND-EX85-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DEBIT-CRDT-IND-EX857-Z = 'Y'
               MOVE WK01-DEBIT-CRDT-IND-EX857 TO
                TOT-DEBIT-CRDT-IND-EX85 OF FEXWM85(0007)
           END-IF.
           MOVE WK01-DEBIT-CRDT-IND-EX857-F TO
                WK01-DEBIT-CRDT-IND-EX85-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DEBIT-CRDT-IND-EX858-Z = 'Y'
               MOVE WK01-DEBIT-CRDT-IND-EX858 TO
                TOT-DEBIT-CRDT-IND-EX85 OF FEXWM85(0008)
           END-IF.
           MOVE WK01-DEBIT-CRDT-IND-EX858-F TO
                WK01-DEBIT-CRDT-IND-EX85-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DEBIT-CRDT-IND-EX859-Z = 'Y'
               MOVE WK01-DEBIT-CRDT-IND-EX859 TO
                TOT-DEBIT-CRDT-IND-EX85 OF FEXWM85(0009)
           END-IF.
           MOVE WK01-DEBIT-CRDT-IND-EX859-F TO
                WK01-DEBIT-CRDT-IND-EX85-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DEBIT-CRDT-IND-EX8510-Z = 'Y'
               MOVE WK01-DEBIT-CRDT-IND-EX8510 TO
                TOT-DEBIT-CRDT-IND-EX85 OF FEXWM85(0010)
           END-IF.
           MOVE WK01-DEBIT-CRDT-IND-EX8510-F TO
                WK01-DEBIT-CRDT-IND-EX85-F (10).
