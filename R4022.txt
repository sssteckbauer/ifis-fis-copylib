       01  R4022-CA-EFCTV.
           02  DB-PROC-ID-4022.
               03  USER-CODE-4022                       PIC X(8).
               03  LAST-ACTVY-DATE-4022                 PIC X(5).
               03  TRMNL-ID-4022                        PIC X(8).
               03  PURGE-FLAG-4022                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  EFCTV-RCRD-KEY-4022.
               03  START-DATE-4022            COMP-3    PIC S9(8).
               03  TIME-STAMP-4022                      PIC X(6).
           02  END-DATE-4022                  COMP-3    PIC S9(8).
           02  STATUS-4022                              PIC X(1).
           02  CNTRL-ACCT-CODE-4022                     PIC X(6).
           02  OFST-ACCT-CODE-4022                      PIC X(6).
           02  PRIOR-YR-CNTRL-ACCT-4022                 PIC X(6).
           02  PRIOR-YR-OFST-ACCT-4022                  PIC X(6).
