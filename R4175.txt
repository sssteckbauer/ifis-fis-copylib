       01  R4175-TOF-DTL.
           02  DB-PROC-ID-4175.
               03  USER-CODE-4175                       PIC X(8).
               03  LAST-ACTVY-DATE-4175                 PIC X(5).
               03  TRMNL-ID-4175                        PIC X(8).
               03  PURGE-FLAG-4175                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  SEQ-NMBR-4175                            PIC 9(4).
           02  ACTVY-DATE-4175                COMP-3    PIC S9(8).
           02  TIME-STAMP-4175                          PIC X(6).
           02  JRNL-TYPE-4175                           PIC X(4).
           02  COA-CODE-4175                            PIC X(1).
           02  FUND-CODE-4175                           PIC X(6).
           02  ORGZN-CODE-4175                          PIC X(6).
           02  ACCT-CODE-4175                           PIC X(6).
           02  PRGRM-CODE-4175                          PIC X(6).
           02  ACTVY-CODE-4175                          PIC X(6).
           02  LCTN-CODE-4175                           PIC X(6).
           02  ACCT-INDX-CODE-4175                      PIC X(10).
           02  FSCL-YR-4175                             PIC X(2).
           02  PSTNG-PRD-4175                           PIC X(2).
           02  BDGT-PRD-4175                            PIC X(2).
           02  TRANS-DESC-4175                          PIC X(35).
           02  CURR-AMT-4175                  COMP-3    PIC S9(10)V99.
           02  DEBIT-CRDT-IND-4175                      PIC X(1).
           02  SAU-CODE-4175                            PIC X(1).
           02  SC-CODE-4175                             PIC X(1).
           02  CL-CODE-4175                             PIC X(1).
           02  TYPE-CODE-4175                           PIC X(2).
           02  FTE-AMT-4175                   COMP-3    PIC S9(10)V99.
           02  FTE-DEBIT-CRDT-IND-4175                  PIC X(1).
           02  PERM-AMT-4175                  COMP-3    PIC S9(10)V99.
           02  PERM-DEBIT-CRDT-IND-4175                 PIC X(1).
           02  PERM-TRANS-DESC-4175                     PIC X(30).
