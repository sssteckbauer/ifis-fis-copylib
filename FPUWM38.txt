      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWM38                              04/24/00  12:40  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWM38.
           02  ACTN-CODE-GROUP-PU38.
               03  ACTN-CODE-PU38           OCCURS 12   PIC X(1).
           02  PAY-MTHD-CODE-4078-PU38      OCCURS 12   PIC X(2).
           02  PAY-MTHD-DESC-4078-PU38      OCCURS 12   PIC X(35).
           02  START-DATE-4078-PU38         OCCURS 12   PIC X(6).
           02  END-DATE-4078-PU38           OCCURS 12   PIC X(6).
           02  ACTVY-DATE-4078-PU38         OCCURS 12   PIC X(6).
