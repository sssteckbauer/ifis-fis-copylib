      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FGAWD07                              04/24/00  13:07  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FGAWD07.
           02  SYSTEM-DATE-GA07               COMP-3    PIC S9(8).
           02  SAVE-DBKEY-ID-4189-GA07
                                     COMP   OCCURS 12   PIC S9(8).
           02  STOP-READ-IND-GA07                       PIC X(1).
           02  FIRST-ONLY-IND-GA07                      PIC X(1).
           02  PREV-ACCT-CODE-GA07                      PIC X(6).
           02  SAVE-DBKEY-ID-4197-GA07        COMP      PIC S9(8).
           02  NEW-4197-IND-GA07                        PIC X(1).
           02  NMRC-ACTG-PRD-GA07                       PIC 9(2).
