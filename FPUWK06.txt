      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWK06                              04/24/00  12:39  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWK06.
           02  COA-CODE-4000-PU06                       PIC X(1).
           02  RQST-RQST-CODE-PU06                      PIC X(8).
           02  BUYER-CODE-DEFAULT-4072-PU06             PIC X(4).
           02  SAVE-DBKEY-ID-MINUS-ONE-PU06             PIC X(012).
           02  SAVE-DBKEY-ID-PLUS-ONE-PU06              PIC X(012).
