      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM79                              04/24/00  12:19  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM79.
           02  ACTN-CODE-GROUP-CH79.
               03  ACTN-CODE-CH79           OCCURS 5    PIC X(1).
           02  COA-CODE-4052-CH79           OCCURS 5    PIC X(1).
           02  XTRNL-ENTY-CODE-4052-CH79    OCCURS 5    PIC X(4).
           02  INTRL-ENTY-CODE-4054-CH79    OCCURS 5    PIC X(4).
           02  XTRNL-CODE-4054-CH79         OCCURS 5    PIC X(10).
           02  INTRL-CODE-4055-CH79         OCCURS 5    PIC X(10).
           02  INTRL-CODE-DESC-4055-CH79    OCCURS 5    PIC X(35).
