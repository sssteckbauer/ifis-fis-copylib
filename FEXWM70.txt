      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FEXWM70                              04/24/00  12:30  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FEXWM70.
           02  ACTN-CODE-GROUP-EX70.
               03  ACTN-CODE-EX70           OCCURS 6    PIC X(1).
           02  RELEASE-NMBR-EX70            OCCURS 6    PIC X(14).
           02  DCMNT-NMBR-EX70              OCCURS 6    PIC X(8).
           02  RLSE-AMT-EX70         COMP-3 OCCURS 6    PIC S9(10)V99.
           02  DEBIT-CRDT-IND-EX70          OCCURS 6    PIC X(1).
           02  CHECK-AMT-DEBIT-CRDT-IND-EX70            PIC X(1).
