       01  R6605.
           02  DB-PROC-ID-6605.
               03  USER-CODE-6605                       PIC X(8).
               03  LAST-ACTVY-DATE-6605                 PIC X(5).
               03  TRMNL-ID-6605                        PIC X(8).
               03  PURGE-FLAG-6605                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  BATCH-HDR-KEY-6605.
               03  UNVRS-CODE-6605                      PIC X(2).
               03  BATCH-USER-CODE-6605                 PIC X(8).
               03  COMM-REF-NMBR-6605         COMP-3    PIC S9(6).
               03  TEXT-CODE-6605                       PIC X(4).
           02  COMM-DATE-6605                 COMP-3    PIC S9(8).
           02  PRNTD-FLAG-6605                          PIC X(1).
