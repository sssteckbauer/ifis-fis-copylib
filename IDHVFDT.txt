       976-FROM-R4008-FNDT SECTION.

FCCCE**        MOVE USER-CODE-4008 OF R4008-FNDT
FCCCE          MOVE DBLINK-USER-CD
               TO FDT-USER-CD.

               MOVE LAST-ACTVY-DATE-4008 OF R4008-FNDT
               TO FDT-LAST-ACTVY-DT.

               MOVE UNVRS-CODE-4008 OF R4008-FNDT
               TO FDT-UNVRS-CD.

               MOVE COA-CODE-4008 OF R4008-FNDT
               TO FDT-COA-CD.

               MOVE FUND-TYPE-CODE-4008 OF R4008-FNDT
               TO FDT-FUND-TYP-CD.

           IF ACTVY-DATE-4008 OF R4008-FNDT  NOT NUMERIC
               MOVE DBLINKX-NULL-DB2-DATE-ISO TO FDT-ACTVY-DT
           ELSE
               MOVE ACTVY-DATE-4008 OF R4008-FNDT
               TO DBLINKX-INPUT-08
               MOVE '1' TO DBLINKX-REQUEST-CODE
               MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
               MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
               CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
               IF DBLINKX-OK
                   MOVE DBLINKX-OUTPUT-PARM
                   TO FDT-ACTVY-DT
               ELSE
                   MOVE DBLINKX-NULL-DB2-DATE-ISO
                   TO FDT-ACTVY-DT
               END-IF
           END-IF.

               MOVE GOVNMT-OWNED-IND-4008 OF R4008-FNDT
               TO FDT-GOV-OWND-IND.
       976-FROM-R4008-FNDT-EXIT.
           EXIT.
