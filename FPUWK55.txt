      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWK55                              04/24/00  12:37  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWK55.
           02  ITEM-NMBR-PU55                 COMP-3    PIC 9(4).
           02  CMDTY-CODE-PU55                          PIC X(8).
           02  QTY-PU55                       COMP-3    PIC S9(6)V99.
           02  UNIT-PRICE-PU55                COMP-3    PIC 9(10)V9999.
           02  SEQ-COUNT-PU55                 COMP-3    PIC 9(4).
           02  SEQ-NMBR-PU55                            PIC 9(4).
           02  SAVE-DBKEY-ID-SEQ-PU55         COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-ITEM-PU55        COMP      PIC S9(8).
