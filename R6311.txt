       01  R6311-ENTY.
           02  DB-PROC-ID-6311.
               03  USER-CODE-6311                       PIC X(8).
               03  LAST-ACTVY-DATE-6311                 PIC X(5).
               03  TRMNL-ID-6311                        PIC X(8).
               03  PURGE-FLAG-6311                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  ENTY-ID-KEY-6311.
               03  UNVRS-CODE-6311                      PIC X(2).
               03  ENTY-ID-6311.
                   04  ENTY-ID-DIGIT-ONE-6311           PIC X(1).
                   04  ENTY-ID-LAST-NINE-6311           PIC X(9).
           02  NAME-KEY-6311                            PIC X(35).
           02  ENTY-FULL-NAME-6311                      PIC X(55).
           02  INTRL-REF-ID-6311              COMP-3    PIC S9(7).
           02  PAN-NMBR-6311                            PIC X(4).
           02  PAN-FLAG-6311                            PIC X(1).
           02  PAN-DATE-6311                  COMP-3    PIC S9(8).
           02  FEIN-ID-6311                             PIC X(9).
           02  FILLER02                                 PIC X(10).
