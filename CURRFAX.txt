      *      CREATED BY CONVERT/DB V8R01 ON 05/01/00 AT 11:32         *
YYY991     MOVE '985-CURR-R4370-FAX' TO DBLINK-CURR-PARAGRAPH.          276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%INITIALIZE DBLINK SUBSCHEMA-CTRL                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE ZERO TO ERROR-STATUS.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-AREA.                                   276 BRTN
YYY991     MOVE SPACES TO ERROR-SET.                                    276 BRTN
YYY991     MOVE SPACES TO ERROR-RECORD.                                 276 BRTN
YYY991     MOVE 'R4370-FAX' TO RECORD-NAME.                             276 BRTN
YYY991     MOVE 'F-FAX' TO AREA-NAME.                                   276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%CLOSE ALL RECORD-RELATED CURSORS                                276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4370-FAX TO DBLINK-RECORD-MADE-CURRENT.         276 BRTN
YYY991     MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.                         276 BRTN
YYY991     PERFORM 991-CLOSE-CURSORS                                    276 BRTN
YYY991        THRU 991-CLOSE-CURSORS-EXIT.                              276 BRTN
YYY991     IF ERROR-STATUS = DBLINK-DB2-ERROR                           276 BRTN
YYY991         GO TO 985-CURR-R4370-FAX-EXIT                            276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991     MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET RECORD KEY                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     IF DBLINK-TYPE-GET NOT = DBLINK-CURR                         276 BRTN
YYY991         MOVE FAX-FAX-DOC-TYP TO DBLINK-R4370-FAX-01              276 BRTN
YYY991         MOVE FAX-FX-DOC-NBR TO DBLINK-R4370-FAX-02               276 BRTN
YYY991         MOVE FAX-CHNG-SEQ-NBR TO DBLINK-R4370-FAX-03             276 BRTN
YYY991         MOVE FAX-FAX-NBR TO DBLINK-R4370-FAX-04                  276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET CURRENT OF RUN-UNIT                                         276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-R4370-FAX TO DBLINK-VIEW-NAME-CURRENT.           276 BRTN
YYY991     MOVE DBLINK-R4370-FAX-KEY TO DBLINK-VIEW-200-CURRENT.        276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS WHICH RECORD OWNS                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER                     276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-FAX-FLAG                                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE DBLINK-NOT-MEMBER TO DBLINK-S-INDX-FAX-FLAG-F           276 BRTN
YYY991     IF FAX-IDX-FAX-FLAG-SETF = DBLINK-MEMBER                     276 BRTN
YYY991         MOVE FAX-IDX-FAX-FLAG-SETF TO DBLINK-S-INDX-FAX-FLAG-F   276 BRTN
YYY991         MOVE FAX-FAX-DOC-TYP TO DBLINK-S-INDX-FAX-FLAG-01        276 BRTN
YYY991         MOVE FAX-FAX-STATUS-CD TO DBLINK-S-INDX-FAX-FLAG-02      276 BRTN
YYY991         MOVE FAX-FAX-RQST-FLAG TO DBLINK-S-INDX-FAX-FLAG-03      276 BRTN
YYY991         MOVE FAX-IDX-FAX-FLAG-TS TO DBLINK-S-INDX-FAX-FLAG-04    276 BRTN
YYY991     END-IF.                                                      276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-FAX-KEY                                              276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE FAX-UNVRS-CD TO DBLINK-S-INDX-FAX-KEY-01.               276 BRTN
YYY991     MOVE FAX-FAX-DOC-TYP TO DBLINK-S-INDX-FAX-KEY-02.            276 BRTN
YYY991     MOVE FAX-FX-DOC-NBR TO DBLINK-S-INDX-FAX-KEY-03.             276 BRTN
YYY991     MOVE FAX-CHNG-SEQ-NBR TO DBLINK-S-INDX-FAX-KEY-04.           276 BRTN
YYY991     MOVE FAX-FAX-NBR TO DBLINK-S-INDX-FAX-KEY-05.                276 BRTN
YYY991                                                                  276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991*%SET S-INDX-FAX-NMBR                                             276 BRTN
YYY991*%****************************** *                                276 BRTN
YYY991     MOVE FAX-FAX-NBR TO DBLINK-S-INDX-FAX-NMBR-01.               276 BRTN
