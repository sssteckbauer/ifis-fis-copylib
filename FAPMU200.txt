    ***Created by Convert/DC version V8R03 on 11/20/00 at 13:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-VNDR-ID-LAST-NN-AP20-Z = 'Y'
               MOVE WK01-VNDR-ID-LAST-NN-AP20 TO
                WK-VNDR-ID-LAST-NINE-AP20 OF FAPWK20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ADR-TYPE-COD-KY-AP20-Z = 'Y'
               MOVE WK01-ADR-TYPE-COD-KY-AP20 TO
                WK-ADR-TYPE-CODE-KEY-AP20 OF FAPWK20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-KY-DSP-AP20-Z = 'Y'
               MOVE WK01-END-DATE-KY-DSP-AP20 TO
                WK-END-DATE-KEY-DISP-AP20 OF FAPWK20
           END-IF.
      *%--------------------------------------------------------------%*
001MLJ     IF  WK01-PYMT-METHOD-TYP-Z          = 'Y'
               MOVE WK01-PYMT-METHOD-TYP  TO
                PYMT-METHOD-TYP-AP20 OF FAPWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATA-FLAG-AP20-Z = 'Y'
               MOVE WK01-DATA-FLAG-AP20 TO ACH-DATA-FLAG-AP20 OF FAPWK20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ADR-6185-R-6139-AP201-Z = 'Y'
               MOVE WK01-ADR-6185-R-6139-AP201 TO
                LINE-ADR-6185-R-6139-AP20 OF FAPWM20(0001)
           END-IF.
           MOVE WK01-ADR-6185-R-6139-AP201-F TO
                WK01-ADR-6185-R-6139-AP20-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-6185-R-6139-AP202-Z = 'Y'
               MOVE WK01-ADR-6185-R-6139-AP202 TO
                LINE-ADR-6185-R-6139-AP20 OF FAPWM20(0002)
           END-IF.
           MOVE WK01-ADR-6185-R-6139-AP202-F TO
                WK01-ADR-6185-R-6139-AP20-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-6185-R-6139-AP203-Z = 'Y'
               MOVE WK01-ADR-6185-R-6139-AP203 TO
                LINE-ADR-6185-R-6139-AP20 OF FAPWM20(0003)
           END-IF.
           MOVE WK01-ADR-6185-R-6139-AP203-F TO
                WK01-ADR-6185-R-6139-AP20-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-ADR-6185-R-6139-AP204-Z = 'Y'
               MOVE WK01-ADR-6185-R-6139-AP204 TO
                LINE-ADR-6185-R-6139-AP20 OF FAPWM20(0004)
           END-IF.
           MOVE WK01-ADR-6185-R-6139-AP204-F TO
                WK01-ADR-6185-R-6139-AP20-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-NAM-6185-R-6139-AP20-Z = 'Y'
               MOVE WK01-NAM-6185-R-6139-AP20 TO
                CITY-NAME-6185-R-6139-AP20 OF FAPWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6151-AP20-Z = 'Y'
               MOVE WK01-CODE-6151-AP20 TO STATE-CODE-6151-AP20 OF
                FAPWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6152-AP20-Z = 'Y'
               MOVE WK01-CODE-6152-AP20 TO ZIP-CODE-6152-AP20 OF FAPWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6153-AP20-Z = 'Y'
               MOVE WK01-CODE-6153-AP20 TO CNTRY-CODE-6153-AP20 OF
                FAPWM20
           END-IF.
      *%--------------------------------------------------------------%*
SN6132     IF WK01-POSTAL-CODE-AP20-Z = 'Y'
SN6132         MOVE WK01-POSTAL-CODE-AP20
SN6132           TO POSTAL-CODE-AP20 OF FAPWM20
SN6132     END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FRGN-PROV-AP20-Z = 'Y'
               MOVE WK01-FRGN-PROV-AP20 TO FOREIGN-PROV-CD-AP20 OF
                FAPWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FRGN-PROV-NAME-Z = 'Y'
               MOVE WK01-FRGN-PROV-NAME-AP20
                                        TO FOREIGN-PROV-NAME-AP20 OF
                FAPWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AREA-CODE-AP20-Z = 'Y'
               MOVE WK01-AREA-CODE-AP20 TO TLPHN-AREA-CODE-AP20 OF
                FAPWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AREA-CODE-AP2A-Z = 'Y'
               MOVE WK01-AREA-CODE-AP2A TO FAX-AREA-CODE-AP20 OF FAPWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-LIN-6185-R-6139-AP20-Z = 'Y'
               MOVE WK01-LIN-6185-R-6139-AP20 TO
                EMADR-LINE-6185-R-6139-AP20 OF FAPWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-COMMENT-LINE-AP20-Z = 'Y'
               MOVE WK01-COMMENT-LINE-AP20 TO
                COMMENT-LINE-AP20 OF FAPWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-START-DATE-AP20-Z = 'Y'
               MOVE WK01-START-DATE-AP20 TO MAP-START-DATE-AP20 OF
                FAPWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-END-DATE-AP20-Z = 'Y'
               MOVE WK01-END-DATE-AP20 TO MAP-END-DATE-AP20 OF FAPWM20
           END-IF.
      *%--------------------------------------------------------------%*
DEVMJM* ACR46019
           IF WK01-UPDT-USER-ID-AP20-Z = 'Y'
               MOVE WK01-UPDT-USER-ID-AP20  TO UPDT-USER-ID-AP20
           END-IF.
      *%--------------------------------------------------------------%*
DEVMJM* ACR46019
           IF WK01-KEY-6311-R-6117-AP20-Z = 'Y'
               MOVE WK01-KEY-6311-R-6117-AP20 TO
                NAME-KEY-6311-R-6117-AP20 OF FAPWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6153-AP20-Z = 'Y'
               MOVE WK01-NAME-6153-AP20 TO FULL-NAME-6153-AP20 OF
                FAPWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-XCHNG-ID-AP20-Z = 'Y'
               MOVE WK01-XCHNG-ID-AP20 TO TLPHN-XCHNG-ID-AP20 OF FAPWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-XCHNG-ID-AP2A-Z = 'Y'
               MOVE WK01-XCHNG-ID-AP2A TO FAX-XCHNG-ID-AP20 OF FAPWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-ID-AP20-Z = 'Y'
               MOVE WK01-SEQ-ID-AP20 TO TLPHN-SEQ-ID-AP20 OF FAPWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-ID-AP2A-Z = 'Y'
               MOVE WK01-SEQ-ID-AP2A TO FAX-SEQ-ID-AP20 OF FAPWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-XTNSN-ID-AP20-Z = 'Y'
               MOVE WK01-XTNSN-ID-AP20 TO TLPHN-XTNSN-ID-AP20 OF FAPWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-DESC-AP20-Z = 'Y'
               MOVE WK01-TYPE-DESC-AP20 TO ADR-TYPE-DESC-AP20 OF FAPWM20
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-AP20-Z = 'Y'
               MOVE WK01-CODE-AP20 TO ACTN-CODE-AP20 OF FAPWM20
           END-IF.
