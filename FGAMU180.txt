    ***Created by Convert/DC version V8R03 on 11/01/00 at 10:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-GA18-Z = 'Y'
               MOVE WK01-DESC-GA18 TO SOURCE-DESC-GA18 OF FGAWK18
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HDR-CODE-GA18-Z = 'Y'
               MOVE WK01-HDR-CODE-GA18 TO ACTN-HDR-CODE-GA18 OF FGAWK18
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TRTMNT-CODE-GA18-Z = 'Y'
               MOVE WK01-TRTMNT-CODE-GA18 TO BDGT-TRTMNT-CODE-GA18 OF
                FGAWK18
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-GA1A-Z = 'Y'
               MOVE WK01-DESC-GA1A TO BDGT-DESC-GA18 OF FGAWK18
           END-IF.
      *%--------------------------------------------------------------%*
DEVMJM* ACR46159
           IF WK01-DEFD-GA1A-Z = 'Y'
               MOVE WK01-DEFD-GA1A TO BDGT-DEFAULT-MSG-GA18 OF FGAWK18
           END-IF.
      *%--------------------------------------------------------------%*
DEVMJM* ACR46159
           IF WK01-CODE-GA181-Z = 'Y'
               MOVE WK01-CODE-GA181 TO ACTN-CODE-GA18 OF FGAWM18(0001)
           END-IF.
           MOVE WK01-CODE-GA181-F TO WK01-CODE-GA18-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-GA182-Z = 'Y'
               MOVE WK01-CODE-GA182 TO ACTN-CODE-GA18 OF FGAWM18(0002)
           END-IF.
           MOVE WK01-CODE-GA182-F TO WK01-CODE-GA18-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-GA183-Z = 'Y'
               MOVE WK01-CODE-GA183 TO ACTN-CODE-GA18 OF FGAWM18(0003)
           END-IF.
           MOVE WK01-CODE-GA183-F TO WK01-CODE-GA18-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-GA184-Z = 'Y'
               MOVE WK01-CODE-GA184 TO ACTN-CODE-GA18 OF FGAWM18(0004)
           END-IF.
           MOVE WK01-CODE-GA184-F TO WK01-CODE-GA18-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-GA185-Z = 'Y'
               MOVE WK01-CODE-GA185 TO ACTN-CODE-GA18 OF FGAWM18(0005)
           END-IF.
           MOVE WK01-CODE-GA185-F TO WK01-CODE-GA18-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-GA186-Z = 'Y'
               MOVE WK01-CODE-GA186 TO ACTN-CODE-GA18 OF FGAWM18(0006)
           END-IF.
           MOVE WK01-CODE-GA186-F TO WK01-CODE-GA18-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-GA187-Z = 'Y'
               MOVE WK01-CODE-GA187 TO ACTN-CODE-GA18 OF FGAWM18(0007)
           END-IF.
           MOVE WK01-CODE-GA187-F TO WK01-CODE-GA18-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-GA188-Z = 'Y'
               MOVE WK01-CODE-GA188 TO ACTN-CODE-GA18 OF FGAWM18(0008)
           END-IF.
           MOVE WK01-CODE-GA188-F TO WK01-CODE-GA18-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-GA189-Z = 'Y'
               MOVE WK01-CODE-GA189 TO ACTN-CODE-GA18 OF FGAWM18(0009)
           END-IF.
           MOVE WK01-CODE-GA189-F TO WK01-CODE-GA18-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-GA1810-Z = 'Y'
               MOVE WK01-CODE-GA1810 TO ACTN-CODE-GA18 OF FGAWM18(0010)
           END-IF.
           MOVE WK01-CODE-GA1810-F TO WK01-CODE-GA18-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-GA181-Z = 'Y'
               MOVE WK01-TYPE-GA181 TO ENCMBRNC-TYPE-GA18 OF
                FGAWM18(0001)
           END-IF.
           MOVE WK01-TYPE-GA181-F TO WK01-TYPE-GA18-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-GA182-Z = 'Y'
               MOVE WK01-TYPE-GA182 TO ENCMBRNC-TYPE-GA18 OF
                FGAWM18(0002)
           END-IF.
           MOVE WK01-TYPE-GA182-F TO WK01-TYPE-GA18-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-GA183-Z = 'Y'
               MOVE WK01-TYPE-GA183 TO ENCMBRNC-TYPE-GA18 OF
                FGAWM18(0003)
           END-IF.
           MOVE WK01-TYPE-GA183-F TO WK01-TYPE-GA18-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-GA184-Z = 'Y'
               MOVE WK01-TYPE-GA184 TO ENCMBRNC-TYPE-GA18 OF
                FGAWM18(0004)
           END-IF.
           MOVE WK01-TYPE-GA184-F TO WK01-TYPE-GA18-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-GA185-Z = 'Y'
               MOVE WK01-TYPE-GA185 TO ENCMBRNC-TYPE-GA18 OF
                FGAWM18(0005)
           END-IF.
           MOVE WK01-TYPE-GA185-F TO WK01-TYPE-GA18-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-GA186-Z = 'Y'
               MOVE WK01-TYPE-GA186 TO ENCMBRNC-TYPE-GA18 OF
                FGAWM18(0006)
           END-IF.
           MOVE WK01-TYPE-GA186-F TO WK01-TYPE-GA18-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-GA187-Z = 'Y'
               MOVE WK01-TYPE-GA187 TO ENCMBRNC-TYPE-GA18 OF
                FGAWM18(0007)
           END-IF.
           MOVE WK01-TYPE-GA187-F TO WK01-TYPE-GA18-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-GA188-Z = 'Y'
               MOVE WK01-TYPE-GA188 TO ENCMBRNC-TYPE-GA18 OF
                FGAWM18(0008)
           END-IF.
           MOVE WK01-TYPE-GA188-F TO WK01-TYPE-GA18-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-GA189-Z = 'Y'
               MOVE WK01-TYPE-GA189 TO ENCMBRNC-TYPE-GA18 OF
                FGAWM18(0009)
           END-IF.
           MOVE WK01-TYPE-GA189-F TO WK01-TYPE-GA18-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-GA1810-Z = 'Y'
               MOVE WK01-TYPE-GA1810 TO ENCMBRNC-TYPE-GA18 OF
                FGAWM18(0010)
           END-IF.
           MOVE WK01-TYPE-GA1810-F TO WK01-TYPE-GA18-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-GA181-Z = 'Y'
               MOVE WK01-CLASS-CODE-GA181 TO PO-CLASS-CODE-GA18 OF
                FGAWM18(0001)
           END-IF.
           MOVE WK01-CLASS-CODE-GA181-F TO WK01-CLASS-CODE-GA18-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-GA182-Z = 'Y'
               MOVE WK01-CLASS-CODE-GA182 TO PO-CLASS-CODE-GA18 OF
                FGAWM18(0002)
           END-IF.
           MOVE WK01-CLASS-CODE-GA182-F TO WK01-CLASS-CODE-GA18-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-GA183-Z = 'Y'
               MOVE WK01-CLASS-CODE-GA183 TO PO-CLASS-CODE-GA18 OF
                FGAWM18(0003)
           END-IF.
           MOVE WK01-CLASS-CODE-GA183-F TO WK01-CLASS-CODE-GA18-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-GA184-Z = 'Y'
               MOVE WK01-CLASS-CODE-GA184 TO PO-CLASS-CODE-GA18 OF
                FGAWM18(0004)
           END-IF.
           MOVE WK01-CLASS-CODE-GA184-F TO WK01-CLASS-CODE-GA18-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-GA185-Z = 'Y'
               MOVE WK01-CLASS-CODE-GA185 TO PO-CLASS-CODE-GA18 OF
                FGAWM18(0005)
           END-IF.
           MOVE WK01-CLASS-CODE-GA185-F TO WK01-CLASS-CODE-GA18-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-GA186-Z = 'Y'
               MOVE WK01-CLASS-CODE-GA186 TO PO-CLASS-CODE-GA18 OF
                FGAWM18(0006)
           END-IF.
           MOVE WK01-CLASS-CODE-GA186-F TO WK01-CLASS-CODE-GA18-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-GA187-Z = 'Y'
               MOVE WK01-CLASS-CODE-GA187 TO PO-CLASS-CODE-GA18 OF
                FGAWM18(0007)
           END-IF.
           MOVE WK01-CLASS-CODE-GA187-F TO WK01-CLASS-CODE-GA18-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-GA188-Z = 'Y'
               MOVE WK01-CLASS-CODE-GA188 TO PO-CLASS-CODE-GA18 OF
                FGAWM18(0008)
           END-IF.
           MOVE WK01-CLASS-CODE-GA188-F TO WK01-CLASS-CODE-GA18-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-GA189-Z = 'Y'
               MOVE WK01-CLASS-CODE-GA189 TO PO-CLASS-CODE-GA18 OF
                FGAWM18(0009)
           END-IF.
           MOVE WK01-CLASS-CODE-GA189-F TO WK01-CLASS-CODE-GA18-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CLASS-CODE-GA1810-Z = 'Y'
               MOVE WK01-CLASS-CODE-GA1810 TO PO-CLASS-CODE-GA18 OF
                FGAWM18(0010)
           END-IF.
           MOVE WK01-CLASS-CODE-GA1810-F TO WK01-CLASS-CODE-GA18-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-TRTMNT-CODE-GA1A11-Z = 'Y'
               MOVE WK01-TRTMNT-CODE-GA1A11 TO
                ENCMBRNC-TRTMNT-CODE-GA18 OF FGAWM18(0001)
           END-IF.
           MOVE WK01-TRTMNT-CODE-GA1A11-F TO WK01-TRTMNT-CODE-GA1A1-F
                (1).
      *%--------------------------------------------------------------%*
           IF WK01-TRTMNT-CODE-GA1A12-Z = 'Y'
               MOVE WK01-TRTMNT-CODE-GA1A12 TO
                ENCMBRNC-TRTMNT-CODE-GA18 OF FGAWM18(0002)
           END-IF.
           MOVE WK01-TRTMNT-CODE-GA1A12-F TO WK01-TRTMNT-CODE-GA1A1-F
                (2).
      *%--------------------------------------------------------------%*
           IF WK01-TRTMNT-CODE-GA1A13-Z = 'Y'
               MOVE WK01-TRTMNT-CODE-GA1A13 TO
                ENCMBRNC-TRTMNT-CODE-GA18 OF FGAWM18(0003)
           END-IF.
           MOVE WK01-TRTMNT-CODE-GA1A13-F TO WK01-TRTMNT-CODE-GA1A1-F
                (3).
      *%--------------------------------------------------------------%*
           IF WK01-TRTMNT-CODE-GA1A14-Z = 'Y'
               MOVE WK01-TRTMNT-CODE-GA1A14 TO
                ENCMBRNC-TRTMNT-CODE-GA18 OF FGAWM18(0004)
           END-IF.
           MOVE WK01-TRTMNT-CODE-GA1A14-F TO WK01-TRTMNT-CODE-GA1A1-F
                (4).
      *%--------------------------------------------------------------%*
           IF WK01-TRTMNT-CODE-GA1A15-Z = 'Y'
               MOVE WK01-TRTMNT-CODE-GA1A15 TO
                ENCMBRNC-TRTMNT-CODE-GA18 OF FGAWM18(0005)
           END-IF.
           MOVE WK01-TRTMNT-CODE-GA1A15-F TO WK01-TRTMNT-CODE-GA1A1-F
                (5).
      *%--------------------------------------------------------------%*
           IF WK01-TRTMNT-CODE-GA1A16-Z = 'Y'
               MOVE WK01-TRTMNT-CODE-GA1A16 TO
                ENCMBRNC-TRTMNT-CODE-GA18 OF FGAWM18(0006)
           END-IF.
           MOVE WK01-TRTMNT-CODE-GA1A16-F TO WK01-TRTMNT-CODE-GA1A1-F
                (6).
      *%--------------------------------------------------------------%*
           IF WK01-TRTMNT-CODE-GA1A17-Z = 'Y'
               MOVE WK01-TRTMNT-CODE-GA1A17 TO
                ENCMBRNC-TRTMNT-CODE-GA18 OF FGAWM18(0007)
           END-IF.
           MOVE WK01-TRTMNT-CODE-GA1A17-F TO WK01-TRTMNT-CODE-GA1A1-F
                (7).
      *%--------------------------------------------------------------%*
           IF WK01-TRTMNT-CODE-GA1A18-Z = 'Y'
               MOVE WK01-TRTMNT-CODE-GA1A18 TO
                ENCMBRNC-TRTMNT-CODE-GA18 OF FGAWM18(0008)
           END-IF.
           MOVE WK01-TRTMNT-CODE-GA1A18-F TO WK01-TRTMNT-CODE-GA1A1-F
                (8).
      *%--------------------------------------------------------------%*
           IF WK01-TRTMNT-CODE-GA1A19-Z = 'Y'
               MOVE WK01-TRTMNT-CODE-GA1A19 TO
                ENCMBRNC-TRTMNT-CODE-GA18 OF FGAWM18(0009)
           END-IF.
           MOVE WK01-TRTMNT-CODE-GA1A19-F TO WK01-TRTMNT-CODE-GA1A1-F
                (9).
      *%--------------------------------------------------------------%*
           IF WK01-TRTMNT-CODE-GA1A110-Z = 'Y'
               MOVE WK01-TRTMNT-CODE-GA1A110 TO
                ENCMBRNC-TRTMNT-CODE-GA18 OF FGAWM18(0010)
           END-IF.
           MOVE WK01-TRTMNT-CODE-GA1A110-F TO WK01-TRTMNT-CODE-GA1A1-F
                (10).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-GA1A11-Z = 'Y'
               MOVE WK01-DESC-GA1A11 TO ENC-DESC-GA18 OF FGAWM18(0001)
           END-IF.
           MOVE WK01-DESC-GA1A11-F TO WK01-DESC-GA1A1-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-GA1A12-Z = 'Y'
               MOVE WK01-DESC-GA1A12 TO ENC-DESC-GA18 OF FGAWM18(0002)
           END-IF.
           MOVE WK01-DESC-GA1A12-F TO WK01-DESC-GA1A1-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-GA1A13-Z = 'Y'
               MOVE WK01-DESC-GA1A13 TO ENC-DESC-GA18 OF FGAWM18(0003)
           END-IF.
           MOVE WK01-DESC-GA1A13-F TO WK01-DESC-GA1A1-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-GA1A14-Z = 'Y'
               MOVE WK01-DESC-GA1A14 TO ENC-DESC-GA18 OF FGAWM18(0004)
           END-IF.
           MOVE WK01-DESC-GA1A14-F TO WK01-DESC-GA1A1-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-GA1A15-Z = 'Y'
               MOVE WK01-DESC-GA1A15 TO ENC-DESC-GA18 OF FGAWM18(0005)
           END-IF.
           MOVE WK01-DESC-GA1A15-F TO WK01-DESC-GA1A1-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-GA1A16-Z = 'Y'
               MOVE WK01-DESC-GA1A16 TO ENC-DESC-GA18 OF FGAWM18(0006)
           END-IF.
           MOVE WK01-DESC-GA1A16-F TO WK01-DESC-GA1A1-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-GA1A17-Z = 'Y'
               MOVE WK01-DESC-GA1A17 TO ENC-DESC-GA18 OF FGAWM18(0007)
           END-IF.
           MOVE WK01-DESC-GA1A17-F TO WK01-DESC-GA1A1-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-GA1A18-Z = 'Y'
               MOVE WK01-DESC-GA1A18 TO ENC-DESC-GA18 OF FGAWM18(0008)
           END-IF.
           MOVE WK01-DESC-GA1A18-F TO WK01-DESC-GA1A1-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-GA1A19-Z = 'Y'
               MOVE WK01-DESC-GA1A19 TO ENC-DESC-GA18 OF FGAWM18(0009)
           END-IF.
           MOVE WK01-DESC-GA1A19-F TO WK01-DESC-GA1A1-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-GA1A110-Z = 'Y'
               MOVE WK01-DESC-GA1A110 TO ENC-DESC-GA18 OF FGAWM18(0010)
           END-IF.
           MOVE WK01-DESC-GA1A110-F TO WK01-DESC-GA1A1-F (10).
