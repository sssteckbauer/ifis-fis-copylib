DEVDMW****   MAINTENANCE DESCRIPTION   **************
DEVDMW*
DEVDMW* USE THIS INSTEAD OF SUBCHEMA-CONTROL FOR CH MODULE
DEVDMW*
DEVDMW*==============================================================
           03  DBLINK-RECORD-SET-KEYS.
           04  DBLINK-RECORD-KEYS.
           05  DBLINK-R4000-COA-KEY.
               10  DBLINK-R4000-COA-01      PIC X(2)        VALUE SPACE.
               10  DBLINK-R4000-COA-02      PIC X(1)        VALUE SPACE.
           05  DBLINK-R4001-FUND-KEY.
               10  DBLINK-R4001-FUND-01     PIC X(6)        VALUE SPACE.
           05  DBLINK-R4002-ORGN-KEY.
               10  DBLINK-R4002-ORGN-01     PIC X(6)        VALUE SPACE.
           05  DBLINK-R4003-ACCT-KEY.
               10  DBLINK-R4003-ACCT-01     PIC X(6)        VALUE SPACE.
           05  DBLINK-R4004-PROG-KEY.
               10  DBLINK-R4004-PROG-01     PIC X(6)        VALUE SPACE.
           05  DBLINK-R4005-FUND-EFCTV-KEY.
               10  DBLINK-R4005-FUND-EFCTV-01
                                            PIC X(6)        VALUE SPACE.
               10  DBLINK-R4005-FUND-EFCTV-02
                                            PIC X(10)       VALUE ZERO.
               10  DBLINK-R4005-FUND-EFCTV-03
                                            PIC X(8)        VALUE ZERO.
           05  DBLINK-R4006-LCTN-KEY.
               10  DBLINK-R4006-LCTN-01     PIC X(6)        VALUE SPACE.
           05  DBLINK-R4007-LCTN-EFCTV-KEY.
               10  DBLINK-R4007-LCTN-EFCTV-01
                                            PIC X(6)        VALUE SPACE.
               10  DBLINK-R4007-LCTN-EFCTV-02
                                            PIC X(10)       VALUE ZERO.
               10  DBLINK-R4007-LCTN-EFCTV-03
                                            PIC X(8)        VALUE ZERO.
           05  DBLINK-R4008-FNDT-KEY.
               10  DBLINK-R4008-FNDT-01     PIC X(2)        VALUE SPACE.
           05  DBLINK-R4010-ORGN-EFCTV-KEY.
               10  DBLINK-R4010-ORGN-EFCTV-01
                                            PIC X(6)        VALUE SPACE.
               10  DBLINK-R4010-ORGN-EFCTV-02
                                            PIC X(10)       VALUE ZERO.
               10  DBLINK-R4010-ORGN-EFCTV-03
                                            PIC X(8)        VALUE ZERO.
           05  DBLINK-R4011-SYSDAT-KEY.
               10  DBLINK-R4011-SYSDAT-01   PIC X(8)        VALUE SPACE.
               10  DBLINK-R4011-SYSDAT-02   PIC X(30)       VALUE SPACE.
           05  DBLINK-R4012-SYSDAT-DTL-KEY.
               10  DBLINK-R4012-SYSDAT-DTL-01
                                            PIC X(8)        VALUE SPACE.
               10  DBLINK-R4012-SYSDAT-DTL-02
                                            PIC X(30)       VALUE SPACE.
               10  DBLINK-R4012-SYSDAT-DTL-03
                                            PIC X(8)        VALUE SPACE.
               10  DBLINK-R4012-SYSDAT-DTL-04
                                            PIC X(8)        VALUE SPACE.
               10  DBLINK-R4012-SYSDAT-DTL-05
                                            PIC S9(2)       COMP-3
                       VALUE ZERO.
           05  DBLINK-R4013-USER-DTL-KEY.
               10  DBLINK-R4013-USER-DTL-01 PIC X(8)        VALUE SPACE.
               10  DBLINK-R4013-USER-DTL-02 PIC X(1)        VALUE SPACE.
           05  DBLINK-R4014-ACTT-KEY.
               10  DBLINK-R4014-ACTT-01     PIC X(2)        VALUE SPACE.
           05  DBLINK-R4015-ACTT-EFCTV-KEY.
               10  DBLINK-R4015-ACTT-EFCTV-01
                                            PIC X(2)        VALUE SPACE.
               10  DBLINK-R4015-ACTT-EFCTV-02
                                            PIC X(10)       VALUE ZERO.
               10  DBLINK-R4015-ACTT-EFCTV-03
                                            PIC X(8)        VALUE ZERO.
           05  DBLINK-R4034-ACCT-EFCTV-KEY.
               10  DBLINK-R4034-ACCT-EFCTV-01
                                            PIC X(6)        VALUE SPACE.
               10  DBLINK-R4034-ACCT-EFCTV-02
                                            PIC X(10)       VALUE ZERO.
               10  DBLINK-R4034-ACCT-EFCTV-03
                                            PIC X(8)        VALUE ZERO.
           05  DBLINK-R4045-ACTI-KEY.
               10  DBLINK-R4045-ACTI-01     PIC X(10)       VALUE SPACE.
           05  DBLINK-R4050-ACTV-KEY.
               10  DBLINK-R4050-ACTV-01     PIC X(6)        VALUE SPACE.
           05  DBLINK-R4053-INTR-ENTY-KEY.
               10  DBLINK-R4053-INTR-ENTY-01
                                            PIC X(4)        VALUE SPACE.
           05  DBLINK-R4052-XTRN-ENTY-KEY.                              235 DOVH
               10  DBLINK-R4052-XTRN-ENTY-01                            235 DOVH
                                            PIC X(4)        VALUE SPACE.235 DOVH
           05  DBLINK-R4054-XTRN-CODE-KEY.
               10  DBLINK-R4054-XTRN-CODE-01
                                            PIC X(10)       VALUE SPACE.
               10  DBLINK-R4054-XTRN-CODE-02
                                            PIC X(26)       VALUE ZERO.
           05  DBLINK-R4066-ACTV-EFCTV-KEY.
               10  DBLINK-R4066-ACTV-EFCTV-01
                                            PIC X(6)        VALUE SPACE.
               10  DBLINK-R4066-ACTV-EFCTV-02
                                            PIC X(10)       VALUE ZERO.
               10  DBLINK-R4066-ACTV-EFCTV-03
                                            PIC X(8)        VALUE ZERO.
           05  DBLINK-R4067-ACTI-EFCTV-KEY.
               10  DBLINK-R4067-ACTI-EFCTV-01
                                            PIC X(10)       VALUE SPACE.
               10  DBLINK-R4067-ACTI-EFCTV-02
                                            PIC X(10)       VALUE ZERO.
               10  DBLINK-R4067-ACTI-EFCTV-03
                                            PIC X(8)        VALUE ZERO.
           05  DBLINK-R4076-COA-EFCTV-KEY.
               10  DBLINK-R4076-COA-EFCTV-01
                                            PIC X(2)        VALUE SPACE.
               10  DBLINK-R4076-COA-EFCTV-02
                                            PIC X(1)        VALUE SPACE.
               10  DBLINK-R4076-COA-EFCTV-03
                                            PIC X(10)       VALUE ZERO.
               10  DBLINK-R4076-COA-EFCTV-04
                                            PIC X(8)        VALUE ZERO.
           05  DBLINK-R4192-USER-PRFL-KEY.
               10  DBLINK-R4192-USER-PRFL-01
                                            PIC X(8)        VALUE SPACE.
           04  DBLINK-SET-KEYS.
           05  DBLINK-S-INDX-ACCT-KEY.                                  240 DOVH
             08  DBLINK-S-INDX-ACCT-KEY-M.                              240 DOVH
             09  DBLINK-S-INDX-ACCT-KEY-U.                              240 DOVH
UNVCOA         10  FILLER                   PIC X(3)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-ACCT-01    PIC X(6)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-ACCT-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-COA-KEY.                                   240 DOVH
             08  DBLINK-S-INDX-COA-KEY-M.                               240 DOVH
             09  DBLINK-S-INDX-COA-KEY-U.                               240 DOVH
               10  DBLINK-S-INDX-COA-01     PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-COA-02     PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-COA-F          PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-FNDT-KEY.                                  240 DOVH
             08  DBLINK-S-INDX-FNDT-KEY-M.                              240 DOVH
             09  DBLINK-S-INDX-FNDT-KEY-U.                              240 DOVH
               10  FILLER                   PIC X(3)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-FNDT-01    PIC X(2)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-FNDT-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-FUND-KEY.                                  240 DOVH
             08  DBLINK-S-INDX-FUND-KEY-M.                              240 DOVH
             09  DBLINK-S-INDX-FUND-KEY-U.                              240 DOVH
               10  FILLER                   PIC X(3)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-FUND-01    PIC X(6)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-FUND-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-INTR-ENTY-KEY.                             240 DOVH
             08  DBLINK-S-INDX-INTR-ENTY-KEY-M.                         240 DOVH
             09  DBLINK-S-INDX-INTR-ENTY-KEY-U.                         240 DOVH
               10  FILLER                   PIC X(3)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-INTR-ENTY-01                           240 DOVH
                                            PIC X(4)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-INTR-ENTY-F    PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-XTRN-ENTY-KEY.                             240 DOVH
             08  DBLINK-S-INDX-XTRN-ENTY-KEY-M.                         240 DOVH
             09  DBLINK-S-INDX-XTRN-ENTY-KEY-U.                         240 DOVH
UNVCOA         10  FILLER                   PIC X(3)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-XTRN-ENTY-01                           240 DOVH
                                            PIC X(4)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-SYSDAT-KEY.                                240 DOVH
             08  DBLINK-S-INDX-SYSDAT-KEY-M.                            240 DOVH
             09  DBLINK-S-INDX-SYSDAT-KEY-U.                            240 DOVH
UNVCD          10  FILLER                   PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-SYSDAT-01  PIC X(8)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-SYSDAT-02  PIC X(30)       VALUE SPACE.240 DOVH
COACD          10  FILLER                   PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-SYSDAT-F       PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-XTRN-ENTY-F    PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-PROG-KEY.                                  240 DOVH
             08  DBLINK-S-INDX-PROG-KEY-M.                              240 DOVH
             09  DBLINK-S-INDX-PROG-KEY-U.                              240 DOVH
               10  FILLER                   PIC X(3)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-PROG-01    PIC X(6)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-PROG-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-ORGN-KEY.                                  240 DOVH
             08  DBLINK-S-INDX-ORGN-KEY-M.                              240 DOVH
             09  DBLINK-S-INDX-ORGN-KEY-U.                              240 DOVH
               10  FILLER                   PIC X(3)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-ORGN-01    PIC X(6)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-ORGN-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-LCTN-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-LCTN-KEY.                                  240 DOVH
             08  DBLINK-S-INDX-LCTN-KEY-M.                              240 DOVH
             09  DBLINK-S-INDX-LCTN-KEY-U.                              240 DOVH
               10  FILLER                   PIC X(3)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-LCTN-01    PIC X(6)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-ACTI-KEY.                                  240 DOVH
             08  DBLINK-S-INDX-ACTI-KEY-M.                              240 DOVH
             09  DBLINK-S-INDX-ACTI-KEY-U.                              240 DOVH
               10  FILLER                   PIC X(3)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-ACTI-01    PIC X(10)       VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-ACTI-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-ACTT-KEY.                                  240 DOVH
             08  DBLINK-S-INDX-ACTT-KEY-M.                              240 DOVH
             09  DBLINK-S-INDX-ACTT-KEY-U.                              240 DOVH
               10  FILLER                   PIC X(3)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-ACTT-01    PIC X(2)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-ACTT-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-ACTV-KEY.                                  240 DOVH
             08  DBLINK-S-INDX-ACTV-KEY-M.                              240 DOVH
             09  DBLINK-S-INDX-ACTV-KEY-U.                              240 DOVH
               10  FILLER                   PIC X(3)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-ACTV-01    PIC X(6)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-ACTV-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-USER-PRFL-KEY.                             240 DOVH
             08  DBLINK-S-INDX-USER-PRFL-KEY-M.                         240 DOVH
             09  DBLINK-S-INDX-USER-PRFL-KEY-U.                         240 DOVH
               10  FILLER                   PIC X(2)        VALUE SPACE.240 DOVH
               10  DBLINK-S-INDX-USER-PRFL-01                           240 DOVH
                                            PIC X(8)        VALUE SPACE.240 DOVH
           05  DBLINK-S-INDX-USER-PRFL-F    PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-4000-4076-KEY.
             08  DBLINK-S-4000-4076-KEY-O.
               10  DBLINK-S-4000-4076-01    PIC X(2)        VALUE SPACE.
               10  DBLINK-S-4000-4076-02    PIC X(1)        VALUE SPACE.
             08  DBLINK-S-4000-4076-KEY-M.
             09  DBLINK-S-4000-4076-KEY-U.
               10  DBLINK-S-4000-4076-03    PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4000-4076-04    PIC X(8)        VALUE ZERO.
           05  DBLINK-WS-S-4000-4076-KEY.
               10  DBLINK-WS-S-4000-4076-03 PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4000-4076-04 PIC X(6)        VALUE SPACE.
           05  DBLINK-S-4000-4076-F         PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4001-4005-O-KEY.
             08  DBLINK-S-4001-4005-O-KEY-O.
               10  DBLINK-S-4001-4005-O-01  PIC X(6)        VALUE SPACE.
             08  DBLINK-S-4001-4005-O-KEY-M.
             09  DBLINK-S-4001-4005-O-KEY-U.
               10  DBLINK-S-4001-4005-O-02  PIC X(6)        VALUE SPACE.
               10  DBLINK-S-4001-4005-O-03  PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4001-4005-O-04  PIC X(8)        VALUE ZERO.
             09  DBLINK-S-4001-4005-O-KEY-S.
               10  DBLINK-S-4001-4005-O-05  PIC X(26)       VALUE ZERO.
           05  DBLINK-WS-S-4001-4005-O-KEY.
               10  DBLINK-WS-S-4001-4005-O-02
                                            PIC X(6)        VALUE SPACE.
               10  DBLINK-WS-S-4001-4005-O-03
                                            PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4001-4005-O-04
                                            PIC X(6)        VALUE SPACE.
             09  DBLINK-S-4001-4005-O-KEY-S.
               10  DBLINK-WS-S-4001-4005-O-05
                                            PIC X(26)       VALUE ZERO.
           05  DBLINK-S-4001-4005-P-KEY.
             08  DBLINK-S-4001-4005-P-KEY-O.
               10  DBLINK-S-4001-4005-P-01  PIC X(6)        VALUE SPACE.
             08  DBLINK-S-4001-4005-P-KEY-M.
             09  DBLINK-S-4001-4005-P-KEY-U.
               10  DBLINK-S-4001-4005-P-02  PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4001-4005-P-03  PIC X(8)        VALUE ZERO.
           05  DBLINK-WS-S-4001-4005-P-KEY.
               10  DBLINK-WS-S-4001-4005-P-02
                                            PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4001-4005-P-03
                                            PIC X(6)        VALUE SPACE.
           05  DBLINK-S-4001-4185-KEY.
             08  DBLINK-S-4001-4185-KEY-O.
               10  DBLINK-S-4001-4185-01    PIC X(6)        VALUE SPACE.
             08  DBLINK-S-4001-4185-KEY-M.
             09  DBLINK-S-4001-4185-KEY-S.
               10  DBLINK-S-4001-4185-02    PIC X(26)       VALUE ZERO.
           05  DBLINK-S-4001-4005-O-F       PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4001-4005-P-F       PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4001-4185-F         PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4002-4010-O-KEY.
             08  DBLINK-S-4002-4010-O-KEY-O.
               10  DBLINK-S-4002-4010-O-01  PIC X(6)        VALUE SPACE.
             08  DBLINK-S-4002-4010-O-KEY-M.
             09  DBLINK-S-4002-4010-O-KEY-U.
               10  DBLINK-S-4002-4010-O-02  PIC X(6)        VALUE SPACE.
               10  DBLINK-S-4002-4010-O-03  PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4002-4010-O-04  PIC X(8)        VALUE ZERO.
             09  DBLINK-S-4002-4010-O-KEY-S.
               10  DBLINK-S-4002-4010-O-05  PIC X(26)       VALUE ZERO.
           05  DBLINK-WS-S-4002-4010-O-KEY.
               10  DBLINK-WS-S-4002-4010-O-02
                                            PIC X(6)        VALUE SPACE.
               10  DBLINK-WS-S-4002-4010-O-03
                                            PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4002-4010-O-04
                                            PIC X(6)        VALUE SPACE.
             09  DBLINK-S-4002-4010-O-KEY-S.
               10  DBLINK-WS-S-4002-4010-O-05
                                            PIC X(26)       VALUE ZERO.
           05  DBLINK-S-4002-4010-P-KEY.
             08  DBLINK-S-4002-4010-P-KEY-O.
               10  DBLINK-S-4002-4010-P-01  PIC X(6)        VALUE SPACE.
             08  DBLINK-S-4002-4010-P-KEY-M.
             09  DBLINK-S-4002-4010-P-KEY-U.
               10  DBLINK-S-4002-4010-P-02  PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4002-4010-P-03  PIC X(8)        VALUE ZERO.
           05  DBLINK-WS-S-4002-4010-P-KEY.
               10  DBLINK-WS-S-4002-4010-P-02
                                            PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4002-4010-P-03
                                            PIC X(6)        VALUE SPACE.
           05  DBLINK-S-4002-4100-KEY.
             08  DBLINK-S-4002-4100-KEY-O.
               10  DBLINK-S-4002-4100-01    PIC X(6)        VALUE SPACE.
             08  DBLINK-S-4002-4100-KEY-M.
             09  DBLINK-S-4002-4100-KEY-U.
               10  DBLINK-S-4002-4100-02    PIC X(4)        VALUE SPACE.
           05  DBLINK-S-4002-4010-O-F       PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4002-4010-P-F       PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4002-4100-F         PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4003-4034-O-KEY.
             08  DBLINK-S-4003-4034-O-KEY-O.
               10  DBLINK-S-4003-4034-O-01  PIC X(6)        VALUE SPACE.
             08  DBLINK-S-4003-4034-O-KEY-M.
             09  DBLINK-S-4003-4034-O-KEY-U.
               10  DBLINK-S-4003-4034-O-02  PIC X(6)        VALUE SPACE.
               10  DBLINK-S-4003-4034-O-03  PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4003-4034-O-04  PIC X(8)        VALUE ZERO.
             09  DBLINK-S-4003-4034-O-KEY-S.
               10  DBLINK-S-4003-4034-O-05  PIC X(26)       VALUE ZERO.
           05  DBLINK-WS-S-4003-4034-O-KEY.
               10  DBLINK-WS-S-4003-4034-O-02
                                            PIC X(6)        VALUE SPACE.
               10  DBLINK-WS-S-4003-4034-O-03
                                            PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4003-4034-O-04
                                            PIC X(6)        VALUE SPACE.
             09  DBLINK-S-4003-4034-O-KEY-S.
               10  DBLINK-WS-S-4003-4034-O-05
                                            PIC X(26)       VALUE ZERO.
           05  DBLINK-S-4003-4034-P-KEY.
             08  DBLINK-S-4003-4034-P-KEY-O.
               10  DBLINK-S-4003-4034-P-01  PIC X(6)        VALUE SPACE.
             08  DBLINK-S-4003-4034-P-KEY-M.
             09  DBLINK-S-4003-4034-P-KEY-U.
               10  DBLINK-S-4003-4034-P-02  PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4003-4034-P-03  PIC X(8)        VALUE ZERO.
           05  DBLINK-WS-S-4003-4034-P-KEY.
               10  DBLINK-WS-S-4003-4034-P-02
                                            PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4003-4034-P-03
                                            PIC X(6)        VALUE SPACE.
           05  DBLINK-S-4003-4034-O-F       PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4003-4034-P-F       PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4004-4105-O-KEY.
             08  DBLINK-S-4004-4105-O-KEY-O.
               10  DBLINK-S-4004-4105-O-01  PIC X(6)        VALUE SPACE.
             08  DBLINK-S-4004-4105-O-KEY-M.
             09  DBLINK-S-4004-4105-O-KEY-U.
               10  DBLINK-S-4004-4105-O-02  PIC X(6)        VALUE SPACE.
               10  DBLINK-S-4004-4105-O-03  PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4004-4105-O-04  PIC X(8)        VALUE ZERO.
             09  DBLINK-S-4004-4105-O-KEY-S.
               10  DBLINK-S-4004-4105-O-05  PIC X(26)       VALUE ZERO.
           05  DBLINK-WS-S-4004-4105-O-KEY.
               10  DBLINK-WS-S-4004-4105-O-02
                                            PIC X(6)        VALUE SPACE.
               10  DBLINK-WS-S-4004-4105-O-03
                                            PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4004-4105-O-04
                                            PIC X(6)        VALUE SPACE.
             09  DBLINK-S-4004-4105-O-KEY-S.
               10  DBLINK-WS-S-4004-4105-O-05
                                            PIC X(26)       VALUE ZERO.
           05  DBLINK-S-4004-4105-P-KEY.
             08  DBLINK-S-4004-4105-P-KEY-O.
               10  DBLINK-S-4004-4105-P-01  PIC X(6)        VALUE SPACE.
             08  DBLINK-S-4004-4105-P-KEY-M.
             09  DBLINK-S-4004-4105-P-KEY-U.
               10  DBLINK-S-4004-4105-P-02  PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4004-4105-P-03  PIC X(8)        VALUE ZERO.
           05  DBLINK-WS-S-4004-4105-P-KEY.
               10  DBLINK-WS-S-4004-4105-P-02
                                            PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4004-4105-P-03
                                            PIC X(6)        VALUE SPACE.
           05  DBLINK-S-4004-4105-O-F       PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4004-4105-P-F       PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4006-4007-O-KEY.
             08  DBLINK-S-4006-4007-O-KEY-O.
               10  DBLINK-S-4006-4007-O-01  PIC X(6)        VALUE SPACE.
             08  DBLINK-S-4006-4007-O-KEY-M.
             09  DBLINK-S-4006-4007-O-KEY-U.
               10  DBLINK-S-4006-4007-O-02  PIC X(6)        VALUE SPACE.
               10  DBLINK-S-4006-4007-O-03  PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4006-4007-O-04  PIC X(8)        VALUE ZERO.
             09  DBLINK-S-4006-4007-O-KEY-S.
               10  DBLINK-S-4006-4007-O-05  PIC X(26)       VALUE ZERO.
           05  DBLINK-WS-S-4006-4007-O-KEY.
               10  DBLINK-WS-S-4006-4007-O-02
                                            PIC X(6)        VALUE SPACE.
               10  DBLINK-WS-S-4006-4007-O-03
                                            PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4006-4007-O-04
                                            PIC X(6)        VALUE SPACE.
             09  DBLINK-S-4006-4007-O-KEY-S.
               10  DBLINK-WS-S-4006-4007-O-05
                                            PIC X(26)       VALUE ZERO.
           05  DBLINK-S-4006-4007-P-KEY.
             08  DBLINK-S-4006-4007-P-KEY-O.
               10  DBLINK-S-4006-4007-P-01  PIC X(6)        VALUE SPACE.
             08  DBLINK-S-4006-4007-P-KEY-M.
             09  DBLINK-S-4006-4007-P-KEY-U.
               10  DBLINK-S-4006-4007-P-02  PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4006-4007-P-03  PIC X(8)        VALUE ZERO.
           05  DBLINK-WS-S-4006-4007-P-KEY.
               10  DBLINK-WS-S-4006-4007-P-02
                                            PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4006-4007-P-03
                                            PIC X(6)        VALUE SPACE.
           05  DBLINK-S-4006-4007-O-F       PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4006-4007-P-F       PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4008-4005-KEY.
             08  DBLINK-S-4008-4005-KEY-O.
               10  DBLINK-S-4008-4005-01    PIC X(2)        VALUE SPACE.
             08  DBLINK-S-4008-4005-KEY-M.
             09  DBLINK-S-4008-4005-KEY-U.
               10  DBLINK-S-4008-4005-02    PIC X(6)        VALUE SPACE.
               10  DBLINK-S-4008-4005-03    PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4008-4005-04    PIC X(8)        VALUE ZERO.
           05  DBLINK-WS-S-4008-4005-KEY.
               10  DBLINK-WS-S-4008-4005-02 PIC X(6)        VALUE SPACE.
               10  DBLINK-WS-S-4008-4005-03 PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4008-4005-04 PIC X(6)        VALUE SPACE.
           05  DBLINK-S-4008-4009-O-KEY.
             08  DBLINK-S-4008-4009-O-KEY-O.
               10  DBLINK-S-4008-4009-O-01  PIC X(2)        VALUE SPACE.
             08  DBLINK-S-4008-4009-O-KEY-M.
             09  DBLINK-S-4008-4009-O-KEY-U.
               10  DBLINK-S-4008-4009-O-02  PIC X(2)        VALUE SPACE.
               10  DBLINK-S-4008-4009-O-03  PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4008-4009-O-04  PIC X(8)        VALUE ZERO.
             09  DBLINK-S-4008-4009-O-KEY-S.
               10  DBLINK-S-4008-4009-O-05  PIC X(26)       VALUE ZERO.
           05  DBLINK-WS-S-4008-4009-O-KEY.
               10  DBLINK-WS-S-4008-4009-O-02
                                            PIC X(2)        VALUE SPACE.
               10  DBLINK-WS-S-4008-4009-O-03
                                            PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4008-4009-O-04
                                            PIC X(6)        VALUE SPACE.
             09  DBLINK-S-4008-4009-O-KEY-S.
               10  DBLINK-WS-S-4008-4009-O-05
                                            PIC X(26)       VALUE ZERO.
           05  DBLINK-S-4008-4009-P-KEY.
             08  DBLINK-S-4008-4009-P-KEY-O.
               10  DBLINK-S-4008-4009-P-01  PIC X(2)        VALUE SPACE.
             08  DBLINK-S-4008-4009-P-KEY-M.
             09  DBLINK-S-4008-4009-P-KEY-U.
               10  DBLINK-S-4008-4009-P-02  PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4008-4009-P-03  PIC X(8)        VALUE ZERO.
           05  DBLINK-WS-S-4008-4009-P-KEY.
               10  DBLINK-WS-S-4008-4009-P-02
                                            PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4008-4009-P-03
                                            PIC X(6)        VALUE SPACE.
           05  DBLINK-S-4008-4005-F         PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4008-4009-O-F       PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4008-4009-P-F       PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4011-4012-KEY.
             08  DBLINK-S-4011-4012-KEY-O.
               10  DBLINK-S-4011-4012-01    PIC X(8)        VALUE SPACE.
               10  DBLINK-S-4011-4012-02    PIC X(30)       VALUE SPACE.
             08  DBLINK-S-4011-4012-KEY-M.
             09  DBLINK-S-4011-4012-KEY-U.
               10  DBLINK-S-4011-4012-03    PIC X(8)        VALUE SPACE.
               10  DBLINK-S-4011-4012-04    PIC X(8)        VALUE SPACE.
               10  DBLINK-S-4011-4012-05    PIC S9(2)       COMP-3
                       VALUE ZERO.
           05  DBLINK-WS-S-4011-4012-KEY.
               10  DBLINK-WS-S-4011-4012-03 PIC X(8)        VALUE SPACE.
               10  DBLINK-WS-S-4011-4012-04 PIC X(8)        VALUE SPACE.
               10  DBLINK-WS-S-4011-4012-05 PIC 9(2)        VALUE ZERO.
           05  DBLINK-S-4011-4018-KEY.
             08  DBLINK-S-4011-4018-KEY-O.
               10  DBLINK-S-4011-4018-01    PIC X(8)        VALUE SPACE.
               10  DBLINK-S-4011-4018-02    PIC X(30)       VALUE SPACE.
             08  DBLINK-S-4011-4018-KEY-M.
             09  DBLINK-S-4011-4018-KEY-U.
               10  DBLINK-S-4011-4018-03    PIC X(8)        VALUE SPACE.
               10  DBLINK-S-4011-4018-04    PIC X(8)        VALUE SPACE.
               10  DBLINK-S-4011-4018-05    PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4011-4018-06    PIC X(8)        VALUE ZERO.
           05  DBLINK-WS-S-4011-4018-KEY.
               10  DBLINK-WS-S-4011-4018-03 PIC X(8)        VALUE SPACE.
               10  DBLINK-WS-S-4011-4018-04 PIC X(8)        VALUE SPACE.
               10  DBLINK-WS-S-4011-4018-05 PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4011-4018-06 PIC X(6)        VALUE SPACE.
           05  DBLINK-S-4011-4012-F         PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4011-4018-F         PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4014-4015-O-KEY.
             08  DBLINK-S-4014-4015-O-KEY-O.
               10  DBLINK-S-4014-4015-O-01  PIC X(2)        VALUE SPACE.
             08  DBLINK-S-4014-4015-O-KEY-M.
             09  DBLINK-S-4014-4015-O-KEY-U.
               10  DBLINK-S-4014-4015-O-02  PIC X(2)        VALUE SPACE.
               10  DBLINK-S-4014-4015-O-03  PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4014-4015-O-04  PIC X(8)        VALUE ZERO.
             09  DBLINK-S-4014-4015-O-KEY-S.
               10  DBLINK-S-4014-4015-O-05  PIC X(26)       VALUE ZERO.
           05  DBLINK-WS-S-4014-4015-O-KEY.
               10  DBLINK-WS-S-4014-4015-O-02
                                            PIC X(2)        VALUE SPACE.
               10  DBLINK-WS-S-4014-4015-O-03
                                            PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4014-4015-O-04
                                            PIC X(6)        VALUE SPACE.
             09  DBLINK-S-4014-4015-O-KEY-S.
               10  DBLINK-WS-S-4014-4015-O-05
                                            PIC X(26)       VALUE ZERO.
           05  DBLINK-S-4014-4015-P-KEY.
             08  DBLINK-S-4014-4015-P-KEY-O.
               10  DBLINK-S-4014-4015-P-01  PIC X(2)        VALUE SPACE.
             08  DBLINK-S-4014-4015-P-KEY-M.
             09  DBLINK-S-4014-4015-P-KEY-U.
               10  DBLINK-S-4014-4015-P-02  PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4014-4015-P-03  PIC X(8)        VALUE ZERO.
           05  DBLINK-WS-S-4014-4015-P-KEY.
               10  DBLINK-WS-S-4014-4015-P-02
                                            PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4014-4015-P-03
                                            PIC X(6)        VALUE SPACE.
           05  DBLINK-S-4014-4034-KEY.
             08  DBLINK-S-4014-4034-KEY-O.
               10  DBLINK-S-4014-4034-01    PIC X(2)        VALUE SPACE.
             08  DBLINK-S-4014-4034-KEY-M.
             09  DBLINK-S-4014-4034-KEY-U.
               10  DBLINK-S-4014-4034-02    PIC X(6)        VALUE SPACE.
               10  DBLINK-S-4014-4034-03    PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4014-4034-04    PIC X(8)        VALUE ZERO.
           05  DBLINK-WS-S-4014-4034-KEY.
               10  DBLINK-WS-S-4014-4034-02 PIC X(6)        VALUE SPACE.
               10  DBLINK-WS-S-4014-4034-03 PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4014-4034-04 PIC X(6)        VALUE SPACE.
           05  DBLINK-S-4014-4015-O-F       PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4014-4015-P-F       PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4014-4034-F         PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4045-4067-KEY.
             08  DBLINK-S-4045-4067-KEY-O.
               10  DBLINK-S-4045-4067-01    PIC X(10)       VALUE SPACE.
             08  DBLINK-S-4045-4067-KEY-M.
             09  DBLINK-S-4045-4067-KEY-U.
               10  DBLINK-S-4045-4067-02    PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4045-4067-03    PIC X(8)        VALUE ZERO.
           05  DBLINK-WS-S-4045-4067-KEY.
               10  DBLINK-WS-S-4045-4067-02 PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4045-4067-03 PIC X(6)        VALUE SPACE.
           05  DBLINK-S-4045-4067-F         PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4050-4066-KEY.
             08  DBLINK-S-4050-4066-KEY-O.
               10  DBLINK-S-4050-4066-01    PIC X(6)        VALUE SPACE.
             08  DBLINK-S-4050-4066-KEY-M.
             09  DBLINK-S-4050-4066-KEY-U.
               10  DBLINK-S-4050-4066-02    PIC X(10)       VALUE ZERO.
               10  DBLINK-S-4050-4066-03    PIC X(8)        VALUE ZERO.
           05  DBLINK-WS-S-4050-4066-KEY.
               10  DBLINK-WS-S-4050-4066-02 PIC S9(8)       COMP-3
                       VALUE ZERO.
               10  DBLINK-WS-S-4050-4066-03 PIC X(6)        VALUE SPACE.
           05  DBLINK-S-4050-4066-F         PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4052-4054-KEY.                                  240 DOVH
             08  DBLINK-S-4052-4054-KEY-O.                              240 DOVH
               10  DBLINK-S-4052-4054-01    PIC X(4)        VALUE SPACE.240 DOVH
             08  DBLINK-S-4052-4054-KEY-M.                              240 DOVH
             09  DBLINK-S-4052-4054-KEY-U.                              240 DOVH
               10  DBLINK-S-4052-4054-02    PIC X(10)       VALUE SPACE.240 DOVH
           05  DBLINK-S-4052-4054-F         PIC X(1)        VALUE SPACE.240 DOVH
           05  DBLINK-S-4053-4054-KEY.
             08  DBLINK-S-4053-4054-KEY-O.
               10  DBLINK-S-4053-4054-01    PIC X(4)        VALUE SPACE.
             08  DBLINK-S-4053-4054-KEY-M.
             09  DBLINK-S-4053-4054-KEY-S.
               10  DBLINK-S-4053-4054-02    PIC X(26)       VALUE ZERO.
           05  DBLINK-S-4053-4054-F         PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4054-4055-KEY.
             08  DBLINK-S-4054-4055-KEY-O.
               10  DBLINK-S-4054-4055-01    PIC X(10)       VALUE SPACE.
               10  DBLINK-S-4054-4055-02    PIC X(26)       VALUE ZERO.
             08  DBLINK-S-4054-4055-KEY-M.
             09  DBLINK-S-4054-4055-KEY-U.
               10  DBLINK-S-4054-4055-03    PIC X(10)       VALUE SPACE.
           05  DBLINK-S-4054-4055-F         PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4192-4013-KEY.
             08  DBLINK-S-4192-4013-KEY-O.
               10  DBLINK-S-4192-4013-01    PIC X(8)        VALUE SPACE.
             08  DBLINK-S-4192-4013-KEY-M.
             09  DBLINK-S-4192-4013-KEY-U.
               10  DBLINK-S-4192-4013-02    PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4192-4020-KEY.
             08  DBLINK-S-4192-4020-KEY-O.
               10  DBLINK-S-4192-4020-01    PIC X(8)        VALUE SPACE.
             08  DBLINK-S-4192-4020-KEY-M.
             09  DBLINK-S-4192-4020-KEY-U.
COACD          10  FILLER                   PIC X(1)        VALUE SPACE.
               10  DBLINK-S-4192-4020-02    PIC X(2)        VALUE SPACE.
               10  DBLINK-S-4192-4020-03    PIC X(6)        VALUE SPACE.
           05  DBLINK-S-4192-4204-KEY.
             08  DBLINK-S-4192-4204-KEY-O.
               10  DBLINK-S-4192-4204-01    PIC X(8)        VALUE SPACE.
             08  DBLINK-S-4192-4204-KEY-M.
             09  DBLINK-S-4192-4204-KEY-S.
               10  DBLINK-S-4192-4204-02    PIC X(26)       VALUE ZERO.
           05  DBLINK-S-4192-4013-F         PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4192-4020-F         PIC X(1)        VALUE SPACE.
           05  DBLINK-S-4192-4204-F         PIC X(1)        VALUE SPACE.
           05  DBLINK-R4105-PROG-EFCTV-KEY.                             235 DOVH
               10  DBLINK-R4105-PROG-EFCTV-01                           235 DOVH
                                            PIC X(6)        VALUE SPACE.235 DOVH
               10  DBLINK-R4105-PROG-EFCTV-02                           235 DOVH
                                            PIC X(10)       VALUE ZERO. 235 DOVH
               10  DBLINK-R4105-PROG-EFCTV-03                           235 DOVH
                                            PIC X(8)        VALUE ZERO. 235 DOVH
