      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FTAWD10                              04/24/00  13:19  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FTAWD10.
           02  WORK-DATE-TA10                 COMP-3    PIC S9(8).
           02  SAVE-DBKEY-ID-4400-TA10                  PIC X(05).
           02  SAVE-DBKEY-ID-4406-TA10                  PIC X(08).
           02  SAVE-DBKEY-ID-4401-TA10                  PIC X(08).
           02  WORK-AMT-TA10                            PIC S9(6)V99.
           02  WORK-RCN-DATE-TA10             COMP-3    PIC S9(8).
           02  HOLD-RCNCLTN-DATE-TA10.
               03  HOLD-YEAR-DATE-TA10                  PIC 9(2).
               03  HOLD-MONTH-DATE-TA10                 PIC 9(2).
               03  HOLD-DAY-DATE-TA10                   PIC 9(2).
           02  HOLD-RCN-DATE-TA10                       PIC 9(6).
           02  SHORT-DESC-TA10                          PIC 9(10).
           02  RCNCLTN-OK-SW-TA10                       PIC X(1).
           02  TMPLT-CHG-FLAG-TA10                      PIC X(1).
           02  EVENT-OK-SW-TA10                         PIC X(1).
           02  SUSP-FLAG-TA10                           PIC X(1).
           02  ERR-FLAG-TA10                            PIC X(1).
           02  TEST-DATE-TA10.
               03  YEAR-DATE-TA10                       PIC 9(2).
               03  MONTH-DATE-TA10                      PIC 9(2).
               03  DAY-DATE-TA10                        PIC 9(2).
           02  CONFIRM-ON-SW-TA10                       PIC X(1).
           02  END-APRVL-FLAG-TA10                      PIC X(1).
           02  FINAL-APRVL-FLAG-TA10                    PIC X(1).
           02  DELETE-FLAG-TA10                         PIC X(1).
           02  APRVL-USED-FLAG-TA10                     PIC X(1).
           02  SHORT-DESC-N-TA10                        PIC 9(10).
           02  NMBR-SWITCH-TA10                         PIC X(1).
           02  WRK-EVENT-BAL-TA10             COMP-3    PIC S9(6)V99.
           02  WRK-EVENT-BAL2-TA10            COMP-3    PIC S9(6)V99.
           02  WORK-TEXT-ENTY-CODE-TA10.
               03  WORK-DCMNT-NMBR-TA10                 PIC X(8).
               03  WORK-TEXT-TYPE-TA10                  PIC X(7).
           02  ACCT-WARNING-IND-TA10                    PIC X(1).
           02  FUND-WARNING-IND-TA10                    PIC X(1).
