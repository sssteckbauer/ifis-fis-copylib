      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPEWD35                              04/24/00  13:10  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPEWD35.
           02  SAVE-DBKEY-ID-6117-PE35        COMP      PIC S9(8).
           02  WORK-ASGN-ID-PE35.
               03  WORK-STORE-ID-PE35.
                   04  WORK-ALPHA-ID-PE35               PIC X(1).
                   04  WORK-NMRC-ID-PE35                PIC 9(7).
               03  WORK-CHCK-DIGIT-ID-PE35              PIC 9(1).
           02  SEED-NMBR-PE35                           PIC 9(9).
           02  RNDM-NMBR-PE35 REDEFINES
               SEED-NMBR-PE35                           PIC V9(9).
           02  PAN-NMBR-PE35 REDEFINES
               SEED-NMBR-PE35                           PIC X(9).
