    ***Created by Convert/DC version V8R03 on 12/12/00 at 14:00***

      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATA-CODE-CH28-Z = 'Y'
               MOVE WK01-DATA-CODE-CH28 TO RQST-DATA-CODE-CH28 OF
                FCHWK28
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH281-Z = 'Y'
               MOVE WK01-CODE-CH281 TO ACTN-CODE-CH28 OF FCHWM28(0001)
           END-IF.
           MOVE WK01-CODE-CH281-F TO WK01-CODE-CH28-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH282-Z = 'Y'
               MOVE WK01-CODE-CH282 TO ACTN-CODE-CH28 OF FCHWM28(0002)
           END-IF.
           MOVE WK01-CODE-CH282-F TO WK01-CODE-CH28-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH283-Z = 'Y'
               MOVE WK01-CODE-CH283 TO ACTN-CODE-CH28 OF FCHWM28(0003)
           END-IF.
           MOVE WK01-CODE-CH283-F TO WK01-CODE-CH28-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH284-Z = 'Y'
               MOVE WK01-CODE-CH284 TO ACTN-CODE-CH28 OF FCHWM28(0004)
           END-IF.
           MOVE WK01-CODE-CH284-F TO WK01-CODE-CH28-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH285-Z = 'Y'
               MOVE WK01-CODE-CH285 TO ACTN-CODE-CH28 OF FCHWM28(0005)
           END-IF.
           MOVE WK01-CODE-CH285-F TO WK01-CODE-CH28-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH286-Z = 'Y'
               MOVE WK01-CODE-CH286 TO ACTN-CODE-CH28 OF FCHWM28(0006)
           END-IF.
           MOVE WK01-CODE-CH286-F TO WK01-CODE-CH28-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH287-Z = 'Y'
               MOVE WK01-CODE-CH287 TO ACTN-CODE-CH28 OF FCHWM28(0007)
           END-IF.
           MOVE WK01-CODE-CH287-F TO WK01-CODE-CH28-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH288-Z = 'Y'
               MOVE WK01-CODE-CH288 TO ACTN-CODE-CH28 OF FCHWM28(0008)
           END-IF.
           MOVE WK01-CODE-CH288-F TO WK01-CODE-CH28-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH289-Z = 'Y'
               MOVE WK01-CODE-CH289 TO ACTN-CODE-CH28 OF FCHWM28(0009)
           END-IF.
           MOVE WK01-CODE-CH289-F TO WK01-CODE-CH28-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH2810-Z = 'Y'
               MOVE WK01-CODE-CH2810 TO ACTN-CODE-CH28 OF FCHWM28(0010)
           END-IF.
           MOVE WK01-CODE-CH2810-F TO WK01-CODE-CH28-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH2811-Z = 'Y'
               MOVE WK01-CODE-CH2811 TO ACTN-CODE-CH28 OF FCHWM28(0011)
           END-IF.
           MOVE WK01-CODE-CH2811-F TO WK01-CODE-CH28-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH2812-Z = 'Y'
               MOVE WK01-CODE-CH2812 TO ACTN-CODE-CH28 OF FCHWM28(0012)
           END-IF.
           MOVE WK01-CODE-CH2812-F TO WK01-CODE-CH28-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4038-CH281-Z = 'Y'
               MOVE WK01-CODE-4038-CH281 TO DATA-CODE-4038-CH28 OF
                FCHWM28(0001)
           END-IF.
           MOVE WK01-CODE-4038-CH281-F TO WK01-CODE-4038-CH28-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4038-CH282-Z = 'Y'
               MOVE WK01-CODE-4038-CH282 TO DATA-CODE-4038-CH28 OF
                FCHWM28(0002)
           END-IF.
           MOVE WK01-CODE-4038-CH282-F TO WK01-CODE-4038-CH28-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4038-CH283-Z = 'Y'
               MOVE WK01-CODE-4038-CH283 TO DATA-CODE-4038-CH28 OF
                FCHWM28(0003)
           END-IF.
           MOVE WK01-CODE-4038-CH283-F TO WK01-CODE-4038-CH28-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4038-CH284-Z = 'Y'
               MOVE WK01-CODE-4038-CH284 TO DATA-CODE-4038-CH28 OF
                FCHWM28(0004)
           END-IF.
           MOVE WK01-CODE-4038-CH284-F TO WK01-CODE-4038-CH28-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4038-CH285-Z = 'Y'
               MOVE WK01-CODE-4038-CH285 TO DATA-CODE-4038-CH28 OF
                FCHWM28(0005)
           END-IF.
           MOVE WK01-CODE-4038-CH285-F TO WK01-CODE-4038-CH28-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4038-CH286-Z = 'Y'
               MOVE WK01-CODE-4038-CH286 TO DATA-CODE-4038-CH28 OF
                FCHWM28(0006)
           END-IF.
           MOVE WK01-CODE-4038-CH286-F TO WK01-CODE-4038-CH28-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4038-CH287-Z = 'Y'
               MOVE WK01-CODE-4038-CH287 TO DATA-CODE-4038-CH28 OF
                FCHWM28(0007)
           END-IF.
           MOVE WK01-CODE-4038-CH287-F TO WK01-CODE-4038-CH28-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4038-CH288-Z = 'Y'
               MOVE WK01-CODE-4038-CH288 TO DATA-CODE-4038-CH28 OF
                FCHWM28(0008)
           END-IF.
           MOVE WK01-CODE-4038-CH288-F TO WK01-CODE-4038-CH28-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4038-CH289-Z = 'Y'
               MOVE WK01-CODE-4038-CH289 TO DATA-CODE-4038-CH28 OF
                FCHWM28(0009)
           END-IF.
           MOVE WK01-CODE-4038-CH289-F TO WK01-CODE-4038-CH28-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4038-CH2810-Z = 'Y'
               MOVE WK01-CODE-4038-CH2810 TO DATA-CODE-4038-CH28 OF
                FCHWM28(0010)
           END-IF.
           MOVE WK01-CODE-4038-CH2810-F TO WK01-CODE-4038-CH28-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4038-CH2811-Z = 'Y'
               MOVE WK01-CODE-4038-CH2811 TO DATA-CODE-4038-CH28 OF
                FCHWM28(0011)
           END-IF.
           MOVE WK01-CODE-4038-CH2811-F TO WK01-CODE-4038-CH28-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4038-CH2812-Z = 'Y'
               MOVE WK01-CODE-4038-CH2812 TO DATA-CODE-4038-CH28 OF
                FCHWM28(0012)
           END-IF.
           MOVE WK01-CODE-4038-CH2812-F TO WK01-CODE-4038-CH28-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH281-Z = 'Y'
               MOVE WK01-DESC-4038-CH281 TO SHORT-DESC-4038-CH28 OF
                FCHWM28(0001)
           END-IF.
           MOVE WK01-DESC-4038-CH281-F TO WK01-DESC-4038-CH28-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH282-Z = 'Y'
               MOVE WK01-DESC-4038-CH282 TO SHORT-DESC-4038-CH28 OF
                FCHWM28(0002)
           END-IF.
           MOVE WK01-DESC-4038-CH282-F TO WK01-DESC-4038-CH28-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH283-Z = 'Y'
               MOVE WK01-DESC-4038-CH283 TO SHORT-DESC-4038-CH28 OF
                FCHWM28(0003)
           END-IF.
           MOVE WK01-DESC-4038-CH283-F TO WK01-DESC-4038-CH28-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH284-Z = 'Y'
               MOVE WK01-DESC-4038-CH284 TO SHORT-DESC-4038-CH28 OF
                FCHWM28(0004)
           END-IF.
           MOVE WK01-DESC-4038-CH284-F TO WK01-DESC-4038-CH28-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH285-Z = 'Y'
               MOVE WK01-DESC-4038-CH285 TO SHORT-DESC-4038-CH28 OF
                FCHWM28(0005)
           END-IF.
           MOVE WK01-DESC-4038-CH285-F TO WK01-DESC-4038-CH28-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH286-Z = 'Y'
               MOVE WK01-DESC-4038-CH286 TO SHORT-DESC-4038-CH28 OF
                FCHWM28(0006)
           END-IF.
           MOVE WK01-DESC-4038-CH286-F TO WK01-DESC-4038-CH28-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH287-Z = 'Y'
               MOVE WK01-DESC-4038-CH287 TO SHORT-DESC-4038-CH28 OF
                FCHWM28(0007)
           END-IF.
           MOVE WK01-DESC-4038-CH287-F TO WK01-DESC-4038-CH28-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH288-Z = 'Y'
               MOVE WK01-DESC-4038-CH288 TO SHORT-DESC-4038-CH28 OF
                FCHWM28(0008)
           END-IF.
           MOVE WK01-DESC-4038-CH288-F TO WK01-DESC-4038-CH28-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH289-Z = 'Y'
               MOVE WK01-DESC-4038-CH289 TO SHORT-DESC-4038-CH28 OF
                FCHWM28(0009)
           END-IF.
           MOVE WK01-DESC-4038-CH289-F TO WK01-DESC-4038-CH28-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH2810-Z = 'Y'
               MOVE WK01-DESC-4038-CH2810 TO SHORT-DESC-4038-CH28 OF
                FCHWM28(0010)
           END-IF.
           MOVE WK01-DESC-4038-CH2810-F TO WK01-DESC-4038-CH28-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH2811-Z = 'Y'
               MOVE WK01-DESC-4038-CH2811 TO SHORT-DESC-4038-CH28 OF
                FCHWM28(0011)
           END-IF.
           MOVE WK01-DESC-4038-CH2811-F TO WK01-DESC-4038-CH28-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH2812-Z = 'Y'
               MOVE WK01-DESC-4038-CH2812 TO SHORT-DESC-4038-CH28 OF
                FCHWM28(0012)
           END-IF.
           MOVE WK01-DESC-4038-CH2812-F TO WK01-DESC-4038-CH28-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH2A1-Z = 'Y'
               MOVE WK01-DESC-4038-CH2A1 TO LONG-DESC-4038-CH28 OF
                FCHWM28(0001)
           END-IF.
           MOVE WK01-DESC-4038-CH2A1-F TO WK01-DESC-4038-CH2A-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH2A2-Z = 'Y'
               MOVE WK01-DESC-4038-CH2A2 TO LONG-DESC-4038-CH28 OF
                FCHWM28(0002)
           END-IF.
           MOVE WK01-DESC-4038-CH2A2-F TO WK01-DESC-4038-CH2A-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH2A3-Z = 'Y'
               MOVE WK01-DESC-4038-CH2A3 TO LONG-DESC-4038-CH28 OF
                FCHWM28(0003)
           END-IF.
           MOVE WK01-DESC-4038-CH2A3-F TO WK01-DESC-4038-CH2A-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH2A4-Z = 'Y'
               MOVE WK01-DESC-4038-CH2A4 TO LONG-DESC-4038-CH28 OF
                FCHWM28(0004)
           END-IF.
           MOVE WK01-DESC-4038-CH2A4-F TO WK01-DESC-4038-CH2A-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH2A5-Z = 'Y'
               MOVE WK01-DESC-4038-CH2A5 TO LONG-DESC-4038-CH28 OF
                FCHWM28(0005)
           END-IF.
           MOVE WK01-DESC-4038-CH2A5-F TO WK01-DESC-4038-CH2A-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH2A6-Z = 'Y'
               MOVE WK01-DESC-4038-CH2A6 TO LONG-DESC-4038-CH28 OF
                FCHWM28(0006)
           END-IF.
           MOVE WK01-DESC-4038-CH2A6-F TO WK01-DESC-4038-CH2A-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH2A7-Z = 'Y'
               MOVE WK01-DESC-4038-CH2A7 TO LONG-DESC-4038-CH28 OF
                FCHWM28(0007)
           END-IF.
           MOVE WK01-DESC-4038-CH2A7-F TO WK01-DESC-4038-CH2A-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH2A8-Z = 'Y'
               MOVE WK01-DESC-4038-CH2A8 TO LONG-DESC-4038-CH28 OF
                FCHWM28(0008)
           END-IF.
           MOVE WK01-DESC-4038-CH2A8-F TO WK01-DESC-4038-CH2A-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH2A9-Z = 'Y'
               MOVE WK01-DESC-4038-CH2A9 TO LONG-DESC-4038-CH28 OF
                FCHWM28(0009)
           END-IF.
           MOVE WK01-DESC-4038-CH2A9-F TO WK01-DESC-4038-CH2A-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH2A10-Z = 'Y'
               MOVE WK01-DESC-4038-CH2A10 TO LONG-DESC-4038-CH28 OF
                FCHWM28(0010)
           END-IF.
           MOVE WK01-DESC-4038-CH2A10-F TO WK01-DESC-4038-CH2A-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH2A11-Z = 'Y'
               MOVE WK01-DESC-4038-CH2A11 TO LONG-DESC-4038-CH28 OF
                FCHWM28(0011)
           END-IF.
           MOVE WK01-DESC-4038-CH2A11-F TO WK01-DESC-4038-CH2A-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4038-CH2A12-Z = 'Y'
               MOVE WK01-DESC-4038-CH2A12 TO LONG-DESC-4038-CH28 OF
                FCHWM28(0012)
           END-IF.
           MOVE WK01-DESC-4038-CH2A12-F TO WK01-DESC-4038-CH2A-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-DATA-FLAG-4038-CH281-Z = 'Y'
               MOVE WK01-DATA-FLAG-4038-CH281 TO
                RQRD-DATA-FLAG-4038-CH28 OF FCHWM28(0001)
           END-IF.
           MOVE WK01-DATA-FLAG-4038-CH281-F TO
                WK01-DATA-FLAG-4038-CH28-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DATA-FLAG-4038-CH282-Z = 'Y'
               MOVE WK01-DATA-FLAG-4038-CH282 TO
                RQRD-DATA-FLAG-4038-CH28 OF FCHWM28(0002)
           END-IF.
           MOVE WK01-DATA-FLAG-4038-CH282-F TO
                WK01-DATA-FLAG-4038-CH28-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DATA-FLAG-4038-CH283-Z = 'Y'
               MOVE WK01-DATA-FLAG-4038-CH283 TO
                RQRD-DATA-FLAG-4038-CH28 OF FCHWM28(0003)
           END-IF.
           MOVE WK01-DATA-FLAG-4038-CH283-F TO
                WK01-DATA-FLAG-4038-CH28-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DATA-FLAG-4038-CH284-Z = 'Y'
               MOVE WK01-DATA-FLAG-4038-CH284 TO
                RQRD-DATA-FLAG-4038-CH28 OF FCHWM28(0004)
           END-IF.
           MOVE WK01-DATA-FLAG-4038-CH284-F TO
                WK01-DATA-FLAG-4038-CH28-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DATA-FLAG-4038-CH285-Z = 'Y'
               MOVE WK01-DATA-FLAG-4038-CH285 TO
                RQRD-DATA-FLAG-4038-CH28 OF FCHWM28(0005)
           END-IF.
           MOVE WK01-DATA-FLAG-4038-CH285-F TO
                WK01-DATA-FLAG-4038-CH28-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DATA-FLAG-4038-CH286-Z = 'Y'
               MOVE WK01-DATA-FLAG-4038-CH286 TO
                RQRD-DATA-FLAG-4038-CH28 OF FCHWM28(0006)
           END-IF.
           MOVE WK01-DATA-FLAG-4038-CH286-F TO
                WK01-DATA-FLAG-4038-CH28-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DATA-FLAG-4038-CH287-Z = 'Y'
               MOVE WK01-DATA-FLAG-4038-CH287 TO
                RQRD-DATA-FLAG-4038-CH28 OF FCHWM28(0007)
           END-IF.
           MOVE WK01-DATA-FLAG-4038-CH287-F TO
                WK01-DATA-FLAG-4038-CH28-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DATA-FLAG-4038-CH288-Z = 'Y'
               MOVE WK01-DATA-FLAG-4038-CH288 TO
                RQRD-DATA-FLAG-4038-CH28 OF FCHWM28(0008)
           END-IF.
           MOVE WK01-DATA-FLAG-4038-CH288-F TO
                WK01-DATA-FLAG-4038-CH28-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DATA-FLAG-4038-CH289-Z = 'Y'
               MOVE WK01-DATA-FLAG-4038-CH289 TO
                RQRD-DATA-FLAG-4038-CH28 OF FCHWM28(0009)
           END-IF.
           MOVE WK01-DATA-FLAG-4038-CH289-F TO
                WK01-DATA-FLAG-4038-CH28-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DATA-FLAG-4038-CH2810-Z = 'Y'
               MOVE WK01-DATA-FLAG-4038-CH2810 TO
                RQRD-DATA-FLAG-4038-CH28 OF FCHWM28(0010)
           END-IF.
           MOVE WK01-DATA-FLAG-4038-CH2810-F TO
                WK01-DATA-FLAG-4038-CH28-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-DATA-FLAG-4038-CH2811-Z = 'Y'
               MOVE WK01-DATA-FLAG-4038-CH2811 TO
                RQRD-DATA-FLAG-4038-CH28 OF FCHWM28(0011)
           END-IF.
           MOVE WK01-DATA-FLAG-4038-CH2811-F TO
                WK01-DATA-FLAG-4038-CH28-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-DATA-FLAG-4038-CH2812-Z = 'Y'
               MOVE WK01-DATA-FLAG-4038-CH2812 TO
                RQRD-DATA-FLAG-4038-CH28 OF FCHWM28(0012)
           END-IF.
           MOVE WK01-DATA-FLAG-4038-CH2812-F TO
                WK01-DATA-FLAG-4038-CH28-F (12).
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-CH281-Z = 'Y'
               MOVE WK01-TEXT-CH281 TO STD-TEXT-CH28 OF FCHWM28(0001)
           END-IF.
           MOVE WK01-TEXT-CH281-F TO WK01-TEXT-CH28-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-CH282-Z = 'Y'
               MOVE WK01-TEXT-CH282 TO STD-TEXT-CH28 OF FCHWM28(0002)
           END-IF.
           MOVE WK01-TEXT-CH282-F TO WK01-TEXT-CH28-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-CH283-Z = 'Y'
               MOVE WK01-TEXT-CH283 TO STD-TEXT-CH28 OF FCHWM28(0003)
           END-IF.
           MOVE WK01-TEXT-CH283-F TO WK01-TEXT-CH28-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-CH284-Z = 'Y'
               MOVE WK01-TEXT-CH284 TO STD-TEXT-CH28 OF FCHWM28(0004)
           END-IF.
           MOVE WK01-TEXT-CH284-F TO WK01-TEXT-CH28-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-CH285-Z = 'Y'
               MOVE WK01-TEXT-CH285 TO STD-TEXT-CH28 OF FCHWM28(0005)
           END-IF.
           MOVE WK01-TEXT-CH285-F TO WK01-TEXT-CH28-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-CH286-Z = 'Y'
               MOVE WK01-TEXT-CH286 TO STD-TEXT-CH28 OF FCHWM28(0006)
           END-IF.
           MOVE WK01-TEXT-CH286-F TO WK01-TEXT-CH28-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-CH287-Z = 'Y'
               MOVE WK01-TEXT-CH287 TO STD-TEXT-CH28 OF FCHWM28(0007)
           END-IF.
           MOVE WK01-TEXT-CH287-F TO WK01-TEXT-CH28-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-CH288-Z = 'Y'
               MOVE WK01-TEXT-CH288 TO STD-TEXT-CH28 OF FCHWM28(0008)
           END-IF.
           MOVE WK01-TEXT-CH288-F TO WK01-TEXT-CH28-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-CH289-Z = 'Y'
               MOVE WK01-TEXT-CH289 TO STD-TEXT-CH28 OF FCHWM28(0009)
           END-IF.
           MOVE WK01-TEXT-CH289-F TO WK01-TEXT-CH28-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-CH2810-Z = 'Y'
               MOVE WK01-TEXT-CH2810 TO STD-TEXT-CH28 OF FCHWM28(0010)
           END-IF.
           MOVE WK01-TEXT-CH2810-F TO WK01-TEXT-CH28-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-CH2811-Z = 'Y'
               MOVE WK01-TEXT-CH2811 TO STD-TEXT-CH28 OF FCHWM28(0011)
           END-IF.
           MOVE WK01-TEXT-CH2811-F TO WK01-TEXT-CH28-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-CH2812-Z = 'Y'
               MOVE WK01-TEXT-CH2812 TO STD-TEXT-CH28 OF FCHWM28(0012)
           END-IF.
           MOVE WK01-TEXT-CH2812-F TO WK01-TEXT-CH28-F (12).
