       01  MAP-CONTROL.

    ***Created by Convert/DC version V8R03 on 11/01/00 at 08:00***

           02  FCHMI91-MAP-CONTROL.
           03  FCHMI91T.
           04  WK01-DCXMAPL-PARMS.
               10  WK01-MAPNAME                  PIC X(8)
                                                 VALUE 'FCHMI91I'.
               10  WK01-IKE-LENGTH               PIC S9(8)  COMP
                                                 VALUE 8919.
               10  WK01-TINA-LENGTH              PIC S9(4)  COMP
                                                 VALUE 892.
           05  WK01-ATTRIBUTES                   PIC X(60)
                                                 VALUE SPACE.
           05  WK01-ATTRIBUTES-ERROR             PIC X(60) VALUE
               'U  BIF'.
           05  WK01-CURSOR-DFLD                  PIC X(36)
                                                 VALUE SPACES.
           05  WK01-CURSOR-DFLD-QUAL             PIC X(30)
                                                 VALUE SPACES.
           05  WK01-ROW                          PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-COLUMN                       PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-ERROR-MSG                    PIC X(80)
                                                 VALUE SPACES.
           05  WK01-MAP-DATA-LEN                 PIC S9(6)
                                                 VALUE ZERO.
           05  WK01-1ST-FLAG                     PIC X(1)
                                                 VALUE 'Y'.
           05  WK01-FLAG-OUTPUT-DATA             PIC X(1)
                                                 VALUE 'Y'.
      *    05  WK01-STATUS-RETURN-FLAG           PIC X(1)
      *                                          VALUE SPACES.
           05  WK01-ALL-BUT-FLAG                 PIC X(1)
                                                 VALUE SPACES.
           05  WK01-FLAG-ALL-FIELDS-CHANGED      PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-CHANGED      PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERROR        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERROR        PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.

           05  WK01-VARDATA                      PIC X(11880)
                                                 VALUE SPACES.
           05  FILLER                            PIC X(100)
                                                 VALUE SPACES.
           04  WK01-ELEMENT-AREA.

               10  WK01-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-6001-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00       PIC X(35)
                                               VALUE SPACES.
               10  WK01-DATE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-HH-MM-TIME-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00       PIC X(4)
                                               VALUE SPACES.
               10  WK01-PM-FLAG-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-USER-ID-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID       PIC X(32)
                                               VALUE SPACES.
               10  WK01-RSPNS-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-FUNC-DESCRIPTION-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION       PIC X(28)
                                               VALUE SPACES.
               10  WK01-STATUS-CODE-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-CODE-4001-CH91-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4001-CH91-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4001-CH91-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4001-CH91-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4001-CH91-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4001-CH91       PIC X(1)
                                               VALUE SPACES.
               10  WK01-CODE-4001-CH9A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4001-CH9A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4001-CH9A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4001-CH9A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4001-CH9A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4001-CH9A       PIC X(6)
                                               VALUE SPACES.
               10  WK01-TITLE-4005-CH91-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4005-CH91-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4005-CH91-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4005-CH91-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4005-CH91-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4005-CH91       PIC X(35)
                                               VALUE SPACES.
               10  WK01-CODE-4005-CH91-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4005-CH91-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4005-CH91-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4005-CH91-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4005-CH91-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4005-CH91       PIC X(6)
                                               VALUE SPACES.
               10  WK01-TITLE-4005-CH9A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4005-CH9A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4005-CH9A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4005-CH9A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4005-CH9A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4005-CH9A       PIC X(35)
                                               VALUE SPACES.
               10  WK01-TITLE-4012-CH911-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH911-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH911-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH911-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH911-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH911       PIC X(20)
                                               VALUE SPACES.
               10  WK01-FUND-4001-CH911-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH911-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH911-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH911-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH911-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH911       PIC X(6)
                                               VALUE SPACES.
               10  WK01-FUND-TITLE-4005-CH911-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH911-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH911-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH911-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH911-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH911       PIC X(35)
                                               VALUE SPACES.
               10  WK01-TITLE-4012-CH912-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH912-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH912-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH912-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH912-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH912       PIC X(20)
                                               VALUE SPACES.
               10  WK01-FUND-4001-CH912-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH912-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH912-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH912-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH912-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH912       PIC X(6)
                                               VALUE SPACES.
               10  WK01-FUND-TITLE-4005-CH912-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH912-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH912-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH912-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH912-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH912       PIC X(35)
                                               VALUE SPACES.
               10  WK01-TITLE-4012-CH913-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH913-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH913-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH913-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH913-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH913       PIC X(20)
                                               VALUE SPACES.
               10  WK01-FUND-4001-CH913-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH913-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH913-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH913-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH913-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH913       PIC X(6)
                                               VALUE SPACES.
               10  WK01-FUND-TITLE-4005-CH913-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH913-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH913-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH913-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH913-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH913       PIC X(35)
                                               VALUE SPACES.
               10  WK01-TITLE-4012-CH914-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH914-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH914-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH914-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH914-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH914       PIC X(20)
                                               VALUE SPACES.
               10  WK01-FUND-4001-CH914-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH914-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH914-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH914-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH914-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH914       PIC X(6)
                                               VALUE SPACES.
               10  WK01-FUND-TITLE-4005-CH914-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH914-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH914-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH914-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH914-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH914       PIC X(35)
                                               VALUE SPACES.
               10  WK01-TITLE-4012-CH915-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH915-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH915-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH915-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH915-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4012-CH915       PIC X(20)
                                               VALUE SPACES.
               10  WK01-FUND-4001-CH915-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH915-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH915-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH915-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH915-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-4001-CH915       PIC X(6)
                                               VALUE SPACES.
               10  WK01-FUND-TITLE-4005-CH915-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH915-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH915-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH915-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH915-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUND-TITLE-4005-CH915       PIC X(35)
                                               VALUE SPACES.
               10  WK01-ZZMESSAGE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE       PIC X(160)
                                               VALUE SPACES.
               10  WK01-PASSED-ONE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE       PIC X(32)
                                               VALUE SPACES.
               10  WK01-MAP-RESPONSE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-TEXT-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00       PIC X(14)
                                               VALUE SPACES.
           04  WK01-SUBSCRIPTED-WORK-AREA.
               05  FILLER                      PIC X(1).
               05  WK01-TITLE-4012-CH91-F             PIC X(1)
                                               OCCURS 5.
               05  WK01-FUND-4001-CH91-F             PIC X(1)
                                               OCCURS 5.
               05  WK01-FUND-TITLE-4005-CH91-F             PIC X(1)
                                               OCCURS 5.
