      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWK81                              04/24/00  12:19  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWK81.
           02  RQST-COA-CODE-CH81                       PIC X(1).
           02  SLCTN-DATE-CH81                          PIC X(6).
           02  SLCTN-STATUS-CH81                        PIC X(1).
           02  RQST-ACCT-TYPE-CODE-CH81                 PIC X(2).
