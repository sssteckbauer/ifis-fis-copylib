    ***Created by Convert/DC version V8R03 on 12/11/00 at 13:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE RPRT-CODE-4060-PU62 OF FPUWK62 TO WK01-CODE-4060-PU62.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-PU62 OF FPUWM62 TO WK01-CODE-PU62.
      *%--------------------------------------------------------------%*
           MOVE USER-ID-4058-PU62 OF FPUWK62 TO WK01-ID-4058-PU62.
      *%--------------------------------------------------------------%*
           MOVE SEQ-NMBR-4058-PU62 OF FPUWM62 TO WK01-NMBR-4058-PU62.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-DATE-4058-PU62 OF FPUWM62 TO WK01-DATE-4058-PU62.
      *%--------------------------------------------------------------%*
           MOVE AS-OF-DATE-4058-PU62 OF FPUWM62 TO
                WK01-OF-DATE-4058-PU62.
      *%--------------------------------------------------------------%*
           MOVE DLT-IND-4058-PU62 OF FPUWM62 TO WK01-IND-4058-PU62.
      *%--------------------------------------------------------------%*
           MOVE HOLD-IND-4058-PU62 OF FPUWM62 TO WK01-IND-4058-PU6A.
      *%--------------------------------------------------------------%*
           MOVE PO-NMBR-4058-PU62 OF FPUWM62 TO WK01-NMBR-4058-PU6A.
      *%--------------------------------------------------------------%*
           MOVE ORGZN-CODE-4058-PU62 OF FPUWM62 TO WK01-CODE-4058-PU62.
      *%--------------------------------------------------------------%*
           MOVE RQST-CODE-4058-PU62 OF FPUWM62 TO WK01-CODE-4058-PU6A.
      *%--------------------------------------------------------------%*
           MOVE CMDTY-CODE-4058-PU62 OF FPUWM62 TO WK01-CODE-4058-PU6B.
      *%--------------------------------------------------------------%*
           MOVE BID-NMBR-4058-PU62 OF FPUWM62 TO WK01-NMBR-4058-PU6B.
      *%--------------------------------------------------------------%*
           MOVE VNDR-ID-LAST-NINE-PU62 OF FPUWM62 TO
                WK01-ID-LAST-NINE-PU62.
      *%--------------------------------------------------------------%*
           MOVE RQST-SEQ-NMBR-PU62 OF FPUWK62 TO WK01-SEQ-NMBR-PU62.
      *%--------------------------------------------------------------%*
           MOVE VNDR-TYPE-CODE-4058-PU62 OF FPUWM62(0001) TO
                WK01-TYPE-CODE-4058-PU621.
      *%--------------------------------------------------------------%*
           MOVE VNDR-TYPE-CODE-4058-PU62 OF FPUWM62(0002) TO
                WK01-TYPE-CODE-4058-PU622.
      *%--------------------------------------------------------------%*
           MOVE VNDR-TYPE-CODE-4058-PU62 OF FPUWM62(0003) TO
                WK01-TYPE-CODE-4058-PU623.
      *%--------------------------------------------------------------%*
           MOVE VNDR-TYPE-CODE-4058-PU62 OF FPUWM62(0004) TO
                WK01-TYPE-CODE-4058-PU624.
      *%--------------------------------------------------------------%*
           MOVE VNDR-TYPE-CODE-4058-PU62 OF FPUWM62(0005) TO
                WK01-TYPE-CODE-4058-PU625.
      *%--------------------------------------------------------------%*
           MOVE VNDR-TYPE-CODE-4058-PU62 OF FPUWM62(0006) TO
                WK01-TYPE-CODE-4058-PU626.
      *%--------------------------------------------------------------%*
           MOVE VNDR-TYPE-CODE-4058-PU62 OF FPUWM62(0007) TO
                WK01-TYPE-CODE-4058-PU627.
      *%--------------------------------------------------------------%*
           MOVE VNDR-TYPE-CODE-4058-PU62 OF FPUWM62(0008) TO
                WK01-TYPE-CODE-4058-PU628.
      *%--------------------------------------------------------------%*
           MOVE VNDR-TYPE-CODE-4058-PU62 OF FPUWM62(0009) TO
                WK01-TYPE-CODE-4058-PU629.
      *%--------------------------------------------------------------%*
           MOVE VNDR-TYPE-CODE-4058-PU62 OF FPUWM62(0010) TO
                WK01-TYPE-CODE-4058-PU6210.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-4058-PU62 OF FPUWM62 TO WK01-CODE-4058-PU6C.
      *%--------------------------------------------------------------%*
           MOVE CHNG-SEQ-NMBR-4058-PU62 OF FPUWM62 TO
                WK01-SEQ-NMBR-4058-PU62.
      *%--------------------------------------------------------------%*
           MOVE RPRT-TITLE-4060-PU62 OF FPUWM62 TO WK01-TITLE-4060-PU62.
      *%--------------------------------------------------------------%*
           MOVE START-DATE-PU62 OF FPUWM62 TO WK01-DATE-PU62.
