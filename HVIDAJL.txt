       975-TO-R4106-AUTO-JRNL SECTION.
           MOVE AJL-USER-CD TO
               USER-CODE-4106 OF R4106-AUTO-JRNL
                                                                     .
           MOVE AJL-LAST-ACTVY-DT TO
               LAST-ACTVY-DATE-4106 OF R4106-AUTO-JRNL
                                                                     .
           MOVE AJL-UNVRS-CD TO
               UNVRS-CODE-4106 OF R4106-AUTO-JRNL
                                                                     .
           MOVE AJL-AUTO-JRNL-ID TO
               AUTO-JRNL-ID-4106 OF R4106-AUTO-JRNL
                                                                     .
           MOVE AJL-ACTVY-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '1' TO DBLINKX-DB2-DATE-FORMAT
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               ACTVY-DATE-4106 OF R4106-AUTO-JRNL
                                                                     .
           MOVE AJL-AUTO-JRNL-TTL TO
               AUTO-JRNL-TITLE-4106 OF R4106-AUTO-JRNL
                                                                     .
           MOVE AJL-RVRSL-IND TO
               RVRSL-IND-4106 OF R4106-AUTO-JRNL
                                                                     .
           MOVE AJL-RVRSL-LAST-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               RVRSL-LAST-DATE-4106 OF R4106-AUTO-JRNL
                                                                     .
           MOVE AJL-NEXT-SUB-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               NEXT-SUB-DATE-4106 OF R4106-AUTO-JRNL
                                                                     .
           MOVE AJL-LAST-SUB-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               LAST-SUB-DATE-4106 OF R4106-AUTO-JRNL
                                                                     .
           MOVE AJL-SUB-TOTAL TO
               SUB-TOTAL-4106 OF R4106-AUTO-JRNL
                                                                     .
           MOVE AJL-SUB-RMNG TO
               SUB-RMNG-4106 OF R4106-AUTO-JRNL
                                                                     .
           MOVE AJL-SUB-CYCLE-IND TO
               SUB-CYCLE-IND-4106 OF R4106-AUTO-JRNL
                                                                     .
           MOVE AJL-SUB-CMPLT TO
               SUB-CMPLT-4106 OF R4106-AUTO-JRNL
                                                                     .
           MOVE AJL-SUB-SCHDL-DAY TO
               SUB-SCHDL-DAY-4106 OF R4106-AUTO-JRNL
                                                                     .
           MOVE AJL-SUB-SCHDL-PRD TO
               SUB-SCHDL-PRD-4106 OF R4106-AUTO-JRNL
                                                                     .
           MOVE AJL-NEXT-RVRSL-DT TO
               DBLINKX-INPUT-PARM
           MOVE '2' TO DBLINKX-REQUEST-CODE
           MOVE '01' TO DBLINKX-IDMS-DATE-FORMAT
           CALL 'IDDTECNV' USING DBLINKX-DATE-TIME-PARMS
           MOVE DBLINKX-OUTPUT-08 TO
               NEXT-RVRSL-DATE-4106 OF R4106-AUTO-JRNL
                                                                     .
           MOVE AJL-DSTBN-IND TO
               DSTBN-IND-4106 OF R4106-AUTO-JRNL
                                                                     .
           MOVE AJL-RVRSL-SUB-IND TO
               RVRSL-SUB-IND-4106 OF R4106-AUTO-JRNL
                                                                     .
           MOVE AJL-COA-CD TO
               COA-CODE-4106 OF R4106-AUTO-JRNL
                                                                     .
           MOVE AJL-FSCL-YR TO
               FSCL-YR-4106 OF R4106-AUTO-JRNL
                                                                     .
           MOVE AJL-ERROR-CNTR TO
               ERROR-CNTR-4106 OF R4106-AUTO-JRNL
                                                                     .
       975-TO-R4106-AUTO-JRNL-EXIT.
           EXIT.
