      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM25                              04/24/00  12:23  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM25.
           02  ACTN-CODE-GROUP-CH25.
               03  ACTN-CODE-CH25                       PIC X(1).
           02  ACCT-TYPE-DESC-4015-CH25                 PIC X(35).
           02  NEXT-CHNG-DATE-4015-CH25                 PIC X(6).
           02  STATUS-DESC-CH25                         PIC X(8).
           02  END-DATE-4015-CH25                       PIC X(6).
           02  ACTVY-DATE-4014-CH25                     PIC X(6).
           02  TIME-STAMP-4015-CH25                     PIC X(6).
           02  AM-PM-FLAG-CH25                          PIC X(1).
           02  INTRL-ACCT-TYPE-CODE-4015-CH25           PIC X(2).
           02  INTRL-ACCT-DESC-4012-CH25                PIC X(35).
           02  PREDCSR-ACCT-TYPE-4015-CH25              PIC X(2).
           02  PREDCSR-ACCT-DESC-4015-CH25              PIC X(35).
           02  NRML-BAL-IND-4015-CH25                   PIC X(1).
           02  NRML-BAL-IND-DESC-4012-CH25              PIC X(35).
