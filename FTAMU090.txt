    ***Created by Convert/DC version V8R03 on 12/11/00 at 12:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY01-Z = 'Y'
               MOVE WK01-TEXT-SY01 TO DCSD-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0A-Z = 'Y'
               MOVE WK01-TEXT-SY0A TO CNFDL-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY0B-Z = 'Y'
               MOVE WK01-TEXT-SY0B TO HOLDS-TEXT-SY01 OF FSYWG01M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-TA09-Z = 'Y'
               MOVE WK01-CODE-TA09 TO ACTN-CODE-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-LIT-TA09-Z = 'Y'
               MOVE WK01-LIT-TA09 TO DELETE-LIT-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DELETE-TA09-Z = 'Y'
               MOVE WK01-DELETE-TA09 TO CONFIRM-DELETE-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4410-TA09-Z = 'Y'
               MOVE WK01-NMBR-4410-TA09 TO DCMNT-NMBR-4410-TA09 OF
                FTAWK09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-LIT-TA0A-Z = 'Y'
               MOVE WK01-LIT-TA0A TO CHECK-LIT-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CHECK-TA09-Z = 'Y'
               MOVE WK01-CHECK-TA09 TO CONFIRM-CHECK-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NIN-4073-T09-Z = 'Y'
               MOVE WK01-ID-LAST-NIN-4073-T09 TO
                VNDR-ID-LAST-NINE-4073-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6117-TA09-Z = 'Y'
               MOVE WK01-KEY-6117-TA09 TO NAME-KEY-6117-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PAD-HLD-IND-4410-T09-Z = 'Y'
               MOVE WK01-PAD-HLD-IND-4410-T09 TO
                OPEN-PAID-HOLD-IND-4410-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4410-TA09-Z = 'Y'
               MOVE WK01-AMT-4410-TA09 TO INVD-AMT-4410-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INV-NMBR-4410-TA09-Z = 'Y'
               MOVE WK01-INV-NMBR-4410-TA09 TO VNDR-INV-NMBR-4410-TA09
                OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MEMO-4410-TA09-Z = 'Y'
               MOVE WK01-MEMO-4410-TA09 TO CRDT-MEMO-4410-TA09 OF
                FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4410-TA09-Z = 'Y'
               MOVE WK01-DATE-4410-TA09 TO INV-DATE-4410-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DUE-DATE-4410-TA09-Z = 'Y'
               MOVE WK01-DUE-DATE-4410-TA09 TO PYMT-DUE-DATE-4410-TA09
                OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-DESC-TA09-Z = 'Y'
               MOVE WK01-TYPE-DESC-TA09 TO BILL-TYPE-DESC-TA09 OF
                FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACCT-CODE-4033-TA09-Z = 'Y'
               MOVE WK01-ACCT-CODE-4033-TA09 TO
                BANK-ACCT-CODE-4033-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-DESC-4062-TA09-Z = 'Y'
               MOVE WK01-CODE-DESC-4062-TA09 TO
                BANK-CODE-DESC-4062-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-CODE-6139-TA09-Z = 'Y'
               MOVE WK01-TYPE-CODE-6139-TA09 TO ADR-TYPE-CODE-6139-TA09
                OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-6137-TA09-Z = 'Y'
               MOVE WK01-DESC-6137-TA09 TO SHORT-DESC-6137-TA09 OF
                FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4410-TA09-Z = 'Y'
               MOVE WK01-IND-4410-TA09 TO APRVL-IND-4410-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ADR-6139-TA091-Z = 'Y'
               MOVE WK01-ADR-6139-TA091 TO LINE-ADR-6139-TA09 OF
                FTAWM09(0001)
           END-IF.
           MOVE WK01-ADR-6139-TA091-F TO WK01-ADR-6139-TA09-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-IND-4410-TA0A-Z = 'Y'
               MOVE WK01-IND-4410-TA0A TO CNCL-IND-4410-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ADR-6139-TA092-Z = 'Y'
               MOVE WK01-ADR-6139-TA092 TO LINE-ADR-6139-TA09 OF
                FTAWM09(0002)
           END-IF.
           MOVE WK01-ADR-6139-TA092-F TO WK01-ADR-6139-TA09-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4410-TA0A-Z = 'Y'
               MOVE WK01-DATE-4410-TA0A TO CNCL-DATE-4410-TA09 OF
                FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ADR-6139-TA093-Z = 'Y'
               MOVE WK01-ADR-6139-TA093 TO LINE-ADR-6139-TA09 OF
                FTAWM09(0003)
           END-IF.
           MOVE WK01-ADR-6139-TA093-F TO WK01-ADR-6139-TA09-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-ERROR-IND-4410-TA09-Z = 'Y'
               MOVE WK01-ERROR-IND-4410-TA09 TO HDR-ERROR-IND-4410-TA09
                OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ADR-6139-TA094-Z = 'Y'
               MOVE WK01-ADR-6139-TA094 TO LINE-ADR-6139-TA09 OF
                FTAWM09(0004)
           END-IF.
           MOVE WK01-ADR-6139-TA094-F TO WK01-ADR-6139-TA09-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CNTR-DTL-4410-TA09-Z = 'Y'
              IF WK01-CNTR-DTL-4410-TA09 NOT NUMERIC
               MOVE WK01-CNTR-DTL-4410-TA09-X TO HDR-CNTR-DTL-4410-TA09
                OF FTAWM09
           ELSE
               MOVE WK01-CNTR-DTL-4410-TA09 TO HDR-CNTR-DTL-4410-TA09
                OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6139-TA09-Z = 'Y'
               MOVE WK01-NAME-6139-TA09 TO CITY-NAME-6139-TA09 OF
                FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6151-TA09-Z = 'Y'
               MOVE WK01-CODE-6151-TA09 TO STATE-CODE-6151-TA09 OF
                FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6152-TA09-Z = 'Y'
               MOVE WK01-CODE-6152-TA09 TO ZIP-CODE-6152-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-6153-TA09-Z = 'Y'
               MOVE WK01-CODE-6153-TA09 TO CNTRY-CODE-6153-TA09 OF
                FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4410-TA0B-Z = 'Y'
               MOVE WK01-IND-4410-TA0B TO CMPLT-IND-4410-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-GRPNG-IND-4410-TA09-Z = 'Y'
               MOVE WK01-GRPNG-IND-4410-TA09 TO
                CHECK-GRPNG-IND-4410-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TYPE-IND-4410-TA09-Z = 'Y'
               MOVE WK01-TYPE-IND-4410-TA09 TO BILL-TYPE-IND-4410-TA09
                OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INV-DATE-4410-TA09-Z = 'Y'
               MOVE WK01-INV-DATE-4410-TA09 TO VNDR-INV-DATE-4410-TA09
                OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TMPLT-CODE-TA09-Z = 'Y'
               MOVE WK01-TMPLT-CODE-TA09 TO APRVL-TMPLT-CODE-TA09 OF
                FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ADR-TYPE-TA09-Z = 'Y'
               MOVE WK01-ADR-TYPE-TA09 TO ACH-ADR-TYPE-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-IND-TA09-Z = 'Y'
               MOVE WK01-TEXT-IND-TA09 TO CHECK-TEXT-IND-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-IND-TA0A-Z = 'Y'
               MOVE WK01-TEXT-IND-TA0A TO DOCT-TEXT-IND-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-TA09-Z = 'Y'
               MOVE WK01-NMBR-TA09 TO CHECK-NMBR-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-TA09-Z = 'Y'
               MOVE WK01-DATE-TA09 TO CHECK-DATE-TA09 OF FTAWM09
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-TA09-Z = 'Y'
               MOVE WK01-STATUS-TA09 TO CHECK-STATUS-TA09 OF FTAWM09
           END-IF.
