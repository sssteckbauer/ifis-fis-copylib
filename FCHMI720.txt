    ***Created by Convert/DC version V8R03 on 12/11/00 at 09:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH721-Z = 'Y'
               MOVE WK01-CODE-4050-CH721 TO COA-CODE-4050-CH72 OF
                FCHWM72(0001)
           END-IF.
           MOVE WK01-CODE-4050-CH721-F TO WK01-CODE-4050-CH72-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH7A1-Z = 'Y'
               MOVE WK01-CODE-4050-CH7A1 TO ACTVY-CODE-4050-CH72 OF
                FCHWM72(0001)
           END-IF.
           MOVE WK01-CODE-4050-CH7A1-F TO WK01-CODE-4050-CH7A-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4066-CH721-Z = 'Y'
               MOVE WK01-TITLE-4066-CH721 TO ACTVY-TITLE-4066-CH72 OF
                FCHWM72(0001)
           END-IF.
           MOVE WK01-TITLE-4066-CH721-F TO WK01-TITLE-4066-CH72-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-4066-CH721-Z = 'Y'
               MOVE WK01-4066-CH721 TO STATUS-4066-CH72 OF FCHWM72(0001)
           END-IF.
           MOVE WK01-4066-CH721-F TO WK01-4066-CH72-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH721-Z = 'Y'
               MOVE WK01-DATE-4066-CH721 TO START-DATE-4066-CH72 OF
                FCHWM72(0001)
           END-IF.
           MOVE WK01-DATE-4066-CH721-F TO WK01-DATE-4066-CH72-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH7A1-Z = 'Y'
               MOVE WK01-DATE-4066-CH7A1 TO END-DATE-4066-CH72 OF
                FCHWM72(0001)
           END-IF.
           MOVE WK01-DATE-4066-CH7A1-F TO WK01-DATE-4066-CH7A-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH722-Z = 'Y'
               MOVE WK01-CODE-4050-CH722 TO COA-CODE-4050-CH72 OF
                FCHWM72(0002)
           END-IF.
           MOVE WK01-CODE-4050-CH722-F TO WK01-CODE-4050-CH72-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH723-Z = 'Y'
               MOVE WK01-CODE-4050-CH723 TO COA-CODE-4050-CH72 OF
                FCHWM72(0003)
           END-IF.
           MOVE WK01-CODE-4050-CH723-F TO WK01-CODE-4050-CH72-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH724-Z = 'Y'
               MOVE WK01-CODE-4050-CH724 TO COA-CODE-4050-CH72 OF
                FCHWM72(0004)
           END-IF.
           MOVE WK01-CODE-4050-CH724-F TO WK01-CODE-4050-CH72-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH725-Z = 'Y'
               MOVE WK01-CODE-4050-CH725 TO COA-CODE-4050-CH72 OF
                FCHWM72(0005)
           END-IF.
           MOVE WK01-CODE-4050-CH725-F TO WK01-CODE-4050-CH72-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH726-Z = 'Y'
               MOVE WK01-CODE-4050-CH726 TO COA-CODE-4050-CH72 OF
                FCHWM72(0006)
           END-IF.
           MOVE WK01-CODE-4050-CH726-F TO WK01-CODE-4050-CH72-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH727-Z = 'Y'
               MOVE WK01-CODE-4050-CH727 TO COA-CODE-4050-CH72 OF
                FCHWM72(0007)
           END-IF.
           MOVE WK01-CODE-4050-CH727-F TO WK01-CODE-4050-CH72-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH728-Z = 'Y'
               MOVE WK01-CODE-4050-CH728 TO COA-CODE-4050-CH72 OF
                FCHWM72(0008)
           END-IF.
           MOVE WK01-CODE-4050-CH728-F TO WK01-CODE-4050-CH72-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH729-Z = 'Y'
               MOVE WK01-CODE-4050-CH729 TO COA-CODE-4050-CH72 OF
                FCHWM72(0009)
           END-IF.
           MOVE WK01-CODE-4050-CH729-F TO WK01-CODE-4050-CH72-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH7210-Z = 'Y'
               MOVE WK01-CODE-4050-CH7210 TO COA-CODE-4050-CH72 OF
                FCHWM72(0010)
           END-IF.
           MOVE WK01-CODE-4050-CH7210-F TO WK01-CODE-4050-CH72-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH7A2-Z = 'Y'
               MOVE WK01-CODE-4050-CH7A2 TO ACTVY-CODE-4050-CH72 OF
                FCHWM72(0002)
           END-IF.
           MOVE WK01-CODE-4050-CH7A2-F TO WK01-CODE-4050-CH7A-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH7A3-Z = 'Y'
               MOVE WK01-CODE-4050-CH7A3 TO ACTVY-CODE-4050-CH72 OF
                FCHWM72(0003)
           END-IF.
           MOVE WK01-CODE-4050-CH7A3-F TO WK01-CODE-4050-CH7A-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH7A4-Z = 'Y'
               MOVE WK01-CODE-4050-CH7A4 TO ACTVY-CODE-4050-CH72 OF
                FCHWM72(0004)
           END-IF.
           MOVE WK01-CODE-4050-CH7A4-F TO WK01-CODE-4050-CH7A-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH7A5-Z = 'Y'
               MOVE WK01-CODE-4050-CH7A5 TO ACTVY-CODE-4050-CH72 OF
                FCHWM72(0005)
           END-IF.
           MOVE WK01-CODE-4050-CH7A5-F TO WK01-CODE-4050-CH7A-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH7A6-Z = 'Y'
               MOVE WK01-CODE-4050-CH7A6 TO ACTVY-CODE-4050-CH72 OF
                FCHWM72(0006)
           END-IF.
           MOVE WK01-CODE-4050-CH7A6-F TO WK01-CODE-4050-CH7A-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH7A7-Z = 'Y'
               MOVE WK01-CODE-4050-CH7A7 TO ACTVY-CODE-4050-CH72 OF
                FCHWM72(0007)
           END-IF.
           MOVE WK01-CODE-4050-CH7A7-F TO WK01-CODE-4050-CH7A-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH7A8-Z = 'Y'
               MOVE WK01-CODE-4050-CH7A8 TO ACTVY-CODE-4050-CH72 OF
                FCHWM72(0008)
           END-IF.
           MOVE WK01-CODE-4050-CH7A8-F TO WK01-CODE-4050-CH7A-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH7A9-Z = 'Y'
               MOVE WK01-CODE-4050-CH7A9 TO ACTVY-CODE-4050-CH72 OF
                FCHWM72(0009)
           END-IF.
           MOVE WK01-CODE-4050-CH7A9-F TO WK01-CODE-4050-CH7A-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH7A10-Z = 'Y'
               MOVE WK01-CODE-4050-CH7A10 TO ACTVY-CODE-4050-CH72 OF
                FCHWM72(0010)
           END-IF.
           MOVE WK01-CODE-4050-CH7A10-F TO WK01-CODE-4050-CH7A-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4066-CH722-Z = 'Y'
               MOVE WK01-TITLE-4066-CH722 TO ACTVY-TITLE-4066-CH72 OF
                FCHWM72(0002)
           END-IF.
           MOVE WK01-TITLE-4066-CH722-F TO WK01-TITLE-4066-CH72-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4066-CH723-Z = 'Y'
               MOVE WK01-TITLE-4066-CH723 TO ACTVY-TITLE-4066-CH72 OF
                FCHWM72(0003)
           END-IF.
           MOVE WK01-TITLE-4066-CH723-F TO WK01-TITLE-4066-CH72-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4066-CH724-Z = 'Y'
               MOVE WK01-TITLE-4066-CH724 TO ACTVY-TITLE-4066-CH72 OF
                FCHWM72(0004)
           END-IF.
           MOVE WK01-TITLE-4066-CH724-F TO WK01-TITLE-4066-CH72-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4066-CH725-Z = 'Y'
               MOVE WK01-TITLE-4066-CH725 TO ACTVY-TITLE-4066-CH72 OF
                FCHWM72(0005)
           END-IF.
           MOVE WK01-TITLE-4066-CH725-F TO WK01-TITLE-4066-CH72-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4066-CH726-Z = 'Y'
               MOVE WK01-TITLE-4066-CH726 TO ACTVY-TITLE-4066-CH72 OF
                FCHWM72(0006)
           END-IF.
           MOVE WK01-TITLE-4066-CH726-F TO WK01-TITLE-4066-CH72-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4066-CH727-Z = 'Y'
               MOVE WK01-TITLE-4066-CH727 TO ACTVY-TITLE-4066-CH72 OF
                FCHWM72(0007)
           END-IF.
           MOVE WK01-TITLE-4066-CH727-F TO WK01-TITLE-4066-CH72-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4066-CH728-Z = 'Y'
               MOVE WK01-TITLE-4066-CH728 TO ACTVY-TITLE-4066-CH72 OF
                FCHWM72(0008)
           END-IF.
           MOVE WK01-TITLE-4066-CH728-F TO WK01-TITLE-4066-CH72-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4066-CH729-Z = 'Y'
               MOVE WK01-TITLE-4066-CH729 TO ACTVY-TITLE-4066-CH72 OF
                FCHWM72(0009)
           END-IF.
           MOVE WK01-TITLE-4066-CH729-F TO WK01-TITLE-4066-CH72-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4066-CH7210-Z = 'Y'
               MOVE WK01-TITLE-4066-CH7210 TO ACTVY-TITLE-4066-CH72 OF
                FCHWM72(0010)
           END-IF.
           MOVE WK01-TITLE-4066-CH7210-F TO WK01-TITLE-4066-CH72-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-4066-CH722-Z = 'Y'
               MOVE WK01-4066-CH722 TO STATUS-4066-CH72 OF FCHWM72(0002)
           END-IF.
           MOVE WK01-4066-CH722-F TO WK01-4066-CH72-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-4066-CH723-Z = 'Y'
               MOVE WK01-4066-CH723 TO STATUS-4066-CH72 OF FCHWM72(0003)
           END-IF.
           MOVE WK01-4066-CH723-F TO WK01-4066-CH72-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-4066-CH724-Z = 'Y'
               MOVE WK01-4066-CH724 TO STATUS-4066-CH72 OF FCHWM72(0004)
           END-IF.
           MOVE WK01-4066-CH724-F TO WK01-4066-CH72-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-4066-CH725-Z = 'Y'
               MOVE WK01-4066-CH725 TO STATUS-4066-CH72 OF FCHWM72(0005)
           END-IF.
           MOVE WK01-4066-CH725-F TO WK01-4066-CH72-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-4066-CH726-Z = 'Y'
               MOVE WK01-4066-CH726 TO STATUS-4066-CH72 OF FCHWM72(0006)
           END-IF.
           MOVE WK01-4066-CH726-F TO WK01-4066-CH72-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-4066-CH727-Z = 'Y'
               MOVE WK01-4066-CH727 TO STATUS-4066-CH72 OF FCHWM72(0007)
           END-IF.
           MOVE WK01-4066-CH727-F TO WK01-4066-CH72-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-4066-CH728-Z = 'Y'
               MOVE WK01-4066-CH728 TO STATUS-4066-CH72 OF FCHWM72(0008)
           END-IF.
           MOVE WK01-4066-CH728-F TO WK01-4066-CH72-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-4066-CH729-Z = 'Y'
               MOVE WK01-4066-CH729 TO STATUS-4066-CH72 OF FCHWM72(0009)
           END-IF.
           MOVE WK01-4066-CH729-F TO WK01-4066-CH72-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-4066-CH7210-Z = 'Y'
               MOVE WK01-4066-CH7210 TO STATUS-4066-CH72 OF
                FCHWM72(0010)
           END-IF.
           MOVE WK01-4066-CH7210-F TO WK01-4066-CH72-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH722-Z = 'Y'
               MOVE WK01-DATE-4066-CH722 TO START-DATE-4066-CH72 OF
                FCHWM72(0002)
           END-IF.
           MOVE WK01-DATE-4066-CH722-F TO WK01-DATE-4066-CH72-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH723-Z = 'Y'
               MOVE WK01-DATE-4066-CH723 TO START-DATE-4066-CH72 OF
                FCHWM72(0003)
           END-IF.
           MOVE WK01-DATE-4066-CH723-F TO WK01-DATE-4066-CH72-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH724-Z = 'Y'
               MOVE WK01-DATE-4066-CH724 TO START-DATE-4066-CH72 OF
                FCHWM72(0004)
           END-IF.
           MOVE WK01-DATE-4066-CH724-F TO WK01-DATE-4066-CH72-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH725-Z = 'Y'
               MOVE WK01-DATE-4066-CH725 TO START-DATE-4066-CH72 OF
                FCHWM72(0005)
           END-IF.
           MOVE WK01-DATE-4066-CH725-F TO WK01-DATE-4066-CH72-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH726-Z = 'Y'
               MOVE WK01-DATE-4066-CH726 TO START-DATE-4066-CH72 OF
                FCHWM72(0006)
           END-IF.
           MOVE WK01-DATE-4066-CH726-F TO WK01-DATE-4066-CH72-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH727-Z = 'Y'
               MOVE WK01-DATE-4066-CH727 TO START-DATE-4066-CH72 OF
                FCHWM72(0007)
           END-IF.
           MOVE WK01-DATE-4066-CH727-F TO WK01-DATE-4066-CH72-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH728-Z = 'Y'
               MOVE WK01-DATE-4066-CH728 TO START-DATE-4066-CH72 OF
                FCHWM72(0008)
           END-IF.
           MOVE WK01-DATE-4066-CH728-F TO WK01-DATE-4066-CH72-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH729-Z = 'Y'
               MOVE WK01-DATE-4066-CH729 TO START-DATE-4066-CH72 OF
                FCHWM72(0009)
           END-IF.
           MOVE WK01-DATE-4066-CH729-F TO WK01-DATE-4066-CH72-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH7210-Z = 'Y'
               MOVE WK01-DATE-4066-CH7210 TO START-DATE-4066-CH72 OF
                FCHWM72(0010)
           END-IF.
           MOVE WK01-DATE-4066-CH7210-F TO WK01-DATE-4066-CH72-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH7A2-Z = 'Y'
               MOVE WK01-DATE-4066-CH7A2 TO END-DATE-4066-CH72 OF
                FCHWM72(0002)
           END-IF.
           MOVE WK01-DATE-4066-CH7A2-F TO WK01-DATE-4066-CH7A-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH7A3-Z = 'Y'
               MOVE WK01-DATE-4066-CH7A3 TO END-DATE-4066-CH72 OF
                FCHWM72(0003)
           END-IF.
           MOVE WK01-DATE-4066-CH7A3-F TO WK01-DATE-4066-CH7A-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH7A4-Z = 'Y'
               MOVE WK01-DATE-4066-CH7A4 TO END-DATE-4066-CH72 OF
                FCHWM72(0004)
           END-IF.
           MOVE WK01-DATE-4066-CH7A4-F TO WK01-DATE-4066-CH7A-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH7A5-Z = 'Y'
               MOVE WK01-DATE-4066-CH7A5 TO END-DATE-4066-CH72 OF
                FCHWM72(0005)
           END-IF.
           MOVE WK01-DATE-4066-CH7A5-F TO WK01-DATE-4066-CH7A-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH7A6-Z = 'Y'
               MOVE WK01-DATE-4066-CH7A6 TO END-DATE-4066-CH72 OF
                FCHWM72(0006)
           END-IF.
           MOVE WK01-DATE-4066-CH7A6-F TO WK01-DATE-4066-CH7A-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH7A7-Z = 'Y'
               MOVE WK01-DATE-4066-CH7A7 TO END-DATE-4066-CH72 OF
                FCHWM72(0007)
           END-IF.
           MOVE WK01-DATE-4066-CH7A7-F TO WK01-DATE-4066-CH7A-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH7A8-Z = 'Y'
               MOVE WK01-DATE-4066-CH7A8 TO END-DATE-4066-CH72 OF
                FCHWM72(0008)
           END-IF.
           MOVE WK01-DATE-4066-CH7A8-F TO WK01-DATE-4066-CH7A-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH7A9-Z = 'Y'
               MOVE WK01-DATE-4066-CH7A9 TO END-DATE-4066-CH72 OF
                FCHWM72(0009)
           END-IF.
           MOVE WK01-DATE-4066-CH7A9-F TO WK01-DATE-4066-CH7A-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH7A10-Z = 'Y'
               MOVE WK01-DATE-4066-CH7A10 TO END-DATE-4066-CH72 OF
                FCHWM72(0010)
           END-IF.
           MOVE WK01-DATE-4066-CH7A10-F TO WK01-DATE-4066-CH7A-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-COA-CODE-CH72-Z = 'Y'
               MOVE WK01-COA-CODE-CH72 TO RQST-COA-CODE-CH72 OF FCHWK72
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ACTVY-CODE-CH72-Z = 'Y'
               MOVE WK01-ACTVY-CODE-CH72 TO RQST-ACTVY-CODE-CH72 OF
                FCHWK72
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH7211-Z = 'Y'
               MOVE WK01-CODE-4050-CH7211 TO COA-CODE-4050-CH72 OF
                FCHWM72(0011)
           END-IF.
           MOVE WK01-CODE-4050-CH7211-F TO WK01-CODE-4050-CH72-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4050-CH7A11-Z = 'Y'
               MOVE WK01-CODE-4050-CH7A11 TO ACTVY-CODE-4050-CH72 OF
                FCHWM72(0011)
           END-IF.
           MOVE WK01-CODE-4050-CH7A11-F TO WK01-CODE-4050-CH7A-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-TITLE-4066-CH7211-Z = 'Y'
               MOVE WK01-TITLE-4066-CH7211 TO ACTVY-TITLE-4066-CH72 OF
                FCHWM72(0011)
           END-IF.
           MOVE WK01-TITLE-4066-CH7211-F TO WK01-TITLE-4066-CH72-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-4066-CH7211-Z = 'Y'
               MOVE WK01-4066-CH7211 TO STATUS-4066-CH72 OF
                FCHWM72(0011)
           END-IF.
           MOVE WK01-4066-CH7211-F TO WK01-4066-CH72-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH7211-Z = 'Y'
               MOVE WK01-DATE-4066-CH7211 TO START-DATE-4066-CH72 OF
                FCHWM72(0011)
           END-IF.
           MOVE WK01-DATE-4066-CH7211-F TO WK01-DATE-4066-CH72-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4066-CH7A11-Z = 'Y'
               MOVE WK01-DATE-4066-CH7A11 TO END-DATE-4066-CH72 OF
                FCHWM72(0011)
           END-IF.
           MOVE WK01-DATE-4066-CH7A11-F TO WK01-DATE-4066-CH7A-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH721-Z = 'Y'
               MOVE WK01-CODE-CH721 TO ACTN-CODE-CH72 OF FCHWM72(0001)
           END-IF.
           MOVE WK01-CODE-CH721-F TO WK01-CODE-CH72-F (1).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH722-Z = 'Y'
               MOVE WK01-CODE-CH722 TO ACTN-CODE-CH72 OF FCHWM72(0002)
           END-IF.
           MOVE WK01-CODE-CH722-F TO WK01-CODE-CH72-F (2).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH723-Z = 'Y'
               MOVE WK01-CODE-CH723 TO ACTN-CODE-CH72 OF FCHWM72(0003)
           END-IF.
           MOVE WK01-CODE-CH723-F TO WK01-CODE-CH72-F (3).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH724-Z = 'Y'
               MOVE WK01-CODE-CH724 TO ACTN-CODE-CH72 OF FCHWM72(0004)
           END-IF.
           MOVE WK01-CODE-CH724-F TO WK01-CODE-CH72-F (4).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH725-Z = 'Y'
               MOVE WK01-CODE-CH725 TO ACTN-CODE-CH72 OF FCHWM72(0005)
           END-IF.
           MOVE WK01-CODE-CH725-F TO WK01-CODE-CH72-F (5).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH726-Z = 'Y'
               MOVE WK01-CODE-CH726 TO ACTN-CODE-CH72 OF FCHWM72(0006)
           END-IF.
           MOVE WK01-CODE-CH726-F TO WK01-CODE-CH72-F (6).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH727-Z = 'Y'
               MOVE WK01-CODE-CH727 TO ACTN-CODE-CH72 OF FCHWM72(0007)
           END-IF.
           MOVE WK01-CODE-CH727-F TO WK01-CODE-CH72-F (7).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH728-Z = 'Y'
               MOVE WK01-CODE-CH728 TO ACTN-CODE-CH72 OF FCHWM72(0008)
           END-IF.
           MOVE WK01-CODE-CH728-F TO WK01-CODE-CH72-F (8).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH729-Z = 'Y'
               MOVE WK01-CODE-CH729 TO ACTN-CODE-CH72 OF FCHWM72(0009)
           END-IF.
           MOVE WK01-CODE-CH729-F TO WK01-CODE-CH72-F (9).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH7210-Z = 'Y'
               MOVE WK01-CODE-CH7210 TO ACTN-CODE-CH72 OF FCHWM72(0010)
           END-IF.
           MOVE WK01-CODE-CH7210-F TO WK01-CODE-CH72-F (10).
      *%--------------------------------------------------------------%*
           IF WK01-CODE-CH7211-Z = 'Y'
               MOVE WK01-CODE-CH7211 TO ACTN-CODE-CH72 OF FCHWM72(0011)
           END-IF.
           MOVE WK01-CODE-CH7211-F TO WK01-CODE-CH72-F (11).
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CH72-Z = 'Y'
               MOVE WK01-STATUS-CH72 TO SLCTN-STATUS-CH72 OF FCHWK72
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-CH72-Z = 'Y'
               MOVE WK01-DATE-CH72 TO SLCTN-DATE-CH72 OF FCHWK72
           END-IF.
