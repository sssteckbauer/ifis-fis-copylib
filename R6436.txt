       01  R6436-PRSN-DTCD.
           02  DB-PROC-ID-6436.
               03  USER-CODE-6436                       PIC X(8).
               03  LAST-ACTVY-DATE-6436                 PIC X(5).
               03  TRMNL-ID-6436                        PIC X(8).
               03  PURGE-FLAG-6436                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  PRSN-DATA-CODE-KEY-6436.
               03  UNVRS-CODE-6436                      PIC X(2).
               03  PRSN-DATA-CODE-6436                  PIC X(6).
           02  SHORT-DESC-6436                          PIC X(10).
           02  LONG-DESC-6436                           PIC X(30).
           02  RQRD-PRSN-DATA-VALUE-FLAG-6436           PIC X(1).
