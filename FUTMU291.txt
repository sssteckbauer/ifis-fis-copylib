    ***Created by Convert/DC version V8R03 on 01/04/01 at 09:00***

      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE TEMPL-OWNER-NAME-UT29     TO WK01-NAME-UT02.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-DESC-UT29            TO WK01-DESC-UT02.
      *%--------------------------------------------------------------%*
           MOVE TEMPL-OWNER-USER-ID-UT29        TO WK01-ID-UT02.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-TYPE-UT29                 TO WK01-TYPE-UT02.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-UT29                  TO WK01-CODE-UT021.
      *%--------------------------------------------------------------%*
           MOVE SEQ-NMBR-UT29                   TO WK01-NMBR-UT021.
      *%--------------------------------------------------------------%*
           MOVE PRIM-LEVEL-USER-ID              TO WK01-ID-UT0A11.
      *%--------------------------------------------------------------%*
           MOVE PRIM-LEVEL-FULL-NAME-UT29       TO WK01-NAME-UT0A11.
      *%--------------------------------------------------------------%*
           MOVE APRVL-LIMIT-UT29            TO WK01-LIMIT-UT29.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-APRVL-CODE-UT29 (0001) TO
                WK01-APRVL-CODE-UT021.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-APRVL-CODE-UT29 (0002) TO
                WK01-APRVL-CODE-UT022.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-APRVL-CODE-UT29 (0003) TO
                WK01-APRVL-CODE-UT023.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-APRVL-CODE-UT29 (0004) TO
                WK01-APRVL-CODE-UT024.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-APRVL-CODE-UT29 (0005) TO
                WK01-APRVL-CODE-UT025.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-APRVL-CODE-UT29 (0006) TO
                WK01-APRVL-CODE-UT026.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-APRVL-CODE-UT29 (0007) TO
                WK01-APRVL-CODE-UT027.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-APRVL-CODE-UT29 (0008) TO
                WK01-APRVL-CODE-UT028.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-APRVL-CODE-UT29 (0009) TO
                WK01-APRVL-CODE-UT029.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-FULL-NAME-UT29(0001) TO WK01-NAME-UT0B11.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-FULL-NAME-UT29(0002) TO WK01-NAME-UT0A12.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-FULL-NAME-UT29(0003) TO WK01-NAME-UT0A13.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-FULL-NAME-UT29(0004) TO WK01-NAME-UT0A14.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-FULL-NAME-UT29(0005) TO WK01-NAME-UT0A15.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-FULL-NAME-UT29(0007) TO WK01-NAME-UT0A17.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-FULL-NAME-UT29(0008) TO WK01-NAME-UT0A18.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-FULL-NAME-UT29(0009) TO WK01-NAME-UT0A19.
      *%--------------------------------------------------------------%*
           MOVE ALTRNT-FULL-NAME-UT29(0006) TO WK01-NAME-UT0A16.
      *%--------------------------------------------------------------%*
           MOVE APRVL-TMPLT-CODE-UT29  TO
                                            WK01-TMPLT-CODE-UT02.
      *%--------------------------------------------------------------%*
           MOVE APRVL-TMPLT-DESC-UT29  TO
                                            WK01-TMPLT-DESC-UT02.
      *%--------------------------------------------------------------%*
