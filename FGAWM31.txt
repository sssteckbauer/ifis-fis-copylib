      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FGAWM31                              04/24/00  12:32  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FGAWM31.
           02  ACTN-CODE-GROUP-GA31.
               03  ACTN-CODE-GA31           OCCURS 6    PIC X(1).
           02  DCMNT-NMBR-GA31              OCCURS 6    PIC X(8).
           02  DCMNT-AMT-GA31        COMP-3 OCCURS 16   PIC S9(10)V99.
           02  ACTVY-DATE-GA31       COMP-3 OCCURS 6    PIC S9(8).
           02  CURR-STATUS-GA31             OCCURS 6    PIC X(10).
           02  PERM-STATUS-GA31             OCCURS 6    PIC X(10).
           02  CURR-STATUS-IND-GA31         OCCURS 6    PIC X(1).
           02  PERM-STATUS-IND-GA31         OCCURS 6    PIC X(1).
           02  DCMNT-DESC-GA31              OCCURS 6    PIC X(35).
