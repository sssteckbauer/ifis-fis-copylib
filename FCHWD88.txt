      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWD88                              04/24/00  12:59  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWD88.
           02  WORK-SLCTN-DATE-CH88           COMP-3    PIC S9(8).
           02  WORK-SYSTEM-DATE-CH88          COMP-3    PIC S9(8).
           02  WORK-SAVE-DBKEY-ID-4045-CH88
                                            OCCURS 11   PIC X(10).
           02  WORK-SAVE-DBKEY-ID-4067-CH88
                                            OCCURS 11   PIC X(28).
           02  WORK-LOOP-FLAG-CH88                      PIC X(1).
           02  WORK-SAVE-DBKEY-CH88           COMP      PIC S9(8).
           02  ACTN-FLAG-CH88                           PIC X(1).
           02  WORK-ADD-FLAG-CH88                       PIC X(1).
           02  WORK-LIST-FLAG-CH88                      PIC X(1).
           02  WORK-SEL-FLAG-CH88                       PIC X(1).
           02  SEL-FLAG-CH88                            PIC X(1).
