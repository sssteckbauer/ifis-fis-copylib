    ***Created by Convert/DC version V8R03 on 12/04/00 at 16:00***

      *%--------------------------------------------------------------%*
           MOVE TRMNL-ID-SY00 OF FSYWG00M TO WK01-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE FULL-NAME-6001-SY00 OF FSYWG00M TO WK01-NAME-6001-SY00.
      *%--------------------------------------------------------------%*
           MOVE AGR-DATE OF ADSO-APPLICATION-GLOBAL-RECORD TO WK01-DATE.
      *%--------------------------------------------------------------%*
           MOVE WORK-HH-MM-TIME-SY00 OF FSYWG00M TO
                WK01-HH-MM-TIME-SY00.
      *%--------------------------------------------------------------%*
           MOVE AM-PM-FLAG-SY00 OF FSYWG00M TO WK01-PM-FLAG-SY00.
      *%--------------------------------------------------------------%*
           MOVE AGR-USER-ID OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-USER-ID.
      *%--------------------------------------------------------------%*
           MOVE CURR-RSPNS-ID-SY00 OF FSYWG00M TO WK01-RSPNS-ID-SY00.
      *%--------------------------------------------------------------%*
           MOVE AGR-FUNC-DESCRIPTION OF ADSO-APPLICATION-GLOBAL-RECORD
                TO WK01-FUNC-DESCRIPTION.
      *%--------------------------------------------------------------%*
           MOVE PAGE-STATUS-CODE-SY00 OF FSYWG00M TO
                WK01-STATUS-CODE-SY00.
      *%--------------------------------------------------------------%*
           MOVE ADR-TYPE-CODE-4150-AP40 OF FAPWM40 TO
                WK01-TYPE-CODE-4150-AP40.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-AP40 OF FAPWM40(0001) TO WK01-ADR-AP401.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-AP40 OF FAPWM40(0002) TO WK01-ADR-AP402.
      *%--------------------------------------------------------------%*
           MOVE LINE-ADR-AP40 OF FAPWM40(0003) TO WK01-ADR-AP403.
      *%--------------------------------------------------------------%*
           MOVE NET-AMT-AP40 OF FAPWM40 TO WK01-AMT-AP40.
      *%--------------------------------------------------------------%*
           MOVE CITY-NAME-AP40 OF FAPWM40 TO WK01-NAME-AP40.
      *%--------------------------------------------------------------%*
           MOVE CHECK-TEXT-AP40 OF FAPWM40(0001) TO WK01-TEXT-AP401.
      *%--------------------------------------------------------------%*
           MOVE CHECK-TEXT-AP40 OF FAPWM40(0002) TO WK01-TEXT-AP402.
      *%--------------------------------------------------------------%*
           MOVE CHECK-TEXT-AP40 OF FAPWM40(0003) TO WK01-TEXT-AP403.
      *%--------------------------------------------------------------%*
           MOVE CMPLT-IND-4150-AP40 OF FAPWM40 TO WK01-IND-4150-AP40.
      *%--------------------------------------------------------------%*
           MOVE APRVL-IND-4150-AP40 OF FAPWM40 TO WK01-IND-4150-AP4A.
      *%--------------------------------------------------------------%*
           MOVE STATE-CODE-AP40 OF FAPWM40 TO WK01-CODE-AP40.
      *%--------------------------------------------------------------%*
           MOVE ZIP-CODE-AP40 OF FAPWM40 TO WK01-CODE-AP4A.
      *%--------------------------------------------------------------%*
DEVMR0* S41335
           MOVE FED-RPT-AMT-AP40 OF FAPWM40
             TO WK01-FED-RPT-AMT.
      *%--------------------------------------------------------------%*
           MOVE FED-WH-AMT-AP40 OF FAPWM40
             TO WK01-FED-WH-AMT.
      *%--------------------------------------------------------------%*
DEVMR0* S41335
DEVMJM* FP3175
           MOVE 592-IND-AP40 OF FAPWM40
             TO WK01-592-IND.
      *%--------------------------------------------------------------%*
           MOVE CA-RPT-AMT-AP40 OF FAPWM40
             TO WK01-CA-RPT-AMT.
      *%--------------------------------------------------------------%*
           MOVE CA-WH-AMT-AP40 OF FAPWM40
             TO WK01-CA-WH-AMT.
      *%--------------------------------------------------------------%*
DEVMJM* FP3175
           MOVE AGR-PASSED-ONE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-PASSED-ONE.
      *%--------------------------------------------------------------%*
           MOVE AGR-MAP-RESPONSE OF ADSO-APPLICATION-GLOBAL-RECORD TO
                WK01-MAP-RESPONSE.
      *%--------------------------------------------------------------%*
           MOVE ERROR-TEXT-SY00 OF FSYWG00M TO WK01-TEXT-SY00.
      *%--------------------------------------------------------------%*
           MOVE CNCL-IND-4150-AP40 OF FAPWM40 TO WK01-IND-4150-AP4B.
      *%--------------------------------------------------------------%*
           MOVE APRVD-AMT-AP40 OF FAPWM40 TO WK01-AMT-AP4A.
      *%--------------------------------------------------------------%*
           MOVE ADDL-CHRG-AP40 OF FAPWM40 TO WK01-CHRG-AP40.
      *%--------------------------------------------------------------%*
           MOVE TAX-AMT-AP40 OF FAPWM40 TO WK01-AMT-AP4B.
      *%--------------------------------------------------------------%*
           MOVE NO-TAX-IND-AP40 OF FAPWM40 TO WK01-TAX-IND-AP40.
      *%--------------------------------------------------------------%*
           MOVE SALES-TAX-IND-AP40 OF FAPWM40 TO WK01-TAX-IND-AP4A.
      *%--------------------------------------------------------------%*
           MOVE USE-TAX-IND-AP40 OF FAPWM40 TO WK01-TAX-IND-AP4B.
      *%--------------------------------------------------------------%*
FP7157     MOVE TAX-TREATY-IND-AP40 OF FAPWM40
FP7157                                  TO WK01-TAX-TREATY-IND-AP40.
FP7157*%--------------------------------------------------------------%*
FP7157     MOVE IRS-INCOME-CD-AP40 OF FAPWM40
FP7157                                  TO WK01-IRS-INCOME-CD-AP40.
FP7157*%--------------------------------------------------------------%*
FP7157     MOVE INC-TYP-AP40 OF FAPWM40
FP7157                                  TO WK01-INC-TYP-AP40.
FP7157*%--------------------------------------------------------------%*
           MOVE TRANS-DATE-4150-AP40 OF FAPWM40 TO WK01-DATE-4150-AP40.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-NMBR-4150-AP40 OF FAPWK40 TO WK01-NMBR-4150-AP40.
      *%--------------------------------------------------------------%*
           MOVE VNDR-ID-LAST-NINE-4073-AP40 OF FAPWK40 TO
                WK01-ID-LAST-NN-4073-AP40.
      *%--------------------------------------------------------------%*
           MOVE ACTN-CODE-AP40 OF FAPWM40 TO WK01-CODE-AP4B.
      *%--------------------------------------------------------------%*
           MOVE SUB-DCMNT-TYPE-AP40 OF FAPWM40 TO WK01-DCMNT-TYPE-AP40.
      *%--------------------------------------------------------------%*
           MOVE DCMNT-REF-NMBR-AP40 OF FAPWM40 TO WK01-REF-NMBR-AP40.
      *%--------------------------------------------------------------%*
           MOVE 1099-IND-4150-AP40 OF FAPWM40 TO WK01-IND-4150-AP4C.
      *%--------------------------------------------------------------%*
           MOVE VNDR-NAME-GP-AP40 OF FAPWM40 TO WK01-NAME-GP-AP40.
      *%--------------------------------------------------------------%*
           MOVE CHECK-GRPNG-IND-4150-AP40 OF FAPWM40 TO
                WK01-GRPNG-IND-4150-AP40.
      *%--------------------------------------------------------------%*
           MOVE CHECK-NMBR-AP40 OF FAPWM40 TO WK01-NMBR-AP40.
      *%--------------------------------------------------------------%*
           MOVE CHECK-DATE-AP40 OF FAPWM40 TO WK01-DATE-AP40.
      *%--------------------------------------------------------------%*
           MOVE APRVL-TMPLT-CODE-AP40 OF FAPWM40 TO
                WK01-TMPLT-CODE-AP40.
      *%--------------------------------------------------------------%*
           MOVE DELETE-LIT-AP40 OF FAPWM40 TO WK01-LIT-AP40.
      *%--------------------------------------------------------------%*
           MOVE CONFIRM-DELETE-AP40 OF FAPWM40 TO WK01-DELETE-AP40.
      *%--------------------------------------------------------------%*
           MOVE ITEM-ACTN-CODE-AP40 OF FAPWM40 TO WK01-ACTN-CODE-AP40.
      *%--------------------------------------------------------------%*
           MOVE ACCT-APRVD-AMT-AP40 OF FAPWM40 TO WK01-APRVD-AMT-AP40.
      *%--------------------------------------------------------------%*
           MOVE ACCT-TAX-AMT-AP40 OF FAPWM40 TO WK01-TAX-AMT-AP40.
      *%--------------------------------------------------------------%*
           MOVE ACCT-ADDL-CHRG-AP40 OF FAPWM40 TO WK01-ADDL-CHRG-AP40.
      *%--------------------------------------------------------------%*
           MOVE ACCT-NET-AMT-AP40 OF FAPWM40 TO WK01-NET-AMT-AP40.
      *%--------------------------------------------------------------%*
           MOVE DCLINK-MESSAGE  TO WK01-ZZMESSAGE.
      *%--------------------------------------------------------------%*
           MOVE TEXT-IND-AP40 OF FAPWM40 TO WK01-IND-AP40.
      *%--------------------------------------------------------------%*
FP2006     MOVE FORCE-CHK-AP40 OF FAPWM40 TO WK01-FORCE-CHK-AP40.
      *%--------------------------------------------------------------%*
           MOVE SEQ-NMBR-WK-AP40 OF FAPWK40 TO WK01-NMBR-WK-AP40.
      *%--------------------------------------------------------------%*
           MOVE COA-CODE-AP40 OF FAPWM40 TO WK01-CODE-AP4C.
      *%--------------------------------------------------------------%*
           MOVE ACCT-INDX-CODE-4152-AP40 OF FAPWM40 TO
                WK01-INDX-CODE-4152-AP40.
      *%--------------------------------------------------------------%*
           MOVE FUND-CODE-4152-AP40 OF FAPWM40 TO WK01-CODE-4152-AP40.
      *%--------------------------------------------------------------%*
           MOVE ORGZN-CODE-4152-AP40 OF FAPWM40 TO WK01-CODE-4152-AP4A.
      *%--------------------------------------------------------------%*
           MOVE ACCT-CODE-4152-AP40 OF FAPWM40 TO WK01-CODE-4152-AP4B.
      *%--------------------------------------------------------------%*
           MOVE PRGRM-CODE-4152-AP40 OF FAPWM40 TO WK01-CODE-4152-AP4C.
      *%--------------------------------------------------------------%*
           MOVE ACTVY-CODE-4152-AP40 OF FAPWM40 TO WK01-CODE-4152-AP4D.
      *%--------------------------------------------------------------%*
           MOVE LCTN-CODE-4152-AP40 OF FAPWM40 TO WK01-CODE-4152-AP4E.
      *%--------------------------------------------------------------%*
           MOVE VNDR-INV-NMBR-4150-AP40 OF FAPWM40 TO
                WK01-INV-NMBR-4150-AP40.
      *%--------------------------------------------------------------%*
           MOVE OPEN-PAID-IND-4150-AP40 OF FAPWM40 TO
                WK01-PAID-IND-4150-AP40.
      *%--------------------------------------------------------------%*
           MOVE CLAUSE-CODE-4169-AP40 OF FAPWM40 TO WK01-CODE-4169-AP40.
