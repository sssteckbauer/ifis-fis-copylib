      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWM48                              04/24/00  12:37  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWM48.
           02  ACTN-CODE-GROUP-PU48.
               03  ACTN-CODE-PU48           OCCURS 3    PIC X(1).
           02  RQST-CODE-4070-PU48          OCCURS 3    PIC X(8).
           02  RQST-NAME-4070-PU48          OCCURS 3    PIC X(35).
           02  TRANS-DATE-4070-PU48         OCCURS 3    PIC X(6).
           02  DLVRY-DATE-4070-PU48         OCCURS 3    PIC X(6).
           02  RQST-CMPLT-IND-4070-PU48     OCCURS 3    PIC X(1).
           02  ORGZN-TITLE-4010-PU48        OCCURS 3    PIC X(35).
