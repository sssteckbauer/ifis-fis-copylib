      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FZYWC01                              04/24/00  13:10  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FZYWC01.
           02  START-FIELD-SY01                         PIC X(1).
           02  PROCESS-ID-SY01                          PIC X(8).
           02  SAVE-DBKEY-ID-SY01             COMP      PIC S9(8).
           02  DATE-SY01                      COMP-3    PIC S9(8).
           02  PRINTER-CLASS-SY01                       PIC 9(2).
           02  FLAG-1-SY01                              PIC X(1).
           02  LONG-DESC-SY01                           PIC X(228).
           02  STOP-FIELD-SY01                          PIC X(1).
