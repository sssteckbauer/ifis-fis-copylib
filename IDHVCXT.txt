       976-FROM-R6260 SECTION.

FCCCE**        MOVE USER-CODE-6260 OF R6260
FCCCE          MOVE DBLINK-USER-CD
               TO CXT-USER-CD.

               MOVE LAST-ACTVY-DATE-6260 OF R6260
               TO CXT-LAST-ACTVY-DT.

               MOVE UNVRS-CODE-6260 OF R6260
               TO CXT-UNVRS-CD.

               MOVE TEXT-CODE-6260 OF R6260
               TO CXT-TEXT-CD.

               MOVE SHORT-DESC-6260 OF R6260
               TO CXT-SHORT-DESC.

               MOVE LONG-DESC-6260 OF R6260
               TO CXT-LONG-DESC.

           IF LINE-LGTH-6260 OF R6260  NOT NUMERIC
               MOVE ZEROS TO CXT-LINE-LGTH
           ELSE
               MOVE LINE-LGTH-6260 OF R6260
               TO CXT-LINE-LGTH
           END-IF.

           IF LINES-PER-PAGE-6260 OF R6260  NOT NUMERIC
               MOVE ZEROS TO CXT-LINES-PER-PAGE
           ELSE
               MOVE LINES-PER-PAGE-6260 OF R6260
               TO CXT-LINES-PER-PAGE
           END-IF.

           IF TOP-MARGIN-6260 OF R6260  NOT NUMERIC
               MOVE ZEROS TO CXT-TOP-MARGIN
           ELSE
               MOVE TOP-MARGIN-6260 OF R6260
               TO CXT-TOP-MARGIN
           END-IF.

               MOVE LABEL-ID-6260 OF R6260
               TO CXT-LABEL-ID.

               MOVE VERIFY-FLAG-6260 OF R6260
               TO CXT-VERIFY-FLAG.
       976-FROM-R6260-EXIT.
           EXIT.
