    ***Created by Convert/DC version V8R03 on 12/11/00 at 15:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-PU66-Z = 'Y'
               MOVE WK01-NMBR-PU66 TO PO-NMBR-PU66 OF FPUWK66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PU66-Z = 'Y'
               MOVE WK01-CODE-PU66 TO ACTN-CODE-PU66 OF FPUWK66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-PU66-Z = 'Y'
               MOVE WK01-AMT-PU66 TO TOTAL-AMT-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CHRG-PU66-Z = 'Y'
               MOVE WK01-CHRG-PU66 TO ADDL-CHRG-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-PU6A-Z = 'Y'
               MOVE WK01-NMBR-PU6A TO ITEM-NMBR-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PU66-Z = 'Y'
               MOVE WK01-PU66 TO QTY-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PRICE-PU66-Z = 'Y'
               MOVE WK01-PRICE-PU66 TO UNIT-PRICE-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DSCNT-AMT-PU66-Z = 'Y'
               MOVE WK01-DSCNT-AMT-PU66 TO ITEM-DSCNT-AMT-PU66 OF
                FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DSCNT-AMT-PU6A-Z = 'Y'
               MOVE WK01-DSCNT-AMT-PU6A TO PO-DSCNT-AMT-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-UNIT-PRICE-PU66-Z = 'Y'
               MOVE WK01-UNIT-PRICE-PU66 TO EXTND-UNIT-PRICE-PU66 OF
                FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TAX-AMT-PU66-Z = 'Y'
               MOVE WK01-TAX-AMT-PU66 TO ITEM-TAX-AMT-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-PU6B-Z = 'Y'
               MOVE WK01-NMBR-PU6B TO SEQ-NMBR-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-INDX-CODE-PU66-Z = 'Y'
               MOVE WK01-INDX-CODE-PU66 TO ACCT-INDX-CODE-PU66 OF
                FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-PU6A-Z = 'Y'
               MOVE WK01-AMT-PU6A TO SEQ-AMT-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PU6A-Z = 'Y'
               MOVE WK01-CODE-PU6A TO FUND-CODE-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TAX-AMT-PU6A-Z = 'Y'
               MOVE WK01-TAX-AMT-PU6A TO SEQ-TAX-AMT-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PU6B-Z = 'Y'
               MOVE WK01-CODE-PU6B TO ORGZN-CODE-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ADDL-CHRG-PU66-Z = 'Y'
               MOVE WK01-ADDL-CHRG-PU66 TO SEQ-ADDL-CHRG-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PU6C-Z = 'Y'
               MOVE WK01-CODE-PU6C TO ACCT-CODE-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PCT-DSTBN-PU66-Z = 'Y'
               MOVE WK01-PCT-DSTBN-PU66 TO AMT-PCT-DSTBN-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PCT-DSTBN-PU6A-Z = 'Y'
               MOVE WK01-PCT-DSTBN-PU6A TO TAX-PCT-DSTBN-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PCT-DSTBN-PU6B-Z = 'Y'
               MOVE WK01-PCT-DSTBN-PU6B TO ADDL-PCT-DSTBN-PU66 OF
                FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-NMBR-PU66-Z = 'Y'
               MOVE WK01-SEQ-NMBR-PU66 TO CHNG-SEQ-NMBR-PU66 OF FPUWK66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CO-PU66-Z = 'Y'
               MOVE WK01-CO-PU66 TO TOTAL-CO-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-COUNT-PU66-Z = 'Y'
               MOVE WK01-COUNT-PU66 TO ITEM-COUNT-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-COUNT-PU6A-Z = 'Y'
               MOVE WK01-COUNT-PU6A TO SEQ-COUNT-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TOTAL-PU66-Z = 'Y'
               MOVE WK01-TOTAL-PU66 TO ITEM-TOTAL-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-ADD-PU66-Z = 'Y'
               MOVE WK01-SEQ-ADD-PU66 TO TOTAL-SEQ-ADD-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-TAX-PU66-Z = 'Y'
               MOVE WK01-SEQ-TAX-PU66 TO TOTAL-SEQ-TAX-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-AMT-PU66-Z = 'Y'
               MOVE WK01-SEQ-AMT-PU66 TO TOTAL-SEQ-AMT-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HDR-DESC-PU66-Z = 'Y'
               MOVE WK01-HDR-DESC-PU66 TO PO-HDR-DESC-PU66 OF FPUWM66
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DTL-DESC-PU66-Z = 'Y'
               MOVE WK01-DTL-DESC-PU66 TO PO-DTL-DESC-PU66 OF FPUWM66
           END-IF.
