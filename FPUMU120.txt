    ***Created by Convert/DC version V8R03 on 12/04/00 at 10:00***

      *%--------------------------------------------------------------%*
           IF WK01-ZZMESSAGE-Z = 'Y'
               MOVE WK01-ZZMESSAGE TO DCLINK-MESSAGE
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-Z = 'Y'
               MOVE WK01-DATE TO AGR-DATE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-USER-ID-Z = 'Y'
               MOVE WK01-USER-ID TO AGR-USER-ID OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FUNC-DESCRIPTION-Z = 'Y'
               MOVE WK01-FUNC-DESCRIPTION TO AGR-FUNC-DESCRIPTION OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PASSED-ONE-Z = 'Y'
               MOVE WK01-PASSED-ONE TO AGR-PASSED-ONE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-MAP-RESPONSE-Z = 'Y'
               MOVE WK01-MAP-RESPONSE TO AGR-MAP-RESPONSE OF
                ADSO-APPLICATION-GLOBAL-RECORD
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-SY00-Z = 'Y'
               MOVE WK01-ID-SY00 TO TRMNL-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-6001-SY00-Z = 'Y'
               MOVE WK01-NAME-6001-SY00 TO FULL-NAME-6001-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PM-FLAG-SY00-Z = 'Y'
               MOVE WK01-PM-FLAG-SY00 TO AM-PM-FLAG-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RSPNS-ID-SY00-Z = 'Y'
               MOVE WK01-RSPNS-ID-SY00 TO CURR-RSPNS-ID-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TEXT-SY00-Z = 'Y'
               MOVE WK01-TEXT-SY00 TO ERROR-TEXT-SY00 OF FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-STATUS-CODE-SY00-Z = 'Y'
               MOVE WK01-STATUS-CODE-SY00 TO PAGE-STATUS-CODE-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-HH-MM-TIME-SY00-Z = 'Y'
               MOVE WK01-HH-MM-TIME-SY00 TO WORK-HH-MM-TIME-SY00 OF
                FSYWG00M
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NMBR-4083-PU12-Z = 'Y'
               MOVE WK01-NMBR-4083-PU12 TO PO-NMBR-4083-PU12 OF FPUWK12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-NMBR-4096-PU12-Z = 'Y'
               MOVE WK01-SEQ-NMBR-4096-PU12 TO CHNG-SEQ-NMBR-4096-PU12
                OF FPUWK12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-ID-LAST-NINE-4073-PU12-Z = 'Y'
               MOVE WK01-ID-LAST-NINE-4073-PU12 TO
                VNDR-ID-LAST-NINE-4073-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-KEY-6311-PU12-Z = 'Y'
               MOVE WK01-KEY-6311-PU12 TO NAME-KEY-6311-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4096-PU12-Z = 'Y'
               MOVE WK01-DATE-4096-PU12 TO ORDER-DATE-4096-PU12 OF
                FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-PU12-Z = 'Y'
               MOVE WK01-CODE-PU12 TO ACTN-CODE-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RATE-CODE-4083-PU12-Z = 'Y'
               MOVE WK01-RATE-CODE-4083-PU12 TO TAX-RATE-CODE-4083-PU12
                OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RATE-DESC-4154-PU12-Z = 'Y'
               MOVE WK01-RATE-DESC-4154-PU12 TO TAX-RATE-DESC-4154-PU12
                OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4083-PU12-Z = 'Y'
               MOVE WK01-CODE-4083-PU12 TO DSCNT-CODE-4083-PU12 OF
                FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4172-PU12-Z = 'Y'
               MOVE WK01-DESC-4172-PU12 TO DSCNT-DESC-4172-PU12 OF
                FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4083-PU1A-Z = 'Y'
               MOVE WK01-CODE-4083-PU1A TO PYMT-CODE-4083-PU12 OF
                FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DESC-4078-PU12-Z = 'Y'
               MOVE WK01-DESC-4078-PU12 TO PYMT-DESC-4078-PU12 OF
                FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-TO-CODE-4083-PU12-Z = 'Y'
               MOVE WK01-TO-CODE-4083-PU12 TO SHIP-TO-CODE-4083-PU12 OF
                FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-4086-PU12-Z = 'Y'
               MOVE WK01-NAME-4086-PU12 TO CNTCT-NAME-4086-PU12 OF
                FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AREA-CODE-4086-PU12-Z = 'Y'
               MOVE WK01-AREA-CODE-4086-PU12 TO
                TLPHN-AREA-CODE-4086-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-XCHNG-ID-4086-PU12-Z = 'Y'
               MOVE WK01-XCHNG-ID-4086-PU12 TO TLPHN-XCHNG-ID-4086-PU12
                OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-SEQ-ID-4086-PU12-Z = 'Y'
               MOVE WK01-SEQ-ID-4086-PU12 TO TLPHN-SEQ-ID-4086-PU12 OF
                FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-XTNSN-ID-4086-PU12-Z = 'Y'
               MOVE WK01-XTNSN-ID-4086-PU12 TO TLPHN-XTNSN-ID-4086-PU12
                OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-1-4086-PU12-Z = 'Y'
               MOVE WK01-1-4086-PU12 TO ADR-1-4086-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-2-4086-PU12-Z = 'Y'
               MOVE WK01-2-4086-PU12 TO ADR-2-4086-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-3-4086-PU12-Z = 'Y'
               MOVE WK01-3-4086-PU12 TO ADR-3-4086-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-4-4086-PU12-Z = 'Y'
               MOVE WK01-4-4086-PU12 TO ADR-4-4086-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-NAME-4086-PU1A-Z = 'Y'
               MOVE WK01-NAME-4086-PU1A TO CITY-NAME-4086-PU12 OF
                FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4086-PU12-Z = 'Y'
               MOVE WK01-CODE-4086-PU12 TO STATE-CODE-4086-PU12 OF
                FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CODE-4086-PU1A-Z = 'Y'
               MOVE WK01-CODE-4086-PU1A TO ZIP-CODE-4086-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FAX-AREA-CODE-PU12-Z = 'Y'
               MOVE WK01-FAX-AREA-CODE-PU12 TO
                FAX-TLPHN-AREA-CODE-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FAX-XCHNG-ID-PU12-Z = 'Y'
               MOVE WK01-FAX-XCHNG-ID-PU12 TO
                FAX-TLPHN-XCHNG-ID-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FAX-SEQ-ID-PU12-Z = 'Y'
               MOVE WK01-FAX-SEQ-ID-PU12 TO
                FAX-TLPHN-SEQ-ID-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-VDR-EMAIL-ADR-PU12-Z = 'Y'
               MOVE WK01-VDR-EMAIL-ADR-PU12
                 TO VDR-EMAIL-ADR-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RISK-CODE-4083-PU12-Z = 'Y'
               MOVE WK01-RISK-CODE-4083-PU12 TO
                TRNST-RISK-CODE-4083-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-RISK-DESC-4102-PU12-Z = 'Y'
               MOVE WK01-RISK-DESC-4102-PU12 TO
                TRNST-RISK-DESC-4102-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-IND-4083-PU12-Z = 'Y'
               MOVE WK01-IND-4083-PU12 TO ACKNWDG-IND-4083-PU12 OF
                FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-DATE-4096-PU1A-Z = 'Y'
               MOVE WK01-DATE-4096-PU1A TO ACKNWDG-DATE-4096-PU12 OF
                FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FLAG-PU12-Z = 'Y'
               MOVE WK01-FLAG-PU12 TO TEXT-FLAG-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4096-PU12-Z = 'Y'
               MOVE WK01-AMT-4096-PU12 TO TOTAL-AMT-4096-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4096-PU1A-Z = 'Y'
               MOVE WK01-AMT-4096-PU1A TO TAX-AMT-4096-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-4096-PU1B-Z = 'Y'
               MOVE WK01-AMT-4096-PU1B TO ADDL-AMT-4096-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-PU12-Z = 'Y'
               MOVE WK01-AMT-PU12 TO NET-AMT-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PU12-Z = 'Y'
               MOVE WK01-PU12 TO MAILCODE-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-CNTCT-NAME-PU12-Z = 'Y'
               MOVE WK01-CNTCT-NAME-PU12 TO VNDR-CNTCT-NAME-PU12 OF
                FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-PCT-PU12-Z = 'Y'
               MOVE WK01-PCT-PU12 TO DSCNT-PCT-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-BEFORE-TAX-IND-PU12-Z = 'Y'
               MOVE WK01-BEFORE-TAX-IND-PU12 TO
                DSCNT-BEFORE-TAX-IND-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-AMT-PU1A-Z = 'Y'
               MOVE WK01-AMT-PU1A TO DSCNT-AMT-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-C-O-PU12-Z = 'Y'
               MOVE WK01-C-O-PU12 TO TOTAL-C-O-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-BY-DATE-PU12-Z = 'Y'
               MOVE WK01-BY-DATE-PU12 TO DLVRY-BY-DATE-PU12 OF FPUWM12
           END-IF.
      *%--------------------------------------------------------------%*
           IF WK01-FAX-EXISTS-IND-PU12-Z = 'Y'
               MOVE WK01-FAX-EXISTS-IND-PU12 TO
                VNDR-FAX-EXISTS-IND-PU12 OF FPUWM12
           END-IF.
